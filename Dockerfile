# This Dockerfile describes the environment to build PM2, not to use it.
# Its main purpose is the for CI.
FROM debian:latest
RUN apt-get update && \
  apt-get --no-install-recommends --yes install \
  build-essential \
  procps \
  autoconf \
  gcc \
  g++ \
  gfortran \
  git \
  pkg-config \
  doxygen \
  graphviz \
  libexpat1-dev \
  hwloc \
  libhwloc-dev \
  libudev-dev \
  libpmi2-0-dev \
  libpmix-dev \
  libpmix-bin \
  libibverbs-dev \
  libssh-dev \
  libpsm2-dev \
  libucx-dev \
  libfabric-dev \
  libpsm-infinipath1-dev \
  librdmacm-dev \
  libcurl4-gnutls-dev \
  liblz4-dev \
  libjansson-dev \
  libbz2-dev \
  libgtg-dev \
  zlib1g-dev \
  valgrind \
  shtool
