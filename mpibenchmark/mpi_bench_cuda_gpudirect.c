/*
 * MadMPI benchmark
 * Copyright (C) 2015-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "mpi_bench_generic.h"

#ifdef HAVE_MPIEXT_H
#include <mpi-ext.h>
#endif /* HAVE_MPIEXT_H */

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

static void*ptr_gpu = NULL;

static int cuda_support = 0;

static int mpi_bench_cuda_gpudirect_skip(void)
{
#if defined(MPIX_CUDA_AWARE_SUPPORT)
  cuda_support = MPIX_Query_cuda_support();
#elif defined(MPIX_GPU_SUPPORT_CUDA)
  MPIX_GPU_query_support(MPIX_GPU_SUPPORT_CUDA, &cuda_support);
#endif /* MPIX_CUDA_AWARE_SUPPORT */
  if(!cuda_support)
    {
      fprintf(stderr, "# MPI library does not support CUDA.\n");
    }
  return !cuda_support;
}
static void mpi_bench_cuda_gpudirect_init(void*buf, size_t len)
{
  if(!cuda_support)
    {
      fprintf(stderr, "Cannot initialize GPUDirect benchmark; MPI library does not support CUDA.\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
  cudaError_t rc = cudaMalloc(&ptr_gpu, len );
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot allocate %ld bytes on GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
}

static void mpi_bench_cuda_gpudirect_finalize(void)
{
  cudaFree(ptr_gpu);
  ptr_gpu = NULL;
}

static void mpi_bench_cuda_gpudirect_server(void*buf, size_t len)
{
  MPI_Recv(ptr_gpu, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Send(ptr_gpu, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD);
}

static void mpi_bench_cuda_gpudirect_client(void*buf, size_t len)
{
  MPI_Send(ptr_gpu, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD);
  MPI_Recv(ptr_gpu, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

const struct mpi_bench_s mpi_bench_cuda_gpudirect =
  {
    .label    = "mpi_bench_cuda_gpudirect",
    .name     = "MPI sendrecv from/to device memory, through GPUDirect",
    .rtt      = MPI_BENCH_RTT_HALF,
    .init     = &mpi_bench_cuda_gpudirect_init,
    .finalize = &mpi_bench_cuda_gpudirect_finalize,
    .server   = &mpi_bench_cuda_gpudirect_server,
    .client   = &mpi_bench_cuda_gpudirect_client,
    .skip     = &mpi_bench_cuda_gpudirect_skip
  };
