/*
 * MadMPI benchmark
 * Copyright (C) 2015-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "mpi_bench_generic.h"

static int size = -1;
static void*recv_buf = NULL;

static int in_place = 1;

static void mpi_bench_coll_iallgather_init(void*buf, size_t len)
{
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  mpi_bench_check_size(size * len);
  recv_buf = malloc(len * size);
}

static void mpi_bench_coll_iallgather_finalize(void)
{
  free(recv_buf);
  recv_buf = NULL;
}

static void mpi_bench_coll_iallgather_func(void*buf, size_t len)
{
  if(in_place)
    {
      int rank = -1;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      memcpy(recv_buf + len * rank, buf, len);
      MPI_Request req;
      MPI_Iallgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, recv_buf, len, MPI_BYTE, MPI_COMM_WORLD, &req);
      MPI_Wait(&req, MPI_STATUS_IGNORE);
    }
  else
    {
      MPI_Request req;
      MPI_Iallgather(buf, len, MPI_BYTE, recv_buf, len, MPI_BYTE, MPI_COMM_WORLD, &req);
      MPI_Wait(&req, MPI_STATUS_IGNORE);
    }
}

const struct mpi_bench_s mpi_bench_coll_iallgather =
  {
    .label      = "mpi_bench_coll_iallgather",
    .name       = "MPI iallgather",
    .rtt        = MPI_BENCH_RTT_COLLECTIVE,
    .collective = 1,
    .init       = &mpi_bench_coll_iallgather_init,
    .finalize   = &mpi_bench_coll_iallgather_finalize,
    .server     = &mpi_bench_coll_iallgather_func,
    .client     = &mpi_bench_coll_iallgather_func
  };
