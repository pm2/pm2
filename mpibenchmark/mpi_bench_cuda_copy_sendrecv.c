/*
 * MadMPI benchmark
 * Copyright (C) 2015-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "mpi_bench_generic.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

static void*ptr_gpu = NULL;

static void mpi_bench_cuda_copy_init(void*buf, size_t len)
{
  cudaError_t rc = cudaMalloc(&ptr_gpu, len );
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot allocate %ld bytes on GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
}

static void mpi_bench_cuda_copy_finalize(void)
{
  cudaFree(ptr_gpu);
  ptr_gpu = NULL;
}

static void mpi_bench_cuda_copy_server(void*buf, size_t len)
{
  MPI_Recv(buf, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  cudaError_t rc = cudaMemcpy(ptr_gpu, buf, len, cudaMemcpyHostToDevice);
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot copy %ld bytes to GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
  rc = cudaMemcpy(buf, ptr_gpu, len, cudaMemcpyDeviceToHost);
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot copy %ld bytes from GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
  MPI_Send(buf, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD);
}

static void mpi_bench_cuda_copy_client(void*buf, size_t len)
{
  cudaError_t rc = cudaMemcpy(buf, ptr_gpu, len, cudaMemcpyDeviceToHost);
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot copy %ld bytes from GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
  MPI_Send(buf, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD);
  MPI_Recv(buf, len, MPI_CHAR, mpi_bench_common.peer, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  rc = cudaMemcpy(ptr_gpu, buf, len, cudaMemcpyHostToDevice);
  if(rc != cudaSuccess)
    {
      fprintf(stderr, "cannot copy %ld bytes to GPU; error %s\n", len, cudaGetErrorString(rc));
      abort();
    }
}

const struct mpi_bench_s mpi_bench_cuda_copy_sendrecv =
  {
    .label    = "mpi_bench_cuda_copy_sendrecv",
    .name     = "MPI sendrecv from/to device memory, through copy",
    .rtt      = MPI_BENCH_RTT_HALF,
    .init     = &mpi_bench_cuda_copy_init,
    .finalize = &mpi_bench_cuda_copy_finalize,
    .server   = &mpi_bench_cuda_copy_server,
    .client   = &mpi_bench_cuda_copy_client
  };
