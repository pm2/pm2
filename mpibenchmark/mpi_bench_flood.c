/*
 * MadMPI benchmark
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "mpi_bench_generic.h"

#include "mpi_sync_clocks.h"

#define SIZE (400 * 1024)
#define ITERS 10

static mpi_sync_clocks_t mpi_clock = NULL;

int main(int argc, char **argv)
{
  int rank, commsize;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &commsize);

  mpi_clock = mpi_sync_clocks_init(MPI_COMM_WORLD);

  char*sbuf = malloc(SIZE);
  char*rbuf = malloc(SIZE);
  memset(sbuf, 0, SIZE);

  if(rank == 0)
    {
      fprintf(stderr, "# commsize | time/iter (usec.) | bw \n");
    }

  MPI_Request*rreqs = malloc(commsize * sizeof(MPI_Request));
  MPI_Request*sreqs = malloc(commsize * sizeof(MPI_Request));
  int c, i, j;
  for(c = 2 ; c < commsize; c++)
    {
      if(rank < c)
        {
          const double t1 = mpi_sync_clocks_get_time_usec(mpi_clock);
          for(i = 0; i < ITERS; i++)
            {
              for(j = 0; j < c - 1; j++)
                {
                  const int dest = (rank + j) % c;
                  const int from = (rank - j + c) % c;
                  const int tag = j;

                  MPI_Isend(sbuf, SIZE, MPI_CHAR, dest, tag, MPI_COMM_WORLD, &sreqs[j]);
                  MPI_Irecv(rbuf, SIZE, MPI_CHAR, from, tag, MPI_COMM_WORLD, &rreqs[j]);

                }
              for(j = 0; j < c - 1; j++)
                {
                  MPI_Wait(&sreqs[j], MPI_STATUS_IGNORE);
                  MPI_Wait(&rreqs[j], MPI_STATUS_IGNORE);
                }
            }
          const double t2 = mpi_sync_clocks_get_time_usec(mpi_clock);
          const double d = t2 - t1;
          if(rank == 0)
            {
              const double d_iter = d / (double)ITERS;
              const double bw = (SIZE * (c - 1) / (d_iter));
              fprintf(stderr, "%8d       %9.2lf     %9.2lf\n", c, d_iter, bw);
            }
        }
    }

  mpi_sync_clocks_shutdown(mpi_clock);

  MPI_Finalize();
  return 0;
}
