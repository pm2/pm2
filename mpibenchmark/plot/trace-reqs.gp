# -*- mode: Gnuplot;-*-

#  MadMPI benchmark
#  Copyright (C) 2016-2024 (see AUTHORS file)
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or (at
#  your option) any later version.
# 
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.

# This file is not supposed to be used standalone.

filename  = ARG1
bench     = ARG2
benchfile = bench . ".dat"
outdir    = filename . ".d/"

print "# filename:  " . filename
print "# outdir:    " . outdir
print "# bench:     " . bench
print "# benchfile: " . benchfile

set grid

set key bmargin box
set key off
set xlabel "Number of requests"
set logscale x 2

set ylabel "Latency per request (usec.)"
set title "Latency for ".bench
set output bench.".".OUT_EXT
plot [:][0:] benchfile using 1:($5/$1) with lines lc 1, benchfile using 1:($5/$1):($8/$1):($9/$1) with errorbars lc 1

