#
README for mpi_sync_clocks
==========================

This document describes mpi_sync_clocks installation and configuration.

for any question, send an email to: <Alexandre.Denis@inria.fr>.

mpi_sync_clocks is an implementation of synchronized clocks using
MPI. It offers:

  - interpolated clocks to get a consistent clock accross nodes
    through interpolation, i.e. computed at the end of the
    execution. This is the most precise method.
  - extrapolated clocks to compute consistent clocks accross nodes in
    realtime. This method is less precise than interpolation.
  - synchronized barrier: a barrier that guarantees to exit on all
    nodes at the same time, contrary to `MPI_Barrier`. This is
    especially useful to benchmark collective operations.


Requirements
------------

  - autoconf (v 2.50 or later)
  - any MPI library

Installation
------------

- Install follows usual autotools procedure:

      ./autogen.sh
      ./configure [your options here]
      make install

- no compilation is needed. The library consists only of headers.

Documentation
-------------

- To locally generate doxygen documentation:

      % make docs

- It is also available online at <http://pm2.gitlabpages.inria.fr/pm2/mpi_sync_clocks/doc/>.

- Reference API documentation: @ref mpi_sync_clocks
