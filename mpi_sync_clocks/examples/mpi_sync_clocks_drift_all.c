/*
 * MPI sync clocks
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/** @file measure drift of multiple synchronized clocks mechanisms
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mpi.h>

#include <mpi_sync_clocks.h>

#define ROUNDS_DEFAULT 100
#define ROUNDS_TIME_SECONDS 5
#define ROUNDTRIPS 1000

#define EXTRAPOLATE_CLOCK_INIT_TIME 10 /**< in seconds */

static int rank = -1, worldsize = -1;

struct nm_clock_method_s
{
  const char*name;
  /** init the clock method & set time origin */
  void (*init)(void);
  /** get local time in usec. */
  double (*get_time_usec)(void);
  void (*finalize)(void);
  /** translate local time to global time */
  double (*translate_time)(double t, int n);
  struct nm_clock_drift_s*p_drift;
};

/* ** generic synchronization ****************************** */

static void generic_sync(int dest, double(*get_time)(void), double*t_master, double*t_slave)
{
  const int tag_sync = 42;
  if(rank == 0)
    {
      /* pingpong to get a common event on both master time and slave time */
      double min_lat = -1.0;
      double t_begin = -1.0, t_mid = -1.0, t_end = -1.0;
      int j;
      for(j = 0; j < ROUNDTRIPS; j++)
        {
          double t_dummy = -1.0;
          t_begin = (*get_time)();
          MPI_Send(&t_dummy, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD);
          MPI_Recv(&t_mid, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          t_end = (*get_time)();
          const double lat = (t_end - t_begin) / 2.0;
          if(min_lat < 0 || lat < min_lat)
            {
              min_lat = lat;
            }
        }
      *t_master = t_begin;
      *t_slave  = t_mid - min_lat;
      MPI_Send(t_master, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD);
      MPI_Send(t_slave, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD);
    }
  else
    {
      int j;
      for(j = 0; j < ROUNDTRIPS; j++)
        {
          double t_dummy = -1.0;
          MPI_Recv(&t_dummy, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          double t_mid = (*get_time)();
          MPI_Send(&t_mid, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD);
        }
      MPI_Recv(t_master, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(t_slave, 1, MPI_DOUBLE, dest, tag_sync, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
}

/* ** method: MPI sync clocks ****************************** */

/* use the mpi_sync_clocks (interpolated CLOCK_MONOTONIC_RAW)
 */

static mpi_sync_clocks_t p_mpi_sync_clocks = NULL;

static void sync_clocks_init(void)
{
  p_mpi_sync_clocks = mpi_sync_clocks_init(MPI_COMM_WORLD);
}

static void sync_clocks_finalize(void)
{
  /* final sync */
  mpi_sync_clocks_synchronize(p_mpi_sync_clocks);
}

static double sync_clocks_get_time_usec(void)
{
  const double t = mpi_sync_clocks_get_time_usec(p_mpi_sync_clocks);
  return t;
}

static double sync_clocks_translate_time(double t_slave, int n)
{
  return mpi_sync_clocks_remote_to_global(p_mpi_sync_clocks, n, t_slave);
}

static struct nm_clock_method_s sync_clocks_method =
  {
   .name           = "mpi_sync_clocks",
   .init           = &sync_clocks_init,
   .get_time_usec  = &sync_clocks_get_time_usec,
   .finalize       = &sync_clocks_finalize,
   .translate_time = &sync_clocks_translate_time
  };


/* ** method: offset clock ********************************* */

/* apply an offset on the CLOCK_MONOTONIC clock. Rely on NTP
 * to fix the skew.
 */

static struct
{
  struct timespec orig; /**< time origin for local time */
  double*offsets;       /**< offset of nodes with node #0 (allocated only on node #0) */
} offset_clock;

static void offset_clock_init(void);
static double offset_clock_get_time_usec(void);
static double offset_clock_translate_time(double t, int n);

static void offset_clock_init(void)
{
  offset_clock.offsets = NULL;
  clock_gettime(CLOCK_MONOTONIC, &offset_clock.orig);
  if(rank == 0)
    {
      offset_clock.offsets = malloc(sizeof(double) * worldsize);
      int i;
      for(i = 0; i < worldsize; i++)
        {
          double t_master = -1.0, t_slave = -1.0;
          generic_sync(i, &offset_clock_get_time_usec, &t_master, &t_slave);
          offset_clock.offsets[i] = t_master - t_slave;
        }
    }
  else
    {
      double t_master = -1.0, t_slave = -1.0;
      generic_sync(0, &offset_clock_get_time_usec, &t_master, &t_slave);
    }
}

static double offset_clock_get_time_usec(void)
{
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  struct timespec diff;
  if(t.tv_nsec > offset_clock.orig.tv_nsec)
    {
      diff.tv_sec  = t.tv_sec - offset_clock.orig.tv_sec;
      diff.tv_nsec = t.tv_nsec - offset_clock.orig.tv_nsec;
    }
  else
    {
      diff.tv_sec  = t.tv_sec - offset_clock.orig.tv_sec - 1;
      diff.tv_nsec = t.tv_nsec - offset_clock.orig.tv_nsec + 1000000000L;
    }
  double delay = 1000000.0 * diff.tv_sec + diff.tv_nsec / 1000.0;
  return delay;
}

static double offset_clock_translate_time(double t_slave, int n)
{
  return t_slave + offset_clock.offsets[n];
}

static struct nm_clock_method_s offset_clock_method =
  {
   .name           = "offset_clock",
   .init           = &offset_clock_init,
   .get_time_usec  = &offset_clock_get_time_usec,
   .finalize       = NULL,
   .translate_time = &offset_clock_translate_time
  };


/* ** method: extrapolate clock **************************** */

/* apply an offset and compute the skew through an init-time
 * calibration. Use CLOCK_MONOTONIC_RAW so as not to get
 * disturbed by NTP corrections.
 */

static struct
{
  struct timespec orig; /**< local origin */
  struct extrapolate_clock_node_s
  {
    double t_orig; /**< synchronization origin, to compute skew */
    double offset; /**< t_slave + offset = t_master */
    double skew;   /**< t_slave * skew = t_master */
  }*nodes;
} extrapolate_clock;

static void extrapolate_clock_init(void);
static double extrapolate_clock_get_time_usec(void);
static double extrapolate_clock_translate_time(double t, int n);

static void extrapolate_clock_init(void)
{
  extrapolate_clock.nodes = NULL;
  clock_gettime(CLOCK_MONOTONIC_RAW, &extrapolate_clock.orig);
  if(rank == 0)
    {
      extrapolate_clock.nodes = malloc(sizeof(struct extrapolate_clock_node_s) * worldsize);
      int i;
      for(i = 0; i < worldsize; i++)
        {
          double t_master = -1.0, t_slave = -1.0;
          generic_sync(i, &extrapolate_clock_get_time_usec, &t_master, &t_slave);
          extrapolate_clock.nodes[i].offset = t_master - t_slave;
          extrapolate_clock.nodes[i].t_orig = t_master;
        }
      sleep(EXTRAPOLATE_CLOCK_INIT_TIME);
      for(i = 0; i < worldsize; i++)
        {
          double t_master = -1.0, t_slave = -1.0;
          generic_sync(i, &extrapolate_clock_get_time_usec, &t_master, &t_slave);
          const double t_diff_master = t_master - extrapolate_clock.nodes[i].t_orig;
          const double t_diff_slave  = t_slave  - (extrapolate_clock.nodes[i].t_orig - extrapolate_clock.nodes[i].offset);
          extrapolate_clock.nodes[i].skew = t_diff_master / t_diff_slave;
        }
    }
  else
    {
      double t_master = -1.0, t_slave = -1.0;
      generic_sync(0, &extrapolate_clock_get_time_usec, &t_master, &t_slave);
      generic_sync(0, &extrapolate_clock_get_time_usec, &t_master, &t_slave);
    }
}

static double extrapolate_clock_get_time_usec(void)
{
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t);
  struct timespec diff;
  if(t.tv_nsec > extrapolate_clock.orig.tv_nsec)
    {
      diff.tv_sec  = t.tv_sec - extrapolate_clock.orig.tv_sec;
      diff.tv_nsec = t.tv_nsec - extrapolate_clock.orig.tv_nsec;
    }
  else
    {
      diff.tv_sec  = t.tv_sec - extrapolate_clock.orig.tv_sec - 1;
      diff.tv_nsec = t.tv_nsec - extrapolate_clock.orig.tv_nsec + 1000000000L;
    }
  double delay = 1000000.0 * diff.tv_sec + diff.tv_nsec / 1000.0;
  return delay;
}

static double extrapolate_clock_translate_time(double t_slave, int n)
{
  return t_slave * extrapolate_clock.nodes[n].skew + extrapolate_clock.nodes[n].offset;
}

static struct nm_clock_method_s extrapolate_clock_method =
  {
   .name           = "extrapolate_clock",
   .init           = &extrapolate_clock_init,
   .get_time_usec  = &extrapolate_clock_get_time_usec,
   .finalize       = NULL,
   .translate_time = &extrapolate_clock_translate_time
  };

/* ** method: offset raw *********************************** */

/* apply an offset on the CLOCK_MONOTONIC_RAW, without fixing the skew.
 */

static struct
{
  struct timespec orig; /**< time origin for local time */
  double*offsets;       /**< offset of nodes with node #0 (allocated only on node #0) */
} offsetraw_clock;

static void offsetraw_clock_init(void);
static double offsetraw_clock_get_time_usec(void);
static double offsetraw_clock_translate_time(double t, int n);

static void offsetraw_clock_init(void)
{
  offsetraw_clock.offsets = NULL;
  clock_gettime(CLOCK_MONOTONIC_RAW, &offsetraw_clock.orig);
  if(rank == 0)
    {
      offsetraw_clock.offsets = malloc(sizeof(double) * worldsize);
      int i;
      for(i = 0; i < worldsize; i++)
        {
          double t_master = -1.0, t_slave = -1.0;
          generic_sync(i, &offsetraw_clock_get_time_usec, &t_master, &t_slave);
          offsetraw_clock.offsets[i] = t_master - t_slave;
        }
    }
  else
    {
      double t_master = -1.0, t_slave = -1.0;
      generic_sync(0, &offsetraw_clock_get_time_usec, &t_master, &t_slave);
    }
}

static double offsetraw_clock_get_time_usec(void)
{
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t);
  struct timespec diff;
  if(t.tv_nsec > offsetraw_clock.orig.tv_nsec)
    {
      diff.tv_sec  = t.tv_sec - offsetraw_clock.orig.tv_sec;
      diff.tv_nsec = t.tv_nsec - offsetraw_clock.orig.tv_nsec;
    }
  else
    {
      diff.tv_sec  = t.tv_sec - offsetraw_clock.orig.tv_sec - 1;
      diff.tv_nsec = t.tv_nsec - offsetraw_clock.orig.tv_nsec + 1000000000L;
    }
  double delay = 1000000.0 * diff.tv_sec + diff.tv_nsec / 1000.0;
  return delay;
}

static double offsetraw_clock_translate_time(double t_slave, int n)
{
  return t_slave + offsetraw_clock.offsets[n];
}

static struct nm_clock_method_s offsetraw_clock_method =
  {
   .name           = "offsetraw_clock",
   .init           = &offsetraw_clock_init,
   .get_time_usec  = &offsetraw_clock_get_time_usec,
   .finalize       = NULL,
   .translate_time = &offsetraw_clock_translate_time
  };


/* ********************************************************* */

void do_compute(int seconds, int omp)
{
  /* compute deadline */
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  t.tv_sec += seconds;

  /* matrix multiply */
#define MATRIX_SIZE 256
  static double matrix1[MATRIX_SIZE][MATRIX_SIZE],
    matrix2[MATRIX_SIZE][MATRIX_SIZE],
    result[MATRIX_SIZE][MATRIX_SIZE];
  static int init_done = 0;
  if(!init_done)
    {
      init_done = 1;
      int i, j;
      for(i = 0; i < MATRIX_SIZE; i++)
        {
          for(j = 0; j < MATRIX_SIZE; j++)
            {
              matrix1[i][j] = 1.0;
              matrix1[i][j] = 2.0;
            }
        }
    }
  for(;;)
    {
      int i, j, k;
      if(omp)
        {
#pragma omp parallel for shared(matrix1, matrix2, result) private(i, j, k)
          for(i = 0; i < MATRIX_SIZE; i++)
            {
              for(j = 0; j < MATRIX_SIZE; j++)
                {
                  result[i][j] = 0;
                  for(k = 0; k < MATRIX_SIZE; k++)
                    {
                      result[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
        }
      else
        {
          for(i = 0; i < MATRIX_SIZE; i++)
            {
              for(j = 0; j < MATRIX_SIZE; j++)
                {
                  result[i][j] = 0;
                  for(k = 0; k < MATRIX_SIZE; k++)
                    {
                      result[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
        }
      struct timespec t2;
      clock_gettime(CLOCK_MONOTONIC, &t2);
      if((t2.tv_sec > t.tv_sec) || ((t2.tv_sec == t.tv_sec) && (t2.tv_nsec > t.tv_nsec)))
        {
          return;
        }
    }
}

/* ** monitoring ******************************************* */

struct monitor_s
{
  const char*name;
  int n_values;    /**< number of values per round */
  char**paths;
  int*p_values;    /**< array of values: n_values * rounds */
};

static struct monitor_s*monitors = NULL;
static int n_monitors = 0;

static inline struct monitor_s*monitor_alloc(void)
{
  n_monitors++;
  monitors = realloc(monitors, sizeof(struct monitor_s) * n_monitors);
  return &monitors[n_monitors - 1];
}

static void temperature_try_device(struct monitor_s*p_monitor, const char*path)
{
  struct stat statbuf;
  int rc = stat(path, &statbuf);
  if(rc == 0)
    {
      fprintf(stderr, "# found cpu temp %s\n", path);
      p_monitor->paths = realloc(p_monitor->paths, sizeof(char*) * (p_monitor->n_values + 1));
      p_monitor->paths[p_monitor->n_values] = strdup(path);
      p_monitor->n_values++;
    }
}

static void temperature_detect_intel(struct monitor_s*p_monitor)
{
  // Intel: /sys/devices/platform/coretemp.*/hwmon/hwmon*/temp*_input
  int c;
  for(c = 0; c < 16; c++)
    {
      char path[100];
      int h;
      for(h = 0; h < 16 ; h++)
        {
          sprintf(path, "/sys/devices/platform/coretemp.%d/hwmon/hwmon%d", c, h);
          struct stat statbuf;
          int rc = stat(path, &statbuf);
          if(rc == 0)
            {
              break;
            }
        }
      int t;
      for(t = 0; t < 64; t++)
        {
          sprintf(path, "/sys/devices/platform/coretemp.%d/hwmon/hwmon%d/temp%d_input", c, h, t);
          temperature_try_device(p_monitor, path);
        }
    }
}

static void temperature_detect_amd(struct monitor_s*p_monitor)
{
  // AMD: /sys/class/hwmon/hwmon*/temp*_input
  char path[100];
  int h;
  for(h = 0; h < 16 ; h++)
    {
      sprintf(path, "/sys/class/hwmon/hwmon%d", h);
      struct stat statbuf;
      int rc = stat(path, &statbuf);
      if(rc == 0)
        {
          break;
        }
    }
  int t;
  for(t = 0; t < 64; t++)
    {
      sprintf(path, "/sys/class/hwmon/hwmon%d/temp%d_input", h, t);
      temperature_try_device(p_monitor, path);
    }
}

static void temperature_init(struct monitor_s*p_monitor, int rounds)
{
  p_monitor->name = "temperature";
  p_monitor->n_values = 0;
  p_monitor->paths = NULL;

  temperature_detect_intel(p_monitor);
  temperature_detect_amd(p_monitor);

  p_monitor->p_values = malloc(sizeof(int) * rounds * p_monitor->n_values);
}

static void freqs_init(struct monitor_s*p_monitor, int rounds)
{
  // /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq
  p_monitor->name = "freqs";
  p_monitor->n_values = 0;
  p_monitor->paths = NULL;
  int n_cpu = 0;
  for(;;)
    {
      char path[100];
      sprintf(path, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", n_cpu);
      struct stat statbuf;
      int rc = stat(path, &statbuf);
      if(rc == 0)
        {
          fprintf(stderr, "# found cpu freq %s\n", path);
          p_monitor->paths = realloc(p_monitor->paths, sizeof(char*) * (p_monitor->n_values + 1));
          p_monitor->paths[p_monitor->n_values] = strdup(path);
          p_monitor->n_values++;
          n_cpu++;
        }
      else
        {
          break;
        }
    }
  p_monitor->p_values = malloc(sizeof(int) * rounds * p_monitor->n_values);
}

static void monitor_get_values(struct monitor_s*p_monitor, int round)
{
  int*p_base_values = p_monitor->p_values + round * p_monitor->n_values;
  int i;
  for(i = 0; i < p_monitor->n_values; i++)
    {
      int fd = open(p_monitor->paths[i], 0);
      assert(fd > 0);
      char buf[100];
      int rc = read(fd, buf, 100);
      if(rc <= 0)
        {
          fprintf(stderr, "# cannot read value for monitor %s\n", p_monitor->paths[i]);
          abort();
        }
      int value = atoi(buf);
      close(fd);
      p_base_values[i] = value;
    }
}

static void monitor_show_values(const struct monitor_s*p_monitor, int rounds, int from, int*p_values)
{
  int round;
  for(round = 0; round < rounds; round++)
    {
      printf("%8d \t %d %s \t ", round, from, p_monitor->name);
      int i;
      for(i = 0; i < p_monitor->n_values; i++)
        {
          printf("%8d \t", p_values[i + round * p_monitor->n_values]);
        }
      printf("\n");
    }
}

static void monitor_finalize_values(const struct monitor_s*p_monitor, int rounds)
{
  if(rank == 0)
    {
      printf("# results for monitor: '%s'; %d values per entry\n", p_monitor->name, p_monitor->n_values);
      printf("# round \t dest \t monitor \t values\n");
      monitor_show_values(p_monitor, rounds, 0, p_monitor->p_values);
      int*p_values = malloc(sizeof(int) * rounds * p_monitor->n_values);
      int from;
      for(from = 1; from < worldsize; from++)
        {
          MPI_Recv(p_values, rounds * p_monitor->n_values, MPI_INT, from, /* tag */ 0x01, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          monitor_show_values(p_monitor, rounds, from, p_values);
        }
      free(p_values);
    }
  else
    {
      MPI_Send(p_monitor->p_values, rounds * p_monitor->n_values, MPI_INT, 0, /* tag */ 0x01, MPI_COMM_WORLD);
    }
}

/* ********************************************************* */

struct nm_clock_entry_s
{
  double t_master;
  double t_slave;
};

struct nm_clock_drift_s
{
  struct nm_clock_entry_s*p_entries; /**< array indexed by round number */
};

static int rounds = ROUNDS_DEFAULT;
static int rounds_time_seconds = ROUNDS_TIME_SECONDS;

static void usage(char**argv)
{
  fprintf(stderr, "usage: %s [--rounds <rounds>] [--compute] [--freqs] [--temperature]\n", argv[0]);
}

int main(int argc, char**argv)
{
  int compute = 0, temperature = 0, freqs = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &worldsize);
  if(worldsize < 2)
    {
      fprintf(stderr, "need at least 2 nodes!\n");
      abort();
    }

  int i;
  for(i = 1; i < argc; i ++)
    {
      if(strcmp(argv[i], "--rounds") == 0)
        {
          if(i + 1 >= argc)
            {
              fprintf(stderr, "--rounds needs a parameter\n");
              usage(argv);
              exit(1);
            }
          const char*s_rounds = argv[i + 1];
          rounds = atoi(s_rounds);
          if(rounds <= 1)
            {
              usage(argv);
              exit(1);
            }
          i += 1;
        }
      else if(strcmp(argv[i], "--compute") == 0)
        {
          fprintf(stderr, "# do computation on slave nodes\n");
          compute = 1;
        }
      else if(strcmp(argv[i], "--temperature") == 0)
        {
          temperature = 1;
        }
      else if(strcmp(argv[i], "--freqs") == 0)
        {
          freqs = 1;
        }
      else if(strcmp(argv[i], "--help") == 0)
        {
          usage(argv);
          exit(0);
        }
      else
        {
          fprintf(stderr, "unknown arg: %s\n", argv[i]);
          usage(argv);
          exit(1);
        }
    }

  if(temperature)
    {
      temperature_init(monitor_alloc(), rounds);
    }
  if(freqs)
    {
      freqs_init(monitor_alloc(), rounds);
    }

  char h[256];
  gethostname(h, 256);
  printf("# rank #%d running on node %s\n", rank, h);
  if(rank == 0)
    {
      printf("# rounds = %d\n", rounds);
      printf("# compute = %d\n", compute);
      printf("# temperature = %d\n", temperature);
      printf("# freqs = %d\n", freqs);
      printf("# round time = %d seconds\n", rounds_time_seconds);
      printf("# total benchmark duration: %d seconds (%d minutes)\n",
             rounds * rounds_time_seconds, (rounds * rounds_time_seconds) / 60);
    }
  int fd = open("/sys/devices/system/clocksource/clocksource0/current_clocksource", O_RDONLY);
  if(fd > 0)
    {
      char buf[256];
      int rc = read(fd, buf, 256);
      buf[255] = '\0';
      if(rc > 0)
        {
          printf("# node #%d: clock source = %s\n", rank, buf);
        }
      close(fd);
    }

  static struct nm_clock_method_s*methods[] =
    {
      &sync_clocks_method,
      &offset_clock_method,
      &extrapolate_clock_method,
      &offsetraw_clock_method
    };
  static const int n_methods = sizeof(methods) / sizeof(struct nm_clock_method_s*);

  int m;
  for(m = 0; m < n_methods; m++)
    {
      fprintf(stderr, "# init method '%s'...\n", methods[m]->name);
      methods[m]->init();
      methods[m]->p_drift = malloc(worldsize * sizeof(struct nm_clock_drift_s));
      int i;
      for(i = 0; i < worldsize; i++)
        {
          methods[m]->p_drift[i].p_entries = malloc(rounds * sizeof(struct nm_clock_entry_s));
        }
      fprintf(stderr, "# method '%s' ok.\n", methods[m]->name);
    }

  int round;
  for(round = 0; round < rounds; round++)
    {
      fprintf(stderr, "# ## round = %d / %d; %d seconds\n", round, rounds, rounds_time_seconds);
      int m;
      for(m = 0; m < n_monitors; m++)
        {
          monitor_get_values(&monitors[m], round);
        }
      MPI_Barrier(MPI_COMM_WORLD);
      if(rank == 0)
        {
          int dest;
          for(dest = 1; dest < worldsize; dest++)
            {
              /* pingpong to get a common event on both master time and slave time */
              double t_master, t_slave;
              for(m = 0; m < n_methods; m++)
                {
                  generic_sync(dest, methods[m]->get_time_usec, &t_master, &t_slave);
                  methods[m]->p_drift[dest].p_entries[round].t_master = t_master;
                  methods[m]->p_drift[dest].p_entries[round].t_slave = t_slave;
                }
            }
        }
      else
        {
          double t_master, t_slave;
          for(m = 0; m < n_methods; m++)
            {
              generic_sync(0, methods[m]->get_time_usec, &t_master, &t_slave);
            }
        }
      if(compute && (rank != 0) && (round > (rounds / 2)))
        {
          do_compute(rounds_time_seconds, (round > (rounds * 3 / 4)));
        }
      else
        {
          sleep(rounds_time_seconds);
        }
    }

  double drift_total[n_methods];

  /* final sync */
  for(m = 0; m < n_methods; m++)
    {
      drift_total[m] = 0.0;
      if(methods[m]->finalize != NULL)
        {
          (*methods[m]->finalize)();
        }

      if(rank == 0)
        {
          printf("# results for method: '%s'\n", methods[m]->name);
          printf("# round;\t dest;\t t_master;\t t_slave;\t drift to global;\t drift to local \t method\n");
          for(round = 0; round < rounds; round++)
            {
              int i;
              for(i = 1; i < worldsize; i++)
                {
                  const double t_master = methods[m]->p_drift[i].p_entries[round].t_master;
                  const double t_slave_local = methods[m]->p_drift[i].p_entries[round].t_slave;
                  const double t_slave = (*methods[m]->translate_time)(t_slave_local, i);
                  printf("%8d \t %d \t %.3lf \t %.3lf \t %16.3lf \t %16.3lf \t %-16s\n",
                         round, i, t_master, t_slave, t_slave - t_master, t_slave_local - t_master,
                         methods[m]->name);
                  fflush(stdout);
                  drift_total[m] += fabs(t_slave - t_master);
                }
            }
        }
    }
  for(m = 0; m < n_monitors; m++)
    {
      monitor_finalize_values(&monitors[m], rounds);
    }
  if(rank == 0)
    {
      for(m = 0; m < n_methods; m++)
        {
          printf("# average drift for method '%s' = %.3lf usec.\n", methods[m]->name, drift_total[m] / (rounds * (worldsize - 1)));
          fflush(stdout);
        }
    }

  MPI_Finalize();

  for(m = 0; m < n_methods; m++)
    {
      for(i = 0; i < worldsize; i++)
        {
          free(methods[m]->p_drift[i].p_entries);
        }
      free(methods[m]->p_drift);
    }

  return 0;
}
