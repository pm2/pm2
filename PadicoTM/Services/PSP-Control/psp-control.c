/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PSP over control channel
 * @ingroup Control
 */

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/PadicoControl.h>
#include <Padico/Module.h>

static int psp_control_module_init(void);
static void psp_control_module_finalize(void);

PADICO_MODULE_DECLARE(PSP_Control, psp_control_module_init, NULL, psp_control_module_finalize,
                      "Topology", "PSP", "NetSelector", "PadicoControl");


PUK_LIST_TYPE(psp_control_packet,
              uint32_t num;
              struct iovec*buffer;
              void*header;
              uint32_t tag;
              );

struct psp_control_stream_s
{
  padico_topo_node_t node;
  uint32_t send;
  uint32_t recv;
  psp_control_packet_list_t packets;
};

typedef struct psp_control_stream_s*psp_control_stream_t;
PUK_VECT_TYPE(psp_control_stream, psp_control_stream_t);

/** PSP/Control static status
 */
static struct
{
  struct padico_psp_directory_s slots;
  marcel_mutex_t lock;
  psp_control_stream_vect_t streams;
} psp_control;


/* *********************************************************
 * *** PadicoSimplePackets over control channel
 *
 * @defgroup PSP_Control component: PSP/Control- SimplePackets over the control channel
 * @ingroup PadicoComponent
 */

static void*psp_control_instantiate(puk_instance_t ai, puk_context_t context);
static void psp_control_destroy(void*_instance);

static void psp_control_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                             padico_psp_handler_t handler, void*key);
static padico_psp_connection_t psp_control_new_message(void*_instance, padico_topo_node_t node, void**sbuf);
static void psp_control_pack(void*_instance, padico_psp_connection_t conn, const char*bytes, size_t size);
static void psp_control_end_message(void*_instance, padico_psp_connection_t conn);

/** instanciation facet for PSP/Control
 * @ingroup PSP_Control
 */
static const struct puk_component_driver_s psp_control_component_driver =
  {
    .instantiate = &psp_control_instantiate,
    .destroy     = &psp_control_destroy
  };
/** SimplePackets facet for PSP/Control
 * @ingroup PSP_Control
 */
static const struct padico_psp_driver_s psp_control_driver =
  {
    .init        = &psp_control_init,
    .connect     = NULL,
    .listen      = NULL,
    .new_message = &psp_control_new_message,
    .pack        = &psp_control_pack,
    .end_message = &psp_control_end_message
  };

/** a PSP/Control instance
 * @ingroup PSP_Control
 */
struct psp_control_instance_s
{
  marcel_mutex_t lock;
  padico_psp_slot_t slot;
  void*sbuf;
};

struct psp_control_connection_s
{
  padico_topo_node_t dest;
  padico_control_msg_t msg;
  int header_sent;
};

static psp_control_stream_t psp_control_getstream(padico_topo_node_t node)
{
  int i;
  for(i = 0; i < psp_control_stream_vect_size(psp_control.streams); i++)
    {
      psp_control_stream_t s = psp_control_stream_vect_at(psp_control.streams, i);
      if(s->node == node)
        {
          return s;
        }
    }
  return NULL;
}

static psp_control_packet_t psp_control_getpacket(psp_control_packet_list_t packets,  uint32_t num)
{
  psp_control_packet_itor_t i = psp_control_packet_list_begin(packets);
  while(i != psp_control_packet_list_end(packets))
    {
      if(i->num == num)
        {
          psp_control_packet_list_remove(packets, i);
          return i;
        }
      i  = psp_control_packet_list_next(i);
    }
  return NULL;
}

/* ********************************************************* */
/* *** PadicoComponent */

static void*psp_control_instantiate(puk_instance_t ai, puk_context_t context)
{
  struct psp_control_instance_s*instance = padico_malloc(sizeof(struct psp_control_instance_s));
  marcel_mutex_init(&instance->lock, NULL);
  instance->slot = NULL;
  instance->sbuf = NULL;
  return instance;
}
static void psp_control_destroy(void*_instance)
{
  struct psp_control_instance_s*instance = _instance;
  if(instance->sbuf)
    padico_free(instance->sbuf);
  if(instance->slot)
    {
      padico_psp_slot_remove(&psp_control.slots, instance->slot);
    }
  padico_free(instance);
}

/* ********************************************************* */
/* *** PadicoSimplePackets over PadicoControl */

static void psp_control_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                             padico_psp_handler_t handler, void*key)
{
  struct psp_control_instance_s*instance = _instance;
  instance->slot = padico_psp_slot_insert(&psp_control.slots, handler, key, tag, *h_size);
  instance->sbuf = padico_malloc(*h_size);
}

static padico_psp_connection_t
psp_control_new_message(void*_instance, padico_topo_node_t node, void**_sbuf)
{
  struct psp_control_instance_s*instance = _instance;
  struct psp_control_connection_s*conn = padico_malloc(sizeof(struct psp_control_connection_s));
  marcel_mutex_lock(&instance->lock);
  *_sbuf = instance->sbuf;
  conn->dest = node;
  conn->msg = padico_control_msg_new();
  marcel_mutex_lock(&psp_control.lock);
  psp_control_stream_t s = psp_control_getstream(node);
  int num;
  if(!s)
    {
      s = padico_malloc(sizeof(struct psp_control_stream_s));
      s->node = node;
      s->send = 0;
      s->recv = 0;
      s->packets = psp_control_packet_list_new();
      psp_control_stream_vect_push_back(psp_control.streams, s);
    }
  assert(node != NULL);
  num = s->send;
  s->send++;
  marcel_mutex_unlock(&psp_control.lock);
  padico_control_msg_add_cmd(conn->msg, "<PSP_Control:packet from=\"%s\" tag=\"%d\" num=\"%d\">",
                             padico_topo_node_getname(padico_topo_getlocalnode()), instance->slot->tag, num);
  conn->header_sent = 0;
  return conn;
}

static void psp_control_pack(void*_instance, padico_psp_connection_t _conn, const char*bytes, size_t b_size)
{
  struct psp_control_instance_s*instance = _instance;
  struct psp_control_connection_s*conn = _conn;
  if(!conn->header_sent)
    {
      padico_control_msg_add_part(conn->msg, instance->sbuf, instance->slot->h_size);
      conn->header_sent=1;
      marcel_mutex_unlock(&instance->lock);
    }
  padico_control_msg_add_part(conn->msg, bytes, b_size);
}

static void psp_control_end_message(void*_instance, padico_psp_connection_t _conn)
{
  struct psp_control_connection_s*conn = _conn;
  if(!conn->header_sent)
    {
      psp_control_pack(_instance, _conn, NULL, 0);
    }
  padico_control_msg_add_cmd(conn->msg, "</PSP_Control:packet>");
  padico_control_send_msg(conn->dest, conn->msg);
  padico_free(conn);
}

static void psp_control_pump(void*_token, void*bytes, size_t size)
{
  struct iovec*token = _token;
  assert(size == token->iov_len);
  memcpy(bytes, token->iov_base, size);
}

static void control_packet_handler(puk_parse_entity_t e)
{
  const char*t_from   = puk_parse_getattr(e, "from");
  const char*t_tag    = puk_parse_getattr(e, "tag");
  const char*t_num    = puk_parse_getattr(e, "num");
  const padico_topo_node_t from = padico_topo_getnodebyname(t_from);
  const uint32_t tag = atoi(t_tag);
  const uint32_t num = atoi(t_num);
  marcel_mutex_lock(&psp_control.lock);
  psp_control_stream_t s = psp_control_getstream(from);
  if(!s)
    {
      s = padico_malloc(sizeof(struct psp_control_stream_s));
      s->send = 0;
      s->recv = 0;
      s->node = from;
      s->packets = psp_control_packet_list_new();
      psp_control_stream_vect_push_back(psp_control.streams, s);
    }
  marcel_mutex_unlock(&psp_control.lock);

  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  assert(puk_parse_entity_vect_size(c) >= 2);
  struct iovec part_header = puk_parse_getpart(puk_parse_entity_vect_at(c, 0));
  void*header;
  padico_psp_slot_t slot;
  struct iovec part_buffer = puk_parse_getpart(puk_parse_entity_vect_at(c, 1));
  if(s->recv != num)
    {
      header = padico_malloc(part_header.iov_len);
      memcpy(header, part_header.iov_base, part_header.iov_len);
      struct iovec*token = padico_malloc(sizeof(struct iovec));
      token->iov_base = padico_malloc(part_buffer.iov_len);
      memcpy(token->iov_base, part_buffer.iov_base, part_buffer.iov_len);
      token->iov_len = part_buffer.iov_len;
      psp_control_packet_t p = psp_control_packet_new();
      p->header = header;
      p->buffer = token;
      p->num = num;
      p->tag = tag;
      psp_control_packet_list_push_back(s->packets, p);
    }
  else
    {
      slot = padico_psp_slot_lookup(&psp_control.slots, tag);
      header = part_header.iov_base;
      (*slot->handler)(header, from, slot->key, &psp_control_pump, (void*)&part_buffer);
      while(1)
        {
          s->recv++;
          psp_control_packet_t p = psp_control_getpacket(s->packets, s->recv);
          if(p)
            {
              slot = padico_psp_slot_lookup(&psp_control.slots, p->tag);
              (*slot->handler)(p->header, from, slot->key, &psp_control_pump, (void*)p->buffer);
              padico_free(p->buffer->iov_base);
              padico_free(p->buffer);
              padico_free(p->header);
              padico_free(p);
            }
          else
            {
              break;
            }
        }
    }
}

/* ********************************************************* */

static void psp_control_event_listener(void*_event)
{
  const struct padico_control_event_s*event = _event;
  if(event->kind == PADICO_CONTROL_EVENT_DELETE_NODE )
    {
      const padico_topo_node_t node = event->NODE.node;
      padico_print("delete node %s\n", padico_topo_node_getname(node));
      psp_control_stream_vect_itor_t i;
      puk_vect_foreach(i, psp_control_stream, psp_control.streams)
        {
          if((*i)->node == node)
            {
              psp_control_packet_list_delete((*i)->packets);
              psp_control_stream_vect_erase(psp_control.streams, i);
              break;
            }
        }
    }
}

/* ********************************************************* */

static int psp_control_module_init(void)
{
  padico_psp_directory_init(&psp_control.slots);
  psp_control.streams = psp_control_stream_vect_new();
  marcel_mutex_init(&psp_control.lock, NULL);
  padico_control_event_subscribe(&psp_control_event_listener);
  puk_component_declare("PSP_Control",
                        puk_component_provides("PadicoComponent", "component", &psp_control_component_driver),
                        puk_component_provides("PadicoSimplePackets", "psp", &psp_control_driver));

  puk_xml_add_action((struct puk_tag_action_s){
    .xml_tag        = "PSP_Control:packet",
    .start_handler  = NULL,
    .end_handler    = &control_packet_handler,
    .required_level = PUK_TRUST_CONTROL
  });

  return 0;
}

static void psp_control_module_finalize(void)
{
  padico_control_event_unsubscribe(&psp_control_event_listener);
  int i;
  for(i = 0; i < psp_control_stream_vect_size(psp_control.streams); i++)
    {
      psp_control_stream_t s = psp_control_stream_vect_at(psp_control.streams, i);
      psp_control_packet_list_delete(s->packets);
      padico_free(s);
    }
  psp_control_stream_vect_delete(psp_control.streams);
}
