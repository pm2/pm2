/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal definitions for VRP module
 * @ingroup VRP
 */

/** @defgroup VRP Service: VRP -- Variable Reliability Protocol
 * @author Alexandre Denis
 */

#include "VRP.h"


#ifdef PADICO

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>

#ifndef PADICO_VRP_DEFINED
PADICO_MODULE_HOOK(VRP);
#endif

#define vrp_enter()           marcel_mutex_lock(&vrp_mutex);
#define vrp_exit()            marcel_mutex_unlock(&vrp_mutex);
#define vrp_mutex_t           marcel_mutex_t
#define vrp_mutex_init(MUTEX) marcel_mutex_init(&(MUTEX), (marcel_mutexattr_t*)NULL)
#define vrp_malloc            padico_malloc
#define vrp_free              padico_free
#define vrp_print             padico_print
#define vrp_trace             padico_trace
#define vrp_show              padico_out
#define vrp_warning           padico_warning
#define vrp_fatal             padico_fatal
#define vrp_log_init()        fopen("/tmp/VRP-timeout.log", "w")
#define vrp_log               fprintf
#define vrp_log_end()         fclose(vrp_log_fd)
extern FILE*vrp_log_fd;

#else /* PADICO */

#error VRP without Padico not supported yet!

#endif /* PADICO */


extern vrp_mutex_t vrp_mutex;

/* safe network I/O
 */
static inline ssize_t vrp_sendto(int s, const void*msg, size_t len, int flags, const struct sockaddr*to, int tolen)
{
  int rc;
  puk_spinlock_acquire();
  rc = sendto(s, msg, len, flags, to, tolen);
  puk_spinlock_release();
  return rc;
}

static inline ssize_t vrp_recvfrom(int s, void*buf, size_t len, int flags, struct sockaddr*from, int*fromlen)
{
  int rc;
  puk_spinlock_acquire();
  rc = recvfrom(s, buf, len, flags, from, fromlen);
  puk_spinlock_release();
  return rc;
}

/*
 * Tables to map from fd to outgoings, incomings, and listeners.
 */
typedef enum
{
  FD_TABLE_TYPE_NONE,
  FD_TABLE_TYPE_OUTGOING,
  FD_TABLE_TYPE_INCOMING
} fd_table_type_t;

typedef struct fd_table_s
{
  fd_table_type_t type;
  void *          value;
} fd_table_t;

struct fd_finder_t
{
  int         fd_tablesize;
  fd_table_t  *fd_table;
};
extern struct fd_finder_t fd_finder;


/* ***********************************************************
 * **      Global parameters
 * ***********************************************************
 */

#define VRP_TIMEOUT_DEFAULT        100000   /* in usec */
#define VRP_TIMEOUT_MAX            2000000  /* in usec */
#define VRP_TIMEOUT_RTT_SMOOTH_FACTOR   (VRP_OUTGOING_WINDOW_SIZE)      /**< width of the window used for the computation of retransmission timeout */
#define VRP_TIMEOUT_DEVIATION_SMOOTH_FACTOR (VRP_OUTGOING_WINDOW_SIZE) /**< width of the window used for the computation of retransmission timeout */
#define VRP_TIMEOUT_FEEDBACK_FACTOR  1.8     /**< RTT feedback in timeout computation */
#define VRP_TIMEOUT_DEVIATION_FACTOR 2      /**< deviation injection factor for timeout computation */

#define VRP_DEAD_TIMEOUT           15       /* in seconds */
#define VRP_FRAME_MAX              32000    /**< maximum frame number */
#define VRP_NO_FRAME              (VRP_FRAME_MAX + 1)
#define VRP_DEFAULT_LOSS_MAX       64     /* = 25 % lost  (255 = 100 % lost) */
#define VRP_DEFAULT_CONS_LOSS_MAX  0

#define VRP_HDR_SIZE               8

#define VRP_MAX_PACKET_SIZE        1460 /* size of a UDP payload over a single Ethernet frame */
#define VRP_DATA_PER_PACKET        (VRP_MAX_PACKET_SIZE - VRP_HDR_SIZE)

#define VRP_OUTGOING_WINDOW_SIZE_BYTES   (1024*64)
#define VRP_OUTGOING_WINDOW_SIZE  ((VRP_OUTGOING_WINDOW_SIZE_BYTES)/(VRP_MAX_PACKET_SIZE))

/* ** VRP_DEAD_TIMEOUT: time in second.
 *    If a connection has no activity for a longer time
 *    than this timeout, it is considered as being dead
 *    and will be freed without waiting for a "close handshake".
 * ** VRP_FRAME_MAX: maximum number for frames.
 *    The frames are numbered for identification. This is
 *    the greatest number they can have. It can be reached,
 *    simply loop with a modulo.
 * ** VRP_DEFAULT_LOSS_MAX: maximum loss rate allowed if
 *    nothing is specified by user. 0 = nothing can be lost,
 *    255 = everything can be lost.
 * ** VRP_DEFAULT_CONS_LOSS_MAX: maximum number of consecutive losses.
 * ** VRP_DATA_PER_PACKET: maximum amount of data in a packet
 * ** VRP_HDR_SIZE: size of the header once it is packed.
 *    depending of the architecture, VRP_HDR_SIZE != sizeof(struct vrp_hdr_s)
 * ** VRP_MAX_PACKET_SIZE: the maximum size of packets that are built.
 *    No problem with big packets on short connection,
 *    but no packets beyond 2 KB will cross the atlantic ocean...
 *    TODO: investigate how it can be adjusted dynamically
 *    (through a modification of VRP_DATA_PER_PACKET).
 */


/* *****************************************************************
 * **    packet definition
 * *****************************************************************
 */


/* packet format
 *
 * common header: 6 bytes
 *  - 1st bit: 1 = header follows
 *             0 = data follows
 *  - 1st & 2nd byte, bits 0-6: frame # (VRP_NO_FRAME if message is not related to a frame)
 *  - bytes 3-6: seq #
 *               from 0 to 2^32 - 1 inside a frame (data)
 *               from 0 to 2^32 - 1 outside a frame (if header bit is set ie. frame = VRP_NO_FRAME)
 *
 * Data packets are acknowledged
 * header packet are not acknowledged
 * if a header packet as a frame# != VRP_NO_FRAME, it becomes the current frame
 *
 */
enum vrp_tag_e
{
  VRP_TAG_ACK = 1,
  /**< ACK for a packet.
   * 4 bytes seq #
   * It acknowledges previous packets of the same frame too
   */
  VRP_TAG_NACK = 2,
  /**< negative ACK for a single packet.
   * 4 bytes: seq #
   */
  VRP_TAG_SINGLE_ACK = 3,
  /**< ACK for a single packet.
   * 4 bytes: seq #
   */
  VRP_TAG_FRAME_SET = 4,
  /**< set this frame as current for following frame parameters
   * 2 bytes: frame number
   */
  VRP_TAG_FRAME_CREATE = 5,
  /**< creates a new frame in the receiving side.
   * 2 bytes: frame number
   * 4 bytes: data size (in bytes)
   * 2 bytes: data per packet
   * notice: this frame becomes the active frame
   */
  VRP_TAG_FRAME_OK = 6,
  /**< frame successfully created
   * 2 bytes: frame number
   */
  VRP_TAG_FRAME_REFUSED = 7,
  /**< frame refused by receiving part
   * 2 bytes: frame number
   */
  VRP_TAG_FRAME_CLOSE = 8,
  /**< frame close: sender->receiver, close connection
   * 2 bytes: frame number
   */
  VRP_TAG_FRAME_MISSING_HDR = 9,
  /**< receiver has data with wrong frame number (not previously created)
   * 2 bytes: frame number
   */
  VRP_TAG_CONS_LOSS = 10,
  /**< set consecutive losses max for current frame
   * 1 byte: number of maximum consecutive losses
   */
  VRP_TAG_LOSS_RATE = 11,
  /**< set maximum for average loss rate for current frame
   * 1 byte: average loss rate (per window) 0 = no loss allowed, 255 = everything can be lost
   */
  VRP_TAG_CRITICAL_PACKET = 12,
  /**< set this packet as critical (must arrive) (seq # relative to current frame)
   * 4 bytes: packet seq number
   */
  VRP_TAG_CRITICAL_INTERVAL = 13,
  /**< same as above with a packet interval
   * 4 bytes: begining packet number
   * 4 bytes: end packet number
   * begining and end are inclusive
   */
  VRP_TAG_LAST = 14,
  /**< marks the end of the header */
  VRP_TAG_PING = 15,
  /**< followed by 8 bytes of data */
  VRP_TAG_PONG = 16
  /**< followed by 8 bytes of data */
};

/** unpacked header
 * @internal */
struct vrp_hdr_s
{
  int    is_hdr;  /**< boolean - true if extended header follows */
  int    frame;   /**< frame number */
  int    seq;     /**< sequence number in the frame */
};
typedef struct vrp_hdr_s vrp_hdr_t;


/* ********************************************************
 * **       Buffer
 * ********************************************************
 */

struct vrp_critical_cell_s
{
  int begin, end;
  struct vrp_critical_cell_s*next;
};

struct vrp_buffer_s
{
  int  size;
  char*data;
  struct vrp_critical_cell_s*critical;
  int max_loss_rate;
  int max_cons_loss;
};

void         vrp_pack_int32(char*packed, int n);
void         vrp_pack_int16(char*packed, int n);
void         vrp_pack_int8(char*packed, int n);
unsigned int vrp_unpack_int32(char*packed);
unsigned int vrp_unpack_int16(char*packed);
unsigned int vrp_unpack_int8(char*packed);
void         vrp_hdr_unpack(char *hdr_packed, struct vrp_hdr_s *hdr);
void         vrp_hdr_pack(struct vrp_hdr_s *hdr, char *hdr_packed);
void         vrp_hdr_pack_std(char*packed, int*size);
int          vrp_hdr_pack_tag(char*c, int*size, enum vrp_tag_e tag, ...);
