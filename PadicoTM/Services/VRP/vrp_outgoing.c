/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Sender-side part of VRP
 * @ingroup VRP
 */

#include "VRP-internals.h"


/* TODO: - determine a reasonable timeout - adaptive?
 *       - parameterize the window size
 *       - detect multi-packet frame-header
 */

/* notice: - we assume that VRP_OUTGOING_TIMEOUT < 1,000,000  (1 sec)
 *         - we assume VRP_OUTGOING_WINDOW_SIZE to be an exact power of 2
 */


struct vrp_out_timing_param_s
{
  long int timeout;            /**< timeout (in usec) */
  long int RTT;                /**< smoothed round-trip time (in usec) */
  long int average_deviation;  /**< average deviation of actual RTT against smoothed RTT (in usec) */
};

struct vrp_out_cell_s
{
  unsigned int       acked:   1;
  unsigned int       sent:    1;
  unsigned int       expired: 1;
  struct timeval     expire_date;
  struct timeval     expected;
  struct timeval     start_date;
  struct timeval     ack_time;
  struct vrp_out_timing_param_s timing; /**< timing params snapshot (at packet start) */
};

struct vrp_out_hdr_s
{
  char content[VRP_MAX_PACKET_SIZE];
  int  size; /* in bytes- actual size of the header */
  struct vrp_out_cell_s cell;
};

enum vrp_out_frame_state_e
{
  VRP_OUT_FRAME_NONE = 0,   /**< no frame in this slot */
  VRP_OUT_FRAME_INIT,       /**< sending frame header */
  VRP_OUT_FRAME_SEND,       /**< sending data */
  VRP_OUT_FRAME_REFUSED,    /**< frame refused by incoming */
  VRP_OUT_FRAME_ERROR,      /**< transmission error */
  VRP_OUT_FRAME_CLOSING     /**< all data sent */
};

enum vrp_outgoing_state_e
{
  VRP_OUTGOING_INIT = 0,
  VRP_OUTGOING_OK,
  VRP_OUTGOING_CLOSING,
  VRP_OUTGOING_CLOSED
};

struct vrp_out_frame_s
{
  int frame_num;
  enum vrp_out_frame_state_e state;
  struct timeval deadline;

  /* sliding window */
  int ack_expected;
  int next_to_send;
  /* buffers */
  int next_to_buffer;
  struct vrp_out_cell_s*cells;
  int num_cells;
  char send_buf[VRP_MAX_PACKET_SIZE];
  /* data */
  struct vrp_buffer_s*src;
  int max_seq;          /**< maximum packet sequence number for this frame */
  int data_per_packet;  /**< data per packet in bytes */
  /* frame header */
  struct vrp_out_hdr_s hdr;
};
typedef struct vrp_out_frame_s*vrp_out_frame_t;

struct vrp_outgoing_s
{
  enum vrp_outgoing_state_e state;
  /* system */
  int fd;
  padico_io_t io;
  struct sockaddr_in local_addr;
  struct sockaddr_in outgoing_addr;
  struct hostent*host;
  char * hostname;
  int port; /* in host byte order */

  struct vrp_out_timing_param_s timing;  /**< dynamic timeout adjustment parameters */

  struct vrp_out_frame_s*current_frame;
  int last_frame_num;

  int max_cons_loss;
  int max_loss_rate;

  int n_read;
  char recv_buf[VRP_MAX_PACKET_SIZE];

  int fault_code;
};

/* private functions */

static inline struct vrp_out_cell_s* vrp_out_cell_translate(vrp_out_frame_t frame, int i)
{
  return &(frame->cells[ (i % frame->num_cells) ]);
}

static void
vrp_outgoing_adapt_timeout(vrp_outgoing_t outgoing,
                           struct vrp_out_cell_s*);

static void
vrp_outgoing_timeout_expired(vrp_outgoing_t outgoing);


static void
vrp_start_timer(struct vrp_out_cell_s*cell, vrp_outgoing_t outgoing);

static int
vrp_check_timeout(struct vrp_out_cell_s*cell);

static void
vrp_out_frame_alive(vrp_out_frame_t frame);

static int
vrp_out_frame_isalive(vrp_out_frame_t frame);


static vrp_out_frame_t
vrp_out_frame_construct(vrp_outgoing_t outgoing,
                        vrp_buffer_t buffer);

static void
vrp_out_frame_destroy(vrp_outgoing_t outgoing, vrp_out_frame_t frame);

static void
vrp_outgoing_send_packet(vrp_outgoing_t outgoing,
                         vrp_out_frame_t frame,
                         int seq);

static void
vrp_out_frame_send_hdr(vrp_outgoing_t outgoing,
                            vrp_out_frame_t frame);

static void
vrp_out_frame_hdr_construct(vrp_outgoing_t outgoing,
                                 vrp_out_frame_t frame);

static int
vrp_outgoing_read_callback(int fd, void *arg);

static void
vrp_outgoing_process_fragment(vrp_outgoing_t outgoing);

static void
vrp_outgoing_send_routine(vrp_outgoing_t outgoing,
                          vrp_out_frame_t frame);

/* *****************************************************************
 * **        VRP outgoing funcs
 * *****************************************************************
 */



/*
 * vrp_outgoing_construct()
 *
 * Construct a vrp_outgoing_t for the given host and port. Look up in the
 * outgoing table to see if one already exists. If it does, bump its reference
 * count and return that one. Otherwise create one, insert into the
 * table with a reference count of 1 and return it.
 */
vrp_outgoing_t
vrp_outgoing_construct(char*hostname, int port,
                              int max_loss_rate, int max_cons_loss)
{
  vrp_outgoing_t outgoing;
  int rc;
  int len;
  int sock_buffsize;
  struct vrp_out_hdr_s ping_hdr;
  struct timeval date;

  outgoing = (vrp_outgoing_t)vrp_malloc(sizeof(struct vrp_outgoing_s));
  vrp_show(50, "enter, host = %s, port = %i\n", hostname, port);
  outgoing->state = VRP_OUTGOING_INIT;

  /* *** create the socket */
  /* socket */
  rc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(rc < 0)
    vrp_fatal("socket() [vrp_outgoing_construct()]");
  else
    outgoing->fd = rc;
  vrp_show(60, "socket fd = %i\n", outgoing->fd);

  /* bind */
  vrp_show(60, "binding socket %i to local port\n", outgoing->fd);
  len = sizeof(outgoing->local_addr);
  outgoing->local_addr.sin_family = AF_INET;
  outgoing->local_addr.sin_port   = htons(0);
  memset(outgoing->local_addr.sin_zero,0,8);
  outgoing->local_addr.sin_addr.s_addr = INADDR_ANY;
  rc = bind(outgoing->fd,
            (struct sockaddr*) &outgoing->local_addr,
            len);
  if(rc < 0)
    vrp_fatal("bind() [vrp_outgoing_construct()]");

  rc = getsockname(outgoing->fd,
                   (struct sockaddr*) &outgoing->local_addr,
                   &len);
  if(rc < 0)
    vrp_fatal("getsockname() [vrp_outgoing_construct()]");

  vrp_show(50, "looking up host %s:%i\n", hostname, port);
  outgoing->host = gethostbyname(hostname);
  outgoing->outgoing_addr.sin_family = AF_INET;
  memcpy(&outgoing->outgoing_addr.sin_addr.s_addr,
         outgoing->host->h_addr_list[0],
         outgoing->host->h_length);
  outgoing->outgoing_addr.sin_port = htons(port);

  vrp_show(60, "Setting descriptor options\n");
  rc = fcntl(outgoing->fd, F_SETFL, O_NONBLOCK);
  if(rc == -1)
    vrp_fatal("fcntl O_NONBLOCK [vrp_outgoing_construct()]");

  sock_buffsize = 64 * 1024 ;
  rc = setsockopt(outgoing->fd, SOL_SOCKET, SO_RCVBUF, (char *) &sock_buffsize,
                  sizeof(sock_buffsize));
  if(rc < 0)
    vrp_fatal("setsockopt SOL_SOCKET [vrp_outgoing_construct()]");

  /* *** init internal structures */
  outgoing->current_frame = NULL;
  srand(getpid());
  outgoing->port = port;
  outgoing->hostname = hostname;
  outgoing->max_loss_rate = max_loss_rate * 256 / 100;
  outgoing->max_cons_loss = max_cons_loss;
  outgoing->last_frame_num = ((unsigned)rand()) % VRP_FRAME_MAX;
  outgoing->n_read = 0;
  outgoing->timing.timeout = VRP_TIMEOUT_DEFAULT;
  outgoing->timing.RTT = VRP_TIMEOUT_DEFAULT;
  outgoing->timing.average_deviation = VRP_TIMEOUT_DEFAULT/4;
  outgoing->fault_code = 0;
  fd_finder.fd_table[outgoing->fd].type = FD_TABLE_TYPE_OUTGOING;
  fd_finder.fd_table[outgoing->fd].value = (void *) outgoing;
  outgoing->io = padico_io_register(outgoing->fd,
                                    PADICO_IO_EVENT_READ,
                                    &vrp_outgoing_read_callback,
                                    outgoing);
  padico_io_activate(outgoing->io);

  /* *** open the connection: ping */
  while(outgoing->state == VRP_OUTGOING_INIT)
  {
    vrp_print("sending ping to IP=%s:%d...\n",
                 inet_ntoa(outgoing->outgoing_addr.sin_addr),
                 ntohs(outgoing->outgoing_addr.sin_port));
    gettimeofday(&date, NULL);
    vrp_hdr_pack_std(ping_hdr.content, &ping_hdr.size);
    vrp_hdr_pack_tag(ping_hdr.content, &ping_hdr.size, VRP_TAG_PING, date.tv_sec, date.tv_usec);
    vrp_hdr_pack_tag(ping_hdr.content, &ping_hdr.size, VRP_TAG_LAST);
    vrp_show(50, "send ping to %s:%d\n", outgoing->hostname, outgoing->port);
    vrp_sendto(outgoing->fd,
               &ping_hdr.content,
               ping_hdr.size,
               0,
               (struct sockaddr*)&outgoing->outgoing_addr,
               sizeof(outgoing->outgoing_addr));
    vrp_start_timer(&ping_hdr.cell, outgoing);
    while((!vrp_check_timeout(&ping_hdr.cell)) && (outgoing->state != VRP_OUTGOING_OK))
      {
        marcel_yield();
      }
  }
  vrp_print("Connection established.\n");
  vrp_print("VRP: outgoing statistics\n"
               "  timeout = %ld usec\n  RTT = %ld usec (latency = %g ms)\n  average_deviation = %ld\n",
               (long)outgoing->timing.timeout,
               (long)outgoing->timing.RTT,
               (double)(outgoing->timing.RTT/2000.0),
               (long)outgoing->timing.average_deviation);

  return outgoing;
}



/*
 * vrp_outgoing_close()
 *
 * Close a outgoing:
 *   1) Remove the outgoing from the fd_table table
 *   2) Close the fd
 *   3) Modify outgoing data structure
 */
void
vrp_outgoing_close(vrp_outgoing_t outgoing)
{
  int done;
  char ack_buf[VRP_MAX_PACKET_SIZE];
  int ack_size;

  vrp_show(50, "closing outgoing\n");
  outgoing->state = VRP_OUTGOING_CLOSING;

  do
    {
      done = 1;
      if((outgoing->current_frame != NULL) &&
         (outgoing->current_frame->state != VRP_OUT_FRAME_CLOSING))
        done = 0;

    }
  while(!done);
  vrp_hdr_pack_std(ack_buf, &ack_size);
  /*
  for(i = 0; i < VRP_OUTGOING_NUM_FRAMES; i++)
    {
      if(outgoing->frames[i].state != VRP_OUT_FRAME_NONE)
        {
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_CLOSE, outgoing->frames[i].frame_num);
          vrp_out_frame_destroy(&outgoing->frames[i]);
        }
    }
  */
  if(outgoing->current_frame != NULL)
    {
      vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_CLOSE, outgoing->current_frame->frame_num);
      vrp_out_frame_destroy(outgoing, outgoing->current_frame);
    }

  vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
  vrp_sendto(outgoing->fd,
             (char*)&ack_buf,
             ack_size,
             0,
             (struct sockaddr*)&outgoing->outgoing_addr,
             sizeof(outgoing->outgoing_addr));
  outgoing->state = VRP_OUTGOING_CLOSED;
  padico_io_unregister(outgoing->io);
  close(outgoing->fd);

  fd_finder.fd_table[outgoing->fd].type = FD_TABLE_TYPE_NONE;
  fd_finder.fd_table[outgoing->fd].value = NULL;
  outgoing->fd = -1;
  vrp_free(outgoing);
}


int
vrp_outgoing_send_frame(vrp_outgoing_t outgoing,
                        vrp_buffer_t buffer)
{
  vrp_out_frame_t frame;
  int rc;

  vrp_show(50, "entering\n");
  rc = 0;
  vrp_enter();
  if(outgoing->state != VRP_OUTGOING_OK)
    {
      vrp_show(5, "not ready\n");
      rc = VRP_ERROR_NOT_READY;
      vrp_exit();
      goto fn_exit;
    }
  if(buffer->size <= 0)
    {
      vrp_exit();
      goto fn_exit;
    }
  /* lookup an empty frame slot */
  /*  frame = vrp_out_frame_slot_lookup(outgoing); */
  frame = vrp_out_frame_construct(outgoing, buffer);
  if(frame == NULL)
    {
      rc = VRP_ERROR_NOT_READY;
      vrp_exit();
      goto fn_exit;
    }
  vrp_exit();
  /* we have now a new frame structure - build & send frame header
   * -> we can free the global vrp lock.
   */
  vrp_out_frame_hdr_construct(outgoing, frame);
  vrp_out_frame_send_hdr(outgoing, frame);
  vrp_show(60, "frame header sent\n");

  while(frame->state != VRP_OUT_FRAME_CLOSING)
    {
      assert((frame->ack_expected <= frame->max_seq) ||
             (frame->state == VRP_OUT_FRAME_CLOSING));

      switch(frame->state)
        {
        case VRP_OUT_FRAME_NONE:
          vrp_warning("VRP: bad frame-- cannot send.\n");
          return VRP_ERROR_BAD_FRAME;
          break;
        case VRP_OUT_FRAME_SEND:
          vrp_outgoing_send_routine(outgoing, frame);
          break;
        case VRP_OUT_FRAME_INIT:
          if(vrp_check_timeout(&frame->hdr.cell))
            {
              vrp_warning("Timeout expired for header frame=%d (timeout=%d)\n",
                          frame->frame_num, outgoing->timing.timeout);
              vrp_outgoing_timeout_expired(outgoing);
              vrp_out_frame_send_hdr(outgoing, frame);
            }
          break;
        case VRP_OUT_FRAME_CLOSING:
          break;
        case VRP_OUT_FRAME_REFUSED:
          vrp_out_frame_destroy(outgoing, frame);
          return VRP_ERROR_CONNECTION_REFUSED;
        case VRP_OUT_FRAME_ERROR:
          vrp_out_frame_destroy(outgoing, frame);
          return VRP_ERROR_INTERNAL_ERROR;
        }
      if(!vrp_out_frame_isalive(frame))
        {
          vrp_show(5, "connection timeout\n");
          vrp_out_frame_destroy(outgoing, frame);
          return VRP_ERROR_CONNECTION_TIMEOUT;
        }
      //      marcel_yield(); // TODO
    }
  vrp_out_frame_destroy(outgoing, frame);
fn_exit:
  assert(outgoing->current_frame == NULL);
  return rc;
}

/** @brief compute new timing parameters
 * @note assume that the cell is received before the timeout expires
 */
static void
vrp_outgoing_adapt_timeout(vrp_outgoing_t outgoing,
                           struct vrp_out_cell_s*cell)
{
  long int deviation;
  long int actual_RTT;

  deviation = (cell->ack_time.tv_sec - cell->expected.tv_sec) * 1000000 +
    (cell->ack_time.tv_usec - cell->expected.tv_usec);

  actual_RTT = (cell->ack_time.tv_sec - cell->start_date.tv_sec) * 1000000 +
    (cell->ack_time.tv_usec - cell->start_date.tv_usec);

  vrp_show(50, "actual_RTT=%i; smoothed params: RTT=%i timeout=%i average_deviation=%d\n",
             (int)actual_RTT,
             (int)outgoing->timing.RTT,
             (int)outgoing->timing.timeout,
             (int)outgoing->timing.average_deviation);

  vrp_log(vrp_log_fd,
          "%d %d %d %d\n",
          (int)actual_RTT,
          (int)outgoing->timing.RTT,
          (int)outgoing->timing.timeout,
          (int)outgoing->timing.average_deviation);
  if((actual_RTT <= cell->timing.timeout) && (actual_RTT > 0))
    {
      /* timeout has not expired: common case */
      outgoing->timing.average_deviation =
        ((VRP_TIMEOUT_DEVIATION_SMOOTH_FACTOR - 1) * outgoing->timing.average_deviation +
         abs(deviation)) / VRP_TIMEOUT_DEVIATION_SMOOTH_FACTOR;
      outgoing->timing.RTT =
        ((VRP_TIMEOUT_RTT_SMOOTH_FACTOR - 1) * outgoing->timing.RTT + actual_RTT) / VRP_TIMEOUT_RTT_SMOOTH_FACTOR;
      outgoing->timing.timeout =
        VRP_TIMEOUT_FEEDBACK_FACTOR  * outgoing->timing.RTT +
        VRP_TIMEOUT_DEVIATION_FACTOR * outgoing->timing.average_deviation;
    }
  else if(actual_RTT <= 0)
    {
      /* ACK arrived before the message left... */
      vrp_warning("unexpected ACK -- cannot compute new timeout\n"
                     " (timeout=%i RTT=%i average_deviation=%i deviation=%i)\n",
                     (int)outgoing->timing.timeout,
                     (int)outgoing->timing.RTT,
                     (int)outgoing->timing.average_deviation,
                     (int)deviation);
    }
  else
    {
      /* timeout had actually expired */
      vrp_warning("trying to compute timing parameters for an expired cell -- detected scheduler race condition.\n");
      vrp_print(" deviation=%d; actual_RTT=%d; RTT=%d; timeout=%d; average_deviation=%d\n",
                   (int)deviation,
                   (int)actual_RTT,
                   (int)outgoing->timing.RTT,
                   (int)outgoing->timing.timeout,
                   (int)outgoing->timing.average_deviation);
    }
}

static void
vrp_outgoing_timeout_expired(vrp_outgoing_t outgoing)
{
  if((outgoing->timing.timeout > 0) &&
     (outgoing->timing.timeout < VRP_TIMEOUT_MAX))
    {
      if(outgoing->timing.average_deviation < outgoing->timing.RTT/VRP_TIMEOUT_DEVIATION_FACTOR)
        {
          outgoing->timing.average_deviation *= 1.8;
        }
      else
        {
          outgoing->timing.average_deviation *= 1.2;
        }

    }
  else if(outgoing->timing.timeout <= 0)
    {
      outgoing->timing.RTT = VRP_TIMEOUT_DEFAULT;
    }
  else
    {
      vrp_show(5, " timeout=%d > MAX_TIMEOUT=%d \n",
                 (int)outgoing->timing.timeout, VRP_TIMEOUT_MAX);
      outgoing->timing.average_deviation = VRP_TIMEOUT_MAX / 4;
      outgoing->timing.RTT = VRP_TIMEOUT_MAX;
    }
  outgoing->timing.timeout =
    VRP_TIMEOUT_FEEDBACK_FACTOR  * outgoing->timing.RTT +
    VRP_TIMEOUT_DEVIATION_FACTOR * outgoing->timing.average_deviation;
  vrp_show(50, " RTT=%i timeout=%i average_deviation=%d\n",
             (int)outgoing->timing.RTT,
             (int)outgoing->timing.timeout,
             (int)outgoing->timing.average_deviation);
}

static void vrp_out_frame_destroy(vrp_outgoing_t outgoing, vrp_out_frame_t frame)
{
  if(frame != NULL)
    {
      vrp_free(frame->cells);
      vrp_free(frame);
      outgoing->current_frame = NULL;
    }
  else
    {
      vrp_warning("cannot destroy empty frame\n");
    }
}

/*
static struct vrp_out_frame_s*
vrp_out_frame_lookup(vrp_outgoing_t outgoing, int frame_num)
{
  int i;
  struct vrp_out_frame_s *current = NULL;

  vrp_show(50, "looking up frame %i\n", frame_num);
  for(i = 0; i < VRP_OUTGOING_NUM_FRAMES; i++)
    {
      if(outgoing->frames[i].state != VRP_OUT_FRAME_NONE)
        {
          current = &outgoing->frames[i];
          if(frame_num  == current->frame_num )
            return(current);
        }
    }
  vrp_show(40, "frame not found\n");
  return NULL;
}
*/

static void
vrp_out_frame_alive(vrp_out_frame_t frame)
{
  gettimeofday(&frame->deadline, NULL);
  frame->deadline.tv_sec += VRP_DEAD_TIMEOUT;
}

static int
vrp_out_frame_isalive(vrp_out_frame_t frame)
{
  struct timeval time;
  gettimeofday(&time, NULL);
  return(time.tv_sec <= frame->deadline.tv_sec);
}

/*
static struct vrp_out_frame_s*
vrp_out_frame_slot_lookup(vrp_outgoing_t outgoing)
{
  int i;

  vrp_show(60, "looking up slot\n");
  for(i = 0; i < VRP_OUTGOING_NUM_FRAMES; i++)
    if(outgoing->frames[i].state == VRP_OUT_FRAME_NONE)
      return(&outgoing->frames[i]);
  vrp_show(5, "no frame slot available\n");
  return NULL;
}
*/

static vrp_out_frame_t
vrp_out_frame_construct(vrp_outgoing_t outgoing,
                        vrp_buffer_t   buffer)
{
  int i;

  vrp_out_frame_t frame = vrp_malloc(sizeof(struct vrp_out_frame_s));

  vrp_show(50, "new frame\n");
  frame->state = VRP_OUT_FRAME_INIT;
  frame->frame_num = outgoing->last_frame_num;
  outgoing->last_frame_num = (outgoing->last_frame_num + 1) % VRP_FRAME_MAX;
  /* build window */
  frame->next_to_send = 0;
  frame->ack_expected = 0;
  frame->next_to_buffer = 0;
  frame->num_cells = (2*VRP_OUTGOING_WINDOW_SIZE);
  frame->cells = vrp_malloc(frame->num_cells * sizeof(struct vrp_out_cell_s));
  for(i = 0; i < frame->num_cells; i++)
    {
      frame->cells[i].acked = 0;
      frame->cells[i].sent = 0;
      frame->cells[i].expired = 0;
    }
  /* cut data into packets */
  frame->data_per_packet = VRP_DATA_PER_PACKET;
  frame->max_seq = (buffer->size - 1) / VRP_DATA_PER_PACKET;
  frame->src = buffer;
  vrp_out_frame_alive(frame);
  vrp_show(50, "frame %i : %i packets\n", frame->frame_num, frame->max_seq+1);

  assert(outgoing->current_frame == NULL);
  outgoing->current_frame = frame;
  return frame;
}


static void
vrp_outgoing_send_packet(vrp_outgoing_t outgoing,
                         vrp_out_frame_t frame,
                         int seq)
{
  struct vrp_hdr_s hdr;
  int size;

  vrp_show(50,"seq = %i frame = %i\n", seq, frame->frame_num);

  hdr.frame = frame->frame_num;
  hdr.seq = seq;
  hdr.is_hdr = 0;
  vrp_hdr_pack(&hdr, frame->send_buf);
  size = ((seq >= frame->max_seq) ?
          frame->src->size - frame->data_per_packet * frame->max_seq :
          frame->data_per_packet);
  memcpy(frame->send_buf + VRP_HDR_SIZE,
         frame->src->data + seq * frame->data_per_packet,
         size);
  size += VRP_HDR_SIZE;
  vrp_sendto(outgoing->fd,
             (char*)&frame->send_buf,
             size,
             0,
             (struct sockaddr*)&outgoing->outgoing_addr,
             sizeof(outgoing->outgoing_addr));
  if(vrp_out_cell_translate(frame, seq)->sent)
    {
      vrp_out_cell_translate(frame, seq)->expired = 1;
    }
  else
    {
      vrp_out_cell_translate(frame, seq)->sent = 1;
    }
  vrp_start_timer(vrp_out_cell_translate(frame, seq), outgoing);
}

static void
vrp_out_frame_send_hdr(vrp_outgoing_t outgoing,
                            vrp_out_frame_t frame)
{
  vrp_show(50, "frame = %i size = %i\n", frame->frame_num, frame->hdr.size);
  assert(frame->hdr.size < VRP_MAX_PACKET_SIZE);
  vrp_sendto(outgoing->fd,
             &frame->hdr.content,
             VRP_MAX_PACKET_SIZE,
             0,
             (struct sockaddr*)&outgoing->outgoing_addr,
             sizeof(outgoing->outgoing_addr));
  vrp_start_timer(&frame->hdr.cell, outgoing);
}


static void
vrp_out_frame_hdr_construct(vrp_outgoing_t outgoing,
                                 vrp_out_frame_t frame)
{
  struct vrp_buffer_s*buffer;
  struct vrp_critical_cell_s*cs;
  int b, e;

  vrp_show(60, "frame = %i\n", frame->frame_num);
  buffer = frame->src;
  vrp_hdr_pack_std(frame->hdr.content, &frame->hdr.size);
  /* 'create frame' tag */
  vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                   VRP_TAG_FRAME_CREATE, frame->frame_num, buffer->size, frame->data_per_packet);
  /* loss tolerance parameters */
  if((buffer->max_cons_loss > 0) && (buffer->max_cons_loss < 32768))
    {
      vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                       VRP_TAG_CONS_LOSS, buffer->max_cons_loss / frame->data_per_packet );
    }
  else if((outgoing->max_cons_loss > 0) && (outgoing->max_cons_loss < 32768))
    {
      vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                       VRP_TAG_CONS_LOSS, outgoing->max_cons_loss / frame->data_per_packet);
    }
  if((buffer->max_loss_rate >=0) && (buffer->max_loss_rate < 256))
    {
      vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                       VRP_TAG_LOSS_RATE, buffer->max_loss_rate);
    }
  else if((outgoing->max_loss_rate >=0) && (outgoing->max_loss_rate < 256))
    {
      vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                       VRP_TAG_LOSS_RATE, outgoing->max_loss_rate);
    }

  /* calculate critical packets */
  cs = buffer->critical;
  while(cs != NULL)
    {
      b = cs->begin / frame->data_per_packet;
      e = (cs->end - 1) / frame->data_per_packet;
      if((b >= 0) && (e <= frame->max_seq) && (b <= e))
        {
          if(b == e)
            {
              vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                               VRP_TAG_CRITICAL_PACKET, b);
            }
          else
            {
              vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                               VRP_TAG_CRITICAL_INTERVAL, b, e);
            }
        }
      cs = cs->next;
    }
  /* check 'end of frame' */
  if(outgoing->current_frame != NULL && outgoing->current_frame->state == VRP_OUT_FRAME_CLOSING)
    {
      vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                       VRP_TAG_FRAME_CLOSE, outgoing->current_frame->frame_num);
      vrp_out_frame_destroy(outgoing, outgoing->current_frame);
    }

  /* end of header */
  vrp_hdr_pack_tag(frame->hdr.content, &frame->hdr.size,
                   VRP_TAG_LAST);
}

int
vrp_outgoing_read_callback(int fd, void *arg)
{
  vrp_outgoing_t outgoing;
  int save_errno;
  int done;
  struct sockaddr from_addr;
  int from_len;

  vrp_show(50, "fd = %i\n", fd);
  outgoing = (vrp_outgoing_t )arg;

  vrp_enter();
  done = 0;
  while(!done)
    {
      from_len = sizeof(from_addr);
      outgoing->n_read = vrp_recvfrom(outgoing->fd,
                                      (char*) outgoing->recv_buf,
                                      VRP_MAX_PACKET_SIZE,
                                      0,
                                      &from_addr,
                                      &from_len);
      save_errno = errno;
      vrp_show(60, "%i bytes read\n", outgoing->n_read);
      if(outgoing->n_read > 0)
        {
          vrp_outgoing_process_fragment(outgoing);
        }
      else if (save_errno == EINTR)
        {
          /* try again */
        }
      else if ((save_errno == EAGAIN) || (save_errno == EWOULDBLOCK))
        {
          done = 1;
        }
      else
        {
          vrp_warning("recvfrom() errno=%d n_read=%d (%s)\n",
                         save_errno, outgoing->n_read, strerror(save_errno));
        }
    }

  vrp_exit();
  return 1;
}


static void
vrp_outgoing_process_fragment(vrp_outgoing_t outgoing)
{
  vrp_hdr_t hdr;
  vrp_out_frame_t frame;
  int i;
  int seq;
  char*last_byte, *c, *prev;
  struct timeval timestamp;
  int sec, usec;
  struct vrp_out_cell_s*cell;

  gettimeofday(&timestamp, NULL);

  vrp_hdr_unpack(outgoing->recv_buf, &hdr);
  vrp_show(40, "received frame=%i seq=%i\n", hdr.frame, hdr.seq);
  c = (char*)outgoing->recv_buf + VRP_HDR_SIZE;
  last_byte = (char*)outgoing->recv_buf + outgoing->n_read - 1;
  /*
  if(hdr.frame != VRP_NO_FRAME)
    frame = vrp_out_frame_lookup(outgoing, hdr.frame);
  else
    frame = NULL;
  */
  if(outgoing->current_frame != NULL && outgoing->current_frame->frame_num == hdr.frame)
    frame = outgoing->current_frame;
  else
    {
      frame = NULL;
      vrp_show(50, "received fragment for no frame (hdr frame=%d current_frame=%p)\n",
                 hdr.frame, outgoing->current_frame);
    }

  if(!hdr.is_hdr)
    goto badheader;

  while(*c != VRP_TAG_LAST)
    {
      if(c > last_byte)
        {
          goto badheader;
        }
      prev = c;
      if(frame != NULL)
        {
          switch(*c)
            {
            case VRP_TAG_ACK:
              if(c + 4 > last_byte)
                goto badheader;
              seq = vrp_unpack_int32(c+1);
              vrp_show(50, "ACK frame=%i packet=%i\n", frame->frame_num, seq);
              if((frame->ack_expected <= seq) &&
                 (seq < frame->next_to_send))
                {
                  cell = vrp_out_cell_translate(frame, seq);
                  cell->ack_time = timestamp;
                  for(i = frame->ack_expected; i <= seq; i++)
                    {
                      vrp_out_cell_translate(frame, i)->acked = 1;
                    }
                  frame->ack_expected = seq + 1;
                  vrp_out_frame_alive(frame);
                  if(!cell->expired)
                    {
                      vrp_outgoing_adapt_timeout(outgoing, cell);
                    }
                }
              else if(seq >= frame->next_to_send)
                {
                  frame->state = VRP_OUT_FRAME_ERROR;
                }
              c += 5;
              break;
            case VRP_TAG_NACK:
              if(c + 4 > last_byte)
                goto badheader;
              seq = vrp_unpack_int32(c+1);
              vrp_show(50, "NACK frame=%i packet=%i\n", frame->frame_num, seq);
              if((frame->ack_expected <= seq) &&
                 (seq < frame->next_to_send))
                {
                  vrp_outgoing_send_packet(outgoing, frame, hdr.seq);
                }
              else if(seq >= frame->next_to_send)
                {
                  frame->state = VRP_OUT_FRAME_ERROR;
                }
              c += 5;
              break;
            case VRP_TAG_SINGLE_ACK:
              if(c + 4 > last_byte)
                goto badheader;
              seq = vrp_unpack_int32(c+1);
              vrp_show(50, "SACK frame=%i packet=%i\n", frame->frame_num, seq);
              if((frame->ack_expected <= seq) &&
                 (seq < frame->next_to_send))
                {
                  cell = vrp_out_cell_translate(frame, seq);
                  cell->acked = 1;
                  cell->ack_time = timestamp;
                  if(!cell->expired)
                    {
                      vrp_outgoing_adapt_timeout(outgoing, cell);
                    }
                  vrp_out_frame_alive(frame);
                }
              else if(seq >= frame->next_to_send)
                {
                  frame->state = VRP_OUT_FRAME_ERROR;
                }
              c += 5;
              break;
            default:
              break;
            }
          /* update ACK info */
          while((frame->ack_expected < frame->next_to_send) &&
                (vrp_out_cell_translate(frame, frame->ack_expected)->acked))
            {
              frame->ack_expected++;
              assert(frame->ack_expected <= frame->max_seq + 1);
            }
          if(frame->ack_expected > frame->max_seq)
            {
              frame->state = VRP_OUT_FRAME_CLOSING;
              vrp_show(50, "CLOSE frame=%i\n", frame->frame_num);
            }
        }
      switch(*c)
        {
        case VRP_TAG_PONG:
          if(c + 8 > last_byte)
            goto badheader;
          if(outgoing->state == VRP_OUTGOING_INIT)
            {
              sec = vrp_unpack_int32(c+1);
              usec = vrp_unpack_int32(c+5);
              outgoing->timing.RTT = (timestamp.tv_usec - usec + 1000000 * (timestamp.tv_sec - sec));
              outgoing->timing.average_deviation = outgoing->timing.RTT / 4;
              outgoing->timing.timeout = outgoing->timing.RTT + 4 * outgoing->timing.average_deviation;
              if((outgoing->timing.timeout > 0) &&
                 (outgoing->timing.timeout < VRP_TIMEOUT_MAX))
                {
                  outgoing->state = VRP_OUTGOING_OK;
                  vrp_show(50, "timeout = %d usec\n", (int)outgoing->timing.timeout);
                }
            }
          c += 9;
          break;
        case VRP_TAG_FRAME_SET:
          {
            int frame_num;
            if(c + 2 > last_byte)
              goto badheader;
            frame_num = vrp_unpack_int16(c+1);
            /*    frame = vrp_out_frame_lookup(outgoing, seq); */
            if(outgoing->current_frame != NULL && outgoing->current_frame->frame_num == frame_num)
              {
                frame = outgoing->current_frame;
              }
            else
              {
                frame = NULL;
                goto badheader;
              }
            c += 3;
          }
          break;
        case VRP_TAG_FRAME_OK:
          {
            int frame_num;
            if(c + 2 > last_byte)
              goto badheader;
            frame_num = vrp_unpack_int16(c+1);
            /*    frame = vrp_out_frame_lookup(outgoing, seq); */
            if(outgoing->current_frame != NULL &&
               outgoing->current_frame->frame_num == frame_num)
              {
                frame = outgoing->current_frame;
                vrp_show(50, "FRAME_OK frame=%i accepted\n", frame_num);
                frame->state = VRP_OUT_FRAME_SEND;
              }
            else
              {
                vrp_warning("received FRAME_OK for other frame than current-- current=%d received=%d\n",
                               outgoing->current_frame->frame_num, hdr.frame);
                frame = NULL;
                goto badheader;
              }
            c += 3;
          }
          break;
        case VRP_TAG_FRAME_REFUSED:
          if(c + 2 > last_byte)
            goto badheader;
          seq = vrp_unpack_int16(c+1);
          vrp_show(20, "FRAME_REFUSED frame=%i refused\n", seq);
          /*      frame = vrp_out_frame_lookup(outgoing, seq); */
          if(outgoing->current_frame != NULL && outgoing->current_frame->frame_num == hdr.frame)
            frame = outgoing->current_frame;
          else
            frame = NULL;
          if(frame != NULL)
            frame->state = VRP_OUT_FRAME_REFUSED;
          else
            {
              goto badheader;
            }
          c += 3;
          break;
        case VRP_TAG_FRAME_MISSING_HDR:
          if(c + 2 > last_byte)
            goto badheader;
          seq = vrp_unpack_int16(c+1);
          /*      frame = vrp_out_frame_lookup(outgoing, seq); */
          if(outgoing->current_frame != NULL && outgoing->current_frame->frame_num == hdr.frame)
            frame = outgoing->current_frame;
          else
            frame = NULL;
          vrp_show(20, "MISSING_HDR frame=%i unknown\n", seq);
          if(frame != NULL)
            vrp_out_frame_send_hdr(outgoing, frame);
          else
            {
              goto badheader;
            }
          c += 3;
          break;
        default:
          break;
        }
      if(prev == c)
        {
          goto badheader;
        }
    }
  return;
badheader:
  vrp_show(20, "Outgoing -- received bad header (frame=%i)\n", hdr.frame);
  return;
}


static void
vrp_outgoing_send_routine(vrp_outgoing_t outgoing,
                          vrp_out_frame_t frame)
{
  struct vrp_out_cell_s*cell;
  int i;
  int first_timeout;

  vrp_show(60, "ack_expected=%i; next_to_send=%i; next_to_buffer=%i; max_seq=%i state=%i\n",
             frame->ack_expected, frame->next_to_send,
             frame->next_to_buffer, frame->max_seq, frame->state);
  /* clean buffers */
  while((frame->next_to_buffer < frame->ack_expected + frame->num_cells - 1) &&
        (frame->next_to_buffer <= frame->max_seq))
    {
      cell = vrp_out_cell_translate(frame, frame->next_to_buffer);
      cell->acked = 0;
      cell->sent = 0;
      cell->expired = 0;
      frame->next_to_buffer++;
    }

  /* check ACK timeout */
  first_timeout = 1;
  for(i = frame->ack_expected; i < frame->next_to_send; i++)
    {
      cell = vrp_out_cell_translate(frame, i);
      if(!cell->acked)
        if(vrp_check_timeout(cell))
          {
            vrp_show(30, "Timeout expired packet=%i/%i frame=%i (timeout=%i usec)\n",
                       i, frame->max_seq, frame->frame_num, (int)outgoing->timing.timeout);
            vrp_show(30, "next_to_send=%d next_to_buffer=%d ack_expected=%d \n",
                       frame->next_to_send, frame->next_to_buffer, frame->ack_expected);
            if(first_timeout)
              {
                vrp_outgoing_timeout_expired(outgoing);
                first_timeout = 0;
              }
            vrp_show(40, "re-send packet=%d frame=%d next_to_send=%d ack_expected=%d\n",
                       i, frame->frame_num, frame->next_to_send, frame->ack_expected);
            vrp_outgoing_send_packet(outgoing, frame, i);
          }
    }

  /* send packets */
  while((frame->next_to_send < frame->next_to_buffer) &&
        (frame->next_to_send < frame->ack_expected + VRP_OUTGOING_WINDOW_SIZE))
    {
      vrp_show(50, "Send packet=%d frame=%d\n", frame->next_to_send, frame->frame_num);
      vrp_outgoing_send_packet(outgoing, frame, frame->next_to_send);
      frame->next_to_send++;
    }
}



static void vrp_start_timer(struct vrp_out_cell_s*cell, vrp_outgoing_t outgoing)
{
  gettimeofday(&cell->start_date, NULL);
  /* compute 'expire_date' */
  cell->expire_date.tv_sec  = cell->start_date.tv_sec;
  cell->expire_date.tv_usec = cell->start_date.tv_usec + outgoing->timing.timeout;
  while(cell->expire_date.tv_usec >= 1000000)
    {
      cell->expire_date.tv_sec  += 1;
      cell->expire_date.tv_usec -= 1000000;
    }
  /* compute 'expected' */
  cell->expected.tv_sec  = cell->start_date.tv_sec;
  cell->expected.tv_usec = cell->start_date.tv_usec + outgoing->timing.RTT;
  while(cell->expected.tv_usec >= 1000000)
    {
      cell->expected.tv_sec  += 1;
      cell->expected.tv_usec -= 1000000;
    }
  cell->timing = outgoing->timing;
}

static int vrp_check_timeout(struct vrp_out_cell_s*cell)
{
  struct timeval date;

  gettimeofday(&date, NULL);
  return(((cell->expire_date.tv_sec == date.tv_sec) &&
          (cell->expire_date.tv_usec < date.tv_usec)) ||
         (cell->expire_date.tv_sec < date.tv_sec));
}
