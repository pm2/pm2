/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Packing/unpacking primitives
 * @ingroup VRP
 */

#include "VRP-internals.h"


void vrp_pack_int32(char*packed, int n)
{
  unsigned long u;
  unsigned char*c;
  u = (unsigned long)n;
  c = (unsigned char*)packed;
  c[0] = (u >> 24) & 0xFF;
  c[1] = (u >> 16) & 0xFF;
  c[2] = (u >>  8) & 0xFF;
  c[3] =  u        & 0xFF;
}

void vrp_pack_int16(char*packed, int n)
{
  unsigned u;
  unsigned char*c;
  u = (unsigned)n;
  c = (unsigned char*)packed;
  c[0] = (u >> 8) & 0xFF;
  c[1] =  u       & 0xFF;
}

void vrp_pack_int8(char*packed, int n)
{
  unsigned u;
  unsigned char*c;
  u = (unsigned)n;
  c = (unsigned char*)packed;
  c[0] = u & 0xFF;
}

unsigned int vrp_unpack_int32(char*packed)
{
  unsigned char*c;
  c = (unsigned char*)packed;
  return((c[0] << 24) | (c[1] << 16) | (c[2] << 8) | c[3] );
}

unsigned int vrp_unpack_int16(char*packed)
{
  unsigned char*c;
  c = (unsigned char*)packed;
  return((c[0] << 8) | c[1] );
}

unsigned int vrp_unpack_int8(char*packed)
{
  unsigned char*c;
  c = (unsigned char*)packed;
  return(c[0]);
}

void vrp_hdr_unpack(char *hdr_packed, struct vrp_hdr_s *hdr)
{
  unsigned char*c;

  c = (unsigned char*)hdr_packed;
  hdr->is_hdr = c[0] & 0x80;
  hdr->frame   = (((unsigned long)(c[0] & 0x7F))  << 8 ) | ((unsigned long)c[1]);
  hdr->seq     = (((unsigned long)c[2]  << 24) |
                  ((unsigned long)c[3]  << 16) |
                  ((unsigned long)c[4]  << 8 ) |
                  ((unsigned long)c[5]));
}

void vrp_hdr_pack(struct vrp_hdr_s *hdr, char *hdr_packed)
{
  unsigned long f, s;
  unsigned char*c;

  c = (unsigned char*)hdr_packed;
  f = hdr->frame;
  s = hdr->seq;
  c[0] = ((f >> 8) & 0x7F) | (hdr->is_hdr ? 0x80 : 0) ;
  c[1] =   f       & 0xFF;
  c[2] = (s >> 24) & 0xFF;
  c[3] = (s >> 16) & 0xFF;
  c[4] = (s >>  8) & 0xFF;
  c[5] =  s        & 0xFF;
}

void vrp_hdr_pack_std(char*packed, int*size)
{
  struct vrp_hdr_s hdr;

  hdr.is_hdr = 1;
  hdr.frame = VRP_NO_FRAME;
  hdr.seq = 0;
  vrp_hdr_pack(&hdr, packed);
  *size = VRP_HDR_SIZE;
}

int vrp_hdr_pack_tag(char*c, int*size, enum vrp_tag_e tag, ...)
{
  va_list pvar;
  va_start(pvar, tag);
  c[*size] = tag;
  (*size)++;
  switch(tag)
    {
      /* tag followed by a seq # */
    case VRP_TAG_ACK:
    case VRP_TAG_NACK:
    case VRP_TAG_SINGLE_ACK:
      vrp_pack_int32(c + *size, va_arg(pvar, int));
      (*size) += 4;
      break;
      /* tag followed by a frame # */
    case VRP_TAG_FRAME_SET:
    case VRP_TAG_FRAME_OK:
    case VRP_TAG_FRAME_REFUSED:
    case VRP_TAG_FRAME_CLOSE:
    case VRP_TAG_FRAME_MISSING_HDR:
      vrp_pack_int16(c + *size, va_arg(pvar, int));
      (*size) += 2;
      break;
      /* tag followed by nothing */
    case VRP_TAG_LAST:
      break;
    case VRP_TAG_FRAME_CREATE:
      vrp_pack_int16(c + *size, va_arg(pvar, int));
      vrp_pack_int32(c + *size + 2, va_arg(pvar, int));
      vrp_pack_int16(c + *size + 6, va_arg(pvar, int));
      (*size) += 8;
      break;
      /* tag followed by a char */
    case VRP_TAG_CONS_LOSS:
    case VRP_TAG_LOSS_RATE:
      vrp_pack_int8(c + *size, va_arg(pvar, int));
      (*size) += 1;
      break;
    case VRP_TAG_CRITICAL_PACKET:
      vrp_pack_int32(c + *size, va_arg(pvar, int));
      (*size) += 4;
      break;
    case VRP_TAG_CRITICAL_INTERVAL:
      vrp_pack_int32(c + *size, va_arg(pvar, int));
      vrp_pack_int32(c + 4 + *size, va_arg(pvar, int));
      (*size) += 8;
      break;
    case VRP_TAG_PING:
    case VRP_TAG_PONG:
      vrp_pack_int32(c + *size, va_arg(pvar, int));
      vrp_pack_int32(c + 4 + *size, va_arg(pvar, int));
      (*size) += 8;
      break;
    }
  va_end(pvar);
  assert((*size) < VRP_MAX_PACKET_SIZE); /* TODO: correct error handling with return code */
  return 0;
}
