/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Receiver-side part of VRP
 * @ingroup VRP
 */

#include "VRP-internals.h"


/* TODO: - check number of *needed* buffers
 */

struct vrp_in_frame_state_s
{
  unsigned int receiving: 1;
  unsigned int ready_for_delivery: 1;
  unsigned int closed:    1;
};

struct vrp_in_cell_s
{
  unsigned int received: 1;
  unsigned int valid:    1;
  unsigned int waiting:  1;
  unsigned int required: 1;
};

struct vrp_in_stat_s
{
  int packets_received;
  int loss_detected;
  int loss_allowed;
  int recovery;
  int ack;
  int nack;
  int sack;
  int duplicated;
};

struct vrp_in_frame_s
{
  /* TODO: retirer la description des donn�es ici pour plut�t int�grer un vrp_in_buffer.
   */

  /* my id */
  struct sockaddr from_addr;
  int from_len;
  int frame_num;
  /* state */
  struct vrp_in_frame_state_s state;
  struct timeval deadline;
  /* storage */
  char *storage;
  int storage_size;
  /* sliding window */
  int next_exp;
  int waiting;
  struct vrp_in_cell_s*buffer; /* actually an array of size last_seq */

  /* losses statistics */
  int loss_max;
  int cons_loss_max;
  int loss_cnt;
  int total_loss;

  /* data expected */
  int data_per_packet;
  int data_size;
  int last_seq;   /* last seq number of the frame */

  /* statistics for tuning */
  struct vrp_in_stat_s stats;
  struct vrp_incoming_s*my_incoming;
};

typedef struct vrp_in_frame_s* vrp_in_frame_t;


/*
 * vrp_incoming_t
 *
 * One of these is allocated for each incoming file descriptor.
 */
struct vrp_incoming_s
{
  /* current frame */
  struct vrp_in_frame_s*frame;

  /* buffer for last received */
  char recv_buf[VRP_MAX_PACKET_SIZE];
  int n_read;

  /* id of last received */
  struct sockaddr from_addr;
  int from_len;

  /* system */
  int fd;
  padico_io_t io;
  struct sockaddr_in local_addr;
  int callback_registered;

  vrp_frame_handler_t frame_handler;
};

static void
vrp_in_frame_destroy(vrp_in_frame_t frame);

static void
vrp_in_frame_alive(vrp_in_frame_t frame);

static int
vrp_in_frame_isalive(vrp_in_frame_t frame);

static void
vrp_incoming_hdr_send(vrp_incoming_t incoming, char*packed, int size);

static int
vrp_incoming_read_callback(int fd, void *arg);

static struct vrp_in_frame_s*
vrp_in_frame_lookup(vrp_incoming_t incoming,
                          struct sockaddr*from_addr,
                          int from_len,
                          int frame_num);

static vrp_in_frame_t
vrp_in_frame_construct(vrp_incoming_t incoming,
                       int frame_num);

static void
vrp_incoming_process_hdr(vrp_incoming_t incoming,
                         vrp_in_frame_t  frame);

static void
vrp_incoming_process_data(vrp_incoming_t incoming,
                          vrp_in_frame_t frame,
                          vrp_hdr_t *hdr);

static void
vrp_incoming_process_fragment(vrp_incoming_t incoming);

static void
vrp_incoming_deliver_frame(vrp_incoming_t incoming,
                           vrp_in_frame_t frame);



/* *****************************************************************
 * **        VRP incoming funcs
 * *****************************************************************
 */




/*
 * vrp_incoming_construct()
 *
 * Construct a vrp_incoming_t for the given port.
 */

vrp_incoming_t
vrp_incoming_construct(int*port, vrp_frame_handler_t frame_handler)
{
  int rc;
  int len;
  int sock_buffsize;
  vrp_incoming_t incoming;

  vrp_show(50, "port = %i\n", *port);
  incoming = (vrp_incoming_t)vrp_malloc(sizeof(struct vrp_incoming_s));
  incoming->n_read = 0;
  incoming->from_len = sizeof(incoming->from_addr);
  /* socket */
  vrp_show(60, "Creating socket\n");
  rc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(rc < 0)
    vrp_fatal("socket() [vrp_incoming_construct()]");
  incoming->fd = rc;

  /* bind */
  vrp_show(60, "Binding socket to local port %d\n", *port);
  len = sizeof(incoming->local_addr);
  incoming->local_addr.sin_family = AF_INET;
  incoming->local_addr.sin_port   = htons(*port);
  memset(incoming->local_addr.sin_zero,0,8);
  incoming->local_addr.sin_addr.s_addr = INADDR_ANY;
  rc = bind(incoming->fd,
            (struct sockaddr*)&incoming->local_addr,
            len);
  if(rc == -1)
    vrp_fatal("bind() [vrp_incoming_construct()]");
  rc = getsockname(incoming->fd,
                   (struct sockaddr*)&incoming->local_addr,
                   &len);
  if(rc == -1)
    vrp_fatal("getsockname() [vrp_incoming_construct()]");
  *port = ntohs(incoming->local_addr.sin_port);
  vrp_show(60, "socket bound to local port %d\n", *port);

  vrp_show(60, "Setting descriptor options\n");
  rc = fcntl(incoming->fd, F_SETFL, O_NONBLOCK);
  if(rc == -1)
    vrp_fatal("fcntl O_NONBLOCK [vrp_incoming_construct()]");

  sock_buffsize = 64 * 1024 ;
  rc = setsockopt(incoming->fd, SOL_SOCKET, SO_RCVBUF, (char *) &sock_buffsize,
                  sizeof(sock_buffsize));
  if(rc < 0)
    vrp_fatal("setsockopt SOL_SOCKET [vrp_incoming_construct()]");

  incoming->frame = NULL;

  fd_finder.fd_table[incoming->fd].type = FD_TABLE_TYPE_INCOMING;
  fd_finder.fd_table[incoming->fd].value = (void *) incoming;

  incoming->io = padico_io_register(incoming->fd,
                                    PADICO_IO_EVENT_READ,
                                    &vrp_incoming_read_callback,
                                    incoming);
  incoming->frame_handler = frame_handler;
  padico_io_activate(incoming->io);

  return incoming;
}


/*
 * vrp_incoming_close()
 *
 * Close an incoming connection:
 *   1) Remove the vrp_incoming_t from the fd_table table
 *   2) Close the fd
 *   3) Put the vrp_incoming_t back on the free list.
 */
void
vrp_incoming_close(vrp_incoming_t incoming)
{
  vrp_show(50, "vrp_incoming_close()\n");
  if(incoming->frame != NULL)
    {
      vrp_in_frame_destroy(incoming->frame);
    }
  padico_io_unregister(incoming->io);
  close(incoming->fd);

  fd_finder.fd_table[incoming->fd].type = FD_TABLE_TYPE_NONE;
  fd_finder.fd_table[incoming->fd].value = NULL;
  incoming->fd = -1;
  vrp_free(incoming);
}


/*
 * private functions
 */


static void
vrp_in_frame_destroy(vrp_in_frame_t frame)
{
  if(frame == NULL)
    return;
  if(frame->storage != NULL)
    vrp_free(frame->storage);
  frame->storage = NULL;
  frame->storage_size = 0;
  if(frame->buffer != NULL)
    vrp_free(frame->buffer);
  frame->buffer = NULL;
  if(frame->my_incoming != NULL)
    {
      frame->my_incoming->frame = NULL;
    }
  vrp_free(frame);
}

static void
vrp_in_frame_alive(vrp_in_frame_t frame)
{
  gettimeofday(&frame->deadline, NULL);
  frame->deadline.tv_sec += VRP_DEAD_TIMEOUT;
}

static int
vrp_in_frame_isalive(vrp_in_frame_t frame)
{
  struct timeval time;
  gettimeofday(&time, NULL);
  return(time.tv_sec <= frame->deadline.tv_sec);
}


static void
vrp_incoming_hdr_send(vrp_incoming_t incoming, char*packed, int size)
{
  vrp_sendto(incoming->fd,
             packed,
             size,
             0,
             (struct sockaddr*)&incoming->from_addr,
             incoming->from_len);
}


static int
vrp_incoming_read_callback(int fd, void *arg)
{
  vrp_incoming_t incoming;
  int save_errno;
  int done;

  static int tmp_loops = 0;
  static int tmp_cb = 0;
  vrp_enter();
  vrp_show(50, "fd = %i\n", fd);
  incoming = (vrp_incoming_t ) arg;
  assert(fd == incoming->fd);

  done = 0;
  tmp_cb ++;
  while(!done)
    {
      tmp_loops++;
      incoming->from_len = sizeof(incoming->from_addr);
      incoming->n_read = vrp_recvfrom(incoming->fd,
                                      (char*) incoming->recv_buf,
                                      VRP_MAX_PACKET_SIZE,
                                      0,
                                      &incoming->from_addr,
                                      &incoming->from_len);
      save_errno = errno;

      if(incoming->n_read > 0)
        {
          vrp_incoming_process_fragment(incoming);
        }
      else if (save_errno == EINTR)
        {
          /* Try again */
        }
      else if ((save_errno == EAGAIN) || (save_errno == EWOULDBLOCK))
        {
          done = 1;
        }
      else
        {
          vrp_fatal("recvfrom() failed [vrp_incoming_read_callback()]");
        }
    }
/*    vrp_incoming_register_callback(incoming); */
  vrp_exit();
  return 1;
}

static struct vrp_in_frame_s*
vrp_in_frame_lookup(vrp_incoming_t incoming,
                    struct sockaddr*from_addr,
                    int from_len,
                    int frame_num)
{
  struct vrp_in_frame_s *frame = NULL;

  vrp_show(60, "looking up frame...\n");
  if(incoming->frame != NULL)
    {
      frame = incoming->frame;
      if((frame_num == frame->frame_num) &&
         (from_len == frame->from_len) &&
         (memcmp(&frame->from_addr, from_addr, from_len) == 0))
        {
          vrp_in_frame_alive(frame);
          return(frame);
        }
    }
  vrp_show(60, "not found\n");
  return NULL;
}


static vrp_in_frame_t
vrp_in_frame_construct(vrp_incoming_t incoming,
                       int frame_num)
{
  vrp_in_frame_t frame = vrp_malloc(sizeof(struct vrp_in_frame_s));
  incoming->frame = frame;
  frame->my_incoming = incoming;
  /* set my id */
  assert(incoming->from_len <= sizeof(frame->from_addr));
  memcpy(&frame->from_addr, &incoming->from_addr, incoming->from_len);
  frame->from_len = incoming->from_len;
  frame->frame_num = frame_num;
  vrp_show(50, "frame = %i\n", frame->frame_num);
  /* set state */
  vrp_in_frame_alive(frame);
  /* storage clean */
  frame->storage = NULL;
  frame->storage_size = 0;
  /* sliding window */
  frame->next_exp = 0;
  frame->waiting = 0;
  frame->buffer = NULL;
  /* losses statistic */
  frame->loss_max = -1; //(VRP_DEFAULT_LOSS_MAX * VRP_INCOMING_WINDOW_SIZE) / 256;
  frame->cons_loss_max = VRP_DEFAULT_CONS_LOSS_MAX;
  frame->loss_cnt = 0;
  frame->total_loss = 0;
  /* data expected */
  frame->data_per_packet = VRP_DATA_PER_PACKET;
  frame->data_size = 0;
  frame->last_seq = -1;

  frame->state.receiving = 1;
  frame->state.closed = 0;
  frame->state.ready_for_delivery = 0;

  frame->stats.packets_received = 0;
  frame->stats.loss_detected = 0;
  frame->stats.loss_allowed = 0;
  frame->stats.recovery = 0;
  frame->stats.ack = 0;
  frame->stats.nack = 0;
  frame->stats.sack = 0;
  frame->stats.duplicated = 0;
  return frame;
}


static void
vrp_incoming_process_hdr(vrp_incoming_t incoming,
                         vrp_in_frame_t  frame)
{
  char *msg, *old_msg;
  int tmp_frame_num;
  int i, b, e;
  char*last_byte;
  char ack_buf[VRP_MAX_PACKET_SIZE];
  int ack_size;

  vrp_show(50, "process header...\n");
  msg = (char*)incoming->recv_buf + VRP_HDR_SIZE;
  last_byte =  (char*)incoming->recv_buf + VRP_MAX_PACKET_SIZE - 1;

  while(*msg != VRP_TAG_LAST)
    {
      if(msg > last_byte)
        {
          vrp_warning("Ooops! Beyond header boundary!\n");
          goto badheader;
        }
      old_msg = msg;
      if(frame != NULL)
        {
          /* there is a current frame */
          switch(*msg)
            {
            case VRP_TAG_CONS_LOSS:
              if(msg + 1 > last_byte)
                {
                  vrp_warning("Missing cons_loss description (1 byte)\n");
                  goto badheader;
                }
              frame->cons_loss_max = vrp_unpack_int8(msg + 1);
              msg += 2;
              vrp_show(60, "consecutive losses %i\n", frame->cons_loss_max);
              break;
            case VRP_TAG_LOSS_RATE:
              if(msg + 1 > last_byte)
                {
                  vrp_warning("Missing loss_rate description (1 byte)\n");
                  goto badheader;
                }
              assert(frame->data_size > 0);
              frame->loss_max = (frame->data_size * vrp_unpack_int8(msg + 1)) / 256;
              vrp_show(60, "losses %i\n", frame->loss_max);
              msg += 2;
              break;
            case VRP_TAG_CRITICAL_PACKET:
              if(msg + 4 > last_byte)
                {
                  vrp_warning("Missing critical data description\n");
                  goto badheader;
                }
              b = vrp_unpack_int32(msg+1);
              if((b >= 0) && (b <= frame->last_seq))
                {
                  frame->buffer[b].required = 1;
                }
              else
                {
                  vrp_warning("Invalid value for critical data description (4 bytes)\n");
                  goto badheader;
                }
              msg += 5;
              vrp_show(60, "critical packet %i\n", b);
              break;
            case VRP_TAG_CRITICAL_INTERVAL:
              if(msg + 8 > last_byte)
                {
                  vrp_warning("Missing critical interval description (8 bytes)\n");
                  goto badheader;
                }
              b = vrp_unpack_int32(msg+1);
              e = vrp_unpack_int32(msg+5);
              if((b >= 0) && (e <= frame->last_seq) && (b <= e))
                {
                  for(i = b; i<= e; i++)
                    frame->buffer[i].required = 1;
                }
              else
                {
                  vrp_warning("Invalid value for critical interval desription\n");
                  goto badheader;
                }
              msg += 9;
              vrp_show(60, "critical interval (%i - %i)\n", b, e);
              break;
            default:
              break;
            }
        }
      switch(*msg)
        {
        case VRP_TAG_LAST:
          break;
        case VRP_TAG_PING:
          if(msg + 8 > last_byte)
            {
              vrp_warning("Missing ping data\n");
              goto badheader;
            }
          b = vrp_unpack_int32(msg + 1);
          e = vrp_unpack_int32(msg + 5);
          vrp_hdr_pack_std(ack_buf, &ack_size);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_PONG, b, e);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
          vrp_incoming_hdr_send(incoming, ack_buf, ack_size);
          msg += 9;
          break;

        case VRP_TAG_FRAME_CREATE:
          if(msg + 8 > last_byte)
            {
              vrp_warning("Missing frame number for frame_create\n");
              goto badheader;
            }
          tmp_frame_num = vrp_unpack_int16(msg + 1);
          if(incoming->frame != NULL && incoming->frame->frame_num == tmp_frame_num)
            {
              /* duplicated frame header -- discard... but acknowledge anyway! */
              vrp_warning("Duplicated frame %d\n", tmp_frame_num);
              vrp_hdr_pack_std(ack_buf, &ack_size);
              vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_OK, tmp_frame_num);
              vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
              vrp_incoming_hdr_send(incoming, ack_buf, ack_size);
              goto badheader;
            }
          else if(incoming->frame != NULL)
            {
              vrp_warning("Cannot create new frame %d. Current frame %d already exists!\n", tmp_frame_num, incoming->frame->frame_num);
              goto badheader;
            }
          frame = vrp_in_frame_construct(incoming, tmp_frame_num);
          vrp_show(50, "CREATE_FRAME %i\n", tmp_frame_num);
          frame->data_size = vrp_unpack_int32(msg + 3);
          frame->data_per_packet = vrp_unpack_int16(msg + 7);
          frame->last_seq = (frame->data_size - 1) / frame->data_per_packet;
          frame->storage_size = frame->data_per_packet * (1 + frame->last_seq);
          /* storage allocated in multiple of packet size */
          vrp_show(60, "Allocating %i bytes\n", frame->storage_size);
          frame->storage = (char*) vrp_malloc(frame->storage_size);
          if(frame->storage == NULL)
            vrp_fatal("CREATE_FRAME: Out of memory\n");
          frame->buffer = (struct vrp_in_cell_s*)
            vrp_malloc(sizeof(struct vrp_in_cell_s) * (frame->last_seq + 2));
          if(frame->buffer == NULL)
            vrp_fatal("CREATE_FRAME: Out of memory\n");
          for(i = 0; i <= frame->last_seq; i++)
            {
              frame->buffer[i].received  = 0;  /* TODO: can be achieved with a single assignment */
              frame->buffer[i].valid     = 0;
              frame->buffer[i].waiting   = 0;
              frame->buffer[i].required  = 0;
            }
          vrp_show(50, "CREATE_FRAME data size = %i, per packet = %i, last seq = %i, storage: %i\n",
                   frame->data_size, frame->data_per_packet, frame->last_seq, frame->storage_size);
          vrp_hdr_pack_std(ack_buf, &ack_size);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_OK, frame->frame_num);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
          vrp_incoming_hdr_send(incoming, ack_buf, ack_size);
          msg += 9;
          break;

        case VRP_TAG_FRAME_CLOSE:
          if(msg + 2 > last_byte)
            {
              vrp_warning("Missing frame number for frame_close\n");
              goto badheader;
            }
          tmp_frame_num = vrp_unpack_int16(msg+1);;
          vrp_show(60, "close frame %i\n", tmp_frame_num);
          if(incoming->frame != NULL && incoming->frame->frame_num == tmp_frame_num)
            {
              incoming->frame->state.closed = 1;
              /* TODO: destroy frame?*/
            }
          msg +=3;
          break;
        default:
          break;
        }
      if(old_msg == msg)
        {
          vrp_warning("VRP header syntax error\n");
          goto badheader;
        }
    }
  return;
badheader:
  vrp_warning("bad frame header detected.\n");
  if(incoming->frame != NULL)
    {
      if(!vrp_in_frame_isalive(incoming->frame))
        {
          vrp_warning("Dead frame detected -- re-init\n");
          vrp_in_frame_destroy(incoming->frame);
        }
    }
}


static void
vrp_incoming_process_data(vrp_incoming_t incoming,
                          vrp_in_frame_t frame,
                          vrp_hdr_t *hdr)
{
  struct vrp_in_cell_s state;
  int cons_loss;
  int seq;
  char ack_buf[VRP_MAX_PACKET_SIZE];
  int ack_size;

  vrp_show(50, "Received frame=%i packet=%i/%i size=%i\n",
             hdr->frame, hdr->seq, frame->last_seq, incoming->n_read );
  frame->stats.packets_received++;
  vrp_in_frame_alive(frame);

  vrp_hdr_pack_std(ack_buf, &ack_size); /* prepare ACK/NACK/... header */
  vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_SET, hdr->frame);
  vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_SINGLE_ACK, hdr->seq);

  if(frame->loss_cnt < 0)
    {
      int i;
      vrp_print("Internal state inconsistency\n");
      for(i=0; i<=frame->next_exp; i++)
        {
          vrp_print(" i=%d received=%d valid=%d waiting=%d required=%d\n",
                       i, frame->buffer[i].received, frame->buffer[i].valid,
                       frame->buffer[i].waiting,
                       frame->buffer[i].required);
        }
      vrp_print(" sliding window state: next_exp=%d waiting=%d\n",
                   frame->next_exp, frame->waiting);
      vrp_print(" loss_max=%d; cons_loss_max=%d\n",
                   frame->loss_max, frame->cons_loss_max);
      vrp_print(" loss_cnt=%d; total_loss=%d\n",
                   frame->loss_cnt, frame->total_loss);
      vrp_fatal("VRP internal error.\n");
    }

  if((frame->waiting <= hdr->seq) && (hdr->seq <= frame->last_seq))
    {
      /* in the window. Continue... */
      state = frame->buffer[hdr->seq];
      if(!state.waiting)
        { /* not outstanding */
          if(state.valid)
            { /* already validated? Uh?!? */
              if(state.received)
                {
                  /* message duplicated */
                  vrp_show(20, "*** duplicated packet=%i frame=%i\n",
                             hdr->seq, frame->frame_num);
                  frame->stats.duplicated++;
                }
              else
                {
                  /* loss was allowed - take it anyway */
                  vrp_show(20, "*** unexpected recovery packet=%i frame=%i\n",
                             hdr->seq, frame->frame_num);
                  frame->buffer[hdr->seq].received = 1;
                  frame->loss_cnt--;
                  frame->total_loss--;
                  assert(hdr->seq <= frame->last_seq);
                  assert(incoming->n_read - VRP_HDR_SIZE <= frame->data_per_packet);
                  memcpy(&frame->storage[frame->data_per_packet * hdr->seq],
                         incoming->recv_buf + VRP_HDR_SIZE,
                         incoming->n_read - VRP_HDR_SIZE);
                }
            }
          else
            {
              /* packet not yet validated, not outstanding: usual case */
              /* ** update losses counters */
              cons_loss=0;
              for(seq = frame->next_exp; seq < hdr->seq; seq++)
                {
                  /* One loop per lost packet.
                   * Usually not executed.
                   */
                  cons_loss++;
                  frame->loss_cnt++;
                  frame->total_loss++;
                  frame->stats.loss_detected++;
                  /*
                    if((seq < VRP_INCOMING_WINDOW_SIZE) ||
                    (frame->buffer[seq - VRP_INCOMING_WINDOW_SIZE].received))
                    {
                    frame->loss_cnt++;
                    }
                  */
                  vrp_show(70, "  cons_loss=%d ; frame->cons_loss_max=%d\n",
                             cons_loss, frame->cons_loss_max);
                  vrp_show(70, "  frame->loss_cnt=%d ; frame->loss_max=%d\n",
                             frame->loss_cnt, frame->loss_max);
                  if((frame->cons_loss_max && (cons_loss > frame->cons_loss_max)) ||
                     (frame->loss_cnt > frame->loss_max) ||
                     (frame->buffer[seq].required))
                    {
                      /* a rule is broken */
                      vrp_show(50, "-> broken rule for packet %i frame %i\n",
                                 seq, frame->frame_num);
                      vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_NACK, seq);
                      frame->stats.nack++;
                      cons_loss = 0;
                      frame->buffer[seq].waiting = 1;
                      frame->loss_cnt--; /* eventually, the packet is considered as received */
                    }
                  else
                    {
                      /* loss is allowed */
                      vrp_show(50, "-> allowed loss for packet %i frame %i\n",
                                 seq, frame->frame_num);
                      frame->stats.loss_allowed++;
                      vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_SINGLE_ACK, seq);
                      frame->stats.sack++;
                      frame->buffer[seq].valid = 1;
                    }
                }
              /*
                if((hdr->seq >= VRP_INCOMING_WINDOW_SIZE) &&
                (!frame->buffer[hdr->seq - VRP_INCOMING_WINDOW_SIZE].received))
                frame->loss_cnt--;
              */
              /* ** validate packet */
              frame->next_exp = hdr->seq + 1;
              frame->buffer[hdr->seq].received = 1;
              frame->buffer[hdr->seq].valid    = 1;
              assert(hdr->seq <= frame->last_seq);
              assert(incoming->n_read - VRP_HDR_SIZE <= frame->data_per_packet);
              memcpy(&frame->storage[frame->data_per_packet * hdr->seq],
                     incoming->recv_buf + VRP_HDR_SIZE,
                     incoming->n_read - VRP_HDR_SIZE);
            }
        }
      else
        { /* outstanding packet */
          /* don't check the rules, it's ok... */
          vrp_show(50, "Received outstanding packet=%i frame=%i\n",
                     hdr->seq, frame->frame_num);
          frame->stats.recovery++;
          frame->buffer[hdr->seq].received = 1;
          frame->buffer[hdr->seq].valid    = 1;
          frame->buffer[hdr->seq].waiting  = 0;
          assert(hdr->seq <= frame->last_seq);
          assert(incoming->n_read - VRP_HDR_SIZE <= frame->data_per_packet);
          memcpy(&frame->storage[frame->data_per_packet * hdr->seq],
                 incoming->recv_buf + VRP_HDR_SIZE,
                 incoming->n_read - VRP_HDR_SIZE);
          frame->total_loss--;
        }
      /*  the packet is in the window (continued) */
      /* ** update 'waiting', send ACKs */

      while((frame->buffer[frame->waiting].valid) &&
            (frame->waiting < frame->next_exp))
        {
          frame->waiting++;
          vrp_show(60, "Now waiting %i\n", frame->waiting);
          assert(frame->waiting <= frame->last_seq + 1);
        }
      /* if we get there, [waiting] not ACKed or waiting == next_exp */
      /* ** check end of frame */
      if((frame->waiting > frame->last_seq) && (frame->state.receiving))
        {
          frame->state.receiving = 0;
          frame->state.ready_for_delivery = 1;
        }
    }
  else if(hdr->seq > frame->last_seq)
    {
      vrp_warning("Received packet beyond frame boundary (received_seq=%d, last_sez=%d)\n",
                     hdr->seq, frame->last_seq);
    }
  else
    {
      vrp_show(40, "received packet no more waited for (seq=%d, waiting=%d, next_exp=%d)\n",
                 hdr->seq, frame->waiting, frame->next_exp);
      frame->stats.duplicated++;
    }
  /* global ACK, close and send packet */
  if(frame->waiting > 0)
    {
      vrp_show(60, "Send ACK #%i \n", frame->waiting - 1);
      vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_ACK, frame->waiting - 1);
      frame->stats.ack++;
    }
  vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
  vrp_incoming_hdr_send(incoming, ack_buf, ack_size);
}


static void
vrp_incoming_process_fragment(vrp_incoming_t incoming)
{
  vrp_hdr_t hdr;
  vrp_in_frame_t frame;
  char ack_buf[VRP_MAX_PACKET_SIZE];
  int ack_size;

  vrp_hdr_unpack(incoming->recv_buf, &hdr);
  vrp_show(60, "frame = %i, seq = %i, size = %i\n",
             hdr.frame, hdr.seq, incoming->n_read);
  /* find current frame */
  if(hdr.frame == VRP_NO_FRAME)
    {
      frame = NULL;
    }
  else
    {
      frame = vrp_in_frame_lookup(incoming, &incoming->from_addr, incoming->from_len, hdr.frame);
    }
  /* process the message */
  if(hdr.is_hdr)
    {
      vrp_incoming_process_hdr(incoming, frame);
    }
  else
    {
      if(frame == NULL)
        {
          /* missing frame header */
          vrp_hdr_pack_std(ack_buf, &ack_size);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_FRAME_MISSING_HDR, hdr.frame);
          vrp_hdr_pack_tag(ack_buf, &ack_size, VRP_TAG_LAST);
          vrp_incoming_hdr_send(incoming, ack_buf, ack_size);
          return;
        }
      else
        {
          vrp_incoming_process_data(incoming, frame, &hdr);
          if(frame->state.ready_for_delivery)
            {
              vrp_incoming_deliver_frame(incoming, frame);
              vrp_in_frame_destroy(frame);
            }
        }
    }
}


static void
vrp_incoming_deliver_frame(vrp_incoming_t incoming,
                           vrp_in_frame_t frame)
{
  struct vrp_in_buffer_s ibuf;
  int i,j;

  assert(frame->state.ready_for_delivery && ! frame->state.receiving);
  vrp_show(20, "---------------------------------------------------\n");
  vrp_show(20, "Frame %i completed! \n", frame->frame_num);
  vrp_show(20, "Residual loss %d (%d) packets (%i bytes) loss_cnt=%i\n",
             frame->total_loss, frame->loss_max,
             frame->total_loss * frame->data_per_packet,
             frame->loss_cnt);
  vrp_show(20, "packets_received=%d loss_detected=%d loss_allowed=%d\n",
             frame->stats.packets_received,
             frame->stats.loss_detected,
             frame->stats.loss_allowed);
  vrp_show(20, "recovery=%d ack=%d nack=%d sack=%d duplicated=%d\n",
             frame->stats.recovery,
             frame->stats.ack,
             frame->stats.nack,
             frame->stats.sack,
             frame->stats.duplicated);
  vrp_show(20, "---------------------------------------------------\n");

  ibuf.size = frame->data_size;
  ibuf.data = frame->storage;
  ibuf.loss_num = frame->total_loss;
  ibuf.loss_granularity = frame->data_per_packet;
  ibuf.loss_residual = frame->total_loss * frame->data_per_packet;
  ibuf.lost = (void**)vrp_malloc(sizeof(void*)*frame->total_loss);
  assert(frame->total_loss <= frame->last_seq);
  for(i=0, j=0; i<=frame->last_seq; i++)
    {
      if(!frame->buffer[i].received)
        {
          ibuf.lost[j] = ibuf.data + i*ibuf.loss_granularity;
          j++;
          assert(j <= ibuf.loss_num);
        }
    }

  if(incoming->frame_handler)
    {
      vrp_show(50, "calling user handler...\n");
      (*incoming->frame_handler)(&ibuf);
      vrp_show(50, "back from handler.\n");
    }
  else
    {
      vrp_warning("VRP: no handler registered for received frame.\n");
    }
  vrp_free(ibuf.lost);
}
