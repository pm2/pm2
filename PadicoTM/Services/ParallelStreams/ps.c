/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Parallel Streams communication links
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/VLink-internals.h>

#include <assert.h>

#include "ParallelStreams-internals.h"

static int vlink_ps_module_init(void);

PADICO_MODULE_DECLARE(ParallelStreams, vlink_ps_module_init, NULL, NULL);

/* ********************************************************* */

static void vlink_ps_init        (padico_vnode_t);
static int  vlink_ps_create      (padico_vnode_t);
static int  vlink_ps_bind        (padico_vnode_t);
static int  vlink_ps_listen      (padico_vnode_t);
static int  vlink_ps_connect     (padico_vnode_t);
static void vlink_ps_fullshutdown(padico_vnode_t);
static int  vlink_ps_close       (padico_vnode_t);
static void vlink_ps_send_post   (padico_vnode_t);
static void vlink_ps_recv_post   (padico_vnode_t);

static puk_component_t vlink_ps_component = NULL;

static const struct padico_vlink_driver_s vlink_ps_driver =
{
  .create       = &vlink_ps_create,
  .bind         = &vlink_ps_bind,
  .listen       = &vlink_ps_listen,
  .connect      = &vlink_ps_connect,
  .fullshutdown = &vlink_ps_fullshutdown,
  .close        = &vlink_ps_close,
  .send_post    = &vlink_ps_send_post,
  .recv_post    = &vlink_ps_recv_post,
  .dumpstat     = NULL
};

VLINK_CREATE_SPECIFIC_ACCESSORS(ps);

static int vlink_ps_reader_worker(padico_vnode_t);
static int vlink_ps_callback_acceptor(int fd, void*key);
static void ps_tune_socket(int fd);

static const uint8_t magic[4] = PS_MAGIC;

/* ********************************************************* */

static int vlink_ps_module_init(void)
{
  vlink_ps_component =
    puk_component_declare("ParallelStreams",
                          puk_component_provides("VLink", "vlink", &vlink_ps_driver),
                          puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                          puk_component_attr("num_ways", PS_DEFAULT_NUM_WAYS),
                          puk_component_attr("block_size", PS_DEFAULT_BLOCK_SIZE));
  return 0;
}


/* ********************************************************* */

static void vlink_ps_init(padico_vnode_t vnode)
{
  puk_context_t context = padico_vnode_get_context(vnode);
  const char*s_nw = puk_context_getattr(context, "num_ways");
  const char*s_bs = puk_context_getattr(context, "block_size");
  vobs_ps_activate(vnode);
  vobs_ps(vnode)->params.num_ways   = atoi(s_nw);
  vobs_ps(vnode)->params.block_size = atoi(s_bs);
  vobs_ps(vnode)->recv.block = 0;
  vobs_ps(vnode)->recv.pos   = 0;
  vobs_ps(vnode)->send.block = 0;
  vobs_ps(vnode)->send.pos   = 0;
  vobs_ps(vnode)->fds        = NULL;
  vobs_ps(vnode)->server_fd  = -1;
  padico_out(10, "init num_ways=%d; block_size=%d\n",
             vobs_ps(vnode)->params.num_ways,
             vobs_ps(vnode)->params.block_size);
}

static int vlink_ps_create(padico_vnode_t vnode)
{
  vlink_ps_init(vnode);
  int fd = padico_sysio_socket(AF_INET, SOCK_STREAM, 0);
  if(fd < 0)
    {
      int err = puk_abi_real_errno();
      return -err;
    }
  vobs_ps(vnode)->server_fd = fd;
  return 0;
}

static void vlink_ps_genaddr(padico_vnode_t vnode)
{
  vnode->binding.primary_len = sizeof(struct sockaddr_in);
  vnode->binding.primary = padico_malloc(vnode->binding.primary_len);
  vnode->binding.primary->sa_family = AF_INET;
  ((struct sockaddr_in*)vnode->binding.primary)->sin_addr = *padico_topo_host_getaddr(padico_topo_getlocalhost());
  ((struct sockaddr_in*)vnode->binding.primary)->sin_port = htons(0);
}

static int vlink_ps_bind(padico_vnode_t vnode)
{
  int rc = -1;
  int err;
  if(vnode->binding.autobind)
    {
      vlink_ps_genaddr(vnode);
      vnode->binding.autobind = 0;
    }
  puk_spinlock_acquire();
  rc = PUK_ABI_FSYS_REAL(bind)(vobs_ps(vnode)->server_fd, vnode->binding.primary,
                          vnode->binding.primary_len);
  err = puk_abi_real_errno();
  puk_spinlock_release();

  if(rc == 0)
    {
      puk_spinlock_acquire();
      rc = PUK_ABI_FSYS_REAL(getsockname)(vobs_ps(vnode)->server_fd,
                                     vnode->binding.primary,
                                     &vnode->binding.primary_len);
      puk_spinlock_release();
      if(rc != 0)
        {
          padico_warning("error in ::getsockname()\n");
        }
    }
  else
    {
      padico_out(20, "vnode=%p hfd=%d bind() rc=%d (%s)\n",
                 vnode, vobs_ps(vnode)->server_fd, rc, strerror(err));
      vnode_set_error(vnode, err);
    }
  return rc;
}

static int vlink_ps_listen(padico_vnode_t vnode)
{
  int rc = -1;
  puk_spinlock_acquire();
  rc = PUK_ABI_FSYS_REAL(listen)(vobs_ps(vnode)->server_fd, vnode->listener.backlog_size);
  puk_spinlock_release();
  vobs_ps(vnode)->server_io =
    padico_io_register(vobs_ps(vnode)->server_fd, PADICO_IO_EVENT_READ,
                       &vlink_ps_callback_acceptor, vnode);
  padico_io_activate(vobs_ps(vnode)->server_io);
  return rc;
}

static int vlink_ps_connect(padico_vnode_t vnode)
{
  int i;
  int rc = -1, err = -1;
  struct ps_connection_header_s hdr;
  vnode->binding.autobind = 1;
  vlink_ps_bind(vnode); /* force autobind */
  vobs_ps(vnode)->fds = padico_malloc(sizeof(int)*vobs_ps(vnode)->params.num_ways);
  for(i = 0; i < vobs_ps(vnode)->params.num_ways; i++)
    {
      memcpy(hdr.ps_magic, magic, sizeof(magic));
      hdr.num_way = htonl(i);
      rc = padico_sysio_socket(PF_INET, SOCK_STREAM, 0);
      err = puk_abi_real_errno();
      if(rc > 0)
        {
          vobs_ps(vnode)->fds[i] = rc;
          puk_spinlock_acquire();
          rc = PUK_ABI_FSYS_REAL(connect)(vobs_ps(vnode)->fds[i],
                                     vnode->remote.remote_addr,
                                     vnode->remote.remote_addrlen);
          err = puk_abi_real_errno();
          puk_spinlock_release();
          if(rc != -1)
            {
              padico_out(40, "connected way=%d; rc=%d\n", i, rc);
              ps_tune_socket(vobs_ps(vnode)->fds[i]);
              padico_sysio_out(vobs_ps(vnode)->fds[i], &hdr, sizeof(hdr));
            }
          else
            {
              padico_out(10, "connect() rc=%d errno=%d (%s)\n",
                         rc, err, strerror(err));
              break;
            }
        }
      else
        {
          vnode_set_error(vnode, err);
          return rc;
        }
    }
  padico_vsock_connector_finalize(vnode);
  return rc;
}

static void vlink_ps_fullshutdown(padico_vnode_t vnode)
{
  int i;
  if(vobs_ps(vnode)->fds != NULL)
    {
      for(i = 0; i < vobs_ps(vnode)->params.num_ways; i++)
        {
          puk_spinlock_acquire();
          PUK_ABI_FSYS_REAL(shutdown)(vobs_ps(vnode)->fds[i], 2);
          puk_spinlock_release();
        }
    }
  if(vobs_ps(vnode)->server_fd != -1)
    {
      puk_spinlock_acquire();
      PUK_ABI_FSYS_REAL(shutdown)(vobs_ps(vnode)->server_fd, 2);
      puk_spinlock_release();
    }
}

static int vlink_ps_close(padico_vnode_t vnode)
{
  int i;
  if(vobs_ps(vnode)->fds != NULL)
    {
      for(i = 0; i < vobs_ps(vnode)->params.num_ways; i++)
        {
          padico_sysio_close(vobs_ps(vnode)->fds[i]);
        }
      padico_free(vobs_ps(vnode)->fds);
    }
  if(vobs_ps(vnode)->server_fd != -1)
    {
      padico_sysio_close(vobs_ps(vnode)->server_fd);
    }
  vobs_ps_deactivate(vnode);
  return 0;
}


static void vlink_ps_send_post(padico_vnode_t vnode)
{
  int size = vnode->wbox.write_posted;
  void*ptr = vnode->wbox.write_buffer;
  while(size > 0)
    {
      int rc __attribute__((unused)) = -1;
      int size1 = vobs_ps(vnode)->params.block_size -
        vobs_ps(vnode)->send.pos; /* remaining size on next slot */
      int size2 = (size >= size1)?size1:size; /* size to use */
      padico_out(20, "sending block- size=%d; size1=%d; size2=%d; way=%d; pos=%d\n",
                 size, size1, size2,
                 vobs_ps(vnode)->send.block, vobs_ps(vnode)->send.pos);
      rc = padico_sysio_out(vobs_ps(vnode)->fds[vobs_ps(vnode)->send.block],
                            ptr, size2);
      padico_out(20, "block sent way=%d; rc=%d\n", vobs_ps(vnode)->send.block, rc);
      assert(rc == size2);
      ptr  += size2;
      size -= size2;
      vobs_ps(vnode)->send.pos = (vobs_ps(vnode)->send.pos + size2) %
        vobs_ps(vnode)->params.block_size;
      if(vobs_ps(vnode)->send.pos == 0)
        {
          vobs_ps(vnode)->send.block =
            (vobs_ps(vnode)->send.block + 1) %
            vobs_ps(vnode)->params.num_ways;
        }
    }
  assert(size == 0);
  vnode->wbox.write_done = vnode->wbox.write_posted;
  vobs_signal(vnode, vobs_attr_wbox);
}

static void vlink_ps_recv_post(padico_vnode_t vnode)
{
  vlink_ps_reader_worker(vnode);
}

static void ps_tune_socket(int fd)
{
  /* set sock opt NODELAY */
  int opt = 1;
  puk_spinlock_acquire();
  PUK_ABI_FSYS_REAL(setsockopt)(fd, SOL_TCP, TCP_NODELAY, &opt, sizeof(opt));
  puk_spinlock_release();
}

#if 0
static int vlink_ps_callback_reader(int fd, void*key)
{
  int again = 0;

  return again;
}
#endif

static int vlink_ps_callback_acceptor(int fd, void*key)
{
  int again = 0;
  struct sockaddr_in addr;
  socklen_t addrlen = sizeof(addr);
  padico_vnode_t vnode = key;
  padico_vnode_t newsock = NULL;
  int i, err;
  padico_out(20, "entering fd=%d; ways=%d; block_size=%d\n", fd,
             vobs_ps(vnode)->params.num_ways, vobs_ps(vnode)->params.block_size);
  vnode_lock(vnode);
  /* create new socket */
  newsock = padico_vnode_component_acceptor(vnode, padico_module_self());
  vlink_ps_init(newsock);
  padico_vsock_init(newsock, AF_INET, SOCK_STREAM, 0);
  vobs_ps(newsock)->fds =
    padico_malloc(sizeof(int)*vobs_ps(vnode)->params.num_ways);
  for(i = 0; i < vobs_ps(vnode)->params.num_ways; i++)
    {
      struct ps_connection_header_s hdr = { .ps_magic = { 'N', 'O', 'N', 'E' } };
      int new_fd = padico_sysio_accept(vobs_ps(vnode)->server_fd, (struct sockaddr*)&addr, &addrlen);
      err = padico_sysio_errno();
      vobs_ps(newsock)->fds[i] = new_fd;
      if(new_fd < 0)
        {
          padico_warning("accept()- fd=%d; %s\n", vobs_ps(vnode)->server_fd, strerror(err));
          vnode_set_error(vnode, err);
          return 0;
        }
      padico_sysio_in(new_fd, &hdr, sizeof(hdr));
      if(memcmp(hdr.ps_magic, magic, 4) != 0)
        {
          padico_warning("received connection with bad magic number i=%d (received: %c%c%c%c; expected: %c%c%c%c\n",
                         i, hdr.ps_magic[0],  hdr.ps_magic[1],  hdr.ps_magic[2],  hdr.ps_magic[3],
                         magic[0], magic[1], magic[2], magic[3]);
        }
      else
        {
          padico_out(40, "accepted way=%d (magic=%c%c%c%c)\n", i,
                     hdr.ps_magic[0],  hdr.ps_magic[1],  hdr.ps_magic[2],  hdr.ps_magic[3]);
        }
      assert(ntohl(hdr.num_way) == i); /* synchronous connection, for now */
      ps_tune_socket(new_fd);
    }
  /* local binding */
  vlink_ps_genaddr(newsock);
  /* remote binding, enqueue and signal */
  padico_vsock_acceptor_finalize(newsock, vnode, (struct sockaddr*)&addr, addrlen);
  vnode_unlock(vnode);
  return again;
}

static int vlink_ps_reader_worker(padico_vnode_t vnode)
{
  int again = 0;
  int rc, err;
  do
    {
      int size1 = vobs_ps(vnode)->params.block_size - vobs_ps(vnode)->recv.pos;
      int todo  = vnode->rbox.read_posted - vnode->rbox.read_done;
      int size2 = (todo >= size1)?size1:todo;
      padico_out(20, "receiving block- todo=%d; size1=%d; size2=%d; way=%d; pos=%d\n",
                 todo, size1, size2, vobs_ps(vnode)->recv.block, vobs_ps(vnode)->recv.pos);
      rc = padico_sysio_read(vobs_ps(vnode)->fds[vobs_ps(vnode)->recv.block],
                             vnode->rbox.read_buffer+vnode->rbox.read_done,
                             size2);
      err = padico_sysio_errno();
      padico_out(20, "block received way=%d; rc=%d; read_done=%ld\n",
                 vobs_ps(vnode)->recv.block, rc, puk_slong(vnode->rbox.read_done));
      if(rc > 0)
        {
          again = 1;
          vnode->rbox.read_done += rc;
          vobs_ps(vnode)->recv.pos = (vobs_ps(vnode)->recv.pos + rc) %
            vobs_ps(vnode)->params.block_size;
          if(vobs_ps(vnode)->recv.pos == 0)
            {
              vobs_ps(vnode)->recv.block = (vobs_ps(vnode)->recv.block + 1) %
                vobs_ps(vnode)->params.num_ways;
            }
        }
      else if(rc == 0)
        {
          padico_warning("End of file!\n");
          vnode->status.end_of_file = 1;
          again = 0;
        }
      else if(rc == -1 && (err == EINTR || err == EAGAIN))
        {
          padico_warning("EINTR!\n");
          again = 1;
        }
      else
        {
          padico_warning("Unkown error (%s)\n", strerror(err));
          again = 0;
        }
    }
  while(again && (vnode->rbox.read_done < vnode->rbox.read_posted));
  vobs_signal(vnode, vobs_attr_rbox);
  return 0;
}
