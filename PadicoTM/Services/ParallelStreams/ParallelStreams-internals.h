/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include "ParallelStreams.h"

/** Magic ID for connection establishment
 * ParallelStreams 2.0 
 */
#define PS_MAGIC {'P', 'S', '.', '2'}

/** The default block size for interleaving. Best result for
 *  block = k*1460 on Ethernet
 * @note should be a string, not an int.
 */
#define PS_DEFAULT_BLOCK_SIZE  "4380"

/** The default number of parallel streams.
 * @note should be a string, not an int.
 */
#define PS_DEFAULT_NUM_WAYS    "4"

struct ps_stream_state_s
{
  int pos;
  int block;
};

struct vlink_ps_specific_s
{
  struct ps_params_s params;
  struct ps_stream_state_s send;
  struct ps_stream_state_s recv;
  int*fds;
  padico_io_t*outs;
  padico_io_t*ins;
  int server_fd;
  padico_io_t server_io;
};


struct ps_connection_header_s
{
  uint8_t  ps_magic[4];
  uint32_t num_way;
};
