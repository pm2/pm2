/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico CORBA Boot Notifier
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>
#include <Padico/CORBA.h>
#include <Padico/COSNaming.h>

#include "boot-event.h"
#include "boot-notifier.h"

#include <string.h>
//#include <strstream>
#include <iostream>

static int  bn_init(void);
static int  bn_run(int argc, char**argv);
static void bn_stop(void);

PADICO_MODULE_DECLARE(CORBA_BootNotifier, bn_init, bn_run, bn_stop);

/* ********************************************************* */

static Padico::ORB_ptr             orb;
static ::Padico2::Boot_Event_ptr   be_server_ptr;
static padico_boot_notify_t        be_n;

static void proceed_one_command(char* arg, char*cmd, char*param, char** res)
{
  if (strcmp(cmd, arg)==0) {
    if (param!=NULL)
      *res = param;
    else
      padico_warning("Found argument %s without parameter -- ignoring\n", arg);
  }
}

static void parse_command_line(int argc, char** argv, char** ior, char **name, char **cookie)
{
  int i;

  for(i=0; i<argc; i++)
    {
      proceed_one_command(argv[i], "--BootEventIOR", argv[i+1], ior);
      proceed_one_command(argv[i], "--BootEventName", argv[i+1], name);
      proceed_one_command(argv[i], "--BootEventCookie", argv[i+1], cookie);
    }

}

static void clean_command_line(int* argc, char**argv)
{
  while(*argv) {
    if (strncmp(*argv, "--BootEvent", 11)==0) {
      //      padico_print("match: %s\n", *argv);
      char** s=argv+2;
      char** d=argv;
      (*argc) = (*argc) - 2;
      //      padico_print("new argc: %d\n", *argc);
      bool b;
      do {
        //      padico_print("copy: %p (%s) -> %p (%s)\n", s, *s, d, *d);
        *d=*s;
        b=*s;
        d++;
        s++;
      } while (b);
    } else
      argv++;
  }
}

padico_boot_notify_t padico_boot_notify_init(int* argc, char **argv)
{
  padico_boot_notify_t n = new padico_boot_notify_s();
  parse_command_line(*argc, argv, &n->server_ior, &n->server_name, &n->cookie);
  clean_command_line(argc, argv);
  return n;
}

::Padico2::Boot_Event_ptr
notify_prepare(padico_boot_notify_t n, CORBA::ORB_ptr orb, CosNaming::NamingContext_ptr ns)
{

  ::Padico2::Boot_Event_ptr server_ptr;

  if ((!n->server_ior) && (!n->server_name)) {
    padico_warning("Notify Helper: missing BootEventIOR or BootEventName!\n");
    return NULL;
  }

  if (n->server_ior)
    {
      try {
        // Conversion d'une chaine de charactere en reference CORBA
        CORBA::Object_ptr obj = orb->string_to_object(n->server_ior);
        // Conversion dans le type correct
        server_ptr = Padico2::Boot_Event::_narrow(obj);
      }
      catch (const CORBA::Exception &)
        {
          padico_warning("Notify prepare: CORBA Exception when narrowing BootEventIOR\n");
          return NULL;
        }
    }
  else
    {
      CORBA::Object_ptr obj;
      if (ns != NULL) {
        try
          {
            CosNaming::Name name;
            // resolve name
            name.length(1);
            name[0].id   = CORBA::string_dup(n->server_name);
            name[0].kind = CORBA::string_dup("");
            obj = ns->resolve(name);
          }
        catch (const CORBA::Exception &)
          {
            padico_warning("Notify prepare: CORBA Exception when resolving BootEventName: %s\n", n->server_name);
            return NULL;
          }
      } else {
        padico_warning("Notify prepare: No server IOR or name servive IOR to resolve servername: %s\n", n->server_name);
        return NULL;
      }

      try
        {
          // Conversion dans le type correct
          server_ptr = Padico2::Boot_Event::_narrow(obj);
        }
      catch (const CORBA::Exception &)
        {
          padico_warning("Notify Helper: CORBA Exception when narrowing BootEventName: %s\n", n->server_name);
          return NULL;
        }
    }
  return server_ptr;

}

int notify_do(padico_boot_notify_t n, ::Padico2::Boot_Event_ptr server_ptr)
{
  try
    {
      // notification
      server_ptr->done(n->cookie);
    }
  catch (const CORBA::Exception &)
    {
      padico_warning("Notify Helper: CORBA Exception when narrowing BootEventName: %s\n", n->server_name);
      return -1;
    }
  return 0;
}

// 0: success
int padico_boot_notify(padico_boot_notify_t pn, CORBA::ORB_ptr orb, CosNaming::NamingContext_ptr ns)
{
  ::Padico2::Boot_Event_ptr srv = notify_prepare(pn, orb, ns);
  if (srv)
    return notify_do(pn, srv);
  else
    return -1;
}

static int bn_init(void)
{
  padico_trace("CORBA Boot Notifier: init...\n");
  orb = Padico::ORB_init();

  be_n=padico_boot_notify_init(padico_getpargc(), padico_getargv());

  if (!be_n->server_ior)  be_n->server_ior  = (char*) padico_getattr("BootEventIOR");
  if (!be_n->server_name) be_n->server_name = (char*) padico_getattr("BootEventName");
  if (!be_n->cookie)      be_n->cookie      = (char*) padico_getattr("BootEventCookie");

  if ((!be_n->server_ior) && (!be_n->server_name)) {
    padico_warning("CORBA Boot Notifier: missing BootEventIOR or BootEventName!\n");
    return 0;
  }

  CosNaming::NamingContext_ptr root_context;
  try
    {
      // resolve name service
      CORBA::Object_var ns_ref = orb->resolve_initial_references ("NameService");
      root_context = CosNaming::NamingContext::_narrow (ns_ref);
    }
  catch(...)
    {
      padico_warning("CORBA Boot Notifier: init...NameService not found\n");
    }

  be_server_ptr=notify_prepare(be_n, Padico::the_orb, root_context);

  padico_trace("CORBA Boot Notifier: init...done\n");

  return 0;
}


static int bn_run(int argc, char**argv)
{
  padico_trace("CORBA Boot Notifier: run !\n");
  try {
    if (be_n->cookie==NULL)
      be_n->cookie="NO DATA";
    return notify_do(be_n, be_server_ptr);
  }
  catch (const CORBA::Exception &)
    {
      padico_warning("CORBA Exception when calling done on server!\n");
      return -1;
    }
  return 0;
}


static void bn_stop(void)
{
  padico_trace("CORBA Boot Notifier: stop\n");
}
