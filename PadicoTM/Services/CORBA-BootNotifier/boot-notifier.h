/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef CORBA_BOOT_NOTIFY_IS_DEFINED
#define CORBA_BOOT_NOTIFY_IS_DEFINED

typedef struct padico_boot_notify_s {
  char* server_ior;
  char* server_name;
  char* cookie;
} *padico_boot_notify_t;

padico_boot_notify_t padico_boot_notify_init(int* argc, char **argv);

// 0: sucess
int padico_boot_notify(padico_boot_notify_t pn, CORBA::ORB_ptr orb, CosNaming::NamingContext_ptr ns);

#endif
