/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file SocketFactory for socket over reversed SSH tunnel
 */

#include <Padico/PadicoTM.h>
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SocketFactory.h>

static int sfsrtun_module_init(void);
static void sfsrtun_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_srtun, sfsrtun_module_init, NULL, sfsrtun_module_finalize,
                      "SysIO", "Topology", "PadicoControl");

/** @defgroup SF_stun component: SocketFactory-stun- socket factory for reversed SSH tunnels
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sfsrtun_create(void*_instance, int family, int type, int protocol);
static int sfsrtun_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen,
                           padico_socketfactory_connector_t notifier, void*key);
static int sfsrtun_close(void*_instance);

/** 'SocketFactory' facet for SocketFactory-srtun
 * @ingroup SF_srtun
 */
static const struct padico_socketfactory_s sfsrtun_sf_driver =
  {
    .create  = &sfsrtun_create,
    .bind    = NULL,
    .listen  = NULL,
    .connect = &sfsrtun_connect,
    .close   = &sfsrtun_close,
    .caps    = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
  };

static void*sfsrtun_instantiate(puk_instance_t, puk_context_t);
static void sfsrtun_destroy(void*);

/** instanciation facet for SocketFactory-srtun
 * @ingroup SF_srtun
 */
static const struct puk_component_driver_s sfsrtun_component_driver =
  {
    .instantiate = &sfsrtun_instantiate,
    .destroy     = &sfsrtun_destroy
  };

/** instance of SocketFactory-srtun
 * @ingroup SF_srtun
 */
struct sfsrtun_instance_s
{
  int fd;
  padico_io_t io_connect;
  padico_socketfactory_connector_t connector;
  void*connector_key;
  pid_t pid;
  const char*ssh_path;
  const char*ssh_host;
  int ssh_port;
  const char*ssh_cipher;

/*   puk_instance_t instance; */
/*   puk_context_t context; */

  unsigned int server_id;
  padico_topo_host_t server;
};


static struct
{
  int port_offset;

  puk_iface_t iface_sf;
  puk_hashtable_t connections; /* hash: id -> status */
  unsigned int next_id;
  marcel_mutex_t connections_mtx;
  padico_tm_bgthread_pool_t pool;
} sfsrtun;

static struct
{
  int pid ;
} sfsrtun_server;

static int sfsrtun_callback_connector(int fd, void*key);



/* ********************************************************* */

static void sfsrtun_connect_server_end_handler(puk_parse_entity_t e);
static void sfsrtun_close_end_handler(puk_parse_entity_t e);

static int sfsrtun_module_init(void)
{
  puk_component_declare("SocketFactory_Reversed_SSH_Tunnel",
                        puk_component_provides("PadicoComponent", "component", &sfsrtun_component_driver),
                        puk_component_provides("SocketFactory", "sf", &sfsrtun_sf_driver),
                        puk_component_attr("ssh_host", NULL),
                        puk_component_attr("ssh_port", NULL),
                        puk_component_attr("ssh_path", "/usr/bin/ssh"),
                        puk_component_attr("ssh_cipher", "blowfish"));

  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "SocketFactory_Reversed_SSH_Tunnel:Connect",
                     .start_handler  = NULL,
                     .end_handler    = &sfsrtun_connect_server_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });

  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "SocketFactory_Reversed_SSH_Tunnel:Close",
                     .start_handler  = NULL,
                     .end_handler    = &sfsrtun_close_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });

  /* Init sfsrtun */
  sfsrtun.iface_sf = puk_iface_lookup("SocketFactory");
  sfsrtun.connections = puk_hashtable_new_int();
  sfsrtun.next_id = 1;
  marcel_mutex_init(&sfsrtun.connections_mtx, 0);
  sfsrtun.port_offset = 0;
  sfsrtun.pool = padico_tm_bgthread_pool_create("SocketFactory_Reversed_SSH_Tunnel");

  sfsrtun_server.pid = -1;
  return 0;
}

static void sfsrtun_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfsrtun.pool);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for SocketFactory_Reversed_SSH_Tunnel */

static void*sfsrtun_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct sfsrtun_instance_s*instance = padico_malloc(sizeof(struct sfsrtun_instance_s));
  instance->fd         = -1;
  instance->pid        = -1;
  instance->io_connect = NULL;
  instance->connector  = NULL;
  instance->ssh_path   = puk_context_getattr(context, "ssh_path");
  instance->ssh_cipher = puk_context_getattr(context, "ssh_cipher");
  instance->ssh_host   = puk_context_getattr(context, "ssh_host");
  const char*ssh_port  = puk_context_getattr(context, "ssh_port");
  instance->ssh_port   = atoi(ssh_port) + sfsrtun.port_offset++;

/*   instance->instance    = assembly; */
/*   instance->context     = context; */
  instance->server = NULL;
  instance->server_id   = 0;


  padico_out(5, "path=%s; host=%s; port=%d; cipher=%s- done.\n",
             instance->ssh_path, instance->ssh_host, instance->ssh_port, instance->ssh_cipher);
  return instance;
}

static void sfsrtun_destroy(void*_instance)
{
  struct sfsrtun_instance_s*instance = _instance;

  sfsrtun_close(_instance);
  if(instance->pid != -1) {
    int rc = -1, err = -1;
    puk_spinlock_acquire();
    rc = kill(instance->pid, SIGTERM);
    err = puk_abi_real_errno();
    puk_spinlock_release();
    instance->pid = -1;
    if(rc) {
      padico_warning("kill()- errno=%d (%s).\n", err, strerror(err));
    }
  }
  padico_free(instance);
  padico_out(40, "done.\n");
}


/* ********************************************************* */
/* *** 'SocketFactory' for SocketFactory_Reversed_SSH_Tunnel */

static int sfsrtun_create(void*_instance, int family, int type, int protocol)
{
  struct sfsrtun_instance_s*instance = _instance;
  int fd  = padico_sysio_socket(family, type, protocol);
  int err = padico_sysio_errno();
  assert(family == AF_INET);
  assert(type == SOCK_STREAM);
  padico_out(10, "family=%d\n", family);
  if(fd > -1) {
    err = 0;
    instance->fd = fd;
    instance->io_connect = padico_io_register(fd, PADICO_IO_EVENT_WRITE, &sfsrtun_callback_connector, instance);
  }
  padico_out(40, "fd=%d; err=%d\n", fd, err);
  return -err;
}


/* Checks whether this node is the server, if not sends a message to the server */
static int sfsrtun_launch_server(struct sfsrtun_instance_s*instance, struct sockaddr_in*inaddr)
{
  padico_out(50, "Launch server\n");

  if (instance->server == NULL) {
    padico_warning("Error: instance->server == NULL.\n");
    return -1;
  }
  const char*server_host = padico_topo_host_getname(instance->server);
  padico_out(40, "*** server host=%s\n", server_host);


  const int ssh_tunnel_rport = ntohs(inaddr->sin_port);
  padico_string_t s_launch = padico_string_new();
  padico_out(50, "ssh tunnel rport: %d\n", ssh_tunnel_rport);
  padico_out(50, "ssh tunnel port: %d\n", instance->ssh_port);
  padico_out(50, "ssh tunnel host: %s\n", instance->ssh_host);
  padico_out(50, "ssh tunnel server: %s\n", server_host);

  padico_out(20, "init server side (server node=%s, client port=%d, server port=%d)...\n",
             server_host, instance->ssh_port, ssh_tunnel_rport);

  padico_string_catf(s_launch,"<SocketFactory_Reversed_SSH_Tunnel:Connect clientHost=\"%s\" clientPort=\"%d\" serverPort=\"%d\" cipher=\"%s\" sshPath=\"%s\"/>" ,
                     instance->ssh_host, instance->ssh_port, ssh_tunnel_rport, instance->ssh_cipher, instance->ssh_path);

  padico_req_t req = padico_control_send(padico_topo_host_getnode(instance->server), padico_string_get(s_launch));
  padico_rc_t rc = padico_tm_req_wait(req);
  padico_string_delete(s_launch);

  int err = padico_rc_iserror(rc);
  if(err != 0) {
    padico_warning("Error: remote connection for host=%s failed: %s.\n",
                   server_host, padico_rc_gettext(rc));
    err = -1;
  } else  {
    char*s_id = padico_rc_gettext(rc);
    instance->server_id = atoi(s_id);
    padico_out(10, "launched reversed tunnel on node=%s, id=%d\n",
               server_host, instance->server_id);
  }
  padico_rc_delete(rc);

  return err;
}



static int sfsrtun_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen,
                          padico_socketfactory_connector_t notifier, void*key)
{
  int rc = -1;
  int err = -1;
  struct sfsrtun_instance_s*instance = _instance;
  struct sockaddr_in inaddr;
  int inaddrlen = sizeof(inaddr);
  int nodelay = 1;

  padico_out(50, "::connect()\n");
  memcpy(&inaddr, addr, sizeof(struct sockaddr_in));
  inaddr.sin_port = htons(instance->ssh_port);
  inet_aton("127.0.0.1", &inaddr.sin_addr);

  /* Resolve server name */
  padico_topo_host_t padico_server_host = padico_topo_host_resolve_byinaddr(&(((struct sockaddr_in*)addr)->sin_addr));
/*   instance->server = padico_server_host; */
  const char*server_host = padico_topo_host_getname(padico_server_host);
  //  instance->server = padico_topo_getnodebyname(server_host);
  instance->server = padico_topo_host_resolvebyname(server_host);

  if(padico_server_host == NULL) {
    padico_warning("connect: host=%s not found\n", server_host);
    return -1;
  } else
    padico_out(40, "server host=%s\n", server_host);

  /* Launch server */
  rc = sfsrtun_launch_server(instance, (struct sockaddr_in*)addr);
  if(rc == -1) {
    padico_warning("Problem while launching server\n");
    return rc;
  }


  /* Wait for socket to open */
  rc  = PUK_ABI_FSYS_WRAP(setsockopt)(instance->fd, SOL_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay));
  err = __puk_abi_wrap__errno;
  if(rc)
    goto exit;
 retry:
  rc  = PUK_ABI_FSYS_WRAP(connect)(instance->fd, &inaddr, inaddrlen);
  err = __puk_abi_wrap__errno;
  padico_out(50, "::connect() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
  if(rc == 0 || (rc == -1 && err == EINPROGRESS))
    {
      err = EINPROGRESS;
      instance->connector     = notifier;
      instance->connector_key = key;
      padico_io_activate(instance->io_connect);
      padico_out(60, "callback_connector() registered\n");
    }
  else if(rc == -1 && (err == ECONNREFUSED || err == EINTR))
    {
/*       int status = -1; */
/*       rc = waitpid(instance->pid, &status, WNOHANG); */
/*       if(rc == instance->pid) */
/*      { */
/*        padico_warning("SSH-Reversed-Tunnel: ssh error- status=%d.\n", status); */
/*        instance->pid = -1; */
/*        err = ENETUNREACH; /\** @todo check what is the best errno when ssh fails. *\/ */
/*        goto exit; */
/*      } */
/*       else */
/*      { */
          padico_out(6, "::connect()- tunnel not ready (%s).\n", strerror(err));
          padico_tm_msleep(50);
          goto retry;
/*      } */
    }
  else
    {
      padico_warning("connect() error while connecting to ssh- errno=%d (%s)\n", err, strerror(err));
    }
  padico_out(40, "err=%d\n", err);
 exit:
  return -err;
}

static int sfsrtun_close(void*_instance)
{
  struct sfsrtun_instance_s*instance = _instance;
  int rc = -1;
  if(instance->io_connect) {
    padico_io_deactivate(instance->io_connect);
    padico_io_unregister(instance->io_connect);
    instance->io_connect = NULL;
  }
  if(instance->fd != -1) {
    rc = PUK_ABI_FSYS_WRAP(close)(instance->fd);
    int err __attribute__((unused)) = __puk_abi_wrap__errno;
    padico_out(50, "::close() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
    instance->fd = -1;
  }

  if(instance->server_id != 0) {
      padico_out(10, "closing server=%s id=%d.\n", padico_topo_host_getname(instance->server), instance->server_id);
      padico_string_t s_close = padico_string_new();
      padico_string_catf(s_close,"<SocketFactory-Reversed-SSH-Tunne:Close id=\"%d\"/>", instance->server_id);

      /* If client == server, then we do not need to do anything, ssh tunnel will be killed by destroy() */
      if(instance->server == padico_topo_getlocalhost())
        puk_xml_parse_buffer(padico_string_get(s_close), strlen(padico_string_get(s_close)), PUK_TRUST_CONTROL);
      /* If client != server, then we need to send a message */
      else
                  padico_control_send_oneway(padico_topo_host_getnode(instance->server), padico_string_get(s_close));
      padico_string_delete(s_close);
  }
  padico_out(10, "closed.\n");

  return rc;
}

/* ********************************************************* */

static void*sfsrtun_connector_finalizer(void*_instance)
{
  struct sfsrtun_instance_s*instance = _instance;
  (*instance->connector)(instance->connector_key, instance->fd);
  return NULL;
}

static int sfsrtun_callback_connector(int fd, void*key)
{
  int again = 0;
  struct sfsrtun_instance_s*instance = key;
  padico_out(40, "fd=%d\n", fd);
  assert(fd == instance->fd);
  padico_tm_bgthread_start(sfsrtun.pool, &sfsrtun_connector_finalizer, instance,
                           "SocketFactory_Reversed_SSH_Tunnel:sfsrtun_connector_finalizer");
  return again;
}


/**************************************************************/
/**  Server */
static void sfsrtun_server_set_error(puk_parse_entity_t e, char*error)
{
  padico_warning("%s\n", error);
  padico_rc_t rc = padico_rc_error("Error: %s", error);
  puk_parse_set_rc(e, rc);
}


static void sfsrtun_close_end_handler(puk_parse_entity_t e)
{
  const char*s_id = puk_parse_getattr(e, "id");
  const unsigned long id = atoi(s_id);
  padico_out(50, "close request for id=%lu.\n", id);
  marcel_mutex_lock(&sfsrtun.connections_mtx);
  void*_status = puk_hashtable_lookup(sfsrtun.connections, (void*) id);
  if(_status)
    puk_hashtable_remove(sfsrtun.connections, (void*) id);
  marcel_mutex_unlock(&sfsrtun.connections_mtx);
  if(_status)
    sfsrtun_destroy((struct sfsrtun_instance_s*) _status);
}


/* connection from gateway to the server socket */
static void sfsrtun_connect_server_end_handler(puk_parse_entity_t e)
{
  /* Retrieve parameters */
  char error[64] = {0};
  const char*s_client_host = puk_parse_getattr(e, "clientHost");
  const char*s_client_port = puk_parse_getattr(e, "clientPort");
  const char*s_server_port = puk_parse_getattr(e, "serverPort");
  const char*s_ssh_cipher  = puk_parse_getattr(e, "cipher");
  const char*s_ssh_path    = puk_parse_getattr(e, "sshPath");
  assert(s_server_port && s_client_host && s_client_port && s_ssh_cipher && s_ssh_path);
  padico_out(20, "start server reversed tunnel: client host=%s client port=%s server port=%s..\n",
             s_client_host, s_client_port, s_server_port);

  /* Check client existence */
  padico_topo_host_t padico_client_host = padico_topo_host_resolvebyname(s_client_host);
  if (padico_client_host == NULL) {
    sprintf(error, "client host=%s not found", s_client_host);
    sfsrtun_server_set_error(e, error);
    return;
  }

  // TUNNEL
  pid_t pid = -1;
  int rc = -1, err = -1;
  if(sfsrtun_server.pid == -1) {
    puk_spinlock_acquire();
    pid = fork();
    err = puk_abi_real_errno();
    puk_spinlock_release();
    if(pid == -1) {
      padico_warning("fork() failed!\n");
    }
    else if(pid == 0) {
      /* Get server hostname */
/*       struct in_addr server_host; */
/*       inet_aton("127.0.0.1", &server_host);  */
/*       const char*s_server_host = padico_strdup(inet_ntoa(server_host)); */

      /* build ssh reversed tunnel string */
/*       padico_topo_node_getname(padico_topo_getlocalnode()); */
/*       struct in_addr inaddr; */
/*       inet_aton("127.0.0.1", &inaddr);  */
/*       const char*ssh_tunnel_host = padico_strdup(inet_ntoa(inaddr)); */
      padico_string_t s_ssh_tunnel = padico_string_new();
/*       padico_string_printf(s_ssh_tunnel, "%d:%s:%d",  s_client_port, s_server_host, s_server_port); */
      padico_string_printf(s_ssh_tunnel, "%s:%s:%s",  s_client_port, padico_topo_hostname(),
                            s_server_port);

      // TODO: verifier s'il ne faut pas plustot l'ip

      const char*ssh_tunnel = padico_string_get(s_ssh_tunnel);
      const char*argv[] =
        {
          s_ssh_path,         /* path to ssh- default: /usr/bin/ssh */
          "-n",               /* no stdin */
          "-T",               /* no pseudo-tty */
          "-N",               /* no remote command */
          "-e", "none",       /* disable escape sequences */
          "-R", ssh_tunnel,   /* the reversed tunnel itself */
          "-c", s_ssh_cipher, /* cipher- default: blowfish (much faster than 3des) */
          s_client_host,      /* our gateway host */
          NULL                /* NULL-terminated array */
        };

      padico_print("ssh_path=%s; ssh_tunnel=%s; ssh_host=%s; cipher=%s\n",
                   s_ssh_path, ssh_tunnel, s_client_host, s_ssh_cipher);
      putenv("LD_LIBRARY_PATH=");
      puk_spinlock_acquire();
      puk_abi_real_errno() = 0;
      rc = PUK_ABI_PROC_REAL(execv)(s_ssh_path, (char**)argv);
      err = puk_abi_real_errno();
      puk_spinlock_release();
      padico_fatal("exec() failed for ssh- rc=%d; errno=%d (%s).\n", rc, err, strerror(err));
    } else {
      /* father: PadicoTM process */
      sfsrtun_server.pid = pid;
                padico_out(10, "father: pid that does not fork :>\n");
    }
  }
}
