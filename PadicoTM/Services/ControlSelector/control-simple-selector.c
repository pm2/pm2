/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @ingroup Control
 */


#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoControl.h>

static int control_simpleselector_init(void);
static void control_simpleselector_finalize(void);

PADICO_MODULE_DECLARE(ControlSimpleSelector, control_simpleselector_init, NULL, control_simpleselector_finalize,
                      "Topology", "NetSelector", "PadicoControl");

/** @defgroup ControlSimpleSelector component: ControlSimpoleSelector- simple selector for the control channel
 * @ingroup PadicoComponent
 */

static struct
{
  puk_hashtable_t table; /* hash: node -> controler instance */
  puk_component_t selector;
} cs_selector = { .selector = NULL };

static void css_event_listener(void*arg);

static puk_instance_t control_select_controler(padico_topo_node_t node);

/* ********************************************************* */

/** 'ControlSelector' facet for the ControlSimpleSelector component
 * @ingroup ControlSimpleSelector
 */
static const struct padico_control_selector_driver_s control_simpleselector_driver =
  {
    .router  = &control_select_controler
  };

/* ********************************************************* */

static int control_simpleselector_init(void)
{
  cs_selector.selector =
    puk_component_declare("ControlSimpleSelector",
                          puk_component_provides("ControlSelector", "selector", &control_simpleselector_driver));
  cs_selector.table = puk_hashtable_new_ptr();
  padico_control_event_subscribe(&css_event_listener);
  padico_control_plug_selector(cs_selector.selector);
  return 0;
}

static void control_simpleselector_finalize(void)
{
  padico_control_event_disable();
  padico_control_event_flush();
  padico_control_unplug_selector(cs_selector.selector);
  padico_control_event_flush();
  padico_control_event_unsubscribe(&css_event_listener);
  padico_control_event_flush();
  padico_control_event_enable();
  puk_component_destroy(cs_selector.selector);
  cs_selector.selector = NULL;
  puk_hashtable_delete(cs_selector.table, NULL);
}

/* ********************************************************* */
/* *** ControlSimpleSelector ******************************* */

/** @pre event channel "Control" disabled
 */
static puk_instance_t control_select_controler(padico_topo_node_t node)
{
  padico_out(40, "node=%s\n", padico_topo_node_getname(node));
  padico_control_event_disable();
  padico_control_event_flush();
  const puk_instance_t instance = puk_hashtable_lookup(cs_selector.table, node);
  padico_control_event_enable();
  return instance;
}

/* ********************************************************* */

static void css_event_listener(void*arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)arg;
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_INIT_CONTROLER:
      {
        const padico_topo_node_vect_t nodes = event->INIT_CONTROLER.nodes;
        const puk_instance_t controler = event->INIT_CONTROLER.controler;
        padico_out(40, "init node event- %d nodes; controler=%s\n",
                   padico_topo_node_vect_size(nodes), controler->component->name);
        padico_topo_node_vect_itor_t i;
        puk_vect_foreach(i, padico_topo_node, nodes)
          {
            const padico_topo_node_t node = *i;
            puk_hashtable_insert(cs_selector.table, node, controler);
            padico_out(40, "init node event- new node *%s* served by %s\n",
                       padico_topo_node_getname(node), controler->component->name);
          }
      }
      break;

    case PADICO_CONTROL_EVENT_NEW_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        const puk_instance_t controler = event->NODE.controler;
        assert(controler != NULL);
        if(puk_hashtable_lookup(cs_selector.table, node) == NULL)
          {
            padico_out(40, "node event- new node *%s* served by %s\n",
                       padico_topo_node_getname(node), controler->component->name);
            puk_hashtable_insert(cs_selector.table, node, controler);
          }
      }
      break;

    case PADICO_CONTROL_EVENT_DELETE_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        if(puk_hashtable_lookup(cs_selector.table, node) != NULL)
          {
            puk_hashtable_remove(cs_selector.table, node);
          }
      }
      break;

    case PADICO_CONTROL_EVENT_UNPLUG_CONTROLER:
      {
        puk_instance_t controler = event->UNPLUG_CONTROLER.controler;
        puk_hashtable_enumerator_t e = NULL;
      restart:
        e = puk_hashtable_enumerator_new(cs_selector.table);
        padico_topo_node_t node = NULL;
        puk_instance_t node_controler = NULL;
        puk_hashtable_enumerator_next2(e, &node, &node_controler);
        while(node)
          {
            if(node_controler == controler)
              {
                puk_hashtable_remove(cs_selector.table, node);
                puk_hashtable_enumerator_delete(e);
                /* we have modified the hashtable, enumerator is not consistent anymore */
                goto restart;
              }
            puk_hashtable_enumerator_next2(e, &node, &node_controler);
          }
        puk_hashtable_enumerator_delete(e);
      }
      break;

    default:
      break;
    }
}
