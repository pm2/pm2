/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM PSP module
 * @ingroup PSP
 */

#include <Padico/Puk.h>
#include <Padico/PM2.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>

#include "PSP.h"

static int padico_psp_module_init(void);

PADICO_MODULE_DECLARE(PSP, padico_psp_module_init, NULL, NULL);


/* ********************************************************** */

static int padico_psp_module_init(void)
{
  padico_out(30, "module_init()\n");

  puk_iface_register("PadicoSimplePackets");

  return 0;
}

/* ********************************************************** */

static uint32_t padico_psp_tag_hash(const void*key)
{
  const uint32_t a = (uint32_t)((long)key);
  return a;
}
static int padico_psp_tag_eq(const void*key1, const void*key2)
{
  const uint32_t a = (uint32_t)((long)key1);
  const uint32_t b = (uint32_t)((long)key2);
  return a == b;
}

void padico_psp_directory_init(struct padico_psp_directory_s*d)
{
  pmarcel_spin_init(&d->lock, 0);
  d->table = puk_hashtable_new(&padico_psp_tag_hash, &padico_psp_tag_eq);
  d->cache = NULL;
}

padico_psp_slot_t padico_psp_slot_insert(struct padico_psp_directory_s*d,
                                         padico_psp_handler_t handler, void*key,
                                         uint32_t tag, size_t h_size)
{
  pmarcel_spin_lock(&d->lock);
  padico_psp_slot_t slot = puk_hashtable_lookup(d->table, puk_long2ptr(tag));
  if(!slot)
    {
      slot = padico_malloc(sizeof(struct padico_psp_slot_s));
      *slot = (struct padico_psp_slot_s){ .handler = handler, .key = key, .tag = tag, .h_size = h_size, .ref_count = 1 };
      puk_hashtable_insert(d->table, puk_long2ptr(tag), slot);
    }
  else
    {
      assert(slot->handler == handler && slot->key == key);
      slot->ref_count++;
    }
  pmarcel_spin_unlock(&d->lock);
  return slot;
}

void padico_psp_slot_remove(struct padico_psp_directory_s*d, padico_psp_slot_t slot)
{
  pmarcel_spin_lock(&d->lock);
  slot->ref_count--;
  if(slot->ref_count <= 0)
    puk_hashtable_remove(d->table, puk_long2ptr(slot->tag));
  pmarcel_spin_unlock(&d->lock);
}

padico_psp_slot_t padico_psp_slot_lookup(struct padico_psp_directory_s*d, uint32_t tag)
{
  pmarcel_spin_lock(&d->lock);
  padico_psp_slot_t slot = (d->cache && (d->cache->tag == tag)) ? d->cache : NULL;
  while(!slot)
    {
      slot = puk_hashtable_lookup(d->table, puk_long2ptr(tag));
      if(!slot)
        {
          pmarcel_spin_unlock(&d->lock);
          sched_yield();
          pmarcel_spin_lock(&d->lock);
        }
    }
  d->cache = slot;
  pmarcel_spin_unlock(&d->lock);
  return slot;
}
