/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @ingroup NetSelector-basic
 */


#include <Padico/NetSelector.h>
#include "Ns-basic-internals.h"

PADICO_MODULE_HOOK(NetSelector_basic);


/* *** Debug *********************************************** */

#ifndef NDEBUG

static void ns_basic_print_entities(ns_entity_vect_t entity_vect)
{
  ns_entity_t*_n;
  puk_vect_foreach(_n, ns_entity, entity_vect)
    {
      ns_entity_t n = *_n;
      switch(n->kind)
        {
        case NS_ENTITY_NODE:
          padico_print("node: %s\n", n->node.name);
          break;
        case NS_ENTITY_HOST:
          padico_print("host: %s\n", n->host.name);
          break;
        case NS_ENTITY_GROUP:
          padico_print("group: %s\n", n->group.name);
          ns_basic_print_entities(&n->group.entities);
          break;
        default:
          padico_fatal("bad entity type.\n");
          break;
        }
    }
}

static void ns_basic_print_groups(puk_hashtable_t groups)
{
  puk_hashtable_enumerator_t e =
    puk_hashtable_enumerator_new(groups);
  char*group_name;
  ns_entity_t group;
  puk_hashtable_enumerator_next2(e, &group_name, &group);
  while(group)
    {
      padico_print("group: %s [root]\n", group->group.name);
      ns_basic_print_entities(&group->group.entities);
      puk_hashtable_enumerator_next2(e, &group_name, &group);
    }
}

static void ns_basic_rule_print_entity(ns_entity_t i)
{
  switch(i->kind)
    {
    case NS_ENTITY_NODE:
      padico_print("     -- node: %s\n", i->node.name?:"*");
      break;
    case NS_ENTITY_HOST:
      padico_print("     -- host: %s\n", i->host.name?:"*");
      break;
    case NS_ENTITY_GROUP:
      padico_print("     -- group: %s\n", i->group.name);
      break;
    default:
      padico_print("     -- [unknown kind]\n");
    }
}

static void ns_basic_print_rules(void)
{
  ns_rule_t rule;
  puk_hashtable_enumerator_t rb_enum =
    puk_hashtable_enumerator_new(nsdb.rulebuckets);
  struct ns_rule_bucket_s*bucket;
  ns_rule_list_t rules;
  puk_hashtable_enumerator_next2(rb_enum, (const void**)&bucket, (void**)&rules);
  while(bucket)
    {
      puk_list_foreach(ns_rule, rule, rules)
        {
          padico_print("***** rule- profile=%s; iface=%s\n",
                       bucket->profile, bucket->iface->name);
          padico_print("* target:\n");
          ns_target_vect_itor_t target;
          puk_vect_foreach(target, ns_target, &rule->targets)
            {
              switch(target->kind)
                {
                case NS_TARGET_CLIQUE:
                  padico_print("  -- clique:\n");
                  ns_basic_rule_print_entity(target->clique.entity);
                  break;
                case NS_TARGET_MULTIPARTITE:
                  {
                    ns_entity_t*_i;
                    padico_print("  -- multipartite:\n");
                    puk_vect_foreach(_i, ns_entity, target->multipartite.subsets)
                      {
                        ns_entity_t i = *_i;
                        ns_basic_rule_print_entity(i);
                      }
                  }
                  break;
                case NS_TARGET_UNKNOWN:
                  padico_print("  -- unknown\n");
                  break;
                default:
                  padico_fatal("bad target type %d in rule.\n", target->kind);
                  break;
                }
            }
          padico_print("* assembly:\n");
          padico_string_t s = puk_component_serialize(rule->assembly);
          padico_print("%s\n", padico_string_get(s));
          padico_string_delete(s);
          padico_print("***** rule end\n");
        }
      puk_hashtable_enumerator_next2(rb_enum, (const void**)&bucket, (void**)&rules);
    }
}

void ns_basic_debug_init(void)
{
  const char*debug = padico_getattr("NS_BASIC_DEBUG");
  const int show_all = debug?(strstr(debug, "all") != NULL):0;
  if(debug)
    {
      padico_print("***** debug NS_BASIC_DEBUG=%s\n", debug);
      if(show_all || strstr(debug, "nodes"))
        {
          puk_hashtable_enumerator_t e =
            puk_hashtable_enumerator_new(nsdb.nodes_by_name);
          char*node_name;
          ns_entity_t node;
          padico_print("*** Listing nodes ***\n");
          puk_hashtable_enumerator_next2(e, &node_name, &node);
          while(node_name)
            {
              padico_print("node: %s (%s)\n", node_name, node->node.name);
              puk_hashtable_enumerator_next2(e, &node_name, &node);
            }
        }
      if(show_all || strstr(debug, "clusters"))
        {
          padico_print("*** Listing groups ***\n");
          ns_basic_print_groups(nsdb.groups_by_name);
        }
      if(show_all || strstr(debug, "rules"))
        {
          padico_print("*** Listing rules ***\n");
          ns_basic_print_rules();
        }
    }
}

#endif /* NDEBUG */
