/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Ns-basic.h
 * @brief Internal structures for NS-basic
 * @ingroup NetSelector-basic
 */


#ifndef NS_BASIC_H
#define NS_BASIC_H

#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>


typedef struct ns_entity_s*ns_entity_t;

PUK_VECT_TYPE(ns_group, struct ns_group_s*);
PUK_VECT_TYPE(ns_entity, ns_entity_t);

struct ns_node_s
{
  const char*name;
  padico_topo_node_t node;
};

struct ns_host_s
{
  const char*name;
  padico_topo_host_t host;
};

struct ns_group_s
{
  const char*name;
  struct ns_entity_vect_s entities; /**< enclosed entities as described in the rules */
  struct ns_entity_vect_s leaves;   /**< flattened vector of all contained terminal entities */
};

struct ns_entity_s
{
  enum ns_entity_kind_e { NS_ENTITY_NONE, NS_ENTITY_NODE, NS_ENTITY_HOST, NS_ENTITY_GROUP } kind;
  union
  {
    struct ns_node_s node;
    struct ns_host_s host;
    struct ns_group_s group;
  };
  struct ns_entity_vect_s contained;
};

struct ns_target_s
{
  enum {
    NS_TARGET_NONE,          /**< not initialized */
    NS_TARGET_UNKNOWN,       /**< default assembly, when the node is unknown */
    NS_TARGET_CLIQUE,        /**< communication between all nodes of a set */
    NS_TARGET_MULTIPARTITE,  /**< communication between sets */
    NS_TARGET_ARROW          /**< _directed_ edge between two sets */
  } kind;
  union
  {
    struct
    {
      ns_entity_t entity;
    } clique;
    struct
    {
      ns_entity_vect_t subsets;
    } multipartite;
    struct
    {
      ns_entity_t tail;
      ns_entity_t head;
    } arrow;
  };
  const char*profile;
};
PUK_VECT_TYPE(ns_target, struct ns_target_s);

PUK_LIST_TYPE(ns_rule,
              puk_component_t assembly; /**< Serial assembly used */
              struct ns_target_vect_s targets;
              );

struct ns_rule_bucket_s
{
  const char* profile; /**< Profile matched by this set of rules */
  puk_iface_t iface;   /**< Iface matched by this rule */
};

struct padico_ns_database_s
{
  /* ** tables used for selection */
  puk_hashtable_t rulebuckets;    /**< rules hashed by bucket [ (iface,profile) -> rule_list ] */
  /* ** tables used during parsing */
  puk_hashtable_t nodes_by_name;  /**< list of nodes found in the config- hashed by nodename */
  puk_hashtable_t hosts_by_name;  /**< list of hosts found in the config- hashed by hostname */
  puk_hashtable_t groups_by_name; /**< list of groups found in the config */
};

/* ** internal status of NetSelector-basic */

/** the internal database of rules and nodes/hosts/groups */
extern struct padico_ns_database_s nsdb;

#define PADICO_NS_DEFAULT_PROFILE "default"

/** Init the parser */
__PUK_SYM_INTERNAL int ns_basic_parse_init(void);

#ifndef NDEBUG
__PUK_SYM_INTERNAL void ns_basic_debug_init(void);
#endif

__PUK_SYM_INTERNAL
puk_component_t ns_basic_assembly(padico_topo_node_t node1,
                                padico_topo_node_t node2,
                                const char* profile,
                                puk_iface_t iface);

__PUK_SYM_INTERNAL
puk_component_t ns_basic_host_assembly(padico_topo_host_t host,
                                     const char* profile,
                                     puk_iface_t iface);


#endif /* NS_BASIC_H */
