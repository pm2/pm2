/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file ns-basic.c
 * @brief Head of NS-basic module, a NS' strategy
 * @ingroup NetSelector-basic
 */

#include <Padico/Module.h>
#include "Ns-basic-internals.h"

static int ns_basic_init(void);
static void ns_basic_finalize(void);

PADICO_MODULE_DECLARE(NetSelector_basic, ns_basic_init, NULL, ns_basic_finalize);

static puk_component_t ns_basic_assembly_serial(padico_topo_node_t node1,
                                                padico_topo_node_t node2,
                                                const char* profile,
                                                puk_iface_t iface);

static puk_component_t ns_basic_assembly_host(padico_topo_host_t host,
                                              const char* profile,
                                              puk_iface_t iface,
                                              uint16_t port,
                                              int protocol);

/* ********************************************************* */

static const struct padico_netselector_driver_s ns_basic_driver =
  {
    .name    = "NetSelector-basic",
    .profile = "default",
    .serial  = &ns_basic_assembly_serial,
    .host    = &ns_basic_assembly_host
  };

/* ********************************************************* */
/* *** NetSelector entry points **************************** */

static puk_component_t ns_basic_assembly_serial(padico_topo_node_t node1,
                                                padico_topo_node_t node2,
                                                const char* profile,
                                                puk_iface_t iface)
{
  puk_component_t assembly = ns_basic_assembly(node1, node2, profile, iface);
  return assembly;
}

static puk_component_t ns_basic_assembly_host(padico_topo_host_t host,
                                              const char* profile,
                                              puk_iface_t iface,
                                              uint16_t port,
                                              int protocol)
{
  puk_component_t assembly = ns_basic_host_assembly(host, profile, iface);
  return assembly;
}

/* ********************************************************* */

static int ns_basic_config_init(void)
{
  int rc = 0;
  const char*conf = padico_getattr("NS_BASIC_CONF");
  if(conf && conf[0] != '/')
    {
      const char*padico_root = padico_getenv("PADICO_ROOT");
      padico_string_t s_conf = padico_string_new();
      padico_string_catf(s_conf, "%s/etc/%s.xml", padico_root, conf);
      conf = padico_string_get(s_conf);
    }
  if(conf != NULL)
    {
      padico_print("opening config: %s\n", conf);
      struct puk_parse_entity_s e = puk_xml_parse_file(conf, PUK_TRUST_CONTROL);
      padico_rc_t parse_rc = puk_parse_get_rc(&e);
      if(padico_rc_iserror(parse_rc))
        {
          padico_warning("error while reading config file: %s\n", padico_rc_gettext(parse_rc));
          rc = -1;
        }
#ifndef NDEBUG
      else
        {
          ns_basic_debug_init();
        }
#endif
    }
  else
    {
      padico_print("no default config file.\n");
    }
  return rc;
}

/* ********************************************************* */
/* *** Helper functions for rulebuckets hashtables ********* */

static uint32_t ns_rulebucket_hash(const void*key)
{
  const struct ns_rule_bucket_s*r = key;
  uint32_t z = puk_hash_pointer(r->iface) + puk_hash_string(r->profile);
  return z;
}

static int ns_rulebucket_eq(const void*key1, const void*key2)
{
  const struct ns_rule_bucket_s*r1 = key1;
  const struct ns_rule_bucket_s*r2 = key2;
  return ( (r1->iface == r2->iface) && (strcmp(r1->profile, r2->profile) == 0));
}

/* ********************************************************* */

int ns_basic_init(void)
{
  nsdb.rulebuckets    = puk_hashtable_new(&ns_rulebucket_hash, &ns_rulebucket_eq);
  nsdb.nodes_by_name  = puk_hashtable_new(&puk_hash_string, &puk_hash_string_eq);
  nsdb.hosts_by_name  = puk_hashtable_new(&puk_hash_string, &puk_hash_string_eq);
  nsdb.groups_by_name = puk_hashtable_new(&puk_hash_string, &puk_hash_string_eq);

  ns_basic_parse_init();
  padico_ns_selector_register(&ns_basic_driver);
  int rc = ns_basic_config_init();
  return rc;
}

void ns_basic_finalize(void)
{
  padico_ns_selector_unregister(&ns_basic_driver);
  puk_hashtable_delete(nsdb.rulebuckets, NULL); /* TODO */
  puk_hashtable_delete(nsdb.nodes_by_name, NULL); /* TODO */
  puk_hashtable_delete(nsdb.hosts_by_name, NULL); /* TODO */
  puk_hashtable_delete(nsdb.groups_by_name, NULL); /* TODO */
}
