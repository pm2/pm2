/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @remark This component heavily relies on 'hfd' for I/O.
 */

#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SysIO.h>
#include <Padico/SocketFactory.h>

static int vlink_socketfactory_module_init(void);

PADICO_MODULE_DECLARE(VLink_SocketFactory, vlink_socketfactory_module_init, NULL, NULL,
                      "VIO", "SysIO", "Topology");

/** @defgroup VLink_SF component: VLink_SocketFactory- VLink over socket factories
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int         vlink_sf_create      (padico_vnode_t);
static int         vlink_sf_bind        (padico_vnode_t);
static int         vlink_sf_connect     (padico_vnode_t);
static int         vlink_sf_listen      (padico_vnode_t);
static void        vlink_sf_fullshutdown(padico_vnode_t);
static int         vlink_sf_close       (padico_vnode_t);
static padico_rc_t vlink_sf_dumpstat    (padico_vnode_t);

/** 'VLink' facet of the VLink_SocketFactory component
 * @ingroup VLink_SF
 */
static const struct padico_vlink_driver_s vlink_sysio_driver =
  {
    .create       = &vlink_sf_create,
    .bind         = &vlink_sf_bind,
    .listen       = &vlink_sf_listen,
    .connect      = &vlink_sf_connect,
    .fullshutdown = &vlink_sf_fullshutdown,
    .close        = &vlink_sf_close,
    .dumpstat     = &vlink_sf_dumpstat,
    .send_post    = &vnode_hfd_write_post,
    .recv_post    = &vnode_hfd_read_post
  };

/** 'sf' component-specific per-VLink information
 * @ingroup VLink_SF
 */
struct vlink_sf_specific_s
{
  struct puk_receptacle_SocketFactory_s sf;
};
VLINK_CREATE_SPECIFIC_ACCESSORS(sf);

static void vlink_sf_connect_finalizer(void*_vnode, int status);
static void vlink_sf_accept_finalizer(void*key, int new_fd, const struct sockaddr*addr, socklen_t addrlen);


/* ********************************************************* */

static int vlink_socketfactory_module_init(void)
{
  puk_component_declare("VLink_SocketFactory",
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink",         "vlink", &vlink_sysio_driver),
                        puk_component_uses("SocketFactory", "sf"));
  return 0;
}

/* ********************************************************* */

/** @warning This function calls NetAcces/SysIO functions.
 * Do not call from inside a SysIO callback!
 */
static int vlink_sf_create(padico_vnode_t vnode)
{
  const puk_instance_t instance = padico_vnode_get_self_instance(vnode);
  vobs_sf_activate(vnode);
  puk_context_indirect_SocketFactory(instance, "sf", &vobs_sf(vnode)->sf);
  vnode_check(vnode, socket);
  struct puk_receptacle_SocketFactory_s*sf = &vobs_sf(vnode)->sf;
  int rc = (*sf->driver->create)(sf->_status, vnode->socket.so_family,
                                 vnode->socket.so_type, vnode->socket.so_protocol);
  vnode->socket.capabilities |= sf->driver->caps;
  padico_out(40, "vnode=%p; create() family=%d; type=%d; protocol=%d; rc=%d (%s)\n",
             vnode, vnode->socket.so_family, vnode->socket.so_type,
             vnode->socket.so_protocol, rc, strerror(-rc));
  if(rc < 0)
    {
      vnode_set_error(vnode, -rc);
      rc = -1;
      vobs_sf_deactivate(vnode);
    }
  return rc;
}

static padico_rc_t vlink_sf_dumpstat(padico_vnode_t vnode)
{
  padico_rc_t rc = NULL;
  if(vnode_is_sf(vnode))
    {
      rc =  padico_rc_msg("  <socket-factory fd=\"%d\"/>\n", vnode->hfd.fd);
    }
  return rc;
}

static int vlink_sf_bind(padico_vnode_t vnode)
{
  vnode_check(vnode, socket);
  vnode_check(vnode, binding);
  padico_out(30, "vnode=%p\n", vnode);
  if(vnode->binding.autobind)
    {
      vnode->binding.primary_len = sizeof(struct sockaddr_pa);
      vnode->binding.primary = padico_malloc(vnode->binding.primary_len);
      padico_sockaddr_init((struct sockaddr_pa*)vnode->binding.primary,
                           &vnode->binding.primary_len, AF_PADICO_AUTO);
      vnode->binding.autobind = 0;
    }
  assert(vnode->binding.primary != NULL);
  struct puk_receptacle_SocketFactory_s*sf = &vobs_sf(vnode)->sf;
  int rc = (*sf->driver->bind)(sf->_status,  vnode->binding.primary, &vnode->binding.primary_len);
  if(rc < 0)
    {
      padico_out(20, "vnode=%p bind() rc=%d (%s)\n", vnode, rc, strerror(-rc));
      vnode_set_error(vnode, -rc);
      rc = -1;
    }

  return rc;
}

static int vlink_sf_listen(padico_vnode_t vnode)
{
  const int backlog = vnode->listener.backlog_size;
  const struct padico_vaddr_s*src_addr = vnode->listener.src_addr;
  padico_out(50, "backlog=%d\n", backlog);
  vnode_check(vnode, socket);
  assert(vnode_is_sf(vnode));
  struct puk_receptacle_SocketFactory_s*sf = &vobs_sf(vnode)->sf;
  int rc = (*sf->driver->listen)(sf->_status, backlog, src_addr?src_addr->addr:NULL, src_addr?src_addr->addrlen:0,
                                 &vlink_sf_accept_finalizer, vnode);
  if(rc < 0)
    {
      vnode_set_error(vnode, -rc);
      rc = -1;
    }
  return rc;
}

static void vlink_sf_fullshutdown(padico_vnode_t vnode)
{
  int rc = -1, err = 0;
  if(vnode->content.hfd)
    {
      const padico_io_t io_read = vnode->hfd.io_read;
      vnode->hfd.io_read = NULL;
      const padico_io_t io_write = vnode->hfd.io_write;
      vnode->hfd.io_write = NULL;
      padico_out(60, "vnode=%p hfd=%d\n", vnode, vnode->hfd.fd);
      assert(vnode->content.hfd);
      if(io_read)
        {
          vnode_unlock(vnode);
          padico_io_deactivate(io_read);
          padico_io_unregister(io_read);
          vnode_lock(vnode);
        }
      if(io_write)
        {
          vnode_unlock(vnode);
          padico_io_deactivate(io_write);
          padico_io_unregister(io_write);
          vnode_lock(vnode);
        }
      rc  = PUK_ABI_FSYS_WRAP(shutdown)(vnode->hfd.fd, 2);
      err = (rc != 0) ? __puk_abi_wrap__errno : 0;
      vnode_set_error(vnode, err);
    }
}

static int vlink_sf_close(padico_vnode_t vnode)
{
  int rc = 0;
  if(vnode->content.listener || vnode->content.connect || !vnode->content.hfd)
    {
      vnode_check(vnode, socket);
      assert(vnode_is_sf(vnode));
      struct puk_receptacle_SocketFactory_s*sf = &vobs_sf(vnode)->sf;
      rc = (*sf->driver->close)(sf->_status);
      if(vnode->content.hfd)
        {
          vnode_deactivate(vnode, hfd);
        }
    }
  else if(vnode->content.hfd)
    {
      vnode_deactivate(vnode, hfd);
      rc = vnode_hfd_close(vnode);
    }
  return rc;
}

/** fills-in the 'primary' binding for an hfd socket
 * used by: connect_finalize(), callback_acceptor()
 * @note should be removed
 */
static void vlink_sf_getsockname(padico_vnode_t vnode)
{
  struct sockaddr*addr;
  socklen_t addrlen = 0;
  int rc = -1, err = 0;

  vnode_check(vnode, socket);
  vnode_check(vnode, hfd);
  padico_out(50, "vnode=%p\n", vnode);
  switch(vnode->socket.so_family)
    {
    case AF_INET:
      addrlen = sizeof(struct sockaddr_in);
      break;
    case AF_INET6:
      addrlen = sizeof(struct sockaddr_in6);
      break;
    case AF_UNIX:
      addrlen = sizeof(struct sockaddr_un);
      break;
    default:
      padico_warning("getsockname() on a socket in an unknown domain (domain=%d)\n",
                     vnode->socket.so_family);
    }
  addr = padico_malloc(addrlen);
  rc  = PUK_ABI_FSYS_WRAP(getsockname)(vnode->hfd.fd, addr, &addrlen);
  err = __puk_abi_wrap__errno;
  if(rc)
    {
      padico_warning("error in getsockname()- errno=%d (%s)\n", err, strerror(err));
    }
  else
    {
      vnode_activate(vnode, binding);
      vnode->binding.primary = addr;
      vnode->binding.primary_len = addrlen;
    }
  padico_out(50, "vnode=%p- exit\n", vnode);
}

static int vlink_sf_connect(padico_vnode_t vnode)
{
  vnode_check(vnode, socket);
  assert(vnode_is_sf(vnode));
  struct puk_receptacle_SocketFactory_s*sf = &vobs_sf(vnode)->sf;
  int rc = (*sf->driver->connect)(sf->_status, vnode->remote.remote_addr, vnode->remote.remote_addrlen,
                                  &vlink_sf_connect_finalizer, vnode);
  vnode_set_error(vnode, -rc);
  if(rc == -EINPROGRESS)
    {
      padico_out(60, "errno=EINPROGRESS\n");
    }
  else if(rc < 0)
    {
      padico_out(10, "errno=%d (%s)\n", -rc, strerror(-rc));
    }
  return (rc==0)?0:-1;
}


/* ** Notification functions ******************************* */

/** Function called upon connection establishment succes as a client.
 * @note asserts vnode is unlocked */
static void vlink_sf_connect_finalizer(void*_vnode, int status)
{
  padico_vnode_t vnode = _vnode;
  int fd = status;
  assert(status >= 0);
  /* detect race condition- close() may have been call on this socket in-between */
  if(vnode->content.socket && vnode->content.remote)
    {
      vnode_hfd_create(vnode, fd, O_RDWR);
      vlink_sf_getsockname(vnode);
      vnode_lock(vnode);
      vnode->status.writable = 1;
      padico_vsock_connector_finalize(vnode);
      vnode_unlock(vnode);
    }
}


/** Function called upon connection establishment succes as a server.
 * @note asserts vnode is unlocked */
static void vlink_sf_accept_finalizer(void*_vnode, int status, const struct sockaddr*addr, socklen_t addrlen)
{
  padico_vnode_t vnode = _vnode;
  vnode_lock(vnode);
  /* detect race condition- close() may have been call on this socket in-between */
  if(vnode->content.socket && vnode->content.listener)
    {
      vnode_check(vnode, listener);
      while(padico_vnode_queue_full(vnode->listener.backlog))
        {
          vobs_wait(vnode);
        }
      if(status < 0)
        {
          const int err = -status;
          vnode_set_error(vnode, err);
          padico_out(40, "error errno=%d (%s)\n", err, strerror(err));
        }
      else
        {
          const int new_fd = status;
          padico_out(40, "vnode=%p new_fd=%d\n", vnode, new_fd);
          padico_vnode_t newsock = padico_vnode_component_acceptor(vnode, padico_module_self());
          padico_vsock_init(newsock, vnode->socket.so_family,
                            vnode->socket.so_type, vnode->socket.so_protocol);
          /* release the lock since vnode_hfd_* functions may take NetAccess SysIO lock,
           * causing a lock order inversion ifever a callback arrives at the same time.
           */
          vnode_unlock(vnode);
          vnode_hfd_create(newsock, new_fd, O_RDWR);
          vlink_sf_getsockname(newsock);
          vnode_lock(vnode);
          /* remote binding, enqueue and signal */
          padico_vsock_acceptor_finalize(newsock, vnode, addr, addrlen);
        }
    }
  vnode_unlock(vnode);
}
