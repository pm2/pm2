/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <Padico/Puk.h>

PUK_MOD_AUTO_DEP(PMIx, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME PMIx

#ifdef __cplusplus
extern "C" {
#endif

  int padico_pmix_getrank(void);
  int padico_pmix_getsize(void);
  void padico_pmix_barrier(void);

  /** PMIx supports wide url without arbitrary bound */
#define PADICO_PMIx_WIDE_URL_SUPPORT 1

  void padico_pmix_abort(const char*msg);
  void padico_pmix_kvs_fence(void);

  int padico_pmix_kvs_publish(const char*key, const char*value);
  char*padico_pmix_kvs_lookup(const char*key, int from);

#ifdef __cplusplus
} /* extern "C" */
#endif
