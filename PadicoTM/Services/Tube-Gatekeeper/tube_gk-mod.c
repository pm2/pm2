/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico Tube Gatekeeper
 * @ingroup TubeGatekeeper
 */

/** @defgroup TubeGatekeeper Service: Tube-Gatekeeper -- server-side for named pipes remote control
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>
#include <errno.h>

#include "padico-run_tube.h"

static int tube_gk_init(void);
static void tube_gk_stop(void);

static int sockfd = 0;
static padico_io_t in_io = NULL;

PADICO_MODULE_DECLARE(Tube_Gatekeeper, tube_gk_init, NULL, tube_gk_stop);


/* ******************************************* */

static int write_tube(int fd, void*data, size_t size)
{
  padico_out(30,"** WRITE (%i)**\n", size);
  int ret = padico_sysio_out(fd, data, size);
  if( ret < size)
    padico_fatal("Write error: %s\n", strerror(__puk_abi_wrap__errno));
  return ret;
}

static void write_char_tube(int fd, const char*string)
{
  if(string != NULL)
    {
      int len = strlen(string)+1;
      int ret = 0;
      ret += write_tube(fd, &len, sizeof(int));
      if( ret < 0)
        padico_fatal("Write error: %s\n", strerror(__puk_abi_wrap__errno));
      ret += write_tube(fd, (void*)string, len);
      if( ret < 0)
        padico_fatal("Write error: %s\n", strerror(__puk_abi_wrap__errno));
    } else {
      int len = 0;
      write_tube(fd, &len, sizeof(int));
    }
}

static int read_tube(int fd, void*data, size_t size)
{
  padico_out(30, "** READ (%i) **\n", size);
  int ret = padico_sysio_in(fd, data, size);
  if(ret < size)
    padico_fatal("Read error: %s\n", strerror(__puk_abi_wrap__errno));
  return ret;
}

static void read_char_tube(int fd, char**ptr)
{
  int size = 0;
  int ret = read_tube(fd, &size, sizeof(int));
  if(size > 0)
    {
      char * tmp = padico_malloc(size*sizeof(char));
      read_tube(fd, tmp, size*sizeof(char));
      *ptr = tmp;
    }
  if(ret < 0)
    padico_fatal("Read char error: %s\n", strerror(__puk_abi_wrap__errno));
}

static void send_rc(int fd, padico_rc_t rc)
{
  if(rc != NULL)
    {
      write_tube(fd, &(rc->rc), sizeof(int));
      write_char_tube(fd, (const char*)rc->msg);
    } else {
      int rc = 0;
      char*msg = NULL;
      write_tube(fd, &rc, sizeof(int));
      write_char_tube(fd, (const char*)msg);
    }
}

static int answer_cmd(int cmd, int fd)
{
  padico_out(30,"** Callback call on fd %i: %i **\n", fd, cmd);
  if(fd > 0)
    {
      switch(cmd){
        int i = 0, ret = 0, size = 0, argc = 0;
        padico_modID_vect_t modIDs;
        char*modID = NULL, **argv = NULL, *tmp = NULL, *groupID = NULL,
          *nodeID = NULL, *xml_command = NULL;
        padico_rc_t rc = NULL;
        padico_group_t group = NULL;
        struct puk_parse_entity_s pe;
      case TUBE_PING:
        ret = 1;
        write_tube(fd, &ret, sizeof(int));
        break;
      case TUBE_KILL:
        ret = 1;
        write_tube(fd, &ret, sizeof(int));
        padico_tm_exit();
        break;
      case TUBE_LISTMODULES:
        modIDs = puk_mod_getmodIDs();
        size = padico_modID_vect_size(modIDs);
        write_tube(fd, &size, sizeof(int));
        for(i=0;i<size;i++)
          write_char_tube(fd, padico_modID_vect_at(modIDs, i));
        break;
      case TUBE_LOADMODULE:
        read_char_tube(fd, &modID);
        rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, modID);
        send_rc(fd, rc);
        break;
      case TUBE_RUNMODULE:
        read_char_tube(fd, &modID);
        read_tube(fd, &argc, sizeof(int));
        argv = padico_malloc(argc*sizeof(char*));
        for(i=0;i<argc;i++)
          {
            tmp = (*argv)+i;
            read_char_tube(fd, &tmp);
          }
        rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, modID, argc, argv);
        send_rc(fd, rc);
        break;
      case TUBE_UNLOADMODULE:
        read_char_tube(fd, &modID);
        rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, modID);
        send_rc(fd, rc);
        break;
      case TUBE_CREATEGROUP:
        read_char_tube(fd, &groupID);
        if(padico_group_lookup(groupID) != NULL)
          {
            rc = padico_rc_error("cannot create group: groupID=**%s** already exists.", groupID);
            padico_print("Error creating group: it already exists\n");
          }
        else
          {
            group = padico_group_init(groupID);
            read_tube(fd, &size, sizeof(int));
            for(i=0;i<size;i++)
              {
                read_char_tube(fd, &nodeID);
                padico_topo_node_t node = padico_topo_getnodebyname(nodeID);
                if(node != NULL)
                  {
                    padico_group_node_add(group, node);
                  }
                else
                  {
                    rc = padico_rc_error("cannot add node **%s**: not recognized as a valid nodeID.", nodeID);
                    break;
                  }
              }
          }
        if(!padico_rc_iserror(rc))
          {
            padico_group_publish(group);
            padico_print("Group created: %s\n", groupID);
          }
        send_rc(fd, rc);
        break;
      case TUBE_RAWXMLCOMMAND:
        read_char_tube(fd, &xml_command);
        pe = puk_xml_parse_buffer(xml_command, strlen(xml_command), PUK_TRUST_CONTROL);
        padico_rc_show(pe.rc);
        send_rc(fd, pe.rc);
        break;
      default:
        padico_warning("Tube Command not understood: %i\n", cmd);
        break;
      }
    }
  return 0;
}

static int tube_callback(int fd, void*key)
{
  int cmd = 0;
  int newsockfd = 0;
  newsockfd = PUK_ABI_FSYS_WRAP(accept)(fd, NULL, NULL);
  if(newsockfd < 0)
    padico_fatal("Accept error: %s", strerror(__puk_abi_wrap__errno));
  read_tube(newsockfd, &cmd, sizeof(int));
  answer_cmd(cmd, newsockfd);
  close(newsockfd);
  return 1;
}

static int tube_gk_init(void)
{
  const int LISTENQ = 10;

  /* Prepare communication */
  struct sockaddr_un addr;
  padico_string_t str_sk;
  padico_topo_node_t me = padico_topo_getlocalnode();
  int len = PADICO_NA_UUID_SIZE;
  char*text = puk_hex_encode(padico_topo_node_getuuid(me), &len, NULL);
  memset(&addr, 0, sizeof(struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  str_sk = padico_string_new();
  padico_string_catf(str_sk, "%s%s%s", "/tmp/padico/", text, ".sk");
  strcpy(addr.sun_path, padico_string_get(str_sk));

  /* Prepare /tmp/padico directory */
  mkdir("/tmp/padico/",  S_IRWXU | S_IRWXG | S_IRWXO);

  /* Prepare communications files */
  sockfd = PUK_ABI_FSYS_WRAP(socket)(AF_UNIX, SOCK_STREAM, 0);
  if (sockfd < 0)
    padico_fatal("Socket creation error: %s", strerror(__puk_abi_wrap__errno));

  /* Bind */
  if(PUK_ABI_FSYS_WRAP(bind)(sockfd, (struct sockaddr *) &addr,
                        sizeof(addr)) < 0)
    padico_fatal("Bind error: socket:%i, path:%s, error:%s\n",
                 sockfd, addr.sun_path, strerror(__puk_abi_wrap__errno));
  /* Starting listening */
  if(PUK_ABI_FSYS_WRAP(listen)(sockfd, LISTENQ) < 0)
    padico_fatal("Listen error: %s.\n", strerror(__puk_abi_wrap__errno));

  in_io = padico_io_register(sockfd, PADICO_IO_EVENT_READ,
                             &tube_callback , NULL);
  padico_io_activate(in_io);
  return 0;
}

static void tube_gk_stop(void)
{
  /* Closing the server */
  padico_io_deactivate(in_io);
  close(sockfd);
}
