/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <Padico/Module.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>
#include <Padico/PadicoTM.h>

#if defined(HAVE_SLURM_PMI2_H)
#include <slurm/pmi2.h>
#elif defined(HAVE_PMI2_H)
#include <pmi2.h>
#else
#error "no suitable pmi2.h found."
#endif
#include "PMI2.h"

static int padico_pmi2_init(void);
static void padico_pmi2_finalize(void);

PADICO_MODULE_DECLARE(PMI2, padico_pmi2_init, NULL, padico_pmi2_finalize,
                      "Topology", "PadicoControl");

static struct
{
  int rank;
  int size;
} padico_pmi2 = { .rank = -1, .size = -1 };

static inline char*padico_pmi2_strerror(int rc)
{
  switch(rc)
    {
    case PMI2_SUCCESS: return "operation completed successfully"; break;
    case PMI2_FAIL: return "operation failed"; break;
    case PMI2_ERR_NOMEM: return "input buffer not large enough"; break;
    case PMI2_ERR_INIT: return "PMI not initialized"; break;
    case PMI2_ERR_INVALID_ARG: return "invalid argument"; break;
    case PMI2_ERR_INVALID_KEY: return "invalid key argument"; break;
    case PMI2_ERR_INVALID_KEY_LENGTH: return "invalid key length argument"; break;
    case PMI2_ERR_INVALID_VAL: return "invalid val argument"; break;
    case PMI2_ERR_INVALID_VAL_LENGTH: return "invalid val length argument"; break;
    case PMI2_ERR_INVALID_LENGTH: return "invalid length argument"; break;
    case PMI2_ERR_INVALID_NUM_ARGS: return "invalid number of arguments"; break;
    case PMI2_ERR_INVALID_ARGS: return "invalid args argument"; break;
    case PMI2_ERR_INVALID_NUM_PARSED: return "invalid num_parsed length argument"; break;
    case PMI2_ERR_INVALID_KEYVALP: return "invalid keyvalp argument"; break;
    case PMI2_ERR_INVALID_SIZE: return "invalid size argument"; break;
    case PMI2_ERR_OTHER: return "other unspecified error"; break;
    }
  return "unknown";
};

/* ********************************************************* */

static int padico_pmi2_init(void)
{
  if(PMI2_Initialized())
    return 0;
#if defined(HAVE_CRAY_PMI)
  /* by default, Cray PMI can only store 8 KVS entries per process, which is
   * barely enough to store host & node topology. We increase to at least 32 entries.
   * We touch the default only for Cray, not for more modern PMI implementations. */
  padico_setenv("PMI_MAX_KVS_ENTRIES", "32");
#endif /* HAVE_CRAY_PMI */
  int spawned, appnum;
  int rc = PMI2_Init(&spawned, &padico_pmi2.size, &padico_pmi2.rank, &appnum);
  if(rc != PMI2_SUCCESS)
    {
      padico_fatal("pmi2 initialization failed.\n");
    }
  const char*version = "unknown";
#ifdef HAVE_SLURM_PMI2_H
  version = "slurm-pmi2";
#endif
#ifdef HAVE_CRAY_PMI
  version = "cray-pmi";
#endif
  padico_print("PMI2 init; version = %s; size = %d; rank = %d; appnum = %d; topo rank = %d\n",
               version, padico_pmi2.size, padico_pmi2.rank, appnum, padico_na_rank());
  if(padico_pmi2.size < 0 || padico_pmi2.rank < 0)
    {
      PMI2_Finalize();
      padico_fatal("pmi2 was initialized but negative values for job and/or rank was returned.\n");
    }
  if(padico_topo_session() != NULL)
    {
      /* check consistency between pmi2 numbering and padico-launch numbering */
      if(padico_pmi2.rank != padico_na_rank())
        {
          PMI2_Finalize();
          padico_fatal("inconsistency detected, PMI_RANK=%d, PMI2_Init rank=%d. Suspecting missing srun parameter %s\n",
                       padico_na_rank(), padico_pmi2.rank,
#if defined(HAVE_CRAY_PMI)
                       "--mpi=cray_shasta"
#elif defined(HAVE_SLURM_PMI2_H)
                       "--mpi=pmi2"
#endif
                       );
        }
    }
  return 0;
}

static void padico_pmi2_finalize(void)
{
  if(PMI2_Initialized())
    PMI2_Finalize();
}

int padico_pmi2_getrank(void)
{
  return padico_pmi2.rank;
}

int padico_pmi2_getsize(void)
{
  return padico_pmi2.size;
}

void padico_pmi2_barrier(void)
{
  int rc;
  if(PMI2_SUCCESS != (rc = PMI2_KVS_Fence()))
    {
      padico_fatal("PMI2_KVS_Fence failed (rc=%d).", rc);
    }
}

void padico_pmi2_abort(const char*msg)
{
  PMI2_Abort(1, msg);
}


/* ********************************************************* */

static int padico_pmi2_kvs_publish_single(const char*key, const char*value)
{
  if(strlen(value) >= PMI2_MAX_VALLEN)
    {
      padico_warning("value too long (%d/%d) for key = %s\n", (int)strlen(value), PMI2_MAX_VALLEN, key);
    }
  int rc = PMI2_KVS_Put(key, value);
  if(rc != PMI2_SUCCESS)
    {
      padico_fatal("PMI2_KVS_Put failed key = %s; rc=%d (%s).",
                   key, rc, padico_pmi2_strerror(rc));
    }
  return 0;
}

static char*padico_pmi2_kvs_lookup_single(const char*key, int from)
{
  char*value = padico_malloc(PMI2_MAX_VALLEN + 1);
  int len = PMI2_MAX_VALLEN;
  int rc = PMI2_KVS_Get(/* jobid */ NULL, PMI2_ID_NULL, key, value, PMI2_MAX_VALLEN + 1, &len);
  if(rc != PMI2_SUCCESS)
    padico_fatal("PMI2_KVS_Get() failed key = %s; rc = %d (%s).\n",
                 key, rc, padico_pmi2_strerror(rc));
  return value;
}

int padico_pmi2_kvs_publish(const char*key, const char*value0)
{
  size_t len = strlen(value0) + 1;
  char*value = puk_hex_encode(value0, &len, NULL);
  int size = strlen(value);
  int nb_msg = (size / (PMI2_MAX_VALLEN - 1)) + (size % (PMI2_MAX_VALLEN - 1) ? 1 : 0);

  char size_key[PMI2_MAX_KEYLEN] = { 0 };
  char size_val[PMI2_MAX_VALLEN] = { 0 };
  sprintf(size_key, "%s-size", key);
  sprintf(size_val , "%d", size);
  padico_pmi2_kvs_publish_single(size_key, size_val);
  for(int i = 0; i < nb_msg; i++)
    {
      char part_key[PMI2_MAX_KEYLEN] = { 0 };
      char part_val[PMI2_MAX_VALLEN + 1] = { 0 };
      int part_size = (i < nb_msg - 1) ? (PMI2_MAX_VALLEN - 1) : size - (i * (PMI2_MAX_VALLEN - 1));
      int offset = i ? (i * (PMI2_MAX_VALLEN - 1))  : 0;

      sprintf(part_key, "%s-part-%d", key, i);
      memcpy(part_val, value + offset, part_size);
      part_val[part_size] = 0;
      padico_pmi2_kvs_publish_single(part_key, part_val);
    }
  padico_free(value);
  return 0;
}

char*padico_pmi2_kvs_lookup(const char*key, int from)
{
  char size_key[PMI2_MAX_KEYLEN] = { 0 };
  sprintf(size_key, "%s-size", key);
  char*size_val = padico_pmi2_kvs_lookup_single(size_key, from);
  assert(size_val != NULL);
  int size = atoi(size_val);
  free(size_val);
  int nb_msg = (size / (PMI2_MAX_VALLEN - 1)) + (size % (PMI2_MAX_VALLEN - 1) ? 1 : 0);
  char*value = padico_malloc(size + 1);
  value[0] = '\0';
  for(int i = 0; i < nb_msg; i++)
    {
      char part_key[PMI2_MAX_KEYLEN];
      sprintf(part_key, "%s-part-%d", key, i);
      char*part_val = padico_pmi2_kvs_lookup_single(part_key, from);
      strcat(value, part_val);
      free(part_val);
    }

  size_t len = strlen(value);
  char*value0 = puk_hex_decode(value, &len, NULL);
  padico_free(value);
  return value0;
}

void padico_pmi2_kvs_fence(void)
{
  int rc;
  if(PMI2_SUCCESS != (rc = PMI2_KVS_Fence()))
    {
      padico_fatal("PMI2_KVS_Fence failed (rc=%d).", rc);
    }
}
