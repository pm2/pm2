/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief NetSelector with lazy name-resolution and lazy component loading.
 * @ingroup NetSelector
 */


#include "NetSelector.h"

#include <Padico/Module.h>
#include <Padico/Puk.h>

static int ns_netselector_init(void);
static void ns_netselector_finalize(void);

PADICO_MODULE_DECLARE(NetSelector, ns_netselector_init, NULL, ns_netselector_finalize, "Topology");

/* ********************************************************* */

static const char default_profile[] = "default";

typedef const struct padico_netselector_driver_s*padico_netselector_driver_t;
/** describes an entry in the selectors hashtable
 */
struct netselector_entry_s
{
  const struct padico_netselector_driver_s*selector; /**< the driver for the given selector */
  const char*name; /**< the name for this entry-
                    * @note this field may be different from selector->name (because of aliases)
                    */
  puk_component_t component;
};
PUK_VECT_TYPE(netselector_entry, struct netselector_entry_s);

static struct
{
  puk_hashtable_t table; /**< table of know selectors [ key: profile; data: netselector_entry_vect_t ] */
  marcel_mutex_t lock;   /**< lock for accessing the hashtable */
} nsdb = { .table = NULL };

static netselector_entry_vect_t ns_selector_lookup(const char*profile);
static void ns_selector_insert(const struct padico_netselector_driver_s*selector, puk_component_t component);


/* ********************************************************* */

static int ns_netselector_init(void)
{
  puk_iface_register("NetSelector");
  nsdb.table = puk_hashtable_new_string();
  marcel_mutex_init(&nsdb.lock, NULL);
  return 0;
}

static void ns_netselector_entry_destructor(void*_key, void*_data)
{
  char*key = _key;
  padico_free(key);
  netselector_entry_vect_t v = _data;
  netselector_entry_vect_delete(v);
}
static void ns_netselector_finalize(void)
{
  puk_hashtable_delete(nsdb.table, &ns_netselector_entry_destructor);
}

/* ********************************************************* */

void padico_ns_selector_register(const struct padico_netselector_driver_s*selector)
{
  padico_out(12, "adding selector=%s for profile=%s\n", selector->name, selector->profile?:"*");
  puk_component_t c = puk_component_declare(selector->name,
                                            puk_component_provides("NetSelector", "selector", selector));
  ns_selector_insert(selector, c);
}

void padico_ns_selector_unregister(const struct padico_netselector_driver_s*selector)
{
  assert(nsdb.table);
  netselector_entry_vect_t v = NULL;
  marcel_mutex_lock(&nsdb.lock);
  v = puk_hashtable_lookup(nsdb.table, selector->profile);
  if(v)
    {
      netselector_entry_vect_itor_t i;
      puk_vect_foreach(i, netselector_entry, v)
        {
          if(strcmp(i->name, selector->name) == 0)
            {
              puk_component_destroy(i->component);
              netselector_entry_vect_erase(v, i);
              break;
            }
        }
    }
  marcel_mutex_unlock(&nsdb.lock);
}

puk_component_t padico_ns_serial_selector(padico_topo_node_t peer, const char*_profile,
                                          puk_iface_t iface)
{
  const char*profile = _profile?:default_profile;
  const netselector_entry_vect_t v = ns_selector_lookup(profile);
  puk_component_t assembly = NULL;
  netselector_entry_vect_itor_t i;
  padico_out(20, "remote peer node=%s- computing assembly for iface=%s; profile=%s.\n",
             peer?padico_topo_node_getname(peer):"*", iface->name, profile);
  assert(peer != NULL);
  puk_vect_foreach(i, netselector_entry, v)
    {
      const padico_netselector_driver_t selector = i->selector;
      if(selector && selector->serial)
        assembly = (*selector->serial)(padico_topo_getlocalnode(), peer, profile, iface);
      if(assembly)
        break;
    }
  if(!assembly)
    padico_warning("profile = %s; iface = %s; peer = %s; *** cannot find any valid component\n",
                   profile, iface->name, peer?padico_topo_node_getname(peer):"*");
  else
    padico_out(20, "profile = %s; assembly = %s\n", profile, assembly->name);
  return assembly;
}

puk_component_t padico_ns_host_selector(padico_topo_host_t host, const char*_profile,
                                        puk_iface_t iface, uint16_t port, int protocol)
{
  const char*profile = _profile?:default_profile;
  const netselector_entry_vect_t v = ns_selector_lookup(profile);
  puk_component_t assembly = NULL;
  netselector_entry_vect_itor_t i;
  padico_out(20, "remote peer host=%s- computing assembly for iface=%s; profile=%s.\n",
             host?padico_topo_host_getname(host):"*", iface->name, profile);
  puk_vect_foreach(i, netselector_entry, v)
    {
      const padico_netselector_driver_t selector = i->selector;
      if(selector && selector->host)
        {
          padico_out(20, "trying selector *%s*\n", selector->name);
          assembly = (*selector->host)(host, profile, iface, port, protocol);
          if(assembly)
            {
              padico_out(20, "got answer from selector *%s*\n", selector->name);
              break;
            }
        }
    }
  padico_out(20, "profile = %s; assembly = %s\n", profile, assembly->name);
  return assembly;
}

/* ********************************************************* */

static void ns_selector_insert(const struct padico_netselector_driver_s*selector, puk_component_t component)
{
  int already = 0;
  if(nsdb.table)
    {
      netselector_entry_vect_t v = NULL;
      marcel_mutex_lock(&nsdb.lock);
      v = puk_hashtable_lookup(nsdb.table, selector->profile);
      if(!v)
        {
          v = netselector_entry_vect_new();
          puk_hashtable_insert(nsdb.table, padico_strdup(selector->profile), v);
        }
      else
        {
          netselector_entry_vect_itor_t i;
          puk_vect_foreach(i, netselector_entry, v)
            {
              if(strcmp(i->name, selector->name) == 0)
                {
                  i->selector = selector;
                  already = 1;
                }
            }

        }
      if(!already)
        {
          const struct netselector_entry_s e =
            {
              .name      = selector->name,
              .selector  = selector,
              .component = component
            };
          netselector_entry_vect_push_back(v, e);
        }
      marcel_mutex_unlock(&nsdb.lock);
    }
}

/** returns the known selector entries (vect) for a given profile
 */
static netselector_entry_vect_t ns_selector_lookup(const char*profile)
{
  netselector_entry_vect_t v = NULL;
  marcel_mutex_lock(&nsdb.lock);
  v = puk_hashtable_lookup(nsdb.table, profile);
  marcel_mutex_unlock(&nsdb.lock);
  if(!v)
    {
      padico_string_t s = padico_string_new();
      const char*selector_name = NULL;
      int i = 0;
      padico_out(12, "resolving selectors for profile *%s*\n", profile);
      do
      {
        padico_string_printf(s, "PADICO_NETSELECTOR_%s_%d", profile, i);
        selector_name = padico_getattr(padico_string_get(s));
        if(selector_name)
          {
            padico_print("resolving- %s:%d... %s\n", profile, i, selector_name);
            const puk_component_t component = puk_component_resolve(selector_name);
            if(!component)
              {
                padico_out(puk_verbose_critical, "invalid component, will not use selector *%s*.\n", selector_name);
              }
          }
        i++;
      }
      while(selector_name);
      padico_string_delete(s);
      marcel_mutex_lock(&nsdb.lock);
      v = puk_hashtable_lookup(nsdb.table, profile);
      marcel_mutex_unlock(&nsdb.lock);
    }
  if((v == NULL) || netselector_entry_vect_empty(v))
    {
      padico_fatal("no selector for profile *%s*.\n", profile);
    }
  return v;
}
