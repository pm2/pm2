/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief API for protocol selection in PadicoTM
 * @ingroup NetSelector
 */

#ifndef PADICO_NETSELECTOR_H
#define PADICO_NETSELECTOR_H

#include <Padico/Puk.h>
#include <Padico/Puk-hashtable.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

PUK_MOD_AUTO_DEP(NetSelector, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME NetSelector

/** @defgroup NetSelector Core module: NetSelector -- The PadicoTM network selector
 * @author Francois Lesueur
 * @author Alexandre Denis
 * @see @ref NetAccessAdapter
 */

#ifdef __cplusplus
extern "C" {
#endif

  /** @defgroup NetSelectorQuery API: NetSelector query and configure interface
   * @see @ref NetAccessAdapter
   * @ingroup NetSelector
   */


  /** @addtogroup NetSelectorQuery
   * @{
   */


  /** NetSelector driver interface
   */
  struct padico_netselector_driver_s
  {
    /** name of this NetSelector implementation */
    const char*name;

    /** name of the targeted profile */
    const char*profile;

    /** select a serial assembly towards a PadicoTM-enabled node */
    puk_component_t (*serial)(padico_topo_node_t node1, padico_topo_node_t node2,
                              const char*profile, puk_iface_t iface);

    /** select a serial assembly towards an abitrary host (PadicoTM-enabled or not) */
    puk_component_t (*host)(padico_topo_host_t host, const char*profile,
                            puk_iface_t iface, uint16_t port, int protocol);
  };

  PUK_IFACE_TYPE(NetSelector, struct padico_netselector_driver_s);

  void padico_ns_selector_register(const struct padico_netselector_driver_s*selector);

  void padico_ns_selector_unregister(const struct padico_netselector_driver_s*selector);


  /* *** Query to the selector ***************************** */

  puk_component_t padico_ns_serial_selector(padico_topo_node_t node,
                                          const char*profile,
                                          puk_iface_t iface);

  puk_component_t padico_ns_host_selector(padico_topo_host_t host,
                                        const char*profile,
                                        puk_iface_t iface, uint16_t port, int protocol);

  /** @} */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PADICO_NETSELECTOR_H */
