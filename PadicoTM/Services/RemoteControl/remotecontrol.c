/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Listenner for use with PadicoControl (XML flavor).
 * @ingroup RemoteControl
 */

/** @defgroup RemoteControl Service: RemoteControl -- server-side for XML/socket remote control
 * @author Alexandre Denis
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#include <Padico/SysIO.h>
#include <Padico/PadicoControl.h>
#include <Padico/SocketFactory.h>
#include <Padico/NetSelector.h>
#include <Padico/Connector.h>


static int remote_control_init(void);
static void remote_control_finalize(void);

PADICO_MODULE_DECLARE(RemoteControl, remote_control_init, NULL, remote_control_finalize);


static void remote_control_event_listener(void*arg);
static void remote_control_new_node(const padico_topo_node_t node);

static const int REMOTE_CONTROL_ISRC     = 0;
static const int REMOTE_CONTROL_NEW_NODE = 1;
static const int REMOTE_CONTROL_DEL_NODE = 2;

struct remote_control_header_s
{
  uint32_t node_size;
  uint32_t msg_size;
  uint32_t flag;
};

static struct
{
  int route; /* determine if we will handle commands to other known nodes */
  int fd;
  marcel_mutex_t lock_send;
  padico_io_t in_io;
  struct puk_receptacle_SocketFactory_s sf;
  puk_hashtable_t nodes; /* hash: node -> node */
  padico_tm_bgthread_pool_t pool;
} remote_control;

/* ********************************************************* */


static void remote_control_pack_hdr(struct remote_control_header_s*hdr)
{
  hdr->node_size = htonl(hdr->node_size);
  hdr->msg_size  = htonl(hdr->msg_size);
  hdr->flag      = htonl(hdr->flag);
}

static void remote_control_unpack_hdr(struct remote_control_header_s*hdr)
{
  hdr->node_size = ntohl(hdr->node_size);
  hdr->msg_size  = ntohl(hdr->msg_size);
  hdr->flag      = ntohl(hdr->flag);
}

static void remote_control_control_send(struct remote_control_header_s*hdr, const char*data)
{
  int size = hdr->node_size + hdr->msg_size;
  remote_control_pack_hdr(hdr);

  marcel_mutex_lock(&remote_control.lock_send);
  padico_sysio_out(remote_control.fd, hdr, sizeof(struct remote_control_header_s));
  padico_sysio_out(remote_control.fd, data, size);
  marcel_mutex_unlock(&remote_control.lock_send);
}

static void remote_control_node_callback(padico_rc_t rc, void*key)
{
  char*node_name = key;
  assert(node_name != NULL);
  char*rc_text = padico_rc_gettext(rc);
  int name_len = strlen(node_name);
  int text_len = strlen(rc_text);

  char*msg = padico_malloc(name_len + text_len + 17);
  msg[0] = 0;
  strcat(msg, node_name);
  if(padico_rc_iserror(rc))
    {
      strcat(msg, "<error>");
      strcat(msg, rc_text);
      strcat(msg, "</error>\n");
      text_len += 16;
    }
  else
    {
      strcat(msg, "<ok>");
      strcat(msg, rc_text);
      strcat(msg, "</ok>\n");
      text_len += 10;
    }
  struct remote_control_header_s hdr = {
      .node_size = name_len,
      .msg_size = text_len,
      .flag = REMOTE_CONTROL_ISRC
  };

  padico_out(20, "return to node=%s msg=%s.\n", node_name, rc_text);
  remote_control_control_send(&hdr, msg);

  padico_rc_delete(rc);
  padico_free(node_name);
  padico_free(msg);
}

struct remote_control_command_s
{
  char*node_name;
  char*msg;
};

static void*remote_control_node_send(void*arg)
{
  struct remote_control_command_s*cmd = arg;
  char*node_name = cmd->node_name;
  if(node_name != NULL && padico_topo_getnodebyname(node_name) == padico_topo_getlocalnode())
    {
      struct puk_parse_entity_s pe = puk_xml_parse_buffer(cmd->msg, strlen(cmd->msg), PUK_TRUST_CONTROL);
      remote_control_node_callback(pe.rc, node_name);
    }
  else if(node_name != NULL && padico_topo_getnodebyname(node_name) != NULL)
    {
      padico_topo_node_t node = padico_topo_getnodebyname(node_name);
      padico_control_send_async(node, cmd->msg, &remote_control_node_callback, node_name);
    }
  else
    {
      padico_warning("Received a command for an unknown node (%s).\n", node_name);
      padico_free(cmd->node_name);
    }
  padico_free(cmd->msg);
  padico_free(cmd);
  return NULL;
}

static void remote_control_connection_lost()
{
  remote_control.fd = -1;
  padico_fatal("lost connection.\n");
}

static int remote_control_control_callback(int fd, void*key)
{
  int again = 1;
  int sysrc = -1;
  struct remote_control_header_s hdr;

  /* receive header */
  assert(remote_control.fd == fd);
  padico_out(40, "entering...\n");
  sysrc = padico_sysio_in(fd, &hdr, sizeof(struct remote_control_header_s));
  padico_out(40, "header received- rc=%d\n", sysrc);
  if(sysrc <= 0)
    {
      again = 0;
      remote_control_connection_lost();
      return again;
    }
  remote_control_unpack_hdr(&hdr);

  /* receive body */
  char*data = NULL;
  const size_t size = hdr.node_size + hdr.msg_size;
  padico_out(40, "expecting %d bytes\n", (int)size);
  if(size > 0)
    {
      data = padico_malloc(size);
      sysrc = padico_sysio_in(fd, data, size);
      padico_out(40, "body received- rc=%d\n", sysrc);
      if(sysrc < size)
        {
          remote_control_connection_lost();
          again = 0;
          return again;
        }
    }
  char*msg = padico_malloc(hdr.msg_size + 1);
  memcpy(msg, data + hdr.node_size, hdr.msg_size);
  msg[hdr.msg_size] = 0;
  char*node_name = data;
  node_name[hdr.node_size] = 0;

  /* deliver the command */
  padico_out(20, "received commmand- len=%d; to=%s msg=*%s*\n", (int)size, node_name, msg);
  struct remote_control_command_s*cmd = padico_malloc(sizeof(struct remote_control_command_s));
  cmd->node_name = node_name;
  cmd->msg = msg;
  padico_tm_bgthread_start(remote_control.pool, &remote_control_node_send, cmd, "remote_control_node_send");
  return again;
}

static void remote_control_connector(void*key, int fd)
{
  remote_control.fd = fd;
  /* activate polling */
  remote_control.in_io = padico_io_register(fd, PADICO_IO_EVENT_READ, &remote_control_control_callback, NULL);
  padico_io_activate(remote_control.in_io);
  //?unregister desactivate?
  padico_out(40, "ready.\n");

  /* authorize to send */
  marcel_mutex_unlock(&remote_control.lock_send);
  /* add itself */
  remote_control_new_node(padico_topo_getlocalnode());

  if(remote_control.route)
    padico_control_event_subscribe(&remote_control_event_listener);
}

static void remote_control_new_node(const padico_topo_node_t node)
{
  padico_topo_node_t n = puk_hashtable_lookup(remote_control.nodes, node);
  if(n == NULL)
    {
      padico_out(10, "add node *%s*\n", padico_topo_node_getname(node));
      const char*node_name = padico_topo_node_getname(node);
      struct remote_control_header_s hdr = {
          .node_size = strlen(node_name),
          .msg_size  = 0,
          .flag      = REMOTE_CONTROL_NEW_NODE
      };
      remote_control_control_send(&hdr, node_name);
      puk_hashtable_insert(remote_control.nodes, node, node);
    }
}

static void remote_control_del_node(const padico_topo_node_t node)
{
  padico_topo_node_t n = puk_hashtable_lookup(remote_control.nodes, node);
  if(n != NULL)
    {
      const char*node_name = padico_topo_node_getname(node);
      struct remote_control_header_s hdr = {
          .node_size = strlen(node_name),
          .msg_size  = 0,
          .flag      = REMOTE_CONTROL_DEL_NODE
      };
      remote_control_control_send(&hdr, node_name);
      puk_hashtable_remove(remote_control.nodes, node);
      padico_out(10, "delete node *%s*\n", padico_topo_node_getname(node));
    }
}

static void remote_control_event_listener(void*arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)arg;
  padico_out(20, "event kind: %d\n", event->kind);
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_INIT_CONTROLER:
      {
        const padico_topo_node_vect_t nodes = event->INIT_CONTROLER.nodes;
        padico_topo_node_vect_itor_t i;
        puk_vect_foreach(i, padico_topo_node, nodes)
          remote_control_new_node(*i);
      }
    break;

    case PADICO_CONTROL_EVENT_NEW_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        remote_control_new_node(node);
      }
    break;

    case PADICO_CONTROL_EVENT_DELETE_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        remote_control_del_node(node);
      }
    break;

    default:
    break;
    }
}

static void remotecontrol_getranks_handler(puk_parse_entity_t e)
{
  puk_parse_set_rc(e, padico_rc_msg("%d/%d", padico_na_rank(), padico_na_size()));
}

static int remote_control_init(void)
{
  int rc;
  const char*control_host = padico_getattr("ControlHost");
  const char*control_port = padico_getattr("ControlPort");
  const char*control_connection = padico_getattr("ControlConnection");
  remote_control.fd = -1;
  remote_control.in_io     = NULL;
  remote_control.nodes = puk_hashtable_new_ptr();
  remote_control.pool = padico_tm_bgthread_pool_create("RemoteControl");
  if((control_host == NULL) ||
     (control_port == NULL))
    {
      padico_warning("missing module attributes- expected: ControlHost, ControlPort, ControlConnection\n");
      return -1;
    }
  if(control_connection != NULL && strcmp(control_connection, "all") == 0)
  {
    remote_control.route = 0;
  }
  else if(control_connection != NULL && strcmp(control_connection, "one") == 0)
  {
    if(padico_na_rank() != 0)
      return 0;
    remote_control.route = 1;
  }
  else
  {
    padico_print("no connection required. exit.\n");
    return 0;
  }

  marcel_mutex_init(&remote_control.lock_send, 0);
  marcel_mutex_lock(&remote_control.lock_send);

  /* declare XML action */
  puk_xml_add_action((struct puk_tag_action_s) {
      .xml_tag        = "getranks",
      .start_handler  = NULL,
      .end_handler    = &remotecontrol_getranks_handler,
      .required_level = PUK_TRUST_OUTSIDE,
      .help           = "RemoteControl: get session size and ranks"
    });

  padico_print("connecting to %s:%s\n", control_host, control_port);
  /* resolve address */
  const puk_iface_t iface_sf = puk_iface_SocketFactory();
  struct in_addr inaddr;
  padico_topo_host_t peer_host = NULL;
  if(control_host[0] >= '0' && control_host[0] <= '9' )
    {
      /* doted IP address */
      rc = inet_aton(control_host, &inaddr);
      if(!rc)
        {
          padico_fatal("cannot parse control host IP address %s.\n", control_host);
        }
      peer_host = padico_topo_host_resolve_byinaddr(&inaddr);
    }
  else
    {
      peer_host = padico_topo_host_resolvebyname(control_host);
      if(!peer_host)
        {
          padico_fatal("cannot resolve control host name %s\n", control_host);
        }
      inaddr = *padico_topo_host_getaddr(peer_host);
    }
  if(!peer_host)
    {
      padico_fatal("cannot resolve host address %s.\n", inet_ntoa(inaddr));
    }
  /* build address */
  struct sockaddr_in remote_sa =
    {
      .sin_family = AF_INET,
      .sin_port   = htons(atoi(control_port)),
      .sin_addr   = *padico_topo_host_getaddr(peer_host)
    };
  /* connect the socket */
  puk_component_t sf_assembly = padico_ns_host_selector(peer_host, "inet", iface_sf, (uint16_t) remote_sa.sin_port, IPPROTO_TCP);
  puk_instance_t sf_instance = puk_component_instantiate(sf_assembly);
  puk_instance_indirect_SocketFactory(sf_instance, NULL, &remote_control.sf);

  rc = (*remote_control.sf.driver->create)(remote_control.sf._status, AF_INET, SOCK_STREAM, 0);
  if(rc == -1)
    {
      padico_warning("socket_create: cannot create the socket.\n");
      return -1;
    }
  rc = (*remote_control.sf.driver->connect)(remote_control.sf._status, (struct sockaddr*) &remote_sa, sizeof(remote_sa),
                                            remote_control_connector, NULL);
  if(rc == 0 || rc == -EINPROGRESS)
    {
      padico_out(40, "Connection launched.\n");
      return 0;
    }
  else
    {
      padico_string_t a = puk_component_serialize(sf_assembly);
      padico_warning("cannot connect to %s:%s; rc=%d (%s). We tried with the following component: \n%s\n",
                     control_host, control_port, -rc, strerror(-rc), padico_string_get(a));
      padico_string_delete(a);
      remote_control.sf.driver = NULL;
      return -1;
    }
}

static void remote_control_finalize(void)
{
  padico_tm_bgthread_pool_wait(remote_control.pool);
  if(remote_control.in_io)
    padico_io_unregister(remote_control.in_io);
  if(remote_control.sf.driver != NULL)
    (*(remote_control.sf.driver)->close)(remote_control.sf._status);
  puk_hashtable_delete(remote_control.nodes, NULL);
}
