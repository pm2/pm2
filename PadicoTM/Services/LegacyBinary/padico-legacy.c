/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Launcher stub for 'LegacyBinary' service.
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>


static void padico_legacy_init(void) __attribute__((constructor));
static void padico_legacy_finalize(void) __attribute__((destructor));


/* ********************************************************* */

static void padico_legacy_init(void)
{
  const char*legacy_binary = getenv("PADICO_LEGACY");
  if(puk_verbose_level() >= puk_verbose_info)
    {
      fprintf(stderr, "LegacyBinary: starting preload for %s.\n", legacy_binary);
    }
  if(puk_opt_parse_bool(getenv("PADICO_VALGRIND")))
    {
      fprintf(stderr, "# PadicoTM: valgrind detected. No preinit done.\n");
      unsetenv("PADICO_VALGRIND");
#ifdef PUK_VG_NOOP
      fprintf(stderr, "# PadicoTM: WARNING- running on valgrind *without valgrind integration*.\n");
      puk_sleep(3);
#endif
      return;
    }
  puk_abi_disable_preload();

  padico_puk_init();
  padico_tm_init();
  if(legacy_binary != NULL)
    {
      padico_tm_ready_wait();
    }
  else
    {
      padico_out(puk_verbose_info, "PADICO_LEGACY not set. Not waiting for LegacyBinary ready notification.\n");
    }
}

static void padico_legacy_finalize(void)
{
  padico_tm_finalize();

  /*  padico_puk_shutdown(); */

  if(puk_opt_parse_bool(getenv("PADICO_VERBOSE")))
    {
      fprintf(stderr, "LegacyBinary: exit.\n");
    }
}
