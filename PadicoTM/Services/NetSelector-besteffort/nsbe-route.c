/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file nsbe-route.c
 * @brief Compute routes of ssh tunnels
 * @ingroup NetSelector-besteffort
 */

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>

PADICO_MODULE_HOOK(NetSelector_besteffort);

/* ********************************************************* */

PUK_VECT_TYPE(nsbe_dijkstra_vertex, struct nsbe_dijkstra_vertex_s*);

/** distance for unreachable vertices; do *not* use -1, it confuses algorithm. */
#define NSBE_UNREACHABLE (1024*1024)

struct nsbe_dijkstra_vertex_s
{
  int weight;                             /**< distance from local vertex (NSBE_UNREACHABLE, if unreachable) */
  padico_topo_host_t host;                  /**< host for this vertex (NULL if vertex is a network) */
  padico_topo_network_t network;          /**< network for this vertex (NULL if vertex is a host) */
  struct nsbe_dijkstra_vertex_s*previous; /**< ancestor in the computed route */
  nsbe_dijkstra_vertex_vect_t outputs;    /**< vertex outputs, computed at init */
};


/** internal state of the Dijkstra algorithm */
struct nsbe_dijkstra_s
{
  nsbe_dijkstra_vertex_vect_t done;      /**< vertices already inserted into working set */
  nsbe_dijkstra_vertex_vect_t todo;      /**< vertices not processed yet */
  struct nsbe_dijkstra_vertex_s*local_v; /**< vertex for local host */
  struct nsbe_dijkstra_vertex_s*dest_v;  /**< vertex for dest host */
};


static struct nsbe_dijkstra_vertex_s*nsbe_dijkstra_vertex_add(puk_hashtable_t vertices, padico_topo_host_t host, padico_topo_network_t network)
{
  assert((host == NULL) || (network == NULL));
  assert(!((host == NULL) && (network == NULL)));
  void*entity = (host == NULL) ? (void*)network : (void*)host;
  struct nsbe_dijkstra_vertex_s*v = puk_hashtable_lookup(vertices, entity);
  if(v == NULL)
    {
      v = padico_malloc(sizeof(struct nsbe_dijkstra_vertex_s));
      v->weight = NSBE_UNREACHABLE;
      v->host = host;
      v->network = network;
      v->previous = NULL;
      v->outputs = nsbe_dijkstra_vertex_vect_new();
      puk_hashtable_insert(vertices, entity, v);
    }
  return v;
}

/** builds the routing graph from the known topology */
static struct nsbe_dijkstra_s*nsbe_dijkstra_init(padico_topo_host_t dest)
{
  struct nsbe_dijkstra_s*d = padico_malloc(sizeof(struct nsbe_dijkstra_s));
  d->done = nsbe_dijkstra_vertex_vect_new();
  d->todo = nsbe_dijkstra_vertex_vect_new();
  puk_hashtable_t vertices = puk_hashtable_new_ptr();
  /* add local and dest hosts */
  d->dest_v = nsbe_dijkstra_vertex_add(vertices, dest, NULL);
  d->local_v = nsbe_dijkstra_vertex_add(vertices, padico_topo_getlocalhost(), NULL);
  d->local_v->weight = 0;
  /* add known hosts running sshd */
  padico_topo_host_vect_t hosts = padico_topo_gethosts();
  padico_topo_host_vect_itor_t h;
  puk_vect_foreach(h, padico_topo_host, hosts)
    {
      if(padico_topo_host_getservice(*h, "ssh"))
        {
          nsbe_dijkstra_vertex_add(vertices, *h, NULL);
        }
    }
  padico_topo_host_vect_delete(hosts);
  /* add known networks */
  padico_topo_network_vect_t networks = padico_topo_networks_getbyfamily(AF_INET);
  padico_topo_network_vect_itor_t n;
  puk_vect_foreach(n, padico_topo_network, networks)
    {
      struct nsbe_dijkstra_vertex_s*v_net = nsbe_dijkstra_vertex_add(vertices, NULL, *n);
      struct padico_topo_host_vect_s hosts;
      padico_topo_host_vect_init(&hosts);
      padico_topo_network_gethosts(v_net->network, &hosts);
      padico_topo_host_vect_itor_t h;
      puk_vect_foreach(h, padico_topo_host, &hosts)
        {
          struct nsbe_dijkstra_vertex_s*v_host = puk_hashtable_lookup(vertices, *h);
          if(v_host)
            {
              if(padico_topo_host_can_input(*h, *n, 22, IPPROTO_TCP))
                {
                  nsbe_dijkstra_vertex_vect_push_back(v_net->outputs, v_host);
                }
              if(padico_topo_host_can_output(*h, *n, 22, IPPROTO_TCP))
                {
                  nsbe_dijkstra_vertex_vect_push_back(v_host->outputs, v_net);
                }
            }
        }
      padico_topo_host_vect_destroy(&hosts);
      padico_topo_network_vect_itor_t n2;
      puk_vect_foreach(n2, padico_topo_network, networks)
        {
          struct nsbe_dijkstra_vertex_s*v_net2 = nsbe_dijkstra_vertex_add(vertices, NULL, *n2);
          if(padico_topo_network_can_input(*n2, *n, 22, IPPROTO_TCP))
            {
              nsbe_dijkstra_vertex_vect_push_back(v_net->outputs, v_net2);
            }
          if(padico_topo_network_can_output(*n2, *n, 22, IPPROTO_TCP))
            {
              nsbe_dijkstra_vertex_vect_push_back(v_net2->outputs, v_net);
            }
        }
    }
  padico_topo_network_vect_delete(networks);
  /* flatten hashtable */
  puk_hashtable_enumerator_t e;
  e = puk_hashtable_enumerator_new(vertices);
  struct nsbe_dijkstra_vertex_s*v;
  puk_hashtable_enumerator_next2(e, NULL, &v);
  while(v)
    {
      nsbe_dijkstra_vertex_vect_push_back(d->todo, v);
      puk_hashtable_enumerator_next2(e, NULL, &v);
    }
  puk_hashtable_enumerator_delete(e);
  puk_hashtable_delete(vertices, NULL);
  return d;
}

static struct nsbe_dijkstra_vertex_s*nsbe_dijkstra_getmin(struct nsbe_dijkstra_s*d)
{
  int min = NSBE_UNREACHABLE;
  struct nsbe_dijkstra_vertex_s*min_v = NULL;
  nsbe_dijkstra_vertex_vect_itor_t i;
  puk_vect_foreach(i, nsbe_dijkstra_vertex, d->todo)
    {
      struct nsbe_dijkstra_vertex_s*v = *i;
      if((min == NSBE_UNREACHABLE) || ((v->weight < min) && (v->weight != NSBE_UNREACHABLE)))
        {
          min = v->weight;
          min_v = v;
        }
    }
  i = nsbe_dijkstra_vertex_vect_find(d->todo, min_v);
  nsbe_dijkstra_vertex_vect_erase(d->todo, i);
  nsbe_dijkstra_vertex_vect_push_back(d->done, min_v);
  return min_v;
}

static void nsbe_dijkstra_destroy(struct nsbe_dijkstra_s*d)
{
  while(!nsbe_dijkstra_vertex_vect_empty(d->todo))
    {
      struct nsbe_dijkstra_vertex_s*v = nsbe_dijkstra_vertex_vect_pop_back(d->todo);
      nsbe_dijkstra_vertex_vect_delete(v->outputs);
      padico_free(v);
    }
  nsbe_dijkstra_vertex_vect_delete(d->todo);
  while(!nsbe_dijkstra_vertex_vect_empty(d->done))
    {
      struct nsbe_dijkstra_vertex_s*v = nsbe_dijkstra_vertex_vect_pop_back(d->done);
      nsbe_dijkstra_vertex_vect_delete(v->outputs);
      padico_free(v);
    }
  nsbe_dijkstra_vertex_vect_delete(d->done);
  padico_free(d);
}

int ns_besteffort_selector_dijkstra(padico_string_t hosts, padico_string_t users,
                                    padico_topo_host_t dest, padico_topo_network_t*dest_n)
{
  int rc = 0;
  struct nsbe_dijkstra_s*d = nsbe_dijkstra_init(dest);
  while(!nsbe_dijkstra_vertex_vect_empty(d->todo))
    {
      struct nsbe_dijkstra_vertex_s*v = nsbe_dijkstra_getmin(d);
      padico_out(10, "loop min = %d (%p [%s])\n", v?v->weight:NSBE_UNREACHABLE, v,
                 (v && v->host)?padico_topo_host_getname(v->host):((v && v->network)?padico_topo_network_getname(v->network):"-"));
      if(v == d->dest_v && padico_topo_host_getservice(dest, "ssh"))
        {
          padico_out(10, "reached destination host %s.\n", padico_topo_host_getname(v->host));
          break;
        }
      if(v && v->network)
        {
          /* check whether network (previous host) can connect to destination */
          if(padico_topo_host_can_input(dest, v->network, PORT_ANY, IPPROTO_TCP) &&
             padico_topo_host_can_output(v->previous->host, v->network, PORT_ANY, IPPROTO_TCP))
            {
              padico_out(10, "network %s (%p) can reach destination node.\n", padico_topo_network_getname(v->network), v);
              d->dest_v->weight = v->weight + 1;
              d->dest_v->previous = v;
              break;
            }
        }
      if(v == d->dest_v)
        {
          padico_out(10, "destination host without ssh service. Skip.\n");
          continue;
        }
      nsbe_dijkstra_vertex_vect_itor_t i;
      puk_vect_foreach(i, nsbe_dijkstra_vertex, v->outputs)
        {
          struct nsbe_dijkstra_vertex_s*v2 = *i;
          if((v2->weight > v->weight + 1) || (v2->weight == NSBE_UNREACHABLE))
            {
              v2->weight = v->weight + 1;
              v2->previous = v;
            }
        }
    }
  padico_out(10, "exit loop- weight = %d. Rolling back route:\n", d->dest_v->weight);
  if(d->dest_v->weight != NSBE_UNREACHABLE)
    {
      /* build route from annotated graph */
      nsbe_dijkstra_vertex_vect_t route = nsbe_dijkstra_vertex_vect_new();
      assert(d->dest_v->previous != NULL);
      *dest_n = d->dest_v->previous->network;
      struct nsbe_dijkstra_vertex_s*v = d->dest_v;
      while(v->previous)
        {
          if(v->host)
            {
              padico_out(40, " - push host %s (%p)\n", padico_topo_host_getname(v->host), v);
              nsbe_dijkstra_vertex_vect_push_back(route, v);
            }
          else if(v->network)
            {
              padico_out(40, " - network %s (%p)\n", padico_topo_network_getname(v->network), v);
            }
          assert(v->previous != v);
          v = v->previous;
        }
      padico_out(40, " - source host %s (%p)\n", padico_topo_host_getname(v->host), v);
      assert(v == d->local_v);
      /* build ssh config string from route */
      struct nsbe_dijkstra_vertex_s**i;
      puk_vect_foreach_reverse(i, nsbe_dijkstra_vertex, route)
        {
          v = *i;
          if(v->host != padico_topo_getlocalhost() && padico_topo_host_getservice(v->host, "ssh"))
            {
              const char*user = padico_topo_host_getproperty(v->host, "login");
              if(padico_string_size(hosts) != 0)
                {
                  padico_string_catf(hosts, ",");
                  padico_string_catf(users, ",");
                }
              padico_string_catf(hosts, "%s", padico_topo_host_getname2(v->host, v->previous->network));
              padico_string_catf(users, "%s", user?user:getenv("USER"));
            }
        }
      nsbe_dijkstra_vertex_vect_delete(route);
      padico_out(40, "hosts: %s\n", padico_string_get(hosts));
      padico_out(40, "users: %s\n", padico_string_get(users));
      rc = 1;
    }
  else
    {
      padico_warning("no route found.\n");
    }
  nsbe_dijkstra_destroy(d);
  return rc;
}
