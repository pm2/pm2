/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VLink-Selector component
 * @ingroup VIO
 */

#include "VLink-selector-internals.h"
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/NetSelector.h>
#include <Padico/Puk-hashtable.h>

static int padico_vlink_selector_init(void);

PADICO_MODULE_DECLARE(VLink_selector, padico_vlink_selector_init, NULL, NULL, "VIO", "PadicoControl");


static int  vlink_selector_create (padico_vnode_t);
static int  vlink_selector_bind   (padico_vnode_t);
static int  vlink_selector_connect(padico_vnode_t);
static int  vlink_selector_listen (padico_vnode_t);
static void vlink_selector_fullshutdown(padico_vnode_t);
static int  vlink_selector_close(padico_vnode_t);

/** driver which incarnates the VLink_selector component */
static const struct padico_vlink_driver_s vlink_selector_driver =
  {
    .create       = vlink_selector_create,
    .bind         = vlink_selector_bind,
    .listen       = vlink_selector_listen,
    .connect      = vlink_selector_connect,
    .fullshutdown = vlink_selector_fullshutdown,
    .close        = vlink_selector_close,
    .send_post    = NULL,
    .recv_post    = NULL,
    .dumpstat     = NULL
  };

/* ********************************************************* */

PUK_VECT_TYPE(padico_vnode, padico_vnode_t);

struct vlink_selector_specific_s
{
  struct padico_vnode_vect_s sub_vnodes;
  padico_vnode_t      primary_vnode;   /**< shortcut to first sub-vnode */
};
VLINK_CREATE_SPECIFIC_ACCESSORS(selector);

/* XXX ------------------------------------------------------ */
static const char default_node_profile[] = "default"; /**< default profile if the peer is a node */
static const char default_host_profile[] = "inet";    /**< default profile if the peer is a host */
static const int  default_proto = IPPROTO_TCP;

/* XXX ------------------------------------------------------ */


/* ********************************************************* */

/* *** helper functions */

static void vlink_selector_client_notifier(padico_vnode_t vnode, vobs_attr_t attr, void*key);
static void vlink_selector_server_notifier(padico_vnode_t vnode, vobs_attr_t attr, void*key);
static int  vlink_selector_create_server(padico_vnode_t server_vnode, puk_component_t assembly,
                                         const struct padico_vaddr_s*src_addr,
                                         struct padico_vaddr_s*new_vaddr);

/* ** assembly request */

struct vlink_assembly_request_s
{
  struct padico_vaddr_s src_addr;
  struct sockaddr*addr;
  socklen_t addrlen;
  int proto; /* IPPROTO_TCP, IPPROTO_UDP, ... 0 for none */
  puk_component_t assembly;
};

static padico_rc_t vlink_selector_process_request(struct vlink_assembly_request_s*as_req);

static padico_vaddr_t vlink_selector_assembly_request(const struct vlink_assembly_request_s*as_req,
                                                      padico_topo_node_t remote_peer);

static void assembly_request_end_handler(puk_parse_entity_t e);
static void vaddr_end_handler(puk_parse_entity_t e);

/* ********************************************************* */

static int padico_vlink_selector_init(void)
{
  /* register the VLink_selector component */
  puk_component_declare("VLink_selector",
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink", "vlink", &vlink_selector_driver));
  puk_xml_add_action((struct puk_tag_action_s) {
    .xml_tag        = "VLink_selector:assembly-request",
    .start_handler  = NULL,
    .end_handler    = &assembly_request_end_handler,
    .required_level = PUK_TRUST_CONTROL,
    .help           = "Used internally by VLink_selector"
  });
  puk_xml_add_action((struct puk_tag_action_s) {
    .xml_tag        = "VLink_selector:vaddr",
    .start_handler  = NULL,
    .end_handler    = &vaddr_end_handler,
    .required_level = PUK_TRUST_CONTROL,
    .help           = "Used internally by VLink_selector"
    });

  padico_vlink_resolver_init();

  return 0;
}

/* ********************************************************* */
/* *** Assembly request ************************************ */

/* ** Assembly request XML structure:
 *
  <VLink_selector:assembly-request>
    <VLink_selector:vaddr> [ destination endpoint ] </VLink_selector:vaddr>
    <VLink_selector:vaddr> [ source endpoint (optional) ] </VLink_selector:vaddr>
    <puk:assembly>
       [ assembly as serialized by PadicoComponents ]
    </puk:assembly>
  </VLink_selector:assembly-request>
 */

static padico_vaddr_t vlink_selector_assembly_request(const struct vlink_assembly_request_s*as_req,
                                                      padico_topo_node_t remote_peer)
{
  padico_string_t s = padico_string_new();
  struct padico_vaddr_s req_vaddr =
    {
      .addr    = as_req->addr,
      .addrlen = as_req->addrlen,
      .proto   = as_req->proto
    };
  padico_string_t s_addr = padico_vaddr_marshall(&req_vaddr);

  padico_string_t name = padico_string_new();
  padico_string_catf(name, "_vlink-selector:_n(%s):_x(%p):%s", padico_topo_nodename(), as_req->assembly, as_req->assembly->name);
  padico_string_t as = puk_component_serialize_id(as_req->assembly, padico_string_get(name));
  padico_string_delete(name);
  padico_string_cat(s, "<VLink_selector:assembly-request>\n");
  padico_string_catf(s, "  <VLink_selector:vaddr>%s</VLink_selector:vaddr>\n",
                    padico_string_get(s_addr));
  padico_string_delete(s_addr);
  if(as_req->src_addr.addr)
    {
      s_addr = padico_vaddr_marshall(&as_req->src_addr);
      padico_string_catf(s, "  <VLink_selector:vaddr endpoint=\"src\">%s</VLink_selector:vaddr>\n",
                         padico_string_get(s_addr));
      padico_string_delete(s_addr);
    }
  padico_string_cat(s, padico_string_get(as));
  padico_string_cat(s, "</VLink_selector:assembly-request>\n");
  padico_string_delete(as);
  padico_req_t req  = padico_control_send(remote_peer, padico_string_get(s));
  padico_rc_t reply = padico_tm_req_wait(req); /**< @note This is an unbounded wait, which is
                                                   * contrary to the asynchronous philosophy of VLink.  */
  padico_string_delete(s);
  padico_vaddr_t reply_vaddr = NULL;
  if(reply != NULL)
    {
      padico_out(40, "got reply for assembly request.\n");
      reply_vaddr = padico_vaddr_unmarshall(padico_rc_gettext(reply));
      padico_rc_delete(reply);
    }
  else
    {
      padico_warning("got empty reply for assembly request.\n");
    }
  return reply_vaddr;
}

static void assembly_request_end_handler(puk_parse_entity_t e)
{
  struct vlink_assembly_request_s*as_req = padico_malloc(sizeof(struct vlink_assembly_request_s));
  as_req->addr     = NULL;
  as_req->addrlen  = -1;
  as_req->proto    = 0;
  as_req->src_addr.addr = NULL;
  as_req->assembly = NULL;

  const puk_parse_entity_vect_t v = puk_parse_get_contained(e);
  padico_rc_t rc = NULL;
  puk_parse_entity_t*_i;
  puk_vect_foreach(_i, puk_parse_entity, v)
    {
      const puk_parse_entity_t i = *_i;
      if(puk_parse_is(i, "VLink_selector:vaddr"))
        {
          const padico_vaddr_t vaddr = puk_parse_get_content(i);
          if(!as_req->addr)
            {
              as_req->addr    = vaddr->addr;
              as_req->addrlen = vaddr->addrlen;
              as_req->proto   = vaddr->proto;
            }
          else if(!as_req->src_addr.addr)
            {
              as_req->src_addr = *vaddr;
            }
          else
            {
              rc = padico_rc_error("duplicate address in assembly-request.");
              goto error;
            }
          padico_free(vaddr);
        }
      else if((puk_parse_is(i, "puk:composite") || puk_parse_is(i, "puk:component"))
              && !as_req->assembly)
        {
          const puk_component_t assembly = puk_parse_get_content(i);
          as_req->assembly = assembly;
        }
      else
        {
          rc = padico_rc_error("unexpected entity in assembly-request.");
          goto error;
        }
    }
  rc = vlink_selector_process_request(as_req);
 error:
  padico_free(as_req);
  puk_parse_set_rc(e, rc);
}

static void vaddr_end_handler(puk_parse_entity_t e)
{
  padico_vaddr_t vaddr = padico_vaddr_unmarshall(e->text);
  puk_parse_set_content(e, vaddr);
}

/* ********************************************************* */

/** Process an incoming assembly request from the control channel.
 */
static padico_rc_t vlink_selector_process_request(struct vlink_assembly_request_s*as_req)
{
  padico_rc_t rc = NULL;
  padico_vnode_t server_vnode =
    vlink_registry_lookup(as_req->addr, as_req->addrlen, as_req->proto);
  puk_component_t assembly = as_req->assembly;
  padico_out(40, "creating serial assembly from request for vnode %p.\n", server_vnode);
  if(server_vnode != NULL)
    {
      struct padico_vaddr_s new_vaddr;
      const struct padico_vaddr_s*src_addr = (as_req->src_addr.addr)?&as_req->src_addr:NULL;
      int err = vlink_selector_create_server(server_vnode, assembly, src_addr, &new_vaddr);
      if(err)
        {
          rc = padico_rc_error("error %d in auto-bind", err);
        }
      else
        {
          /* serialize the address address of sub-vnode */
          padico_string_t s_vaddr = padico_vaddr_marshall(&new_vaddr);
          padico_out(40, "sending new vaddr: %s\n", padico_string_get(s_vaddr));
          rc = padico_rc_msg(padico_string_get(s_vaddr));
          padico_string_delete(s_vaddr);
        }
    }
  else
    {
      padico_warning("received assembly request for address not in registry!\n");
      rc = padico_rc_error("Unknown address");
    }
  return rc;
}


/* ********************************************************* */
/* *** Interface: 'VLink' for component: 'VLink_selector' **** */

static int vlink_selector_create(padico_vnode_t vnode)
{
  padico_out(40, "vnode=%p\n", vnode);
  /* create primary vnode */
  const puk_component_t assembly = padico_ns_host_selector(NULL, default_host_profile, puk_iface_VLink(), 0, 0);
  const puk_instance_t instance = puk_component_instantiate(assembly);
  struct puk_receptacle_VLink_s r;
  puk_instance_indirect_VLink(instance, NULL, &r);
  padico_vnode_t primary_vnode = r._status;
  int rc = padico_vsock_create(primary_vnode,
                               vnode->socket.so_family, vnode->socket.so_type, vnode->socket.so_protocol);
  padico_out(40, "created primary vnode=%p for selector vnode=%p\n", primary_vnode, vnode);
  /* activate and fill vobs_selector */
  vobs_selector_activate(vnode);
  vobs_selector(vnode)->primary_vnode = primary_vnode;
  padico_vnode_vect_init(&vobs_selector(vnode)->sub_vnodes);
  padico_vnode_vect_push_back(&vobs_selector(vnode)->sub_vnodes, primary_vnode);
  return rc;
}

static int vlink_selector_bind(padico_vnode_t vnode)
{
  padico_vnode_t primary_vnode = vobs_selector(vnode)->primary_vnode;
  int rc = padico_vsock_bind(primary_vnode, vnode->binding.primary, vnode->binding.primary_len);
  if(!vnode->binding.primary)
    {
      vnode->binding.primary = padico_malloc(primary_vnode->binding.primary_len);
    }
  memcpy(vnode->binding.primary, primary_vnode->binding.primary, primary_vnode->binding.primary_len);
  vnode->binding.primary_len = primary_vnode->binding.primary_len;
  vlink_registry_register(vnode, vnode->binding.primary,
                          vnode->binding.primary_len, default_proto);
  /* XXX (default_proto) */
  return rc;
}

static int vlink_selector_connect(padico_vnode_t root_vnode)
{
  int rc = -1;
  puk_component_t assembly = NULL;
  padico_topo_node_t peer_node = NULL;
  padico_topo_host_t peer_host = NULL;
  padico_vaddr_t wanted_vaddr = padico_vaddr_new(root_vnode->remote.remote_addr,
                                                 root_vnode->remote.remote_addrlen,
                                                 default_proto);
  padico_out(40, "vnode=%p\n", root_vnode);
  /* Step 1- resolve vnode->remote.addr into a peer node/host. */
  peer_node = vlink_resolver_resolve_node(wanted_vaddr);
  if(peer_node)
    {
      /* Step 2- resolve destination node into an assembly */
      padico_out(40, "vnode=%p; destination is a node.\n", root_vnode);
      assembly = padico_ns_serial_selector(peer_node, default_node_profile, puk_iface_VLink());
    }
  else
    {
      /* unknown node, try to resolve address as host. */
      peer_host = vlink_resolver_resolve_host(wanted_vaddr);
      if(peer_host)
        {
          /* Step 2- resolve destination host into an assembly */
          padico_out(40, "vnode=%p; destination is a host.\n", root_vnode);
          uint16_t port = 0;
          if(root_vnode->remote.remote_addr->sa_family == AF_INET)
            port = ((struct sockaddr_in*)root_vnode->remote.remote_addr)->sin_port;
          else if (root_vnode->remote.remote_addr->sa_family == AF_INET6)
            port = ((struct sockaddr_in6*)root_vnode->remote.remote_addr)->sin6_port;
          assembly = padico_ns_host_selector(peer_host, default_host_profile, puk_iface_VLink(), port, default_proto);
        }
      else
        {
          padico_out(40, "vnode=%p; destination cannot be resolved- returning EHOSTUNREACH.\n", root_vnode);
          vnode_set_error(root_vnode, EHOSTUNREACH);
          return -1;
        }
    }
  vnode_show_assembly(assembly, TOP_DOWN, "outgoing connection");

  /* Step 3- create the assembly localy */
  padico_vnode_t new_vnode = NULL;
  padico_vnode_vect_itor_t i;
  puk_vect_foreach(i, padico_vnode, &vobs_selector(root_vnode)->sub_vnodes)
    {
      if((*i)->assembly_stuff.instance->component == assembly)
        {
          /* selected assembly is already existing- keep it. */
          padico_out(30, "vnode=%p has already assembly=%s as primary\n",
                     root_vnode, assembly->name);
          new_vnode = *i;
          padico_vnode_vect_erase(&vobs_selector(root_vnode)->sub_vnodes, i);
          break;
        }
    }
  if(!new_vnode)
    {
      /* unknown selected component- instantiate. */
      const int family   = root_vnode->socket.so_family;
      const int type     = root_vnode->socket.so_type;
      const int protocol = root_vnode->socket.so_protocol;
      puk_instance_t new_instance = puk_component_instantiate(assembly);
      struct puk_receptacle_VLink_s r;
      puk_instance_indirect_VLink(new_instance, NULL, &r);
      new_vnode = r._status;
      padico_vsock_create(new_vnode, family, type, protocol);
    }
  vnode_show_vlink(new_vnode, TOP_DOWN, "outgoing connection");

  /* Step 3.1- pivot the root_vnode to new_vnode */
  padico_out(10, "pivoting- root_vnode=%p; new_vnode=%p\n", root_vnode, new_vnode);
  vnode_activate(root_vnode, pivot);
  root_vnode->pivot.new_vnode = new_vnode;
  vobs_signal(root_vnode, vobs_attr_pivot);

  if(new_vnode->socket.capabilities & padico_vsock_caps_clientmustbind)
    {
      padico_warning("vnode=%p 'client-must-bind' property set- performing autobind().\n", new_vnode);
      vnode_lock(new_vnode);
      padico_vsock_autobind(new_vnode);
      vnode_unlock(new_vnode);
    }

  /* Step 4- [optional] send the assembly to the remote peer and get server vaddr */
  if(peer_node && !(new_vnode->socket.capabilities & padico_vsock_caps_directconnect))
    {
      padico_out(40, "sending the assembly request to the peer node...\n");
      const struct vlink_assembly_request_s as_req =
        {
          .src_addr  = new_vnode->content.binding?
          (struct padico_vaddr_s)
          {
            .addr    = new_vnode->binding.primary,
            .addrlen = new_vnode->binding.primary_len,
            .proto   = new_vnode->socket.so_protocol
          }:
          (struct padico_vaddr_s)
          {
            .addr    = NULL,
            .addrlen = 0,
            .proto   = -1
          },
          .addr      = root_vnode->remote.remote_addr,
          .addrlen   = root_vnode->remote.remote_addrlen,
          .proto     = default_proto, /* XXX */
          .assembly  = assembly
        };
      const padico_vaddr_t secondary_vaddr = vlink_selector_assembly_request(&as_req, peer_node);
      if(!secondary_vaddr)
        {
          vnode_set_error(new_vnode, EADDRNOTAVAIL);
          return -1;
        }
      /* prepare address rewriting with original destination vaddr */
      vobs_subscribe(new_vnode, &vlink_selector_client_notifier, wanted_vaddr);
      vnode_lock(new_vnode);
      rc = padico_vsock_active_connect(new_vnode, secondary_vaddr->addr, secondary_vaddr->addrlen);
      vnode_unlock(new_vnode);
      padico_vaddr_delete(secondary_vaddr);
    }
  else
    {
      vnode_lock(new_vnode);
      rc = padico_vsock_active_connect(new_vnode, wanted_vaddr->addr, wanted_vaddr->addrlen);
      vnode_unlock(new_vnode);
      padico_vaddr_delete(wanted_vaddr);
    }
  return rc;
}

static int vlink_selector_listen(padico_vnode_t vnode)
{
  vobs_subscribe(vobs_selector(vnode)->primary_vnode, &vlink_selector_server_notifier, vnode);
  int rc = padico_vsock_passive_connect(vobs_selector(vnode)->primary_vnode,
                                        vnode->listener.backlog_size, vnode->listener.src_addr);
  return rc;
}

static void vlink_selector_fullshutdown(padico_vnode_t vnode)
{
  padico_vnode_vect_itor_t i;
  vlink_registry_unregister(vnode);
  for(i  = padico_vnode_vect_begin(&vobs_selector(vnode)->sub_vnodes);
      i != padico_vnode_vect_end(&vobs_selector(vnode)->sub_vnodes);
      i  = padico_vnode_vect_next(i))
    {
      padico_vnode_t v = *i;
      vnode_lock(v);
      vobs_unsubscribe(v);
      padico_vsock_fullshutdown(v);
      vnode_unlock(v);
    }
}

static int vlink_selector_close(padico_vnode_t vnode)
{
  int rc = 0;
  padico_vnode_vect_itor_t i;
  vlink_registry_unregister(vnode);
  for(i  = padico_vnode_vect_begin(&vobs_selector(vnode)->sub_vnodes);
      i != padico_vnode_vect_end(&vobs_selector(vnode)->sub_vnodes);
      i  = padico_vnode_vect_next(i))
    {
      padico_vnode_t v = *i;
      vnode_lock(v);
      const struct padico_vlink_driver_s*driver = padico_vnode_get_vlink_driver(v);
      if(driver && driver->close)
        {
          rc = (*(driver->close))(v);
        }
      vnode_unlock(v);
    }
  padico_vnode_vect_destroy(&vobs_selector(vnode)->sub_vnodes);
  vobs_selector_deactivate(vnode);
  return rc;
}


/* ********************************************************* */
/* *** Helper functions ************************************ */

static void vlink_selector_client_notifier(padico_vnode_t vnode, vobs_attr_t attr, void*key)
{
  padico_vaddr_t remote_vaddr = key;
  padico_out(20, "vnode=%p; client notifier for event *%s*\n", vnode, vobs_attr_label[attr]);
  switch(attr)
    {
    case vobs_attr_remote:
      /* re-write binding and peer address */
      if(vnode->remote.established)
        {
          padico_out(20, "vnode=%p; connection established.\n", vnode);
          if(vnode->remote.remote_addr->sa_family == AF_PADICO)
            {
              /* re-write with saved address */
              padico_out(20, "re-write peer address in client.\n");
              padico_free(vnode->remote.remote_addr);
              vnode->remote.remote_addr    = remote_vaddr->addr;
              vnode->remote.remote_addrlen = remote_vaddr->addrlen;
            }
          if(vnode->binding.primary && vnode->binding.primary->sa_family == AF_PADICO)
            {
              struct sockaddr_in*inaddr = padico_malloc(sizeof(struct sockaddr_in));
              padico_out(20, "re-write local binding in client.\n");
              inaddr->sin_family = AF_INET;
              inaddr->sin_addr =
                *padico_topo_host_getaddr(padico_topo_getlocalhost());
              inaddr->sin_port = htons(-1); /* XXX TODO faked port */
              padico_free(vnode->binding.primary);
              vnode->binding.primary     = (struct sockaddr*)inaddr;
              vnode->binding.primary_len = sizeof(struct sockaddr_in);
            }

          vobs_unsubscribe(vnode);
        }
      break;
    case vobs_attr_status:
      /* ignore status notifications */
      break;
    default:
      padico_fatal("unexpected attr %d (%s) in selector client notifier.\n",
                   attr, vobs_attr_label[attr]);
      break;
    }
}

static void vlink_selector_server_notifier(padico_vnode_t vnode, vobs_attr_t attr, void*key)
{
  padico_vnode_t selector_vnode = key;
  switch(attr)
    {
    case vobs_attr_listener:
      /* get the new connection in sub-vnode and forward it to father vnode */
       if(!padico_vnode_queue_empty(vnode->listener.backlog))
        {
          padico_vnode_t newsock = padico_vnode_queue_retrieve(vnode->listener.backlog);
          padico_out(40, "vnode=%p; got connection\n", vnode);
          vnode_show_vlink(newsock, BOTTOM_UP, "incoming connection");
          if(newsock->binding.primary->sa_family == AF_PADICO)
            {
              /* re-write the binding of the servant socket */
              struct sockaddr_in*inaddr = padico_malloc(sizeof(struct sockaddr_in));
              padico_out(20, "re-write local binding of servant\n");
              inaddr->sin_family = AF_INET;
              inaddr->sin_addr =
                *padico_topo_host_getaddr(padico_topo_getlocalhost());
              inaddr->sin_port = htons(-1); /* XXX TODO faked port */
              padico_free(newsock->binding.primary);
              newsock->binding.primary = (struct sockaddr*)inaddr;
              newsock->binding.primary_len = sizeof(struct sockaddr_in);
            }
          if(newsock->remote.remote_addr->sa_family == AF_PADICO)
            {
              /* re-write the address of the peer */
              struct sockaddr_in*inaddr = padico_malloc(sizeof(struct sockaddr_in));
              padico_warning("re-write peer address in servant --TODO-- fake address\n");
              inaddr->sin_family = AF_INET;
              inaddr->sin_addr.s_addr = INADDR_ANY; /* XXX TODO */
              inaddr->sin_port   = htons(-1); /* XXX */
              padico_free(newsock->remote.remote_addr);
              newsock->remote.remote_addr = (struct sockaddr*)inaddr;
              newsock->remote.remote_addrlen = sizeof(struct sockaddr_in);
            }
          vnode_lock(selector_vnode);
          padico_vnode_queue_append(selector_vnode->listener.backlog, newsock);
          padico_out(20, "vnode=%p- signaling...\n", vnode);
          vobs_signal(selector_vnode, vobs_attr_listener);
          padico_out(20, "vnode=%p- exit\n", vnode);
          vnode_unlock(selector_vnode);
        }
       else
         {
           padico_warning("empty backlog in selector server notifier.\n");
         }
      break;
    case vobs_attr_status:
      /* ignore status notifications */
      break;
    default:
      padico_fatal("unexpected attr %d (%s) in selector server notifier.\n",
                   attr, vobs_attr_label[attr]);
      break;
    }
}

/** Create a new assembly and turn it into a server vlink.
 * @note This function performs 'auto-bind' which leads to a
 * new vaddr which is different from the address the server
 * vnode was listening to.
 */
static int vlink_selector_create_server(padico_vnode_t server_vnode, puk_component_t assembly,
                                        const struct padico_vaddr_s*src_addr,
                                        struct padico_vaddr_s*new_vaddr)
{
  int err = -1;
  /* 1- build a serial assembly from 'assembly' */
  puk_instance_t vlink = puk_component_instantiate(assembly);
  struct puk_receptacle_VLink_s r;
  puk_instance_indirect_VLink(vlink, NULL, &r);
  padico_vnode_t new_vnode = r._status;
  err = padico_vsock_create(new_vnode,
                            server_vnode->socket.so_family,
                            server_vnode->socket.so_type,
                            server_vnode->socket.so_protocol);
  if(err)
    goto exit_nolock;
  if(new_vnode->socket.capabilities & padico_vsock_caps_clientonly)
    {
      padico_warning("trying to create a server socket with a client-only assembly.\n");
      err = -EOPNOTSUPP;
      goto exit_nolock;
    }

  vnode_show_vlink(new_vnode, BOTTOM_UP, "server connection");
  /* 2- register the new vnode as a sub-vnode of the server vnode */
  padico_vnode_vect_push_back(&vobs_selector(server_vnode)->sub_vnodes, new_vnode);
  /* 3- bind */
  padico_out(40, "vnode=%p; binding as new sub-vnode\n", new_vnode);
  vnode_lock(new_vnode);
  err = padico_vsock_autobind(new_vnode);
  if(err)
    goto exit;
  /* 4- get address */
  new_vaddr->addr    = new_vnode->binding.primary;
  new_vaddr->addrlen = new_vnode->binding.primary_len;
  new_vaddr->proto   = default_proto; /* XXX */
  /* 5- set call-back on new vnode to forward signals to its father selector vnode */
  padico_out(30, "vnode=%p adding notifier\n", new_vnode);
  vobs_subscribe(new_vnode, &vlink_selector_server_notifier, server_vnode);
  /* 6- post a listen() */
  padico_vsock_passive_connect(new_vnode, server_vnode->listener.backlog_size, src_addr);
 exit:
  vnode_unlock(new_vnode);
 exit_nolock:
  return err;
}
