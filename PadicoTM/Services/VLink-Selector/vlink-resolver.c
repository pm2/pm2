/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Address resolver for VLink
 * @ingroup VIO
 */


#include "VLink-selector-internals.h"
#include <Padico/VLink-debug.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>
#include <Padico/PadicoControl.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>

PADICO_MODULE_HOOK(VLink_selector);

/** Describe a local binding [address <--> vnode]
 */
PUK_LIST_TYPE(vlink_binding,
              padico_vnode_t vnode; /**< local vnode serving this address */
              struct padico_vaddr_s vaddr;
              );

/** Registry for local vnode <-> address binding*/
static struct vlink_registry_s
{
  vlink_binding_list_t bindings;
  marcel_mutex_t lock;
} vlink_registry;

static int padico_vaddr_matches(const struct padico_vaddr_s*server,
                                const struct padico_vaddr_s*client);

static padico_topo_node_t vlink_control_resolve(const struct padico_vaddr_s*vaddr,
                                                padico_topo_node_vect_t candidates);

/* *** resolver ******************************************** */

/** @brief Resolve a vaddr into a node. Returns NULL if node not found.
 */
padico_topo_node_t vlink_resolver_resolve_node(const struct padico_vaddr_s*vaddr)
{
  padico_topo_node_t node = NULL;

  switch(vaddr->addr->sa_family)
    {
    case AF_INET:
      {
        struct sockaddr_in*inaddr = (struct sockaddr_in*)vaddr->addr;
        padico_topo_host_t host = padico_topo_host_getbyinaddr(&inaddr->sin_addr);

        if(host != NULL)
          {
            padico_topo_node_vect_t candidates = padico_topo_host_getnodes(host);
            if(!padico_topo_node_vect_empty(candidates))
              {
                node = vlink_control_resolve(vaddr, candidates);
              }
          }
      }
      break;

    case AF_INET6:
      {
        padico_warning("resolving node as IPv6 address.\n");
        struct sockaddr_in6*in6addr = (struct sockaddr_in6*)vaddr->addr;
        padico_topo_host_t host = padico_topo_host_getbyin6addr(&in6addr->sin6_addr);
        if(host != NULL)
          {
            padico_topo_node_vect_t candidates = padico_topo_host_getnodes(host);
            if(!padico_topo_node_vect_empty(candidates))
              node = vlink_control_resolve(vaddr, candidates);
          }
      }
      break;

    case AF_UNIX:
      {
        struct sockaddr_un*addr_un __attribute__((unused)) = (struct sockaddr_un*)vaddr->addr;
        padico_out(6, "VLink-resolver: family=AF_UNIX; path=**%s**\n", addr_un->sun_path);
        node = NULL;
      }
      break;

    default:
      padico_warning("trying to use address family %d -- not available\n",
                     vaddr->addr->sa_family);
      node = NULL;
    }
  return node;
}

/** @brief Resolve a vaddr into a host.
    Creates host if needed (interoperability).
    Returns NULL if host unreachable or address family not supported.
 */
padico_topo_host_t vlink_resolver_resolve_host(const struct padico_vaddr_s*vaddr)
{
  padico_topo_host_t host = NULL;

  switch(vaddr->addr->sa_family)
    {
    case AF_INET:
      {
        const struct in_addr*inaddr = &((struct sockaddr_in*)vaddr->addr)->sin_addr;
        host = padico_topo_host_resolve_byinaddr(inaddr);
      }
      break;
    case AF_INET6:
      {
        padico_warning("resolving host as IPv6 address.\n");
        const struct in6_addr*in6addr = &((struct sockaddr_in6*)vaddr->addr)->sin6_addr;
        host = padico_topo_host_resolve_byin6addr(in6addr);
      }
      break;
    case AF_UNIX:
      {
        host = padico_topo_getlocalhost();
      }
      break;
    default:
      host = NULL;
    }
  return host;
}

/* *** sockaddr toolbox ************************************ */

static int padico_vaddr_matches(const struct padico_vaddr_s*server,
                                const struct padico_vaddr_s*client)
{
  int matches = 0;
  if( /* (i->proto == proto) && */ /* XXX */
     (server->addrlen == client->addrlen) &&
     (server->addr->sa_family == client->addr->sa_family))
    {
      switch(server->addr->sa_family)
        {
        case AF_INET:
          {
            const struct sockaddr_in*const inaddr_c = (struct sockaddr_in*)client->addr;
            const struct sockaddr_in*const inaddr_s = (struct sockaddr_in*)server->addr;
            if( (inaddr_s->sin_port == inaddr_c->sin_port) &&
                ( (inaddr_s->sin_addr.s_addr == inaddr_c->sin_addr.s_addr) ||
                  (inaddr_s->sin_addr.s_addr == INADDR_ANY)) )
              {
                return 1;
              }
          }
          break;

        case AF_INET6:
          {
            const struct sockaddr_in6*const in6addr_c = (struct sockaddr_in6*)client->addr;
            const struct sockaddr_in6*const in6addr_s = (struct sockaddr_in6*)server->addr;
            return( (in6addr_s->sin6_port == in6addr_c->sin6_port) &&
                    ( (memcmp(&in6addr_s->sin6_addr, &in6addr_c->sin6_addr, sizeof(struct in6_addr))  == 0) ||
                      (memcmp(&in6addr_s->sin6_addr, &in6addr_any, sizeof(struct in6_addr)) == 0)) );
          }
          break;

        case AF_UNIX:
          {
            if(memcmp(server->addr, client->addr, server->addrlen) == 0)
              return 1;
          }
          break;

        default:
          padico_fatal("Looking for an unkown family of socket.\n");
        }
    }
  return matches;
}


/* *** VLink registry ************************************** */

static void vlink_registry_bind(padico_vnode_t vnode, struct sockaddr*addr, socklen_t addrlen, int proto)
{
  vlink_binding_t b = vlink_binding_new();
  b->vnode         = vnode;
  b->vaddr.proto   = proto;
  b->vaddr.addr    = addr;
  b->vaddr.addrlen = addrlen;
  marcel_mutex_lock(&vlink_registry.lock);
  vlink_binding_list_push_back(vlink_registry.bindings, b);
  marcel_mutex_unlock(&vlink_registry.lock);
}

void vlink_registry_register(padico_vnode_t vnode, const struct sockaddr*addr,
                             socklen_t addrlen, int proto)
{
  padico_out(40, "vnode=%p registering\n", vnode);
  switch(addr->sa_family)
    {
    case AF_INET:
      {
        /* normalize IPv4 address */
        struct sockaddr_in*inaddr = padico_malloc(addrlen);
        memcpy(inaddr, addr, addrlen);
        memset(inaddr->sin_zero, 0, sizeof(inaddr->sin_zero));
        vlink_registry_bind(vnode, (struct sockaddr*)inaddr, addrlen, proto);
        padico_out(40, "  register IPv4 addr = %s\n", inet_ntoa(inaddr->sin_addr));
      }
      break;

    case AF_INET6:
      {
        /* normalize IPv6 address */
        struct sockaddr_in6*in6addr = padico_malloc(addrlen);
        memcpy(in6addr, addr, addrlen);
        in6addr->sin6_flowinfo = 0;
        in6addr->sin6_scope_id = 0;
        vlink_registry_bind(vnode, (struct sockaddr*)in6addr, addrlen, proto);
        if(IN6_IS_ADDR_UNSPECIFIED(&in6addr->sin6_addr))
          {
            /* create wildcard IPv4 binding in dual stack mode */
            struct sockaddr_in*in4addr = padico_malloc(sizeof(struct sockaddr_in));
            in4addr->sin_family = AF_INET;
            in4addr->sin_port = in6addr->sin6_port;
            in4addr->sin_addr.s_addr = htonl(INADDR_ANY);
            memset(in4addr->sin_zero, 0, sizeof(in4addr->sin_zero));
            vlink_registry_bind(vnode, (struct sockaddr*)in4addr, sizeof(struct sockaddr_in), proto);
          }
      }
      break;
    }
}

void vlink_registry_unregister(padico_vnode_t vnode)
{
  marcel_mutex_lock(&vlink_registry.lock);
  vlink_binding_itor_t i = vlink_binding_list_begin(vlink_registry.bindings);
  while(i != vlink_binding_list_end(vlink_registry.bindings))
    {
      if(i->vnode == vnode)
        {
          vlink_binding_itor_t j = vlink_binding_list_next(i);
          vlink_binding_list_remove(vlink_registry.bindings, i);
          padico_free(i->vaddr.addr);
          vlink_binding_delete(i);
          i = j;
        }
      else
        {
          i  = vlink_binding_list_next(i);
        }
    }
  marcel_mutex_unlock(&vlink_registry.lock);
}

padico_vnode_t vlink_registry_lookup(const struct sockaddr*addr, socklen_t addrlen, int proto)
{
  padico_vnode_t vnode = NULL;
  const struct padico_vaddr_s vaddr =
    {
      .addr    = (struct sockaddr*)addr, /* discard 'const', but it is not changed anyway */
      .addrlen = addrlen,
      .proto   = proto
    };
  vlink_binding_itor_t i;
  padico_out(40, "looking up: addrlen=%d; addr family=%d; proto=%d\n",
             addrlen, addr->sa_family, proto);
  marcel_mutex_lock(&vlink_registry.lock);
  for(i  = vlink_binding_list_begin(vlink_registry.bindings);
      i != vlink_binding_list_end(vlink_registry.bindings);
      i  = vlink_binding_list_next(i))
    {
      padico_out(60, "trying: vnode=%p; addrlen=%d; addr family=%d; proto=%d\n",
                 i->vnode, i->vaddr.addrlen,
                 i->vaddr.addr->sa_family,
                 i->vaddr.proto);
      vlink_address_trace(i->vaddr.addr, i->vaddr.addrlen);
      if(padico_vaddr_matches(&i->vaddr, &vaddr))
        {
          vnode = i->vnode;
          padico_out(40, "vnode=%p matches\n", vnode);
          break;
        }
    }
  marcel_mutex_unlock(&vlink_registry.lock);
  return vnode;
}


static padico_topo_node_t vlink_control_resolve(const struct padico_vaddr_s*vaddr,
                                              padico_topo_node_vect_t candidates)
{
  padico_topo_node_vect_itor_t n;
  padico_reqs_vect_t reqs = padico_reqs_vect_new();
  padico_topo_node_t selected_node = NULL;
  padico_out(40, "resolving vaddr...\n");
  /* send requests to candidates */
  puk_vect_foreach(n, padico_topo_node, candidates)
    {
      const padico_topo_node_t node = *n;
      if(node == padico_topo_getlocalnode())
        {
          padico_reqs_vect_push_back(reqs, NULL); /* place-holder for self */
        }
      else
        {
          padico_string_t s_addr  = padico_vaddr_marshall(vaddr);
          padico_string_t request = padico_string_new();
          padico_string_catf(request, "<VLink-resolver-request>%s</VLink-resolver-request>\n",
                             padico_string_get(s_addr));
          padico_out(40, "sending request *%s* to node %s\n",
                     padico_string_get(request), padico_topo_node_getname(node));
          padico_req_t req = padico_control_send(node, padico_string_get(request));
          padico_string_delete(request);
          req->notify_key = node;
          padico_reqs_vect_push_back(reqs, req);
        }
    }
  /* wait for reply */
  padico_reqs_vect_itor_t r;
  puk_vect_foreach(r, padico_reqs, reqs)
    {
      padico_req_t req = *r;
      if(req != NULL)
        {
          /* padico_rc_t reply = padico_tm_req_wait(req); */
          padico_rc_t reply = NULL;
          struct timeval starttime;
          gettimeofday(&starttime, NULL);
          while(!req->completed)
            {
              struct timeval nowtime;
              gettimeofday(&nowtime, NULL);
              if( ((nowtime.tv_sec - starttime.tv_sec) * 1000000)
                  + (nowtime.tv_usec - starttime.tv_usec) > 1000000 )
                break;
            }
          if(req->completed)
            {
              reply = req->rc;
              padico_topo_node_t node = req->notify_key;
              padico_out(40, "got reply rc=%d from node %s.\n",
                         padico_rc_iserror(reply), padico_topo_node_getname(node));
              if(!padico_rc_iserror(reply))
                {
                  selected_node = node;
                }
              if(reply)
                {
                  padico_rc_delete(reply);
                }
            }
        }
    }
  padico_reqs_vect_delete(reqs);
  if(selected_node == NULL)
    {
      padico_out(40, "no matching node for this address.\n");
    }
  else
    {
      padico_out(40, "node=%s\n", padico_topo_node_getname(selected_node));
    }
  return selected_node;
}


/** Handler for <VLink-resolver-request/>
 */
static void resolver_request_end_handler(puk_parse_entity_t e)
{
  padico_vaddr_t vaddr = NULL;
  padico_vnode_t vnode = NULL;
  assert(e->text != NULL);
  padico_out(40, "received VLink-resolver request for vaddr: *%s*\n", e->text);
  vaddr = padico_vaddr_unmarshall(e->text);
  vnode = vlink_registry_lookup(vaddr->addr, vaddr->addrlen, vaddr->proto);
  if(vnode == NULL)
    {
      puk_parse_set_rc(e, padico_rc_error("address not managed by VLink"));
    }
  else
    {
      puk_parse_set_rc(e, padico_rc_ok());
    }
  padico_free(vaddr);
}

/* ********************************************************* */

void padico_vlink_resolver_init(void)
{
  /* init local registry */
  marcel_mutex_init(&vlink_registry.lock, NULL);
  vlink_registry.bindings = vlink_binding_list_new();

  puk_xml_add_action((struct puk_tag_action_s) {
    .xml_tag        = "VLink-resolver-request",
    .start_handler  = NULL,
    .end_handler    = &resolver_request_end_handler,
    .required_level = PUK_TRUST_CONTROL,
    .help           = "Used internally by VLink-resolver"
    });
}
