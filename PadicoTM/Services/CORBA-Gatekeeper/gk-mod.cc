/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico CORBA Gatekeeper
 * @ingroup CORBAGatekeeper
 */

/** @defgroup CORBAGatekeeper Service: CORBA-Gatekeeper -- server-side for CORBA remote control
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/Connector.h>
#include <Padico/Module.h>
#include <Padico/CORBA.h>
#include <Padico/COSNaming.h>

#include "gatekeeper.h"

#include <string.h>
//#include <strstream>
#include <iostream>

/*
 * Padico/Nodes/<host1fqdn>:0
 *                      ...:1
 *          .../<host2fqdn>:0
 *                      ...:1
 * Padico/Shortcuts/<host1> -> Padico/Nodes/<host1fqdn>:0
 *                 /<host1>:1 -> Padico/Nodes/<host2fqdn>:1
 */

static int  gk_init(void);
static int  gk_start(void);
static int  gk_corba_init(void);
static int gk_run(int argc, char**argv);
static void gk_stop(void);

PADICO_MODULE_DECLARE(CORBA_Gatekeeper, gk_init, gk_run, gk_stop);

/* ********************************************************* */

class Padico_Gatekeeper_impl :
  virtual public POA_Padico_Gatekeeper
{
public:
  Padico_Gatekeeper_impl();
  void GatekeeperPing(void);
  void GatekeeperKill(void);

  CORBA::Long LoadModule  (const char*name,
                           CORBA::Boolean wait_completion,
                           Padico_Gatekeeper::ReturnCode_t_out);
  CORBA::Long RunModule   (const char*name,
                           const Padico_Gatekeeper::Args_t& args,
                           CORBA::Boolean wait_completion,
                           Padico_Gatekeeper::ReturnCode_t_out);
  CORBA::Long UnloadModule(const char*name,
                           CORBA::Boolean wait_completion,
                           Padico_Gatekeeper::ReturnCode_t_out);

  void ListModule(Padico_Gatekeeper::SeqMods_t& m);
  CORBA::Boolean QueryModule(const char*name);
  void CreateGroup(const char*group_id, const Padico_Gatekeeper::SeqNodes_t&nodes);

  void ConnectorNew(const char*component,
                    Padico_Gatekeeper::Bytes_t_out ref,
                    Padico_Gatekeeper::ReturnCode_t_out rc);

  void ConnectorConnect(const char* component,
                        const Padico_Gatekeeper::Bytes_t&ref,
                        Padico_Gatekeeper::ReturnCode_t_out rc);

  void DoCommand(const char*xml_cmd,
                 CORBA::Boolean wait_completion,
                 Padico_Gatekeeper::ReturnCode_t_out);
};

static Padico::ORB_ptr                orb;
static PortableServer::POA_ptr        poa;
static PortableServer::POAManager_ptr poa_manager;
static Padico_Gatekeeper_impl*        local_gk;
static PortableServer::ObjectId_var   local_gk_oid;
static CosNaming::NamingContext_ptr   root_context;

//static string ns_gk_name;

Padico_Gatekeeper_impl::Padico_Gatekeeper_impl()
{
}

void Padico_Gatekeeper_impl::GatekeeperPing(void)
{
  padico_trace("Padico_Gatekeeper: PING\n");
}

void Padico_Gatekeeper_impl::GatekeeperKill(void)
{
  padico_trace("Padico_Gatekeeper::GatekeeperKill()\n");
  padico_tm_exit();
}

CORBA::Long Padico_Gatekeeper_impl::LoadModule(const char*name,
                                               CORBA::Boolean wait_completion,
                                               Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  padico_rc_t rc = NULL;
  gk_rc = new Padico_Gatekeeper::ReturnCode_t;
  padico_trace("Padico_Gatekeeper::LoadModule %s\n", name);
  rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, name);
  if(rc)
    {
      gk_rc->rc = rc->rc;
      gk_rc->text = CORBA::string_dup(padico_string_get(rc->msg));
      padico_out(30, "  rc=%d (%s)\n", rc->rc, padico_string_get(rc->msg));
    }
  else
    {
      gk_rc->rc = 0;
      gk_rc->text = CORBA::string_dup("-");
    }
  return (padico_rc_iserror(rc));
}



CORBA::Long Padico_Gatekeeper_impl::RunModule(const char*name,
                                              const Padico_Gatekeeper::Args_t& args,
                                              CORBA::Boolean wait_completion,
                                              Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  padico_rc_t rc = NULL;
  int argc;
  char**argv;
  int i;

  padico_tm_thread_givename("CORBA Thread");

  gk_rc = new Padico_Gatekeeper::ReturnCode_t;
  argc = args.length() + 1;
  argv = (char**)padico_malloc(sizeof(char*)*(argc+1));
  /** @bug memory leak: this malloc() is never freed
   */
  argv[0] = padico_strdup(name);
  argv[argc] = NULL;
  for(i=1; i<argc; i++)
    argv[i] = padico_strdup(args[i-1]);
  padico_trace("Padico_Gatekeeper::RunModule %s\n", name);
  rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, name, argc, argv);
  if(rc)
    {
      gk_rc->rc = rc->rc;
      gk_rc->text = CORBA::string_dup(padico_string_get(rc->msg));
      padico_out(30, "  rc=%d (%s)\n", rc->rc, padico_string_get(rc->msg));
    }
  else
    {
      gk_rc->rc = 0;
      gk_rc->text = CORBA::string_dup("-");
    }
  return (padico_rc_iserror(rc));
}

CORBA::Long Padico_Gatekeeper_impl::UnloadModule(const char*name,
                                                 CORBA::Boolean wait_completion,
                                                 Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  padico_rc_t rc = NULL;
  gk_rc = new Padico_Gatekeeper::ReturnCode_t;
  padico_trace("Padico_Gatekeeper::UnloadModule %s\n", name);
  rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, name);
  if(rc)
    {
      gk_rc->rc = rc->rc;
      gk_rc->text = CORBA::string_dup(padico_string_get(rc->msg));
      padico_out(30, "  rc=%d (%s)\n", rc->rc, padico_string_get(rc->msg));
    }
  else
    {
      gk_rc->rc = 0;
      gk_rc->text = CORBA::string_dup("-");
    }
  return (padico_rc_iserror(rc));
}

void Padico_Gatekeeper_impl::ListModule(Padico_Gatekeeper::SeqMods_t& mods)
{
  padico_modID_vect_t modIDs = puk_mod_getmodIDs();
  padico_trace("Padico_Gatekeeper::ListModule\n");

  padico_modID_itor_t m;
  int i =0;
  for(m  = padico_modID_vect_begin(modIDs);
      m != padico_modID_vect_end(modIDs);
      m  = padico_modID_vect_next(m), i++)
    {
      mods.length(i+1);
      mods[i] = CORBA::string_dup(*m);
    }
}

CORBA::Boolean Padico_Gatekeeper_impl::QueryModule(const char*name)
{
  puk_mod_t mod;
  padico_trace("Padico_Gatekeeper::QueryModule %s\n", name);
  mod = puk_mod_getbyname(name);
  if(mod == NULL)
    return 0;
  else
    return 1;
}

void Padico_Gatekeeper_impl::CreateGroup(const char*group_id,
                                         const Padico_Gatekeeper::SeqNodes_t&nodes)
{
  padico_group_t group;
  unsigned int i;
  group = padico_group_init(group_id);
  for(i = 0; i < nodes.length(); i++)
    {
      const char*nodeID = nodes[i];
      padico_topo_node_t node = padico_topo_getnodebyname(nodeID);
      if(node != NULL)
        {
          padico_group_node_add(group, node);
        }
      else
        {
          padico_warning("cannot add node **%s**: not recognized as a valid nodeID.\n", nodeID);
          break;
        }
    }
  padico_group_publish(group);
}

void Padico_Gatekeeper_impl::ConnectorNew(const char*component,
                                          Padico_Gatekeeper::Bytes_t_out _ref,
                                          Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  int len = 128;
  char buffer[len];
  padico_rc_t rc = NULL;
  Padico_Gatekeeper::Bytes_t_var ref   = new Padico_Gatekeeper::Bytes_t;
  gk_rc = new Padico_Gatekeeper::ReturnCode_t;

  padico_out(2, "invoking connector_new()\n");
  rc = padico_control_connector_new(buffer, &len);
  padico_out(2, "done. len=%d\n", len);
  if(rc)
    {
      ref->length(0);
      gk_rc->rc = rc->rc;
      gk_rc->text = CORBA::string_dup(padico_string_get(rc->msg));
      padico_out(30, "  rc=%d (%s)\n", rc->rc, padico_string_get(rc->msg));
    }
  else
    {
      ref->length(len);
      for(long i = 0; i < len ; i++)
        {
          ref[i] = (CORBA::Octet)(buffer[i]);
        }
      padico_out(2, "bytes copied into out buffer.\n");
      gk_rc->rc = 0;
      gk_rc->text = CORBA::string_dup("-");
    }
  _ref = ref.out();
}

void Padico_Gatekeeper_impl::ConnectorConnect(const char*component,
                                              const Padico_Gatekeeper::Bytes_t&ref,
                                              Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  long len = ref.length();
  CORBA::Octet* buffer = new CORBA::Octet[len];
  for(long i = 0; i < len; i++)
    {
      buffer[i] = ref[i];
    }

  padico_out(2, "- invoking connector_connect()\n");
  padico_control_connector_connect(buffer, len);
  padico_out(2, "- done.\n");

  gk_rc = new Padico_Gatekeeper::ReturnCode_t;
  gk_rc->rc = 0;
  gk_rc->text = CORBA::string_dup("-");
  delete buffer;
  padico_out(2, "- exit.\n");
}

void Padico_Gatekeeper_impl::DoCommand(const char*xml_cmd,
                                       CORBA::Boolean wait_completion,
                                       Padico_Gatekeeper::ReturnCode_t_out gk_rc)
{
  padico_rc_t rc = NULL;
  gk_rc = new Padico_Gatekeeper::ReturnCode_t;
  rc = puk_xml_parse_buffer((char*)xml_cmd, strlen(xml_cmd), PUK_TRUST_CONTROL).rc;
  padico_out(30, "rc=%p\n", rc);
  /** @todo ce serait bien de passer par padico_tm_* pour prendre
   * en compte le wait_completion...
   */
  if(rc)
    {
      gk_rc->rc = rc->rc;
      gk_rc->text = CORBA::string_dup(padico_string_get(rc->msg));
      padico_out(30, "  rc=%d (%s)\n", rc->rc, padico_string_get(rc->msg));
    }
  else
    {
      gk_rc->rc = 0;
      gk_rc->text = CORBA::string_dup("");
    }
}


static CosNaming::NamingContext_ptr get_context(char*id, char*kind,
                                                CosNaming::NamingContext_ptr& root_context)
{
  padico_trace("Gatekeeper: get_context() name=**%s.%s**\n", id, kind);
  CosNaming::NamingContext_ptr ctx;

  CosNaming::Name name;
  name.length(1);
  name[0].kind = CORBA::string_dup(kind);
  name[0].id   = CORBA::string_dup(id);

  try
    {
      ctx = CosNaming::NamingContext::_narrow(root_context->resolve(name));
    }
  catch(CosNaming::NamingContext::NotFound)
    {
      padico_trace("Gatekeeper: get_context() name=**%s.%s** not found -- creating...\n",
                   id, kind);
      try
        {
          ctx = CosNaming::NamingContext::_narrow(root_context->bind_new_context(name));
        }
      catch(CosNaming::NamingContext::AlreadyBound)
        {
          ctx = CosNaming::NamingContext::_narrow(root_context->resolve(name));
          padico_trace("Gatekeeper: get_context() name=**%s.%s** concurrently created!\n",
                       id, kind);
        }
    }
  return CosNaming::NamingContext::_duplicate(ctx);
}

static void resolve_node_context(CosNaming::NamingContext_ptr& root_ctx,
                                 CosNaming::NamingContext_ptr& nodes_ctx_out,
                                 CosNaming::NamingContext_ptr& shortcuts_ctx_out)
{
  try
    {
      CosNaming::NamingContext_ptr padico_ctx = get_context("Padico", "", root_ctx);
      CosNaming::NamingContext_ptr nodes_ctx  = get_context("Nodes", "", padico_ctx);
      CosNaming::NamingContext_ptr shortcuts_ctx = get_context("Shortcuts", "", padico_ctx);
      nodes_ctx_out = CosNaming::NamingContext::_duplicate(nodes_ctx);
      shortcuts_ctx_out = CosNaming::NamingContext::_duplicate(shortcuts_ctx);
    }
  catch(...)
    {
      padico_fatal("cannot resolve contexts\n");
    }
}

static void bind_node_ref(const char*name_string, CosNaming::NamingContext_ptr& ctx)
{
  CosNaming::Name name;
  name.length(1);
  name[0].kind = CORBA::string_dup("PadicoNode");
  name[0].id   = CORBA::string_dup(name_string);
  try
    {
      ctx->rebind(name, local_gk->_this());
    }
  catch(...)
    {
      padico_fatal("error while binding host entry\n");
    }
}

static void unbind_node_ref(const char*name_string, CosNaming::NamingContext_ptr& ctx)
{
  CosNaming::Name name;
  name.length(1);
  name[0].kind = CORBA::string_dup("PadicoNode");
  name[0].id   = CORBA::string_dup(name_string);
  try
    {
      ctx->unbind(name);
    }
  catch(CosNaming::NamingContext::NotFound)
    {
      padico_warning("error while unbinding host entry\n");
    }
}


static int gk_corba_init(void)
{
  int rc = 0;
  padico_trace("Gatekeeper: CORBA init\n");
  orb = Padico::ORB_init();
  try
    {
      CORBA::Object_var poa_ref = orb->resolve_initial_references ("RootPOA");
      poa = PortableServer::POA::_narrow (poa_ref);
      poa_manager = poa->the_POAManager();
      padico_trace("Gatekeeper: activating POA\n");
      poa_manager->activate();
    }
  catch(...)
    {
      padico_fatal("POA init exception\n");
    }

  padico_trace("Gatekeeper: Resolving NameService\n");
  try
    {
      // resolve name service
      CORBA::Object_var ns_ref =
        orb->resolve_initial_references ("NameService");
      root_context = CosNaming::NamingContext::_narrow (ns_ref);
      // try to resolve/create the "Padico" context
      //   -> throws an exception if NameService not available
      CosNaming::NamingContext_ptr padico_ctx __attribute__((__unused__)) =
        /* this is actually a dummy variable, never used except
         * to check whether its initialization throws an exception
         */
        get_context("Padico", "", root_context);
    }
  catch(...)
    {
      padico_warning("NameService not found\n");
      rc = -1;
    }
  return rc;
}


static int gk_start(void)
{
  int rc = 0;
  padico_out(30, "Gatekeeper: starting Gatekeeper\n");
  local_gk = new Padico_Gatekeeper_impl();
  local_gk_oid = poa->activate_object(local_gk);

  try
    {
      CosNaming::NamingContext_ptr nodes_ctx;
      CosNaming::NamingContext_ptr shortcuts_ctx;
      resolve_node_context(root_context, nodes_ctx, shortcuts_ctx);

      bind_node_ref(padico_topo_nodename(), nodes_ctx);
      bind_node_ref(padico_topo_nodealias(), shortcuts_ctx);
    }
  catch(...)
    {
      padico_warning("Error while binding Gatekeeper\n");
      rc = -1;
    }
  return rc;
}

int gk_run(int argc, char**argv)
{
  Padico::the_orb->run();
  return 0;
}


static void gk_stop(void)
{
  padico_out(30, "Gatekeeper: unbinding %s\n", padico_topo_nodename());
  try
    {
      CosNaming::NamingContext_ptr nodes_ctx;
      CosNaming::NamingContext_ptr shortcuts_ctx;
      resolve_node_context(root_context, nodes_ctx, shortcuts_ctx);

      unbind_node_ref(padico_topo_nodename(), nodes_ctx);
      unbind_node_ref(padico_topo_nodealias(), shortcuts_ctx);

    }
  catch(...)
    {
      padico_warning("not found in NameService\n");
    }
  poa->deactivate_object(local_gk_oid.in());
  local_gk->_remove_ref();
  delete local_gk;
}

static int gk_init(void)
{
  int rc;
  rc = gk_corba_init();
  if(rc == 0)
    {
      rc = gk_start();
    }
  else
    {
      padico_warning("cannot initialize CORBA Gatekeeper.\n");
    }
  if(rc == 0)
    {
      padico_out(5, "Gatekeeper ready.\n");
    }
  else
    {
      padico_out(5, "Gatekeeper intialization error (rc=%d)\n", rc);
    }
  return rc;
}
