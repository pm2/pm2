/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico NetAccess control channel over SysIO
 * @ingroup Control
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/SysIO.h>
#include <Padico/PadicoControl.h>
#include <Padico/SocketFactory.h>
#include <Padico/AddrDB.h>
#include <Padico/Topology.h>
#include <arpa/inet.h>

static int control_sysio_init(void);
static void control_sysio_finalize(void);

PADICO_MODULE_DECLARE(Control_SysIO, control_sysio_init, NULL, control_sysio_finalize,
                      "PadicoControl", "NetSelector", "Topology", "AddrDB", "SysIO");

/** @defgroup Control_SysIO component: Control/SysIO- control channel over SysIO
 * @ingroup PadicoComponent
 */

/* *** PadicoControl driver ******************************** */

static void sysio_control_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg);

/** 'Control' facet for Control/SysIO
 * @ingroup Control_SysIO
 */
static const struct padico_control_driver_s sysio_control_driver =
{
  .send_common = &sysio_control_send_common
};

/* *** PadicoBootstrap driver ******************************** */

static int sysio_control_bootstrap_new(void*_status);
static int sysio_control_bootstrap_connect(void*_status, const char*hostname, padico_topo_uuid_t uuid);

/** 'Bootstrap' facet for Control/SysIO
 * @ingroup Control_SysIO
 */
static const struct padico_bootstrap_driver_s sysio_bootstrap_driver =
{
  .bootstrap_new     = &sysio_control_bootstrap_new,
  .bootstrap_connect = &sysio_control_bootstrap_connect
};

/* *** PadicoConnector driver ******************************** */

static int sysio_control_connector_new(void*_status, const void**local_addr, size_t*addr_size);
static int sysio_control_connector_connect(void*_status, const void*remote_addr, size_t addr_size);

/** 'Connector' facet for Control/SysIO
 * @ingroup Control_SysIO
 */
static const struct padico_control_connector_driver_s sysio_connector_driver =
  {
    .connector_init    = NULL,
    .connector_new     = &sysio_control_connector_new,
    .connector_connect = &sysio_control_connector_connect,
    .connector_close   = NULL,
  };

/* *** PadicoComponent driver ******************************** */

static void*sysio_control_instantiate(puk_instance_t, puk_context_t);
static void sysio_control_destroy(void*);

/** instanciation facet for Control/SysIO
 * @ingroup Control_SysIO
 */
static const struct puk_component_driver_s sysio_control_component_driver =
{
  .instantiate = &sysio_control_instantiate,
  .destroy     = &sysio_control_destroy
};


/* ********************************************************* */

enum sysio_control_state_e
  {
    SYSIO_CONTROL_STATE_INIT = 0,    /**< connection is initializing */
    SYSIO_CONTROL_STATE_CONNECTED,   /**< connection is established */
    SYSIO_CONTROL_STATE_DISCONNECTED /**< connection has been disconnected */
  };


PUK_LIST_DECLARE_TYPE2(sysio_control_global, struct sysio_control_status_s);
PUK_LIST_DECLARE_TYPE2(sysio_control_backlog, struct sysio_control_status_s);

/** status of an instance of a Control/SysIO component.
 * @ingroup Control_SysIO
 */
struct sysio_control_status_s
{
  PUK_LIST_LINK(sysio_control_global);
  PUK_LIST_LINK(sysio_control_backlog);
  struct puk_receptacle_SocketFactory_s sf;
  struct sockaddr_in local_addr;  /**< address of local endpoint */
  puk_instance_t instance;        /**< self as an instance (used for interactions with padico_control_*) */
  marcel_mutex_t lock;            /**< lock for the SysIO control channel */
  marcel_cond_t connection_event; /**< condition signaled upon connection event */
  enum sysio_control_state_e state;  /**< whether the channel is connected */
  padico_req_t req;               /**< request object used for asynchronous connection */
  padico_topo_node_t peer;          /**< peer node */
  padico_io_t in_io;              /**< IO handler (used for receipt only) */
  int fd;                         /**< physical file descriptor of the socket (puk_abi_wrap_* level) */
  sysio_control_backlog_list_t backlog; /**< list of accepted connections */
};
PUK_LIST_CREATE_FUNCS(sysio_control_global);
PUK_LIST_CREATE_FUNCS(sysio_control_backlog);

static struct
{
  marcel_mutex_t lock;
  sysio_control_global_list_t instances;
  padico_tm_bgthread_pool_t pool;
  puk_component_t component;
} sysio_control = { .component = NULL };

/* ********************************************************* */
/* *** PadicoControl */

static void sysio_control_connector_notify(struct sysio_control_status_s*status, padico_rc_t rc);

static void sysio_control_connection_lost(struct sysio_control_status_s*status)
{
  status->state = SYSIO_CONTROL_STATE_DISCONNECTED;
  padico_control_delete_node(status->instance, status->peer);
  sysio_control_connector_notify(status, padico_rc_error("Control_SysIO: connection lost"));
}

static void sysio_control_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg)
{
  struct sysio_control_status_s*status = _status;
  const struct iovec*v = padico_control_msg_get_iovec(msg);
  const int iovcnt = padico_control_msg_get_iovec_size(msg);
  const size_t size = padico_control_msg_get_size(msg);
  struct iovec*v2 = padico_malloc(sizeof(struct iovec) * (iovcnt + 1));
  int i;
  for(i=0; i<iovcnt; i++)
    {
      v2[i+1].iov_base = v[i].iov_base;
      v2[i+1].iov_len = v[i].iov_len;
    }

  padico_out(30, "locking- fd=%d; size=%ld.\n", status->fd, puk_ulong(size));
  marcel_mutex_lock(&status->lock);
  while(status->state == SYSIO_CONTROL_STATE_INIT)
    {
      padico_out(80, "wait for the establishment of the control channel.\n");
      marcel_cond_wait(&status->connection_event, &status->lock);
    }
  assert(status->fd > 0);
  assert(node == status->peer);
  padico_out(30, "sending- fd=%d; size=%ld.\n", status->fd, puk_ulong(size));
  const uint32_t n_size = htonl(size);
  v2[0].iov_base = (void*)&n_size;
  v2[0].iov_len = sizeof(n_size);
  int sysrc = padico_sysio_outv(status->fd, v2, iovcnt + 1);
  marcel_mutex_unlock(&status->lock);
  if(sysrc != (size + sizeof(n_size)))
    {
      sysio_control_connection_lost(status);
    }
  padico_free(v2);
  padico_out(30, "send ok.\n");
}

static int sysio_control_read_callback(int fd, void*key)
{
  int again = 1;
  struct sysio_control_status_s*status = (struct sysio_control_status_s*)key;
  uint32_t n_size = 0;

  assert(status->fd == fd);
  padico_out(40, "entering...\n");
  int sysrc = padico_sysio_in(fd, &n_size, sizeof(uint32_t));
  if(sysrc <= 0)
    {
      again = 0;
      sysio_control_connection_lost(status);
      return again;
    }
  const size_t size = htonl(n_size);
  char*bytes = NULL;
  if(size > 0)
    {
      bytes = padico_malloc(size);
      sysrc = padico_sysio_in(fd, bytes, size);
      if(sysrc < size)
        {
          again = 0;
          sysio_control_connection_lost(status);
          return again;
        }
    }
  padico_out(40, "received message- len=%ld; msg=*%s*\n", puk_ulong(size), bytes);
  padico_control_deliver_message(status->instance, status->peer, bytes, size);
  return again;
}



/* ********************************************************* */

/* *** socket handles */

/* ip and port must be in network byte order */
static int sysio_control_socket_create(struct sysio_control_status_s*status)
{
  assert(status != NULL);
  puk_context_indirect_SocketFactory(status->instance, "sf", &status->sf);
  assert(status->sf.driver && status->sf._status);
  struct puk_receptacle_SocketFactory_s*sf = &status->sf;
  int rc = (*sf->driver->create)(sf->_status, AF_INET, SOCK_STREAM, 0);
  if(rc < 0)
    padico_out(puk_verbose_critical, "socket_create: cannot create the socket \n");
  return rc;
}


static int sysio_control_socket_connect(struct sysio_control_status_s*status,
                                        const struct sockaddr_in*servaddr,
                                        padico_socketfactory_connector_t connector)
{
  struct puk_receptacle_SocketFactory_s*sf = &status->sf;
  if(sf->driver->connect == NULL)
    {
      padico_out(puk_verbose_critical, "boostrap_connect: "
                 "the socketfactory does not provide the required driver (connect).\n");
      return -1;
    }
  padico_out(puk_verbose_info, "connecting to addr=%s; port=%d\n", inet_ntoa(servaddr->sin_addr), ntohs(servaddr->sin_port));
  int rc = (*sf->driver->connect)(sf->_status, (struct sockaddr*) servaddr, sizeof(*servaddr),
                                  connector, status);
  if(rc == -EINPROGRESS)
    rc = 0;
  if(rc != 0)
    padico_out(puk_verbose_notice, "socket_connect()- Connection failed- rc = %d (%s).\n", rc, strerror(-rc));
  return rc;
}

static int sysio_control_socket_close(struct sysio_control_status_s*status)
{
  int rc = 0;
  assert(status != NULL);
  struct puk_receptacle_SocketFactory_s*sf = &status->sf;
  if(sf->driver && sf->driver->close)
    rc = (*sf->driver->close)(sf->_status);
  if(status->in_io)
    padico_io_unregister(status->in_io);
  status->in_io = NULL;
  if(status->fd != -1)
    padico_sysio_close(status->fd);
  status->fd = -1;
  padico_out(40, "connection closed, rc=%d\n", rc);
  return rc;
}

/* ********************************************************* */

/* *** PadicoConnector */


static void sysio_control_connector_notify(struct sysio_control_status_s*status, padico_rc_t rc)
{
  if(status->req)
    {
      padico_tm_req_notify(status->req->id, rc);
      status->req = NULL;
    }
  else padico_rc_delete(rc);
}

static void*sysio_control_start(void*_status)
{
  struct sysio_control_status_s*status = _status;

  /* announce local node to peer node */
  padico_string_t self_node = padico_topo_node_serialize(padico_topo_getlocalnode());
  const char*msg = padico_string_get(self_node);
  marcel_mutex_lock(&status->lock);
  assert(status->fd > 0);
  assert(status->state == SYSIO_CONTROL_STATE_INIT);
  uint32_t size = strlen(msg) + 1;
  int sysrc = padico_sysio_out(status->fd, &size, sizeof(size));
  if(sysrc > 0)
    {
      sysrc = padico_sysio_out(status->fd, msg, size);
    }
  marcel_mutex_unlock(&status->lock);
  if(sysrc < 0)
    {
      sysio_control_connection_lost(status);
    }
  msg = NULL;
  padico_string_delete(self_node);
  /* receive peer announce */
  sysrc = padico_sysio_in(status->fd, &size, sizeof(size));
  if(sysrc <= 0)
    {
      sysio_control_connection_lost(status);
      return NULL;
    }
  char*recv_msg = padico_malloc(size);
  sysrc = padico_sysio_in(status->fd, recv_msg, size);
  if(sysrc < size)
    {
      sysio_control_connection_lost(status);
      return NULL;
    }
  /* parse peer announce */
  padico_control_event_disable();
  struct puk_parse_entity_s e = puk_xml_parse_buffer(recv_msg, size - 1, PUK_TRUST_CLUSTER);
  padico_rc_t rc = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc))
    {
      padico_rc_show(rc);
      padico_fatal("parse error on initial handshake message.");
    }
  if(!puk_parse_is(&e, "Topology:node"))
    {
      padico_fatal("root entity is not \"Topology:node\" while initializing control channel.\n");
    }
  padico_topo_node_t peer = puk_parse_get_content(&e);
  assert(peer != NULL);
  assert(status->peer == NULL);
  status->peer = peer;
  padico_control_new_node(status->instance, peer);
  padico_control_event_enable();

  assert(padico_topo_getnodebyuuid(padico_topo_node_getuuid(peer)) != NULL);

  /* authorize to send */
  marcel_mutex_lock(&status->lock);
  status->state = SYSIO_CONTROL_STATE_CONNECTED;
  status->in_io = padico_io_register(status->fd, PADICO_IO_EVENT_READ,
                                     &sysio_control_read_callback, status);
  marcel_cond_broadcast(&status->connection_event);
  marcel_mutex_unlock(&status->lock);
  /* activate polling */
  padico_io_activate(status->in_io);

  /* notify connection establishment */
  padico_out(20, "the connection to node %s is up\n", padico_topo_node_getname(peer));
  sysio_control_connector_notify(status, padico_rc_ok());
  padico_free(recv_msg);
  /* flush all events that may have been generate by the new_node event handler
   * since they may declare new nodes (from router) that will send us messages.
   * Remote routers may push messages before our local router event knows they exist!
   * Better know them *before* activating I/O to avoid troubles.
   */
  padico_control_event_disable();
  padico_control_event_flush();
  padico_control_event_enable();

  return NULL;
}



/* ********************************************************* */
/* *** PadicoBootstrap */

static void sysio_control_bootstrap_acceptor(void*key, int new_fd, const struct sockaddr*addr, socklen_t addrlen)
{
  struct sysio_control_status_s*server_status = (struct sysio_control_status_s*) key;
  /* the new controler will manage this new socket */
  padico_out(20, "new bootstrap connection received from=%s.\n",
             inet_ntoa(((struct sockaddr_in*) addr)->sin_addr));
  puk_instance_t instance = puk_component_instantiate(server_status->instance->component);
  struct puk_receptacle_PadicoControl_s r;
  puk_instance_indirect_PadicoControl(instance, NULL, &r);
  struct sysio_control_status_s*conn_status = r._status;
  conn_status->fd = new_fd;
  padico_control_plug_controler(conn_status->instance, server_status->instance);
  /* complete connection */
  padico_tm_bgthread_start(sysio_control.pool, &sysio_control_start, conn_status, "sysio_control_start");
  padico_out(20, "bootstrap connection accepted from=%s.\n",
             inet_ntoa(((struct sockaddr_in*) addr)->sin_addr));
  /* enqueue in backlog */
  marcel_mutex_lock(&server_status->lock);
  sysio_control_backlog_list_push_back(server_status->backlog, conn_status);
  marcel_cond_broadcast(&server_status->connection_event);
  marcel_mutex_unlock(&server_status->lock);
  if(server_status->req)
    {
      padico_tm_req_notify(server_status->req->id, padico_rc_ok());
      server_status->req = NULL;
    }
}

static void sysio_control_bootstrap_connector(void*key, int fd)
{
  struct sysio_control_status_s*status = (struct sysio_control_status_s*) key;
  padico_out(20, "bootstrap connecting...\n");
  status->fd = fd;
  padico_control_plug_controler(status->instance, NULL);
  padico_tm_bgthread_start(sysio_control.pool, &sysio_control_start, status, "sysio_control_start");
  padico_out(20, "bootstrap connected.\n");
}

static int sysio_control_bootstrap_server(struct sysio_control_status_s*status, struct sockaddr_in*inaddr)
{
  sysio_control_socket_create(status);
  struct puk_receptacle_SocketFactory_s*sf = &status->sf;
  const int default_backlog = 32;
  int backlog = padico_na_size() > default_backlog ? padico_na_size() : default_backlog;
  if((sf->driver->bind == NULL) || (sf->driver->listen == NULL))
    {
      padico_fatal("socket_listen: the socketfactory does not provide the required methods (bind, listen).\n");
      return -1;
    }
  if(sf->driver->setsockopt)
    {
      int val = 1;
      (*sf->driver->setsockopt)(sf->_status, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    }
  socklen_t addr_len = sizeof(struct sockaddr_in);
  int rc = (*sf->driver->bind)(sf->_status, (struct sockaddr*)inaddr, &addr_len);
  if(rc)
    {
      padico_fatal("socket_bind: err %d (%s)\n", -rc, strerror(-rc));
    }
  rc = (*sf->driver->listen)(sf->_status, backlog, (struct sockaddr*)inaddr, addr_len,
                             &sysio_control_bootstrap_acceptor, status);
  if(rc)
    {
      padico_fatal("socket_listen: err %d (%s)\n", -rc, strerror(-rc));
    }
  return rc;
}

static int sysio_control_bootstrap_client(struct sysio_control_status_s*status, const struct sockaddr_in*servaddr)
{
  sysio_control_socket_create(status);
  int rc = sysio_control_socket_connect(status, servaddr, &sysio_control_bootstrap_connector);
  if(rc == 0)
    {
      marcel_mutex_lock(&status->lock);
      while(status->state == SYSIO_CONTROL_STATE_INIT)
        {
          padico_out(80, "wait for the establishment of the control channel.\n");
          marcel_cond_wait(&status->connection_event, &status->lock);
        }
      if(status->state != SYSIO_CONTROL_STATE_CONNECTED)
        {
          rc = -ENETUNREACH;
        }
      marcel_mutex_unlock(&status->lock);
    }
  return rc;
}

/** get a bootstrap port from an UUID */
static uint16_t sysio_control_bootstrap_auto_port(padico_topo_node_t node)
{
  const unsigned int port_base  = 20000;
  const unsigned int port_end   = 32000;
  const unsigned int port_range = port_end - port_base;
  const char*session_id = padico_topo_node_getsession(padico_topo_getlocalnode());
  const unsigned int port_offset = puk_hash_default((void*)session_id, strlen(session_id));
  const uint16_t port = port_base + (port_offset % port_range);
  return port;
}

static int sysio_control_bootstrap_new(void*_status)
{
  struct sysio_control_status_s*status = _status;
  uint16_t port = sysio_control_bootstrap_auto_port(padico_topo_getlocalnode());
  struct sockaddr_in inaddr = (struct sockaddr_in)
    {
      .sin_family = AF_INET,
      .sin_addr.s_addr = htonl(INADDR_ANY),
      .sin_port = htons(port)
    };
  int rc = sysio_control_bootstrap_server(status, &inaddr);
  return rc;
}

static int sysio_control_bootstrap_connect(void*_status, const char*hostname, padico_topo_uuid_t uuid)
{
  struct sysio_control_status_s*status = _status;
  const padico_topo_host_t host = padico_topo_host_resolvebyname(hostname);
  const struct in_addr*addr = padico_topo_host_getaddr(host);
  if(!addr)
    {
      padico_fatal("cannot find host '%s': host has no IP address; host = %s\n",
                   hostname, padico_string_get(padico_topo_host_serialize(host)));
    }
  const uint16_t port = sysio_control_bootstrap_auto_port(NULL);
  struct sockaddr_in servaddr =
    {
      .sin_family = AF_INET,
      .sin_addr   = *addr,
      .sin_port   = htons(port)
    };
  int rc = sysio_control_bootstrap_client(status, &servaddr);
  return rc;
}

/* ********************************************************* */
/* *** PadicoConnector */

static int sysio_control_connector_new(void*_status, const void**local_addr, size_t*addr_size)
{
  struct sysio_control_status_s*status = _status;
  status->local_addr = (struct sockaddr_in)
    {
      .sin_family = AF_INET,
      .sin_addr.s_addr = htonl(INADDR_ANY),
      .sin_port = htons(0)
    };
  int rc = sysio_control_bootstrap_server(status, &status->local_addr);
  if(rc)
    {
      padico_warning("error %d (%s) while creating connector.\n", rc, strerror(-rc));
      return rc;
    }
  status->local_addr.sin_addr = puk_inet_getaddr(); /* binding to INADDR_ANY leads to getsockname returning 0.0.0.0 => force local IP address in sockaddr */
  *local_addr = &status->local_addr;
  *addr_size = sizeof(struct sockaddr_in);
  return 0;
}

static int sysio_control_connector_connect(void*_status, const void*remote_addr, size_t addr_size)
{
  struct sysio_control_status_s*status = _status;
  const struct sockaddr_in*inaddr = remote_addr;
  assert(addr_size == sizeof(struct sockaddr_in));
  if(memcmp(&status->local_addr, remote_addr, addr_size) > 0)
    {

      /* local is client */
      int rc = sysio_control_bootstrap_client(status, inaddr);
      if(rc)
        {
          padico_warning("error %d (%s) while connecting connector.\n", rc, strerror(-rc));
          return rc;
        }
    }
  else
    {
      /* local is server */
      marcel_mutex_lock(&status->lock);
      while(sysio_control_backlog_list_empty(status->backlog))
        {
          marcel_cond_wait(&status->connection_event, &status->lock);
        }
      sysio_control_backlog_list_pop_front(status->backlog);
      marcel_mutex_unlock(&status->lock);
    }
  return 0;
}

/* ********************************************************* */
/* *** PadicoComponent */

static void*sysio_control_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct sysio_control_status_s*status = padico_malloc(sizeof(struct sysio_control_status_s));
  *status = (struct sysio_control_status_s)
    {
      .peer     = NULL,
      .fd       = -1,
      .in_io    = NULL,
      .state    = SYSIO_CONTROL_STATE_INIT,
      .req      = NULL,
      .instance = instance,
      .sf       = { .driver = NULL, ._status = NULL},
      .backlog  = sysio_control_backlog_list_new()
    };
  marcel_mutex_init(&status->lock, 0);
  marcel_cond_init(&status->connection_event, 0);
  marcel_mutex_lock(&sysio_control.lock);
  sysio_control_global_list_push_back(sysio_control.instances, status);
  marcel_mutex_unlock(&sysio_control.lock);
  return (void*)status;
}

static void sysio_control_destroy(void*_status)
{
  if(_status)
    {
      struct sysio_control_status_s*status = _status;
      if(status->fd != -1)
        sysio_control_socket_close(status);
      marcel_mutex_lock(&sysio_control.lock);
      sysio_control_global_list_remove(sysio_control.instances, status);
      marcel_mutex_unlock(&sysio_control.lock);
      padico_control_unplug_controler(status->instance);
      marcel_mutex_destroy(&status->lock);
      sysio_control_backlog_list_delete(status->backlog);
      marcel_cond_destroy(&status->connection_event);
      padico_free(status);
    }
}

/* ********************************************************* */

static int control_sysio_init(void)
{
  marcel_mutex_init(&sysio_control.lock, NULL);
  sysio_control.instances = sysio_control_global_list_new();
  sysio_control.pool = padico_tm_bgthread_pool_create("Control_SysIO");
  sysio_control.component =
    puk_component_declare("Control_SysIO",
                          puk_component_provides("PadicoComponent", "component", &sysio_control_component_driver),
                          puk_component_provides("PadicoControl",   "control",   &sysio_control_driver),
                          puk_component_provides("PadicoBootstrap", "bootstrap", &sysio_bootstrap_driver),
                          puk_component_provides("PadicoConnector", "connector", &sysio_connector_driver),
                          puk_component_uses("SocketFactory", "sf"));
  padico_control_event_sync();
  return 0;
}

static void control_sysio_finalize(void)
{
  marcel_mutex_lock(&sysio_control.lock);
  while(!sysio_control_global_list_empty(sysio_control.instances))
    {
        struct sysio_control_status_s*status = sysio_control_global_list_pop_front(sysio_control.instances);
        padico_control_unplug_controler(status->instance);
        sysio_control_socket_close(status);
    }
  marcel_mutex_unlock(&sysio_control.lock);
  padico_tm_bgthread_pool_wait(sysio_control.pool);
  sysio_control_global_list_delete(sysio_control.instances);
  puk_component_destroy(sysio_control.component);
}
