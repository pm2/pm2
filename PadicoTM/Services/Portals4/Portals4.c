/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Portals4.c
 *  @brief Portals4 topology detection and description
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>

#include <portals4.h>


static int padico_portals4_init(void);
static void padico_portals4_finalize(void);

PADICO_MODULE_DECLARE(Portals4, padico_portals4_init, NULL, padico_portals4_finalize,
                      "Topology");


/* ********************************************************* */

struct padico_portals4_port_s
{
  ptl_process_t process_id;
  padico_topo_network_t network;
};

static struct
{
  struct padico_portals4_port_s*port;
} padico_portals4 = { .port = NULL };


/* ********************************************************* */

static int padico_portals4_init(void)
{
  int rc = PtlInit();
  if(rc != PTL_OK)
    {
      padico_warning("failed to initialise portals4 library");
      return -1;
    }

  ptl_handle_ni_t ni_handle;
  ptl_ni_limits_t ni_limits;
  rc = PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PTL_PID_ANY, NULL, &ni_limits, &ni_handle);
  if(rc != PTL_OK)
    {
      padico_warning("failed to initialise portals4 network interface");
      PtlFini();
      return -1;
    }
  struct padico_portals4_port_s*port = padico_malloc(sizeof(struct padico_portals4_port_s));
  rc = PtlGetPhysId(ni_handle, &port->process_id);
  if(rc != PTL_OK)
    {
      padico_warning("failed to get phys ID");
      PtlNIFini(ni_handle);
      PtlFini();
      return -1;
    }

  padico_print("phys ID- nid = %d; pid = %d\n", port->process_id.phys.nid, port->process_id.phys.pid);

  padico_topo_network_t network = padico_topo_network_getbyid("BXI");
  if(network)
    {
      port->network = network;
    }
  else
    {
      port->network = padico_topo_network_create("BXI", PADICO_TOPO_SCOPE_HOST, sizeof(ptl_process_t), AF_UNSPEC);
    }
  padico_topo_host_bind(padico_topo_getlocalhost(), port->network, &port->process_id);
  padico_portals4.port = port;

  PtlNIFini(ni_handle);
  PtlFini();
  return 0;
}

static void padico_portals4_finalize(void)
{
  if(padico_portals4.port != NULL)
    {
      padico_free(padico_portals4.port);
    }
}
