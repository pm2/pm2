/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file fabric.c
 *  @brief libfabric topology detection and description
 *  Detects only Cray CXI for now.
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <rdma/fabric.h>

static int padico_libfabric_init(void);
static void padico_libfabric_finalize(void);

PADICO_MODULE_DECLARE(libfabric, padico_libfabric_init, NULL, padico_libfabric_finalize, "Topology");

/* size for a CXI address in string format. 18 bytes should be enough; stay on the safe side */
#define CXI_ADDR_SIZE 32

/* ********************************************************* */

struct padico_libfabric_port_s
{
  char address[CXI_ADDR_SIZE];
  padico_topo_network_t network;
};

static struct
{
  struct padico_libfabric_port_s*port;
} padico_libfabric = { .port = NULL };


/* ********************************************************* */

static int padico_libfabric_init(void)
{
  struct stat s;
  int rc = stat("/dev/cxi0", &s);
  if(!rc)
    {
      rc = stat("/sys/class/cxi/cxi0/device/net/hsn0/address", &s);
      if(!rc)
        {
          /* hack to detect CXI address here, even though CXI is not documented.
           * maybe we'd better simply use MAC address of hsn0 ethernet iface */
          FILE*fd = fopen("/sys/class/cxi/cxi0/device/net/hsn0/address", "r");
          char addr[CXI_ADDR_SIZE] = { '\0' };
          fgets(addr, CXI_ADDR_SIZE, fd);
          fclose(fd);
          char*b = strchr(addr, '\n');
          if(b)
            {
              *b = '\0';
            }
          padico_print("detected Cray CXI; address = %s\n", addr);
          struct padico_libfabric_port_s*port = padico_malloc(sizeof(struct padico_libfabric_port_s));
          memcpy(port->address, addr, CXI_ADDR_SIZE);
          padico_topo_network_t network = padico_topo_network_getbyid("CXI");
          if(network)
            {
              port->network = network;
            }
          else
            {
              port->network = padico_topo_network_create("CXI", PADICO_TOPO_SCOPE_HOST, CXI_ADDR_SIZE, AF_UNSPEC);
            }
          padico_topo_host_bind(padico_topo_getlocalhost(), port->network, port->address);
          padico_libfabric.port = port;

        }
      else
        {
          padico_warning("detected Cray CXI but cannot read address.\n");
        }
    }
  else
    {
      padico_warning("no network detected.\n");
    }

  return 0;
}

static void padico_libfabric_finalize(void)
{
  if(padico_libfabric.port != NULL)
    {
      padico_free(padico_libfabric.port);
    }
}
