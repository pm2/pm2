/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Socket factory for plain sockets
 */

#include <Padico/VLink-internals.h>
#include <Padico/Module.h>

#include "SocketFactory.h"

static int sfplain_module_init(void);
static void sfplain_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_Plain, sfplain_module_init, NULL, sfplain_module_finalize,
                      "SysIO", "Topology");

/** @defgroup SF_Plain component: SocketFactory-plain- Socket factory which builds plain TCP sockets.
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sfplain_create(void*_status, int family, int type, int protocol);
static int sfplain_bind(void*_status, struct sockaddr*addr, socklen_t*addrlen);
static int sfplain_listen(void*_status, int backlog, const struct sockaddr*addr, socklen_t addrlen, padico_socketfactory_acceptor_t notifier, void*key);
static int sfplain_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen, padico_socketfactory_connector_t notifier, void*key);
static int sfplain_close(void*_status);
static int sfplain_setsockopt(void*_status, int level, int opt, const void*val, int len);

/** 'SocketFactory' facet for SocketFactory-plain
 * @ingroup SF_Plain
 */
static const struct padico_socketfactory_s sfplain_sf_driver =
  {
    .create     = &sfplain_create,
    .bind       = &sfplain_bind,
    .listen     = &sfplain_listen,
    .connect    = &sfplain_connect,
    .close      = &sfplain_close,
    .setsockopt = &sfplain_setsockopt,
    .caps       = padico_vsock_caps_directconnect
  };

static void*sfplain_instantiate(puk_instance_t, puk_context_t);
static void sfplain_destroy(void*);

/** instanciation facet for SocketFactory-plain
 * @ingroup SF_Plain
 */
static const struct puk_component_driver_s sfplain_component_driver =
  {
    .instantiate = &sfplain_instantiate,
    .destroy     = &sfplain_destroy
  };

/** status of SocketFactory-plain
 * @ingroup SF_Plain
 */
struct sfplain_status_s
{
  int fd;
  padico_io_t io_connect;
  padico_io_t io_listen;
  padico_socketfactory_acceptor_t acceptor;
  void*acceptor_key;
  padico_socketfactory_connector_t connector;
  void*connector_key;
};

static int sfplain_callback_acceptor(int fd, void*key);
static int sfplain_callback_connector(int fd, void*key);

/* ********************************************************* */

static struct
{
  padico_tm_bgthread_pool_t pool;
  puk_component_t component;
} sfplain = { .component = NULL };

static int sfplain_module_init(void)
{
  sfplain.pool = padico_tm_bgthread_pool_create("SocketFactory_Plain");
  sfplain.component =
    puk_component_declare("SocketFactory_Plain",
                          puk_component_provides("PadicoComponent", "component", &sfplain_component_driver),
                          puk_component_provides("SocketFactory", "sf", &sfplain_sf_driver));
  return 0;
}

static void sfplain_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfplain.pool);
  puk_component_destroy(sfplain.component);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for PlainSocketFactory */

static void*sfplain_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct sfplain_status_s*status = padico_malloc(sizeof(struct sfplain_status_s));
  status->fd = -1;
  status->io_connect = NULL;
  status->io_listen  = NULL;
  status->acceptor   = NULL;
  status->connector  = NULL;
  padico_out(40, "done.\n");
  return status;
}

static void sfplain_destroy(void*_status)
{
  struct sfplain_status_s*status = _status;
  sfplain_close(_status);
  padico_free(status);
  padico_out(40, "done.\n");
}

/* ********************************************************* */
/* *** 'SocketFactory' for PlainSocketFactory */

static int sfplain_create(void*_status, int family, int type, int protocol)
{
  struct sfplain_status_s*status = _status;
  int fd  = padico_sysio_socket(family, type, protocol);
  int err = padico_sysio_errno();
  if(fd > -1)
    {
      int val, rc = 0;
      if((family == AF_INET) || (family == AF_INET6))
        {
          val = 1;
          rc  = PUK_ABI_FSYS_WRAP(setsockopt)(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
          err = __puk_abi_wrap__errno;
          val = 1;
          rc  = PUK_ABI_FSYS_WRAP(setsockopt)(fd, SOL_TCP, TCP_NODELAY, &val, sizeof(val));
        }
      if(!rc)
        {
          err = 0;
          status->fd = fd;
        }
    }
  padico_out(40, "fd=%d; err=%d\n", fd, err);
  return -err;
}

static int sfplain_bind(void*_status, struct sockaddr*_addr, socklen_t*addrlen)
{
  struct sfplain_status_s*status = _status;
  int err = 0;
  switch(_addr->sa_family)
    {
    case AF_PADICO:
      {
        struct sockaddr_pa*addr = (struct sockaddr_pa*)_addr;
        err = padico_sockaddr_check(addr, AF_PADICO_AUTO);
        if(err)
          goto error;
        padico_sockaddr_init(addr, addrlen, AF_PADICO_INET);
        struct sockaddr_in*addr_in = padico_sockaddr_payload(struct sockaddr_in, addr);
        addr_in->sin_family = AF_INET;
        addr_in->sin_addr   = *padico_topo_host_getaddr(padico_topo_getlocalhost());
        addr_in->sin_port   = htons(0);
        int rc = PUK_ABI_FSYS_WRAP(bind)(status->fd, addr_in, sizeof(*addr_in));
        if(rc < 0)
          err = __puk_abi_wrap__errno;
      }
      break;

    case AF_INET:
      {
        struct sockaddr_in*addr_in = (struct sockaddr_in*)_addr;
        *addrlen = sizeof(struct sockaddr_in);
        int rc = PUK_ABI_FSYS_WRAP(bind)(status->fd, addr_in, sizeof(*addr_in));
        if(rc < 0)
          err = __puk_abi_wrap__errno;
      }
      break;

    case AF_INET6:
      {
        struct sockaddr_in6*addr_in6 = (struct sockaddr_in6*)_addr;
        *addrlen = sizeof(struct sockaddr_in6);
        int rc = PUK_ABI_FSYS_WRAP(bind)(status->fd, addr_in6, sizeof(*addr_in6));
        if(rc < 0)
          err = __puk_abi_wrap__errno;
      }
      break;

    case AF_UNIX:
      {
        struct sockaddr_un*addr_un = (struct sockaddr_un*)_addr;
        *addrlen = sizeof(struct sockaddr_un);
        int rc = PUK_ABI_FSYS_WRAP(bind)(status->fd, addr_un, sizeof(*addr_un));
        if(rc < 0)
          err = __puk_abi_wrap__errno;
      }
      break;

    default:
      padico_warning("implementaiton does not support address family %d.\n", _addr->sa_family);
      err = EAFNOSUPPORT;
      goto error;
      break;
    }
  if(err == 0)
    {
      status->io_listen  = padico_io_register(status->fd, PADICO_IO_EVENT_READ, &sfplain_callback_acceptor, status);
      int rc = PUK_ABI_FSYS_WRAP(getsockname)(status->fd, _addr, addrlen);
      if(rc < 0)
        err = __puk_abi_wrap__errno;
      if(rc == 0)
        {
          err = 0;
        }
      else
        {
          padico_warning("error in getsockname- (%s)\n", strerror(err));
        }
    }
 error:
  padico_out(40, "fd=%d; err=%d\n", status->fd, err);
  return -err;
}

static int sfplain_listen(void*_status, int backlog, const struct sockaddr*addr, socklen_t addrlen,
                          padico_socketfactory_acceptor_t notifier, void*key)
{
  int rc = -1, err = -1;
  struct sfplain_status_s*status = _status;
  rc  = PUK_ABI_FSYS_WRAP(listen)(status->fd, backlog);
  err = __puk_abi_wrap__errno;
  if(rc)
    {
      padico_warning("::listen() error rc=%d errno=%d (%s)\n", rc, err, strerror(err));
    }
  else
    {
      err = 0;
      status->acceptor = notifier;
      status->acceptor_key = key;
      padico_io_activate(status->io_listen);
    }
  padico_out(40, "rc=%d; errno=%d\n", rc, err);
  return -err;
}

static int sfplain_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                           padico_socketfactory_connector_t notifier, void*key)
{
  int rc = -1, err = -1;
  struct sfplain_status_s*status = _status;
  struct sockaddr* addr_any = NULL;
  switch(addr->sa_family)
    {
    case AF_INET:
      {
        struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
        addr_any = (struct sockaddr*)addr_in;
        padico_out(50, "::connect() inaddr = %s:%d\n", inet_ntoa(addr_in->sin_addr), ntohs(addr_in->sin_port));
        break;
      }
    case AF_UNIX:
      {
        struct sockaddr_un*addr_un = (struct sockaddr_un*)addr;
        addr_any = (struct sockaddr*)addr_un;
        padico_out(50, "::connect() path = %s\n", addr_un->sun_path);
        break;
      }
    case AF_PADICO:
      if(padico_sockaddr_magic(addr) == AF_PADICO_INET)
        {
          addr_any = (struct sockaddr*)padico_sockaddr_payload(struct sockaddr_in, addr);
        }
      else
        {
          err = EAFNOSUPPORT;
          goto error;
        }
      break;
    default:
      err = EAFNOSUPPORT;
      goto error;
    }

 retry_connect:
  rc  = PUK_ABI_FSYS_WRAP(connect)(status->fd, addr_any, addrlen);
  err = __puk_abi_wrap__errno;
  padico_out(50, "::connect() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
  if(rc == -1 && err == EINTR)
    goto retry_connect;

  status->connector = notifier;
  status->connector_key = key;

  if(rc == 0 || (rc == -1 && err == EINPROGRESS))
    {
      err = EINPROGRESS;
      status->io_connect = padico_io_register(status->fd, PADICO_IO_EVENT_WRITE, &sfplain_callback_connector, status);
      padico_io_activate(status->io_connect);
      padico_out(60, "callback_connector() registered\n");
    }
 error:
  padico_out(40, "err=%d\n", err);
  return -err;
}

static int sfplain_close(void*_status)
{
  struct sfplain_status_s*status = _status;
  int rc = -1;
  if(status->io_connect)
    {
      padico_io_deactivate(status->io_connect);
      padico_io_unregister(status->io_connect);
      status->io_connect = NULL;
    }
  if(status->io_listen)
    {
      padico_io_deactivate(status->io_listen);
      padico_io_unregister(status->io_listen);
      status->io_listen = NULL;
    }
  if(status->fd != -1)
    {
      rc = PUK_ABI_FSYS_WRAP(close)(status->fd);
      int err __attribute__((unused)) = __puk_abi_wrap__errno;
      padico_out(50, "::close() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
      status->fd = -1;
    }
  return rc;
}

static int sfplain_setsockopt(void*_status, int level, int opt, const void*val, int len)
{
  struct sfplain_status_s*status = _status;
  int rc  = PUK_ABI_FSYS_WRAP(setsockopt)(status->fd, level, opt, val, len);
  int err = __puk_abi_wrap__errno;
  return rc ? -err : 0;
}

/* ********************************************************* */

struct sfplain_acceptor_s
{
  struct sfplain_status_s*status;
  int             new_fd;
  struct sockaddr addr;
  socklen_t       addrlen;
};

static void*sfplain_acceptor_finalizer(void*_a)
{
  struct sfplain_acceptor_s*a = _a;
  a->addrlen = sizeof(a->addr);
  (*a->status->acceptor)(a->status->acceptor_key, a->new_fd, &a->addr, a->addrlen);
  padico_free(a);
  return NULL;
}

static void*sfplain_connector_finalizer(void*_status)
{
  struct sfplain_status_s*status = _status;
  (*status->connector)(status->connector_key, status->fd);
  return NULL;
}

static int sfplain_callback_acceptor(int fd, void*key)
{
  struct sfplain_status_s*status = key;
  int again = 1;
  int rc = -1, err = -1;
  struct sfplain_acceptor_s*a = padico_malloc(sizeof(struct sfplain_acceptor_s));
  a->addrlen = sizeof(a->addr);
  padico_out(60, "fd=%d; accepting...\n", fd);
 retry_accept:
  rc  = PUK_ABI_FSYS_WRAP(accept)(status->fd, &a->addr, &a->addrlen);
  err = __puk_abi_wrap__errno;
  padico_out(60, "fd=%d unlocked! accept() rc=%d errno=%d\n", fd, rc, err);
  if(rc == -1 && err == EINTR)
    goto retry_accept;
  if(rc != -1)
    {
      a->status = status;
      a->new_fd = rc;
      padico_out(30, "fd=%d ok new socket fd=%d\n",
                 fd, rc);
      padico_tm_bgthread_start(sfplain.pool, &sfplain_acceptor_finalizer, a,
                               "PlainSocketFactory:sfplain_acceptor_finalizer");
    }
  else
    {
      padico_warning("::accept() error rc=%d errno=%d (%s)\n", rc, err, strerror(err));
    }

  return again;
}

static int sfplain_callback_connector(int fd, void*key)
{
  int again = 0;
  struct sfplain_status_s*status = key;
  padico_out(40, "fd=%d\n", fd);
  assert(fd == status->fd);
  padico_tm_bgthread_start(sfplain.pool, &sfplain_connector_finalizer, status,
                           "PlainSocketFactory:sfplain_connector_finalizer");
  return again;
}



/* ********************************************************* */
