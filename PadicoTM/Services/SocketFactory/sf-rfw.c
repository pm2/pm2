/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Socket factory for reverse socket
*/

#include <Padico/VLink-internals.h>
#include <Padico/Module.h>

#include "SocketFactory.h"

static int sfrfw_module_init(void);
static void sfrfw_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_Reverse_Forward, sfrfw_module_init, NULL, sfrfw_module_finalize);

/** @defgroup SF_Reverse_Forward component: SocketFactory-reverse-forward- Socket factory which builds a socket in
 * the reverse way.
 * client -> server becomes :
 *
 * client <- gateway -> server
 *
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sfrfw_create(void*_status, int family, int type, int protocol);
static int sfrfw_connect(void*_status, const struct sockaddr*_addr, socklen_t addrlen,
                         padico_socketfactory_connector_t notifier, void*key);
static int sfrfw_close(void*_status);

/** 'SocketFactory' facet for SocketFactory-reverse-forward
 * @ingroup SF_Reverse_Forward
 */
static const struct padico_socketfactory_s sfrfw_sf_driver =
{
  .create  = &sfrfw_create,
  .bind    = NULL,
  .listen  = NULL,
  .connect = &sfrfw_connect,
  .close   = &sfrfw_close,
  .caps    = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
};

static void*sfrfw_instantiate(puk_instance_t, puk_context_t);
static void sfrfw_destroy(void*);

/** instanciation facet for SocketFactory-reverse-forward
 * @ingroup SF_Reverse_Forward
 */
static const struct puk_component_driver_s sfrfw_component_driver =
{
  .instantiate = &sfrfw_instantiate,
  .destroy     = &sfrfw_destroy
};

/** instance of SocketFactory-reverse-forward
 * @ingroup SF_Reverse_Forward
 */
struct sfrfw_status_s
{
  puk_instance_t instance;
  puk_context_t context;
  padico_topo_node_t gateway;
  unsigned int gateway_id;
};


struct sfrfw_gateway_status_s
{
  uintptr_t id;

  puk_instance_t server_instance;
  int server_fd;
  padico_io_t server_io;

  padico_topo_host_t client_host;
  uint32_t client_port;
  puk_instance_t client_instance;
  int client_fd;
  padico_io_t client_io;
};

static struct
{
  puk_iface_t iface_sf;
  puk_hashtable_t connections; /* hash: id -> status */
  unsigned int next_id;
  marcel_mutex_t connections_mtx;
  padico_tm_bgthread_pool_t pool;
  puk_component_t component;
} sfrfw_gateway = { .component = NULL };

/* ********************************************************* */

static void sfrfw_gateway_connect_server_end_handler(puk_parse_entity_t e);
static void sfrfw_gateway_close_end_handler(puk_parse_entity_t e);

static int sfrfw_module_init(void)
{
  sfrfw_gateway.component =
    puk_component_declare("SocketFactory_Reverse_Forward",
                          puk_component_provides("PadicoComponent", "component", &sfrfw_component_driver),
                          puk_component_provides("SocketFactory", "prov-sf", &sfrfw_sf_driver),
                          puk_component_uses("SocketFactory", "used-sf"),
                          puk_component_attr("gateway_node_name", NULL));
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "SocketFactory_Reverse_Forward:Connect",
                     .start_handler  = NULL,
                     .end_handler    = &sfrfw_gateway_connect_server_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "SocketFactory_Reverse_Forward:Close",
                     .start_handler  = NULL,
                     .end_handler    = &sfrfw_gateway_close_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  sfrfw_gateway.iface_sf = puk_iface_lookup("SocketFactory");
  sfrfw_gateway.connections = puk_hashtable_new_int();
  sfrfw_gateway.next_id = 1;
  marcel_mutex_init(&sfrfw_gateway.connections_mtx, 0);
  sfrfw_gateway.pool = padico_tm_bgthread_pool_create("SocketFactory_Reverse_Forward");
  return 0;
}

static void sfrfw_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfrfw_gateway.pool);
  puk_xml_delete_action("SocketFactory_Reverse_Forward:Connect");
  puk_xml_delete_action("SocketFactory_Reverse_Forward:Close");
  puk_hashtable_delete(sfrfw_gateway.connections, NULL);
  puk_component_destroy(sfrfw_gateway.component);
}


/* ********************************************************* */
/* *** 'PadicoComponent' for ReverseForward SocketFactory      */

static void*sfrfw_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct sfrfw_status_s*status = padico_malloc(sizeof(struct sfrfw_status_s));
  *status = (struct sfrfw_status_s) {
        .instance    = instance,
        .context     = context,
        .gateway     = NULL,
        .gateway_id  = 0
  };
  padico_out(40, "done.\n");
  return status;
}

static void sfrfw_destroy(void*_status)
{
  struct sfrfw_status_s*status = _status;
  sfrfw_close(status);
  padico_free(status);
}

/* ********************************************************* */
/* *** 'SocketFactory' for ReverseForwardSocketFactory */

static int sfrfw_create(void*_status, int family, int type, int protocol)
{
  return 0;
}

struct sfrfw_connector_param_s
{
  struct sfrfw_status_s*status;
  padico_socketfactory_connector_t notifier;
  void*key;
};
static void sfrfw_client_acceptor(void*key, int new_fd, const struct sockaddr*addr, socklen_t addrlen);
static int sfrfw_launch_gateway(const char*server_host, const uint16_t server_port,
                                struct sfrfw_status_s*status, const uint16_t client_port);

static int sfrfw_connect(void*_status, const struct sockaddr*_addr, socklen_t addrlen,
                         padico_socketfactory_connector_t notifier, void*key)
{
  struct sfrfw_status_s*status = _status;
  const struct sockaddr_in*server_addr = (struct sockaddr_in*) _addr;
  int server_port = ntohs(server_addr->sin_port);
  padico_topo_host_t padico_server_host = padico_topo_host_resolve_byinaddr(&(server_addr->sin_addr));
  const char*server_host = padico_topo_host_getname(padico_server_host);
  if(padico_server_host == NULL)
    {
      padico_warning("connect: host=%s not found\n", server_host);
      return -1;
    }
  struct puk_receptacle_SocketFactory_s sf;
  puk_context_indirect_SocketFactory(status->instance, "used-sf", &sf);
  int rc = (*sf.driver->create)(sf._status, AF_INET, SOCK_STREAM, 0);
  if(rc != 0)
    {
      padico_warning("connect: cannot create the socket for host=%s\n", server_host);
      return -1;
    }
  struct sockaddr_in client_addr;
  int client_addrlen = sizeof(struct sockaddr_in);
  bzero(&client_addr, sizeof(client_addr));
  client_addr.sin_family = AF_INET;
  client_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  client_addr.sin_port = 0;
  rc = (*sf.driver->bind)(sf._status, (struct sockaddr*) &client_addr, (socklen_t*) &client_addrlen);
  if(rc >= 0)
    {
      struct sfrfw_connector_param_s*param = padico_malloc(sizeof(struct sfrfw_connector_param_s));
      *param = (struct sfrfw_connector_param_s) {
          .status   = status,
          .notifier = notifier,
          .key      = key
      };
      rc = (*sf.driver->listen)(sf._status, 10, (struct sockaddr*) &client_addr, client_addrlen,
                                sfrfw_client_acceptor, param);
    }
  if(rc != 0)
    {
      padico_warning("connect: bind or listen failed.\n");
      return rc;
    }
  /* TODO: if the attribute is not set we could take the server host as gateway */
  const char*gateway_node_name = puk_context_getattr(status->context, "gateway_node_name");
  status->gateway = padico_topo_getnodebyname(gateway_node_name);
  if(status->gateway == NULL)
    {
      padico_warning("launch_gateway(): unknown gateway node=%s\n",
                     gateway_node_name);
      return -1;
    }
  rc = sfrfw_launch_gateway(server_host, server_port, status, client_addr.sin_port);
  if(rc == 0)
    rc = -EINPROGRESS;
  return rc;
}

static void sfrfw_client_acceptor(void*key, int new_fd, const struct sockaddr*addr, socklen_t addrlen)
{
  struct sfrfw_connector_param_s*param = key;

  padico_out(50, "close listening socket.\n");
  struct puk_receptacle_SocketFactory_s sf;
  puk_context_indirect_SocketFactory(param->status->instance, "used-sf", &sf);
  (*sf.driver->close)(sf._status);
  padico_out(10, "connected.\n");
  (param->notifier)(param->key, new_fd);

  padico_free(param);
}

static void sfrfw_launch_gateway_notifer(padico_rc_t rc, void*_key)
{
  struct sfrfw_status_s*status = _key;
  int err = padico_rc_iserror(rc);
  if(err != 0)
    {
      padico_warning("Error: remote connection failed: %s.\n",
                     padico_rc_gettext(rc));
      err = -1;
    }
  else
    {
      char*s_id = padico_rc_gettext(rc);
      status->gateway_id = atoi(s_id);
      padico_out(10, "launched gateway on node=%s, id=%d\n",
                 padico_topo_node_getname(status->gateway), status->gateway_id);
    }
  padico_rc_delete(rc);
}

static int sfrfw_launch_gateway(const char*server_host, const uint16_t server_port,
                                struct sfrfw_status_s*status, const uint16_t client_port)
{
  padico_out(20, "init gateway side (gateway node=%s, local port=%d, host=%s:%d)..\n",
             padico_topo_node_getname(status->gateway), client_port, server_host, server_port);
  padico_string_t s_launch = padico_string_new();
  padico_string_catf(s_launch,"<SocketFactory_Reverse_Forward:Connect serverHost=\"%s\" serverPort=\"%d\" " ,
                     server_host, server_port);
  padico_string_catf(s_launch,"clientHost=\"%s\" clientPort=\"%d\"/>",
                     padico_topo_hostname(), client_port);
  padico_control_send_async(status->gateway, padico_string_get(s_launch), &sfrfw_launch_gateway_notifer, status);
  padico_string_delete(s_launch);

  return -EINPROGRESS;
}

static int sfrfw_close(void*_status)
{
  struct sfrfw_status_s*status = _status;
  if(status->gateway_id != 0)
    {
      //TODO: not useful? the gateway can see when the socket close.
      padico_out(10, "closing gateway=%s id=%d.\n", padico_topo_node_getname(status->gateway), status->gateway_id);
      padico_string_t s_close = padico_string_new();
      padico_string_catf(s_close,"<SocketFactory_Reverse_Forward:Close id=\"%d\"/>", status->gateway_id);
      padico_control_send_oneway(status->gateway, padico_string_get(s_close));
      padico_string_delete(s_close);
    }
  padico_out(10, "closed.\n");
  return 0;
}

/* ********************************************************* */
/* *** gateway                                               */

static struct sfrfw_gateway_status_s*sfrfw_gateway_new();
static puk_instance_t sfrfw_gateway_get_socket(padico_topo_host_t padico_server_host, uint16_t port);
static void sfrfw_gateway_set_error(puk_parse_entity_t e, char*error);
static void sfrfw_gateway_server_callback_connector(void*key, int fd);
static const int GATEWAY_READ_BUFSIZE = 4 * 1024;
static int sfrfw_gateway_server_read_callback(int fd, void*key);
static void sfrfw_gateway_connect_client(struct sfrfw_gateway_status_s*status);
static void sfrfw_gateway_client_callback_connector(void*key, int fd);
static int sfrfw_gateway_client_read_callback(int fd, void*key);
static void*sfrfw_gateway_error(void*_status);
static void sfrfw_gateway_close(struct sfrfw_gateway_status_s*status);


/* connection from gateway to the server socket */
static void sfrfw_gateway_connect_server_end_handler(puk_parse_entity_t e)
{
  char error[64] = {0};
  const char*s_server_host = puk_parse_getattr(e, "serverHost");
  const char*s_server_port = puk_parse_getattr(e, "serverPort");
  const char*s_client_host = puk_parse_getattr(e, "clientHost");
  const char*s_client_port = puk_parse_getattr(e, "clientPort");
  assert(s_server_port && s_server_host && s_client_host && s_client_port);
  padico_out(20, "start gateway: host=%s port=%s client host=%s..\n",
             s_server_host, s_server_port, s_client_host);
  padico_topo_host_t padico_server_host = padico_topo_host_resolvebyname(s_server_host);
  if(padico_server_host == NULL)
    {
      sprintf(error, "server host=%s not found", s_server_host);
      sfrfw_gateway_set_error(e, error);
      return;
    }
  const padico_topo_host_t client_host = padico_topo_host_resolvebyname(s_client_host);
  assert(client_host);
  padico_out(20, "connecting to server host=%s port=%s ..\n", s_server_host, s_server_port);

  struct sfrfw_gateway_status_s*status = sfrfw_gateway_new();
  status->client_host = client_host;
  status->client_port = atol(s_client_port);
  status->server_instance = sfrfw_gateway_get_socket(padico_server_host, (uint16_t) atoi(s_server_port));
  struct puk_receptacle_SocketFactory_s sf;
  puk_instance_indirect_SocketFactory(status->server_instance, NULL, &sf);
  int rc = (*sf.driver->create)(sf._status, AF_INET, SOCK_STREAM, 0);
  if(rc != 0)
    {
      sprintf(error, "cannot create the socket for host=%s", s_server_host);
      sfrfw_gateway_set_error(e, error);
      sfrfw_gateway_error(status);
      return;//TODO free
    }
  struct sockaddr_in server_addr;
  bzero(&server_addr, sizeof(server_addr));
  server_addr.sin_family      = AF_INET;
  server_addr.sin_addr.s_addr = padico_topo_host_getaddr(padico_server_host)->s_addr;
  server_addr.sin_port        = htons((uint16_t) atoi(s_server_port));
  rc = (*sf.driver->connect)(sf._status, (struct sockaddr*) &server_addr, sizeof(server_addr),
                             sfrfw_gateway_server_callback_connector, status);
  if(rc != -EINPROGRESS && rc != 0)
    {
      sprintf(error, "connection failed towards host=%s port=%s", s_server_host, s_server_port);
      sfrfw_gateway_set_error(e, error);
      sfrfw_gateway_error(status);
      return;
    }
  puk_parse_set_rc(e, padico_rc_msg("%d", status->id));
}

static struct sfrfw_gateway_status_s*sfrfw_gateway_new()
{
  struct sfrfw_gateway_status_s*status = padico_malloc(sizeof(struct sfrfw_gateway_status_s));
  marcel_mutex_lock(&sfrfw_gateway.connections_mtx);
  *status = (struct sfrfw_gateway_status_s) {
      .client_host     = NULL,
      .client_port     = 0,
      .id              = sfrfw_gateway.next_id++, /* int is big enough? */
      .server_instance = NULL,
      .server_fd       = -1,
      .server_io       = NULL,
      .client_instance = NULL,
      .client_fd       = -1,
      .client_io       = NULL
  };
  puk_hashtable_insert(sfrfw_gateway.connections, (void*) status->id, status);
  marcel_mutex_unlock(&sfrfw_gateway.connections_mtx);
  return status;
}

static puk_instance_t sfrfw_gateway_get_socket(padico_topo_host_t padico_server_host, uint16_t port)
{
  puk_component_t assembly = NULL;
  assembly = padico_ns_host_selector(padico_server_host, "inet", sfrfw_gateway.iface_sf, port, IPPROTO_TCP);
  if(assembly == NULL)
    padico_fatal("no SocketFactory assembly found.\n");
  padico_out(50, "host=%s served by assembly %s\n", padico_topo_host_getname(padico_server_host), assembly->name);
  return puk_component_instantiate(assembly);
}

static void sfrfw_gateway_set_error(puk_parse_entity_t e, char*error)
{
  padico_warning("%s\n", error);
  padico_rc_t rc = padico_rc_error("Error: %s", error);
  puk_parse_set_rc(e, rc);
}

static void sfrfw_gateway_server_callback_connector(void*key, int fd)
{
  struct sfrfw_gateway_status_s*status = (struct sfrfw_gateway_status_s*) key;
  status->server_fd = fd;
  status->server_io = padico_io_register(fd, PADICO_IO_EVENT_READ, &sfrfw_gateway_server_read_callback, status);
  padico_out(10, "connection to server host done (fd=%d).\n", fd);
  sfrfw_gateway_connect_client(status);
}

static int sfrfw_gateway_server_read_callback(int fd, void*key)
{
  struct sfrfw_gateway_status_s*status = key;
  char buf[GATEWAY_READ_BUFSIZE];
  int sysrc = padico_sysio_read(fd, &buf, sizeof(buf));
  if(sysrc > 0)
    {
      padico_out(50, "size : %d \n", sysrc);
      padico_sysio_out(status->client_fd, &buf, sysrc);
      return 1;
    }
  else
    {
      padico_out(20, "connection lost\n");
      padico_tm_bgthread_start(sfrfw_gateway.pool, &sfrfw_gateway_error, status, "gateway_error");
      return 0;
    }
}

static void sfrfw_gateway_connect_client(struct sfrfw_gateway_status_s*status)
{
  padico_out(20, "reverse connect to client node=%s ..\n", padico_topo_host_getname(status->client_host));
  status->client_instance = sfrfw_gateway_get_socket(status->client_host, ntohs(status->client_port));
  struct puk_receptacle_SocketFactory_s sf;
  puk_instance_indirect_SocketFactory(status->client_instance, NULL, &sf);
  int rc = (*sf.driver->create)(sf._status, AF_INET, SOCK_STREAM, 0);
  if(rc != 0)
    {
      padico_warning("reverse connect: cannot create the socket for node=%s\n",
                     padico_topo_host_getname(status->client_host));
      padico_tm_bgthread_start(sfrfw_gateway.pool, &sfrfw_gateway_error, status, "gateway_error");
      //TODO: the sockets of the gateway will be closed but the client will not be informed of the error.
      return;
    }
  struct sockaddr_in client_addr;
  int client_addrlen = sizeof(struct sockaddr_in);
  bzero(&client_addr, sizeof(client_addr));
  client_addr.sin_family = AF_INET;
  client_addr.sin_addr   = *padico_topo_host_getaddr(status->client_host);
  client_addr.sin_port   = status->client_port;
  padico_out(10, "connect to addr=%s port=%d\n", inet_ntoa(client_addr.sin_addr), ntohs(status->client_port));
  rc = (*sf.driver->connect)(sf._status, (struct sockaddr*) &client_addr, client_addrlen,
                             sfrfw_gateway_client_callback_connector, status);
  if(rc != -EINPROGRESS && rc != 0)
    {
      padico_warning("reverse connect failed (client node=%s).\n",
                     padico_topo_host_getname(status->client_host));
      padico_tm_bgthread_start(sfrfw_gateway.pool, &sfrfw_gateway_error, status, "gateway_error");
      //TODO: the sockets of the gateway will be closed but the client will not be informed of the error.
    }
}

static void sfrfw_gateway_client_callback_connector(void*key, int fd)
{
  struct sfrfw_gateway_status_s*status = key;
  padico_out(10, "reverse connection to client node=%s ok (fd=%d).\n",
             padico_topo_host_getname(status->client_host), fd);
  status->client_fd = fd;
  status->client_io = padico_io_register(fd, PADICO_IO_EVENT_READ, &sfrfw_gateway_client_read_callback, status);
  padico_io_activate(status->client_io);
  padico_io_activate(status->server_io);
}

static int sfrfw_gateway_client_read_callback(int fd, void*key)
{
  struct sfrfw_gateway_status_s*status = key;
  char buf[GATEWAY_READ_BUFSIZE];
  int sysrc = padico_sysio_read(fd, &buf, sizeof(buf));
  if(sysrc > 0)
    {
      padico_out(50, "size : %d \n", sysrc);
      padico_sysio_out(status->server_fd, &buf, sysrc);
      return 1;
    }
  else
    {
      padico_out(20, "connection lost\n");
      padico_tm_bgthread_start(sfrfw_gateway.pool, &sfrfw_gateway_error, status, "gateway_error");
      return 0;
    }
}

static void*sfrfw_gateway_error(void*_status)
{
  struct sfrfw_gateway_status_s*status = _status;
  marcel_mutex_lock(&sfrfw_gateway.connections_mtx);
  puk_hashtable_remove(sfrfw_gateway.connections, (void*) status->id);
  marcel_mutex_unlock(&sfrfw_gateway.connections_mtx);

  sfrfw_gateway_close(status);
  return NULL;
}

static void sfrfw_gateway_close(struct sfrfw_gateway_status_s*status)
{
  padico_out(10, "closing connection id=%lu\n", status->id);
  if(status->server_io)
    {
      padico_io_deactivate(status->server_io);
      padico_io_unregister(status->server_io);
      status->server_io = NULL;
    }
  if(status->server_instance)
    {
      struct puk_receptacle_SocketFactory_s sf;
      puk_instance_indirect_SocketFactory(status->server_instance, NULL, &sf);
      (*sf.driver->close)(sf._status);
    }
  if(status->client_io)
    {
      padico_io_deactivate(status->client_io);
      padico_io_unregister(status->client_io);
      status->client_io = NULL;
    }
  if(status->client_instance)
    {
      struct puk_receptacle_SocketFactory_s sf;
      puk_instance_indirect_SocketFactory(status->client_instance, NULL, &sf);
      (*sf.driver->close)(sf._status);
    }
  padico_free(status);
}

static void sfrfw_gateway_close_end_handler(puk_parse_entity_t e)
{
  const char*s_id = puk_parse_getattr(e, "id");
  const uintptr_t id = atoi(s_id);
  padico_out(50, "close request for id=%lu.\n", id);
  marcel_mutex_lock(&sfrfw_gateway.connections_mtx);
  void*_status = puk_hashtable_lookup(sfrfw_gateway.connections, (void*) id);
  if(_status)
    puk_hashtable_remove(sfrfw_gateway.connections, (void*) id);
  marcel_mutex_unlock(&sfrfw_gateway.connections_mtx);
  if(_status)
    sfrfw_gateway_close((struct sfrfw_gateway_status_s*) _status);
}

/* TODO: unload module function */
