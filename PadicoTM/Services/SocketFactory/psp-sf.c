/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file PSP interface over sockets
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/SysIO.h>
#include <Padico/AddrDB.h>
#include "SocketFactory.h"

static int psp_socketfactory_module_init(void);
static void psp_socketfactory_module_finalize(void);

PADICO_MODULE_DECLARE(PSP_SocketFactory, psp_socketfactory_module_init, NULL, psp_socketfactory_module_finalize,
                      "SysIO", "AddrDB", "PSP", "Topology");

/* ********************************************************* */

static void*psp_sf_instantiate(puk_instance_t i, puk_context_t context);
static void psp_sf_destroy(void*_status);

static void psp_sf_init(void*_status, uint32_t tag, const char*label, size_t*h_size,
                        padico_psp_handler_t handler, void*key);
static void psp_sf_connect(void*_status, padico_topo_node_t node);
static padico_psp_connection_t psp_sf_new_message(void*_status, padico_topo_node_t node, void**sbuf);
static void psp_sf_pack(void*_status, padico_psp_connection_t conn,
                        const char*bytes, size_t size);
static void psp_sf_end_message(void*_status, padico_psp_connection_t conn);

static const struct puk_component_driver_s psp_sf_component_driver =
  {
    .instantiate = &psp_sf_instantiate,
    .destroy     = &psp_sf_destroy
  };

static const struct padico_psp_driver_s psp_sf_psp_driver =
  {
    .init        = &psp_sf_init,
    .listen      = NULL,
    .connect     = &psp_sf_connect,
    .new_message = &psp_sf_new_message,
    .pack        = &psp_sf_pack,
    .end_message = &psp_sf_end_message
  };

struct psp_sf_status_s
{
  padico_psp_slot_t slot;
  padico_topo_node_t peer;
  struct puk_receptacle_SocketFactory_s sf;
  int fd;
  void*sbuf;
  int h_sent;
  padico_io_t io;
};

static struct
{
  struct padico_psp_directory_s directory;
  marcel_mutex_t lock;
  marcel_cond_t event;
  puk_component_t component;
} psp_sf;


/* ********************************************************* */

static int psp_socketfactory_module_init(void)
{
  marcel_mutex_init(&psp_sf.lock, NULL);
  marcel_cond_init(&psp_sf.event, NULL);
  padico_psp_directory_init(&psp_sf.directory);
  psp_sf.component =
    puk_component_declare("PSP_SocketFactory",
                          puk_component_provides("PadicoComponent", "component", &psp_sf_component_driver),
                          puk_component_provides("PadicoSimplePackets", "psp", &psp_sf_psp_driver),
                          puk_component_uses("SocketFactory", "sf"));
  return 0;
}

static void psp_socketfactory_module_finalize(void)
{
  puk_component_destroy(psp_sf.component);
}

/* ********************************************************* */

static void*psp_sf_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct psp_sf_status_s*status = padico_malloc(sizeof(struct psp_sf_status_s));
  status->slot = NULL;
  status->sbuf = NULL;
  puk_context_indirect_SocketFactory(instance, "sf", &status->sf);
  return status;
}

static void psp_sf_destroy(void*_status)
{
  struct psp_sf_status_s*status = _status;
  padico_psp_slot_remove(&psp_sf.directory, status->slot);
  if(status->sbuf)
    padico_free(status->sbuf);
  padico_free(status);
}

/* ********************************************************* */

static void psp_sf_init(void*_status, uint32_t tag, const char*label, size_t*h_size,
                        padico_psp_handler_t handler, void*key)
{
  struct psp_sf_status_s*status = _status;
  status->slot = padico_psp_slot_insert(&psp_sf.directory, handler, key, tag, *h_size);
  status->peer = NULL;
  status->fd = -1;
  if(*h_size > 0)
    {
      status->sbuf = padico_malloc(*h_size);
    }
  int rc = (*status->sf.driver->create)(status->sf._status, AF_INET, SOCK_STREAM, 0);
  if(rc)
    {
      padico_warning("error while creating socket.\n");
    }
}

static void psp_sf_acceptor(void*_status, int fd, const struct sockaddr*addr, socklen_t addrlen)
{
  struct psp_sf_status_s*status = _status;
  marcel_mutex_lock(&psp_sf.lock);
  status->fd = fd;
  marcel_cond_broadcast(&psp_sf.event);
  marcel_mutex_unlock(&psp_sf.lock);
}
static void psp_sf_connector(void*_status, int fd)
{
  struct psp_sf_status_s*status = _status;
  marcel_mutex_lock(&psp_sf.lock);
  status->fd = fd;
  marcel_cond_broadcast(&psp_sf.event);
  marcel_mutex_unlock(&psp_sf.lock);
}

static void psp_sf_pump(void*_status, void*bytes, size_t size)
{
  struct psp_sf_status_s*status = _status;
  padico_sysio_in(status->fd, bytes, size);
}

static int psp_sf_callback(int fd, void*_status)
{
  static void*hbuf = NULL;
  static int hsize = 0;
  struct psp_sf_status_s*status = _status;
  if(status->slot->h_size > hsize)
    {
      hsize = status->slot->h_size;
      hbuf = padico_realloc(hbuf, hsize);
    }
  if(status->slot->h_size > 0)
    {
      padico_sysio_in(status->fd, hbuf, status->slot->h_size);
    }
  (*status->slot->handler)(hbuf, status->peer, status->slot->key, &psp_sf_pump, status);
  return 1;
}

static void psp_sf_connect(void*_status, padico_topo_node_t node)
{
  struct psp_sf_status_s*status = _status;
  if(status->peer != NULL)
    {
      padico_fatal("PSP_SocketFactory supports only 1 node per instance.\n");
    }
  status->peer = node;
  padico_topo_uuid_t self_uuid = padico_topo_node_getuuid(padico_topo_getlocalnode());
  padico_topo_uuid_t peer_uuid = padico_topo_node_getuuid(node);
  if(memcmp(self_uuid, peer_uuid, PADICO_TOPO_UUID_SIZE) > 0)
    {
      /* server */
      struct sockaddr_pa s = { .spa_family = AF_PADICO, .spa_magic = AF_PADICO_AUTO, .spa_payload = { 0 } };
      socklen_t l = sizeof(s);
      int rc = (*status->sf.driver->bind)(status->sf._status, (struct sockaddr*)&s, &l);
      if(rc)
        {
          padico_warning("bind() rc = %d\n", rc);
        }
      padico_addrdb_publish(node, "PSP_SocketFactory",
                            &status->slot->tag, sizeof(status->slot->tag),
                            &s, l);
      rc = (*status->sf.driver->listen)(status->sf._status, 1, NULL, 0, &psp_sf_acceptor, status);
      if(rc)
        {
          padico_warning("listen() rc = %d\n", rc);
        }
    }
  else
    {
      /* client */
      struct sockaddr_pa s = { .spa_family = AF_PADICO, .spa_magic = AF_PADICO_AUTO, .spa_payload = { 0 } };
      socklen_t l = sizeof(s);
      padico_req_t req = padico_tm_req_new(NULL, NULL);
      padico_addrdb_get(node, "PSP_SocketFactory",
                        &status->slot->tag, sizeof(status->slot->tag), &s, l, req);
      padico_tm_req_wait(req);
      int rc = (*status->sf.driver->connect)(status->sf._status, (struct sockaddr*)&s, l, &psp_sf_connector, status);
      if(rc && -rc != EINPROGRESS)
        {
          padico_warning("connect() rc = %d (%s)\n", rc, strerror(-rc));
        }
    }
  marcel_mutex_lock(&psp_sf.lock);
  while(status->fd == -1)
    {
      marcel_cond_wait(&psp_sf.event, &psp_sf.lock);
    }
  marcel_mutex_unlock(&psp_sf.lock);
  status->io = padico_io_register(status->fd, PADICO_IO_EVENT_READ, &psp_sf_callback, status);
  padico_io_activate(status->io);
}

static padico_psp_connection_t psp_sf_new_message(void*_status, padico_topo_node_t node, void**sbuf)
{
  struct psp_sf_status_s*status = _status;
  if(status->peer != node)
    {
      padico_fatal("wrong destination node given to new_message().\n");
    }
  status->h_sent = 0;
  *sbuf = status->sbuf;
  return NULL;
}

static void psp_sf_pack(void*_status, padico_psp_connection_t conn,
                        const char*bytes, size_t size)
{
  struct psp_sf_status_s*status = _status;
  if(!status->h_sent)
    {
      padico_sysio_out(status->fd, status->sbuf, status->slot->h_size);
      status->h_sent = 1;
    }
  padico_sysio_out(status->fd, bytes, size);
}

static void psp_sf_end_message(void*_status, padico_psp_connection_t conn)
{
  struct psp_sf_status_s*status = _status;
  if(!status->h_sent)
    {
      padico_sysio_out(status->fd, status->sbuf, status->slot->h_size);
      status->h_sent = 1;
    }
}
