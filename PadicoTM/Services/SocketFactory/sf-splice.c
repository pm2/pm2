/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Socket factory for TCP splicing
 */

#include <Padico/PadicoTM.h>
#include <Padico/VLink-API.h>
#include <Padico/PadicoControl.h>
#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "SocketFactory.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static int sfsplice_module_init(void);
static void sfsplice_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_Splicing, sfsplice_module_init, NULL, sfsplice_module_finalize,
                      "Topology", "VIO", "SysIO", "PadicoControl");


/* Splicing request format:
   <SocketFactory_Splicing:request token="xx">
     [ source address ]
   </SocketFactory_Splicing:request>
 */


/** @defgroup SF_splice component: SocketFactory-splice- socket factory that performs TCP splicing
 * @ingroup PadicoComponent
 */


/* ********************************************************* */


/* ** 'SocketFactory' driver */

static int sfsplice_create(void*_instance, int family, int type, int protocol);
static int sfsplice_bind(void*_instance, struct sockaddr*addr, socklen_t*addrlen);
static int sfsplice_listen(void*_instance, int backlog, const struct sockaddr*addr, socklen_t addrlen,
                           padico_socketfactory_acceptor_t notifier, void*key);
static int sfsplice_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen,
                            padico_socketfactory_connector_t notifier, void*key);

/** 'SocketFactory' facet for SocketFactory-splice
 * @ingroup SF_splice
 */
static const struct padico_socketfactory_s sfsplice_sf_driver =
  {
    .create  = &sfsplice_create,
    .bind    = &sfsplice_bind,
    .listen  = &sfsplice_listen,
    .connect = &sfsplice_connect,
    .caps    = padico_vsock_caps_secondaryonly | padico_vsock_caps_clientmustbind
  };

/* ** 'PadicoComponent' driver */

static void*sfsplice_instantiate(puk_instance_t, puk_context_t);
static void sfsplice_destroy(void*);

/** instanciation facet for SocketFactory-splice
 * @ingroup SF_splice
 */
static const struct puk_component_driver_s sfsplice_component_driver =
  {
    .instantiate = &sfsplice_instantiate,
    .destroy     = &sfsplice_destroy
  };

/** instance of SocketFactory-splice
 * @ingroup SF_splice
 */
struct sfsplice_instance_s
{
  long key;
  int fd;
  struct sockaddr_in my_inaddr;
  struct sockaddr_in peer_inaddr;
  padico_io_t io_connect;
  int bound;
  union
  {
    struct
    {
      padico_socketfactory_acceptor_t handler;
      void*key;
    } acceptor;
    struct
    {
      padico_socketfactory_connector_t handler;
      void*key;
    } connector;
  };
};


/* ********************************************************* */

/* ** addresses */

#define AF_PADICO_SPLC AF_PADICO_MAGIC('S','P','L','C')

/** packed "splicing" address
 */
struct sockaddr_padico_splc
{
  uint32_t splc_key;
  uint32_t splc_addr;
  uint16_t splc_port;
  char     splc_node[AF_PADICO_PAYLOAD-16];
};

/* ** Globals */

static struct
{
  marcel_mutex_t lock;
  puk_hashtable_t instances; /**< key: 'key'; data: instance */
  long _next_key;
  padico_tm_bgthread_pool_t pool;
  puk_component_t component;
} sfsplc = { ._next_key = 1, .component = NULL };

static int sfsplice_callback_acceptor(int fd, void*key);
static int sfsplice_callback_connector(int fd, void*key);
static int sfsplice_do_splicing(struct sfsplice_instance_s*instance,
                                int(*finalizer)(int, void*));
static void sfsplice_request_handler(puk_parse_entity_t e);

static inline void sfsplice_register(struct sfsplice_instance_s*instance)
{
  marcel_mutex_lock(&sfsplc.lock);
  instance->key = sfsplc._next_key++;
  puk_hashtable_insert(sfsplc.instances, puk_long2ptr(instance->key), instance);
  marcel_mutex_unlock(&sfsplc.lock);
}

static inline void sfsplice_unregister(struct sfsplice_instance_s*instance)
{
  marcel_mutex_lock(&sfsplc.lock);
  if(instance->key != -1)
    {
      puk_hashtable_remove(sfsplc.instances, puk_long2ptr(instance->key));
    }
  marcel_mutex_unlock(&sfsplc.lock);
}

static inline struct sfsplice_instance_s*sfsplice_lookup(long token)
{
  marcel_mutex_lock(&sfsplc.lock);
  struct sfsplice_instance_s*const instance =
    puk_hashtable_lookup(sfsplc.instances, puk_long2ptr(token));
  marcel_mutex_unlock(&sfsplc.lock);
  return instance;
}

/* ********************************************************* */

static int sfsplice_module_init(void)
{
  sfsplc.instances = puk_hashtable_new_int();
  marcel_mutex_init(&sfsplc.lock, NULL);
  sfsplc.pool = padico_tm_bgthread_pool_create("SocketFactory-Splice");
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "SocketFactory_Splicing:request",
    .end_handler    = &sfsplice_request_handler,
    .required_level = PUK_TRUST_CONTROL,
    .help           = "Used internally by SocketFactory_Splicing"
  });
  sfsplc.component =
    puk_component_declare("SocketFactory_Splicing",
                          puk_component_provides("PadicoComponent", "component",&sfsplice_component_driver),
                          puk_component_provides("SocketFactory", "sf", &sfsplice_sf_driver));
  return 0;
}

static void sfsplice_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfsplc.pool);
  puk_component_destroy(sfsplc.component);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for SocketFactory_Splicing */

static void*sfsplice_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct sfsplice_instance_s*instance = padico_malloc(sizeof(struct sfsplice_instance_s));
  instance->fd    = -1;
  instance->bound = 0;
  instance->key   = -1;
  instance->io_connect = NULL;
  instance->acceptor.handler  = NULL;
  instance->connector.handler = NULL;
  instance->peer_inaddr.sin_family = -1;
  sfsplice_register(instance);
  padico_out(40, "done.\n");
  return instance;
}

static void sfsplice_destroy(void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  if(instance->io_connect)
    {
      padico_io_deactivate(instance->io_connect);
      padico_io_unregister(instance->io_connect);
      instance->io_connect = NULL;
    }
  sfsplice_unregister(instance);
  padico_free(instance);
  padico_out(40, "done.\n");
}

/* ********************************************************* */
/* *** 'SocketFactory' for SocketFactory_Splicing */

static int sfsplice_create(void*_instance, int family, int type, int protocol)
{
  struct sfsplice_instance_s*instance = _instance;
  int fd  = padico_sysio_socket(family, type, protocol);
  int err = padico_sysio_errno();
  if(fd > -1)
    {
      err = 0;
      instance->fd = fd;
    }
  padico_out(40, "fd=%d; err=%d\n", fd, err);
  return -err;
}

static int sfsplice_bind(void*_instance, struct sockaddr*_addr, socklen_t*addrlen)
{
  struct sfsplice_instance_s*const instance = _instance;
  struct sockaddr_pa*const addr = (struct sockaddr_pa*)_addr;
  int rc = -1;
  int err = padico_sockaddr_check(addr, AF_PADICO_AUTO);
  struct sockaddr_in addr_in =
    {
      .sin_family = AF_INET,
      .sin_addr   = *padico_topo_host_getaddr(padico_topo_getlocalhost()),
      .sin_port   = htons(0)
    };
  if(err)
    goto error;
  /* bind physical socket */
  rc  = PUK_ABI_FSYS_WRAP(bind)(instance->fd, &addr_in, sizeof(addr_in));
  err = __puk_abi_wrap__errno;
  if(rc)
    goto error;
  /* get address of physical socket */
  socklen_t inaddrlen = sizeof(instance->my_inaddr);
  rc  = PUK_ABI_FSYS_WRAP(getsockname)(instance->fd, &instance->my_inaddr, &inaddrlen);
  err = __puk_abi_wrap__errno;
  if(rc)
    goto error;
  /* generate virtual address in the AF_PADICO address family */
  struct sockaddr_padico_splc*const splcaddr =
    padico_sockaddr_payload(struct sockaddr_padico_splc, addr);
  if(*addrlen < sizeof(struct sockaddr_pa))
    {
      /* SocketFactory has no 'getsockname' thus it *is* an error
       * when we cannot return the address. @todo Investigate whether
       * we should return EFAULT or EINVAL in this case.
       */
      err = EFAULT;
      goto error;
    }
  padico_sockaddr_init(addr, addrlen, AF_PADICO_SPLC);
  *splcaddr = (struct sockaddr_padico_splc)
    {
      .splc_addr = instance->my_inaddr.sin_addr.s_addr,
      .splc_port = instance->my_inaddr.sin_port,
      .splc_key  = htonl(instance->key)
    };
  const char*nodename = padico_topo_nodename();
  assert(strlen(nodename) < sizeof(splcaddr->splc_node));
  strncpy(splcaddr->splc_node, nodename, sizeof(splcaddr->splc_node));
  instance->bound = 1;
  err = 0;
 error:
  if(err)
    {
      padico_warning("error in bind()- err=%d (%s)\n",
                     err, strerror(err));
    }
  padico_out(40, "fd=%d; err=%d\n", instance->fd, err);
  return -err;
}

static int sfsplice_listen(void*_instance, int backlog, const struct sockaddr*addr, socklen_t addrlen,
                           padico_socketfactory_acceptor_t notifier, void*key)
{
  struct sfsplice_instance_s*instance = _instance;
  int err = 0;
  assert(instance->bound);
  instance->acceptor.handler = notifier;
  instance->acceptor.key = key;
  padico_out(40, "err=%d\n", err);
  return -err;
}

static int sfsplice_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen,
                            padico_socketfactory_connector_t notifier, void*key)
{
  struct sfsplice_instance_s*instance = _instance;
  struct sockaddr_padico_splc*splcaddr =
    padico_sockaddr_payload(struct sockaddr_padico_splc, (struct sockaddr_pa*)addr);
  assert(instance->bound);
  instance->connector.handler = notifier;
  instance->connector.key = key;
  /* some address mangling */
  instance->peer_inaddr = (struct sockaddr_in)
    {
      .sin_family = AF_INET,
      .sin_port   = splcaddr->splc_port,
      .sin_addr   = { .s_addr = splcaddr->splc_addr }
    };
  padico_vaddr_t src_vaddr =  padico_vaddr_new((struct sockaddr*)&instance->my_inaddr,
                                                 sizeof(struct sockaddr_in),
                                                 IPPROTO_TCP);
  padico_string_t s_src_vaddr = padico_vaddr_marshall(src_vaddr);
  padico_topo_node_t dest_node = padico_topo_getnodebyname(splcaddr->splc_node);
  /* build & send splicing request */
  padico_string_t s = padico_string_new();
  padico_string_catf(s, "<SocketFactory_Splicing:request token=\"%d\">%s</SocketFactory_Splicing:request>\n",
                     (int)instance->key, padico_string_get(s_src_vaddr));
  padico_out(30, "sending request *%s*\n", padico_string_get(s));
  padico_control_send_oneway(dest_node, padico_string_get(s));
  /* cleanup */
  padico_string_delete(s);
  padico_string_delete(s_src_vaddr);
  /* do our half of splicing */
  int err = sfsplice_do_splicing(instance, &sfsplice_callback_connector);
  padico_out(40, "err=%d\n", err);
  return -err;
}


/* ********************************************************* */

static int sfsplice_do_splicing(struct sfsplice_instance_s*instance,
                                int(*finalizer)(int, void*))
{
  static const int retry_pause = 50;
  static const int burst_factor = 3;
  int retry = 1000;
  int burst = 0;
  int connected = 0;
  int rc = -1, err = -1;

  padico_print("sfsplice_do_splicing()- entering.\n");

  while(retry > 0 && !connected)
    {
      padico_print("sfsplice_do_splicing()- local inaddr %s:%d\n",
                   inet_ntoa(instance->my_inaddr.sin_addr),
                   ntohs(instance->my_inaddr.sin_port));
      padico_print("sfsplice_do_splicing()- connecting to %s:%d\n",
                   inet_ntoa(instance->peer_inaddr.sin_addr),
                   ntohs(instance->peer_inaddr.sin_port));
      rc  = PUK_ABI_FSYS_WRAP(connect)(instance->fd, (struct sockaddr*)&instance->peer_inaddr,
                                  sizeof(instance->peer_inaddr));
      err = __puk_abi_wrap__errno;
      padico_out(50, "::connect() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
      if(rc == 0)
        {
          connected = 1;
          err = EINPROGRESS;
          instance->io_connect = padico_io_register(instance->fd, PADICO_IO_EVENT_WRITE,
                                                    finalizer, instance);
          padico_io_activate(instance->io_connect);
          padico_out(60, "callback_connector() registered\n");
        }
      else
        {
          padico_warning("::connect()- err=%d (%s)\n", err, strerror(err));
          if(burst > 0)
            burst--;
          else
            {
              burst = burst_factor;
              padico_tm_msleep(retry_pause);
            }
          if(retry > 0)
            retry--;
        }
    }

  padico_print("sfsplice_do_splicing() err=%d.\n", err);

  return err;
}

/* ********************************************************* */

static void*sfsplice_request_process(void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  padico_print("processing request.\n");
  int err = sfsplice_do_splicing(instance, &sfsplice_callback_acceptor);
  /* TODO: if(err) send  error code to client. */
  if(err)
    {
      padico_warning("err=%d (%s)\n", err, strerror(err));
    }
  return NULL;
}

static void sfsplice_request_handler(puk_parse_entity_t e)
{
  const char*s_token = puk_parse_getattr(e, "token");
  const long token = atol(s_token);
  struct sfsplice_instance_s*instance = sfsplice_lookup(token);
  const char*s_vaddr = puk_parse_get_text(e);
  const padico_vaddr_t vaddr = padico_vaddr_unmarshall(s_vaddr);
  assert(instance->acceptor.handler != NULL);
  instance->peer_inaddr = *((struct sockaddr_in*)vaddr->addr);
  padico_tm_bgthread_start(sfsplc.pool, &sfsplice_request_process, instance, "sfsplice_request_process");
}


/* ********************************************************* */
/* ** SysIO callbacks for finalizing */

/* ** Acceptor */

static void*sfsplice_acceptor_finalizer(void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  (*instance->acceptor.handler)(instance->acceptor.key, instance->fd,
                                (struct sockaddr*)&instance->my_inaddr, sizeof(instance->my_inaddr));
  return NULL;
}
static int sfsplice_callback_acceptor(int fd, void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  const int again = 0;

  padico_print("connected.\n");

  padico_out(30, "fd=%d\n", fd);
  padico_tm_bgthread_start(sfsplc.pool, &sfsplice_acceptor_finalizer, instance,
                           "SocketFactory_Splicing:sfsplice_acceptor_finalizer");
  return again;
}


/* ** Connector */

static void*sfsplice_connector_finalizer(void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  (*instance->connector.handler)(instance->connector.key, instance->fd);
  return NULL;
}
static int sfsplice_callback_connector(int fd, void*_instance)
{
  struct sfsplice_instance_s*instance = _instance;
  const int again = 0;

  padico_print("connected.\n");

  padico_out(40, "fd=%d\n", fd);
  assert(fd == instance->fd);
  padico_tm_bgthread_start(sfsplc.pool, &sfsplice_connector_finalizer, instance,
                           "SocketFactory_Splicing:sfsplice_connector_finalizer");
  return again;
}



/* ********************************************************* */
