/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VLink debug tools
 */

#ifndef VLINK_DEBUG_H
#define VLINK_DEBUG_H

#ifdef VLINK_PROFILE
#include <Padico/Timing.h>
#endif


/* *** Debug and statistics ******************************** */

struct vlink_debug_s
{
  int show_vlink_assembly;
};
/** @brief auto-diagnosis information block for VIO.
 * See: <VIO:dumpstat/> */
struct vlink_statistics_s
{
  struct vlink_debug_s debug;
  int num_fd;             /* number of file descriptors */
  int open_sockets;       /* number of open sockets */
  int num_recv_by_copy;    /* number of copy because of reception before recv() posted */
  int num_recv_zero_copy;  /* number of reception without copy */
  int num_hfd_force_synchro;
  int num_immediate_write;
  int num_immediate_read;
  int num_write;
  int num_read;
  long long int bytes_recv_by_copy;
  long long int bytes_recv_zero_copy;
  int misaligned_receive; /* number of misaligned buffers for receipt */
  int misaligned_send;    /* number of misaligned buffers for sending */
  int no_rbox;
  int rbox_too_small;
  int rendezvous_zero_copy;
};
extern struct vlink_statistics_s vlink_stat; /* defined in vio-stats.c */


/* *** Profiling ******************************************* */

#ifdef VLINK_PROFILE
extern padico_timing_profiler_t vlink_rprofiler; /* AD:profile */
extern padico_timing_profiler_t vlink_wprofiler; /* AD:profile */
#  define vlink_profile_point(A, B) padico_timing_profile_point(A, B)
#  define vlink_profile_start(A)    padico_timing_profile_start(A)
#else
#  define vlink_profile_point(A, B)
#  define vlink_profile_start(A)
#endif

/* *** Labels for traces *********************************** */

static const char*vobs_attr_label[] __attribute__ ((unused)) =
{
  "status",
  "socket",
  "remote",
  "listener",
  "rbox",
  "wbox",
  "pivot"
};

/* *** Traces ********************************************** */

#ifdef PADICO_TRACE_ON
void vnode_show_vlink(padico_vnode_t vnode, int way, const char*text);
void vnode_show_assembly(puk_component_t assembly, int way, const char*text);
#else  /* PADICO_TRACE_ON */
static inline void vnode_show_assembly(puk_component_t assembly, int way, const char*text)
{ /* empty */ }
static inline void vnode_show_vlink(padico_vnode_t vnode, int way, const char*text)
{ /* empty */ }
#endif /* PADICO_TRACE_ON */

#ifdef NDEBUG
static inline void vlink_address_trace(const struct sockaddr*addr, socklen_t addrlen)
{
  /* empty */
}
#else
#define vlink_address_trace(addr,addrlen)                               \
  switch(addr->sa_family)                                               \
    {                                                                   \
    case AF_PADICO:                                                     \
      {                                                                 \
        __attribute__ ((unused)) const struct sockaddr_pa*pa = (struct sockaddr_pa*)addr; \
        padico_out(20, " family=AF_PADICO; magic=%u\n", pa->spa_magic); \
      }                                                                 \
      break;                                                            \
    case AF_INET:                                                       \
      {                                                                 \
        __attribute__ ((unused)) const struct sockaddr_in*in = (struct sockaddr_in*)addr; \
        padico_out(20, " family=AF_INET; port=%6d; addr=%s\n",          \
                     (int)(ntohs(in->sin_port)), inet_ntoa(in->sin_addr)); \
      }                                                                 \
      break;                                                            \
    case AF_UNIX:                                                       \
      {                                                                 \
        __attribute__ ((unused)) const struct sockaddr_un*un = (struct sockaddr_un*)addr; \
        padico_out(20, " family=AF_UNIX; path=%s\n",                    \
                     un->sun_path);                                     \
      }                                                                 \
      break;                                                            \
    case AF_INET6:                                                      \
      {                                                                 \
        __attribute__ ((unused)) const struct sockaddr_in6*in6 = (struct sockaddr_in6*)addr; \
        __attribute__ ((unused)) char addr6[INET6_ADDRSTRLEN];          \
        inet_ntop(AF_INET6, &in6->sin6_addr, addr6, INET6_ADDRSTRLEN);  \
        padico_out(20, " family=AF_INET6; port=%6d; addr=%s\n",         \
                     (int)(ntohs(in6->sin6_port)), addr6);              \
      }                                                                 \
      break;                                                            \
    default:                                                            \
      padico_warning(" family=%d [unknown]\n", addr->sa_family);        \
    }
#endif

/* *** Run-time consistency checks ************************* */

static inline void vnode_check_failed(padico_vnode_t vnode, const char*func, const char*file, int line)
{
  padico_print("#########################################\n");
  padico_print("# VIO: consistency check failed\n");
  padico_print("# function=%s\n", func);
  padico_print("# file=%s:%d\n", file, line);
  padico_print("# vnode=%p\n", vnode);
  padico_print("# hfd=%d socket=%d binding=%d remote=%d\n",
               vnode->content.hfd,
               vnode->content.socket,
               vnode->content.binding,
               vnode->content.remote);
  padico_print("# listener=%d rbox=%d wbox=%d\n",
               vnode->content.listener,
               vnode->content.rbox,
               vnode->content.wbox);
  padico_fatal("VIO consistency check failed.\n");
}

static inline int vnode_check_consistency(padico_vnode_t vnode, const char*func, const char*file, int line)
{
  if(vnode == NULL)
    padico_fatal("vnode is NULL.\n");
  if((vnode->content.listener && !vnode->content.socket)  ||
     (vnode->content.binding && !vnode->content.socket)   ||
     (vnode->content.remote && !vnode->content.socket)
     )
    {
      vnode_check_failed(vnode, func, file, line);
    }
  return 1;
}
#define vnode_check(VNODE, FIELD) \
  assert((((VNODE)->content.FIELD?:vnode_check_failed(VNODE,__FUNCTION__,__FILE__,__LINE__)), \
          vnode_check_consistency(VNODE,__FUNCTION__,__FILE__,__LINE__)))



#endif /* VLINK_DEBUG_H */
