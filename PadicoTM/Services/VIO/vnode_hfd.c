/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 ** @brief Low-level file descriptors driver for vnodes
 * @ingroup VIO
 */

#include "VLink-internals.h"
#include <Padico/Module.h>
#include <Padico/SysIO.h>

PADICO_MODULE_HOOK(VIO);

#define VLINK_SYSIO_DIRECT_SEND_THRESHOLD (4096)

static int vnode_hfd_callback_reader(int fd, void*key);
static int vnode_hfd_callback_writer(int fd, void*key);
static int vlink_sysio_reader_worker(padico_vnode_t vnode);
static int vlink_sysio_writer_worker(padico_vnode_t vnode);

/* ********************************************************* */

/** flags: O_RDONLY | O_WRONLY | O_RDWR
 */
void vnode_hfd_create(padico_vnode_t vnode, int fd, int flags)
{
  const int mode = flags & O_ACCMODE;
  padico_out(50, "vnode=%p hfd=%d\n", vnode, fd);
  vnode_activate(vnode, hfd);
  vnode->hfd.fd = fd;
  if((mode == O_RDONLY) || (mode == O_RDWR))
    {
      vnode->hfd.io_read  = padico_io_register(vnode->hfd.fd, PADICO_IO_EVENT_READ,
                                               &vnode_hfd_callback_reader, vnode);
      padico_io_activate(vnode->hfd.io_read);
    }
  else
    {
      vnode->hfd.io_read = NULL;
    }
  if((mode == O_WRONLY) || (mode == O_RDWR))
    {
      vnode->hfd.io_write = padico_io_register(vnode->hfd.fd, PADICO_IO_EVENT_WRITE,
                                               &vnode_hfd_callback_writer, vnode);
      padico_io_activate(vnode->hfd.io_write);
    }
  else
    {
      vnode->hfd.io_write = NULL;
    }
}

void vnode_hfd_read_post(padico_vnode_t vnode)
{
  int again = 1;
  vnode_check(vnode, hfd);
  padico_out(50, "vnode=%p; hfd=%d; entering- activating io...\n", vnode, vnode->hfd.fd);
  if(vnode->status.readable)
    {
      /* immediate read */
      again = vlink_sysio_reader_worker(vnode);
      vlink_stat.num_immediate_read++;
      if(again)
        {
          /* re-arm polling */
          vnode_unlock(vnode);
          padico_io_activate(vnode->hfd.io_read);
          vnode_lock(vnode);
        }
    }
  if(vnode->rbox.read_done < vnode->rbox.read_posted)
    {
      vnode_unlock(vnode);
      padico_io_activate(vnode->hfd.io_read);
      vnode_lock(vnode);
      padico_out(60, "io activated.\n");
      if(!(vnode->rbox.read_done > 0))
        {
          int read_done = vnode->rbox.read_done ;
          vlink_stat.num_hfd_force_synchro++;
          padico_out(60, "synchronizing.\n");
          vnode_unlock(vnode);
          if(padico_sysio_refresh(vnode->hfd.io_read))
            {
              vnode_lock(vnode);
              if(vnode->rbox.read_done == read_done)
                vobs_wait(vnode);
            }
          else
            vnode_lock(vnode);
          padico_out(60, "synchronized.\n");
        }
    }
  padico_out(60, "vnode=%p; hfd=%d; exit rlen=%ld\n", vnode, vnode->hfd.fd, puk_slong(vnode->rbox.read_done));
}

void vnode_hfd_write_post(padico_vnode_t vnode)
{
  int again = 1;
  vnode_check(vnode, hfd);
  if(vnode->status.writable ||
     vnode->wbox.write_posted <= VLINK_SYSIO_DIRECT_SEND_THRESHOLD)
    {
      /* try immediate write */
      again = vlink_sysio_writer_worker(vnode);
      vlink_stat.num_immediate_write++;
      if(again)
        {
          /* re-arm polling */
          vnode_unlock(vnode);
          padico_io_activate(vnode->hfd.io_write);
          vnode_lock(vnode);
        }
    }
  if(vnode->wbox.write_done < vnode->wbox.write_posted)
    {
      vnode_unlock(vnode);
      padico_io_activate(vnode->hfd.io_write);
      vnode_lock(vnode);
      if(!(vnode->wbox.write_done > 0))
        {
          vlink_stat.num_hfd_force_synchro++;
          vnode_unlock(vnode);
          padico_sysio_refresh(vnode->hfd.io_write);
          vnode_lock(vnode);
        }
    }
}

int vnode_hfd_close(padico_vnode_t vnode)
{
  int rc = -1;
  padico_out(40,"vnode=%p hfd=%d\n", vnode, vnode->hfd.fd);
  vnode->status.opened = 0;
  if(vnode->content.hfd)
    {
      const padico_io_t io_read = vnode->hfd.io_read;
      vnode->hfd.io_read = NULL;
      const padico_io_t io_write = vnode->hfd.io_write;
      vnode->hfd.io_write = NULL;
      const int fd = vnode->hfd.fd;
      vnode->hfd.fd = -1;
      vnode->content.hfd = 0;
      vnode_unlock(vnode);
      if(io_read)
        {
          padico_io_deactivate(io_read);
          padico_io_unregister(io_read);
        }
      if(io_write)
        {
          padico_io_deactivate(io_write);
          padico_io_unregister(io_write);
        }
      rc = PUK_ABI_FSYS_WRAP(close)(fd);
    }
  return rc;
}


int vnode_hfd_fcntl(padico_vnode_t vnode, int cmd, void*arg)
{
  padico_out(10, "hfd=%d; cmd=%d; arg=%p\n", vnode->hfd.fd, cmd, arg);
  int rc  = PUK_ABI_FSYS_WRAP(fcntl)(vnode->hfd.fd, cmd, arg);
  int err = __puk_abi_wrap__errno;
  padico_out(10, "hfd=%d; rc=%d; errno=%d\n", vnode->hfd.fd, rc, err);
  vnode_set_error(vnode, err);
  return rc;
}



/* ********************************************************* */

/** @brief The function which reads data.
 * @note May be called directly if status.readable is true;
 *       use the callback otherwise.
 * @pre vnode locked
 * @param vnode a vnode for which: status.readable=1 && rbox posted
 * @return 'again' flag, whether callback should be reactivated or not.
 */
static int vlink_sysio_reader_worker(padico_vnode_t vnode)
{
  int again = 0;
  assert(vnode->rbox.read_done >= 0);
  const size_t todo = vnode->rbox.read_posted - vnode->rbox.read_done;
  padico_out(50, "vnode=%p fd=%d read_done=%ld reading %ld bytes\n",
             vnode, vnode->hfd.fd, puk_slong(vnode->rbox.read_done),
             puk_ulong(todo));
  int rc = padico_sysio_read(vnode->hfd.fd,
                             vnode->rbox.read_buffer + vnode->rbox.read_done, todo);
  int err = padico_sysio_errno();
  /* update 'status.readable' & 'again' */
  if(rc > 0)
    {
      /* received ok */
      vnode->rbox.read_done += rc;
      padico_out(50, "::read() received %d bytes (read_done=%ld/%ld)\n",
                 rc, puk_slong(vnode->rbox.read_done), puk_slong(vnode->rbox.read_posted));
      vnode->status.readable = 0;
      vobs_signal(vnode, vobs_attr_rbox);
      /*
        if(vnode->rbox.read_done < vnode->rbox.read_posted)
        again = 1;
        else
        again = 0;
      */
      again = 1;
    }
  else if(rc == 0)
    {
      /* unexpected EOF detected */
      padico_out(40, "::read()- rc = 0- detected EOF\n");
      vnode->status.end_of_file = 1;
      if(vnode->content.remote)
        {
          vnode->remote.can_recv = 0;
        }
      vnode->status.readable = 0;
      vobs_signal(vnode, vobs_attr_status);
      again = 0;
    }
  else if(err == EINTR)
    {
      /* EINTR */
      vnode->status.readable = 1;
      again = 1;
    }
  else if(err == EAGAIN || err == EWOULDBLOCK)
    {
      /* EAGAIN */
      padico_warning("read()- hfd not ready while marked 'readable'\n");
      vnode->status.readable = 0;
      again = 1;
    }
  else
    {
      /* error */
      padico_warning("read()- hfd=%d rc=%d errno=%d (%s)\n",
                     vnode->hfd.fd, rc, err, strerror(err));
      vnode_set_error(vnode, err);
      vnode->status.readable = 0;
      again = 0;
    }
  padico_out(50, "vnode=%p fd=%d read_done=%ld exiting again=%d rc=%d\n",
             vnode, vnode->hfd.fd, puk_slong(vnode->rbox.read_done), again, rc);
  return again;
}

static int vlink_sysio_writer_worker(padico_vnode_t vnode)
{
  int again = 0;
  padico_out(40, "fd=%d writing %ld bytes (done=%ld; posted=%ld)\n",
             vnode->hfd.fd,
             puk_slong(vnode->wbox.write_posted - vnode->wbox.write_done),
             puk_slong(vnode->wbox.write_done), puk_slong(vnode->wbox.write_posted));
  int rc = padico_sysio_write(vnode->hfd.fd,
                              vnode->wbox.write_buffer + vnode->wbox.write_done,
                              vnode->wbox.write_posted - vnode->wbox.write_done);
  int err = padico_sysio_errno();
  if(rc > 0)
    {
      /* write ok */
      padico_out(50, "::write() written %d bytes (done=%ld; posted=%ld)\n",
                 rc, puk_slong(vnode->wbox.write_done), puk_slong(vnode->wbox.write_posted));
      vnode->status.writable = 0;
      vnode->wbox.write_done += rc;
      again = 1;
      vobs_signal(vnode, vobs_attr_wbox);
    }
  else if(rc == -1 && err == EINTR)
    {
      /* EINTR */
      again = 1;
      vnode->status.writable = 1;
    }
  else if(rc == -1 && err == EAGAIN)
    {
      /* EAGAIN */
      padico_warning("write()- hfd not ready while marked 'writable'\n");
      again = 1;
      vnode->status.writable = 0;
    }
  else if(rc == -1 && (err == EPIPE || err == ECONNRESET || err == EIO))
    {
      /* fatal errors: EPIPE, ECONNRESET, EIO */
      again = 0;
      vnode->remote.can_send = 0;
      vnode_set_error(vnode, err);
    }
  else
    {
      /* error */
      padico_warning("write() unexpected error- rc=%d errno=%d (%s)\n",
                     rc, err, strerror(err));
      again = 0;
      vnode_set_error(vnode, err);
    }
  return again;
}

static int vnode_hfd_callback_reader(int fd, void*key)
{
  int again = 0;
  padico_vnode_t vnode = (padico_vnode_t)key;
  padico_out(20, "vnode=%p fd=%d entering\n", vnode, fd);
  vnode_lock(vnode);
  if(!vnode->content.hfd)
    {
      /* hfd is being closed */
      again = 0;
    }
  else
    {
      vnode_check(vnode, hfd);
      padico_out(40, "vnode=%p locked- hfd=%d (%d)\n", vnode, fd, vnode->hfd.fd);
      assert(fd == vnode->hfd.fd);
      if(vnode->content.rbox && (vnode->rbox.read_done < vnode->rbox.read_posted))
        {
          again = vlink_sysio_reader_worker(vnode);
        }
      else
        {
          padico_out(20, "no rbox -- fd=%d set status.readable and signaling observer\n", fd);
          vnode->status.readable = 1;
          vobs_signal(vnode, vobs_attr_status);
          again = 0; /* XXX */
        }
      /* check whether we are in a permitted state */
      assert(again ||  /* operation in progress: callback re-posted */
             vnode->status.end_of_file || /* we reached EOF */
             vnode_get_error(vnode) != 0 || /* there is an error with the vnode */
             (!vnode->content.rbox && vnode->status.readable ) || /* some data is buffered */
             (vnode->content.rbox && (vnode->rbox.read_done == vnode->rbox.read_posted)) /* receive completed */
             );
    }
  vnode_unlock(vnode);
  return again;
}

static int vnode_hfd_callback_writer(int fd, void*key)
{
  int again = 0;
  padico_vnode_t vnode = (padico_vnode_t)key;
  padico_out(20, "vnode=%p fd=%d entering\n", vnode, fd);
  vnode_lock(vnode);
  if(!vnode->content.hfd)
    {
      /* hfd is being closed */
      again = 0;
    }
  else
    {
      vnode_check(vnode, hfd);
      if(vnode->content.wbox && (vnode->wbox.write_done < vnode->wbox.write_posted))
        {
          again = vlink_sysio_writer_worker(vnode);
        }
      else
        {
          padico_out(20, "no wbox -- fd=%d set status.writable and signaling observer\n", fd);
          vnode->status.writable = 1;
          vobs_signal(vnode, vobs_attr_status);
          again = 0;
        }
      /* check whether we are in a permitted state */
      assert(again || /* operation in progress: re-post callback */
             vnode->status.writable || /* ready to write */
             !vnode->remote.can_send || /* link is shutdown */
             vnode_get_error(vnode) != 0 || /* there is an error with the vnode */
             (vnode->content.wbox && (vnode->wbox.write_done == vnode->wbox.write_posted)) /* write completed */
             );
    }
  vnode_unlock(vnode);
  padico_out(20, "exit -- fd=%d; again=%d\n", fd, again);
  return again;
}
