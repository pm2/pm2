/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Socket-specific part for vnodes.
 * @ingroup VIO
 */


#include "VLink-internals.h"
#include <Padico/Topology.h>
#include <Padico/Module.h>

#include <netinet/in.h>
#include <arpa/inet.h>

PADICO_MODULE_HOOK(VIO);


/* ********************************************************* */


void padico_vsock_init(padico_vnode_t vnode, int domain, int type, int protocol)
{
  vnode_activate(vnode, socket);
  vlink_stat.open_sockets++;
  vnode->socket.capabilities = 0;
  vnode->socket.so_options   = 0;
  vnode->socket.so_family    = domain;
  vnode->socket.so_type      = type;
  vnode->socket.so_protocol  = protocol;
}

/** Turns a vnode into a socket.
 * @return -errno (0 if success)
 */
int padico_vsock_create(padico_vnode_t vnode, int domain, int type, int protocol)
{
  int rc = -ENOSYS;
  const struct padico_vlink_driver_s* driver = padico_vnode_get_vlink_driver(vnode);
  padico_vsock_init(vnode, domain, type, protocol);
  if(driver && driver->create)
    {
      padico_out(40, "vnode=%p create() component=*%s*\n",
                 vnode, padico_vnode_component_name(vnode));
      rc = (*(driver->create))(vnode);
    }
  else
    {
      padico_out(40, "vnode=%p component=*%s* no driver for create!\n",
                 vnode, padico_vnode_component_name(vnode));
    }
  return rc;
}

void padico_vsock_fullshutdown(padico_vnode_t vnode)
{
  padico_out(40, "vnode=%p\n", vnode);
  if(vnode->content.remote)
    {
      vnode->remote.can_send = 0;
      vnode->remote.can_recv = 0;
    }
  const struct padico_vlink_driver_s*driver = padico_vnode_get_vlink_driver(vnode);
  if(driver && driver->fullshutdown)
    {
      padico_out(10, "vnode=%p shutdown() component=*%s*\n", vnode,
                 padico_vnode_component_name(vnode));
      (*(driver->fullshutdown))(vnode);
    }
  if(vnode->content.remote)
    {
      vnode->remote.established = 0;
      vnode->content.remote = 0;
    }
  vnode->status.end_of_file = 1;
  padico_out(40, "vnode=%p; done.\n", vnode);
}

/* @pre vnode locked
 */
int padico_vsock_autobind(padico_vnode_t vnode)
{
  int rc = padico_vsock_bind(vnode, NULL, 0);
  return rc;
}

/* @pre vnode locked
 */
int padico_vsock_bind(padico_vnode_t vnode, const struct sockaddr*addr, socklen_t addrlen)
{
  int rc = 0;
  vnode_check(vnode, socket);
  vnode_activate(vnode, binding);
  if(addr)
    {
      /* copy primary address */
      vnode->binding.primary = padico_malloc(addrlen);
      memcpy(vnode->binding.primary, addr, addrlen);
      vnode->binding.primary_len = addrlen;
      vnode->binding.autobind = 0;
      /* check the addressing space */
      vlink_address_trace(vnode->binding.primary,
                          vnode->binding.primary_len);
    }
  else
    {
      vnode->binding.primary     = NULL;
      vnode->binding.primary_len = 0;
      vnode->binding.autobind    = 1;
    }
  const struct padico_vlink_driver_s *driver =
    padico_vnode_get_vlink_driver(vnode);
  if(driver && driver->bind)
    {
      padico_out(40, "vnode=%p bind() component=*%s*\n",
                 vnode, padico_vnode_component_name(vnode));
      rc = (*(driver->bind))(vnode);
      padico_out(40, "vnode=%p rc=%d\n", vnode, rc);
    }
  else
    {
      padico_out(40, "vnode=%p component=*%s* no driver for bind!\n",
                 vnode, padico_vnode_component_name(vnode));
    }
  return rc;
}


/* *** Connection establishment **************************** */

/**
 * @pre vnode locked
 */
int padico_vsock_passive_connect(padico_vnode_t vnode, int backlog_size,
                                 const struct padico_vaddr_s*src_addr)
{
  int rc = 0;
  if(vnode->content.socket)
    {
      if(vnode->content.binding)
        {
          padico_out(40, "vnode=%p binding: \n", vnode);
          vlink_address_trace(vnode->binding.primary, vnode->binding.primary_len);
        }
      else
        {
          padico_warning("vnode=%p; listen() on unbound socket!\n", vnode);
          padico_vsock_autobind(vnode);
        }
      vnode_activate(vnode, listener);
      vnode->listener.backlog_size = backlog_size;
      vnode->listener.backlog      = padico_vnode_queue_new(backlog_size);
      vnode->listener.src_addr     = src_addr;
      vnode_clear_error(vnode);
      {
        const struct padico_vlink_driver_s *driver =
          padico_vnode_get_vlink_driver(vnode);
        if(driver && driver->listen)
          {
            padico_out(40, "vnode=%p listen() component=*%s*\n",
                       vnode, padico_vnode_component_name(vnode));
            rc = (*(driver->listen))(vnode);
          }
        else
          {
            padico_out(40, "vnode=%p component=*%s* no driver for listen!\n",
                       vnode, padico_vnode_component_name(vnode));
          }
      }
    }
  else
    {
      rc = -1;
      padico_warning("padico_vnode_listen() vnode is not a socket.\n");
      vnode_set_error(vnode, ENOTSOCK);
    }
  return rc;
}

/** @return 0 if success (connected)
 * @return -1 if error (maybe in progress)
 */
int padico_vsock_active_connect(padico_vnode_t vnode, const struct sockaddr*name, socklen_t namelen)
{
  const struct padico_vlink_driver_s*driver = padico_vnode_get_vlink_driver(vnode);
  int rc = -1;
  vnode_check(vnode, socket);
  vnode_activate(vnode, remote);
  vnode_activate(vnode, connect);
  vnode->status.connecting  = 1;
  vnode->remote.established = 0;
  vnode->remote.can_send    = 0;
  vnode->remote.can_recv    = 0;
  vnode->remote.remote_addrlen = namelen;
  vnode->remote.remote_addr = padico_malloc(namelen);
  memcpy(vnode->remote.remote_addr, name, namelen);
  padico_out(40, "vnode=%p \n", vnode);
  vlink_address_trace(vnode->remote.remote_addr, vnode->remote.remote_addrlen);
  if(driver && driver->connect)
    {
      padico_out(40, "vnode=%p connect() component=*%s*\n",
                 vnode, padico_vnode_component_name(vnode));
      rc = (*driver->connect)(vnode);
      const int err = vnode_get_error(vnode);
      padico_out(50, "vnode=%p connect()- rc=%d; err=%d (%s)\n", vnode, rc, err, strerror(err));
      if(rc && !vlink_is_transient_error(err))
        {
          vnode_deactivate(vnode, remote);
        }
    }
  else
    {
      padico_out(40, "vnode=%p component=*%s* no driver for connect!\n",
                 vnode, padico_vnode_component_name(vnode));
    }
  return rc;
}


/* *** VSock toolbox *************************************** */

/** Adapters call this function to notify the VLink_selector that an
 * outgoing connection has been accepted. The selector then promotes
 * the currently tried component to connected component in charge of the
 * 'remote' field of the vnode, and shutdowns every other components.
 * @param vnode the successfully connected vnode
 */
void padico_vsock_connector_finalize(padico_vnode_t vnode)
{
  padico_out(50, "vnode=%p\n", vnode);
  vnode_check(vnode, remote);
  vnode_set_error(vnode, 0);
  vnode->status.connecting  = 0;
  vnode->remote.established = 1;
  vnode->remote.can_send    = 1;
  vnode->remote.can_recv    = 1;
  vobs_signal(vnode, vobs_attr_remote);
  padico_out(50, "vnode=%p; done\n", vnode);
}

/** Adapters call this function to notify that an incoming connection
 * has been accepted. The selector promotes the 'selected' component to
 * connected component in charge of the 'remote' field, and shutdowns
 * every other components not selected.
 * @param vnode    the newly created socket
 * @param selected the component which created this socket (thus promoted to connected component)
 * @param addr     address of the remote peer
 * @param addrlen  length in bytes of 'addr'
 */
void padico_vsock_acceptor_finalize(padico_vnode_t vnode, padico_vnode_t server_vnode,
                                    const struct sockaddr*addr, int addrlen)
{
  padico_out(40, "vnode=%p\n", vnode);
  vnode_lock(vnode);
  vnode_activate(vnode, remote);
  vnode_set_error(vnode, 0);
  vnode->status.connecting     = 0;
  vnode->remote.established    = 1;
  vnode->remote.can_recv       = 1;
  vnode->remote.can_send       = 1;
  vnode->remote.remote_addr    = padico_malloc(addrlen);
  vnode->remote.remote_addrlen = addrlen;
  memcpy(vnode->remote.remote_addr, addr, addrlen);
  padico_vnode_queue_append(server_vnode->listener.backlog, vnode);
  vnode_unlock(vnode);
  padico_out(40, "vnode=%p signaling\n", server_vnode);
  vobs_signal(server_vnode, vobs_attr_listener);
}


/* *** Packet connection-less communications *************** */

ssize_t padico_vsock_sendto(padico_vnode_t vnode, const void*buf, size_t len, int flags,
                            const struct sockaddr *to, int  tolen)
{
  ssize_t rc = 0;
  if(vnode->content.wbox)
    {
      padico_warning("padico_vsock_sendto_post()- sendto already posted\n");
      vnode_set_error(vnode, EIO);
      rc = -1;
    }
  else
    {
      const padico_topo_host_t remote_host = padico_topo_host_getbyinaddr(&((struct sockaddr_in*)to)->sin_addr);
      const padico_topo_node_t remote_node = remote_host ? padico_topo_host_getnode(remote_host) : NULL;
      if(remote_node)
        {
          /* sendto through MadIO */
          padico_out(40, "calling vsock_madio_sendto()- vnode=%p\n", vnode);
          padico_warning("madio_sendto() called- deprecated.\n");
#if 0
          vsock_madio_sendto(vnode, buf, len, remote_node, ntohs(((struct sockaddr_in*)to)->sin_port));
#endif
        }
      else
        {
          /* sendto through SysIO */
          int err = 0;
          padico_warning("sendto()- vnode=%p; len=%lu\n", vnode, (unsigned long)len);
          rc  = PUK_ABI_FSYS_WRAP(sendto)(vnode->hfd.fd, buf, len, flags, to, tolen);
          err = __puk_abi_wrap__errno;
          padico_warning("sendto()- vnode%p; rc=%ld\n", vnode, (long)rc);
          vnode_set_error(vnode, err);
        }
    }
  return rc;
}

ssize_t padico_vsock_recvfrom(padico_vnode_t vnode, void *buf, size_t len, int flags,
                              struct sockaddr *from, int *fromlen)
{
  ssize_t rc = 0;
  if(vnode->content.rbox)
    {
      padico_warning("padico_vsock_recvfrom()- recvfrom already posted vnode=%p\n", vnode);
      vnode_set_error(vnode, EIO);
      rc = -1;
    }
  else
    {
      padico_out(40, "vnode=%p\n", vnode);
      vnode_activate(vnode, rbox);
      vnode->rbox.read_posted    = len;
      vnode->rbox.read_done      = 0;
      vnode->rbox.read_buffer    = buf;
      vnode->rbox.from           = from;
      vnode->rbox.fromlen        = fromlen;
      vnode->rbox.cant_interrupt_read = 0;

      padico_warning("madio_recvfrom() called- deprecated.\n");
#if 0
      vsock_madio_recvfrom(vnode, buf, len); /* XXX */
#endif
      while(vnode->rbox.read_done == 0)
        {
          padico_out(50, "vnode=%p waiting...\n", vnode);
          vobs_wait(vnode);
        }
      rc = vnode->rbox.read_done;
      vnode->rbox.read_posted = -1;
      vnode->rbox.read_buffer = NULL;
      vnode->rbox.read_done   = 0;
      vnode->content.rbox = 0;
      padico_out(40, "vnode=%p ok- rc=%ld\n", vnode, puk_slong(rc));
    }

  return rc;
}
