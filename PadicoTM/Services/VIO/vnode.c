/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Vnode multiplexer -- implements padico_vnode_* (external API)
 * @ingroup VIO
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include "VLink-internals.h"

static int vio_mod_init(void);
static void vio_mod_finalize(void);

PADICO_MODULE_DECLARE(VIO, vio_mod_init, NULL, vio_mod_finalize,
                      "Topology", "SysIO");

struct vnode_instance_lock_s
{
  marcel_mutex_t mutex;
  int refcount;
};
static struct
{
  puk_hashtable_t locks; /**< hash: instance (ptr) -> vnode_instance_lock */
  marcel_mutex_t lock;
} vnode_db;

static void vnode_instance_lock_destructor(void*_key, void*_value);

/* ********************************************************* */

static int vio_mod_init(void)
{
  padico_out(20, "sizeof(struct padico_vnode_s)=%ld\n",
             puk_ulong(sizeof(struct padico_vnode_s)));

  vnode_db.locks = puk_hashtable_new_ptr();
  marcel_mutex_init(&vnode_db.lock, NULL);
  puk_iface_register("VLink");
  puk_iface_register("VPipe");
  puk_iface_register("VFile");
  puk_iface_register("VFile64");
  puk_iface_register("VLinkMessaging");

  padico_vio_init();
  padico_viostat_init();
  padico_vio_abi_init();
  return 0;
}

static void vio_mod_finalize(void)
{
  puk_hashtable_delete(vnode_db.locks, &vnode_instance_lock_destructor);
  marcel_mutex_destroy(&vnode_db.lock);
}

static void vnode_instance_lock_destructor(void*_key, void*_value)
{
  struct vnode_instance_lock_s*lock = _value;
      marcel_mutex_destroy(&lock->mutex);
      padico_free(lock);
}

/* ********************************************************* */
/* *** 'PadicoComponent' driver for vnode ******************** */

static void*vnode_instantiate(puk_instance_t assembly, puk_context_t context);
static void vnode_destroy(void*instance);

const struct puk_component_driver_s padico_vnode_component_driver =
  {
    .instantiate = &vnode_instantiate,
    .destroy     = &vnode_destroy
  };

static void*vnode_instantiate(puk_instance_t instance, puk_context_t context)
{
  const puk_component_t component = context?context->component:instance->component;

  padico_vnode_t vnode = padico_malloc(sizeof(struct padico_vnode_s));
  padico_out(40, "vnode=%p; assembly=%s\n", vnode, instance->component->name);
  memset(&vnode->content, 0, sizeof(vnode->content));
  vnode->cookie = NULL;
  vnode->status.error       = 0;
  vnode->status.readable    = 0;
  vnode->status.writable    = 0;
  vnode->status.opened      = 0;
  vnode->status.connecting  = 0;
  vnode->status.end_of_file = 0;
  vnode->status.pending_op  = 0;
  marcel_cond_init(&vnode->observer.wait_event, NULL);
  vnode->observer.notifier.func = NULL;
  vnode->observer.commit        = NULL;
  vobs_secondary_ref_vect_init(&vnode->observer.secondary);
  vnode->assembly_stuff.instance     = instance;
  vnode->assembly_stuff.context      = context;
  vnode->assembly_stuff.specific     = NULL;
  vnode->assembly_stuff.vlink_driver = puk_component_get_driver_VLink(component, NULL);
  marcel_mutex_lock(&vnode_db.lock);
  struct vnode_instance_lock_s*lock = puk_hashtable_lookup(vnode_db.locks, instance);
  if(lock)
    {
      padico_out(40, "vnode=%p; add lock=%p refcount; instance=%p\n", vnode, lock, instance);
      lock->refcount++;
    }
  else
    {
      lock = padico_malloc(sizeof(struct vnode_instance_lock_s));
      marcel_mutex_init(&lock->mutex, NULL);
      lock->refcount = 1;
      puk_hashtable_insert(vnode_db.locks, instance, lock);
      padico_out(40, "vnode=%p; create lock=%p; instance=%p\n", vnode, lock, instance);
    }
  vnode->lock_ptr = &lock->mutex;
#ifdef DEBUG
  vnode->lock_count = 0;
  vnode->lock_owner = (marcel_t)NULL;
#endif
  marcel_mutex_unlock(&vnode_db.lock);
  return vnode;
}

static void vnode_destroy(void*_status)
{
  padico_vnode_t vnode = _status;
  puk_instance_t instance = vnode->assembly_stuff.instance;
  vobs_secondary_ref_vect_init(&vnode->observer.secondary);
  padico_out(50, "destroy vnode=%p; instance=%p\n", vnode, instance);
  marcel_mutex_lock(&vnode_db.lock);
  struct vnode_instance_lock_s*lock = puk_hashtable_lookup(vnode_db.locks, instance);
  assert(lock != NULL);
  lock->refcount--;
  if(lock->refcount == 0)
    {
      puk_hashtable_remove(vnode_db.locks, instance);
      marcel_mutex_destroy(&lock->mutex);
      padico_free(lock);
    }
  marcel_mutex_unlock(&vnode_db.lock);
  padico_free(vnode);
  padico_out(50, "vnode=%p; instance=%p; done.\n", vnode, instance);
}


/* *** padico_vnode_*: vnode multiplexers ******************** */

ssize_t padico_vnode_read_post(padico_vnode_t vnode, void*buf, size_t count)
{
  ssize_t rc = 0;
  assert(count > 0);
  padico_out(50, "vnode=%p buf=%p size=%ld entering\n", vnode, buf, puk_ulong(count));
  vlink_stat.num_read++;
  if(vnode->content.rbox)
    {
      padico_warning("padico_vnode_read_post() read already posted vnode=%p\n", vnode);
      vnode_set_error(vnode, EALREADY);
      rc = -1;
    }
  else
    {
      if(count > 0)
        {
          assert(buf != NULL);
          vnode_activate(vnode, rbox);
          vnode_clear_error(vnode);
          vnode->rbox.read_posted    = count;
          vnode->rbox.read_done      = 0;
          vnode->rbox.read_buffer    = buf;
          vnode->rbox.cant_interrupt_read = 0;
          vnode->rbox.tid = marcel_self();
          {
            const struct padico_vlink_driver_s*driver =
              padico_vnode_get_vlink_driver(vnode);
            if(driver)
              {
                if(driver->recv_post)
                  {
                    assert(vnode->rbox.read_buffer != NULL);
                    (*(driver->recv_post))(vnode);
                    rc = vnode->rbox.read_done;
                  }
                else
                  {
                    padico_fatal("no driver for recv_post()\n");
                    vnode_set_error(vnode, EPROTONOSUPPORT);
                    rc = -1;
                  }
              }
            else
              {
                padico_warning("no component for recv_post()\n");
                vnode_set_error(vnode, EPROTONOSUPPORT);
                rc = -1;
              }
          }
        }
      else
        {
          padico_warning("posted read() with size=0\n");
        }
    }
  padico_out(50, "vnode=%p buf=%p size=%ld exiting rc=%ld\n",
             vnode, buf, puk_ulong(count), puk_slong(rc));
  return rc;
}


void padico_vnode_read_clear(padico_vnode_t vnode)
{
  vnode_check(vnode, rbox);
  vnode->rbox.read_posted = -1;
  vnode->rbox.read_buffer = NULL;
  vnode->rbox.read_done   = 0;
  vnode->content.rbox = 0;
  vobs_commit(vnode, vobs_attr_rbox);
}


ssize_t padico_vnode_write_post(padico_vnode_t vnode, const void*buf, size_t count)
{
  ssize_t rc = 0;
  padico_out(50, "vnode=%p buf=%p size=%ld entering\n", vnode, buf, puk_ulong(count));
  vlink_stat.num_write++;
  if(vnode->content.wbox)
    {
      padico_warning("padico_vnode_write_post() write already posted\n");
      vnode_set_error(vnode, EALREADY);
      rc = -1;
      goto error;
    }
  if(vnode->content.remote && !vnode->remote.established)
    {
      vnode_set_error(vnode, EBADF);
      rc = -1;
      goto error;
    }
  const struct padico_vlink_driver_s*driver = padico_vnode_get_vlink_driver(vnode);
  if(!driver || !driver->send_post)
    {
      padico_warning("no driver for send_post()\n");
      vnode_set_error(vnode, EPROTONOSUPPORT);
      rc = -1;
      goto error;
    }
  vnode_activate(vnode, wbox);
  vnode_clear_error(vnode);
  vnode->wbox.write_posted = count;
  vnode->wbox.write_done   = 0;
  vnode->wbox.write_buffer = (void*)buf; /* cast from (const void*) into (void*), but believe me, most of the time write_buffer is const, really :-) */
  vnode->wbox.is_owner     = 0;
  (*(driver->send_post))(vnode);
  rc = vnode->wbox.write_done;
 error:
  padico_out(50, "vnode=%p buf=%p size=%ld exiting rc=%ld\n",
             vnode, buf, puk_ulong(count), puk_slong(rc));
  return rc;
}

void padico_vnode_write_clear(padico_vnode_t vnode)
{
  vnode_check(vnode, wbox);
  if(vnode->wbox.is_owner)
    {
      padico_free(vnode->wbox.write_buffer);
    }
  vnode->wbox.write_buffer = NULL;
  vnode->wbox.write_posted = -1;
  vnode->content.wbox = 0;
  vobs_commit(vnode, vobs_attr_wbox);
}

int padico_vnode_close(padico_vnode_t vnode)
{
  int rc = 0;
  padico_out(50, "vnode=%p; socket=%d; listener=%d; remote=%d\n",
             vnode, vnode->content.socket, vnode->content.listener, vnode->content.remote);
  if(vnode->content.socket && vnode->content.listener)
    {
      padico_vnode_queue_delete(vnode->listener.backlog);
      vnode->listener.backlog_size = -1;
    }
  if(vnode->content.remote || vnode->content.socket)
    {
      padico_vsock_fullshutdown(vnode);
    }
  padico_out(50, "vnode=%p; invoking driver->close\n", vnode);
  const struct padico_vlink_driver_s*driver = padico_vnode_get_vlink_driver(vnode);
  if(driver && driver->close)
    {
      rc = (*(driver->close))(vnode);
    }

  /* TODO: should be 'selector' */
  if(vnode->content.hfd)
    {
      padico_out(50, "vnode=%p; closing hfd\n", vnode);
      vnode_hfd_close(vnode);
    }
  padico_out(50, "vnode=%p; done.\n", vnode);
  return rc;
}
