/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Observers for vnode -- manage lock, synchro, grouping.
 * @ingroup VIO
 */

#include <Padico/Module.h>
#include "VLink-API.h"
#include "VLink-internals.h"

PADICO_MODULE_HOOK(VIO);

/* *** helper functions */

static inline int vnode_read_poll(padico_vnode_t vnode)
{
  int ready = 0;
  if(vnode->content.listener)
    {
      ready |= !padico_vnode_queue_empty(vnode->listener.backlog);
    }
  else
    {
      ready |= vnode->status.readable;
    }
  return ready;
}

static inline int vnode_write_poll(padico_vnode_t vnode)
{
  int ready = vnode->status.writable &&
    (vnode->status.opened || (vnode->content.remote && vnode->remote.established));
  if(vnode->content.wbox)
    {
      padico_warning("write poll() while write op already posted!\n");
      return 0;
    }
  return ready;
}

static inline int vnode_error_poll(padico_vnode_t vnode)
{
  padico_warning("vnode_error_poll() not supported yet.\n");
  return 0;
}


/* *** vobs secondary ************************************** */

void vobs_secondary_wait(vobs_secondary_t obs)
{
  padico_out(60, "obs=%p waiting on secondary observer...\n", obs);
  marcel_cond_wait(&obs->waiting, &obs->lock);
  padico_out(60, "obs=%p secondary observer unlocked.\n", obs);
}

int vobs_secondary_timedwait(vobs_secondary_t obs, const struct timespec*timeout)
{
  int rc = -1;
  padico_out(60, "obs=%p timed waiting on secondary observer...\n", obs);
  rc = marcel_cond_timedwait(&obs->waiting, &obs->lock, timeout);
  padico_out(60, "obs=%p secondary observer unlocked (rc=%d).\n", obs, rc);
  return rc;
}

void vobs_secondary_init(vobs_secondary_t obs, int size)
{
  assert(obs != NULL);
  marcel_mutex_init(&obs->lock, NULL);
  marcel_cond_init(&obs->waiting, NULL);
  obs->observers = padico_malloc(size*sizeof(struct padico_vnode_observer_s));
  obs->n_obs = 0;
}

void vobs_secondary_destroy(vobs_secondary_t obs)
{
  int i;
  for(i = 0; i < obs->n_obs; i++)
    {
      padico_vnode_t vnode = obs->observers[i].vnode;
      if(!vnode)
        continue;
      vnode_lock(vnode);
      vobs_secondary_ref_vect_itor_t s;
      puk_vect_foreach(s, vobs_secondary_ref, &vnode->observer.secondary)
        {
          if(s->obs == obs)
            {
              vobs_secondary_ref_vect_erase(&vnode->observer.secondary, s);
              break;
            }
        }
      vnode_unlock(vnode);
    }
  vobs_secondary_lock(obs);
  padico_free(obs->observers);
  vobs_secondary_unlock(obs);
}

int vobs_secondary_update(vobs_secondary_t obs)
{
  int n_ready = 0;
  int i;
  padico_out(60, "entering- observer=%p n_obs=%d\n", obs, obs->n_obs);
  for(i = 0; i < obs->n_obs; i++)
    {
      int ready = 0;
      padico_vnode_t vnode = obs->observers[i].vnode;
      /* Update the status of 'vnode' in the observer */
      obs->observers[i].revent = 0;
      if(!vnode)
        {
          obs->observers[i].revent |= POLLNVAL;
          n_ready++;
          continue;
        }
      if((obs->observers[i].event & VNODE_EVENT_READ) &&
         vnode_read_poll(vnode))
        {
          obs->observers[i].revent |= VNODE_EVENT_READ;
          ready = 1;
        }
      if((obs->observers[i].event & VNODE_EVENT_WRITE) &&
         vnode_write_poll(vnode))
        {
          obs->observers[i].revent |= VNODE_EVENT_WRITE;
          ready = 1;
        }
      if((obs->observers[i].event & VNODE_EVENT_ERROR) &&
         vnode_is_fatal_error(vnode))
        {
          obs->observers[i].revent |= VNODE_EVENT_ERROR;
          ready = 1;
        }
      n_ready += ready;
      padico_out(70, "i=%d; vnode=%p; revent=%d\n", i, vnode, obs->observers[i].revent);
    }
  padico_out(60, "exit- observer=%p; n_ready=%d\n", obs, n_ready);
  return n_ready;
}
