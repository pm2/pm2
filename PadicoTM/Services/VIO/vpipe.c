/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Pipe-specific part for vnodes.
 * @ingroup VIO
 */

#include "VLink-internals.h"
#include <Padico/Module.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>

static int padico_vpipe_init(void);
static void padico_vpipe_finalize(void);

PADICO_MODULE_DECLARE(VPipe_Shm, padico_vpipe_init, NULL, padico_vpipe_finalize, "VIO");

static void vpipe_write(padico_vnode_t vnode);
static void vpipe_read (padico_vnode_t vnode);
static int  vpipe_close(padico_vnode_t vnode);
static int  vpipe_pipe(padico_vnode_t vnodes[2]);

/** @defgroup VPipe component: VPipe- component for virtual pipes in VIO
 * @ingroup PadicoComponent
 */

/** The VPipe static data block
 * @ingroup VPipe
 */
static struct
{
  puk_component_t component;
} vpipe = { .component = NULL };

/** driver for the 'VLink' facet of VPipe
 * @ingroup VPipe
 */
static const struct padico_vlink_driver_s vpipe_vlink_driver =
  {
    .create       = NULL,
    .bind         = NULL,
    .listen       = NULL,
    .connect      = NULL,
    .fullshutdown = NULL,
    .close        = &vpipe_close,
    .send_post    = &vpipe_write,
    .recv_post    = &vpipe_read,
    .dumpstat     = NULL
  };
/** driver for the 'VPipe' facet of VPipe
 * @ingroup VPipe
 */
static const struct padico_vpipe_driver_s vpipe_driver =
  {
    .pipe = &vpipe_pipe
  };

PUK_LIST_TYPE(vpipe_fragment,
              char*ptr;
              size_t size;
              );
/** status for a VPipe instance.
 * @ingroup VPipe
 */
struct vlink_vpipe_specific_s
{
  padico_vnode_t other_vnode; /**< pointer to the other end of the pipe */
  vpipe_fragment_list_t rbuf; /**< buffer of incoming fragments */
};

VLINK_CREATE_SPECIFIC_ACCESSORS(vpipe);

static int padico_vpipe_init(void)
{
  vpipe.component =
    puk_component_declare("VPipe_Shm",
                          puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                          puk_component_provides("VPipe", "vpipe", &vpipe_driver),
                          puk_component_provides("VLink", "vlink", &vpipe_vlink_driver));
  return 0;
}

static void padico_vpipe_finalize(void)
{
  puk_component_destroy(vpipe.component);
}

/* ********************************************************* */

static int vpipe_pipe(padico_vnode_t vnodes[2])
{
  int rc = 0;
  padico_out(20, "create vpipe\n");
  puk_instance_t instance1 = puk_component_instantiate(vpipe.component);
  puk_instance_t instance2 = puk_component_instantiate(vpipe.component);
  struct puk_receptacle_VLink_s r[2];
  puk_instance_indirect_VLink(instance1, NULL, &r[0]);
  puk_instance_indirect_VLink(instance2, NULL, &r[1]);
  vnodes[0] = r[0]._status;
  vnodes[1] = r[1]._status;
  vobs_vpipe_activate(vnodes[0]);
  vobs_vpipe_activate(vnodes[1]);
  vpipe_specific(vnodes[0])->other_vnode = vnodes[1];
  vpipe_specific(vnodes[1])->other_vnode = vnodes[0];
  vpipe_specific(vnodes[0])->rbuf = vpipe_fragment_list_new();
  vpipe_specific(vnodes[1])->rbuf = vpipe_fragment_list_new();
  vnodes[0]->status.opened = 1;
  vnodes[1]->status.opened = 1;
  vnodes[0]->status.writable = 1;
  vnodes[1]->status.writable = 1;
  vnodes[0]->status.readable = 0;
  vnodes[1]->status.readable = 0;
  vnode_set_error(vnodes[0], 0);
  vnode_set_error(vnodes[1], 0);
  return rc;
}

/** @pre vnode locked
 */
static void vpipe_write(padico_vnode_t vnode)
{
  padico_vnode_t ov = vpipe_specific(vnode)->other_vnode;
  int avl_size = 0;
  padico_out(20, "vnode=%p entering\n", vnode);
  if(!ov)
    {
      padico_warning("vpipe_write(): vnode=%p no peer!\n", vnode);
      vnode->wbox.write_done = -1;
      vnode_set_error(vnode, EPIPE);
    }
  else
    {
      vnode_lock(ov);
      if(ov->content.rbox &&
         ((avl_size = ov->rbox.read_posted - ov->rbox.read_done) > 0))
        {
          /* immediate receive */
          if(vnode->wbox.write_posted <= avl_size)
            {
              /* give the whole fragment directly to 'ov' */
              padico_out(20, "vnode=%p copy whole fragment\n", vnode);
              memcpy(ov->rbox.read_buffer, vnode->wbox.write_buffer, vnode->wbox.write_posted);
              ov->rbox.read_done += vnode->wbox.write_posted;
              vnode->wbox.write_done = vnode->wbox.write_posted;
            }
          else
            {
              /* give a part; store the remaining in ov->rbuf */
              vpipe_fragment_t f = vpipe_fragment_new();
              padico_out(20, "vnode=%p copy partial fragment\n", vnode);
              memcpy(ov->rbox.read_buffer, vnode->wbox.write_buffer, avl_size);
              ov->rbox.read_done += avl_size;
              f->size = vnode->wbox.write_posted - avl_size;
              f->ptr = padico_malloc(f->size);
              memcpy(f->ptr, vnode->wbox.write_buffer, f->size);
              vpipe_fragment_list_push_back((vpipe_specific(ov)->rbuf), f);
              vnode->wbox.write_done = vnode->wbox.write_posted;
              ov->status.readable = 1;
            }
        }
      else
        {
          /* 'ov' not ready; store whole fragment in ov->rbuf */
          vpipe_fragment_t f = vpipe_fragment_new();
          padico_out(20, "vnode=%p store fragment\n", vnode);
          f->size = vnode->wbox.write_posted;
          f->ptr = padico_malloc(f->size);
          memcpy(f->ptr, vnode->wbox.write_buffer, vnode->wbox.write_posted);
          vpipe_fragment_list_push_back((vpipe_specific(ov)->rbuf), f);
          vnode->wbox.write_done = vnode->wbox.write_posted;
          ov->status.readable = 1;
        }
      vobs_signal(ov, vobs_attr_rbox);
      vnode_unlock(ov);
    }
  padico_out(20, "exiting vnode=%p\n", vnode);
}

/** @pre vnode locked
 */
static void vpipe_read(padico_vnode_t vnode)
{
  vpipe_fragment_list_t buf = vpipe_specific(vnode)->rbuf;
  padico_out(20, "vnode=%p entering\n", vnode);
  if(vpipe_fragment_list_empty(buf))
    {
      if(!(vpipe_specific(vnode)->other_vnode))
        {
          padico_out(20, "vnode=%p; other vnode closed- signaling EOF\n", vnode);
          vnode->status.end_of_file = 1;
        }
    }
  else
    {
      /* take data in buffer */
      vpipe_fragment_t f = vpipe_fragment_list_front(buf);
      padico_out(20, "vnode=%p take data in buffer\n", vnode);
      if(f->size <= vnode->rbox.read_posted)
        {
          /* take a whole fragment */
          memcpy(vnode->rbox.read_buffer, f->ptr, f->size);
          vnode->rbox.read_done = f->size;
          vpipe_fragment_list_pop_front(buf);
          padico_free(f->ptr);
          vpipe_fragment_delete(f);
        }
      else
        {
          int newsize = f->size - vnode->rbox.read_posted;
          char*tmp = padico_malloc(newsize);
          /* take a part of a fragment */
          memcpy(vnode->rbox.read_buffer, f->ptr, vnode->rbox.read_posted);
          vnode->rbox.read_done = vnode->rbox.read_posted;
          /* resize the remaining fragment */
          memcpy(tmp, f->ptr+vnode->rbox.read_done, newsize);
          padico_free(f->ptr);
          f->size = newsize;
          f->ptr = tmp;
        }
    }
  if(vpipe_fragment_list_empty(buf))
    {
      vnode->status.readable = 0;
    }
  padico_out(20, "exiting vnode=%p done=%ld\n", vnode, puk_slong(vnode->rbox.read_done));
}

static int vpipe_close(padico_vnode_t vnode)
{
  int rc = 0;
  vpipe_fragment_list_t buf = vpipe_specific(vnode)->rbuf;
  padico_vnode_t ov = vpipe_specific(vnode)->other_vnode;

  padico_out(20, "entering vnode=%p\n", vnode);
  /* discard the remaining data in the pipe */
  while(!vpipe_fragment_list_empty(buf))
    {
      vpipe_fragment_t f = vpipe_fragment_list_pop_front(buf);
      padico_free(f->ptr);
      vpipe_fragment_delete(f);
    }
  vpipe_fragment_list_delete(buf);
  /* disconnect other endpoint */
  if(ov)
    {
      vnode_lock(ov);
      vpipe_specific(ov)->other_vnode = NULL;
      vnode_unlock(ov);
    }
  vnode->status.opened = 0;
  vobs_vpipe_deactivate(vnode);
  return rc;
}
