/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal structures definition for the VLink/Packet component -- This is *not* a public API.
 * @ingroup VIO
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/VLink-internals.h>


/* *** Types *********************************************** */

/** Address for a VLink_Packet endpoint
 */
struct vopw_address_s
{
  padico_topo_node_t na_node; /**< node descriptor */
  uint16_t tag;             /**< tag which uniquely identifies the virtual socket */
};
/** Serialized form of a vopw_address_s (in network byte order)
 */
struct sockaddr_padico_vopw
{
  uint16_t vopw_tag;
  char     vopw_node_uuid[PADICO_TOPO_UUID_SIZE];
};
/** the AF_PADICO_VOPW magic number as an uint32_t
 */
#define AF_PADICO_VOPW AF_PADICO_MAGIC('V', 'O', 'P', 'W')

/* *** iboxes ********************************************** */

/** @brief automatic buffers with sliding pointers
 */
struct vopw_buffer_s
{
  char*data;          /**< beginning of the data buffer */
  size_t total_size;  /**< size of the 'data' buffer */
  size_t offset;
  size_t size;
};

/** @brief incoming buffering box for Madeleine wrapper
 */
PUK_LIST_TYPE(vopw_ibox,
              int what;                    /**< rendezvous or buffer (VOPW_IBOX_* defines) */
              padico_topo_node_t node_from;
              uint16_t port;               /**< originating port number ('host' byte order) */
              size_t rendezvous_size;      /**< size of the rendez-vous, if present */
              struct vopw_buffer_s buffer; /**< raw data received by copy */
              );

/* *** iboxes types */

#define VOPW_IBOX_DATA       0x01
#define VOPW_IBOX_RENDEZVOUS 0x02
#define VOPW_IBOX_OKTOSEND   0x04
#define VOPW_IBOX_PACKET     0x08
#define VOPW_IBOX_EOS        0x10


/* *** Buffer management *********************************** */

#define VOPW_BUF_MEMTOBUFFER(BUF, MEM, SIZ) \
  { memcpy((BUF).data+(BUF).size, (MEM), (SIZ)); \
    (BUF).size += (SIZ); }

#define VOPW_BUF_PUMPTOBUFFER(BUF, SIZ, TOKEN, PUMP) \
  { (*(PUMP))((TOKEN), ((BUF).data +(BUF).size), (SIZ)); \
    (BUF).size += (SIZ); }

#define VOPW_BUF_BUFFERTOMEM(MEM, BUF, SIZ) \
  { memcpy(MEM, (BUF).data+(BUF).offset, SIZ); \
    (BUF).offset += SIZ; }

#define VOPW_BUF_ALLOC(BUF, SIZ) \
  { (BUF).data=padico_malloc(SIZ); \
    (BUF).offset=0; \
    (BUF).size=0; \
    (BUF).total_size=(SIZ); }

#define VOPW_BUF_CURRENTSIZE(BUF) \
  ((BUF).size-(BUF).offset)

#define VOPW_BUF_FREE(BUF) \
  padico_free((BUF).data)


/* *** Tags management ************************************* */

/* maximum number of simultaneously opened vsocks */
#ifdef PUKABI
#define VOPW_VNODES_MAX PUK_ABI_FD_MAX
#else /* PUKABI */
#define VOPW_VNODES_MAX 256
#endif /* PUKABI */
#define VOPW_TAG_NOTAG  -1
#define VOPW_TAG_ANY     0
#define VOPW_TAG_FIRST   1
#define VOPW_TAG_LAST    (VOPW_VNODES_MAX-1)
#define VOPW_TAG_NEXT(T) (((T)==VOPW_TAG_LAST)?(VOPW_TAG_FIRST):((T)+1))

/* *** Dynamic node resolution ***************************** */

struct vopw_dynres_entry_s
{
  uint32_t addr;
  uint32_t rank;
  uint16_t port;
  uint16_t tag;
};
PUK_VECT_TYPE(vopw_dynres, struct vopw_dynres_entry_s);

/** Dynamic address resolution manager
 */
struct vopw_dynres_s
{
  vopw_dynres_vect_t cache; /**< cache of known addresses */
  marcel_mutex_t     lock;  /**< lock for the cache access */
  marcel_cond_t      cond;  /**< monitor events in the cache */
};

/* *** "on the wire" operations **************************** */

/* used only for deferred requests */
#define VOPW_OP_NONE         0x00

#define VOPW_OP_CONNECT      0x01
struct vopw_hdr_info_connect_s
{
  uint32_t my_addr;   /**< IP address of the client side */
  uint16_t my_tag;    /**< tag of the client side */
  uint16_t your_tag;  /**< tag of the server socket */
  uint16_t domain;    /**< domain of the virtual socket.
                       ** @note only AF_INET is actually supported */
  uint16_t type;      /**< SOCK_STREAM, SOCK_DGRAM */
} __attribute__((packed));

#define VOPW_OP_CONNREFUSED  0x02

#define VOPW_OP_ACCEPTED     0x03
struct vopw_hdr_info_accepted_s
{
  uint32_t my_addr;   /**< IP address of the server */
  uint16_t my_tag;    /**< tag for the newly created socket */
} __attribute__((packed));

#define VOPW_OP_CLOSE        0x04 /* the sender will not receive anymore */

#define VOPW_OP_EOS          0x05 /* end-of-stream: the sender will not send anymore */

#define VOPW_OP_GET_TAG      0x06 /* Port-to-tag resolution request */
struct vopw_hdr_info_gettag_s
{
  uint16_t domain;    /**< @note only AF_INET supported */
  uint16_t type;      /**< SOCK_STREAM, SOCK_DGRAM */
  uint16_t your_port; /**< port number requested */
} __attribute__((packed));

#define VOPW_OP_GIVE_TAG     0x07 /* Port-to-tag resolution reply */
struct vopw_hdr_info_givetag_s
{
  uint32_t my_addr;
  uint32_t my_rank;
  uint16_t my_port;
  uint16_t my_tag;
} __attribute__((packed));

#define VOPW_OP_DATA         0x11 /* streamed data */
struct vopw_hdr_info_data_s
{
  uint32_t len;     /**< length of data (including data in header) */
  uint32_t tlen;    /**< length of data in message body */
  char     data[4]; /**< place holder for a pointer- actually, more space is used */
} __attribute__((packed));

#define VOPW_OP_RENDEZVOUS   0x12
struct vopw_hdr_info_rendezvous_s
{
  uint32_t proposed_len; /**< length of the data ready to be sent */
  /** @note It is safe to assert VOPW_DATA_IN_HEADER < VOPW_RENDEZVOUS_THRESHOLD  */
} __attribute__((packed));

#define VOPW_OP_OKTOSEND     0x14
struct vopw_hdr_info_oktosend_s
{
  uint32_t accepted_len; /**< accepted_len <= proposed_len - DATA_IN_HEADER */
} __attribute__((packed));

#define VOPW_OP_PACKET       0x15 /* packet-based data */
struct vopw_hdr_info_packet_s
{
  uint32_t len;       /**< length of data (including data in header) */
  uint16_t port;      /**< port from where the packet is sent */
  char     data[4];   /**< place-holder for actual data */
} __attribute__((packed));

/** @brief Header for all outgoing messages through VSock/Madeleine wrapper
 ** @warning sizeof(struct vopw_hdr_s) should be < PADICO_MADIO_HDR_SIZE
 ** tested in vsock_vopw_init()
 */
struct vopw_hdr_s
{
  uint16_t op;  /**< operator code */
  uint16_t tag; /**< destination MAD tag */
  union vopw_hdr_info_u
  {
    struct vopw_hdr_info_connect_s    CONNECT;
    struct vopw_hdr_info_accepted_s   ACCEPTED;
    struct vopw_hdr_info_data_s       DATA;
    struct vopw_hdr_info_rendezvous_s RENDEZVOUS;
    struct vopw_hdr_info_oktosend_s   OKTOSEND;
    struct vopw_hdr_info_gettag_s     GETTAG;
    struct vopw_hdr_info_givetag_s    GIVETAG;
    struct vopw_hdr_info_packet_s     PACKET;
  } info;    /**< variable part of the header */
} __attribute__((packed));


/** Structure to describe deferred operations to be executed in
 * a different thread than the receiving one. 'hdr' is in network
 * byte order; other fields are in host byte order.
 */
struct vopw_deferred_req_s
{
  int op; /* VOPW_OP_NONE: no default header- full header given instead in field 'hdr' */
  struct vopw_hdr_s hdr;
  padico_vnode_t vnode;
  padico_topo_node_t dest_node;
  int dest_tag;
};
