/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * helper to bootstrap PadicoTM as a library
 */

#ifndef PADICO_BOOTLIB_H
#define PADICO_BOOTLIB_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <stdlib.h>

/** initialize PadicoTM as a library */
static inline puk_mod_t padico_lib_init(void)
{
  puk_mod_t boot_mod = NULL;
  if(puk_opt_parse_bool(getenv("PADICO_PRELOAD")))
    {
      /* PadicoTM was preloaded by launcher */
      puk_mod_t tm_mod = puk_mod_getbyname("PadicoTM");
      assert(tm_mod != NULL);
      puk_mod_lock_use(tm_mod); /* lock PadicoTM module to prevent auto-unload */
    }
  else
    {
      /* manually load PadicoTM */
      padico_puk_add_path(PADICOTM_ROOT);
      padico_rc_t rc = padico_puk_mod_resolve(&boot_mod, "PadicoBootLib");
      if((boot_mod == NULL) || padico_rc_iserror(rc))
        {
          padico_fatal("cannot resolve module PadicoBootLib.\n");
        }
      rc = padico_puk_mod_load(boot_mod);
      puk_mod_lock_use(boot_mod); /* lock PadicoBootLib module to prevent auto-unload */
      if(padico_rc_iserror(rc))
        {
          padico_fatal("cannot load module PadicoBootLib.\n");
        }
      puk_mod_t tm_mod = NULL;
      padico_rc_delete(rc);
      rc = padico_puk_mod_resolve(&tm_mod, "PadicoTM");
      if(!padico_rc_iserror(rc))
        {
          puk_mod_add_dep(tm_mod, boot_mod);
        }
      padico_rc_delete(rc);
    }
  return boot_mod;
}

static inline void padico_lib_finalize(puk_mod_t boot_mod)
{
  puk_mod_unlock_use(boot_mod); /* release lock, allow unload */
  /*
    padico_rc_t rc = padico_puk_mod_unload(boot_mod);
    if(padico_rc_iserror(rc))
    {
    padico_warning("error while unloading.\n");
    }
    padico_rc_delete(rc);
  */
}

#endif /* PADICO_BOOTLIB_H */
