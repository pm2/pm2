/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* @file Timing.h
 * @brief Timing service
 * @note includes a lot of copy/paste from PM2 tbx_tick
 * @author Alexandre Denis and Raymond Namyst
 */

#ifndef PADICO_TIMING_H
#define PADICO_TIMING_H


#include <sys/types.h>
#include <sys/time.h>

#include <Padico/Puk.h>

PUK_MOD_AUTO_DEP(Timing, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME Timing

#ifdef __cplusplus
extern "C" {
#endif

#define PADICO_TIMING_GET_TICK(t)       padico_timing_get_tick(&(t))
#define PADICO_TIMING_TICK_DIFF(t1, t2) padico_timing_diff_tick(&(t1), &(t2))


  /* *** Profiling ***************************************** */

  struct padico_timing_profiler_s
  {
    int num_points;         /**< number of profiling points per iteration */
    int num_iter;           /**< number of iterations beeing profiled */
    int current_iter;       /**< current iteration number */
    padico_timing_t*points; /**< profiling points data (size = num_iter x num_points array) */
    const char*filename;    /**< filename where to flush profiling data */
    const char**labels;     /**< labels of points (size = num_points) */
  };
  typedef struct padico_timing_profiler_s* padico_timing_profiler_t;

  extern padico_timing_profiler_t padico_timing_profiler_new(int num_points,
                                                             int num_iter,
                                                             int ignore,
                                                             const char*filename);

  extern void padico_timing_profile_point_label(padico_timing_profiler_t p, int n_point, const char*label);

#define padico_timing_profile_point(P, N) padico_timing_profile_point_label((P), (N), __FUNCTION__)

  extern void padico_timing_profile_start(padico_timing_profiler_t p);

  extern void padico_timing_profile_flush(padico_timing_profiler_t p);

#undef PADICO_TIMING_DO_FLUSH

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* PADICO_TIMING_H */
