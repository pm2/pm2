/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief gSOAP interface for remote process steering
 * @ingroup SOAPGatekeeper
 */

//gsoap padico_gatekeeper service name: PadicoGatekeeper
//gsoap padico_gatekeeper schema namespace: urn:Padico


/* ** General operations
 * *********************************************************
 */
/** return code- mapping for padico_rc_t */
struct padico_gatekeeper__rc_t
{
  int rc;
  char*msg;
};

int padico_gatekeeper__Ping(struct padico_gatekeeper__rc_t*out);

int padico_gatekeeper__Kill(struct padico_gatekeeper__rc_t*out);


/* ** Module/jobs operations
 * *********************************************************
 */

/** a job ID- mapping for puk_job_t */
typedef long  padico_gatekeeper__jobID_t;

/** a module ID- currently, a string */
typedef char* padico_gatekeeper__modID_t;

/** arguments for new jobs */
struct padico_gatekeeper__job_args_s
{
  int __sizeargv;
  char**argv;
};

struct padico_gatekeeper__mod_list_s
{
  int __sizemods;
  padico_gatekeeper__modID_t*mods;
};

struct padico_gatekeeper__SeqString
{
  char*__ptr;
  int __size;
};

struct padico_gatekeeper__attr_s
{
  char*label;
  char*value;
};
struct padico_gatekeeper__attr_list_s
{
  struct padico_gatekeeper__attr_s*__ptr;
  int __size;
};

struct padico_gatekeeper__mod_desc_s
{
  padico_gatekeeper__modID_t modID;
  char* driver;
  struct padico_gatekeeper__attr_list_s attributes;
  struct padico_gatekeeper__SeqString   requires;
  struct padico_gatekeeper__SeqString   units;
};

int padico_gatekeeper__LoadModule(padico_gatekeeper__modID_t modname,
                                  struct padico_gatekeeper__rc_t*out);

int padico_gatekeeper__UnloadModule(padico_gatekeeper__modID_t modname,
                                    struct padico_gatekeeper__rc_t*out);

/** synchronously runs a module */
int padico_gatekeeper__RunModule(padico_gatekeeper__modID_t modname,
                                 struct padico_gatekeeper__job_args_s args,
                                 struct padico_gatekeeper__rc_t*outrc);

/** synchronously runs a module */
int padico_gatekeeper__StartJob(padico_gatekeeper__modID_t modname,
                                struct padico_gatekeeper__job_args_s args,
                                struct StartJobResponse {
                                  padico_gatekeeper__jobID_t jobID;
                                  struct padico_gatekeeper__rc_t*outrc;
                                }*out);

/*
int padico_gatekeeper__NewModule(struct padico_gatekeeper__mod_desc_s module,
                                 struct padico_gatekeeper__rc_t*out);
*/
int padico_gatekeeper__ListModules(struct padico_gatekeeper__mod_list_s*outlist);


/* ** Nodes/groups/topology
 * *********************************************************
 */

typedef char*padico_gatekeeper__nodeID_t;
typedef char*padico_gatekeeper__hostID_t;
typedef char*padico_gatekeeper__networkID_t;
typedef char*padico_gatekeeper__groupID_t;


struct padico_gatekeeper__node_s
{
  padico_gatekeeper__nodeID_t canonical_nodename;
  int rank;
  padico_gatekeeper__hostID_t hostID;
};


// a list of nodes- is it used anywhere outside group_s?
struct padico_gatekeeper__nodeID_list_s
{
  int __sizenodes;
  padico_gatekeeper__nodeID_t* nodes;
};

// a network, basically a list of nodes (full struct)
struct padico_gatekeeper__network_s
{
  padico_gatekeeper__networkID_t networkID;
  int __sizenodes;
  struct padico_gatekeeper__node_s*nodes;
};

// a group, basically a list of nodeID
struct padico_gatekeeper__group_s
{
  padico_gatekeeper__groupID_t groupID;
  struct padico_gatekeeper__nodeID_list_s nodes;
};

int padico_gatekeeper__GetNodeInfo(padico_gatekeeper__nodeID_t nodeID,
                                   struct GetNodeInfoResponse {
                                     struct padico_gatekeeper__node_s node;
                                     struct padico_gatekeeper__rc_t rc;
                                   }*out);
/*
int padico_gatekeeper__GetGroupInfo(padico_gatekeeper__groupID_t groupID,
                                    struct GetGroupInfoResponse {
                                      struct padico_gatekeeper__group_s group;
                                      struct padico_gatekeeper__rc_t rc;
                                    }*out);
*/
int padico_gatekeeper__CreateGroup(padico_gatekeeper__groupID_t groupID,
                                   struct padico_gatekeeper__nodeID_list_s nodes,
                                   struct padico_gatekeeper__rc_t*out);
/*
int padico_gatekeeper__DeleteGroup(padico_gatekeeper__groupID_t groupID,
                                   struct padico_gatekeeper__rc_t*out);
*/
struct padico_gatekeeper__cluster_contact_s
{
  padico_gatekeeper__hostID_t hostID;
  int port;
};


int padico_gatekeeper__GetLocalNetworkInfo(struct padico_gatekeeper__network_s*network);

int padico_gatekeeper__BridgeInit(struct padico_gatekeeper__cluster_contact_s*out);

int padico_gatekeeper__BridgeConnect(struct padico_gatekeeper__cluster_contact_s contact,
                                     struct padico_gatekeeper__rc_t*out);


/* ** Raw steering
 * *********************************************************
 */

int padico_gatekeeper__RawXMLCommand(char*xml_command,
                                     struct padico_gatekeeper__rc_t*out);
