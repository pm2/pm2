/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#include <Padico/Module.h>

#if defined(MARCEL)
#  warning "Shm: Marcel is enabled- not building module 'Shm'"
#else /* MARCEL */

static int psp_shm_module_init(void);
static void psp_shm_module_finalize(void);

PADICO_MODULE_DECLARE(PSP_Shm, psp_shm_module_init, NULL, psp_shm_module_finalize,
                      "Shm", "PSP");

#include "Shm.h"

//#define PSP_SHM_PIOMAN



/** @defgroup Shm component: Shm- inter-process shared memory communication
 * @ingroup PadicoComponent
 */

/* ********************************************************* */
/* *** PadicoSimplePackets for Shm */
static void*psp_shm_instantiate(puk_instance_t ai, puk_context_t context);
static void psp_shm_destroy(void*_instance);
static void psp_shm_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                           padico_psp_handler_t handler, void*key);
static padico_psp_connection_t psp_shm_new_message(void*_instance, padico_topo_node_t node, void**_sbuf);
static void psp_shm_pack(void*_instance, padico_psp_connection_t _conn,
                           const char*bytes, size_t size);
static void psp_shm_end_message(void*_instance, padico_psp_connection_t _conn);

/** instanciation facet for Shm
 * @ingroup Shm
 */
static const struct puk_component_driver_s psp_shm_component_driver =
  {
    .instantiate = &psp_shm_instantiate,
    .destroy     = &psp_shm_destroy
  };
/** 'SimplePackets' facet for Shm
 * @ingroup Shm
 */
static const struct padico_psp_driver_s psp_shm_driver =
  {
    .init        = &psp_shm_init,
    .connect     = NULL,
    .listen      = NULL,
    .new_message = &psp_shm_new_message,
    .pack        = &psp_shm_pack,
    .end_message = &psp_shm_end_message
  };

/** a PSP/Shm instance
 * @ingroup Shm
 */
struct psp_shm_instance_s
{
  padico_psp_slot_t slot;
  int header_block; /**< block containing the sbuf header; -1 if already sent */
#warning TODO- spinlock in instance to ensure single sender semantics
};

/** global state of PSP_Shm */
static struct
{
  struct padico_shm_s*shm;
  struct padico_psp_directory_s slots;
  int running;
  padico_tm_bgthread_pool_t pool;
#ifdef PSP_SHM_PIOMAN
  struct piom_ltask ltask;
#endif
} psp_shm;

/* ********************************************************* */

static void*psp_shm_polling_worker(void*dummy);
int psp_shm_polling_ltask(void*_arg);

static void psp_shm_start_polling(void)
{
  if(psp_shm.running == 0)
    {
      /* init polling */
#ifdef PSP_SHM_PIOMAN
      piom_ltask_create(&psp_shm.ltask, &psp_shm_polling_ltask, NULL, PIOM_LTASK_OPTION_REPEAT, piom_vpset_full);
      piom_ltask_submit(&psp_shm.ltask);
#else /* PSP_SHM_PIOMAN */
      padico_tm_bgthread_start(psp_shm.pool, &psp_shm_polling_worker, NULL, "psp_shm_polling_worker");
#endif /* PSP_SHM_PIOMAN */
    }
  psp_shm.running++;
}

/* ********************************************************* */
/* *** PadicoComponent */

static void*psp_shm_instantiate(puk_instance_t ai, puk_context_t context)
{
  struct psp_shm_instance_s*instance = padico_malloc(sizeof(struct psp_shm_instance_s));
  instance->header_block = -1;
  instance->slot = NULL;
  psp_shm_start_polling();
  return instance;
}
static void psp_shm_destroy(void*_instance)
{
  struct psp_shm_instance_s*instance = _instance;
  if(instance->slot->tag != -1)
    padico_psp_slot_remove(&psp_shm.slots, padico_psp_slot_lookup(&psp_shm.slots,instance->slot->tag));
  padico_free(instance);
}

/* ********************************************************* */
/* *** PadicoSimplePackets */

struct psp_shm_header_s
{
  uint32_t tag;
};

static void psp_shm_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                         padico_psp_handler_t handler, void*key)
{
  struct psp_shm_instance_s*instance = _instance;
  padico_out(20, "h_size=%d; tag=%d\n", (int)*h_size, tag);
  *h_size = PADICO_SHM_BLOCKSIZE - sizeof(struct padico_shm_short_header_s) - sizeof(struct psp_shm_header_s);
  assert(sizeof(struct padico_shm_short_header_s) + sizeof(struct psp_shm_header_s) + *h_size <= PADICO_SHM_BLOCKSIZE);
  instance->slot = padico_psp_slot_insert(&psp_shm.slots, handler, key, tag, *h_size);
}


static void psp_shm_pump(void*_token, void*bytes, size_t b_size)
{
  padico_shm_large_recv(psp_shm.shm, bytes, b_size, *(int*)_token);
}


static void psp_shm_poll_handler(int block_num)
{
  void*block = padico_shm_block_get_ptr(psp_shm.shm, block_num);
  struct padico_shm_short_header_s*shm_header = block;
  const int from = shm_header->from;
  struct psp_shm_header_s*psp_header = block + sizeof(struct padico_shm_short_header_s);
  const int tag = psp_header->tag;
  void*header = block + sizeof(struct padico_shm_short_header_s) + sizeof(struct psp_shm_header_s);

  padico_out(40, "ready! from=%d; block #%d\n", from, block_num);
  padico_topo_node_t node_from = padico_topo_getnodebyuuid((void*)psp_shm.shm->seg->directory.nodes[from].node_uuid);
  padico_psp_slot_t instance = padico_psp_slot_lookup(&psp_shm.slots, tag);
  padico_out(40, "invoking handler...\n");
  (*instance->handler)(header, node_from, instance->key, &psp_shm_pump , (void*)&from);
  /* release header block */
  padico_shm_block_free(psp_shm.shm, block_num);
  padico_out(40, "done.\n");
}

#ifdef PSP_SHM_PIOMAN
#warning experimental Shm-pioman
int psp_shm_polling_ltask(void*_arg)
{
  int block_num = padico_shm_short_recv_poll();
  if(block_num != -1)
    {
      psp_shm_poll_handler(block_num);
    }
  return 0;
}
#else /* PSP_SHM_PIOMAN */
static void*psp_shm_polling_worker(void*dummy)
{
  for(;;)
    {
      int block_num = -1;
      padico_out(40, "waiting...\n");
      do
        {
          block_num = padico_shm_short_recv_poll(psp_shm.shm, NULL);
          if(block_num == -1)
            sched_yield();
        }
      while(block_num == -1);
      psp_shm_poll_handler(block_num);
    }
  return NULL;
}
#endif /* PSP_SHM_PIOMAN */

static inline void psp_shm_send_header(struct psp_shm_instance_s*instance, struct padico_shm_node_s*dest)
{
  if(instance->header_block != -1)
    {
      int rc = padico_shm_short_send_commit(psp_shm.shm, dest, instance->header_block, instance->slot->h_size, 1);
      if(rc)
        padico_fatal("cannot send- receiver queue full.\n");
      instance->header_block = -1;
    }
}

static padico_psp_connection_t psp_shm_new_message(void*_instance, padico_topo_node_t node, void**_sbuf)
{
  struct psp_shm_instance_s*instance = _instance;
  struct padico_shm_node_s*dest = padico_shm_directory_node_lookup(psp_shm.shm, node);
  /* preapare header */
  int block_num = -1;
  do
    {
      block_num = padico_shm_block_alloc(psp_shm.shm);
    }
  while(block_num == -1);
  assert(instance->header_block == -1);
  instance->header_block = block_num;
  void*block = padico_shm_block_get_ptr(psp_shm.shm, block_num);
  struct psp_shm_header_s*psp_header = block + sizeof(struct padico_shm_short_header_s);
  psp_header->tag = instance->slot->tag;
  *_sbuf = block + sizeof(struct padico_shm_short_header_s) + sizeof(struct psp_shm_header_s);
  return (padico_psp_connection_t)dest;
}

static void psp_shm_pack(void*_instance, padico_psp_connection_t _conn,
                         const char*bytes, size_t size)
{
  struct psp_shm_instance_s*instance = _instance;
  struct padico_shm_node_s*dest = (struct padico_shm_node_s*)_conn;
  psp_shm_send_header(instance, dest);
  padico_shm_large_send(psp_shm.shm, bytes, size, dest);
}

static void psp_shm_end_message(void*_instance, padico_psp_connection_t _conn)
{
  struct psp_shm_instance_s*instance = _instance;
  struct padico_shm_node_s*dest = (struct padico_shm_node_s*)_conn;
  psp_shm_send_header(instance, dest);
}


/* ********************************************************* */

int psp_shm_module_init(void)
{
  psp_shm.shm = padico_shm_init("PSP_Shm");

  psp_shm.running = 0;
  psp_shm.pool = padico_tm_bgthread_pool_create("PSP_Shm");
  /* init instances index */
  padico_psp_directory_init(&psp_shm.slots);

  /* declare component */
  puk_component_declare("PSP_Shm",
                        puk_component_provides("PadicoComponent", "component", &psp_shm_component_driver),
                        puk_component_provides("PadicoSimplePackets", "psp", &psp_shm_driver));
  return 0;
}

void psp_shm_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(psp_shm.pool);
}

#endif /* MARCEL */
