/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Grid topology directory
 * @ingroup Topology
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PM2.h>
#include <Padico/PadicoTM.h>

#include "Topology-internals.h"

#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <resolv.h>
#include <dirent.h>
#include <poll.h>

#ifdef HAVE_IFADDRS_H
#include <ifaddrs.h>
#include <netpacket/packet.h>
#endif
#include <sys/ioctl.h>

#ifdef HAVE_LINUX_NETLINK_H
#include <linux/netlink.h>
#endif
#ifdef HAVE_LINUX_RTNETLINK_H
#include <linux/rtnetlink.h>
#endif
#ifdef HAVE_LINUX_SOCKIOS_H
#include <linux/sockios.h>
#endif
#ifdef HAVE_LINUX_ETHTOOL_H
#include <linux/ethtool.h>
#endif
#ifdef HAVE_LINUX_IF_H
#include <linux/if.h>
#else
#include <net/if.h>
#endif /* HAVE_LINUX_IF_H */

static int padico_topology_init(void);
static void padico_topology_finalize(void);

PADICO_MODULE_DECLARE(Topology, padico_topology_init, NULL, padico_topology_finalize);

PADICO_MODULE_ATTR(disable_dns, "PADICO_DISABLE_DNS", "do not try to resolve hostnames", bool, 0);
PADICO_MODULE_ATTR(disable_ipv6, "PADICO_DISABLE_IPV6", "do not use IPv6 addresses", bool, 0);
PADICO_MODULE_ATTR(autodetect, "PADICO_AUTODETECT", "auto-detect networks on startup and load drivers", bool, 1);

static struct padico_topology_s na_topology;

struct padico_topology_s*topology_get(void)
{
  return &na_topology;
}

static void topology_populate(void);
static void topo_inet_addr_detect(padico_topo_host_t localhost);
static void topo_inet_route_detect(padico_topo_host_t localhost);
static void topo_inet_service_detect(padico_topo_host_t localhost);
static void topo_device_detect(void);
static const char*topo_addr_visibility_name(padico_topo_addr_visibility_t v);
static const char*topo_hwaddr_type_name(int type);
static void topo_host_destroy(padico_topo_host_t host);
static void topo_network_destroy(padico_topo_network_t network);
__PUK_SYM_INTERNAL void topo_binding_destroy(struct topo_binding_s*binding);
__PUK_SYM_INTERNAL void topo_property_destroy(struct topo_property_s*prop);


/* ********************************************************* */

/* Helper for addresses in bindings */

const struct topo_address_s*topo_binding_getaddr(const struct topo_binding_s*_binding)
{
  struct topo_binding_s*const binding = (struct topo_binding_s*)_binding; /* wild cast to deal with iterators, but we don't write binding, really */
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      const struct topo_property_s*prop = *p;
      if(prop->kind == TOPO_PROPERTY_ADDR)
        {
          return &prop->content.ADDR.addr;
        }
    }
  return NULL;
}

/* hashing functions for addresses */

static int topo_address_eq(const void*_addr1, const void*_addr2)
{
  const struct topo_address_s*addr1 = _addr1;
  const struct topo_address_s*addr2 = _addr2;
  return (addr1->size == addr2->size) &&
         (memcmp(&addr1->_addr_storage[0], &addr2->_addr_storage[0], addr1->size) == 0);
}
static uint32_t topo_address_hash(const void*_addr)
{
  const struct topo_address_s*addr = _addr;
  return puk_hash_default((const void*)&addr->_addr_storage[0], addr->size);
}

/* hashing functions for UUID */

static int topo_uuid_eq(const void*uuid1, const void*uuid2)
{
  return (memcmp(uuid1, uuid2, PADICO_TOPO_UUID_SIZE) == 0);
}
static uint32_t topo_uuid_hash(const void*uuid)
{
  return puk_hash_default(uuid, PADICO_TOPO_UUID_SIZE);
}

/* ********************************************************* */
/* ** hwaddr topology modification */

padico_topo_hwaddr_t padico_topo_hwaddr_create(const char*name, int index, int type, size_t len, const void*addr)
{
  struct topo_hwaddr_s*hwaddr = padico_malloc(sizeof(struct topo_hwaddr_s) + len);
  hwaddr->type = type;
  hwaddr->index = index;
  hwaddr->len = len;
  memcpy(&hwaddr->_hwaddr_storage, addr, len);
  assert(strlen(name) < TOPO_HWADDR_NAMSIZ);
  strncpy(hwaddr->name, name, TOPO_HWADDR_NAMSIZ);
  hwaddr->name[TOPO_HWADDR_NAMSIZ - 1] = 0;
  return hwaddr;
}

void padico_topo_hwaddr_get(padico_topo_hwaddr_t hwaddr, const char**name, int*index, int*type, size_t*len, const void**addr)
{
  if(name)
    *name = hwaddr->name;
  if(index)
    *index = hwaddr->index;
  if(type)
    *type = hwaddr->type;
  if(len)
    *len = hwaddr->len;
  if(addr)
    *addr = &hwaddr->_hwaddr_storage;
}

void padico_topo_hwaddr_add(padico_topo_host_t host, padico_topo_hwaddr_t*_hwaddr)
{
  padico_topo_hwaddr_t hwaddr = *_hwaddr;
  padico_topo_hwaddr_t byname = topo_hwaddr_getbyname(host, hwaddr->name);
  if(byname == NULL)
    {
      /* store new hwaddr */
      topo_hwaddr_vect_push_back(&host->hwaddrs, hwaddr);
    }
  else if((byname->len == hwaddr->len) && (memcmp(byname->_hwaddr_storage, hwaddr->_hwaddr_storage, byname->len) == 0))
    {
      /* duplicate detected (same name, same address, index not compared since it may be invalid at this stage) */
      padico_free((void*)hwaddr);
      hwaddr = NULL;
      *_hwaddr = byname;
    }
  else
    {
      padico_fatal("conflicting hwaddr with same name=%s; different address detected.\n", hwaddr->name);
    }
}

void padico_topo_hwaddr_destroy(padico_topo_hwaddr_t hwaddr)
{
  padico_free((void*)hwaddr);
}

/* ********************************************************* */
/* ** binding topology modification */

void padico_topo_binding_add_addr(padico_topo_binding_t binding, const void*addr)
{
  padico_topo_network_t network = binding->network;
  const size_t addr_size = network->addr_size;
  const int family = network->family;
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + addr_size);
  prop->kind = TOPO_PROPERTY_ADDR;
  prop->content.ADDR.addr.family = family;
  prop->content.ADDR.addr.size = addr_size;
  memcpy(&prop->content.ADDR.addr._addr_storage[0], addr, addr_size);
  topo_property_vect_push_back(&binding->props, prop);
  /*  puk_hashtable_insert(binding->network->bindings_by_addr, &prop->content.ADDR._addr_storage, binding); */
}

void padico_topo_binding_add_hwaddr(padico_topo_binding_t binding, padico_topo_hwaddr_t hwaddr)
{
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_HWADDR;
  prop->content.HWADDR.hwaddr = hwaddr;
  memcpy(prop->content.HWADDR.ifname, hwaddr->name, TOPO_HWADDR_NAMSIZ);
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_index(padico_topo_binding_t binding, int64_t index)
{
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_INDEX;
  prop->content.INDEX.index = index;
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_gw_addr(padico_topo_binding_t binding, struct in_addr*inaddr)
{
  topo_property_vect_itor_t p;
  struct topo_property_s*prop = NULL;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      prop = *p;
      if(prop->kind == TOPO_PROPERTY_GW_ADDR &&
         memcmp(&prop->content.GW_ADDR.addr._addr_storage[0], inaddr,  prop->content.GW_ADDR.addr.size) == 0)
        return;
    }
  prop = padico_malloc(sizeof(struct topo_property_s) + sizeof(struct in_addr));
  prop->kind = TOPO_PROPERTY_GW_ADDR;
  prop->content.GW_ADDR.addr.family = AF_INET;
  prop->content.GW_ADDR.addr.size = sizeof(struct in_addr);
  memcpy(&prop->content.GW_ADDR.addr._addr_storage[0], inaddr, sizeof(struct in_addr));
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_gw_hwaddr(padico_topo_binding_t binding, padico_topo_hwaddr_t hwaddr)
{
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_GW_HWADDR;
  prop->content.GW_HWADDR.hwaddr = hwaddr;
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_hostname(padico_topo_binding_t binding, const char*name)
{
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_HOSTNAME;
  prop->content.HOSTNAME.name = padico_strdup(name);
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_route(padico_topo_binding_t binding, padico_topo_network_t dest, const void*gwaddr)
{
  padico_topo_network_t network = binding->network;
  const size_t addr_size = network->addr_size;
  const int family = network->family;
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + addr_size);
  prop->kind = TOPO_PROPERTY_ROUTE;
  prop->content.ROUTE.dest = dest;
  prop->content.ROUTE.gw.family = family;
  prop->content.ROUTE.gw.size = addr_size;
  memcpy(&prop->content.ROUTE.gw._addr_storage[0], gwaddr, addr_size);
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_filter(padico_topo_binding_t binding, padico_topo_filter_policy_t policy, uint16_t port, int protocol)
{
  assert(policy > _PADICO_TOPO_FILTER_NONE && policy < _PADICO_TOPO_FILTER_MAX);
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_FILTER;
  prop->content.FILTER.filter.policy = policy;
  prop->content.FILTER.filter.port = port;
  prop->content.FILTER.filter.protocol = protocol;
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_add_perf(padico_topo_binding_t binding, int latency, int bandwidth)
{
  struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
  prop->kind = TOPO_PROPERTY_PERF;
  prop->content.PERF.perf.lat = latency;
  prop->content.PERF.perf.bw = bandwidth;
  topo_property_vect_push_back(&binding->props, prop);
}

void padico_topo_binding_get_perf(padico_topo_binding_t binding, int*latency_nsec, int*bandwidth_kbps)
{
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      const struct topo_property_s*prop = *p;
      if(prop->kind == TOPO_PROPERTY_PERF)
        {
          if(latency_nsec != NULL)
            *latency_nsec = prop->content.PERF.perf.lat;
          if(bandwidth_kbps != NULL)
            *bandwidth_kbps = prop->content.PERF.perf.bw;
          return;
        }
    }
}

padico_topo_hwaddr_t padico_topo_binding_gethwaddr(padico_topo_binding_t binding)
{
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      const struct topo_property_s*prop = *p;
      if(prop->kind == TOPO_PROPERTY_HWADDR)
        {
          return prop->content.HWADDR.hwaddr;
        }
    }
  return NULL;
}

int padico_topo_binding_get_index(padico_topo_binding_t binding, int64_t*index)
{
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      const struct topo_property_s*prop = *p;
      if(prop->kind == TOPO_PROPERTY_INDEX)
        {
          *index = prop->content.INDEX.index;
          return 0;
        }
    }
  return -1;
}


void padico_sockaddr_init(struct sockaddr_pa*addr, socklen_t*addrlen, int magic)
{
  assert(addr && addrlen && *addrlen >= sizeof(struct sockaddr_pa));
  memset(addr->spa_payload, 0, AF_PADICO_PAYLOAD); /* reset with 0s; better for debug! */
  addr->spa_family = AF_PADICO;
  addr->spa_magic  = magic;
}

int padico_sockaddr_check(const struct sockaddr_pa*addr, int magic)
{
  const struct sockaddr*saddr = (struct sockaddr*)addr;
  if(saddr->sa_family != AF_PADICO)
    {
#ifndef NDEBUG
      padico_warning("bad address family- sa_family=%d; expected=%d (AF_PADICO).\n",
                     saddr->sa_family, AF_PADICO);
#endif
      return EAFNOSUPPORT;
    }
  else if(addr->spa_magic != magic)
    {
#ifndef NDEBUG
      const padico_address_kind_t m = AF_PADICO_MAKE(addr->spa_magic);
      const padico_address_kind_t e = AF_PADICO_MAKE(magic);
      padico_warning("bad Padico address magic- spa_magic=%d (%c%c%c%c); expected=%d (%c%c%c%c).\n",
                     addr->spa_magic, m.bytes[0], m.bytes[1], m.bytes[2], m.bytes[3],
                     magic,           e.bytes[0], e.bytes[1], e.bytes[2], e.bytes[3]);
#endif
      return EADDRNOTAVAIL;
    }
  return 0;
}

/* ********************************************************* */

static int padico_topology_init(void)
{

  sethostent(1);

  na_topology.networks    = padico_topo_network_vect_new();
  na_topology.node_table  = puk_hashtable_new(&topo_uuid_hash, &topo_uuid_eq);
  na_topology.nodesbyname = puk_hashtable_new_string();
  na_topology.hostsbyname = puk_hashtable_new_string();
  na_topology.inet        = padico_topo_network_create("inet", PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
  na_topology.inet6       = padico_topo_network_create("inet6", PADICO_TOPO_SCOPE_HOST, sizeof(struct in6_addr), AF_INET6);
  topology_parser_init();
  topology_populate();
  return 0;
}

static void topo_node_entry_destructor(void*_key, void*_data)
{
  struct topo_node_s*node = _data;
  topo_node_destroy(node);
}

static void topo_host_entry_destructor(void*_key, void*_data)
{
  struct topo_host_s*host = _data;
  topo_host_destroy(host);
}

static void padico_topology_finalize(void)
{
  puk_hashtable_delete(na_topology.node_table, &topo_node_entry_destructor);
  puk_hashtable_delete(na_topology.nodesbyname, NULL);
  /* hosts may be registered with several names in the table; extract all single hosts */
  puk_hashtable_t hosts = puk_hashtable_new_ptr();
  puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(na_topology.hostsbyname);
  padico_topo_host_t h;
  h = puk_hashtable_enumerator_next_data(e);
  while(h)
    {
      if(!puk_hashtable_lookup(hosts, h))
        puk_hashtable_insert(hosts, h, h);
      h = puk_hashtable_enumerator_next_data(e);
    }
  puk_hashtable_enumerator_delete(e);
  puk_hashtable_delete(na_topology.hostsbyname, NULL);
  puk_hashtable_delete(hosts, &topo_host_entry_destructor);
  padico_topo_network_vect_itor_t ni;
  puk_vect_foreach(ni, padico_topo_network, na_topology.networks)
    {
      topo_network_destroy(*ni);
    }
  padico_topo_network_vect_delete(na_topology.networks);
  endhostent();
}

/** frontend to gethostbyaddr */
static const struct hostent*topo_gethostbyaddr(const void*addr, socklen_t len, int family)
{
  static int disable_ipv6 = -1;
  if(disable_ipv6 < 0)
    {
      disable_ipv6 = padico_module_attr_disable_ipv6_getvalue();
      if(disable_ipv6)
        {
          padico_warning("IPv6 disabled by user request.\n");
        }
    }
  if(padico_module_attr_disable_dns_getvalue())
    {
      return NULL;
    }
  char s_addr[256] = { '\0' };
  inet_ntop(family, addr, s_addr, 256);
  if(disable_ipv6 && family == AF_INET6)
    {
      return NULL;
    }
  const struct hostent*he = gethostbyaddr(addr, len, family);
  return he;
}

/** frontend to gethostbyname2 to manage TRY_AGAIN error */
static const struct hostent*topo_gethostbyname2(const char*name, int family)
{
  static int disable_ipv6 = -1;
  if(disable_ipv6 < 0)
    {
      disable_ipv6 = padico_module_attr_disable_ipv6_getvalue();
      if(disable_ipv6)
        {
          padico_warning("IPv6 disabled by user request.\n");
        }
    }
  h_errno = 0;
  if(disable_ipv6 && family == AF_INET6)
    {
      return NULL;
    }
  const struct hostent*he = gethostbyname2(name, family);
  if(!he)
    {
      int count = 5;
      while(he == NULL && h_errno == TRY_AGAIN && count > 0)
        {
          h_errno = 0;
          he = gethostbyname2(name, family);
          count--;
        }
    }
  if(!he)
    {
      /* some resolver are picky with short names without final dot */
      if(strchr(name, '.') == NULL)
        {
          char*tmp_name = padico_malloc(strlen(name) + 2);
          strcpy(tmp_name, name);
          strcat(tmp_name, ".");
          he = gethostbyname2(tmp_name, family);
          padico_free(tmp_name);
        }
    }
  return he;
}

static void topo_detect_link_perf(padico_topo_binding_t b, const padico_topo_hwaddr_t hwaddr)
{
#if defined(HAVE_LINUX_SOCKIOS_H) && defined(HAVE_LINUX_ETHTOOL_H) && defined(HAVE_LINUX_IF_H)
  int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
   if(sock < 0)
     return;
   struct ifreq ifr;
   struct ethtool_cmd edata;
   int rc;
   strncpy(ifr.ifr_name, hwaddr->name, sizeof(ifr.ifr_name));
   ifr.ifr_data = &edata;
   edata.cmd = ETHTOOL_GSET;
   rc = ioctl(sock, SIOCETHTOOL, &ifr);
   if (rc < 0)
     goto close;
   padico_topo_binding_add_perf(b, 300000, edata.speed*1000/8);
 close:
   close(sock);
#endif
}

/** detect network interfaces using Linux getifaddrs, if available
 */
static int topo_inet_addr_detect_getifaddrs(padico_topo_host_t localhost)
{
#ifdef HAVE_IFADDRS_H
  struct ifaddrs*ifa_list = NULL;
  int rc = getifaddrs(&ifa_list);
  if(rc == 0)
    {
      const struct ifaddrs*i;
      for(i = ifa_list; i != NULL; i = i->ifa_next)
        {
          const int is_loopback = !!(i->ifa_flags & IFF_LOOPBACK);
          const int is_up = !!(i->ifa_flags & IFF_UP);
          if (i->ifa_addr && i->ifa_addr->sa_family == AF_INET)
            {
              /* ** detect IPv4 address */
              const struct sockaddr_in*inaddr = (const struct sockaddr_in*)i->ifa_addr;
              padico_topo_addr_visibility_t v = padico_topo_inet_visibility(inaddr->sin_addr.s_addr);
              const uint32_t netmask = ntohl(((struct sockaddr_in*)i->ifa_netmask)->sin_addr.s_addr);
              int prefix_len = 0;
              while((prefix_len < 32) && ((netmask & (0x01UL << (31 - prefix_len))) != 0))
                {
                  prefix_len ++;
                }
              padico_print("address  %8s; inet=%15s/%2d (%s)\n",
                           i->ifa_name, inet_ntoa(inaddr->sin_addr),
                           prefix_len, topo_addr_visibility_name(v));

              if(is_up)
                {
                  padico_topo_hwaddr_t hwaddr = topo_hwaddr_getbyname(localhost, i->ifa_name);
                  const struct hostent*he = topo_gethostbyaddr(&inaddr->sin_addr, sizeof(struct sockaddr_in), AF_INET);
                  if(he)
                    padico_topo_host_add_name(localhost, he->h_name, -1);
                  switch(v)
                    {
                    case PADICO_TOPO_ADDR_VISIBILITY_PUBLIC:
                    case PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL:
                      {
                        padico_topo_binding_t binding =
                          padico_topo_host_bind(localhost, na_topology.inet, &inaddr->sin_addr);
                        if(hwaddr)
                          padico_topo_binding_add_hwaddr(binding, hwaddr);
                        if(he)
                          padico_topo_binding_add_hostname(binding, he->h_name);
                      }
                      break;
                    case PADICO_TOPO_ADDR_VISIBILITY_PRIVATE:
                      {
                        padico_string_t network_name = padico_string_new();
                        const struct in_addr network_addr = { .s_addr = inaddr->sin_addr.s_addr & htonl(netmask) };
                        padico_string_printf(network_name, "inet-[%s/%d]", inet_ntoa(network_addr), prefix_len);
                        padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
                        if(network == NULL)
                          {
                            network = padico_topo_network_create(padico_string_get(network_name),
                                                                 PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
                          }
                        padico_topo_binding_t priv_binding = padico_topo_host_bind(localhost, network, &inaddr->sin_addr);
                        if(hwaddr)
                          {
                            padico_topo_binding_add_hwaddr(priv_binding, hwaddr);
                            topo_detect_link_perf(priv_binding, hwaddr);
                          }
                        if(he)
                          padico_topo_binding_add_hostname(priv_binding, he->h_name);
                        padico_string_delete(network_name);
                      }
                      break;
                    case PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK:
                      {
                        padico_string_t network_name = padico_string_new();
                        padico_string_printf(network_name, "inet-[loopback/%s]", localhost->hostname);
                        padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
                        if(network == NULL)
                          {
                            network = padico_topo_network_create(padico_string_get(network_name),
                                                                 PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
                          }
                        padico_topo_host_bind(localhost, network, &inaddr->sin_addr);
                        padico_string_delete(network_name);
                      }
                      break;
                    case PADICO_TOPO_ADDR_VISIBILITY_NONE:
                    default:
                      padico_warning("not storing address in binding.\n");
                    }
                }
            }
          else if(i->ifa_addr && i->ifa_addr->sa_family == AF_INET6)
            {
              /* ** detect IPv6 address */
              const struct sockaddr_in6*in6addr = (const struct sockaddr_in6*)i->ifa_addr;
              padico_topo_addr_visibility_t v = padico_topo_inet6_visibility(&in6addr->sin6_addr);
              char addr[INET6_ADDRSTRLEN];
              inet_ntop(AF_INET6, &in6addr->sin6_addr, addr, INET6_ADDRSTRLEN);
              padico_print("address  %8s; inet6=%46s (%s)\n", i->ifa_name, addr, topo_addr_visibility_name(v));
              if(is_up && !is_loopback && v == PADICO_TOPO_ADDR_VISIBILITY_PUBLIC)
                {
                  padico_topo_binding_t binding =
                    padico_topo_host_bind(localhost, na_topology.inet6, &in6addr->sin6_addr);
                  padico_topo_hwaddr_t hwaddr = topo_hwaddr_getbyname(localhost, i->ifa_name);
                  if(hwaddr)
                    padico_topo_binding_add_hwaddr(binding, hwaddr);
                }
            }
          else if(i->ifa_addr && i->ifa_addr->sa_family == AF_PACKET)
            {
              /* ** detect hardware address */
              const struct sockaddr_ll*lladdr = (const struct sockaddr_ll*)i->ifa_addr;
              padico_string_t s_addr = padico_string_new();
              int j;
              for(j = 0; j < lladdr->sll_halen; j++)
                {
                  padico_string_catf(s_addr, (j==0)?"%02X":":%02X", lladdr->sll_addr[j]);
                }
              padico_print("hardware %8s; hwaddr=%s; index=%d (%s)\n",
                           i->ifa_name, padico_string_get(s_addr),
                           lladdr->sll_ifindex, topo_hwaddr_type_name(lladdr->sll_hatype));
              padico_string_delete(s_addr);
              if(lladdr->sll_hatype != ARPHRD_LOOPBACK)
                {
                  padico_topo_hwaddr_t hwaddr =
                    padico_topo_hwaddr_create(i->ifa_name, lladdr->sll_ifindex, lladdr->sll_hatype, lladdr->sll_halen, lladdr->sll_addr);
                  padico_topo_hwaddr_add(localhost, &hwaddr);
                }
            }
          else if(i->ifa_addr)
            {
              padico_print("unknown address family (%d)\n", i->ifa_addr->sa_family);
            }
        }
    }
  else
    {
      padico_warning("error while listing network interfaces (getifaddrs: %s).\n", strerror(errno));
      return -1;
    }
  freeifaddrs(ifa_list);
  return 0;
#else /* HAVE_IFADDRS_H */
  return -1;
#endif /* HAVE_IFADDRS_H */
}

/** detect network interfaces using netdevice ioctl
 */
static void topo_inet_addr_detect_siocgifconf(padico_topo_host_t localhost)
{
  const size_t max_ifindex = 32;
  struct ifreq ifreqs[max_ifindex];
  struct ifconf ifc =
    {
      .ifc_len = max_ifindex * sizeof(struct ifreq),
      .ifc_buf = (void*)&ifreqs
    };
  memset(&ifreqs[0], 0, max_ifindex * sizeof(struct ifreq));
  int sockfd = PUK_ABI_FSYS_WRAP(socket)(AF_INET, SOCK_DGRAM, 0); /* socket for netdevice ioctl() */
  if(sockfd < 0)
    {
      padico_warning("error while enumerating addresses.\n");
      return;
    }
  int rc = PUK_ABI_FSYS_WRAP(ioctl)(sockfd, SIOCGIFCONF, &ifc);
  /* warning- SIOCGIFCONF only returns IPv4 addresses */
  if(rc)
    padico_fatal("error %s while enumerating addresses.\n",
                 strerror(__puk_abi_wrap__errno));
  int i;
  for(i = 0; i < ifc.ifc_len / sizeof(struct ifreq); i++)
    {
      struct ifreq*ifr = &ifreqs[i];
      padico_print("IF addr family=%d; if=%s\n",
                   ifr->ifr_addr.sa_family, ifr->ifr_name);
      PUK_ABI_FSYS_WRAP(ioctl)(sockfd, SIOCGIFHWADDR, ifr);
      padico_print("IF hw addr family=%d; if=%s\n",
                   ifr->ifr_addr.sa_family, ifr->ifr_name);
    }
  PUK_ABI_FSYS_WRAP(close)(sockfd);
}

/** detect network interfaces using netlink socket
 */
static int topo_inet_addr_detect_netlink(padico_topo_host_t localhost)
{
#if defined(HAVE_LINUX_NETLINK_H) && defined(HAVE_LINUX_RTNETLINK_H)
  /* create netlink socket */
  int fd = PUK_ABI_FSYS_WRAP(socket)(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
  if(fd < 0)
    {
      padico_warning("cannot open netlink socket (socket: %s).\n", strerror(errno));
      return -1;
    }
  struct sockaddr_nl addr = {
    .nl_family = AF_NETLINK,
    .nl_pad    = 0,
    .nl_pid    = getpid(),
    .nl_groups = 0
  };
  int rc = PUK_ABI_FSYS_WRAP(bind)(fd, (struct sockaddr*)&addr, sizeof(addr));
  if(rc < 0)
    {
      padico_warning("cannot bind netlink socket.\n");
      return -1;
    }
  /* netlink request */
  struct
  {
    struct nlmsghdr nl; /**< netlink header */
    struct ifaddrmsg ifa;
    char            buf[8192];
  } req;
  memset(&req, 0, sizeof(req));
  req.nl.nlmsg_len   = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
  req.nl.nlmsg_type  = RTM_GETADDR;
  req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
  req.nl.nlmsg_seq   = 0;
  req.nl.nlmsg_pid   = getpid();
  req.ifa.ifa_family = AF_INET;
  struct iovec iov = { .iov_base = (void*)&req.nl, .iov_len = req.nl.nlmsg_len };
  /* send request */
  struct sockaddr_nl peer = {
    .nl_family = AF_NETLINK,
    .nl_pad    = 0,
    .nl_pid    = 0, /* 0 = kernel */
    .nl_groups = 0
  };
  struct msghdr msg = {
    .msg_iov     = &iov,
    .msg_iovlen  = 1,
    .msg_name    = (void*)&peer,
    .msg_namelen = sizeof(peer)
  };
  rc = PUK_ABI_FSYS_WRAP(sendmsg)(fd, &msg, 0);
  if(rc < 0)
    padico_fatal("error while sending netlink request.\n");
  /* receive reply */
  char buf[8192];
  memset(buf, 0, sizeof(buf));
  char*p = buf;
  int nll = 0;
  while(1)
    {
      rc = PUK_ABI_FSYS_WRAP(recv)(fd, p, sizeof(buf) - nll, 0);
      struct nlmsghdr*nlp = (struct nlmsghdr*)p;
      if(nlp->nlmsg_type == NLMSG_DONE)
        break;
      p += rc;
      nll += rc;
    }
  /* parse reply */
  const struct nlmsghdr*nlp = (struct nlmsghdr*)buf;
  while(NLMSG_OK(nlp, nll))
    {
      const struct ifaddrmsg*ifa = (struct ifaddrmsg*)NLMSG_DATA(nlp);
      const struct rtattr*rtap = (struct rtattr*)IFA_RTA(ifa);
      int rtl = IFA_PAYLOAD(nlp);
      while(RTA_OK(rtap, rtl))
        {
          switch(rtap->rta_type)
            {
            case IFA_LOCAL:
              {
                if(ifa->ifa_family == AF_INET)
                  {
                    const struct in_addr*inaddr = (const struct in_addr*)RTA_DATA(rtap);
                    const int prefix_len = ifa->ifa_prefixlen;
                    const int ifindex = ifa->ifa_index;
                    padico_topo_addr_visibility_t v = padico_topo_inet_visibility(inaddr->s_addr);
                    padico_print("netlink   index=%d; inet=%15s/%2d (%s)\n",
                                 ifindex, inet_ntoa(*inaddr), prefix_len, topo_addr_visibility_name(v));
                    switch(v)
                      {
                      case PADICO_TOPO_ADDR_VISIBILITY_PUBLIC:
                      case PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL:
                        {
                          padico_topo_host_bind(localhost, na_topology.inet, inaddr);
                        }
                        break;
                      case PADICO_TOPO_ADDR_VISIBILITY_PRIVATE:
                        {
                          padico_warning("TODO- private address\n");
                          padico_topo_host_bind(localhost, na_topology.inet, inaddr);
                          /*
                          padico_string_t network_name = padico_string_new();
                          const struct in_addr network_addr = { .s_addr = inaddr->s_addr & htonl(netmask) };
                          padico_string_printf(network_name, "inet-[%s/%d]", inet_ntoa(network_addr), prefix_len);
                          padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
                          if(network == NULL)
                            {
                              network = padico_topo_network_create(padico_string_get(network_name),
                                                                   PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
                            }
                          padico_topo_binding_t priv_binding = padico_topo_host_bind(localhost, network, &inaddr->sin_addr);
                          padico_string_delete(network_name);
                          */
                        }
                        break;
                      case PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK:
                        padico_warning("TODO- loopback\n");
                        break;
                      case PADICO_TOPO_ADDR_VISIBILITY_NONE:
                      default:
                        padico_warning("not storing address in binding.\n");

                      }
                  }
              }
              break;
            default:
              break;
            }
          rtap = RTA_NEXT(rtap,rtl);
        }
      nlp = NLMSG_NEXT(nlp, nll);
    }


  PUK_ABI_FSYS_WRAP(close)(fd);
  return 0;
#else
  return -1;
#endif /* HAVE_LINUX_NETLINK_H && HAVE_LINUX_RTNETLINK_H */
}

static void topo_inet_addr_detect(padico_topo_host_t localhost)
{
  /* list network interfaces */
  int rc = topo_inet_addr_detect_getifaddrs(localhost);
  if(rc != 0)
    {
      rc = topo_inet_addr_detect_netlink(localhost);
      if(rc != 0)
        {
          topo_inet_addr_detect_siocgifconf(localhost);
        }
    }
}

static void topo_inet_route_detect(padico_topo_host_t localhost)
{
#if defined(HAVE_LINUX_NETLINK_H) && defined(HAVE_LINUX_RTNETLINK_H)
  /* create rtnetlink socket */
  int fd = PUK_ABI_FSYS_WRAP(socket)(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
  if(fd < 0)
    {
      padico_warning("cannot open rtnetlink socket (socket: %s).\n", strerror(errno));
      return;
    }
  struct sockaddr_nl addr = {
    .nl_family = AF_NETLINK,
    .nl_pad    = 0,
    .nl_pid    = getpid(),
    .nl_groups = 0
  };
  int rc = PUK_ABI_FSYS_WRAP(bind)(fd, (struct sockaddr*)&addr, sizeof(addr));
  if(rc < 0)
    padico_fatal("cannot bind rtnetlink socket.\n");
  /* rtnetlink request */
  struct
  {
    struct nlmsghdr nl; /**< netlink header */
    struct rtmsg    rt; /**< rtnetlink request */
    char            buf[8192];
  } req;
  memset(&req, 0, sizeof(req));
  req.nl.nlmsg_len   = NLMSG_LENGTH(sizeof(struct rtmsg));
  req.nl.nlmsg_type  = RTM_GETROUTE;
  req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
  req.nl.nlmsg_seq   = 0;
  req.nl.nlmsg_pid   = getpid();
  req.rt.rtm_family  = AF_INET;
  req.rt.rtm_table   = RT_TABLE_MAIN;
  req.rt.rtm_type    = RTN_UNICAST; /* gateway */
  struct iovec iov = { .iov_base = (void*)&req.nl, .iov_len = req.nl.nlmsg_len };
  /* send request */
  struct sockaddr_nl peer = {
    .nl_family = AF_NETLINK,
    .nl_pad    = 0,
    .nl_pid    = 0, /* 0 = kernel */
    .nl_groups = 0
  };
  struct msghdr msg = {
    .msg_iov     = &iov,
    .msg_iovlen  = 1,
    .msg_name    = (void*)&peer,
    .msg_namelen = sizeof(peer)
  };
  rc = PUK_ABI_FSYS_WRAP(sendmsg)(fd, &msg, 0);
  if(rc < 0)
    padico_fatal("error while sending rtnetlink request.\n");
  /* receive reply */
  char buf[8192];
  memset(buf, 0, sizeof(buf));
  char*p = buf;
  int nll = 0;
  while(1)
    {
      rc = PUK_ABI_FSYS_WRAP(recv)(fd, p, sizeof(buf) - nll, 0);
      struct nlmsghdr*nlp = (struct nlmsghdr*)p;
      if(nlp->nlmsg_type == NLMSG_DONE)
        break;
      p += rc;
      nll += rc;
    }
  /* parse reply */
  const struct nlmsghdr*nlp = (struct nlmsghdr*)buf;
  while(NLMSG_OK(nlp, nll))
    {
      const struct rtmsg*rtp = (struct rtmsg*)NLMSG_DATA(nlp);
      if(rtp->rtm_table == RT_TABLE_MAIN)
        {
          int ifn = -1;
          struct in_addr gw = { .s_addr = INADDR_NONE };
          struct in_addr dest = { .s_addr = INADDR_NONE };
          const struct rtattr*rtap = (struct rtattr*)RTM_RTA(rtp);
          int rtl = RTM_PAYLOAD(nlp);
          while(RTA_OK(rtap, rtl))
            {
              switch(rtap->rta_type)
                {
                case RTA_DST:     /* destination IPv4 address */
                  dest = *(struct in_addr*)RTA_DATA(rtap);
                  break;
                case RTA_GATEWAY: /* gateway IPv4 address */
                  gw = *(struct in_addr*)RTA_DATA(rtap);
                  break;
                case RTA_OIF:     /* outbound interface number */
                  ifn = *(int*)RTA_DATA(rtap);
                default:
                  break;
                }
              rtap = RTA_NEXT(rtap,rtl);
            }
          if(ifn != -1 && gw.s_addr != INADDR_NONE)
            {
              padico_topo_hwaddr_t hwaddr = topo_hwaddr_getbyindex(localhost, ifn);
              char*dest_s = padico_strdup(inet_ntoa(dest));
              padico_print("route    %8s; gw = %15s; dest = %15s/%d\n", hwaddr->name, inet_ntoa(gw), dest_s, rtp->rtm_dst_len);
              struct ifreq ifr;
              struct arpreq arpreq;
              int sockfd = PUK_ABI_FSYS_WRAP(socket)(AF_INET, SOCK_DGRAM, 0);
              ifr.ifr_addr.sa_family = AF_INET;
              strncpy(ifr.ifr_name, hwaddr->name, TOPO_HWADDR_NAMSIZ);
              PUK_ABI_FSYS_WRAP(ioctl)(sockfd, SIOCGIFADDR, &ifr);
              memset(&arpreq, 0, sizeof(struct arpreq));
              struct sockaddr_in*gwaddr = (struct sockaddr_in*)&arpreq.arp_pa;
              memcpy(&gwaddr->sin_addr, &gw, sizeof(struct in_addr));
              gwaddr->sin_family = AF_INET;
              strcpy(arpreq.arp_dev, hwaddr->name);
              padico_topo_hwaddr_t gw_hwaddr = NULL;
              if(PUK_ABI_FSYS_WRAP(ioctl)(sockfd, SIOCGARP, &arpreq) == 0)
                {
                  gw_hwaddr = padico_topo_hwaddr_create("", 0, ARPHRD_ETHER, 6, &arpreq.arp_ha.sa_data[0]);
                }
              PUK_ABI_FSYS_WRAP(close)(sockfd);
              struct in_addr local = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
              padico_topo_addr_visibility_t local_v = padico_topo_inet_visibility(local.s_addr);
              padico_topo_addr_visibility_t dest_v = padico_topo_inet_visibility(dest.s_addr);
              if(local_v == PADICO_TOPO_ADDR_VISIBILITY_PRIVATE)
                {
                  /* find binding for given hwaddr */
                  padico_topo_binding_vect_itor_t b;
                  puk_vect_foreach(b, padico_topo_binding, &localhost->bindings)
                    {
                      padico_topo_binding_t binding = *b;
                      topo_property_vect_itor_t p;
                      puk_vect_foreach(p, topo_property, &binding->props)
                        {
                          if((*p)->kind == TOPO_PROPERTY_HWADDR && (*p)->content.HWADDR.hwaddr == hwaddr)
                            {
                              padico_topo_binding_add_gw_addr(binding, &gw);
                              if(gw_hwaddr)
                                padico_topo_binding_add_gw_hwaddr(binding, gw_hwaddr);
                              if(dest.s_addr == INADDR_NONE)
                                {
                                  padico_topo_network_t network =  binding->network;
                                  if(!network->uplink  && network->family == AF_INET)
                                    {
                                      padico_topo_binding_t uplink = topo_binding_create(na_topology.inet);
                                      uplink->entity.network = network;
                                      uplink->entity.kind = TOPO_ENTITY_NETWORK;
                                      network->uplink = uplink;
                                      padico_topo_binding_add_filter(uplink, PADICO_TOPO_FILTER_BLOCK_IN, PORT_ANY, IPPROTO_TCP);
                                      padico_topo_binding_add_filter(uplink, PADICO_TOPO_FILTER_BLOCK_IN, PORT_ANY, IPPROTO_UDP);
                                    }
                                }
                              if(dest.s_addr != INADDR_NONE && dest_v == PADICO_TOPO_ADDR_VISIBILITY_PRIVATE)
                                {
                                  padico_string_t network_name = padico_string_new();
                                  padico_string_printf(network_name, "inet-[%s/%d]", dest_s, rtp->rtm_dst_len);
                                  padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
                                  if(!network)
                                    {
                                      network = padico_topo_network_create(padico_string_get(network_name),
                                                                           PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
                                      padico_topo_binding_add_route(binding, network, &gw);
                                    }
                                }
                              break; /* props have been modified- iterator is now invalid */
                            }
                        }
                    }
                }
              if(gw_hwaddr != NULL)
                {
                  padico_topo_hwaddr_destroy(gw_hwaddr);
                }
              padico_free(dest_s);
            }
        }
      nlp = NLMSG_NEXT(nlp, nll);
    }
  PUK_ABI_FSYS_WRAP(close)(fd);
#endif
}

static void topo_inet_service_detect(padico_topo_host_t localhost)
{
  unsigned long txq, rxq, retrnsmt, inode, tm;
  int sl, local_port, remote_port, state, tr, uid, timeout;
  char line[2048], local_addr[128], remote_addr[128], end[1024];
  size_t line_len = 0, rc = 0;
  line_len = sizeof(line);
  FILE *tcp_file = fopen("/proc/net/tcp", "r");
  setservent(1);
  if(tcp_file)
    {
      fgets(line, line_len, tcp_file);
      do
        {
          if(!fgets(line, line_len, tcp_file))
            break;
          rc = sscanf(line, "%d: %[0-9A-Fa-f]:%X %64[0-9A-Fa-f]:%X %X %lX:%lX %X:%lX %lX %d %d %ld %512s\n",
                      &sl, local_addr, &local_port, remote_addr, &remote_port, &state, &txq, &rxq, &tr, &tm, &retrnsmt, &uid, &timeout, &inode, end);
          if(rc <= 0)
            break;
          struct in_addr local_inaddr;
          sscanf(local_addr, "%X", &(local_inaddr.s_addr));
          local_inaddr.s_addr = htonl(local_inaddr.s_addr);
          if(state == TCP_LISTEN && local_inaddr.s_addr != INADDR_LOOPBACK && local_port < 1024)
            {
              struct servent*servent = getservbyport(htons(local_port), "tcp");
              if(servent)
                padico_topo_host_add_service(localhost, servent->s_name, ntohs(servent->s_port));
            }
        }
      while(!feof(tcp_file));
      fclose(tcp_file);
    }
  endservent();
}

/* if host have special devices, try load driver */
static void topo_device_detect(void)
{
  if(padico_module_attr_autodetect_getvalue())
    {
      /* ** auto-detect infiniband */
      struct stat s;
      int ret;
      padico_rc_t rc = NULL;
      ret = stat("/dev/infiniband", &s);
      if(!ret && S_ISDIR(s.st_mode))
        {
#ifdef HAVE_IBVERBS
          puk_mod_t mod = NULL;
          rc = padico_puk_mod_resolve(&mod, "InfinibandVerbs");
          if(mod)
            {
              rc = padico_puk_mod_load(mod);
            }
          else
            {
              padico_warning("detected infiniband device but failed to load ibverbs driver.\n");
            }
#else /* HAVE_IBVERBS */
          padico_warning("detected infiniband device but ibverbs drivers not compiled.\n");
#endif /* HAVE_IBVERBS */
        }
      /* ** auto-detect PSM & PSM2 */
      int psm_detected = 0;
      int psm2_detected = 0;
      DIR*d = opendir("/sys/class/infiniband/");
      if(d)
        {
          struct dirent*dir;
          while((dir = readdir(d)) != NULL)
            {
              if(strncmp(dir->d_name, "qib", 3) == 0)
                {
                  psm_detected = 1;
                }
              if(strncmp(dir->d_name, "hfi", 3) == 0)
                {
                  psm2_detected = 1;
                }
            }
          closedir(d);
        }
      if(psm_detected)
        {
          padico_print("detected PSM device.\n");
#ifdef HAVE_PSM
          puk_mod_t mod = NULL;
          rc = padico_puk_mod_resolve(&mod, "PSM-discover");
          if(mod)
            {
              rc = padico_puk_mod_load(mod);
            }
          else
            {
              padico_warning("detected PSM device but failed to load PSM driver.\n");
            }
#else /* HAVE_PSM */
          padico_warning("detected PSM device but psm driver not compiled.\n");
#endif /* HAVE_PSM */
        }
      if(psm2_detected)
        {
          padico_print("detected PSM2 device.\n");
#ifdef HAVE_PSM2
          puk_mod_t mod = NULL;
          rc = padico_puk_mod_resolve(&mod, "PSM2-discover");
          if(mod)
            {
              rc = padico_puk_mod_load(mod);
            }
          else
            {
              padico_warning("detected PSM2 device but failed to load PSM2 driver.\n");
            }
#else /* HAVE_PSM2 */
          padico_warning("detected PSM2 device but psm2 driver not compiled.\n");
#endif /* HAVE_PSM2 */
        }

      /* ** auto-detect BXI */
      ret = stat("/dev/bxi", &s);
      if(!ret && S_ISDIR(s.st_mode))
        {
#ifdef HAVE_PORTALS4
          puk_mod_t mod = NULL;
          rc = padico_puk_mod_resolve(&mod, "Portals4");
          if(mod)
            {
              rc = padico_puk_mod_load(mod);
            }
          else
            {
              padico_warning("detected BXI device but failed to load portals4 driver.\n");
            }
#else /* HAVE_PORTALS4 */
          padico_warning("detected BXI device but portals4 drivers not compiled.\n");
#endif /* HAVE_PORTALS4 */
        }

      /* ** auto-detect Cray CXI (Slingshot) */
      ret = stat("/dev/cxi0", &s);
      if(!ret)
        {
#ifdef HAVE_FABRIC_H
          puk_mod_t mod = NULL;
          rc = padico_puk_mod_resolve(&mod, "libfabric");
          if(mod)
            {
              rc = padico_puk_mod_load(mod);
            }
          else
            {
              padico_warning("detected Cray Slingshot device but failed to load libfabric driver.\n");
            }
#else /* HAVE_FABRIC_H */
          padico_warning("detected Cray Slingshot device but libfabric not detect.\n");
#endif /* HAVE_FABRIC_H */
        }

      /* TODO- add Myrinet detection */
      if(rc)
        padico_rc_delete(rc);
    }
}

padico_topo_addr_visibility_t padico_topo_inet_visibility(in_addr_t _inaddr)
{
  in_addr_t inaddr = ntohl(_inaddr);
  padico_topo_addr_visibility_t v = PADICO_TOPO_ADDR_VISIBILITY_NONE;
  if((inaddr & 0xff000000) == 0x7f000000)           /* 127.0.0.x/8 */
    v = PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK;
  else if( ((inaddr & 0xff000000) == 0x0a000000) || /* 10.x.x.x/8 */
           ((inaddr & 0xfff00000) == 0xac100000) || /* 172.16.x.x/12 */
           ((inaddr & 0xffff0000) == 0xc0a80000) )  /* 192.168.x.x/16 */
    v = PADICO_TOPO_ADDR_VISIBILITY_PRIVATE;
  else if( ((inaddr & 0xffff0000) == 0xa9fe0000) || /* 169.254.x.x/16*/
           ((inaddr & 0xffffff00) == 0xc0000200) )  /* 192.0.2.x/24 */
    v = PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL;
  else
    v = PADICO_TOPO_ADDR_VISIBILITY_PUBLIC;
  return v;
}

padico_topo_addr_visibility_t padico_topo_inet6_visibility(const struct in6_addr*in6addr)
{
  if(memcmp(in6addr, &in6addr_loopback, sizeof(struct in6_addr)) == 0)
    return PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK;
  else if((ntohs(in6addr->s6_addr16[0]) & 0xffc0) == 0xfe80) /* fe80::/10 */
    return PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL;
  else if((ntohs(in6addr->s6_addr16[0]) & 0xfe00) == 0xfc00) /* fc00::/7 */
    return PADICO_TOPO_ADDR_VISIBILITY_PRIVATE;
  else
    return PADICO_TOPO_ADDR_VISIBILITY_PUBLIC;
}

/** inet address visibility pretty print */
static const char*topo_addr_visibility_name(padico_topo_addr_visibility_t v)
{
  switch(v)
    {
    case PADICO_TOPO_ADDR_VISIBILITY_NONE:
      return "none";
    case PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK:
      return "loopback";
    case PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL:
      return "link-local";
    case PADICO_TOPO_ADDR_VISIBILITY_PRIVATE:
      return "private";
    case PADICO_TOPO_ADDR_VISIBILITY_PUBLIC:
      return "public";
    default:
      return "unknown";
    }
}

/** MAC address type pretty print */
static const char*topo_hwaddr_type_name(int type)
{
  switch(type)
    {
    case  ARPHRD_ETHER:
    case ARPHRD_EETHER:
      return "ethernet";
    case ARPHRD_LOOPBACK:
      return "loopback";
#ifdef ARPHRD_ATM
    case ARPHRD_ATM:
      return "atm";
#endif
#ifdef ARPHRD_IEEE1394
    case ARPHRD_IEEE1394:
      return "ieee1394";
#endif
#ifdef ARPHRD_INFINIBAND
    case ARPHRD_INFINIBAND:
      return "infiniband";
#endif
#ifdef ARPHRD_PPP
    case ARPHRD_PPP:
        return "ppp";
#endif
#ifdef ARPHRD_IRDA
    case  ARPHRD_IRDA:
      return "IrDA";
#endif
#ifdef ARPHRD_IEEE80211
    case ARPHRD_IEEE80211:
      return "ieee802.11";
#endif
    default:
      return "unknown";
    }
}

static void topology_populate(void)
{
  /* regular boot env */
  const char*attr_uuid = padico_getenv("PADICO_NODE_UUID");
  const char*boot_id   = padico_getenv("PADICO_BOOT_ID");
  const char*boot_rank = padico_getenv("PADICO_BOOT_RANK");
  /* PMIx */
  const char*pmix_namespace = padico_getenv("PMIX_NAMESPACE");
  const char*pmix_rank = padico_getenv("PMIX_RANK");
  /* PMI2 */
  const char*pmi_job_id = padico_getenv("PMI_JOBID");
  const char*pmi_rank = padico_getenv("PMI_RANK");
  /* Puk sim */
  const char*puk_mult_root = padico_getenv("PUK_MULT_ROOT");
  char puk_mult_rank[16];
  /* retrieve job attributes (job id, rank) */
  if(boot_id != NULL && boot_rank != NULL)
    {
      padico_print("create node from padico-launch attributes.\n");
    }
  else if(pmix_namespace != NULL && pmix_rank != NULL)
    {
      /* try PMIx */
      padico_print("create node from PMIx information; namespace = %s; rank = %s.\n",
                   pmix_namespace, pmix_rank);
      boot_rank = pmix_rank;
      boot_id = pmix_namespace;
    }
  else if(pmi_job_id != NULL && pmi_rank != NULL)
    {
      /* try PMI2 */
      padico_print("create node from PMI/PMI2 information; job_id = %s; rank = %s.\n",
                   pmi_job_id, pmi_rank);
      boot_rank = pmi_rank;
      boot_id = pmi_job_id;
    }
  else if(padico_getenv("SLURM_JOB_ID") != NULL)
    {
      /* slurm without regular env, PMI2, PMIx- oops */
      padico_fatal("detected slurm job, but no padico-launch environment nor PMI2/PMIx environment. Please launch your binary either with padico-launch, srun --mpi=pmi2, or srun --mpi=pmix.\n");
    }
  else if(puk_mult_root != NULL)
    {
      /* simulation */
      boot_id = padico_getenv("PUK_MULT_BINARY");
      int puk_mult_num = puk_mult_getnum();
      boot_rank = &puk_mult_rank[0];
      snprintf(puk_mult_rank, 16, "%d", puk_mult_num);
      padico_print("create node from Puk simulation information; job_id = %s; rank = %s.\n",
                   boot_id, boot_rank);
    }
  else
    {
      /* single node */
      padico_string_t s_boot_id = padico_string_new();
      padico_string_printf(s_boot_id, "%s-%d", padico_hostname(), getpid());
      boot_id = padico_strdup(padico_string_get(s_boot_id));
      padico_string_delete(s_boot_id);
      padico_print("single node: %s.\n", boot_id);
    }
  const int pid = getpid();
  const int rank = boot_rank ? atoi(boot_rank) : 0;

  if(puk_mult_root)
    {
      /* some functions used to detect networks are not re-entrant
       * (rtnetlink, gethostent, etc.)
       * -> switch to exclusive mode when using simulation with multiple
       * nodes per process.
       */
      puk_mult_exclusive_lock();
    }

  /* create localhost */
  const int local_hostname_size = 257;
  char local_hostname[local_hostname_size];
  gethostname(local_hostname, local_hostname_size);
  const struct hostent*he = topo_gethostbyname2(local_hostname, AF_INET);
  padico_topo_host_t localhost = NULL;
  if(he == NULL)
    {
      padico_warning("local hostname = %s is not resolvable (%s).\n", local_hostname, hstrerror(h_errno));
      localhost = padico_topo_host_create(local_hostname);
    }
  else
    {
      localhost = padico_topo_host_create(he->h_name);
      padico_topo_host_add_name(localhost, local_hostname, -1);
      const int uid = getuid();
      struct passwd*login = getpwuid(uid);
      if(login)
        padico_topo_host_add_property(localhost, "login", login->pw_name);
    }

  /* create node name */
  padico_string_t nodename = padico_string_new();
  if(puk_mult_root)
    {
      padico_string_printf(nodename, "%s#%d-%s/%s", localhost->hostname, pid, puk_getsymprefix(), boot_id);
    }
  else
    {
      padico_string_printf(nodename, "%s#%d/%s", localhost->hostname, pid, boot_id);
    }
  padico_print("this node is- %s\n", padico_string_get(nodename));

  /* get (or create) node UUID */
  char*uuid = NULL;
  if(attr_uuid != NULL)
    {
      uuid = padico_strdup(attr_uuid);
    }
  else
    {
      static const char hex_digits[] = "0123456789ABCDEF\0";
      uuid = padico_malloc(sizeof(struct topo_uuid_s));
      uint32_t crc = puk_checksum_softcrc(padico_string_get(nodename), padico_string_size(nodename));
      uuid[0] = hex_digits[(crc & 0x0000000F) >> 0];
      uuid[1] = hex_digits[(crc & 0x000000F0) >> 4];
      uuid[2] = hex_digits[(crc & 0x00000F00) >> 8];
      uuid[3] = hex_digits[(crc & 0x0000F000) >> 12];
      uuid[4] = hex_digits[(crc & 0x000F0000) >> 16];
      uuid[5] = hex_digits[(crc & 0x00F00000) >> 20];
      uuid[6] = hex_digits[(crc & 0x0F000000) >> 24];
      uuid[7] = hex_digits[(crc & 0xF0000000) >> 28];
      srandom(getpid());
      int i;
      for(i = 8 ; i< PADICO_TOPO_UUID_SIZE; i++)
        {
          uuid[i] = hex_digits[random() % 16];
        }
      uuid[PADICO_TOPO_UUID_SIZE] = 0;
    }

  /* create node  */
  na_topology.local_node =
    topo_node_create(padico_string_get(nodename), localhost, uuid, boot_id);
  padico_string_delete(nodename);
  padico_free(uuid);

  /* loopback and session networks */
  padico_string_t s_session = padico_string_new();
  padico_string_printf(s_session, "Session-[%s]", boot_id);
  padico_topo_network_t session = padico_topo_network_create(padico_string_get(s_session), PADICO_TOPO_SCOPE_NODE, sizeof(int), AF_UNSPEC);
  padico_topo_node_bind(na_topology.local_node, session, &rank);
  padico_string_delete(s_session);
  padico_string_t s_hloop = padico_string_new();
  padico_string_printf(s_hloop, "LoopbackHost-[%s]", localhost->hostname);
  padico_topo_network_t hloopback = padico_topo_network_create(padico_string_get(s_hloop), PADICO_TOPO_SCOPE_NODE, sizeof(int), AF_UNSPEC);
  padico_topo_node_bind(na_topology.local_node, hloopback, &pid);
  padico_string_delete(s_hloop);
  padico_string_t s_nloop = padico_string_new();
  padico_string_printf(s_nloop, "LoopbackNode-[%s]", padico_topo_node_getname(na_topology.local_node));
  padico_topo_network_t nloopback = padico_topo_network_create(padico_string_get(s_nloop), PADICO_TOPO_SCOPE_NODE, sizeof(int), AF_UNSPEC);
  padico_topo_node_bind(na_topology.local_node, nloopback, &pid);
  padico_string_delete(s_nloop);

  /* detecte inet features */
  padico_print("this host is %s- detecting networks...\n", localhost->hostname);
  topo_inet_addr_detect(localhost);
  topo_inet_route_detect(localhost);
  topo_inet_service_detect(localhost);

  /* detecte special device */
  topo_device_detect();

  if(puk_mult_root)
    {
      puk_mult_exclusive_unlock();
    }
}

/* ********************************************************* */

/** Create a new 'host' record from a given canonical name
 * @note this function takes ownership of memory pointed by 'fqdn' (supposed to be malloced)
 */
padico_topo_host_t padico_topo_host_create(const char*hostname)
{
  padico_topo_host_t host = padico_malloc(sizeof(struct topo_host_s));
  padico_topo_node_vect_init(&host->nodes);
  padico_topo_binding_vect_init(&host->bindings);
  topo_hwaddr_vect_init(&host->hwaddrs);
  topo_hostname_vect_init(&host->hnames);
  topo_property_vect_init(&host->props);
  host->hostname = padico_strdup(hostname);
  padico_topo_host_add_name(host, hostname, -1);
  padico_print("creating host %s.\n", host->hostname);
  return host;
}

static void topo_host_destroy(padico_topo_host_t host)
{
  padico_topo_node_vect_destroy(&host->nodes);
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      topo_binding_destroy(*i);
    }
  padico_topo_binding_vect_destroy(&host->bindings);
  topo_hwaddr_vect_itor_t h;
  puk_vect_foreach(h, topo_hwaddr, &host->hwaddrs)
    {
      padico_free((void*)*h);
    }
  topo_hwaddr_vect_destroy(&host->hwaddrs);
  topo_hostname_vect_itor_t n;
  puk_vect_foreach(n, topo_hostname, &host->hnames)
    {
      padico_free((*n)->name);
      padico_free((void*)*n);
    }
  topo_hostname_vect_destroy(&host->hnames);
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &host->props)
    {
      topo_property_destroy(*p);
    }
  topo_property_vect_destroy(&host->props);
  padico_free((void*)host->hostname);
  padico_free(host);
}

padico_topo_host_t topo_host_lookup(const char*hostname)
{
  padico_topo_host_t host = puk_hashtable_lookup(na_topology.hostsbyname, hostname);
  if(!host)
    {
      if(strcmp(hostname, "localhost") == 0 ||
         strcmp(hostname, "ip6-localhost") == 0 ||
         strcmp(hostname, "ip6-loopback") == 0)
        {
          host = padico_topo_getlocalhost();
        }
    }
  return host;
}

void padico_topo_host_add_name(padico_topo_host_t host, const char*_name, int resolvable)
{
  if(strcmp(_name, "localhost") == 0)
    {
      return;
    }
  if(strcmp(_name, "") == 0)
    {
      padico_warning("trying to add empty hostname.\n");
      return;
    }
  topo_hostname_vect_itor_t i;
  puk_vect_foreach(i, topo_hostname, &host->hnames)
    {
      if(strcmp((*i)->name, _name) == 0)
        return;
    }
  if(resolvable == -1)
    {
      if(padico_module_attr_disable_dns_getvalue())
        {
          padico_warning("DNS query disabled by user request.\n");
          resolvable = 0;
        }
      else
        {
          unsigned char reply[1024];
          int len = 1024;
          int rc = res_init();
          if(rc != 0)
            padico_warning("cannot initialize resovler.\n");
          rc = res_query(_name, ns_c_in, ns_t_a, reply, len);
          if(rc == -1)
            resolvable = 0;
          else
            resolvable = 1;
        }
    }
  struct topo_hostname_s*hname = padico_malloc(sizeof(struct topo_hostname_s));
  hname->name = padico_strdup(_name);
  hname->resolvable = resolvable;
  topo_hostname_vect_push_back(&host->hnames, hname);
  puk_hashtable_insert(na_topology.hostsbyname, hname->name, host);
}

void padico_topo_host_add_service(padico_topo_host_t host, const char*service_name, uint16_t service_port)
{
  struct topo_property_s*prop;
  topo_property_vect_itor_t p;
  size_t name_len = strlen(service_name);
  puk_vect_foreach(p, topo_property, &host->props)
    {
      prop = *p;
      if(prop->kind == TOPO_PROPERTY_SERVICE)
        {
          padico_topo_service_t service = &prop->content.SERVICE.service;
          if(!strcmp(service->name, service_name)
             && service_port == service->port)
            return;
        }
    }
  prop = padico_malloc(sizeof(struct topo_property_s) + name_len + 1);
  prop->kind = TOPO_PROPERTY_SERVICE;
  padico_topo_service_t service = &prop->content.SERVICE.service;
  memcpy(&service->name[0], service_name, name_len);
  service->name[name_len] = 0;
  service->port = service_port;
  topo_property_vect_push_back(&host->props, prop);
}

void padico_topo_host_add_property(padico_topo_host_t host, const char*label, const char*value)
{
  const char*find = padico_topo_host_getproperty(host, label);
  if(!find || (find && strcmp(find, value)))
    {
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_MISC;
      prop->content.MISC.label = padico_strdup(label);
      prop->content.MISC.value = padico_strdup(value);
      topo_property_vect_push_back(&host->props, prop);
    }
}

/* creates a binding between a host and a network */
padico_topo_binding_t padico_topo_host_bind(padico_topo_host_t host, padico_topo_network_t network, const void*address)
{
  padico_topo_binding_t binding = topo_binding_create(network);
  padico_topo_binding_add_addr(binding, address);
  topo_binding_bind_host(&binding, host);
  return binding;
}

/* creates a binding between a node and a network */
padico_topo_binding_t padico_topo_node_bind(padico_topo_node_t node, padico_topo_network_t network, const void*address)
{
  padico_topo_binding_t binding = topo_binding_create(network);
  padico_topo_binding_add_addr(binding, address);
  topo_binding_bind_node(&binding, node);
  return binding;
}

void topo_binding_destroy(struct topo_binding_s*binding)
{
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      topo_property_destroy(*p);
    }
  topo_property_vect_destroy(&binding->props);
  padico_free(binding);
}

void topo_property_destroy(struct topo_property_s*prop)
{
  switch(prop->kind)
    {
    case TOPO_PROPERTY_HWADDR:
      /* hwaddr belong to host; don't free here */
      break;
    case TOPO_PROPERTY_HOSTNAME:
      padico_free(prop->content.HOSTNAME.name);
      break;
    case TOPO_PROPERTY_MISC:
      padico_free(prop->content.MISC.label);
      padico_free(prop->content.MISC.value);
      break;
    default:
      break;
    }
  padico_free(prop);
}

/** merge properties from b2 into 'binding', then destroy b2 */
void topo_binding_merge(padico_topo_binding_t binding, padico_topo_binding_t b2)
{
  assert(binding->network == b2->network);
  topo_property_vect_itor_t p1, p2;
  puk_vect_foreach(p2, topo_property, &b2->props)
    {
      int found = 0;
      struct topo_property_s*prop2 = *p2;
      /* check whether props from 'b2' are already present in 'binding' */
      puk_vect_foreach(p1, topo_property, &binding->props)
        {
          struct topo_property_s*prop1 = *p1;
          if(prop1->kind == prop2->kind)
            {
              switch(prop1->kind)
                {
                case TOPO_PROPERTY_GW_ADDR:
                case TOPO_PROPERTY_ADDR:
                  found |= (memcmp(prop1->content.ADDR.addr._addr_storage, prop2->content.ADDR.addr._addr_storage,
                                   binding->network->addr_size) == 0);
                  break;
                case TOPO_PROPERTY_GW_HWADDR:
                case TOPO_PROPERTY_HWADDR:
                  found |= (prop1->content.HWADDR.hwaddr == prop2->content.HWADDR.hwaddr);
                  break;
                case TOPO_PROPERTY_GW_HOST:
                  found |= (prop1->content.GW_HOST.gw_host == prop2->content.GW_HOST.gw_host);
                  break;
                case TOPO_PROPERTY_FILTER:
                  found |= ((prop1->content.FILTER.filter.port == prop2->content.FILTER.filter.port) &&
                            (prop1->content.FILTER.filter.policy == prop2->content.FILTER.filter.policy));
                  break;
                case TOPO_PROPERTY_SERVICE:
                  found |= ((prop1->content.SERVICE.service.port == prop2->content.SERVICE.service.port) &&
                            (strcmp(prop1->content.SERVICE.service.name, prop2->content.SERVICE.service.name) == 0));
                  break;
                case TOPO_PROPERTY_HOSTNAME:
                  found |= (!strcmp(prop1->content.HOSTNAME.name, prop2->content.HOSTNAME.name));
                  break;
                case TOPO_PROPERTY_ROUTE:
                  found |= (prop1->content.ROUTE.dest == prop2->content.ROUTE.dest &&
                            !memcmp(prop1->content.ROUTE.gw._addr_storage, prop2->content.ROUTE.gw._addr_storage,
                                    binding->network->addr_size));
                  break;
                case TOPO_PROPERTY_PERF:
                  found |= ((prop1->content.PERF.perf.lat == prop2->content.PERF.perf.lat) &&
                            (prop1->content.PERF.perf.bw == prop2->content.PERF.perf.bw));
                  break;
                case TOPO_PROPERTY_INDEX:
                  found |= (prop1->content.INDEX.index == prop2->content.INDEX.index);
                  break;
                case TOPO_PROPERTY_MISC:
                  found |= ( (strcmp(prop1->content.MISC.label, prop2->content.MISC.label) == 0) &&
                             (strcmp(prop1->content.MISC.value, prop2->content.MISC.value) == 0) );
                  break;
                default:
                  padico_fatal("topo_binding_merge()- unknown property type.\n");
                  break;
                }
            }
        }
      if(!found)
        {
          topo_property_vect_push_back(&binding->props, prop2);
        }
      else
        {
          topo_property_destroy(prop2);
        }
      *p2 = NULL;
    }
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &b2->network->bindings)
    {
      if (*b == b2){
        padico_topo_binding_vect_erase(&b2->network->bindings, b);
        break;
      }
    }
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &b2->props)
    {
      if(*p != NULL)
        topo_property_destroy(*p);
    }
  topo_property_vect_destroy(&b2->props);
  padico_free(b2);
}

void topo_binding_bind_host(padico_topo_binding_t*_binding, padico_topo_host_t host)
{
  padico_topo_binding_t binding = *_binding;
  assert(binding != NULL);
  /* resolve hwaddr stored by name */
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &binding->props)
    {
      if(((*p)->kind == TOPO_PROPERTY_HWADDR) && ((*p)->content.HWADDR.hwaddr == NULL))
        {
          padico_topo_hwaddr_t hwaddr = topo_hwaddr_getbyname(host, (*p)->content.HWADDR.ifname);
          (*p)->content.HWADDR.hwaddr = hwaddr;
        }
    }
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &host->bindings)
    {
      if((*b)->network == binding->network)
        {
          const struct topo_address_s*addr1 = topo_binding_getaddr(binding);
          const struct topo_address_s*addr2 = topo_binding_getaddr(*b);
          if(topo_address_eq(addr1, addr2))
            {
              topo_binding_merge(*b, binding);
              *_binding = *b;
              return; /* avoid double-binding */
            }
        }
    }
  assert(binding->network->scope == PADICO_TOPO_SCOPE_HOST);
  binding->entity.host = host;
  binding->entity.kind = TOPO_ENTITY_HOST;
  padico_topo_binding_vect_push_back(&host->bindings, binding);
}

void topo_binding_bind_node(padico_topo_binding_t*_binding, padico_topo_node_t node)
{
  padico_topo_binding_t binding = *_binding;
  assert(binding != NULL);
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &node->bindings)
    {
      if((*b)->network == binding->network)
        {
          const struct topo_address_s*addr1 = topo_binding_getaddr(binding);
          const struct topo_address_s*addr2 = topo_binding_getaddr(*b);
          if(topo_address_eq(addr1, addr2))
            {
              topo_binding_merge(*b, binding);
              *_binding = *b;
              return; /* avoid double-binding */
            }
        }
    }
  assert(binding->network->scope == PADICO_TOPO_SCOPE_NODE);
  binding->entity.node = node;
  binding->entity.kind = TOPO_ENTITY_NONE;
  padico_topo_binding_vect_push_back(&node->bindings, binding);
}


/** Create a new node, either locally or received as XML description
 */
padico_topo_node_t topo_node_create(const char*name,
                                  padico_topo_host_t host,
                                  const char*uuid,
                                  const char*boot_id)
{
  padico_topo_node_t node = padico_malloc(sizeof(struct topo_node_s));;
  padico_print("new node- [%s] %s\n", uuid, name);
  /* create a node */
  padico_topo_binding_vect_init(&node->bindings);
  padico_topo_node_vect_push_back(&host->nodes, node);
  node->host               = host;
  node->canonical_nodename = padico_strdup(name);
  node->boot_id            = padico_strdup(boot_id);
  assert(uuid != NULL);
  assert(strlen(uuid) == PADICO_TOPO_UUID_SIZE);
  memcpy(node->uuid.bytes, uuid, PADICO_TOPO_UUID_SIZE);
  node->uuid.zero = 0;
  padico_topo_node_t node_by_uuid = padico_topo_getnodebyuuid(&node->uuid);
  if(node_by_uuid != NULL)
    {
      padico_fatal("uuid = %s colision; old node = %s; new node = %s\n",
                   uuid, name, node_by_uuid->canonical_nodename);
    }
  puk_hashtable_insert(na_topology.node_table, node->uuid.bytes, node);
  puk_hashtable_insert(na_topology.nodesbyname, node->canonical_nodename, node);
  return node;
}

void topo_node_destroy(padico_topo_node_t node)
{
  padico_free((void*)node->canonical_nodename);
  padico_free((void*)node->boot_id);
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &node->bindings)
    {
      topo_binding_destroy(*i);
    }
  padico_topo_binding_vect_destroy(&node->bindings);
  padico_free(node);
}

padico_topo_network_t padico_topo_network_create(const char*id, padico_topo_binding_scope_t scope, size_t addr_size, int family)
{
  padico_topo_network_t network = padico_topo_network_getbyid(id);
  if(network != NULL)
    {
      padico_warning("duplicate declaration for network %s\n", id);
      return network;
    }
  network = padico_malloc(sizeof(struct topo_network_s));
  assert(scope == PADICO_TOPO_SCOPE_NODE || scope == PADICO_TOPO_SCOPE_HOST);
  padico_topo_binding_vect_init(&network->bindings);
  network->bindings_by_addr = puk_hashtable_new(&topo_address_hash, &topo_address_eq);
  network->network_id = padico_strdup(id);
  network->scope = scope;
  network->addr_size = addr_size;
  network->family = family;
  network->uplink = NULL;
  network->perf.lat = 0;
  network->perf.bw = 0;
  padico_topo_network_vect_push_back(na_topology.networks, network);
  return network;
}

static void topo_network_destroy(padico_topo_network_t network)
{
  padico_topo_binding_vect_destroy(&network->bindings); /* don't free bindings here; they belong to node/host */
  puk_hashtable_delete(network->bindings_by_addr, NULL);
  padico_free((void*)network->network_id);
  if(network->uplink)
    {
      topo_binding_destroy(network->uplink);
    }
  padico_free(network);
}

/* *** Topology inspectors ********************************* */


/* *** local info */

padico_topo_node_t padico_topo_getlocalnode(void)
{
  return na_topology.local_node;
}

/* *** node inspectors */

padico_topo_address_t padico_topo_node_getaddress(padico_topo_node_t node, padico_topo_network_t network)
{
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &node->bindings)
    {
      const struct topo_binding_s*const binding = *i;
      if(binding->network == network && network->scope == PADICO_TOPO_SCOPE_NODE)
        {
          return topo_binding_getaddr(binding);
        }
    }
  return NULL;
}

padico_topo_uuid_t padico_topo_node_getuuid(padico_topo_node_t node)
{
  return &node->uuid;
}

const char*padico_topo_node_getname(padico_topo_node_t node)
{
  return node->canonical_nodename;
}

const char*padico_topo_node_getsession(padico_topo_node_t node)
{
  return node->boot_id;
}

padico_topo_host_t padico_topo_node_gethost(padico_topo_node_t node)
{
  return node->host;
}

void padico_topo_node_getnetworks(padico_topo_node_t node, padico_topo_network_vect_t networks)
{
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &node->bindings)
    {
      padico_topo_network_vect_push_back(networks, (*b)->network);
    }
}


/* *** host inspector */

const char*padico_topo_host_getname(padico_topo_host_t host)
{
  return host->hostname;
}

const char*padico_topo_host_getname2(padico_topo_host_t host, padico_topo_network_t network)
{
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      if((*i)->network == network)
        {
          topo_property_vect_itor_t p;
          puk_vect_foreach(p, topo_property, &(*i)->props)
            {
              struct topo_property_s*prop = *p;
              if(prop->kind == TOPO_PROPERTY_HOSTNAME)
                {
                  return prop->content.HOSTNAME.name;
                }
            }
        }
    }
  topo_hostname_vect_itor_t h;
  puk_vect_foreach(h, topo_hostname, &host->hnames)
    {
      if((*h)->resolvable)
        return (*h)->name;
    }
  const struct in_addr*addr = padico_topo_host_getaddr2(host, network);
  return inet_ntoa(*addr);
}

const struct in_addr*padico_topo_host_getaddr(padico_topo_host_t host)
{
  const struct in_addr*inaddr = NULL;
  padico_topo_addr_visibility_t max_v = PADICO_TOPO_ADDR_VISIBILITY_NONE;
  padico_topo_binding_vect_itor_t i;
  /* find a binding for network 'inet' */
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      if((*i)->network == na_topology.inet)
        {
          const struct in_addr*_inaddr = topo_address_as_inaddr(topo_binding_getaddr(*i));
          padico_topo_addr_visibility_t v = padico_topo_inet_visibility(_inaddr->s_addr);
          if(v > max_v)
            {
              inaddr = _inaddr;
              max_v = v;
            }
        }
    }
  /* in case of failure, find a binding for any AF_INET network */
  if(!inaddr)
    {
      puk_vect_foreach(i, padico_topo_binding, &host->bindings)
        {
          if((*i)->network->family == AF_INET)
            {
              const struct in_addr*_inaddr = topo_address_as_inaddr(topo_binding_getaddr(*i));
              padico_topo_addr_visibility_t v = padico_topo_inet_visibility(_inaddr->s_addr);
              if(v > max_v)
                {
                  inaddr = _inaddr;
                  max_v = v;
                }
            }
        }
    }
  if(!inaddr)
    {
      padico_warning("host %s has no inet address.\n", host->hostname);
    }
  return inaddr;
}

padico_topo_binding_vect_t padico_topo_host_getbindings(padico_topo_host_t host, padico_topo_network_t network)
{
  padico_topo_binding_vect_t b = padico_topo_binding_vect_new();
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      if((*i)->network == network)
        {
          padico_topo_binding_vect_push_back(b, *i);
        }
    }
  return b;
}

const struct in_addr*padico_topo_host_getaddr2(padico_topo_host_t host, padico_topo_network_t network)
{
  const struct in_addr*inaddr = NULL;
  padico_topo_binding_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      if((*i)->network == network &&
         (*i)->network->family == AF_INET)
        {
          inaddr = topo_address_as_inaddr(topo_binding_getaddr(*i));
        }
    }
  return inaddr;
}

padico_topo_node_vect_t padico_topo_host_getnodes(padico_topo_host_t host)
{
  return &host->nodes;
}

void padico_topo_host_getnetworks(padico_topo_host_t host, padico_topo_network_vect_t networks)
{
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &host->bindings)
    {
      padico_topo_network_t network = (*b)->network;
      padico_topo_network_vect_push_back(networks, network);
      while(network->uplink != NULL)
        {
          network = network->uplink->network;
          padico_topo_network_vect_push_back(networks, network);
        }
    }
}

static padico_topo_filter_policy_t padico_topo_host_policy(topo_net_entity_t entity, padico_topo_network_t network, uint16_t port, int protocol)
{
  padico_topo_filter_policy_t policy = 0;
  if((network->family == AF_INET || network->family == AF_INET6) && protocol)
    {
      /* find binding between host and network */
      padico_topo_binding_t binding = NULL;
      if(entity.kind == TOPO_ENTITY_HOST)
        {
          padico_topo_host_t host = entity.host;
          padico_topo_binding_vect_itor_t b;
          puk_vect_foreach(b, padico_topo_binding, &host->bindings)
            {
              padico_topo_network_t network2 = (*b)->network;
              if(network2 == network)
                {
                  binding = *b;
                  break;
                }
              while(network2->uplink != NULL)
                {
                  if(network2->uplink->network == network)
                    {
                      binding = network2->uplink;
                      break;
                    }
                  else
                    {
                      network2 = network2->uplink->network;
                    }
                }
            }
        }
      else if(entity.kind == TOPO_ENTITY_NETWORK)
        {
          if(entity.network->uplink != NULL && entity.network->uplink->network == network)
            binding = entity.network->uplink;
        }
      /* compute policy from binding */
      if(binding)
        {
          topo_property_vect_itor_t p;
          puk_vect_foreach(p, topo_property, &binding->props)
            {
              const struct topo_property_s*prop = *p;
              if((prop->kind == TOPO_PROPERTY_FILTER)
                 &&
                 (prop->content.FILTER.filter.protocol == protocol)
                 &&
                 (prop->content.FILTER.filter.port == port ||
                  prop->content.FILTER.filter.port == PORT_ANY))
                {
                  if( ((prop->content.FILTER.filter.policy & PADICO_TOPO_FILTER_MASK_OUT) && ((policy & PADICO_TOPO_FILTER_MASK_OUT) == 0))
                      ||
                      ((prop->content.FILTER.filter.policy & PADICO_TOPO_FILTER_MASK_IN) && ((policy & PADICO_TOPO_FILTER_MASK_IN) == 0)))
                    {
                      policy |= prop->content.FILTER.filter.policy;
                    }
                }
            }
          /* apply default policy */
          if(!(policy & PADICO_TOPO_FILTER_MASK_IN))
            policy |= PADICO_TOPO_FILTER_ALLOW_IN;
          if(!(policy & PADICO_TOPO_FILTER_MASK_OUT))
            policy |= PADICO_TOPO_FILTER_ALLOW_OUT;
        }
    }
  else
    {
      /* default policy for non-IP network */
      policy |= PADICO_TOPO_FILTER_ALLOW_IN;
      policy |= PADICO_TOPO_FILTER_ALLOW_OUT;
    }
  return policy;
}


int padico_topo_host_can_input(padico_topo_host_t host, padico_topo_network_t network, uint16_t port, int protocol)
{
  struct topo_net_entity_s e;
  e.kind = TOPO_ENTITY_HOST;
  e.host = host;
  return !!(padico_topo_host_policy(e, network, port, protocol) & PADICO_TOPO_FILTER_ALLOW_IN);
}

int padico_topo_host_can_output(padico_topo_host_t host, padico_topo_network_t network, uint16_t port, int protocol)
{
  struct topo_net_entity_s e;
  e.kind = TOPO_ENTITY_HOST;
  e.host = host;
  return !!(padico_topo_host_policy(e, network, port, protocol) & PADICO_TOPO_FILTER_ALLOW_OUT);
}

int padico_topo_network_can_input(padico_topo_network_t subnet, padico_topo_network_t network, uint16_t port, int protocol)
{
  struct topo_net_entity_s e;
  e.kind = TOPO_ENTITY_NETWORK;
  e.network = subnet;
  return !!(padico_topo_host_policy(e, network, port, protocol) & PADICO_TOPO_FILTER_ALLOW_IN);
}

int padico_topo_network_can_output(padico_topo_network_t subnet, padico_topo_network_t network, uint16_t port, int protocol)
{
  struct topo_net_entity_s e;
  e.kind = TOPO_ENTITY_NETWORK;
  e.network = subnet;
  return !!(padico_topo_host_policy(e, network, port, protocol) & PADICO_TOPO_FILTER_ALLOW_OUT);
}

int padico_topo_host_getservice(padico_topo_host_t host, char*service_name)
{
  struct topo_property_s*prop;
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &host->props)
    {
      prop = *p;
      if(prop->kind == TOPO_PROPERTY_SERVICE)
        {
          padico_topo_service_t service = &prop->content.SERVICE.service;
          if(!strcmp(service->name, service_name))
            return service->port;
        }
    }
  return 0;
}

const char*padico_topo_host_getproperty(padico_topo_host_t host, const char*label)
{
  struct topo_property_s*prop;
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, &host->props)
    {
      prop = *p;
      if(prop->kind == TOPO_PROPERTY_MISC)
        {
          if(!strcmp((*p)->content.MISC.label, label))
            return (*p)->content.MISC.value;
        }
    }
  return NULL;
}

padico_topo_host_vect_t padico_topo_gethosts(void)
{
  padico_topo_host_vect_t hosts = padico_topo_host_vect_new();
  puk_hashtable_enumerator_t e;
  char*name;
  e = puk_hashtable_enumerator_new(na_topology.hostsbyname);
  padico_topo_host_t host;
  puk_hashtable_enumerator_next2(e, &name, &host);
  while(host)
    {
      if(strcmp(name, host->hostname) == 0)
        {
          padico_topo_host_vect_push_back(hosts, host);
        }
      puk_hashtable_enumerator_next2(e, &name, &host);
    }
  puk_hashtable_enumerator_delete(e);
  return hosts;
}

/* *** network inspector */

const char*padico_topo_network_getname(padico_topo_network_t network)
{
  return network->network_id;
}
int padico_topo_network_size(padico_topo_network_t network)
{
  return padico_topo_binding_vect_size(&network->bindings);
}
static inline int padico_topo_network_inet_info(const padico_topo_network_t network, struct in_addr*addr, int*cidr)
{
  char addr_s[16];
  memset(addr_s, 0, 16);
  if(sscanf(network->network_id, "inet-[%[^/]/%d]", addr_s, cidr) != 2)
    return 0;
  return inet_pton(AF_INET, addr_s, addr);
}
static inline int na_bindings_vect_contains_network(struct padico_topo_binding_vect_s*bindings, padico_topo_network_t network)
{
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, bindings)
    {
      if((*b)->network == network)
        return 1;
    }
  return 0;
}
int padico_topo_network_contains_node(padico_topo_network_t network, padico_topo_node_t node)
{
  return na_bindings_vect_contains_network(&node->bindings, network);
}
int padico_topo_network_contains_host(padico_topo_network_t network, padico_topo_host_t host)
{
  return na_bindings_vect_contains_network(&host->bindings, network);
}

static topo_net_entity_t na_network_getentity_byaddr(padico_topo_network_t network, const void*_addr)
{
  /*
  struct topo_binding_s key;
  void*addr = topo_binding_getaddr(&key);
  key.network = network;
  memcpy(addr, _addr, network->addr_size);
  struct topo_binding_s*binding = puk_hashtable_lookup(network->bindings, addr);
  return binding?binding->entity:(topo_net_entity_t){ .as_ptr = NULL};
  */
  /* TODO- linear search for now, since hashing variable-length addresses is broken */
#warning TODO
  padico_topo_binding_vect_itor_t b;
  puk_vect_foreach(b, padico_topo_binding, &network->bindings)
    {
      const struct topo_address_s*addr = topo_binding_getaddr(*b);
      if(addr && (memcmp(addr->_addr_storage, _addr, network->addr_size) == 0))
        {
          return (*b)->entity;
        }
    }
  topo_net_entity_t e;
  e.as_ptr = NULL;
  return e;
}

void padico_topo_network_gethosts(padico_topo_network_t n, padico_topo_host_vect_t hosts)
{
  if(n->scope == PADICO_TOPO_SCOPE_HOST)
    {
      padico_topo_binding_vect_itor_t b;
      puk_vect_foreach(b, padico_topo_binding, &n->bindings)
        {
          const topo_net_entity_t e = (*b)->entity;
          if(e.kind == TOPO_ENTITY_HOST)
            {
              padico_topo_host_vect_push_back(hosts, e.host);
            }
        }
    }
}

void padico_topo_network_get_common_host(padico_topo_network_t n1, padico_topo_network_t n2, padico_topo_host_vect_t hosts)
{
  if(n1->scope == PADICO_TOPO_SCOPE_HOST && n2->scope == PADICO_TOPO_SCOPE_HOST)
    {
      padico_topo_binding_vect_itor_t b1;
      puk_vect_foreach(b1, padico_topo_binding, &n1->bindings)
        {
          const topo_net_entity_t e1 = (*b1)->entity;
          if(e1.kind == TOPO_ENTITY_HOST)
            {
              padico_topo_binding_vect_itor_t b2;
              puk_vect_foreach(b2, padico_topo_binding, &n2->bindings)
                {
                  const topo_net_entity_t e2 = (*b2)->entity;
                  if(e2.kind == TOPO_ENTITY_HOST && e1.host == e2.host)
                    {
                      padico_topo_host_vect_push_back(hosts, e1.host);
                    }
                }
            }
        }
    }
}

void padico_topo_network_set_perf(padico_topo_network_t n, int latency_nsec, int bandwidth_kbps)
{
  assert(n != NULL);
  if(latency_nsec > 0)
    n->perf.lat = latency_nsec;
  if(bandwidth_kbps > 0)
    n->perf.bw = bandwidth_kbps;
}

void padico_topo_network_get_perf(padico_topo_network_t n, int*latency_nsec, int*bandwidth_kbps)
{
  if(latency_nsec != NULL)
    *latency_nsec = n->perf.lat;
  if(bandwidth_kbps != NULL)
    *bandwidth_kbps = n->perf.bw;
}


const void*padico_topo_address_getbytes(padico_topo_address_t address)
{
  return &address->_addr_storage[0];
}

/* *** lookup */

/* network lookup */

padico_topo_network_t padico_topo_network_getbyid(const char*network_id)
{
  int i;
  for(i = 0; i < padico_topo_network_vect_size(na_topology.networks); i++)
    {
      padico_topo_network_t network = padico_topo_network_vect_at(na_topology.networks, i);
      if(strcmp(network->network_id, network_id) == 0)
        {
          return network;
        }
    }
  return NULL;
}

padico_topo_network_vect_t padico_topo_networks_getbyfamily(int family)
{
  padico_topo_network_vect_t networks = padico_topo_network_vect_new();
  int i;
  for(i = 0; i < padico_topo_network_vect_size(na_topology.networks); i++)
    {
      padico_topo_network_t n = padico_topo_network_vect_at(na_topology.networks, i);
      if(n->family == family)
        {
          padico_topo_network_vect_push_back(networks, n);
        }
    }
  return networks;
}

/* node lookup */

padico_topo_node_t padico_topo_network_getnodebyaddr(padico_topo_network_t network, const void*_addr)
{
  assert(network->scope == PADICO_TOPO_SCOPE_NODE);
  const topo_net_entity_t entity = na_network_getentity_byaddr(network, _addr);
  return entity.node;
}

padico_topo_node_t padico_topo_getnodebyname(const char*nodename)
{
  padico_out(50, "looking up node with name: %s\n", nodename);
  padico_topo_node_t node = puk_hashtable_lookup(na_topology.nodesbyname, nodename);
  if(node)
    {
      padico_out(50, "node found: %s\n", nodename);
      return node;
    }
  padico_out(50, "no node found. Trying hosts.\n");
  padico_topo_host_t host = topo_host_lookup(nodename);
  if(host)
    {
      return padico_topo_host_getnode(host);
    }
  return NULL;
}

padico_topo_node_t padico_topo_getnodebyuuid(padico_topo_uuid_t uuid)
{
  return puk_hashtable_lookup(na_topology.node_table, uuid);
}

/* host lookup */

padico_topo_host_t padico_topo_network_gethostbyaddr(padico_topo_network_t network, const void*_addr)
{
  if (network->scope == PADICO_TOPO_SCOPE_HOST)
    {
      const topo_net_entity_t entity = na_network_getentity_byaddr(network, _addr);
      return entity.host;
    }
  return NULL;
}

padico_topo_host_t padico_topo_host_getbyinaddr(const struct in_addr*addr)
{
  padico_topo_network_vect_t inet_networks = padico_topo_networks_getbyfamily(AF_INET);
  int i;
  for(i = 0; i < padico_topo_network_vect_size(inet_networks); i++)
    {
      padico_topo_network_t net = padico_topo_network_vect_at(inet_networks, i);
      const padico_topo_host_t host = padico_topo_network_gethostbyaddr(net, addr);
      if(host != NULL)
        return host;
    }
  return NULL;
}

padico_topo_host_t padico_topo_host_getbyin6addr(const struct in6_addr*addr)
{
  padico_topo_network_vect_t inet_networks = padico_topo_networks_getbyfamily(AF_INET6);
  int i;
  for(i = 0; i < padico_topo_network_vect_size(inet_networks); i++)
    {
      padico_topo_network_t net = padico_topo_network_vect_at(inet_networks, i);
      const padico_topo_host_t host = padico_topo_network_gethostbyaddr(net, addr);
      if(host != NULL)
        return host;
    }
  return NULL;
}

padico_topo_host_t padico_topo_host_resolve_byinaddr(const struct in_addr*addr)
{
  padico_out(50, "resolving up host with addr: %s\n", inet_ntoa(*addr));
  padico_topo_host_t host = padico_topo_host_getbyinaddr(addr);
  if(!host)
    {
      /* no host in database with this address- perform a full resolve */
      char s_addr[INET_ADDRSTRLEN];
      inet_ntop(AF_INET, addr, s_addr, INET_ADDRSTRLEN);
      host = padico_topo_host_resolvebyname(s_addr);
    }
  return host;
}

padico_topo_host_t padico_topo_host_resolve_byin6addr(const struct in6_addr*addr)
{
  padico_topo_host_t host = padico_topo_host_getbyin6addr(addr);
  if(!host)
    {
      /* no host in database with this address- perform a full resolve */
      char s_addr[INET6_ADDRSTRLEN];
      inet_ntop(AF_INET6, addr, s_addr, INET6_ADDRSTRLEN);
      host = padico_topo_host_resolvebyname(s_addr);
    }
  return host;
}

/** try to ping a host to detect whether it is reachable.
 * make a UDP ping, like tracepath, so as to run as non-root.
 */
static int topo_host_ping(const struct in_addr*inaddr)
{
  int ping = 0;
  static const int ping_port = 9; /* DISCARD port */
  /* open UDP socket */
  int fd = PUK_ABI_FSYS_WRAP(socket)(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(fd < 0)
    {
      padico_fatal("failed to open socket; err = %s", strerror(errno));
    }
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_addr = *inaddr;
  addr.sin_port = htons(ping_port);
  addr.sin_family = AF_INET;
  int rc = PUK_ABI_FSYS_WRAP(connect)(fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
  if(rc < 0)
    {
      padico_warning("failed to connect to %s.", inet_ntoa(*inaddr));
      goto errors;
    }
  /* send a msg to nowhere */
  char*hello = "hello";
  rc = PUK_ABI_FSYS_WRAP(sendto)(fd, hello, strlen(hello), 0, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
  if(rc < 0)
    {
      padico_warning("failed to send to %s.", inet_ntoa(*inaddr));
      goto errors;
    }
  /* wait for error */
  struct pollfd p = { .fd = fd, .events = POLLIN | POLLERR };
  rc = PUK_ABI_FSYS_WRAP(poll)(&p, 1, 100 /* milliseconds */);
  padico_out(50, "poll()- rc = %d; in = %d; err = %d\n", rc, p.revents & POLLIN, p.revents & POLLERR);
  if(rc < 0)
    {
      padico_warning("poll() failed for ping to %s.", inet_ntoa(*inaddr));
      ping = 0;
      goto out;
    }
  if(rc == 0)
    {
      /* no error; remote host exists */
      ping = 1;
      goto out;
    }
  /* get error */
#define BUFSIZE 512
  char buffer[BUFSIZE];
  socklen_t addrlen = sizeof(addr);
  rc = PUK_ABI_FSYS_WRAP(recvfrom)(fd, buffer, BUFSIZE, MSG_DONTWAIT, (struct sockaddr_in*)&addr, &addrlen);
  int err = errno;
  if(rc == 0)
    {
      /* no error; someone is listening on DISCARD port */
      ping = 1;
      goto out;
    }
  else if(rc > 0)
    {
      /* we received a message */
      padico_warning("recvfrom() received unexpected message from %s.\n", inet_ntoa(*inaddr));
      ping = 1;
      goto out;
    }

 errors: /* interpret errors */
  switch(err)
    {
    case EHOSTUNREACH:
    case ENETUNREACH:
      ping = 0;
      break;
    case ECONNREFUSED: /* connexion was refused, but packet arrived succesfully */
      ping = 1;
      break;
    default:
      padico_warning("unknown errno = %d (%s) for ping to %s",
                     err, strerror(err), inet_ntoa(*inaddr));
      ping = 0;
      break;
    }

 out:
  PUK_ABI_FSYS_WRAP(close)(fd);
  return ping;
}

/** Create a new host from host entry */
static padico_topo_host_t topo_host_from_hostent(const struct hostent*he)
{
  assert(he != NULL);
  padico_topo_host_t host = topo_host_lookup(he->h_name);
  if(!host)
    {
      padico_print("creating host %s from resolvable host entry.\n", he->h_name);
      host = padico_topo_host_create(he->h_name);
      /* add aliases */
      char**alias = he->h_aliases;
      while(*alias)
        {
          padico_print("adding alias %s for host %s.\n", *alias, host->hostname);
          padico_topo_host_add_name(host, *alias, -1);
          alias++;
        }
      /* add 'inet' addresses */
      he = topo_gethostbyname2(host->hostname, AF_INET);
      if(he)
        {
          const struct in_addr**addr = (const struct in_addr**)he->h_addr_list;
          while(*addr)
            {
              padico_topo_addr_visibility_t v = padico_topo_inet_visibility((*addr)->s_addr);
              if(v == PADICO_TOPO_ADDR_VISIBILITY_PUBLIC)
                {
                  padico_print("adding inet address %s for host %s.\n", inet_ntoa(**addr), host->hostname);
                  padico_topo_host_bind(host, na_topology.inet, *addr);
                }
              else if (v == PADICO_TOPO_ADDR_VISIBILITY_PRIVATE)
                {
                  /* add remote host to private network if it shares a subnet with localhost */
                  int added = 0;
                  padico_topo_host_t localhost = padico_topo_getlocalhost();
                  padico_topo_binding_vect_itor_t b;
                  puk_vect_foreach(b, padico_topo_binding, &localhost->bindings)
                    {
                      if((*b)->network != na_topology.inet)
                        {
                          int cidr;
                          struct in_addr network_addr;
                          if(padico_topo_network_inet_info((*b)->network, &network_addr, &cidr) == 1)
                            {
                              assert(cidr > 0 && cidr < 32);
                              uint32_t netmask = (0xffffffff << (32 - cidr));
                              netmask = htonl(netmask);
                              if(((*addr)->s_addr & netmask) == network_addr.s_addr)
                                {
                                  padico_print("adding address %s for host %s on network %s.\n",
                                               inet_ntoa(**addr), host->hostname, (*b)->network->network_id);
                                  padico_topo_host_bind(host, (*b)->network, *addr);
                                  added = 1;
                                  break;
                                }
                            }
                        }
                    }
                  if(!added)
                    {
                      padico_print("not adding address %s for host %s; private network not known by localhost.\n",
                                   inet_ntoa(**addr), host->hostname);
                      const int ping = topo_host_ping(*addr);
                      padico_print("ping = %d for remote address %s for host %s.\n", ping, inet_ntoa(**addr), host->hostname);
                      if(ping)
                        {
                          /* remote host pingable, but on a different private network; add it to network with a gw */
                          puk_vect_foreach(b, padico_topo_binding, &localhost->bindings)
                            {
                              if((*b)->network != na_topology.inet)
                                {
                                  topo_property_vect_itor_t p;
                                  puk_vect_foreach(p, topo_property, &(*b)->props)
                                    {
                                      struct topo_property_s*prop = *p;
                                      if(prop->kind == TOPO_PROPERTY_GW_ADDR)
                                        {
                                          /* this private network has a gw; binds the new host to the same network */
                                          padico_print("adding address %s for host %s on network %s through gw.\n",
                                                       inet_ntoa(**addr), host->hostname, (*b)->network->network_id);
                                          padico_topo_host_bind(host, (*b)->network, *addr);
                                          break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
               addr++;
            }
        }
      /* add 'inet6' addresses */
      he = topo_gethostbyname2(host->hostname, AF_INET6);
      if(he)
        {
          const struct in6_addr**addr = (const struct in6_addr**)he->h_addr_list;
          while(*addr)
            {
              char s_addr[INET6_ADDRSTRLEN];
              inet_ntop(AF_INET6, *addr, s_addr, INET6_ADDRSTRLEN);
              padico_print("adding inet6 address %s for host %s.\n", s_addr, host->hostname);
              padico_topo_host_bind(host, na_topology.inet6, *addr);
              addr++;
            }
        }
    }
  return host;
}

/** resolve a host from name */
padico_topo_host_t padico_topo_host_resolvebyname(const char*hostname)
{
  const struct hostent*he = NULL;
  /* try to lookup in topology database. */
  padico_topo_host_t host = topo_host_lookup(hostname);
  if(host)
    {
      return host;
    }
  /* try to interpret as IPv4 address */
  struct sockaddr_in inaddr;
  const int isv4 = inet_pton(AF_INET, hostname, &inaddr);
  if(isv4 == 1)
    {
      host = padico_topo_network_gethostbyaddr(na_topology.inet, &inaddr);
      if(!host)
        {
          he = topo_gethostbyaddr(&inaddr, sizeof(struct in_addr), AF_INET);
          if(he)
            {
              host = topo_host_from_hostent(he);
            }
          else
            {
              padico_print("creating host %s as non-resolvable IPv4 address.\n", hostname);
              host = padico_topo_host_create(hostname);
              padico_topo_host_bind(host, na_topology.inet, &inaddr.sin_addr);
            }
        }
      return host;
    }
  /* try to interpret as IPv6 address */
  struct sockaddr_in6 in6addr;
  const int isv6 = inet_pton(AF_INET6, hostname, &in6addr);
  if(isv6 == 1)
    {
      host = padico_topo_network_gethostbyaddr(na_topology.inet6, &in6addr);
      if(!host)
        {
          he = topo_gethostbyaddr(&in6addr, sizeof(struct in6_addr), AF_INET6);
          if(he)
            {
              host = topo_host_from_hostent(he);
            }
          else
            {
              padico_print("creating host %s as non-resolvable IPv6 address.\n", hostname);
              host = padico_topo_host_create(hostname);
              padico_topo_host_bind(host, na_topology.inet6, &in6addr.sin6_addr);
            }
        }
      return host;
    }
  /* try a DNS request with the given name */
  he = topo_gethostbyname2(hostname, AF_INET);
  if(!he)
    {
      /* host is *not* DNS-resolvable- create address-less anyway */
      int err = h_errno;
      padico_warning("hostname %s cannot be resolved (h_errno=%d, %s). Creating anyway with no address.\n",
                     hostname, err, hstrerror(err));
      host = padico_topo_host_create(hostname);
      return host;
    }
  if(strcmp(he->h_name, hostname) != 0)
    {
      host = topo_host_lookup(he->h_name);
      if(host)
        {
          /* host was already known with an alternate name- add the name in database */
          padico_topo_host_add_name(host, hostname, -1);
          return host;
        }
    }
  /* create host from hostent */
  host = topo_host_from_hostent(he);
  return host;
}

/* *** Disconnection *************************************** */

/** Handle node disconnection.
 * Diconnect node from all its bindings, but do not destroy it since pending
 * pointers to it may remain.
 */
void padico_topo_node_disconnect(padico_topo_node_t node)
{
  padico_topo_node_vect_itor_t n = padico_topo_node_vect_find(&node->host->nodes, node);
  if(n == NULL)
    return;
  padico_print("node lost- [%s] %s\n", node->uuid.bytes, node->canonical_nodename);
  padico_topo_node_vect_erase(&node->host->nodes, n);
  padico_topo_binding_vect_itor_t v;
  puk_vect_foreach(v, padico_topo_binding, &node->bindings)
    {
      padico_topo_network_t network = (*v)->network;
      padico_topo_binding_vect_itor_t b;
      puk_vect_foreach(b, padico_topo_binding, &network->bindings)
        {
          if((*b) == (*v))
            {
              padico_topo_binding_vect_erase(&network->bindings, b);
              break;
            }
        }
      topo_binding_destroy(*v);
      /* do not remove v from its vector since we are iterating on it; the vector
       * is trimmed all at once after the loop */
    }
  padico_topo_binding_vect_resize(&node->bindings, 0);
}
