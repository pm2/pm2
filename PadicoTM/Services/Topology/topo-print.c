/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file topo-print.xml
 * @brief Display the known topology
 * @ingroup Topology
 * @author Sebastien Barascou
 */

#include "Topology.h"
#include "Topology-internals.h"
#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef HAVE_CGRAPH
#include <graphviz/cgraph.h>
#endif /* HAVE_CGRAPH */

static int padico_topo_print_main(int argc, char**argv);

PADICO_MODULE_DECLARE(Topo_Print, NULL, padico_topo_print_main, NULL);

static FILE* padico_topo_print_open_file(const char*filename, char*wanted)
{
  FILE*f = NULL;
  char*ext = strrchr(filename, '.');
  if(strcmp(filename, "-") == 0)
    return stdout;
  else if(!ext || (ext && strncmp(ext+1, wanted, strlen(wanted))))
    {
      padico_string_t filename_s = padico_string_new();
      padico_string_catf(filename_s,"%s.%s", filename, wanted);
      f = fopen(padico_string_get(filename_s), "w+");
      padico_string_delete(filename_s);
    }
  else
    f = fopen(filename, "w+");
  return f;
}

static void padico_topo_print_padico(FILE*f)
{
  padico_string_t topology =  padico_na_topology_serialize(PADICO_TOPO_SCOPE_HOST);
  fprintf(f, "<Topology:topology xmlns:Topology=\"http://runtime.bordeaux.inria.fr/PadicoTM/Topology\">\n"
          "%s"
          "</Topology:topology>\n", padico_string_get(topology));
  padico_string_delete(topology);
}

#ifdef HAVE_CGRAPH
static char* padico_topo_print_dot_headport(const padico_topo_binding_t b)
{
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, (struct topo_property_vect_s*)&b->props)
    {
      if((*p)->kind == TOPO_PROPERTY_ADDR)
        {
          const struct topo_address_s*addr = &(*p)->content.ADDR.addr;
          size_t addr_size = b->network->addr_size;
          return  puk_hex_encode(addr->_addr_storage, &addr_size, NULL);
        }
    }
  return NULL;
}

static Agnode_t* padico_topo_print_dot_host(Agraph_t*graph, padico_topo_host_t host)
{
  Agnode_t*host_node = agnode(graph, (char*)padico_topo_host_getname(host), 1);
  agsafeset(host_node, "shape", "record", "");
  padico_string_t label = padico_string_new();
  padico_string_catf(label, "<name> %s", padico_topo_host_getname(host));
  padico_string_catf(label, "|{");
  padico_topo_binding_vect_itor_t i;
  int first = 1;
  puk_vect_foreach(i, padico_topo_binding, &host->bindings)
    {
      const struct topo_binding_s*b = *i;
      topo_property_vect_itor_t p;
      puk_vect_foreach(p, topo_property, (struct topo_property_vect_s*)&b->props)
        {
          if((*p)->kind == TOPO_PROPERTY_ADDR)
            {
              char*s_addr = NULL;
              const char*ret = NULL;
              const struct topo_address_s*addr = &(*p)->content.ADDR.addr;
              if(addr->family == AF_INET)
                {
                  s_addr = padico_malloc(INET_ADDRSTRLEN);
                  ret = inet_ntop(AF_INET, addr->_addr_storage, s_addr, INET_ADDRSTRLEN);
                }
              else if(addr->family == AF_INET6)
                {
                  s_addr = padico_malloc(INET6_ADDRSTRLEN);
                  ret = inet_ntop(AF_INET6, addr->_addr_storage, s_addr, INET6_ADDRSTRLEN);
                }
              else
                {
                  s_addr = padico_strdup(" ");
                  ret = s_addr;
                }
              if(ret != NULL)
                {
                  char*find= NULL;
                  padico_string_t hex_addr_s = padico_string_new();
                  size_t addr_size = b->network->addr_size;
                  char*hex_addr = puk_hex_encode(addr->_addr_storage, &addr_size, NULL);
                  padico_string_catf(hex_addr_s, "<%s>", hex_addr);
                  padico_free(hex_addr);
                  find = strstr(padico_string_get(label), padico_string_get(hex_addr_s));
                  if(!find)
                    {
                      if(first)
                        {
                          padico_string_catf(label, "%s %s", padico_string_get(hex_addr_s), ret);
                          first = 0;
                        }
                      else
                        padico_string_catf(label, "| %s %s", padico_string_get(hex_addr_s), ret);
                    }
                  padico_string_delete(hex_addr_s);
                }
              if(s_addr != NULL)
                padico_free(s_addr);
            }
        }
    }
  padico_string_catf(label, "}");
  agsafeset(host_node, "label", padico_string_get(label), "");
  padico_string_delete(label);
  return host_node;
}

static padico_string_t padico_topo_print_dot_filter(padico_topo_binding_t b)
{
  setprotoent(1);
  padico_string_t label = padico_string_new();
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, (struct topo_property_vect_s*)&b->props)
    {
      if((*p)->kind == TOPO_PROPERTY_FILTER)
        {
          const padico_topo_filter_t filter = &(*p)->content.FILTER.filter;
          const char*proto_name = topo_filter_protocol_getname(filter->protocol);
          padico_string_catf(label, "%s: %s port %d\\n",
                             topo_filter_policy_getname(filter->policy), proto_name, filter->port);
        }
    }
  endprotoent();
  return label;
}

static void padico_topo_print_dot(FILE*f)
{
  Agraph_t* topo = agopen("topology", Agstrictundirected, NULL);
  /* graph attributes*/
  agattr(topo, AGRAPH, "ranksep", "5");
  agattr(topo, AGRAPH, "rankdir", "RL");
  /* node attributes*/
  agattr(topo, AGNODE, "color", "");
  agattr(topo, AGNODE, "shape", "");
  agattr(topo, AGNODE, "style", "");
  agattr(topo, AGNODE, "label", "");
  agattr(topo, AGNODE, "fontsize", "");
  /* edge attributes*/
  agattr(topo, AGEDGE, "labelfontsize", "5");
  agattr(topo, AGEDGE, "label", "");
  agattr(topo, AGEDGE, "labeldistance", "2");
  agattr(topo, AGEDGE, "headlabel", "");
  agattr(topo, AGEDGE, "taillabel", "");
  agattr(topo, AGEDGE, "headport", "");
  agattr(topo, AGEDGE, "len", "3");
  agattr(topo, AGEDGE, "dir", "");
  agattr(topo, AGEDGE, "arrowhead", "");
  padico_topo_host_vect_t hosts = padico_topo_gethosts();
  padico_topo_host_vect_itor_t h;
  puk_vect_foreach(h, padico_topo_host, hosts)
    {
      Agnode_t*host_node =  padico_topo_print_dot_host(topo, *h);
      agsafeset(host_node, "fontsize", "5", "");
      padico_topo_binding_vect_itor_t i;
      puk_vect_foreach(i, padico_topo_binding, &((*h)->bindings))
        {
          const padico_topo_binding_t b = *i;
          if(b->network->scope == PADICO_TOPO_SCOPE_HOST)
            {
              Agnode_t*network_node = agnode(topo, (char*)padico_topo_network_getname(b->network), 1);
              agsafeset(network_node, "shape", "hexagon","");
              agsafeset(network_node, "style", "filled", "");
              agsafeset(network_node, "color", "lightskyblue1", "");
              Agedge_t*edge = agedge(topo, network_node, host_node, "", 1);
              char*headport = padico_topo_print_dot_headport(b);
              if(headport)
                  {
                    agsafeset(edge, "headport", headport, "");
                    padico_free(headport);
                  }
              if(b->network->uplink != NULL)
                {
                  Agnode_t*uplink_node = agnode(topo, (char*)padico_topo_network_getname(b->network->uplink->network), 1);
                  agsafeset(uplink_node, "shape", "hexagon","");
                  agsafeset(uplink_node, "style", "filled", "");
                  agsafeset(uplink_node, "color", "lightskyblue1", "");
                  edge = agedge(topo, network_node, uplink_node, "", 1);
                  padico_string_t filter = padico_topo_print_dot_filter(b->network->uplink);
                  agsafeset(edge, "taillabel", padico_string_get(filter), "");
                  padico_string_delete(filter);
                }
            }
        }
    }
  padico_topo_host_vect_destroy(hosts);

  agwrite(topo, f);
  agclose(topo);
}
#endif /* HAVE_CGRAPH */

static int padico_topo_print_main(int argc, char**argv)
{
  if(padico_na_rank() == 0)
    {
      FILE*f = stdout;
      if(argc < 2)
        {
          printf("Topo-Print: Usage: Topo-Print <format> <filename>\n");
          printf("Topo-Print: <format> can be:\n"
                 "Topo-Print:   padico: the native PadicoTM Topology format\n"
#ifdef HAVE_CGRAPH
                 "Topo-Print:   dot: a representation of topology usable by graphviz tools\n"
#else
                 "              (graphviz not detected)\n"
#endif /* HAVE_CGRAPH */
                 "Topo-Print: <filename>: file to write to. Give '-' for stdout.\n"
                 );
          return 0;
        }
      const char* format = argv[1];
      if(!strncmp(format, "padico", 6))
        {
          if(argc >= 3)
            f = padico_topo_print_open_file(argv[2], "xml");
          if(f == NULL)
            {
              printf("Topo-Print: cannot open file:%s\n", argv[2]);
              return 1;
            }
          padico_topo_print_padico(f);
        }
#ifdef HAVE_CGRAPH
      else if(!strncmp(format, "dot", 3))
        {
          if(argc >= 3)
            f = padico_topo_print_open_file(argv[2], "dot");
          if(f == NULL)
            {
              printf("Topo-Print: cannot open file:%s\n", argv[2]);
              return 1;
            }
          padico_topo_print_dot(f);
        }
#endif /* HAVE_CGRAPH */
      else
        {
          printf("Topo-Print: unrecognized output format *%s*\n", format);
        }
      if(f != stdout)
        fclose(f);
    }
  return 0;
}
