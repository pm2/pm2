/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file internal definitions for module Topology
 *  @ingroup Topology
 */

#ifndef TOPOLOGY_INTERNALS_H
#define TOPOLOGY_INTERNALS_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include "Topology.h"

#include <netdb.h>
#ifdef HAVE_LINUX_IF_H
#include <linux/if.h>
#else
#include <net/if.h>
#endif /* HAVE_LINUX_IF_H */
#include <net/if_arp.h>

/* *** Topology model ************************************** */


PUK_VECT_TYPE(topo_hwaddr, padico_topo_hwaddr_t);

/** storage size for hardware address- sockaddr_ll has 8 bytes */
#define TOPO_HWADDR_PADDING 16

#ifdef IFNAMSIZ
#define TOPO_HWADDR_NAMSIZ IFNAMSIZ
#else
#warning IFNAMSIZ undefined.
#define TOPO_HWADDR_NAMSIZ 16
#endif

/** Node UUID, in plain text (hex encoding) */
struct topo_uuid_s
{
  char bytes[PADICO_TOPO_UUID_SIZE];
  char zero;
};

/** a network address used in bindings */
struct topo_address_s
{
  int family; /**< address family, using AF_* */
  int size;   /**< address size, used for hashing */
  char _addr_storage[1]; /**< placeholder for address- more bytes are actually allocated */
};

/** a service which running on a host */
struct topo_service_s
{
  uint16_t port;
  char name[1];
};

/** a filter rule, i.e a port number, and a policy */
struct topo_filter_s
{
  padico_topo_filter_policy_t policy;
  int protocol;
  uint16_t port;
};

/** performance information about link, i.e estimated latency and bandwidth  */
struct topo_perf_s
{
  int lat;  /**< network latency in nanoseconds */
  int bw;   /**< network bandwidth in KB/s */
};

/** an entity that can be member of a network through a binding */
struct topo_net_entity_s
{
  enum
    {
      TOPO_ENTITY_NONE = 0,
      TOPO_ENTITY_HOST,
      TOPO_ENTITY_NODE,
      TOPO_ENTITY_NETWORK
    } kind;
  union
  {
    padico_topo_node_t node;
    padico_topo_host_t host;
    padico_topo_network_t network;
    void*as_ptr;
  };
};
typedef struct topo_net_entity_s topo_net_entity_t;

typedef enum
  {
    TOPO_PROPERTY_ADDR,      /**< protocol address for the binding */
    TOPO_PROPERTY_HWADDR,    /**< hardware address/iface */
    TOPO_PROPERTY_HOSTNAME,  /**< name of the host on the network*/
    TOPO_PROPERTY_FILTER,    /**< filtering- TODO */
    TOPO_PROPERTY_PERF,      /**< performance properties- TODO */
    TOPO_PROPERTY_GW_ADDR,   /**< IPv4 address of the gateway */
    TOPO_PROPERTY_GW_HWADDR, /**< hardware address of the gateway */
    TOPO_PROPERTY_GW_HOST,   /**< gateway as a host */
    TOPO_PROPERTY_SERVICE,   /**< service which run on a host */
    TOPO_PROPERTY_ROUTE,     /**< describe a route between private networks */
    TOPO_PROPERTY_INDEX,     /**< index of the board or port number */
    TOPO_PROPERTY_MISC       /**< arbitrary attribute (string), without semantics */
  } topo_property_kind_t;

struct topo_property_s
{
  topo_property_kind_t kind;
  union
  {
    struct
    {
      struct topo_address_s addr;
    } ADDR, GW_ADDR;   /**< for ADDR */
    struct
    {
      struct topo_filter_s filter;
    } FILTER; /**< for FILTER* */
    struct
    {
      padico_topo_hwaddr_t hwaddr;     /**< pointer to hwaddr allocated in host */
      char ifname[TOPO_HWADDR_NAMSIZ]; /**< ifname, when hwaddr is unresolved */
    } HWADDR, GW_HWADDR; /**< for HWADDR */
    struct
    {
      padico_topo_host_t gw_host;
    } GW_HOST;
    struct
    {
      struct topo_service_s service;
    } SERVICE;
    struct
    {
      char*name;
    } HOSTNAME;
    struct
    {
      padico_topo_network_t dest;
      struct topo_address_s gw;
    } ROUTE;
    struct
    {
      struct topo_perf_s perf;
    } PERF;
    struct
    {
      int64_t index;
    } INDEX;
    struct
    {
      char*label;
      char*value;
    } MISC;   /**< for MISC */
  } content;
};
PUK_VECT_TYPE(topo_property, struct topo_property_s*);

/** a binding of an entity with a network (protocol level) */
struct topo_binding_s
{
  topo_net_entity_t entity;          /**< contained entity */
  padico_topo_network_t network;     /**< container network */
  struct topo_property_vect_s props; /**< binding properties, e.g. address */
};

struct topo_hostname_s
{
  char*name;
  int resolvable;
};
PUK_VECT_TYPE(topo_hostname, struct topo_hostname_s*);

/** a host, i.e. a machine, with its bindings */
struct topo_host_s
{
  const char*hostname;                 /**< canonical fully qualified domain name, as reported by gethostbyname(3) */
  struct topo_hostname_vect_s hnames;  /**< names of the host, including short hostname and fqdn */
  struct padico_topo_node_vect_s nodes;  /**< nodes on this host */
  struct padico_topo_binding_vect_s bindings; /**< protocol bindings */
  struct topo_hwaddr_vect_s hwaddrs;   /**< hardware addresses */
  struct topo_property_vect_s props;   /**< host properties, e.g service*/
};

/** a node, i.e. a running process, with its bindings */
struct topo_node_s
{
  struct topo_uuid_s uuid;       /**< universally unique ID for the node */
  const char*canonical_nodename; /**< <fqdn_hostname>:<num> */
  padico_topo_host_t host;         /**< host containing this node */
  const char*boot_id;            /**< session ID identifying our 'padico-launch' instance */
  struct padico_topo_binding_vect_s bindings; /**< network bindings of the node */
};

/** a hardware address- identifies a host  */
struct topo_hwaddr_s
{
  char name[TOPO_HWADDR_NAMSIZ]; /**< hw interface name, as seen through netdevice interface */
  int index;                     /**< hw interface number, as seen through rtnetlink interface */
  unsigned short int type;       /**< ARPHRD_* as defined in <net/if_arp.h> */
  size_t len;                    /**< actual length of hardware address */
  char _hwaddr_storage[1];       /**< placeholder area for address */
};

struct na_network_type_s
{
  const char*name;
  size_t addr_size;
  padico_topo_binding_scope_t scope;
};

/** a network, containing entities with bindings */
struct topo_network_s
{
  const char*network_id;           /**< session ID of the network */
  size_t addr_size;                /**< size of the addresses on the given network */
  int family;                      /**< address family used on tyhe network- AF_UNSPEC if unknown */
  padico_topo_binding_scope_t scope; /**< scope (host/node) of the bindings on this network */
  struct padico_topo_binding_vect_s bindings; /**< bindings (nodes/hosts) of this network */
  puk_hashtable_t bindings_by_addr;    /**< bindings hashed by address */
  struct topo_binding_s*uplink;        /**< uplink to another network, if any */
  struct topo_perf_s perf;             /**< estimated network performance */
};


/** Shared structure to describes the "known world"
 */
struct padico_topology_s
{
  padico_topo_node_t local_node; /**< local node descriptor */
  padico_topo_network_t  inet; /**< global network- *public* IPv4 internet */
  padico_topo_network_t inet6; /**< global network- IPv6 internet */
  puk_hashtable_t  node_table; /**< index of node,  hashed by UUID */
  puk_hashtable_t nodesbyname; /**< index of nodes, hashed by nodename (canonical+alias, there may be duplicate entries) */
  puk_hashtable_t hostsbyname; /**< index of hosts, hashed by name */
  padico_topo_network_vect_t networks; /**< vector of known networks */
};

__PUK_SYM_INTERNAL struct padico_topology_s*topology_get(void);


/* ********************************************************* */

__PUK_SYM_INTERNAL void topology_parser_init(void);

__PUK_SYM_INTERNAL const struct topo_address_s*topo_binding_getaddr(const struct topo_binding_s*binding);
__PUK_SYM_INTERNAL padico_topo_node_t topo_node_create(const char*name, padico_topo_host_t host,
                                                     const char*uuid, const char*boot_id);
__PUK_SYM_INTERNAL void topo_node_destroy(padico_topo_node_t node);

__PUK_SYM_INTERNAL padico_topo_host_t topo_host_lookup(const char*hostname);
__PUK_SYM_INTERNAL padico_topo_host_t topo_host_create(const char*hostname);
__PUK_SYM_INTERNAL void topo_binding_bind_host(padico_topo_binding_t*_binding, padico_topo_host_t host);
/** add a binding to a node; take ownership of binding */
__PUK_SYM_INTERNAL void topo_binding_bind_node(padico_topo_binding_t*_binding, padico_topo_node_t node);
__PUK_SYM_INTERNAL void topo_binding_merge(padico_topo_binding_t binding, padico_topo_binding_t b2);
__PUK_SYM_INTERNAL void topo_binding_destroy(struct topo_binding_s*binding);
__PUK_SYM_INTERNAL void topo_property_destroy(struct topo_property_s*prop);

/* ********************************************************* */
/* ** hwaddr toolbox */

static inline padico_topo_hwaddr_t topo_hwaddr_getbyname(padico_topo_host_t host, const char*name)
{
  topo_hwaddr_vect_itor_t i;
  puk_vect_foreach(i, topo_hwaddr, &host->hwaddrs)
    {
      if(strcmp(name, (*i)->name) == 0)
        return *i;
    }
  return NULL;
}
static inline padico_topo_hwaddr_t topo_hwaddr_getbyindex(padico_topo_host_t host, int index)
{
  topo_hwaddr_vect_itor_t i;
  puk_vect_foreach(i, topo_hwaddr, &host->hwaddrs)
    {
      if((*i)->index == index)
        return *i;
    }
  return NULL;
}

/* ********************************************************* */
/* ** filter toolbox */

static const char*topo_filter_policy_names[] =
  {
    [_PADICO_TOPO_FILTER_NONE]     = NULL,
    [PADICO_TOPO_FILTER_BLOCK_IN]  = "block-input",
    [PADICO_TOPO_FILTER_BLOCK_OUT] = "block-output",
    [PADICO_TOPO_FILTER_ALLOW_IN]  = "allow-input",
    [PADICO_TOPO_FILTER_ALLOW_OUT] = "allow-output",
    [_PADICO_TOPO_FILTER_MAX]      = NULL
  };

static inline padico_topo_filter_policy_t topo_filter_policy_getbyname(const char*name)
{
  padico_topo_filter_policy_t i;
  for(i = _PADICO_TOPO_FILTER_NONE; i < _PADICO_TOPO_FILTER_MAX; i++)
    {
      if((topo_filter_policy_names[i] != NULL) &&
         (strcmp(name, topo_filter_policy_names[i]) == 0))
        {
          return i;
        }
    }
  return _PADICO_TOPO_FILTER_NONE;
}

static inline const char*topo_filter_policy_getname(padico_topo_filter_policy_t policy)
{
  if(policy > _PADICO_TOPO_FILTER_NONE && policy < _PADICO_TOPO_FILTER_MAX)
    return topo_filter_policy_names[(int)policy];
  else
    return NULL;
}

/** frontend for getprotobyname(), working even in the absence of /etc/protocols */
static inline int topo_filter_protocol_getnumber(const char*proto_name)
{
  if(strcmp(proto_name, "tcp") == 0)
    return IPPROTO_TCP;
  else if(strcmp(proto_name, "udp") == 0)
    return IPPROTO_UDP;
  else
    {
      struct protoent*p = getprotobyname(proto_name);
      if(p)
        return p->p_proto;
      else
        {
          padico_warning("unknown protocol name %s\n", proto_name);
          return -1;
        }
    }
}

/** frontend for getprotobynumber(), working even in the absence of /etc/protocols */
static inline const char*topo_filter_protocol_getname(int protocol)
{
  if(protocol == IPPROTO_TCP)
    return "tcp";
  else if(protocol == IPPROTO_UDP)
    return "udp";
  else
    {
      const struct protoent*p = getprotobynumber(protocol);
      if(p)
        return p->p_name;
      else
        {
          padico_warning("unknown protocol number %d\n", protocol);
          return "[unknown]";
        }
    }
}

/* ********************************************************* */
/* ** addr */

static inline const struct in_addr*topo_address_as_inaddr(padico_topo_address_t addr)
{
  assert(addr->family == AF_INET);
  assert(addr->size == sizeof(struct in_addr));
  return (struct in_addr*)&addr->_addr_storage[0];
}

/* ********************************************************* */
/* ** binding toolbox */

static inline padico_topo_binding_t topo_binding_create(padico_topo_network_t network)
{
  struct topo_binding_s*binding = padico_malloc(sizeof(struct topo_binding_s));
  binding->entity.kind = TOPO_ENTITY_NONE;
  binding->entity.as_ptr = NULL;
  binding->network = network;
  topo_property_vect_init(&binding->props);
  padico_topo_binding_vect_push_back(&network->bindings, binding);
  return binding;
}


#endif /* TOPOLOGY_INTERNALS_H */
