/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

static int control_bootstrap_pmi2_init(void);
static void control_bootstrap_pmi2_finalize(void);

PADICO_MODULE_DECLARE(ControlBootstrapPMI2, control_bootstrap_pmi2_init, NULL, control_bootstrap_pmi2_finalize);

#ifdef HAVE_PMI2

#include <Padico/NetSelector.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>
#include <Padico/PMI2.h>

PUK_VECT_TYPE(puk_instance, puk_instance_t);

static struct
{
  struct puk_component_vect_s components;
  puk_instance_vect_t instances;       /**< listening bootstrap instances */
  struct
  {
    marcel_mutex_t lock;
    marcel_cond_t ready;
    padico_topo_node_vect_t nodes;
    int nodes_ready;
  } sync;
} control_bootstrap;

/* ********************************************************* */

static void control_bootstrap_pmi2_ready_handler(puk_parse_entity_t e)
{
  marcel_mutex_lock(&control_bootstrap.sync.lock);
  control_bootstrap.sync.nodes_ready++;
  marcel_cond_signal(&control_bootstrap.sync.ready);
  marcel_mutex_unlock(&control_bootstrap.sync.lock);
}

static puk_component_t control_bootstrap_pmi2_component_init(int local_rank, int remote_rank)
{
  /* get remote topology before using NetSelector */
  padico_string_t s_key = padico_string_new();
  /* 1. resolve remote host */
  padico_string_printf(s_key, "padico_bootstrap_host_%d", remote_rank);
  char*remote_topo = padico_pmi2_kvs_lookup(padico_string_get(s_key), remote_rank);
  padico_control_event_disable();
  struct puk_parse_entity_s e = puk_xml_parse_buffer(remote_topo, strlen(remote_topo), PUK_TRUST_CLUSTER);
  padico_rc_t rc2 = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc2))
    {
      padico_rc_show(rc2);
    }
  padico_control_event_enable();
  padico_free(remote_topo);
  /* 2. resolve remote node */
  padico_string_printf(s_key, "padico_bootstrap_node_%d", remote_rank);
  char*s_node = padico_pmi2_kvs_lookup(padico_string_get(s_key), remote_rank);
  padico_control_event_disable();
  e = puk_xml_parse_buffer(s_node, strlen(s_node), PUK_TRUST_CLUSTER);
  rc2 = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc2))
    {
      padico_rc_show(rc2);
    }
  padico_topo_node_t remote_node = puk_parse_get_content(&e);
  padico_control_event_enable();
  padico_string_delete(s_key);
  /* 3. resolve connector */
  puk_component_t component =
    padico_ns_serial_selector(remote_node, NULL, puk_iface_PadicoConnector());
  if(component == NULL)
    {
      padico_fatal("no PadicoConnector assembly found.\n");
    }
  const struct padico_control_connector_driver_s*connector = puk_component_get_driver_PadicoConnector(component, NULL);
  if(connector == NULL &&
     puk_component_get_driver_PadicoControl(component, NULL) == NULL)
    {
      padico_fatal("bootstrap component do not provide required interface\
                   (PadicoConnector, PadicoControl).\n");
    }
  /* init context */
  puk_context_t context = puk_component_get_context(component, puk_iface_PadicoConnector(), NULL);
  if(context == NULL)
    {
      padico_fatal("cannot find context in component %s.\n", component->name);
    }
  (*connector->connector_init)(context);
  return component;
}

static puk_instance_t control_bootstrap_pmi2_instance_init(puk_component_t component, int local_rank, int remote_rank)
{
  padico_print("local_rank = %d; remote_rank = %d; component = %s\n",
             local_rank, remote_rank, component->name);
  puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_PadicoConnector_s r;
  puk_instance_indirect_PadicoConnector(instance, NULL, &r);
  padico_out(50, "init bootstrap served by assembly %s\n", instance->component->name);
  if(r.driver->connector_new == NULL)
    {
      padico_fatal("selected connector has no 'connector_new' method. Context-wide 'init' method not supported yet.\n");
    }
  /* init & get local address */
  const void*local_addr;
  size_t addr_size;
  (*r.driver->connector_new)(r._status, &local_addr, &addr_size);
  char*local_addr_enc = puk_hex_encode(local_addr, &addr_size, NULL);
  /* publish address */
  padico_string_t s_key = padico_string_new();
  padico_string_printf(s_key, "padico_bootstrap_addr_%d-%d", local_rank, remote_rank);
  padico_pmi2_kvs_publish(padico_string_get(s_key), local_addr_enc);
  padico_string_delete(s_key);
  return instance;
}
static void control_bootstrap_pmi2_instance_connect(puk_instance_t instance, int local_rank, int remote_rank)
{
  /* get remote addr & connect */
  padico_string_t s_key = padico_string_new();
  padico_string_printf(s_key, "padico_bootstrap_addr_%d-%d", remote_rank, local_rank);
  char*remote_addr_enc = padico_pmi2_kvs_lookup(padico_string_get(s_key), remote_rank);
  size_t remote_addr_size = strlen(remote_addr_enc);
  void*remote_addr = puk_hex_decode(remote_addr_enc, &remote_addr_size, NULL);
  struct puk_receptacle_PadicoConnector_s r;
  puk_instance_indirect_PadicoConnector(instance, NULL, &r);
  (*r.driver->connector_connect)(r._status, remote_addr, remote_addr_size);
  padico_string_delete(s_key);
}

static int control_bootstrap_pmi2_init(void)
{
  puk_component_vect_init(&control_bootstrap.components);
  control_bootstrap.instances = puk_instance_vect_new();
  marcel_mutex_init(&control_bootstrap.sync.lock, NULL);
  marcel_cond_init(&control_bootstrap.sync.ready, NULL);
  control_bootstrap.sync.nodes = padico_topo_node_vect_new();
  control_bootstrap.sync.nodes_ready = 0;
  puk_xml_add_action((struct puk_tag_action_s){
             .xml_tag        = "ControlBootstrap:ready",
             .start_handler  = NULL,
             .end_handler    = &control_bootstrap_pmi2_ready_handler,
             .required_level = PUK_TRUST_CONTROL
             });

  const int rank = atoi(padico_getattr("PADICO_BOOT_RANK")?:"0");
  const int size = atoi(padico_getattr("PADICO_BOOT_SIZE")?:"1");
  /* ** publish local topology (host & node) */
  padico_string_t s_key = padico_string_new();
  padico_string_printf(s_key, "padico_bootstrap_host_%d", rank);
  padico_string_t s_localhost = padico_topo_host_serialize(padico_topo_getlocalhost());
  padico_pmi2_kvs_publish(padico_string_get(s_key), padico_string_get(s_localhost));
  s_key = padico_string_new();
  padico_string_printf(s_key, "padico_bootstrap_node_%d", rank);
  padico_string_t s_localnode = padico_topo_node_serialize(padico_topo_getlocalnode());
  padico_pmi2_kvs_publish(padico_string_get(s_key), padico_string_get(s_localnode));
  padico_pmi2_kvs_fence();
  padico_string_delete(s_localnode);
  padico_string_delete(s_localhost);

  if(rank == 0)
    {
      int i;
      /* resolve components */
      puk_component_vect_push_back(&control_bootstrap.components, NULL); /* placeholder at #0 */
      for(i = 1; i < size; i++)
        {
          puk_component_t component = control_bootstrap_pmi2_component_init(0, i);
          puk_component_vect_push_back(&control_bootstrap.components, component);
        }
      /* instantiate */
      for(i = 1; i < size; i++)
        {
          puk_instance_t instance = control_bootstrap_pmi2_instance_init(puk_component_vect_at(&control_bootstrap.components, i), 0, i);
          puk_instance_vect_push_back(control_bootstrap.instances, instance);
        }
      padico_pmi2_kvs_fence();
      /* connect */
      puk_instance_vect_itor_t itor;
      i = 1;
      puk_vect_foreach(itor, puk_instance, control_bootstrap.instances)
        {
          control_bootstrap_pmi2_instance_connect(*itor, 0, i);
          i++;
        }
    }
  else
    {
      const int rdv_rank = 0;
      puk_component_t component = control_bootstrap_pmi2_component_init(rank, rdv_rank);
      puk_component_vect_push_back(&control_bootstrap.components, component);
      puk_instance_t instance = control_bootstrap_pmi2_instance_init(component, rank, rdv_rank);
      puk_instance_vect_push_back(control_bootstrap.instances, instance);
      padico_pmi2_kvs_fence();
      control_bootstrap_pmi2_instance_connect(instance, rank, rdv_rank);
    }

  int wait_nodes = size - 1;
  marcel_mutex_lock(&control_bootstrap.sync.lock);
  padico_topo_network_t session = padico_topo_session_getnetwork(padico_topo_node_getsession(padico_topo_getlocalnode()));
  while(padico_topo_network_size(session) < size)
    {
      marcel_cond_wait(&control_bootstrap.sync.ready, &control_bootstrap.sync.lock);
    }
  if(rank == 0)
    {
      /* wait all nodes to acknowledge they are ready */
      padico_topo_network_t session = padico_topo_session_getnetwork(padico_topo_node_getsession(padico_topo_getlocalnode()));
      while((control_bootstrap.sync.nodes_ready < wait_nodes) ||
            padico_topo_network_size(session) < size)
        {
          marcel_cond_wait(&control_bootstrap.sync.ready, &control_bootstrap.sync.lock);
        }
    }
  else
    {
      padico_topo_node_t session_0 =
        padico_topo_session_getnodebyrank(padico_topo_node_getsession(padico_topo_getlocalnode()), 0);
      padico_string_t msg = padico_string_new();
      padico_string_t self_node = padico_topo_node_serialize(padico_topo_getlocalnode());
      padico_string_printf(msg, "<ControlBootstrap:ready>%s</ControlBootstrap:ready>\n", padico_string_get(self_node));
      padico_string_delete(self_node);
      padico_control_send_oneway(session_0, padico_string_get(msg));
      padico_string_delete(msg);
    }

  marcel_mutex_unlock(&control_bootstrap.sync.lock);
  padico_print("rank = %d; control channel is ready.\n", rank);
  padico_string_delete(s_key);
  return 0;

}

static void control_bootstrap_pmi2_finalize(void)
{
  while(!puk_instance_vect_empty(control_bootstrap.instances))
    {
      puk_instance_t inst = puk_instance_vect_pop_back(control_bootstrap.instances);
      puk_instance_destroy(inst);
    }
  while(!puk_component_vect_empty(&control_bootstrap.components))
    {
      puk_component_t component = puk_component_vect_pop_back(&control_bootstrap.components);
      if(component)
        {
          const struct padico_control_connector_driver_s*connector =
            puk_component_get_driver_PadicoConnector(component, NULL);
          if(connector->connector_close)
            {
              puk_context_t context = puk_component_get_context(component, puk_iface_PadicoConnector(), NULL);
              (*connector->connector_close)(context);
            }
        }
    }
  puk_instance_vect_delete(control_bootstrap.instances);
  puk_component_vect_destroy(&control_bootstrap.components);
  padico_topo_node_vect_delete(control_bootstrap.sync.nodes);
  puk_xml_delete_action("ControlBootstrap:ready");
}

#else /* HAVE_PMI2 */

static int control_bootstrap_pmi2_init(void)
{
  padico_fatal("PadicoTM was compiled without PMI2 support.\n");
}

static void control_bootstrap_pmi2_finalize(void)
{
  /* do nothing */
}

#endif /* HAVE_PMI2 */
