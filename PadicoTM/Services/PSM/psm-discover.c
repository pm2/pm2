/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file psm-discover.c
 *  @brief Intel PSM detection
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <psm.h>

static int padico_psm_init(void);
static void padico_psm_finalize(void);

PADICO_MODULE_DECLARE(PSM_discover, padico_psm_init, NULL, padico_psm_finalize);

/* ********************************************************* */

static uint64_t dummy_addr = 0; /* dummy address; real PSM addressing system uses ibverbs */

static int init_done = 0;

static void padico_psm_discover(void)
{
  int ver_major = PSM_VERNO_MAJOR;
  int ver_minor = PSM_VERNO_MINOR;
  int rc = psm_init(&ver_major, &ver_minor);
  if(rc != PSM_OK)
    {
      padico_out(puk_verbose_critical, "cannot init PSM library.\n");
      return;
    }
  init_done = 1;
  uint32_t num_units = -1;
  rc = psm_ep_num_devunits(&num_units);
  if(rc == PSM_OK)
    {
      padico_print("detected %d devices.\n", num_units);
      const char*s_uuid = padico_getenv("PADICO_BOOT_UUID");
      padico_string_t s_net = padico_string_new();
      padico_string_printf(s_net, "PSM-[%s]", s_uuid);
      padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(s_net));
      if(network == NULL)
        {
          network = padico_topo_network_create(padico_string_get(s_net), PADICO_TOPO_SCOPE_HOST, sizeof(uint64_t), AF_UNSPEC);
        }
      padico_string_delete(s_net);
      padico_topo_host_bind(padico_topo_getlocalhost(), network, &dummy_addr);
    }
  else
    {
      padico_out(puk_verbose_notice, "no device found.\n");
    }
}

static int padico_psm_init(void)
{
  padico_psm_discover();
  return 0;
}

static void padico_psm_finalize(void)
{
  if(init_done)
    {
      init_done = 0;
      psm_finalize();
    }
}
