/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Services/Console/console-mod.c
 * @brief Interactive console for steering the processes
 * @ingroup Console
 */

/** @defgroup Console Service: Console -- interactive console plugged on local tty
 * @author Alexandre Denis
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

static int padico_console_init(void);
static int padico_console_run(int argc, char**argv);
void padico_console_finalize(void);

PADICO_MODULE_DECLARE(Console, padico_console_init, padico_console_run, padico_console_finalize);

static void console_loop(void);
static int done = 0;
static int running = 0;
static char*console_name;
static FILE*console;

static int padico_console_init(void)
{
  int rc;
  if(isatty(0))
    {
      console_name = ttyname(0);
      rc = 0;
    }
  else
    {
      padico_warning("stdin is not a tty-- abort.\n");
      console_name = NULL;
      rc = -1;
    }
  return rc;
}

static int padico_console_run(int argc, char**argv)
{
  padico_trace("run()\n");
  if(console_name != NULL)
    {
      padico_print("Starting Padico console on %s\n", console_name);
      /*   fcntl(fd, F_SETFD, O_NONBLOCK); */
      console = fopen(console_name, "r+");
      setbuf(console, NULL);
      if(console == NULL)
        {
          padico_warning("open error\n");
          return -1;
        }
      running = 1;
      console_loop();
    }
  else
    {
      padico_warning("stdin is not a tty\n");
    }
  return 0;
}

void padico_console_finalize(void)
{
  padico_trace("finalize() join thread\n");
  done = 1;
  if(running)
    {
      fclose(console);
      padico_print("close on %s\n", console_name);
    }
  running = 0;
  padico_trace("finalize() ok\n");
}

#define TAB_SIZE 4096

static void  console_loop(void)
{
  char cmd[TAB_SIZE];
  char*c;
  padico_rc_t rc = NULL;

  fprintf(console, "# Welcome in %s console\n", PADICOTM_VERSION_STRING);
  fprintf(console, "# ok/console %s\n", padico_hostname());
  while(!done)
    {
      fprintf(console, "# PadicoConsole > ");
      fflush(console);
      c = fgets(cmd, TAB_SIZE, console);
      if(feof(console))
        {
          fprintf(console, "\n# ok/EOF\n");
          done = 1;
        }
      else if(c == NULL)
        {
          continue;
        }
      else if(cmd[0] == '\n')
        {
          continue;
        }
      else if(cmd[0] == '<')
        {
          /* TODO: it should be converted into a PADICO_TM_CMDTASK */
          struct puk_parse_entity_s pe;
          pe = puk_xml_parse_buffer(cmd, strlen(cmd), PUK_TRUST_LOCAL);
          rc = pe.rc;
        }
      else if(strncasecmp(cmd, "help\n", TAB_SIZE) == 0)
        {
          rc = padico_rc_msg("Commands are: load run unload start restart exit lsmod lsaction lstopo file inspectmod");
        }
      else if(strncasecmp(cmd, "xml\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          struct puk_parse_entity_s pe;
          fprintf(console, "# xml? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          pe = puk_xml_parse_buffer(tab, strlen(tab), PUK_TRUST_LOCAL);
          rc = pe.rc;
        }
      else if(strncasecmp(cmd, "lstopo\n", TAB_SIZE) == 0)
        {
          const char cmd[] = "<lstopo/>";
          struct puk_parse_entity_s pe = puk_xml_parse_buffer(cmd, strlen(cmd), PUK_TRUST_LOCAL);
          rc = pe.rc;
        }
      else if(strncasecmp(cmd, "lsmod\n", TAB_SIZE) == 0)
        {
          rc = puk_mod_lsmod();
        }
      else if(strncasecmp(cmd, "lsaction\n", TAB_SIZE) == 0)
        {
          rc = puk_xml_lsaction();
        }
      else if(strncasecmp(cmd, "inspectmod\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# inspectmod? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = puk_mod_inspectmod(tab);
        }
      else if(strncasecmp(cmd, "load\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# Load? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, tab);
        }
      else if(strncasecmp(cmd, "run\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# Run? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, tab, 0, NULL);
        }
      else if(strncasecmp(cmd, "unload\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# Unload? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, tab);
        }
      else if(strncasecmp(cmd, "start\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# Start? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, tab);
          if(!padico_rc_iserror(rc))
            rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, tab, 0 , NULL);
        }
      else if(strncasecmp(cmd, "restart\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# Restart? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, tab);
          if(!padico_rc_iserror(rc))
            rc = padico_tm_mod_action(PADICO_TM_MOD_RUN, tab);
          if(!padico_rc_iserror(rc))
            rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, tab);
        }
      else if(strncasecmp(cmd, "file\n", TAB_SIZE) == 0)
        {
          char tab[TAB_SIZE];
          fprintf(console, "# File? ");
          fflush(console);
          fgets(tab, TAB_SIZE, console);
          tab[strlen(tab)-1] = 0;
          padico_task_t t = padico_malloc(sizeof(struct padico_task_s));
          t->task_req = NULL;
          t->kind = PADICO_TM_TASK_FILE;
          t->tinfo.ftask.file = padico_strdup(tab);
          padico_tm_task_enqueue(t, "task file");
        }
      else if(strncasecmp(cmd, "exit\n", TAB_SIZE) == 0)
        {
          done = 1;
          padico_tm_exit();
          rc = NULL;
        }
      else
        {
          rc = padico_rc_error("unknown command. Type 'help' for help.");
        }
      if(rc == NULL)
        {
          fprintf(stderr, "ok/0\n");
        }
      else if(rc->rc)
        {
          fprintf(stderr, "err/%s\n", padico_string_get(rc->msg));
        }
      else if(rc->msg)
        {
          fprintf(stderr, "ok/%s\n", padico_string_get(rc->msg));
        }
      if(rc)
        {
          padico_rc_delete(rc);
          rc = NULL;
        }
      fflush(console);
    }
  /* padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, padico_module_self_name()); */
}
