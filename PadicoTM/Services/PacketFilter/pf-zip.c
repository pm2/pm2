/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief ZIP Packet Filter component
 */

#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include "PacketFilter.h"

#ifdef HAVE_ZLIB_H
#include <zlib.h>
#endif


/* ********************************************************* */
/* *** ZIP Packet Filter *********************************** */
/* ********************************************************* */

/** @defgroup PacketFilter_ZIP component: PacketFilter_ZIP- a packet filter that performs ZIP compression
 * @ingroup PadicoComponent
 */

static void*zpf_instantiate(puk_instance_t assembly, puk_context_t context);
static void zpf_destroy(void*state);
static void zpf_init(void);
static void zpf_encode(void*, const void*, size_t, void**, size_t*);
static void zpf_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_ZIP
 * @ingroup PacketFilter_ZIP
 */
static const struct puk_component_driver_s zip_component_driver =
  {
    .instantiate    = &zpf_instantiate,
    .destroy        = &zpf_destroy,
    .component_init = &zpf_init
  };

/** 'PacketFilter' facet for PacketFilter_ZIP
 * @ingroup PacketFilter_ZIP
 */
static const struct packetfilter_driver_s zpf_driver =
  {
    .pf_encode = &zpf_encode,
    .pf_decode = &zpf_decode
  };

/* ********************************************************* */

static void zpf_init(void)
{
  padico_print("using zlib version %s\n", zlibVersion());
}

PADICO_MODULE_COMPONENT(PacketFilter_ZIP,
                        puk_component_declare("PacketFilter_ZIP",
                                              puk_component_provides("PadicoComponent", "component", &zip_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &zpf_driver),
                                              puk_component_attr("level", "3")));

/* ********************************************************* */

/** instance status for PacketFilter_ZIP
 * @ingroup PacketFiler_ZIP
 */
struct zip_state_s
{
  int level;
  z_stream in_stream;
  z_stream out_stream;
};

static void* zpf_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct zip_state_s*state = padico_malloc(sizeof(struct zip_state_s));
  const char*s_level = puk_context_getattr(context, "level");
  assert(s_level);
  int level = atoi(s_level);
  state->level = level;
  return state;
}

static void zpf_destroy(void*state)
{
  assert(state != NULL);
  padico_free(state);
}


/* 'out' allocated by encode function
 */
static void zpf_encode(void*w,
                       const void*in, size_t in_size,
                       void**out, size_t* out_size)
{
  struct zip_state_s*state = w;
  unsigned long s = in_size + in_size / 16 + 64;
  void*p = padico_malloc(s);
  int rc = compress2(p, &s, in, in_size, state->level);
  if(rc != Z_OK)
    {
      /* compression failed: don't compress */
      padico_fatal("compression failed.\n");
      memcpy(p, in, in_size);
      s = in_size;
    }
  *out = p;
  *out_size = s;
}

/* 'out' allocated by caller
*/
static void zpf_decode(void*state,
                       const void*in, size_t in_size,
                       void*out, size_t* out_size)
{
  unsigned long s = *out_size;
  int rc = uncompress(out, &s, in, in_size);
  if(rc != Z_OK)
    {
      padico_fatal("CRC error while uncompressing- in_size=%lu; out_size=%lu; rc=%d\n",
                   (unsigned long)in_size, (unsigned long)*out_size, rc);
    }
  *out_size = s;
}
