/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief ZSTD Packet Filter component
 */

#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include "PacketFilter.h"

#ifdef HAVE_ZSTD_H
#include <zstd.h>
#endif


/* ********************************************************* */
/* *** ZSTD Packet Filter ********************************** */
/* ********************************************************* */

/** @defgroup PacketFilter_ZSTD component: PacketFilter_ZSTD - a packet filter that performs ZSTD compression
 * @ingroup PadicoComponent
 */

static void*pfzstd_instantiate(puk_instance_t assembly, puk_context_t context);
static void pfzstd_destroy(void*state);
static void pfzstd_init(void);
static void pfzstd_encode(void*, const void*, size_t, void**, size_t*);
static void pfzstd_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_ZSTD
 * @ingroup PacketFilter_ZSTD
 */
static const struct puk_component_driver_s zstd_component_driver =
  {
    .instantiate    = &pfzstd_instantiate,
    .destroy        = &pfzstd_destroy,
    .component_init = &pfzstd_init
  };

/** 'PacketFilter' facet for PacketFilter_ZSTD
 * @ingroup PacketFilter_ZSTD
 */
static const struct packetfilter_driver_s pfzstd_driver =
  {
    .pf_encode = &pfzstd_encode,
    .pf_decode = &pfzstd_decode
  };

/* ********************************************************* */

static void pfzstd_init(void)
{
  padico_print("using Zstandard library version %s\n", ZSTD_versionString());
}

PADICO_MODULE_COMPONENT(PacketFilter_ZSTD,
                        puk_component_declare("PacketFilter_ZSTD",
                                              puk_component_provides("PadicoComponent", "component", &zstd_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &pfzstd_driver),
                                              puk_component_attr("level", "20")));

/* ********************************************************* */

/** instance status for PacketFilter_ZSTD
 * @ingroup PacketFiler_ZSTD
 */
struct zstd_state_s
{
  int level;
};

static void* pfzstd_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct zstd_state_s*state = padico_malloc(sizeof(struct zstd_state_s));
  const char*s_level = puk_context_getattr(context, "level");
  assert(s_level);
  int level = atoi(s_level);
  state->level = level;
  return state;
}

static void pfzstd_destroy(void*state)
{
  assert(state != NULL);
  padico_free(state);
}

/* 'out' allocated by encode function */
static void pfzstd_encode(void*w,
                          const void*in, size_t in_size,
                          void**out, size_t* out_size)
{
  struct zstd_state_s*state = w;

  /* Determine maximum compressed size */
  size_t max_compressed_size = ZSTD_compressBound(in_size);
  void*p = padico_malloc(max_compressed_size);

  /* Perform compression */
  size_t compressed_size = ZSTD_compress(p, max_compressed_size, in, in_size, state->level);
  if(ZSTD_isError(compressed_size))
    {
      /* Compression failed*/
      padico_fatal("ZSTD compression failed: %s\n", ZSTD_getErrorName(compressed_size));
    }
  *out = p;
  *out_size = compressed_size;
}

/* 'out' allocated by caller */
static void pfzstd_decode(void*state,
                          const void*in, size_t in_size,
                          void*out, size_t* out_size)
{
  size_t decompressed_size = *out_size;

  /* Perform decompression */
  size_t rc = ZSTD_decompress(out, decompressed_size, in, in_size);
  if(ZSTD_isError(rc))
    {
      padico_fatal("ZSTD decompression failed: %s\n", ZSTD_getErrorName(rc));
    }
  *out_size = rc; /* Actual size of decompressed data */
}
