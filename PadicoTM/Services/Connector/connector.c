/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Front-ends (C and XML) for PadicoConnector
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoControl.h>

#include "Connector.h"

static int padico_connector_init(void);
static void padico_connector_finalize(void);

PADICO_MODULE_DECLARE(Connector, padico_connector_init, NULL, padico_connector_finalize,
                      "Topology", "NetSelector", "PadicoControl");


/* XML parsing handlers */
static void padico_connector_new_handler(puk_parse_entity_t e);
static void padico_connector_connect_handler(puk_parse_entity_t e);

/* ********************************************************* */

padico_rc_t padico_control_connector_new(const void**addr, size_t*addr_len)
{
  padico_topo_host_t host = NULL;
  const puk_component_t component = padico_ns_host_selector(host, NULL, puk_iface_PadicoConnector(), 0, 0);
  if(component == NULL)
    {
      return padico_rc_error("Cannot create connector: no component found\n");
    }
  puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_PadicoConnector_s r;
  puk_instance_indirect_PadicoConnector(instance, NULL, &r);
  if(r.driver == NULL)
    {
      return padico_rc_error("Cannot create connector: component=%s has no 'PadicoConnnector' facet.", component->name);
    }
  int rc = (*r.driver->connector_new)(r._status, addr, addr_len);
  if(rc)
    {
      return padico_rc_error("Error %d while invoking ConnectorNew on component %s.", rc, component->name);
    }
  return padico_rc_ok();
}

padico_rc_t padico_control_connector_connect(const void*addr, size_t addr_len)
{
  padico_topo_host_t host = NULL;
  const puk_component_t component = padico_ns_host_selector(host, NULL, puk_iface_PadicoConnector(), 0 /* port */, 0 /* IPPROTO_TCP */);
  if(component == NULL)
    {
      return padico_rc_error("Cannot create connector: no component found\n");
    }
  puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_PadicoConnector_s r;
  puk_instance_indirect_PadicoConnector(instance, NULL, &r);
  if(r.driver == NULL)
    {
      return padico_rc_error("Cannot create connector: component=%s has no 'PadicoConnnector' facet.", component->name);
    }
  int rc = (*r.driver->connector_connect)(r._status, addr, addr_len);
  if(rc)
    {
      return padico_rc_error("Error %d while invoking ConnectorConnect on component %s.", rc, component->name);
    }
  return padico_rc_ok();
}

/* ********************************************************* */

static void padico_connector_new_handler(puk_parse_entity_t e)
{
  size_t addr_len = 0;
  const void*addr = NULL;
  padico_rc_t rc = padico_control_connector_new(&addr, &addr_len);
  if(!padico_rc_iserror(rc))
    {
      const char*bytes = addr;
      size_t size = addr_len;
      char*text = puk_hex_encode(bytes, &size, NULL);
      padico_out(20, "encoded=*%s* (len=%d)\n", text, (int)size);
      puk_parse_set_rc(e, padico_rc_msg("%s", text));
      padico_free(text);
    }
  else
    {
      puk_parse_set_rc(e, rc);
    }
}

static void padico_connector_connect_handler(puk_parse_entity_t e)
{
  const char*addr = puk_parse_getattr(e, "address");
  size_t len = strlen(addr);
  assert(addr != NULL && len < 1024 && len > 0);
  char*buffer = puk_hex_decode(addr, &len, NULL);
  assert(buffer != NULL);
  padico_control_connector_connect(buffer, len);
  padico_free(buffer);
}


/* ********************************************************* */


static int padico_connector_init(void)
{
  puk_iface_register("PadicoConnector");

  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "Connector:New",
    .start_handler  = &padico_connector_new_handler,
    .required_level = PUK_TRUST_OUTSIDE
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "Connector:Connect",
    .start_handler  = &padico_connector_connect_handler,
    .required_level = PUK_TRUST_OUTSIDE
  });

  return 0;
}

static void padico_connector_finalize(void)
{
  puk_xml_delete_action("Connector:New");
  puk_xml_delete_action("Connector:Connect");
}
