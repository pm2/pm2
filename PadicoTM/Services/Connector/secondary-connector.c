/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Secondary connector- provides PadicoConnector interface
 * atop PadicoBootstrap to enable the use of primary controlers as secondary.
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>
#include <Padico/AddrDB.h>

static int padico_secondary_connector_init(void);

PADICO_MODULE_DECLARE(SecondaryConnector, padico_secondary_connector_init, NULL, NULL);

/* ********************************************************* */

static void*secondary_connector_instantiate(puk_instance_t instance, puk_context_t context);
static void secondary_connector_destroy(void*_status);

static const struct puk_component_driver_s secondary_connector_component_driver =
{
  .instantiate = &secondary_connector_instantiate,
  .destroy     = &secondary_connector_destroy
};

static int secondary_connector_new(void*_status, const void**local_addr, size_t*addr_size);
static int secondary_connector_connect(void*_status, const void*remote_addr, size_t addr_size);

static const struct padico_control_connector_driver_s secondary_connector_driver =
{
  .connector_new     = &secondary_connector_new,
  .connector_connect = &secondary_connector_connect
};

struct secondary_connector_status_s
{
  struct puk_receptacle_PadicoBootstrap_s bootstrap;
};

/* ********************************************************* */

static int padico_secondary_connector_init(void)
{
  puk_component_declare("SecondaryConnector",
                        puk_component_provides("PadicoComponent", "component", &secondary_connector_component_driver),
                        puk_component_provides("PadicoConnector", "connector", &secondary_connector_driver),
                        puk_component_uses("PadicoBootstrap", "bootstrap"));
  return 0;
}

/* ********************************************************* */

static void*secondary_connector_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct secondary_connector_status_s*status = padico_malloc(sizeof(struct secondary_connector_status_s));
  puk_context_indirect_PadicoBootstrap(instance, "bootstrap", &status->bootstrap);
  return status;
}
static void secondary_connector_destroy(void*_status)
{
  padico_free(_status);
}

/* ********************************************************* */

static int secondary_connector_new(void*_status, const void**local_addr, size_t*addr_size)
{
  const char*uuid = (const char*)padico_topo_node_getuuid(padico_topo_getlocalnode());
  *local_addr = uuid;
  *addr_size = PADICO_TOPO_UUID_SIZE + 1;
  return 0;
}

static int secondary_connector_connect(void*_status, const void*remote_addr, size_t addr_size)
{
  struct secondary_connector_status_s*status = _status;
  padico_print("connect- local=%s; remote=%s\n",
               (char*)padico_topo_node_getuuid(padico_topo_getlocalnode()),
               (char*)remote_addr);
  int rc = 0;
  const int is_server = (memcmp(padico_topo_node_getuuid(padico_topo_getlocalnode()), remote_addr, PADICO_TOPO_UUID_SIZE) > 0);
  if(is_server)
    {
      padico_print("bootstrap_new\n");
      rc = status->bootstrap.driver->bootstrap_new(status->bootstrap._status);
      padico_print("bootstrap_new done.\n");
    }
  else
    {
      padico_print("bootstrap_connect\n");
      padico_topo_uuid_t uuid = remote_addr;
      padico_topo_node_t node = padico_topo_getnodebyuuid(uuid);
      padico_topo_host_t host = padico_topo_node_gethost(node);
      const char*hostname = padico_topo_host_getname(host);
      rc = status->bootstrap.driver->bootstrap_connect(status->bootstrap._status, hostname, uuid);
      padico_print("bootstrap_connect- done\n");
    }
  return rc;
}
