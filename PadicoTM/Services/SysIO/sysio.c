/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief System I/O management
 * @ingroup SysIO
 */

#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <sched.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>

#include "SysIO.h"


#ifdef SYSIO_PROFILE
#include <Padico/Timing.h>
#endif

#ifdef HAVE_POLL_H
#  include <poll.h>
#  define ENABLE_RETRY_STRATEGY
#  define SYSIO_MAX_RETRY 100
#else
#  error "PadicoTM SysIO requires poll()"
#endif

#if defined(MARCEL) && !defined(PIOMAN)
#  error PadicoTM SysIO requires PIOMan when Marcel is enabled
#endif

static int padico_na_sysio_init(void);
static void padico_na_sysio_stop(void);

PADICO_MODULE_DECLARE(SysIO, padico_na_sysio_init, NULL, padico_na_sysio_stop);


/* *** SysIO default parameters ****************** */

#define PADICO_SYSIO_DEFAULT_INTERLEAVE_TIME 1      /* -2: no yield; -1: sched_yield; 0: yield; >0: time (usec) */
#define PADICO_SYSIO_DEFAULT_IDLE_FACTOR     -1     /* in loops without callback performed */
#define PADICO_SYSIO_DEFAULT_RETRY_FACTOR    10000  /* in bytes/milliseconds */
#define PADICO_SYSIO_DEFAULT_RETRY_SLICE     5      /* in milliseconds */
#define PADICO_SYSIO_DEFAULT_SNDBUF          262144 /* in bytes */
#define PADICO_SYSIO_DEFAULT_RCVBUF          131072 /* in bytes */
#define PADICO_SYSIO_DEFAULT_STRATEGY_RETRY  1      /* 1: retry activated; 0: no retry */
#define PADICO_SYSIO_DEFAULT_STRATEGY_LOCK   1      /* 1: lock scheduling while I/O; 0: don't lock */


/** @internal */
struct padico_sysio_s
{
  int                           fd; /**< associated file descriptor */
  volatile unsigned int  activated; /**< activation for the polling loop */
  volatile unsigned int       what; /**< what action is registered */
  volatile unsigned int     status; /**< current hardware status for r/w/e */
  padico_sysio_callback_t callback; /**< callback to invoke uppon read-polling */
  void*               callback_key; /**< arg passed to callback */
};
PUK_VECT_TYPE(padico_sysio, struct padico_sysio_s);


static void* na_sysio_thread(void*);
static void  na_sysio_rebuild_fds(void);
static int   na_sysio_update_status(int timeout);
static void  na_sysio_unlock_force(void);
#ifdef MARCEL
static int na_sysio_fastpoll_func(void*arg);
#endif

/* TODO: the ios vect should be indexed by fd number for reverse mapping
 */
#define PADICO_IO_MAX_FD 1024
/* TODO: not scalable- use getrlimit(RLIMIT_NOFILE) instead
 */


/** @internal */
static struct padico_sysio_mgr_s
{
  marcel_t         tid;     /**< thread which incarnates the io manager */
  volatile int power_on;    /**< is the thread running? */
  padico_sysio_vect_t ios;  /**< io descriptors */
  int              idle;
  int unlock_fds[2];
  padico_io_t unlock_io;
#ifdef MARCEL
  marcel_mutex_t   lock;    /**< lock for ios vector */
  struct piom_ltask ltask;
#else /* MARCEL */
  int polling_running;         /**< polling is running- don't touch fds */
  int rebuild_req;             /**< request to stop polling to rebuild fds*/
  pthread_cond_t cond;         /**< signal change on polling_running=0 or rebuild_req=1 */
  pthread_mutex_t status_lock; /**< locks access to 'polling_running' or 'rebuild_req' */
  pthread_mutex_t io_lock;     /**< locks access between multiple padico_io_* calls */
#endif /* MARCEL */
  struct sysio_pollfd_s
  {
    struct pollfd poll_fds[PADICO_IO_MAX_FD]; /**< persistent pollfd structure */
    int nfd; /**< size of the above array actually used */
  } pollfd;
  /* tuning parameters */
  int              interleave_factor; /**< time to wait between system pollings */
  int              idle_factor;       /**< number of empty pollings before switching into 'idle' mode */
  int              retry_factor;      /**< in bytes/msec. (typical: 50000) */
  int              retry_slice;       /**< in bytes */
  int              default_SNDBUF;
  int              default_RCVBUF;
  int              strategy_retry;
  int              strategy_lock;
} na_ios;

#ifdef MARCEL
static volatile marcel_t lock_owner = NULL;
#endif

static inline void na_sysio_lock(void)
{
#ifdef MARCEL
  if(lock_owner == marcel_self())
    padico_fatal("dealock in sysio\n");
  marcel_mutex_lock(&na_ios.lock);
  if(na_ios.ltask.state != PIOM_LTASK_STATE_NONE)
    piom_ltask_mask(&na_ios.ltask);
  lock_owner = marcel_self();
#else /* MARCEL */
  pthread_mutex_lock(&na_ios.status_lock);
  na_ios.rebuild_req++;
  while(na_ios.polling_running)
    {
      na_sysio_unlock_force();
      pthread_cond_wait(&na_ios.cond, &na_ios.status_lock);
    }
  pthread_mutex_unlock(&na_ios.status_lock);
  pthread_mutex_lock(&na_ios.io_lock);
  assert(na_ios.rebuild_req > 0);
  assert(na_ios.polling_running == 0);
#endif /* MARCEL */
}
static inline void na_sysio_unlock(void)
{
#ifdef MARCEL
  lock_owner = NULL;
  if(na_ios.ltask.state != PIOM_LTASK_STATE_NONE)
    piom_ltask_unmask(&na_ios.ltask);
  marcel_mutex_unlock(&na_ios.lock);
#else
  pthread_mutex_unlock(&na_ios.io_lock);
  pthread_mutex_lock(&na_ios.status_lock);
  na_ios.rebuild_req--;
  assert(na_ios.rebuild_req >= 0);
  assert(na_ios.polling_running == 0);
  pthread_cond_broadcast(&na_ios.cond);
  pthread_mutex_unlock(&na_ios.status_lock);
#endif
}

/* ********************************************************* */

/** @internal
 * for statistics
 */
padico_sysio_vect_t padico_na_internal_getios(void)
{
  return na_ios.ios;
}


/* *** public API ****************************************** */

padico_io_t padico_io_register(int fd, unsigned int what, padico_sysio_callback_t callback, void*key)
{
  struct padico_sysio_s io;
  padico_sysio_vect_itor_t v;
  padico_io_t pio = NULL;
  io.fd           = fd;
  io.what         = what;
  io.callback     = callback;
  io.callback_key = key;
  io.activated    = 0;
  io.status       = 0;

  padico_out(40, "register fd=%d what=%d\n", fd, what);
  if(fd < 0)
    {
      padico_fatal("padico_io_register() fd=%d what=%d\n", fd, what);
    }
  if(fd >= PADICO_IO_MAX_FD)
    {
      padico_fatal("trying to register fd=%d; max fd = %d", fd, PADICO_IO_MAX_FD);
    }

  na_sysio_lock();
  for(v = padico_sysio_vect_begin(na_ios.ios);
      v!= padico_sysio_vect_end(na_ios.ios);
      v = padico_sysio_vect_next(v))
    {
      if(v->fd == -1)
        pio = v;
    }
  if(pio)
    {
      *pio = io;
    }
  else
    {
      if(padico_sysio_vect_empty(na_ios.ios))
        {
          padico_sysio_vect_reserve(na_ios.ios, PADICO_IO_MAX_FD);
        }
      padico_sysio_vect_push_back(na_ios.ios, io);
      pio = padico_sysio_vect_rbegin(na_ios.ios);
      assert(padico_sysio_vect_size(na_ios.ios) < PADICO_IO_MAX_FD);
      /** @bug Some troubles in this implementation...
       * Since padico_io_t are static pointers into a dynamic vector,
       * we must be sure that the I/O vector is *never* resized!
       * It's too strong a constraint for scalability. Static
       * assertions about a dynamic vector are obviously not the
       * way to go. It's enough for testing, though...
       */
    }
  na_sysio_rebuild_fds();
  na_sysio_unlock();
  padico_out(40, "unlock fd=%d what=%d\n", fd, what);
  return pio;
}

int padico_sysio_refresh(padico_io_t io)
{
  int rc = 0;
  padico_out(40, "na_ios.ios[%p]\n", io);
#ifdef MARCEL
  piom_polling_force();
#warning TODO- no guarantee that *our* ltask is scheduled here
#else
  na_sysio_lock();
  na_sysio_update_status(0);
  rc = io->status & io->what;
  na_sysio_unlock();
#endif
  return rc;
}

void padico_io_activate(padico_io_t io)
{
  na_sysio_lock();
  padico_out(40, "activate na_ios.ios[%p] activated=%d what=%d fd=%d\n",
             io, io->activated, io->what, io->fd);
  io->activated = io->what;
  na_sysio_rebuild_fds();
  na_sysio_unlock();
}

void padico_io_deactivate(padico_io_t io)
{
  na_sysio_lock();
  padico_out(40, "deactivate na_ios.ios[%p] activated=%d what=%d fd=%d\n",
             io, io->activated, io->what, io->fd);
  io->activated = 0;
  na_sysio_rebuild_fds();
  na_sysio_unlock();
}

void padico_io_unregister(padico_io_t io)
{
  padico_out(40, "unregister na_ios.ios[%p] activated=%d fd=%d\n",
               io, io->activated, io->fd);
  na_sysio_lock();
  assert(io->fd != -1);
  io->activated = 0;
  io->status    = 0;
  io->what      = 0;
  io->fd        = -1;
  na_sysio_rebuild_fds();
  na_sysio_unlock();
}

int padico_sysio_getfd(padico_io_t io)
{
  return io->fd;
}


/* *** Internals ******************************************* */

/** na_sysio scavenger- detect (and recover) bad file descriptors
 */
static void na_sysio_scavenger(void)
{
  struct timeval timeout = {.tv_sec = 0, .tv_usec = 0};
  int rc, err;
  padico_sysio_vect_itor_t v;
  for(v = padico_sysio_vect_begin(na_ios.ios);
      v!= padico_sysio_vect_end(na_ios.ios);
      v = padico_sysio_vect_next(v))
    {
      if(v->fd != -1)
        {
          fd_set test_fd;
          FD_ZERO(&test_fd);
          FD_SET(v->fd, &test_fd);
          rc  = PUK_ABI_FSYS_WRAP(select)(v->fd+1, &test_fd, NULL, NULL, &timeout);
          err = __puk_abi_wrap__errno;
          if(rc == -1)
            {
              padico_print("fd=%d error- %s\n", v->fd, strerror(err));
            }
          else
            {
              padico_print("fd=%d ok.\n", v->fd);
            }
        }
    }
  padico_fatal("EBADF in select() -- PadicoTM internal error.\n");
}

/* ********************************************************* */

static void na_sysio_rebuild_fds(void)
{
  padico_sysio_vect_itor_t v;
  na_ios.pollfd.nfd = 0;
  for(v = padico_sysio_vect_begin(na_ios.ios);
      v!= padico_sysio_vect_end(na_ios.ios);
      v = padico_sysio_vect_next(v))
    {
      if(v->fd != -1)
        {
          struct pollfd p =
            {
              .fd      = v->fd,
              .events  = 0,
              .revents = 0
            };
          if(v->activated & PADICO_IO_EVENT_READ)
            p.events |= POLLIN;
          if(v->activated & PADICO_IO_EVENT_WRITE)
            p.events |= POLLOUT;
          if(v->activated & PADICO_IO_EVENT_ERROR)
            p.events |= POLLERR;
          na_ios.pollfd.poll_fds[na_ios.pollfd.nfd] = p;
          na_ios.pollfd.nfd++;
        }
    }
}

/* poll()-based update status */
static int na_sysio_update_status(int timeout)
{

#ifdef SYSIO_PROFILE
  static int refresh_count = 0;
  padico_timing_t t1, t2;
  refresh_count++;
  padico_timing_get_tick(&t1);
#endif

  int ready;
 retry_poll:
  ready = 0;
  int i;
  for(i = 0; i < na_ios.pollfd.nfd; i++)
    na_ios.pollfd.poll_fds[i].revents = 0;
  int rc  = PUK_ABI_FSYS_WRAP(poll)(na_ios.pollfd.poll_fds, na_ios.pollfd.nfd, timeout);
  int err = __puk_abi_wrap__errno;
  if(rc >= 0)
    {
      int error = 0;
      padico_sysio_vect_itor_t v;
      int j = 0;
      for(v = padico_sysio_vect_begin(na_ios.ios);
          v!= padico_sysio_vect_end(na_ios.ios);
          v = padico_sysio_vect_next(v))
        {
          const struct pollfd p = na_ios.pollfd.poll_fds[j];
          if(j < na_ios.pollfd.nfd && p.fd == v->fd)
            {
              v->status =
                ((p.revents & POLLIN) ?PADICO_IO_EVENT_READ :0) |
                ((p.revents & POLLOUT)?PADICO_IO_EVENT_WRITE:0) |
                ((p.revents & POLLERR)?PADICO_IO_EVENT_ERROR:0);
              if(p.revents & (POLLERR|POLLHUP|POLLNVAL))
                {
                  error = 1;
                  (v)->activated = 0;
                }
              j++;
            }
          else
            {
              v->status = 0;
            }
          if(v->activated & v->status)
            {
              ready++;
            }
        }
      if(error)
        {
          na_sysio_rebuild_fds();
        }
      /* infinite timeout should *always* lead to at least one io ready */
      if(!(timeout >= 0 || ready > 0 || error))
        goto retry_poll;
    }
  else
    {
      /* rc < 0 -- error in poll() */
      if(err == EBADF)
        na_sysio_scavenger();
      else if((err == EINTR)||(err == EAGAIN))
        goto retry_poll;
      else
        padico_warning("error in poll() rc=%d errno=%d (%s)\n",
                       rc, err, strerror(err));
    }

#ifdef SYSIO_PROFILE
  padico_timing_get_tick(&t2);
  if(refresh_count % 1000 == 0)
    {
      double t = padico_timing_diff_usec(&t1, &t2);
      static padico_timing_t last_timestamp;
      padico_print("profiling- refresh average: %g usec. (count=%d; delta=%g usec.)\n",
                   t, refresh_count, padico_timing_diff_usec(&last_timestamp, &t2)/1000.0);
      last_timestamp = t2;
    }
#endif

  return ready;
}

static void* na_sysio_thread(void*dummy __attribute__((unused)))
{
  padico_tm_thread_givename("PadicoTM_SysIO");
  na_ios.tid = marcel_self();
  padico_trace("running\n");

  while(na_ios.power_on)
    {
      int ready = 0;
      padico_out(90, "looping\n");
#ifdef MARCEL
      /* PIOMan- wait event */
      padico_out(20, "before piom_ltask_wait()\n");
      piom_ltask_wait(&na_ios.ltask);
      padico_out(20, "after piom_ltask_wait() -----------------------------------------------------\n");
      ready = 1;
#else /* MARCEL */
      /* System pthread */
      pthread_mutex_lock(&na_ios.status_lock);
      while(na_ios.rebuild_req)
        pthread_cond_wait(&na_ios.cond, &na_ios.status_lock);
      na_ios.polling_running = 1;
      pthread_mutex_unlock(&na_ios.status_lock);

      ready = na_sysio_update_status(-1);

#endif /* MARCEL */
      if(ready > 0)
        {
          padico_out(90, "performing callbacks...\n");
          padico_sysio_vect_itor_t v;
          int is_idle = 1;
          int need_rebuild = 0;
#ifdef MARCEL
          na_sysio_lock();
#endif
          for(v = padico_sysio_vect_begin(na_ios.ios);
              v!= padico_sysio_vect_end(na_ios.ios);
              v = padico_sysio_vect_next(v))
            {
              const unsigned masked_status = (v->activated & v->status);
              if(masked_status)
                {
                  int again;
                  padico_out(40, "invoking callback for fd=%d (status=%d, masked=%u)\n",
                             v->fd, v->status, masked_status);
                  if(!v->activated)
                    {
                      padico_warning("race condition detected in NetAccess na_sysio\n");
                      again = 0;
                    }
                  else
                    {
#ifdef MARCEL
                      //                      piom_ltask_unmask(&na_ios.ltask);
#endif
                      again = (v->callback)(v->fd, v->callback_key);
#ifdef MARCEL
                      //                      piom_ltask_mask(&na_ios.ltask);
#endif
                      is_idle = 0;
                    }
                  if(!again)
                    {
                      v->activated = 0;
                      need_rebuild = 1;
                    }
                  padico_out(90, "return from callback for fd=%d again=%d\n",
                             v->fd, again);
                }
            }
          if(need_rebuild)
            na_sysio_rebuild_fds();
#ifdef MARCEL
          na_sysio_unlock();
#endif
          if(is_idle)
            na_ios.idle++;
          else
            na_ios.idle = 0;
          padico_out(90, "callbacks done.\n");
        }
      else
        {
          na_ios.idle++;
        }
      padico_out(90, "loop end- ready=%d; idle=%d\n", ready, na_ios.idle);
      ready = 0;
#ifndef MARCEL
      pthread_mutex_lock(&na_ios.status_lock);
      na_ios.polling_running = 0;
      pthread_cond_broadcast(&na_ios.cond);
      pthread_mutex_unlock(&na_ios.status_lock);
#else
      if(na_ios.power_on)
        {
          marcel_mutex_lock(&na_ios.lock);
          piom_ltask_create(&na_ios.ltask, &na_sysio_fastpoll_func, NULL, PIOM_LTASK_OPTION_REPEAT);
          piom_ltask_submit(&na_ios.ltask);
          marcel_mutex_unlock(&na_ios.lock);
        }
#endif
    }
  return NULL;
}

/* ********************************************************* */

int padico_sysio_errno(void)
{
  return __puk_abi_wrap__errno;
}

static void na_sysio_tune_socket(int fd)
{
#ifdef SOL_TCP
  static const int proto_number = SOL_TCP;
#else
#warning "****** SOL_TCP undefined. Taking protocol number in /etc/protocols"
  static int proto_number = -1;
  if(proto_number == -1)
    {
      static const char proto_name[] = "tcp";
      struct protoent* p = getprotobyname(proto_name);
      proto_number = p->p_proto;
    }
#endif /* SOL_TCP */
  /* send and receive windows size */
  int val = na_ios.default_SNDBUF;
  PUK_ABI_FSYS_WRAP(setsockopt)(fd, SOL_SOCKET, SO_SNDBUF, &val, sizeof(val));
  val = na_ios.default_RCVBUF;
  PUK_ABI_FSYS_WRAP(setsockopt)(fd, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val));
  /* disable Nagle algorithm as default- by default, sockets are tuned for TCP */
  val = 1;
  PUK_ABI_FSYS_WRAP(setsockopt)(fd, proto_number, TCP_NODELAY,  &val, sizeof(val));
}

int padico_sysio_socket(int domain, int type, int protocol)
{
  int fd = -1, rc;
  padico_out(50, "domain=%d; type=%d; protocol=%d\n", domain, type, protocol);
  rc = PUK_ABI_FSYS_WRAP(socket)(domain, type, protocol);
  if(rc >= 0)
    {
      fd = rc;
      {
        na_sysio_tune_socket(fd);
      }
    }
  padico_out(50, "fd=%d\n", fd);
  return fd;
}

ssize_t padico_sysio_read(int fildes, void *buf, ssize_t nbyte)
{
  int rc;
  int err = -1;
  padico_out(40, "reading %d bytes fd=%d\n", (int)nbyte, fildes);
  rc = PUK_ABI_FSYS_WRAP(read)(fildes, buf, nbyte);
  err = __puk_abi_wrap__errno;
  padico_out(40, "read() rc=%d (%s)\n", rc, strerror(err));
  if(rc < 0)
    goto exit_read;
#ifdef ENABLE_RETRY_STRATEGY
  if(na_ios.strategy_retry && rc > 0 && rc < nbyte)
    {
      int nretry = 0;
      int rc2;
      int t2;
      struct pollfd p =
        {
          .fd = fildes,
          .events = POLLIN
        };
      while(rc < nbyte)
      {
        int retry_timeout = (nbyte-rc)/na_ios.retry_factor;
        do {
          assert(retry_timeout >= 0);
          if(retry_timeout >= na_ios.retry_slice)
            t2 = na_ios.retry_slice;
          else t2 = retry_timeout;
          rc2 = PUK_ABI_FSYS_WRAP(poll)(&p, 1, t2);
          err = __puk_abi_wrap__errno;
          retry_timeout -= t2;
          if(nretry++ > SYSIO_MAX_RETRY)
            {
              goto exit_read;
            }
        } while((rc2 == -1 && err == EINTR) ||
                (rc2 == 0 && retry_timeout > 0));
        if((rc2 > 0) && ((p.revents & POLLIN) != 0))
          {
            rc2 = PUK_ABI_FSYS_WRAP(read)(fildes, buf+rc, nbyte-rc);
            err = __puk_abi_wrap__errno;
            padico_out(40, "read() rc=%d (%s) -- retry\n", rc2, strerror(err));
            if(rc2 > 0)
              {
                rc += rc2;
                retry_timeout = (nbyte-rc)/na_ios.retry_factor;
              }
            else if(rc2 == -1 && err == EINTR)
              {
              }
            else
              {
                break;
              }
          }
        else
          {
            break;
          }
      }
    }
#endif // ENABLE_RETRY_STRATEGY
 exit_read:
  padico_out(40,"read() ok rc=%d fd=%d.\n", rc, fildes);
  return rc;
}

ssize_t padico_sysio_writev(int fildes, const struct iovec *iov, int iovcnt)
{
  int rc;
  rc = PUK_ABI_FSYS_WRAP(writev)(fildes, iov, iovcnt);
  if (rc < 0)
    {
        int err = err = __puk_abi_wrap__errno;
    }
  return rc;
}

ssize_t padico_sysio_write(int fildes, const void *buf, ssize_t nbyte)
{
  int rc;
  padico_out(60, "writing %d bytes fd=%d\n", (int)nbyte, fildes);
  rc = PUK_ABI_FSYS_WRAP(write)(fildes, buf, nbyte);
  padico_out(60, "write() rc=%d\n", rc);
  if(rc < 0)
    goto exit_write;
#ifdef ENABLE_RETRY_STRATEGY
  if(na_ios.strategy_retry && rc > 0 && rc < nbyte)
    {
      int nretry = 0;
      int rc2;
      int t2;
      struct pollfd p =
        {
          .fd = fildes,
          .events = POLLOUT
        };
      while(rc < nbyte)
      {
        int err = -1;
        int retry_timeout = (nbyte-rc)/na_ios.retry_factor;
        do {
          assert(retry_timeout >= 0);
          if(retry_timeout >= na_ios.retry_slice)
            t2 = na_ios.retry_slice;
          else t2 = retry_timeout;
          rc2 = PUK_ABI_FSYS_WRAP(poll)(&p, 1, t2);
          err = __puk_abi_wrap__errno;
          retry_timeout -= t2;
          if(nretry++ > SYSIO_MAX_RETRY)
            {
              goto exit_write;
            }
        } while((rc2 == -1 && err == EINTR) ||
                (rc2 == 0 && retry_timeout > 0));
        if((rc2 > 0) && ((p.revents & POLLOUT) != 0))
          {
            rc2 = PUK_ABI_FSYS_WRAP(write)(fildes, buf+rc, nbyte-rc);
            err = __puk_abi_wrap__errno;
            padico_out(60, "write() rc=%d -- retry\n", rc2);
            if(rc2 > 0)
              {
                rc += rc2;
                retry_timeout = (nbyte-rc)/na_ios.retry_factor;
              }
            else if(rc2 == -1 && err == EINTR)
              {
              }
            else
              {
                break;
              }
          }
        else
          {
            break;
          }
      }
    }
#endif // ENABLE_RETRY_STRATEGY
 exit_write:
  if(na_ios.strategy_lock)
    {
      //      enable_preemption();
    }
  return rc;
}
int padico_sysio_shutdown_wr(int s)
{
  int rc;
  padico_out(50, "fd=%d\n", s);
  rc = PUK_ABI_FSYS_WRAP(shutdown)(s, SHUT_WR);
  return rc;
}

int padico_sysio_close(int fd)
{
  int rc;
  padico_out(50, "fd=%d\n", fd);
  rc = PUK_ABI_FSYS_WRAP(close)(fd);
  return rc;
}

int padico_sysio_accept(int s, struct sockaddr*addr, socklen_t*addrlen)
{
  padico_out(50, "fd=%d\n", s);
  const int rc = PUK_ABI_FSYS_WRAP(accept)(s, addr, addrlen);
  na_sysio_tune_socket(rc);
  return rc;
}

ssize_t padico_sysio_in(int fildes, void *buf, size_t nbyte)
{
  int done = 0;
  while(done < nbyte)
    {
      int rc = padico_sysio_read(fildes, buf+done, nbyte - done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        return done;
      else
        return rc;
    }
  return done;
}

ssize_t padico_sysio_out(int fildes, const void *buf, size_t nbyte)
{
  int done = 0;
  while(done < nbyte)
    {
      int rc = padico_sysio_write(fildes, buf+done, nbyte-done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        return done;
      else
        return rc;
      if(done < nbyte)
        {
          padico_tm_yield();
        }
    }
  return done;
}

ssize_t padico_sysio_outv(int fildes, const struct iovec *buf, int iovcnt)
{
  int rc = padico_sysio_writev(fildes, buf, iovcnt);
  return rc;
}

int padico_sysio_eos(int fd)
{
  char c;
  const int rc = PUK_ABI_FSYS_WRAP(recv)(fd, &c, sizeof(c), MSG_PEEK);
  return (rc == 0);
}

int padico_sysio_connected_inet_socket(const struct in_addr*in_addr, int port)
{
  int s = padico_sysio_socket(PF_INET, SOCK_STREAM, 0);
  if(s >= 0)
    {
      int rc = -1;
      const struct sockaddr_in addr =
        {
          .sin_family = AF_INET,
          .sin_addr   = *in_addr,
          .sin_port   = htons(port)
        };
      rc = PUK_ABI_FSYS_WRAP(connect)(s, (const struct sockaddr*)&addr, sizeof(addr));
      if(rc != 0)
        {
          s = rc;
          PUK_ABI_FSYS_WRAP(close)(s);
        }
      else
        {
          na_sysio_tune_socket(s);
        }
    }
  return s;
}

int padico_sysio_connected_inet_socket_sockaddr(const struct sockaddr* addr)
{
  int s = padico_sysio_socket(PF_INET, SOCK_STREAM, 0);
  if(s >= 0)
    {
      int rc = -1;
      rc = PUK_ABI_FSYS_WRAP(connect)(s, addr, sizeof(struct sockaddr));
      if(rc != 0)
        {
          s = rc;
          PUK_ABI_FSYS_WRAP(close)(s);
        }
      else
        {
          na_sysio_tune_socket(s);
        }
    }
  return s;
}

int padico_sysio_bound_inet_socket(int*port)
{
  int s = padico_sysio_socket(PF_INET, SOCK_STREAM, 0);
  int default_port = htons(0);
  if(port == NULL)
    port = &default_port;
  if(s >= 0)
    {
      int val = 1;
      int rc  = PUK_ABI_FSYS_WRAP(setsockopt)(s, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      val = 1;
      rc  = PUK_ABI_FSYS_WRAP(setsockopt)(s, SOL_TCP, TCP_NODELAY, &val, sizeof(val));
      struct sockaddr_in addr =
        {
          .sin_family = AF_INET,
          .sin_port = htons(*port),
          .sin_addr.s_addr = htonl(INADDR_ANY)
        };
      socklen_t addr_len = sizeof(addr);
      rc = PUK_ABI_FSYS_WRAP(bind)(s, (struct sockaddr*)&addr, addr_len);
      if(rc != 0)
        {
          PUK_ABI_FSYS_WRAP(close)(s);
          s = rc;
        }
      else
        {
          rc = PUK_ABI_FSYS_WRAP(getsockname)(s, (struct sockaddr*)&addr, &addr_len);
          if(rc != 0)
            {
              PUK_ABI_FSYS_WRAP(close)(s);
              s = rc;
            }
          *port = (int)(ntohs(addr.sin_port));
          PUK_ABI_FSYS_WRAP(listen)(s, 1);
        }
    }
  return s;
}

/* ********************************************************* */

#define SHOW_ATTR(NAME, RC) (RC) = padico_rc_add((RC), padico_rc_msg("\n  %s=\"%d\"", #NAME, na_ios.NAME))
static padico_rc_t na_sysio_dump_params(void)
{
  padico_rc_t rc = padico_rc_msg("<SysIO:tune");
  SHOW_ATTR(interleave_factor, rc);
  SHOW_ATTR(idle_factor,       rc);
  SHOW_ATTR(retry_factor,      rc);
  SHOW_ATTR(default_SNDBUF,    rc);
  SHOW_ATTR(default_RCVBUF,    rc);
  SHOW_ATTR(retry_slice,       rc);
  SHOW_ATTR(strategy_retry,    rc);
  SHOW_ATTR(strategy_lock,     rc);
  rc = padico_rc_add(rc, padico_rc_msg(" />\n"));
  return rc;
}

#define GEN_ATTR(NAME, E) \
  { const char*tmp = puk_parse_getattr(E, #NAME); \
    if(tmp!=NULL) {na_ios.NAME = atoi(tmp);}}

static void na_sysiotune_start_handler(puk_parse_entity_t e)
{
  GEN_ATTR(interleave_factor, e);
  GEN_ATTR(idle_factor,       e);
  GEN_ATTR(retry_factor,      e);
  GEN_ATTR(default_SNDBUF,    e);
  GEN_ATTR(default_RCVBUF,    e);
  GEN_ATTR(retry_slice,       e);
  GEN_ATTR(strategy_retry,    e);
  GEN_ATTR(strategy_lock,     e);
}
static struct puk_tag_action_s na_sysiotune_action =
{
  .xml_tag        = "SysIO:tune",
  .start_handler  = &na_sysiotune_start_handler,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_CONTROL
};

static void na_sysiogetparams_end_handler(puk_parse_entity_t e)
{
  puk_parse_set_rc(e, na_sysio_dump_params());
}
static struct puk_tag_action_s na_sysiogetparams_action =
{
  .xml_tag        = "SysIO:getparams",
  .start_handler  = NULL,
  .end_handler    = &na_sysiogetparams_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

#ifdef MARCEL
static int na_sysio_fastpoll_func(void*arg)
{
  const int ready = na_sysio_update_status(0);
  padico_out(90, "nready = %d\n", ready);
  if(ready > 0)
  {
    piom_ltask_completed(&na_ios.ltask);
  }
  return 0;
}

#endif /* MARCEL */

/** callback called upon write in unlock_io */
static int na_sysio_unlock_callback(int fd, void*key __attribute__((unused)))
{
  char a;
  padico_sysio_in(fd, &a, sizeof(a));
  return 1;
}

/** force poll() to unlock to update fds table */
static void na_sysio_unlock_force(void)
{
  const char a = 0;
  padico_sysio_out(na_ios.unlock_fds[1], &a, sizeof(a));
}


/* *** status inspector ************************************ */

padico_rc_t padico_sysio_stat(void)
{
  padico_rc_t rc = padico_rc_msg("<SysIO:status>\n");
  /* NetAccess: SysIO */
  padico_sysio_vect_t ios = padico_na_internal_getios();
  padico_sysio_vect_itor_t v;
  for(v = padico_sysio_vect_begin(ios);
      v!= padico_sysio_vect_end(ios);
      v = padico_sysio_vect_next(v))
    {
      rc = padico_rc_add(rc, padico_rc_msg("    <SysIO:desc io=\"%p\" fd=\"%d\" activated=\"%d\" what=\"%d\" status=\"%d\"/>\n",
                                           v,v->fd, v->activated, v->what, v->status));
    }
  rc = padico_rc_add(rc, padico_rc_msg("</SysIO:status>"));
  padico_print("dumping status:\n");
  padico_rc_show(rc);
  return rc;
}

static void padico_sysio_stat_handler(puk_parse_entity_t e)
{
  padico_rc_t rc = padico_sysio_stat();
  puk_parse_set_rc(e, rc);
}

static const struct puk_tag_action_s sysio_getstats_action =
{
  .xml_tag        = "SysIO:getstats",
  .start_handler  = NULL,
  .end_handler    = &padico_sysio_stat_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static int padico_na_sysio_init(void)
{
  /* init parameters */
  na_ios.interleave_factor = PADICO_SYSIO_DEFAULT_INTERLEAVE_TIME;
  na_ios.idle_factor       = PADICO_SYSIO_DEFAULT_IDLE_FACTOR;
  na_ios.retry_factor      = PADICO_SYSIO_DEFAULT_RETRY_FACTOR;
  na_ios.retry_slice       = PADICO_SYSIO_DEFAULT_RETRY_SLICE;
  na_ios.default_SNDBUF    = PADICO_SYSIO_DEFAULT_SNDBUF;
  na_ios.default_RCVBUF    = PADICO_SYSIO_DEFAULT_RCVBUF;
  na_ios.strategy_retry    = PADICO_SYSIO_DEFAULT_STRATEGY_RETRY;
  na_ios.strategy_lock     = PADICO_SYSIO_DEFAULT_STRATEGY_LOCK;
  na_ios.ios = padico_sysio_vect_new();
  na_ios.power_on = 1;
  na_ios.idle = 0;
#ifdef MARCEL
  marcel_mutex_init(&na_ios.lock, NULL);
  piom_ltask_create(&na_ios.ltask, &na_sysio_fastpoll_func, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&na_ios.ltask);
#else /* MARCEL */
  pthread_mutex_init(&na_ios.status_lock, NULL);
  pthread_mutex_init(&na_ios.io_lock, NULL);
  pthread_cond_init(&na_ios.cond, NULL);
  na_ios.rebuild_req = 0;
  na_ios.polling_running = 0;
#endif /* MARCEL */
  /* register tuning commands */
  puk_xml_add_action(na_sysiotune_action);
  puk_xml_add_action(na_sysiogetparams_action);
  puk_xml_add_action(sysio_getstats_action);
#ifdef SYSIO_PROFILE
  padico_out(puk_verbose_notice, "********** SysIO profiling activated. **********\n");
#endif

  /* mask SIGPIPE- EPIPE is enough to detect closed connections  */
  struct sigaction sa;
  sigaction(SIGPIPE, NULL, &sa);
  sa.sa_handler = SIG_IGN;
  sigaction(SIGPIPE, &sa, NULL);

  PUK_ABI_FSYS_WRAP(pipe)(na_ios.unlock_fds);
  na_ios.unlock_io = padico_io_register(na_ios.unlock_fds[0], PADICO_IO_EVENT_READ, &na_sysio_unlock_callback, NULL);
  padico_io_activate(na_ios.unlock_io);

  /* start polling */
  na_sysio_rebuild_fds();
  marcel_create(&na_ios.tid, NULL, &na_sysio_thread, NULL);


  return 0;
}

static void padico_na_sysio_stop(void)
{
  na_sysio_lock();
  na_ios.power_on = 0;
#ifdef MARCEL
  piom_ltask_completed(&na_ios.ltask);
#endif
  na_sysio_unlock();
  na_sysio_unlock_force();
  marcel_join(na_ios.tid, NULL);
  padico_sysio_vect_delete(na_ios.ios);
}
