/* token-ring.c
 */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <Padico/VIO.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>

int data_check_init(void);
int data_check_run(int argc, char**argv);
void data_check_finalize(void);


PADICO_MODULE_DECLARE(data_check, data_check_init, data_check_run, data_check_finalize);

#define DEFAULT_PORT   5040
#define MAX_LEN        128*1024*1024
#define ITER           15
#define SEED           42

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int fd;
static int server_fd;
static int size;
static int rank;
padico_topo_node_t next;

int data_check_init(void)
{
  size = padico_na_size();
  rank = padico_na_rank();
  const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());

  next = padico_topo_session_getnodebyrank(session, (rank+1));

  int rc = -1, err = -1;
  if(rank == 0)
    {
      fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(next));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port + (rank + 1));
      do
        {
          padico_print("datacheck: Connecting to server...\n");
          rc = padico_vio_connect(fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          if(rc != 0)
            {
              err = padico_vio_errno;
              padico_warning("datacheck: cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
            }
          else
            {
              padico_print("datacheck: Client connected to %s:%d.\n",
                           inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
            }
        }
      while(rc != 0);

    }
  if(rank == 1)
    {
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port + rank);
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("Cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error port:%d\n", port);
          return -1;
        }
      padico_print("Server ready\n");
      fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(fd < 0)
        {
          err = padico_vio_errno;
          padico_warning("TOKEN-RING: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("Received connection from %s:%d.\n",
                   inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
      padico_vio_close(server_fd);
    }

  return 0;
}

void data_check_finalize(void)
{
  padico_vio_close(fd);
}

static size_t findfirstdiff(const int*b1, const int*b2, size_t len)
{
  size_t cur = 0;
  for(cur = 0; cur < len / sizeof(int); cur++)
    {
      if(b1[cur] != b2[cur])
        break;
    }
  return cur;
}

static void data_check_in(int fildes, void *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = padico_vio_read(fildes, buf + done, nbyte - done);

      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
    }
  assert(done == nbyte);
}

static void data_check_out(int fildes, const void *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = padico_vio_write(fildes, buf + done, nbyte - done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
    }
  assert(done == nbyte);
}

static void client()
{
  puk_component_t component = padico_vio_component(fd);
  padico_string_t c = puk_component_serialize(component);
  padico_print("datacheck: component used is %s\n", padico_string_get(c));
  padico_string_delete(c);
  srand(SEED);
  int*buffer = malloc(MAX_LEN);
  int i = 0;
  size_t len = 0;
  int ack;
  for(i = 0; i < MAX_LEN / sizeof(int); i++)
    {
      buffer[i] = rand();
    }
  for(len = sizeof(int); len < MAX_LEN; len *= 2)
    {
      for(i = 0; i < ITER; i++)
        {
          data_check_out(fd, buffer, len);
          data_check_in(fd, &ack, sizeof(ack));
          if(ack == 0)
            break;
        }
    }
  free(buffer);
}

static int server()
{
  srand(SEED);
  int i;
  int ack;
  size_t len = 0;
  int*source = malloc(MAX_LEN);
  int*buffer = malloc(MAX_LEN);
  for(i = 0; i < MAX_LEN / sizeof(int); i++)
    {
      source[i] = rand();
    }
  for(len = sizeof(int); len < MAX_LEN; len *= 2)
    {
      for(i = 0; i < ITER; i++)
        {
          data_check_in(fd, buffer, len);
          if(memcmp(buffer, source, len))
            {
              printf("datacheck: received buffer and source buffer are different\n"
                     "datacheck: fisrt error occur at %d\n", findfirstdiff(buffer, source, len));
              ack = 0;
              data_check_out(fd, &ack, sizeof(ack));
              return 1;
            }
          else
            {
              ack = 1;
              data_check_out(fd, &ack, sizeof(ack));
            }
        }
      padico_print("datacheck: len=%d pass\n", len);
    }
  return 0;
  free(buffer);
}

int data_check_run(int argc, char**argv)
{
  if(rank == 0)
    {
      client();
    }
  if(rank == 1)
    {
      if(server())
        {
          printf("TEST_FAILED\n");
          return 1;
        }
      else
        printf("TEST_SUCCESS\n");
    }
  return 0;
}
