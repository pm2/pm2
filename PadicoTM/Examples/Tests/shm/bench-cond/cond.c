
/*
 * PM2: Parallel Multithreaded Machine
 * Copyright (C) 2001 "the PM2 team" (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <tbx.h>
#include <values.h>

static pthread_cond_t cond;
static pthread_mutex_t mutex;
static sem_t sem;

static double time_min = DBL_MAX;
static double time_max = 0.0;
static double time_total = 0.0;

static void* f(void* arg)
{
  int n = (int) (intptr_t) arg;
  tbx_tick_t t1, t2;

  pthread_mutex_lock(&mutex);
  sem_post(&sem);
  TBX_GET_TICK(t1);
  while (--n)
    {
      pthread_cond_wait(&cond, &mutex);
      pthread_cond_signal(&cond);
    }
  TBX_GET_TICK(t2);
  pthread_mutex_unlock(&mutex);

  const double delay = TBX_TIMING_DELAY(t1, t2) / (2 * (double) (intptr_t) arg);
  printf("cond time =  %fus\n", delay);
  if(delay > time_max)
    time_max = delay;
  if(delay < time_min)
    time_min = delay;
  time_total += delay;
  return NULL;
}

static void bench_cond(unsigned nb)
{
        pthread_t pid;
        pthread_condattr_t c_attr;
        pthread_attr_t m_attr;
        void* status;
        register int n;

        if (!nb)
                return;

        n = nb >> 1;
        n++;

        pthread_attr_init(&m_attr);
        pthread_condattr_init(&c_attr);
        pthread_cond_init(&cond, &c_attr);
        pthread_condattr_destroy(&c_attr);
        pthread_mutex_init(&mutex, NULL);
        sem_init(&sem, 0, 0);
        pthread_create(&pid, &m_attr, f, (void*) (intptr_t) n);

        sem_wait(&sem);
        pthread_mutex_lock(&mutex);
        while (--n) {
                pthread_cond_signal(&cond);
                pthread_cond_wait(&cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        pthread_join(pid, &status);
        sem_destroy(&sem);
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&cond);
}

int main(int argc, char *argv[])
{
  unsigned nb = 100000;
  const int essais = 100;
  int i = essais;
  nb = (argc == 2) ? atoi(argv[1]) : 100000;

  while(i--)
    {
      bench_cond(nb);
    }

  fprintf(stderr, "  min = %g; max = %g; avg = %g\n", time_min, time_max, time_total / (double)essais);

  return 0;
}
