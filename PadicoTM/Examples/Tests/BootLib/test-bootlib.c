/* test PadicoTM Bootlib feature
 * author: Alexandre Denis
 */

#include <stdio.h>
#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PadicoBootLib.h>

int main(int argc, char**argv)
{
  fprintf(stderr, "# init Puk...\n");
  padico_puk_init();
  fprintf(stderr, "# init PadicoTM...\n");
  puk_mod_t boot_mod = padico_lib_init();
  fprintf(stderr, "# finalize PadicoTM...\n");
  padico_lib_finalize(boot_mod);
  fprintf(stderr, "# finalize Puk...\n");
  padico_puk_shutdown();
  fprintf(stderr, "# done.\n");
  return 0;
}
