#include <sys/types.h>
#include <sys/socket.h>



inline void print_error(char *func)
{
  perror(func);
  exit(1);
}

inline int usage(int argc, char *argv[])
{
  if(argc!=4)
    {
      printf("Usage: %s <client/server> <host> <port>\n", argv[0]);
      exit(0);
    }
}

int create_socket()
{
  int reuse = 1;
  int fd = socket(AF_INET, SOCK_STREAM, 0); 
  setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (int *)&reuse, sizeof(reuse));
  if (fd < 0)
    print_error("socket()");
  return fd;
}

void bind_and_listen(int sock_fd, int port)
{
  struct sockaddr_in serv_addr;
  bzero(&serv_addr,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  if (bind(sock_fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    print_error("bind()");
  struct sockaddr_in cli_addr;
  if(listen(sock_fd, 5) < 0)
    print_error("accept()");
}

void connect2(int sock_fd, int port, char *name)
{
  struct hostent *info_serv;
  info_serv=gethostbyname(name);
  struct sockaddr_in serv_addr;
  bzero(&serv_addr,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  memcpy(&serv_addr.sin_addr, info_serv->h_addr_list[0], info_serv->h_length);
  
  if (connect(sock_fd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    print_error("connect()");
}

void close2(int sock_fd)
{
  if ( close(sock_fd) < 0 )
    print_error("close()");
}
