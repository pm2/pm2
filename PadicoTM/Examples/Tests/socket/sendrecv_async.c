#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "helper.h"

#define MSGLEN 512

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char msg[MSGLEN];
  memset(msg, '\0', MSGLEN);


  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");
      strcpy(msg, "Hello client\0");
      if(send(clifd, msg, strlen(msg), MSG_DONTWAIT) < 0)
        print_error("send()");
      printf("Server: I send \"%s\"\n", msg);
      close2(clifd);

    }
  else if(strncmp("client",argv[1],6)==0)
    {
      int rc;
      connect2(sock_fd, port, argv[2]);

      fd_set rset;
      FD_ZERO(&rset);
      FD_SET(sock_fd, &rset);
      if((rc = select(sock_fd+1, &rset, NULL, NULL, NULL)) < 0)
        print_error("select()");

      if(FD_ISSET(sock_fd, &rset))
        if(recv(sock_fd, msg, MSGLEN, MSG_DONTWAIT) < 0)
          print_error("recv()");
      printf("Client: I receive \"%s\"\n", msg);

    }
  close2(sock_fd);

  return 0;
}
