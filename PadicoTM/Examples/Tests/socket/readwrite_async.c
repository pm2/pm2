#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "helper.h"

#define MSGLEN 20

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char msg[MSGLEN];
  memset(msg, '\0', MSGLEN);

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");
      strcpy(msg, "Hello client\0");

      int flags = fcntl(clifd, F_GETFL, 0);
      if(flags < 0)
        print_error("fcntl() get options:");
      if(fcntl(sock_fd, F_SETFL, flags|O_NONBLOCK))
        print_error("fcntl() set options:");

      if(write(clifd, msg, strlen(msg)) < 0)
        print_error("write()");
      printf("Server: I send \"%s\"\n", msg);
      read(clifd, msg, 1);
      close2(clifd);
    }
  else if(strncmp("client",argv[1],6)==0)
    {
      connect2(sock_fd, port, argv[2]);
      int rc = 0;
      int flags = fcntl(sock_fd, F_GETFL, 0);
      if(flags < 0)
        print_error("fcntl() get options:");
      if(fcntl(sock_fd, F_SETFL, flags|O_NONBLOCK))
        print_error("fcntl() set options:");

      if(read(sock_fd, msg, MSGLEN) < 0)
        {
          if (errno!=EAGAIN && errno!=EWOULDBLOCK)
            print_error("read()");
          else{
            fd_set rset;
            FD_ZERO(&rset);
            FD_SET(sock_fd, &rset);
            if((rc = select(sock_fd+1, &rset, NULL, NULL, NULL)) < 0)
              print_error("select()");
            if(FD_ISSET(sock_fd, &rset))
              if(read(sock_fd, msg, MSGLEN) < 0)
                print_error("read()");
          }
        }
      printf("Client: I receive \"%s\"\n", msg);
      if(read(sock_fd, msg, 1) < 0)
        {
          if(errno==EAGAIN || errno==EWOULDBLOCK)
            printf("read2 EAGAIN Ok\n");
          else
            print_error("read()");
        }
      if (write(sock_fd, msg, 1)!=1)
        print_error("write()");
    }

  close2(sock_fd);
  return 0;
}
