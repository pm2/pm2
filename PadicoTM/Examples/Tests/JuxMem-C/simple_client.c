/**
 * A simple JuxMem client
 * $Id: simple_client.c,v 1.19 2005/06/24 11:33:22 mjan Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <juxmem.h>

#define DEBUG 1

int main(int argc, char **argv)
{
    JuxMem_peer * juxmem_peer = NULL;
    char * data_id = NULL;
    char * data_input = NULL;
    char * data_output = NULL;

    if (argc != 4) {
        printf("Syntax: simple_client memory_size number_of_providers number_of_clusters\n");
        exit(0);
    }

    juxmem_peer = juxmem_initialize(NULL, (int) DEBUG, "jxta.log");
    assert(juxmem_peer != NULL);

    data_output = juxmem_malloc(juxmem_peer, atoi(argv[1]), &data_id, atoi(argv[2]), atoi(argv[3]), EC_PROTOCOL, BASIC_SOG);
    if (data_output == NULL) /** No more memory */
      return 1;
    fprintf(stdout, "Data id of data_output is %s\n", data_id);

    while (juxmem_acquire(juxmem_peer, data_output) != (int) TRUE) { fprintf(stdout, "Failed to acquire write lock, retrying\n"); };
    data_output = (char*) memset((void *) data_output, 0x61, atoi(argv[1]) - 1);
    data_output[atoi(argv[1]) - 1] = '\0';
    fprintf(stdout, "Data id %s = %s\n", data_id, data_output);
    while (juxmem_release(juxmem_peer, data_output) != (int) TRUE) { fprintf(stdout, "Failed to release write lock, retrying\n"); };
    juxmem_unmap(juxmem_peer, data_output);

    data_input = juxmem_mmap(juxmem_peer, NULL, 0, data_id, 0);
    if (data_input == NULL) /** No more memory */
      return 1;
    fprintf(stdout, "Data id of data_input is %s\n", data_id);

    while (juxmem_acquire_read(juxmem_peer, data_input) != (int) TRUE) { fprintf(stdout, "Failed to acquire read lock, retrying\n"); };
    fprintf(stdout, "Data id %s = %s\n", data_id, data_input);
    while (juxmem_release(juxmem_peer, data_input) != (int) TRUE) { fprintf(stdout, "Failed to release read lock, retrying\n"); };
    juxmem_unmap(juxmem_peer, data_input);

    juxmem_free(juxmem_peer, data_id);
    free(data_id);

    assert(juxmem_terminate(juxmem_peer) == (int) TRUE);

    return 0;
}
