#!/bin/bash

source helper.sh

echo "TEST: BOOTSTRAP TEST 2"
padico-launch --timeout 60 -DPADICO_BOOTSTRAP_LISTEN=${PADICO_PORT} --host ${PADICO_HOST1} sleep 20 &> rdvnode &
pid1=$!
padico-launch --timeout 60 -DPADICO_BOOTSTRAP_PORT=${PADICO_PORT} -DPADICO_BOOTSTRAP_HOST=${PADICO_HOST1} --host ${PADICO_HOST2}  sleep 15 &> node1 &
pid2=$!
padico-launch --timeout 60 -DPADICO_BOOTSTRAP_PORT=${PADICO_PORT} -DPADICO_BOOTSTRAP_HOST=${PADICO_HOST1} --host ${PADICO_HOST1}  sleep 1 &> node2
testrc $?
padico-launch --timeout 60 -DPADICO_BOOTSTRAP_PORT=${PADICO_PORT} -DPADICO_BOOTSTRAP_HOST=${PADICO_HOST1} --host ${PADICO_HOSTS}  -n ${PADICO_NODE} sleep 1 &> node3
testrc $?
wait ${pid2}
testrc $?
wait ${pid1}
testrc $?
success
rm rdvnode node1 node2 node3
clean

