#!/bin/bash

source helper.sh

echo "TEST: BOOTSTRAP TEST 1"
padico-launch --timeout 60 -v -l=. --host ${PADICO_HOSTS} -n ${PADICO_NODE} sleep 1  &> node1
rc=$?
if [ "x$rc" != "x0" ]; then
    failed
else
    if [ "x$(ls *.log | wc -l)" != "x${PADICO_NODE}" ]; then
	failed
    else
	if [ "x$(grep "new node" *.log | wc -l)" != "x$(( ${PADICO_NODE} * ${PADICO_NODE} ))" ]; then
	    failed
	else
	    success
	fi
    fi
fi

rm node1 *.log
clean
