#! /bin/sh

PARAMS="-Droundtrip=1000000 -Dstartlen=0 -Dmaxlen=100"

#CPUBIND="--padico-cpubind:0=0 --padico-cpubind:1=2 --padico-cpubind:2=4 --padico-cpubind:3=6 --padico-cpubind:4=8 --padico-cpubind:5=10 --padico-cpubind:6=12 --padico-cpubind:7=14 --padico-cpubind:8=1 --padico-cpubind:9=3 --padico-cpubind:10=5 --padico-cpubind:11=7 --padico-cpubind:12=9 --padico-cpubind:13=11 --padico-cpubind:14=13 --padico-cpubind:15=15"

#CPUBIND="--padico-cpubind:0=0 --padico-cpubind:1=2  --padico-cpubind:2=1 --padico-cpubind:3=3 --padico-cpubind:4=4 --padico-cpubind:5=6  --padico-cpubind:6=5 --padico-cpubind:7=7 --padico-cpubind:8=8 --padico-cpubind:9=10 --padico-cpubind:10=9 --padico-cpubind:11=11 --padico-cpubind:12=12 --padico-cpubind:13=14 --padico-cpubind:14=13 --padico-cpubind:15=15"

# ## jolly
CPUBIND="--padico-hwloc-cpubind:0=core:0 --padico-hwloc-cpubind:1=core:1 --padico-hwloc-cpubind:2=core:2 --padico-hwloc-cpubind:3=core:3 --padico-hwloc-cpubind:4=core:4 --padico-hwloc-cpubind:5=core:5 --padico-hwloc-cpubind:6=core:6 --padico-hwloc-cpubind:7=core:7 --padico-hwloc-cpubind:8=core:8 --padico-hwloc-cpubind:9=core:9 --padico-hwloc-cpubind:10=core:10 --padico-hwloc-cpubind:11=core:11 --padico-hwloc-cpubind:12=core:12 --padico-hwloc-cpubind:13=core:13 --padico-hwloc-cpubind:14=core:14 --padico-hwloc-cpubind:15=core:15 --padico-hwloc-cpubind:16=core:16"

padico-launch -n 16 -c -p -v ${PARAMS} -DPADICO_AUTODETECT=no ${CPUBIND} bench-lfqueue
case 
