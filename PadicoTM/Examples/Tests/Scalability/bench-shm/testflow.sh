#!/bin/bash

rm topo$1.txt
lstopo --merge --output-format console --no-io -p > topo$1.txt
sed -i '1d' topo$1.txt

k=$(./testcpubinding topo$1.txt) 

for ((i=2;i<=16;i=i+2))
do
    echo "1"
    rm tmp.dat
    rm dataflow$1$i.dat
    padico-launch -q -n $i testdrivershmflow -S 1024 > tmp.dat
   
    ./form_flow tmp.dat $i > tmp2.dat
    sort -n tmp2.dat > dataflow$1$i.dat
    
    pltcmd="plot 'dataflow$1$i.dat' using 1:6:5:7 with yerrorlines title 'Flow without positioning' lc 8 lw 2 pt 2"
    echo "2"
    for((j=-1;j < k;j++))
    do
	echo "3"
	if [ $j -eq -1 ];then
	    rm dataflow$1${i}coretocore.dat
	else
	    name=$(./cachename topo$1.txt $k $j)
	    rm dataflow$1$i$name.dat 
	fi
	
	rm tmp.dat
	binding=$(./testcpubinding2 topo$1.txt $k $j $i)
	echo $binding
	padico-launch -q -n $i $binding testdrivershmflow -S 1024 > tmp.dat 
	
	if [ $j -eq -1 ];then
	    echo "4"
	    ./form_flow tmp.dat $i > tmp2.dat
	    sort -n tmp2.dat > dataflow$1${i}coretocore.dat
	    echo "5"
	else
	    echo "6"
	    ./form_flow tmp.dat $i > tmp2.dat
	    sort -n tmp2.dat > dataflow$1$i$name.dat
	    echo "7"
	fi
 
	if [ $j -eq -1 ];then
	    
	    pltcmd=$pltcmd",'dataflow$1${i}coretocore.dat' using 1:6:5:7 with yerrorlines title 'Flow with core-to-core positioning' lc 9 lw 2 pt 2"
	else
	    
	    pltcmd=$pltcmd",'dataflow$1$i$name.dat' using 1:6:5:7 with yerrorlines title 'Flow with positioning mode $name' lc $j lw 2 pt 2" 
	fi
    done
    gnuplot<<EOF
set terminal png size 1500,1000
set output 'flow$1$i.png'
set xlabel 'Packet size (byte)'
set ylabel 'Flow (MB/s)'
set logscale x 2
set xrange [1024:]
set key box
set grid
set ytics 100
set title 'Flow (MB/s) on $1 depending on packet size for $i process'
$pltcmd
EOF
    
done
rm tmp.dat
rm tmp2.dat
rm topo$1.txt







