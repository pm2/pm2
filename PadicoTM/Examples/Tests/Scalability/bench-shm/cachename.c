#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFMAX 5000


int main(int argc, char *argv[])
{
  if (argc!=4){
    printf("wrong\n");
    return 0;
  }
  else
    {
    FILE* fichier = NULL;

    fichier = fopen(argv[1], "r");

    char buff[BUFFMAX];
    int i=0;
    int nblevel=atoi(argv[2]);
    int mode = atoi(argv[3]);
    char namelevel[nblevel][15];

    if(fichier != NULL)
      {
        while(fgets(buff,BUFFMAX,fichier)!=NULL&&strstr(buff,"PU")==NULL)
          {
            sscanf(buff," %s ",namelevel[i]);
            i++;
          }
        if(mode!=-1){
          printf("%s",namelevel[mode]);
        }
      }
    fclose(fichier);
    }
  return 0;
}
