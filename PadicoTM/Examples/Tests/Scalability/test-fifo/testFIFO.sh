#!/bin/bash

if [ $# != 2 ]
then
    
    echo $#
    
    echo './testFIFO.sh <mode> <machine>'
    echo 1

else
    
    echo $#

    ./testFIFOlfqueue $1 > lfqueue$1$2.dat
    ./testFIFOlqueuemutex $1 > lqueuemutex$1$2.dat
    ./testFIFOlqueuespinlock $1 > lqueuespinlock$1$2.dat

    if [ $1 == 0 ]
    then
	
	echo 'useless to create an image with just one point'

    elif [ $1 == 1 ]
    then

	echo 'creation of images of minimum rates, maximum rates, median rates of the consumer threads and the rates of the producer thread in function of the number of consumer threads'
	
        gnuplot<<EOF
set terminal png size 3000,1000
set output 'Rate1N$2.png'
set multiplot
set xlabel 'Nb. consumer threads'
set ylabel 'Rate (Nb. dequeue/s)'
set logscale y 2
set key box
set grid
set size 0.5 , 1
set origin 0.0 , 0.0
set title 'Rates for consumer threads in mode 1 producer-N consumers on $2'
plot 'lfqueue$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO lock-free median' lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO mutex median' lc 2 lw 2 pt 2,'lqueuespinlock$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO spinlock median' lc 3 lw 2 pt 2
set size 0.5 , 1
set origin 0.5 , 0.0
set xlabel 'Nb. consumer threads'
set ylabel 'Rate (Nb. enqueue/s)'
set title 'Rate for producer thread in mode 1 producer-N consumers on $2'
plot 'lfqueue$1$2.dat' using 2:4 title 'FIFO lock-free' with linespoint lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 2:4 title 'FIFO mutex' with linespoint lc 2 lw 2 pt 2, 'lqueuespinlock$1$2.dat' using 2:4 title 'FIFO spinlock' with linespoint lc 3 lw 2 pt 2
EOF

    elif [ $1 == 2 ]
    then

	echo 'creation of images of minimum rates, maximum rates, median rates of the producers threads and the rates of the consumer thread in function of the number of producer threads'

        gnuplot<<EOF
set terminal png size 3000,1000
set output 'RateN1$2.png'
set multiplot
set xlabel 'Nb. producer threads'
set ylabel 'Rate (Nb. enqueue/s)'
set logscale y 2
set key box
set grid
set size 0.5 , 1
set origin 0.0 , 0.0
set title 'Rates for producer threads in mode N producer-1 consumers on $2'
plot 'lfqueue$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO lock-free median' lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO mutex median' lc 2 lw 2 pt 2,'lqueuespinlock$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO spinlock median' lc 3 lw 2 pt 2
set size 0.5 , 1
set origin 0.5 , 0.0
set xlabel 'Nb. producer threads'
set ylabel 'Rate (Nb. dequeue/s)'
set title 'Rate for producer thread in mode N producer-1 consumers on $2'
plot 'lfqueue$1$2.dat' using 1:7 title 'FIFO lock-free' with linespoint lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 1:7 title 'FIFO mutex' with linespoint lc 2 lw 2 pt 2, 'lqueuespinlock$1$2.dat' using 1:7 title 'FIFO spinlock' with linespoint lc 3 lw 2 pt 2
EOF

    elif [ $1 == 3 ]
    then

	echo 'creation of images of minimum rates, maximum rates, median rates of the producer and consumer threads in function of the number of threads(the number of producer threads and consumer threads are equal in this case'

         gnuplot<<EOF
set terminal png size 3000,1000
set output 'RateNN$2.png'
set multiplot
set xlabel 'Nb. threads'
set ylabel 'Rate (Nb. enqueue/s)'
set logscale y 2
set key box
set grid
set size 0.5 , 1
set origin 0.0 , 0.0
set title 'Rates for producer threads in mode N producer-N consumers on $2'
plot 'lfqueue$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO lock-free median' lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO mutex median' lc 2 lw 2 pt 2,'lqueuespinlock$1$2.dat' using 1:4:3:5 with yerrorlines title 'FIFO spinlock median' lc 3 lw 2 pt 2
set size 0.5 , 1
set origin 0.5 , 0.0
set xlabel 'Nb. threads'
set ylabel 'Rate (Nb. dequeue/s)'
set title 'Rates for consumer threads in mode N producer-N consumers on $2'
plot 'lfqueue$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO lock-free median' lc 1 lw 2 pt 2, 'lqueuemutex$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO mutex median' lc 2 lw 2 pt 2,'lqueuespinlock$1$2.dat' using 2:7:6:8 with yerrorlines title 'FIFO spinlock median' lc 3 lw 2 pt 2
EOF



    else
	echo 'Wrong mode'
    fi 
    
fi
    

