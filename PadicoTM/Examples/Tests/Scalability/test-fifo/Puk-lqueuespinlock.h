/** @file lock FIFO (with spinlock) as circular array
 */

#include <pthread.h>

#ifndef PUK_LQUEUE_H
#define PUK_LQUEUE_H

/** builds a lock FIFO type and functions.
 *  ENAME is base name for symbols
 *  TYPE is the type of elements in queue- must be atomic (1, 2, 4, or 8 bytes long)
 *  LQUEUE_NULL is a nil value to fill and detect empty cells
 *  LQUEUE_SIZE is the array size
 *  Only 'init', 'enqueue', 'dequeue' operations are available.
 *  Iterators are not possible.
 *  push_front/push_back/po_front/pop_back is probably possible at a high
 *  cost in code size and performance. For simplicity and performance, only
 *  'enqueue' = push_back; 'dequeue' = pop_front are available (2 atomic ops each).
 */
#define PUK_LQUEUE_TYPE(ENAME, TYPE, LQUEUE_SIZE)                       \
  typedef TYPE ENAME ## _lqueue_elem_t;                                         \
                                                                        \
  struct ENAME ## _lqueue_s                                             \
  {                                                                     \
    volatile ENAME ## _lqueue_elem_t _queue[(LQUEUE_SIZE)];             \
    volatile unsigned _head;                                            \
    volatile unsigned _tail;                                            \
    pthread_spinlock_t my_lock;                                                         \
  };                                                                    \
                                                                        \
  static inline void ENAME ## _lqueue_init(struct ENAME ## _lqueue_s*queue) \
  {                                                                     \
    pthread_spin_init(&queue->my_lock,0);                               \
    queue->_head = 0;                                                   \
    queue->_tail = 0;                                                   \
  }                                                                     \
                                                                        \
  static inline int ENAME ## _lqueue_enqueue_ext(struct ENAME ## _lqueue_s* queue, ENAME ## _lqueue_elem_t e) \
  {                                                                     \
  /* allocate a slot for value */                                       \
  pthread_spin_lock(&queue->my_lock);                                   \
  const unsigned head = queue->_head;                                   \
  const unsigned tail = queue->_tail;                                   \
  if(head >= tail + (unsigned)(LQUEUE_SIZE) - 1)                        \
    {                                                                   \
      /* queue full */                                                  \
      pthread_spin_unlock(&queue->my_lock);                             \
      return -1;                                                        \
    }                                                                   \
                                                                        \
  /* slot is still NULL for concurrent readers, already reserved if concurrent writers */ \
  const unsigned cell = head % (unsigned)(LQUEUE_SIZE);                         \
  /* store value in reserved slot */                                    \
  queue->_queue[cell] = e;                                              \
  queue->_head = head + 1;                                              \
  pthread_spin_unlock(&queue->my_lock);                                         \
  return 0;                                                             \
  }                                                                     \
                                                                        \
  static inline int ENAME ## _lqueue_enqueue(struct ENAME ## _lqueue_s*queue, ENAME ## _lqueue_elem_t e) \
  { return ENAME ## _lqueue_enqueue_ext(queue, e); }                    \
                                                                        \
  static inline ENAME ## _lqueue_elem_t ENAME ## _lqueue_dequeue_ext(struct ENAME ## _lqueue_s* queue) \
  {                                                                     \
    ENAME ## _lqueue_elem_t e;                                          \
    unsigned tail;                                                      \
    /* try to dequeue */                                                \
    pthread_spin_lock(&queue->my_lock);                                         \
    tail = queue->_tail;                                                \
    if(tail >= queue->_head)                                            \
      {                                                                         \
        /* queue was empty, abort dequeue */                            \
        pthread_spin_unlock(&queue->my_lock);                                   \
        return -1;                                                      \
      }                                                                                 \
    const unsigned cell = tail % (LQUEUE_SIZE);                                 \
    e = queue->_queue[cell];                                            \
    queue->_tail = tail + 1;                                            \
    pthread_spin_unlock(&queue->my_lock);                                       \
    return e;                                                           \
  }                                                                     \
                                                                        \
  static inline ENAME ## _lqueue_elem_t ENAME ## _lqueue_dequeue(struct ENAME ## _lqueue_s*queue) \
  { return ENAME ## _lqueue_dequeue_ext(queue); }                       \
                                                                                \
  /** hint whether the queue is empty.                                  \
   *  Do not rely on it: state may have changed before dequeue          \
   *  Usefull to reduce active polling, though */                               \
  static inline int ENAME ## _lqueue_empty(struct ENAME ## _lqueue_s*queue) \
  {                                                                     \
    return(queue->_tail < queue->_head);                                \
  }                                                                     \

#endif /* PUK_LQUEUE_H */
