/** @file
 * Test VIO sockets between a node inside a PadicoTM cluster and
 * another node outside the cluster. Server side.
 */

#ifdef PADICO
#include <Padico/PadicoTM.h>
#endif

#include "io-test.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>


struct servant_s
{
  int fd;
  unsigned char*buffer;
};
struct server_s
{
  int fd;
  struct servant_s servants[MAX_CLIENTS];
};

static struct server_s* make_server()
{
  int rc;
  struct sockaddr_in addr;
  struct server_s* server = malloc(sizeof(struct server_s));

  /* socket */
  server->fd = make_socket();
  printf("[vio-server] socket() fd_server=%d\n", server->fd);
  if(server->fd == -1)
    test_failed();

  /* bind */
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(SERVER_PORT_NUMBER);
  rc = bind(server->fd, (struct sockaddr*)&addr, sizeof(addr));
  printf("[vio-server] bind() fd=%d rc=%d\n", server->fd, rc);
  if(rc)
    {
      int err = errno;
      printf("bind() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
      test_failed();
    }

  /* listen */
  rc = listen(server->fd, 5);
  printf("[vio-server] listen() fd=%d rc=%d\n", server->fd, rc);
  if(rc)
    test_failed();
  return server;
}

static void make_servant(struct server_s*server, struct servant_s*servant)
{
  struct sockaddr_in addr;
  socklen_t addr_len = sizeof(addr);
  /* accept */
  servant->fd = accept(server->fd, (struct sockaddr*)&addr, &addr_len);
  printf("[vio-server] accept() fd_server=%d fd_servant=%d\n",
         server->fd, servant->fd);
  if(servant->fd == -1)
    test_failed();
  {
    /* set sock opt NODELAY */
    int opt = 1;
    int rc;
    rc = setsockopt(servant->fd, SOL_TCP, TCP_NODELAY, &opt, sizeof(opt));
    rc = setsockopt(servant->fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  }
}


static void servant_loop(struct servant_s*servant)
{
  int i, s;
  int fd = servant->fd;
  printf("[%p] fd=%d starting\n", servant, fd);
  servant->buffer = malloc(BUFFER_SIZE);
  for(s=4; s<BUFFER_SIZE; s*=2)
    {
      unsigned char*buffer = servant->buffer;
      struct timeval t_begin, t_end;
      double t_usec;
      //      printf("[vio-server] SERVANT 0x%p fd=%d receiving size=%d\n", servant, fd, s);
      gettimeofday(&t_begin, NULL);
      for(i=0; i<config_num_roundtrip; i++)
        {
          /* re-init buffer */
          if(config_reinit_buffer)
            {
              memset(buffer, 0, s);
            }
          recv_msg(fd, buffer, s);
          if(config_check_buffer)
            {
              check_buffer(buffer, s, i);
            }
          send_msg(fd, buffer, s);
        }
      gettimeofday(&t_end, NULL);
      t_usec = 1000000.0 * (double)(t_end.tv_sec - t_begin.tv_sec)
        + (double)(t_end.tv_usec - t_begin.tv_usec);
      printf("[%p] size=%7d time=%9.2f usec bw=%5.2f MB /s\n", servant,
             s, t_usec/(config_num_roundtrip*2),
             (((double)s) * 2.0 * config_num_roundtrip) / t_usec);
    }
  free(servant->buffer);
  printf("[%p] fd=%d closing...\n", servant, servant->fd);
  close(servant->fd);
  printf("[%p] fd=%d completed\n", servant, servant->fd);
  servant->fd = -1;
}
static void* server_handler(struct server_s*server)
{
  int n;
  for(n=0; n < MAX_CLIENTS; n++)
    {
      make_servant(server, &server->servants[n]);
#ifdef PADICO
      padico_tm_bgthread_start(NULL, &servant_loop, &server->servants[n], "vio-server:servant_loop");
#else
      servant_loop(&server->servants[n]);
#endif
    }
  return NULL;
}

int main(int argc, char**argv)
{
  struct server_s*server1;
  printf("[vio-server] init\n");
  server1 = make_server();
  server_handler(server1);
  return 0;
}
