
#include "io-test.h"

int make_socket(void)
{
  int rc;
  int val;
  int vallen;
  int fd = socket(AF_INET, SOCK_STREAM, 0);
  if(fd == -1)
    {
      int err = errno;
      printf("[vio-test] socket error %d (%s)\n", err, strerror(err));
      test_failed();
    }
  printf("[vio-test] socket fd=%d\n", fd);
  val = DEFAULT_SNDBUF;
  rc = setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &val, sizeof(val));
  if(rc < 0)
    {
      int err = errno;
      printf("[vio-test] setsockopt() SO_SNDBUF rc=%d error %d (%s)\n", rc, err, strerror(err));
      test_failed();
    }
  val = DEFAULT_RCVBUF;
  rc = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val));
  if(rc < 0)
    {
      int err = errno;
      printf("[vio-test] setsockopt() SO_RCVBUF rc=%d error %d (%s)\n", rc, err, strerror(err));
      test_failed();
    }
  return fd;
}

void send_msg1(int fd, unsigned char*buffer, int size, int size1)
{
  int done = 0;
  int count = 0;
  if(size1 == 0) size1 = ALIGN_VAL;
  while(done < size)
    {
      int rc;
      count ++;
      rc = write(fd, buffer+done, size1);
      if(rc >= 0)
        {
          done += rc;
          size1 = size - done;
        }
      else
        {
          int err = errno;
          printf("write() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
          test_failed();
        }
    }
  /*
  printf("send_msg() count=%d (%d bytes/packet)\n",
         count, size/count);
  */
}

void send_msg(int fd, unsigned char*buffer, int size)
{
  send_msg1(fd, buffer, size, size);
}

void fill_buffer(unsigned char*buffer, int size, int val)
{
  int i;
  for(i=0; i<size; i++)
    {
      buffer[i] = val+i;
    }
}

void check_buffer(unsigned char*buffer, int s, int i)
{
  int j;
  int val = s+i;
  for(j=0; j<s; j++)
    {
      unsigned char expected = (unsigned char)(val+j);
      unsigned char received = (unsigned char)(buffer[j]);
      if(received != expected)
        {
          printf("[vio-server] check buffer failed at byte %d (size=%d)\n", j, s);
          printf("[vio-server] expected=0x%X, received=0x%X roundtrip=%d\n",
                         (int)(expected), (int)(received), i);
          test_failed();
        }
    }
}

void recv_msg(int fd, unsigned char*buffer, int size)
{
  int done = 0;
  int rc = 0;
  int count = 0;
  while(done < size && rc >= 0)
    {
      count++;
      rc = read(fd, buffer+done, size-done);
      done += rc;
      if(rc <= 0)
        {
          int err = errno;
          printf("read() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
          test_failed();
        }
    }
  /*  printf("recv_msg() count=%d (%d bytes/packet)\n",
      count, size/count);
  */
}

void test_failed(void)
{
  printf("[vio-test] failed.\n");
  abort();
}
