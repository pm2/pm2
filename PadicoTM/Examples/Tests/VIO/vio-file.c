/** @file
 * Test VIO files.
 */

#include <Padico/Puk.h>
#include <Padico/VIO.h>
#include <Padico/PM2.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>

int main_file(int argc, char**argv);

PADICO_MODULE_DECLARE(vio_file, NULL, main_file, NULL);

static int vio_file_fd;
static char*vio_file_name = NULL;

int main_file(int argc, char**argv)
{
  int buffer_size;
  const int max_buffer_size = 1024*1024*128;
  const int min_buffer_size = 1024*1024;
  const int small_chunk     = 128;
  const int max_iter        = 16;

  padico_tm_thread_givename("vio-file: main()");

  /* init */
  vio_file_name =  tempnam("/tmp", "VIO");
  padico_print("VIO-file: creating filename = %s\n", vio_file_name);
  vio_file_fd = open(vio_file_name,  O_RDWR | O_CREAT | O_TRUNC, 0600);
  if(vio_file_fd == -1)
    {
      padico_warning("error while creating file\n");
      return -1;
    }
  /* test */
  for(buffer_size = min_buffer_size; buffer_size <= max_buffer_size; buffer_size *=2)
    {
      const int big_chunk = buffer_size;
      int j;
      int chunk_size;
      char*buffer = padico_malloc(buffer_size);
      padico_print("VIO-file: starting test fd=%d buffer_size=%dMB\n",
                   vio_file_fd, buffer_size/(1024*1024));
      for(j = 0, chunk_size = big_chunk;
          j < max_iter && chunk_size > small_chunk;
          j++, chunk_size /= 2)
        {
          padico_timing_t t1, t2;
          double t, t_per_chunk;
          int rc;
          int num_chunks = buffer_size/chunk_size;
          int i;
          /* write test */
          rc = lseek(vio_file_fd, 0, SEEK_SET);
          if(rc != 0)
            padico_warning("after lseek: offset=%d\n", rc);
          padico_timing_get_tick(&t1);
          for(i=0; i<num_chunks; i++)
            {
              rc = write(vio_file_fd, buffer, chunk_size);
              if(rc != chunk_size)
                padico_warning("write rc=%d\n", rc);
            }
          padico_timing_get_tick(&t2);
          t = padico_timing_diff_usec(&t1, &t2);
          t_per_chunk = (t/num_chunks);
          padico_print("[write] %5dx%8.2f KB t=%7.1f ms (%5dx%6.0f us) %5.1f MB/s\n",
                       num_chunks, (float)(chunk_size/1024.0), (float)(t/1000.0),
                       num_chunks, t_per_chunk,
                       (double)buffer_size/t);
          /* read test */
          rc = lseek(vio_file_fd, 0, SEEK_SET);
          if(rc != 0)
            padico_warning("after lseek: offset=%d\n", rc);
          fsync(vio_file_fd);
          padico_timing_get_tick(&t1);
          for(i=0; i<num_chunks; i++)
            {
              rc = read(vio_file_fd, buffer, chunk_size);
              if(rc != chunk_size)
                padico_warning("read rc=%d\n", rc);
            }
          padico_timing_get_tick(&t2);
          t = padico_timing_diff_usec(&t1, &t2);
          t_per_chunk = (t/num_chunks);
          padico_print("[read]  %5dx%8.2f KB t=%7.1f ms (%5dx%6.0f us) %5.1f MB/s\n",
                       num_chunks, (float)(chunk_size/1024.0), (float)(t/1000.0),
                       num_chunks, t_per_chunk,
                       (double)buffer_size/t);

        }
      padico_free(buffer);
      padico_print("VIO-file: ok\n");
    }
  /* close */
  padico_print("VIO-file: close file\n");
  close(vio_file_fd);
  padico_print("VIO-file: unlink filename = %s\n", vio_file_name);
  unlink(vio_file_name);
  return 0;
}
