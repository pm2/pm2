#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
  int fd;

  fd= open ("/tmp/toto", O_RDWR|O_CREAT);

  if(chmod("/tmp/toto", S_IRWXU)<0){
    fprintf(stderr,"chmod failure\n");
  }

  if(lseek(fd, 100, SEEK_SET)<0){
    fprintf(stderr,"errno:%d\n",errno);
    perror("lseek failure\n");
  } else
    fprintf(stderr,"lseek ok\n");

  close(fd);
}

