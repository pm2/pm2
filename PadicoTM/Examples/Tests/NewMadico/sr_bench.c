/*
 * NewMadeleine
 * Copyright (C) 2006 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "helper.h"

#define MIN_DEFAULT     0
#define MAX_DEFAULT     (8 * 1024 * 1024)
#define MULT_DEFAULT    2
#define INCR_DEFAULT    0
#define WARMUPS_DEFAULT 100
#define LOOPS_DEFAULT   2000

static __inline__
uint32_t _next(uint32_t len, uint32_t multiplier, uint32_t increment)
{
  if (!len)
    return 1+increment;
  else
    return len*multiplier+increment;
}

int
main(int          argc,
     char       **argv) {
        char            *buf            = NULL;
        uint32_t         len;
        uint32_t         start_len      = MIN_DEFAULT;
        uint32_t         end_len        = MAX_DEFAULT;
        uint32_t         multiplier     = MULT_DEFAULT;
        uint32_t         increment      = INCR_DEFAULT;
        int              iterations     = LOOPS_DEFAULT;
        int              warmups        = WARMUPS_DEFAULT;

        sr_bench_init(&argc, argv);

        if(!is_server)
          waiting_connection();

        start_len = MIN_DEFAULT;
        end_len = MAX_DEFAULT;
        increment = INCR_DEFAULT;
        multiplier = MULT_DEFAULT;
        iterations = LOOPS_DEFAULT;
        warmups = WARMUPS_DEFAULT;

        buf = malloc(end_len);
        memset(buf, 0, end_len);

        if (is_server) {
          int k;
                /* server
                 */
                for(len = start_len; len <= end_len; len = _next(len, multiplier, increment)) {
                  for(k = 0; k < iterations + warmups; k++) {
                    nm_so_request request;

                    nm_so_sr_irecv(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_rwait(sr_if, request);

                    nm_so_sr_isend(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_swait(sr_if, request);
                  }
                }

        } else {
          tbx_tick_t t1, t2;
          double sum, lat, bw_million_byte, bw_mbyte;
          int k;
                /* client
                 */
                printf("# size |  latency     |   10^6 B/s   |   MB/s    |\n");

                waiting_connection();

                for(len = start_len; len <= end_len; len = _next(len, multiplier, increment)) {

                  for(k = 0; k < warmups; k++) {
                    nm_so_request request;

                    nm_so_sr_isend(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_swait(sr_if, request);

                    nm_so_sr_irecv(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_rwait(sr_if, request);
                  }

                  TBX_GET_TICK(t1);

                  for(k = 0; k < iterations; k++) {
                    nm_so_request request;

                    nm_so_sr_isend(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_swait(sr_if, request);

                    nm_so_sr_irecv(sr_if, gate_id, 0, buf, len, &request);
                    nm_so_sr_rwait(sr_if, request);
                  }

                  TBX_GET_TICK(t2);

                   sum = TBX_TIMING_DELAY(t1, t2);

                  lat         = sum / (2 * iterations);
                  bw_million_byte = len * (iterations / (sum / 2));
                  bw_mbyte        = bw_million_byte / 1.048576;

                  printf("%d\t%lf\t%8.3f\t%8.3f\n",
                         len, lat, bw_million_byte, bw_mbyte);
                }
        }

        nmad_exit();
        exit(0);
}
