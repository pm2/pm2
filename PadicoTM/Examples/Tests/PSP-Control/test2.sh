#!/bin/bash

source helper.sh

echo "TEST: PSP_Control/Control_SSH2"

cat > NetSelectorConf.xml <<EOF
<?xml version="1.0"?>
<NS:rule-set 
   xmlns:NS="http://runtime.bordeaux.inria.fr/PadicoTM/NetSelector" 
   xmlns:puk="http://runtime.bordeaux.inria.fr/PadicoTM/Puk"
   >

  <puk:composite id="control-over-ssh">
    <puk:component id="0" name="Control_SSH2">
      <puk:attr label="ssh_host">morris</puk:attr>
      <puk:attr label="ssh_user">${USER}</puk:attr>
    </puk:component>
    <puk:entry-point provider-id="0" iface="PadicoBootstrap"/>
    <puk:entry-point provider-id="0" iface="PadicoControl"/>
  </puk:composite>

  <puk:composite id="vlink-over-control">
    <puk:component id="0" name="PSP_Control"/>
    <puk:component id="1" name="VLink_Packet">
      <puk:uses iface="PadicoSimplePackets" provider-id="0"/>
    </puk:component>
    <puk:entry-point provider-id="1" iface="VLink"/>
    <puk:entry-point provider-id="0" iface="PadicoSimplePackets"/>
  </puk:composite>

  <NS:rule>
    <puk:composite ref="control-over-ssh"/>
    <NS:target kind="multipartite">
      <NS:host name="${PADICO_HOST1}"/>
      <NS:host name="${PADICO_HOST2}"/>
    </NS:target>
  </NS:rule>

  <NS:rule>
    <puk:composite ref="vlink-over-control"/>
    <NS:target kind="multipartite">
      <NS:host name="${PADICO_HOST1}"/>
      <NS:host name="${PADICO_HOST2}"/>
    </NS:target>
  </NS:rule>


</NS:rule-set>

EOF

padico-launch --timeout 30 -n 2 --host ${PADICO_HOSTS} -DNS_BASIC_CONF=${PWD}/NetSelectorConf.xml -Dmaxlen=1 bench-VIO
rc=$?
rm NetSelectorConf.xml
testrc ${rc}
success