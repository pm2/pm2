/**
 * simple test for UDP send/receive.
 */
import java.io.*;
import java.net.*;
//import SocketParam;
//import SocketDoWork;
//import SocketTime;

public class SocketMain {
    
  /* Ce flag permet juste de choisir diff�rents comportements du programme.
     Il est pass�, optionnellement, comme args[1]. On peut s'en servir par
     exemple pour interrompre le prog � un certain point.
  */
  static int flag = 0;
  static final int port = 41000;
  static int nb = 10;

  static int min = 1;
  static int max = 2000000;
  static SocketDoWork sdw;
  static int idx = 0;  
  static int sizes[];
  static double tmoys[];
  static double debits[];

  static void one_test(SocketParam p, SocketParam pp, int nb) throws IOException {
    long start, stop;
    long time;
    double tmoy = 0;
    double debit;

    System.out.println ("\n[=== " + nb + " �changes de " + p.n + " octets ===]");
    sizes[idx] = p.n;

    /* Execute une boucle de transfert de p.n = pp.n octets,
       dans les deux sens:
       - le maitre envoie des donn�es, l'esclave attend
       - l'esclave envoie des donn�es, le maitre attend
       La mesure de temps est faite nb fois
    */
      try {
          sdw.sendSize(p);
          sdw.recvSize(pp);
          sdw.sendData(p);
          sdw.recvData(pp);
          pp.check();
          pp.clear();
          if (p.n != pp.n)
              System.out.println("ERREUR: " + p.n + "!=" + pp.n);
          else
              System.out.println("Echange " + p.n + "==" + pp.n);
      } catch (IOException e) {
          System.err.println("Probl�me d'envoi des tailles");
      }

    for(int i=0; i <= nb; i++) {
      /* Echange des tailles de donn�es:
         - Le maitre envoie la taille de son buffer d'envoi
         - L'esclave recoit la taille, redimensionne �ventuellement
           son buffer, l'efface, puis renvoie la taille
         - Le maitre recoit la taille, redimensionne son buffer de r�cep,
           et l'efface.
      */

      SocketTime.timeInit();
      try {
          sdw.sendData(p);
          sdw.recvData(pp);
      } catch (IOException e) {
          System.err.println("Probl�me d'envoi des donn�es");
      }
      stop = SocketTime.getTimeMicros();
      pp.check();
      pp.clear();

      if (i > 0)
          {      
              time = stop / 2;
              tmoy += (double)time;
              debit = (double)p.n * 1000000.0 / (double)time;
              System.out.println(i + ": " + p.n + " octets transf�r�s en " + time +
                                 " us --> " + debit + " octets/seconde");
          }
      else System.out.println("Non compt�: " + i + ": " + p.n + " octets en " + stop/2);
    }

    tmoy /= (double)nb;
    tmoys[idx] = tmoy;
    debit = (double)p.n * 1000000.0 / (double)tmoy;
    debits[idx] = debit;
    System.out.println("Moyenne: " + p.n + " octets en " + tmoy + " us --> "
                       + debit + "octets/seconde\n");
  }

  static void do_master_work(Socket s) {
    sizes = new int[40];
    tmoys = new double[40];
    debits = new double[40];

    try { 
      sdw = new SocketDoWork(s);

      SocketParam p = new SocketParam(1,max);
      SocketParam pp = new SocketParam(1,max);
      System.out.println("Echange de donn�es non align�es");
      try {
          sdw.sendrecvUnaligned(p, pp, 0);
          sdw.sendrecvUnaligned(p, pp, 1);
          sdw.sendrecvUnaligned(p, pp, 2);
          sdw.sendrecvUnaligned(p, pp, 3);
      } catch (IOException e) {
          System.out.println("ERREUR !");
          System.exit(1);
      }

      /*      one_test(p,pp,10);
              one_test(p,pp,nb);*/
      for(int n=min; n<=max; n=n*2){ // , nb=3*nb/4) {
        p.n = n;
        one_test(p,pp,nb);
        idx++;
      }
    } catch (IOException e) {
      System.out.println("IOException: "+e);
    }

    System.out.println("\nTaille\t\tTemps moy (us)\tDebit");
    for (int n = 0; n < idx; n++)
      {
        System.out.println(sizes[n]+"\t\t"+tmoys[n]+"\t\t"+debits[n]);
      }
  }
  static void master() {
      System.out.println("Master running\n");
      ServerSocket ss;
    try {          
      ss = new ServerSocket(port);
    } catch (IOException e) {
      System.out.println(e);
      return;
    }
    
    // 2 accept phase
    Socket cs;
    try {          
      cs = ss.accept();
    } catch (IOException e) {
      System.out.println(e);
      return;
    }
    System.out.println("Connection done");
    
    // 3 do work
    try {
        cs.setTcpNoDelay(true);
    } catch (SocketException e) {}

    if ( flag != 1)
    do_master_work(cs);
    
    // 4. close all
    try {
      cs.close();
      ss.close();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }
    System.out.println("Master terminated !");
  }

  static void do_slave_work(Socket s) {

      SocketParam p = new SocketParam(0, max);
    try {
      sdw = new SocketDoWork(s);              
    } catch (IOException e) {
      System.err.println("Impossible de cr�er les streams: " + e);
    }
    try {
        sdw.recvsendUnaligned(p);
        sdw.recvsendUnaligned(p);
        sdw.recvsendUnaligned(p);
        sdw.recvsendUnaligned(p);
    } catch (IOException e) {
        System.out.println("Erreur recept donnees non alignees");
        System.exit(1);
    }

      for(;;) {
          try { 
              System.out.println("[=== Debut d'un echange ===]");
              sdw.recvSize(p);
              System.out.println("Size = " + p.n + " bytes");
              sdw.sendSize(p);
              sdw.recvData(p);
              sdw.sendData(p);
              p.clear();
              for (int j = 0; j <= nb; j++) {
                  sdw.recvData(p);
                  sdw.sendData(p);
              }
              System.out.println ("Echange de " + p.n + " octets termin�\n");
          } catch (IOException e) {
            //      e.printStackTrace();
                      System.out.println("Master finished transmitting: "+e);
              return ;
          }
      }
  }

   
  static void slave(String name) {
    System.out.println("Slave running with servername: "+name);
    Socket cs;

    try {
      Thread.sleep(2000);
    } catch (InterruptedException ee) {ee.printStackTrace();}

    try {
      cs = new Socket(name,port);
    } catch (IOException e) {
      System.out.println("ICI: "+e);
      return;
    }
    
    System.out.println("Slave: Connection ok !\n");

    // 3 do work
    
    try {
        cs.setTcpNoDelay(true);
    } catch (SocketException e) {}
    if (flag != 1)
    do_slave_work(cs);
    
    // 4. close all
    try {
      cs.close();
    } catch (IOException e) {
      System.out.println(e);
      return;
    }
    System.out.println("Slave terminated !");
  }
  
    static void usage() {
        System.out.println("Usage");
        System.out.println("  standalone: SocketMain [-m: for master][-s <server name>: for slave]");
        System.out.println("  in padico:  SocketMain [<server name>]");
        System.exit(-1);
    }

  public static void padico_module_run(String av[]) throws Exception {

      String n = java.net.InetAddress.getLocalHost().getHostName();

    System.out.println("\nIn padico_module_run of SocketMain of "+n);

    if (av.length == 0) usage();    
    if (av.length >= 2) {
        min = Integer.valueOf(av[1]).intValue();
        System.out.println("Valeur mini: " + min);
    }
    if (av.length >= 3) {
        max = Integer.valueOf(av[2]).intValue();
        System.out.println("Valeur maxi: " + max);
    }

    /*System.out.println("Time: "+ JniPadico.getTime());
      int rank = JniPadico.getRank();
      System.err.println("Rank: "+ rank);*/

    if (av[0].equals(n)) 
      master();
    else {
      System.err.println("av.length: "+ av.length);
      System.err.println("av[0]: "+ av[0]);
      slave(av[0]);
    }

    System.out.println("\n" + n + " Out of padico_module_run of SocketMain!\n");
  }

  public static void main(String av[]) throws Exception
  {           
    System.out.println("SocketMain is running !!!!!!!!\n");

    if (av.length == 0) usage();    
    if (av.length == 3) {
      flag = Integer.valueOf(av[2]).intValue();
      System.out.println("Valeur du flag: " + flag);
    }

    if (av[0].compareTo("-m")==0) {
      if (av.length < 1) usage();
      master();
    } else if (av[0].compareTo("-s")==0) {
      if (av.length < 2) usage();
      slave(av[1]);
    } else {
      // Padico
      padico_module_run(av);
    }    
  }
}
