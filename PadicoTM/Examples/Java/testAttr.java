import Padico.*;

public class testAttr {

  public static void padico_module_init(JModule mod) {
    System.err.println("In Padico Module Init of testArr");
    System.err.println("Trying to find toto attribute...");
    String val = mod.getAttr("toto");
    if (val != null)
      System.err.println("Found that toto = "+val);
    else
      System.err.println("Do not found toto attribute");
    System.err.println("Trying to set titi attribute to bird");
    mod.setAttr("titi","bird");
    String val2 = mod.getAttr("titi");
    if (val2 != null)
        if (val2 == "bird")
            System.err.println("Trying to set titi attribute to bird...ok");
        else
            System.err.println("Error: read titi = "+val2);
    else
        System.err.println("Error: attribute titi not found");

  }

  
  public static void padico_module_finalize(JModule mod) {
    System.err.println("In Padico Module Finalize of testArr");
    System.err.println("Trying to find toto attribute...");
    String val = mod.getAttr("toto");
    if (val != null)
      System.err.println("Found that toto = "+val);
    else
      System.err.println("Do not found toto attribute");
    System.err.println("Trying to find titi attribute...");
    String val2 = mod.getAttr("titi");
    if (val2 != null)
        if (val2 == "bird")
            System.err.println("Ok: titi = bird.");
        else
            System.err.println("Error: read titi = "+val2);

    else
      System.err.println("Do not found titi attribute");
  }


  public static void main(String av[]) throws Exception
  {               
    System.err.println(" testAttr main ok...");
  }
}
