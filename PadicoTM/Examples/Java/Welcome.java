import Padico.*;

public class Welcome {
    
  static int cnt=0;
  static private synchronized int get_cnt() {
    cnt++;
    return cnt;
  }
  
    public static void main(String av[]) throws Exception
    {           

      int c = get_cnt();

      System.out.println("["+c+"] Welcome starts at time "+JniPadico.getTime());

      Thread tid = Thread.currentThread();
        
      for(int i=1;i<11;i++) {
          System.out.println("["+c+"] Welcome ! "+i+"/10");
          tid.sleep(2000); // in ms
      } 

      System.out.println("["+c+"] Welcome has finished at time "+JniPadico.getTime());
    }
}
