import java.io.*;
import java.net.*;
import SocketParam;

/* Cette classe cr�e un flot d'entr�e et un flot de sortie pour la socket
   fournie en param�tre. On peut ensuite envoyer et recevoir des tableaux
   de taille variable (de type SocketParam).
*/

public class SocketDoWork {
    
    Socket s;
    DataInputStream  sin;
    DataOutputStream sout;

    SocketDoWork(java.net.Socket is) throws IOException {
        s = is;         
        sin  = new DataInputStream(s.getInputStream());
        sout = new DataOutputStream(s.getOutputStream());
    }

    void sendSize(SocketParam p) throws IOException  {
        sout.writeInt(p.n);
    }

    void recvSize(SocketParam p) throws IOException {
        p.n = sin.readInt();
        p.setBuf();
        p.clear();
    }

    void sendData(SocketParam p) throws IOException  {
        sout.write(p.buf, 0, p.n);
    }

    void recvData(SocketParam p) throws IOException {
        sin.readFully(p.buf, 0, p.n);
    }
}
