#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/NetAccess.h>
#include <Padico/Module.h>

#include <stdsoap2.h>

#include "soapH.h"
#include "HelloSOAP.nsmap"

#define SOAP_DEFAULT_PORT 18084
#define SOAP_URL_MAXSIZE 1024

static struct soap soap;

int soapclient_main(int argc, char **argv);

PADICO_MODULE_DECLARE(HelloSOAP_client, NULL, soapclient_main, NULL);


int soapclient_main(int argc, char **argv)
{
  char server[SOAP_URL_MAXSIZE];
  char*out = NULL;
  int rc;

  if (argc != 2)
    {
      fprintf(stderr, "Usage: %s <server>\n", argv[0]);
      padico_module_exit(0);
    }
  soap_init(&soap);
  snprintf(server, SOAP_URL_MAXSIZE, "http://%s:%d/HelloSOAP",
           argv[1], SOAP_DEFAULT_PORT);
  padico_print("Contacting: %s\n", server);
  rc = soap_call_HelloSOAP__SayHello(&soap, server, "", &out);
  if(rc)
    {
      padico_print("Failed!\n");
    }
  else
    {
      padico_print("Ok.\n");
    }
  soap_end(&soap);
  return 0;
}
