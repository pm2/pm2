/* Padico tutorial 3: "CORBA client"
 * PadicoTM/Examples/Tutorial/03-HelloCORBA/Hello-client.cc
 */

#include <iostream>
#include <Padico/CORBA.h>
#include <Padico/COSNaming.h>
#include <Padico/Module.h>

#include "Hello.h"

int client_init(void);
int say_hello(int argc, char**argv);

PADICO_MODULE_DECLARE(Hello_client, client_init, &say_hello, NULL);

static Padico::ORB_ptr                orb;
static CosNaming::NamingContext_ptr   naming_context;
static Hello_server_ptr               server;
static CosNaming::Name                server_name;

int client_init(void)
{
  orb = Padico::ORB_init();

  try
    {
      CORBA::Object_var ns_ref =
        orb->resolve_initial_references ("NameService");
      naming_context = CosNaming::NamingContext::_narrow (ns_ref);
    }
  catch(...)
    {
      padico_print("Hello-client: cannot resolve NameService.\n");
      return -1;
    }

  server_name.length (1);
  server_name[0].id = CORBA::string_dup("default");
  server_name[0].kind = CORBA::string_dup("HelloServer");

  try
    {
      CORBA::Object_var server_obj = naming_context->resolve(server_name);
      server = Hello_server::_narrow(server_obj);
    }
  catch(...)
    {
      padico_print("Hello-client: cannot resolve HelloServer in NameService.\n");
      return -1;
    }

  return 0;
}

int say_hello(int argc, char**argv)
{
  try {
    server->Hello();
  }
  catch(...)
    {
      padico_print("Hello-client: exception while invoking Hello server\n");
      return -1;
    }
  return 0;
}
