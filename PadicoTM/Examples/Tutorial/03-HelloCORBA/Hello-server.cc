/* Padico tutorial 3: "CORBA server"
 * PadicoTM/Examples/Tutorial/03-HelloCORBA/Hello-server.cc
 */

#include <iostream>
#include <Padico/CORBA.h>
#include <Padico/COSNaming.h>
#include <Padico/Module.h>
#include "Hello.h"

extern "C" int server_init(void);
extern "C" void server_finalize(void);

PADICO_MODULE_DECLARE(Hello_server, server_init, NULL, server_finalize);

class Hello_server_impl : virtual public POA_Hello_server
{
public:
  void Hello(void)
  {
    ::std::cout << "**********************************" << ::std::endl;
    ::std::cout << "********** Hello world! **********" << ::std::endl;
    ::std::cout << "**********************************" << ::std::endl;
  }
};


static Padico::ORB_ptr                orb;
static PortableServer::POA_var        poa;
static PortableServer::POAManager_var poa_manager;
static CosNaming::NamingContext_var   naming_context;
static Hello_server_impl*             server;
static PortableServer::ObjectId_var   server_oid;
static CosNaming::Name                server_name;

int server_init(void)
{
  orb = Padico::ORB_init();

  CORBA::Object_var poa_ref = orb->resolve_initial_references ("RootPOA");
  poa = PortableServer::POA::_narrow (poa_ref);
  poa_manager = poa->the_POAManager();
  poa_manager->activate();

  CORBA::Object_var ns_ref =
    orb->resolve_initial_references ("NameService");
  naming_context = CosNaming::NamingContext::_narrow (ns_ref);

  server = new Hello_server_impl;
  server_oid = poa->activate_object(server);

  server_name.length (1);
  server_name[0].id = CORBA::string_dup("default");
  server_name[0].kind = CORBA::string_dup("HelloServer");
  naming_context->rebind(server_name, server->_this());
  return 0;
}

void server_finalize(void)
{
  naming_context->unbind(server_name);
  //    poa->deactivate_object(server_oid);
  delete server;
}
