#! /bin/sh

if [ "x$PADICO_CORE" = "x" ]; then
  echo "$0 requires PADICO_CORE"
  exit 1
fi

padico-mkmod --driver=pkg --bootable --padico-core $PADICO_CORE -o ${PADICO_CORE}-java $PADICO_CORE VSock Kaffe

echo "Startup module with JAVA: ${PADICO_CORE}-java"
