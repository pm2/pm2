#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include <values.h>
#include <tbx.h>


#define LEN (8 * 1024 * 1024)
#define COUNT 500000

static char buffer[LEN];

int main(int argc, char**argv)
{
  int self = -1;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &self);
  int peer = 1 - self;
  const int tag = 0;

  long s;
  int count = COUNT;
  if(self == 0)
    printf("# size (bytes) \t time (usec.) \t bandwidth (MB/s)\n");
  for(s = 0; s < LEN; s = s * 1.4 + 1)
    {
      if(self == 0)
        {
          double min = DBL_MAX;
          int k;
          for(k = 0; k < count; k++)
            {
              MPI_Status status;
              tbx_tick_t t1, t2;
              TBX_GET_TICK(t1);
              MPI_Send(buffer, s, MPI_CHAR, peer, tag, MPI_COMM_WORLD);
              MPI_Recv(buffer, s, MPI_CHAR, peer, tag, MPI_COMM_WORLD, &status);
              TBX_GET_TICK(t2);
              const double d = TBX_TIMING_DELAY(t1, t2);
              if(d < min)
                min = d;
            }
          const double t = min / 2.0;
          const double bw = (double)s / t;
          printf("%lu\t%f\t%f \t(%d)\n", (long)s, t, bw, count);
          fflush(stdout);
        }
      else
        {
          int k;
          for(k = 0; k < count; k++)
            {
              MPI_Status status;
              MPI_Recv(buffer, s, MPI_CHAR, peer, tag, MPI_COMM_WORLD, &status);
              MPI_Send(buffer, s, MPI_CHAR, peer, tag, MPI_COMM_WORLD);
            }
        }
      count /= 1.2;
      if(count < 10)
        count = 10;
    }
  MPI_Finalize();
  return 0;
}
