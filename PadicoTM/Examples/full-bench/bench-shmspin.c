/* bench-shmspin.c
 * benchmark for shared memory transfer with spin waiting
 */


#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "bench-common.h"
#include <Padico/Module.h>
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

int bench_shmspin_init(void);
int bench_shmspin_run(int argc, char**argv);
void bench_shmspin_finalize(void);

PADICO_MODULE_DECLARE(bench_shmspin, bench_shmspin_init, bench_shmspin_run, bench_shmspin_finalize);

static char shm_path[256] = { '\0' };

#define BUFSIZE (8 * 1024 * 1024)

struct buffer_s
{
  volatile int busy;  /**< data posted in buffer */
  volatile int clear; /**< buffer can be written */
  char buf[BUFSIZE];
};
struct seg_s
{
  struct buffer_s buffers[2];
};

static struct buffer_s*self = NULL;
static struct buffer_s*peer = NULL;
static struct seg_s*seg = NULL;

int bench_shmspin_init(void)
{
  snprintf(shm_path, 256, "/dev/shm/bench-shmspin-%s", padico_getenv("USER"));
  int fd = PUK_ABI_FSYS_WRAP(open)(shm_path, O_RDWR|O_CREAT, 00700);
  if(fd == -1)
    {
      padico_fatal("BENCH: shm_open failed.\n");
    }
  ftruncate(fd, sizeof(struct seg_s));
  seg = PUK_ABI_FSYS_WRAP(mmap)(NULL, sizeof(struct seg_s), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if(seg == MAP_FAILED)
    {
      padico_fatal("BENCH: mmap failed (%s) fd = %d.\n", strerror(errno), fd);
    }
  if(bench_common_param.is_server)
    {
      self = &seg->buffers[0];
      peer = &seg->buffers[1];
    }
  else
    {
      self = &seg->buffers[1];
      peer = &seg->buffers[0];
    }
  self->busy  = 0;
  self->clear = 1;

  while(!peer->clear)
    ;
  if(bench_common_param.is_server)
    {
      PUK_ABI_FSYS_WRAP(close)(fd);
      unlink(shm_path);
    }

  return 0;
}

void bench_shmspin_finalize(void)
{
}

static inline void my_shm_send(const char*ptr, int size)
{
  while(!peer->clear)
    ;
  __sync_synchronize();
  if(size > 0)
    memcpy(peer->buf, ptr, size);
  peer->busy = 1;
}

static inline void my_shm_recv(char*ptr, int size)
{
  while(!self->busy)
    ;
  __sync_synchronize();
  if(size > 0)
    memcpy(ptr, self->buf, size);
  self->busy = 0;
  self->clear = 1;
}

static void shmspin_server(char*buffer, size_t lenbuf)
{
  my_shm_send(buffer, lenbuf);
  my_shm_recv(buffer, lenbuf);
}

static void shmspin_client(char*buffer, size_t lenbuf)
{
  my_shm_recv(buffer, lenbuf);
  my_shm_send(buffer, lenbuf);
}

static struct bench_desc_s shmspin_bench =
  {
    "shmspin",
    &shmspin_server,
    &shmspin_client
  };

int bench_shmspin_run(int argc, char**argv)
{
  the_bench(&shmspin_bench);
  return 0;
}
