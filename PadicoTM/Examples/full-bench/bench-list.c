/* bench-list.c
 * benchmark for list-based single process queues
 */

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>

int bench_list_init(void);
int bench_list_run(int argc, char**argv);
void bench_list_finalize(void);

PADICO_MODULE_DECLARE(bench_list, bench_list_init, bench_list_run, bench_list_finalize);

/* ********************************************************* */

PUK_LIST_TYPE(bench_buffer,
              volatile void*_buffer;
              );

static pthread_spinlock_t lock;

static bench_buffer_list_t l;

int bench_list_init(void)
{
  l = bench_buffer_list_new();
  pthread_spin_init(&lock, 0);
  return 0;
}

void bench_list_finalize(void)
{
  bench_buffer_list_delete(l);
}

static void bench_list_server(char*buffer, size_t lenbuf)
{
  struct bench_buffer_s e1, e2, *e;

  pthread_spin_lock(&lock);
  e1._buffer = buffer;
  bench_buffer_list_push_back(l, &e1);
  pthread_spin_unlock(&lock);

  pthread_spin_lock(&lock);
  e = bench_buffer_list_pop_front(l);
  buffer = e->_buffer;
  pthread_spin_unlock(&lock);

  pthread_spin_lock(&lock);
  e2._buffer = buffer;
  bench_buffer_list_push_back(l, &e2);
  pthread_spin_unlock(&lock);

  pthread_spin_lock(&lock);
  e = bench_buffer_list_pop_front(l);
  buffer = e->_buffer;
  pthread_spin_unlock(&lock);
}

static void bench_list_client(char*buffer, size_t lenbuf)
{
  /* client does nothing */
}

static struct bench_desc_s bench_list =
  {
    "list",
    &bench_list_server,
    &bench_list_client
  };

int bench_list_run(int argc, char**argv)
{
  the_bench(&bench_list);
  return 0;
}
