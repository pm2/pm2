/** @file
 * @brief gSOAP 'Hello' interface example
 */

//gsoap HelloSOAP service name: HelloSOAP
//gsoap HelloSOAP schema namespace: urn:Padico

struct HelloSOAP__longlist_s
{
  int __sizedata;
  long int*data;
};

int HelloSOAP__SendData(struct HelloSOAP__longlist_s in,
                        struct HelloSOAP__longlist_s*out);

int HelloSOAP__SendString(char*in, char**out);
