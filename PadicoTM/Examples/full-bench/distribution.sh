#! /bin/bash
######################################################################################################
# distribution.sh
#
# NOM
#        distribution.sh - Cr�e une courbe de distribution
#
# SYNOPSIS
#       ./distribution.sh [OPTION] ENTR�E
#
# DESCRIPTION
#       Cr�e une courbe de la distribution de chaque fichier en entr�e.
#
#       -h
#		afficher les r�gles d'usage
#
#       -o fichier
#		sp�cifier le fichier de sortie (par d�faut out.ps)
#
#       -min N
#		d�finir le d�but de l'intervale d'affichage (par d�faut 0)
#		
#       -max N
#		d�finir la fin de l'intervale d'affichage (par d�faut 120)
#
#       Les fichiers de donn�es sont constitu�s de suites de nombres :
#	   81.7
#	   79.9
#	   80.5
#	   83.4
#	   81.3
#	   79.1
#	   80.2
#
# EXEMPLE D'UTILISATION
#
#       $> grep "#4 bytes" /tmp/VIO-login.dis |awk '{print$3}'> 4_bytes
#       $> grep "#8 bytes" /tmp/VIO-login.dis |awk '{print$3}'> 8_bytes
#       $> ./distribution.sh -min 20 -max 50 -o distribution.ps 8_bytes 4_bytes 
######################################################################################################

usage() {
    echo "USAGE : $0 [-o fichier.ps] [-min intervale_min] [-max intervale_max] fichiers"
}

list_fic=""
ps_file=out.ps
int_min=0
int_max=0
user_int_min=0
user_int_max=0

to_delete=""

until [ -z "$1" ]  # Until all parameters used up...
do
  case $1 in
      -o)
	  shift
	  ps_file=$1
	  ;;
      -min)
	  shift
	  user_int_min=$1
	  ;;
      -max)
	  shift
	  user_int_max=$1
	  ;;
      -b)
	  shift
	  bytes="$1"
	  f="$2"
	  tmp=/tmp/dis-${USER}-${bytes}.dat
	  to_delete="${to_delete} ${tmp}"
	  trap "/bin/rm ${to_delete}" 0
	  grep "#${bytes} bytes" $f | awk '{print$3}'> $tmp
	  list_fic="${list_fic} $tmp"
	  shift
	  ;;
      -h|--help)
	  usage
	  exit 0
	  ;;
      *)
	  list_fic="$list_fic $1"
	  ;;
  esac
  shift
done

if [ -z "$list_fic" ]; then
    usage
    exit 1;
fi

for fic in $list_fic
  do
  liste=$( sort -g $fic )
  i=0;
  prev=0;
  file="$fic.stat"
  echo "# processing ${fic}..."
  to_delete="${to_delete} ${file}"
  trap "/bin/rm ${to_delete}" 0
  rm -f $file
  touch $file
  for ligne in $liste
    do  
    v=$( echo $ligne | cut -f 1 -d '.' )
    if [ ${int_min} = 0 -o ${v} -lt ${int_min} ]; then
	int_min=$(( $v - 1 ))
    fi
    if [ ${int_max} = 0 -o ${v} -gt ${int_max} ]; then
	int_max=$(( $v + 1 ))
    fi
    case $ligne in
	"$prev")
	    ;;
	*)
	    echo -e "$prev\t $i " >> $file
	    prev=$ligne;
	    i=0;
	    ;;
    esac
    i=$((i+1));
  done
done

if [ $user_int_min != 0 ]; then
    int_min=$user_int_min
fi
if [ $user_int_max != 0 ]; then
    int_max=$user_int_max
fi

gp_file=/tmp/courbe-${USER}.gnuplot
to_delete="${to_delete} ${gp_file}"
trap "/bin/rm ${to_delete}" 0

cat > $gp_file <<EOF
set terminal postscript color
set output "$ps_file"
set style data linespoints
set ylabel "count"
set xlabel "latency (usec.)"
plot [${int_min}:${int_max}] $( echo ${list_fic} | sed -e 's/\([^ ]*\)/"\1.stat"/g;s/ /,/g' )
EOF

cat $gp_file 
gnuplot ${gp_file}

