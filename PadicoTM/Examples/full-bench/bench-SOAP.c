/* bench-SOAP.c
 */

#include "bench-common.h"

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>

int bench_soap_init(void);
int bench_soap_run(int argc, char**argv);
void bench_soap_finalize(void);

PADICO_MODULE_DECLARE(bench_SOAP, bench_soap_init, bench_soap_run, bench_soap_finalize);

#include <stdsoap2.h>

#include "soapH.h"
#include "HelloSOAP.nsmap"

#define SOAP_DEFAULT_PORT 18084
#define SOAP_URL_MAXSIZE 1024

static struct soap soap;
static char server_url[SOAP_URL_MAXSIZE];

/* ********************************************************* */

int HelloSOAP__SendData(struct soap *soap,
                        struct HelloSOAP__longlist_s inparam,
                        struct HelloSOAP__longlist_s*outparam)
{
  *outparam = inparam;
  return 0;
}

int HelloSOAP__SendString(struct soap *soap, char*inparam, char**outparam)
{
  *outparam = inparam;
  return 0;
}

/* ********************************************************* */

static int bench_soap_make_server(void)
{
  int rc = 0;
  int m;
  int soap_port = SOAP_DEFAULT_PORT;
  const char*soap_port_string = padico_getattr("SOAP_PORT");

  if(soap_port_string != NULL)
    {
      soap_port = atoi(soap_port_string);
    }

  padico_out(puk_verbose_always, "creating HelloSOAP server on port %d\n", soap_port);
  soap_init(&soap);
  m = soap_bind(&soap, NULL, soap_port, 10);
  if (m < 0)
    {
      padico_out(puk_verbose_always, "server failed; rc = %d\n", m);
      soap_print_fault(&soap, stderr);
      rc = -1;
    }
  else
    {
      padico_out(puk_verbose_always, "server ready; URL=http://%s:%d/HelloSOAP\n",
                   padico_topo_host_getname(padico_topo_getlocalhost()), soap_port);
    }
  return rc;
}

static void* bench_soap_server_worker(void*dummy)
{
  int s;
  padico_out(puk_verbose_always, "starting SOAP server\n");
  for ( ; ; )
    {
      s = soap_accept(&soap);
      if (s < 0)
        {
          padico_out(puk_verbose_always, "error in soap_accept(); rc = %d\n", s);
          soap_print_fault(&soap, stderr);
        }
      else
        {
          int rc;
          rc = soap_serve(&soap);
          if(rc)
            {
              padico_out(puk_verbose_always, "error in soap_serve(); rc = %d\n", rc);
              soap_print_fault(&soap, stderr);
            }
          soap_end(&soap);
        }
    }
  return NULL;
}


/* ********************************************************* */

static void bench_soap_init_client(const char*name)
{
  soap_init(&soap);
  snprintf(server_url, SOAP_URL_MAXSIZE, "http://%s:%d/HelloSOAP",
           name, SOAP_DEFAULT_PORT);
  padico_out(puk_verbose_always, "Client init connection to server: %s (%s)\n", server_url, name);
}

int bench_soap_init(void)
{
  marcel_t bench_soap_tid;
  if(!bench_common_param.is_server)
    {
      bench_soap_make_server();
      marcel_create(&bench_soap_tid, NULL, &bench_soap_server_worker, NULL);
    }
  else
    {
      bench_soap_init_client(padico_topo_host_getname(padico_topo_node_gethost(bench_common_param.server_node)));
    }
  return 0;
}

static void soap_server(char*buffer, size_t lenbuf)
{
  struct HelloSOAP__longlist_s outparam;
  struct HelloSOAP__longlist_s inparam;

  inparam.__sizedata = lenbuf;
  inparam.data = calloc(lenbuf, sizeof(long));
  padico_out(20, "Contacting: %s (size = %zu)\n", server_url, lenbuf);
  outparam.__sizedata = 0;
  outparam.data = NULL;
  int rc = soap_call_HelloSOAP__SendData(&soap, server_url, "", inparam, &outparam);
  if(outparam.data)
    free(outparam.data);
  if(rc)
    {
      padico_out(puk_verbose_always, "size = %zu; failed! rc = %d\n", lenbuf, rc);
      soap_print_fault(&soap, stderr);
    }
  free(inparam.data);
}

static void soap_client(char*buffer, size_t lenbuf)
{
}


/* ********************************************************* */

struct bench_desc_s soap_bench =
{
  .name   = "SOAP",
  .server = &soap_server,
  .client = &soap_client
};

int bench_soap_run(int argc, char**argv)
{
  the_bench(&soap_bench);
  return 0;
}

void bench_soap_finalize(void)
{
  soap_end(&soap);
}
