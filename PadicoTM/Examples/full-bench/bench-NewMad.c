/** @file bench-NewMad.c
 */

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>
#include <nm_launcher_interface.h>
#include <nm_launcher.h>

int bench_newmad_init(void);
int bench_newmad_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_NewMad, bench_newmad_init, bench_newmad_run, NULL);

static nm_gate_t peer_gate = NULL;
static nm_session_t p_session = NULL;

int bench_newmad_init(void)
{
  int fake_argc = 1;
  char*fake_argv[] = { "bench-NewMad", NULL };
  putenv("NM_LAUNCHER=madico");
  nm_launcher_init(&fake_argc, fake_argv);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int size = -1;
  nm_launcher_get_size(&size);

  nm_session_open(&p_session, "bench-NewMad");
  int peer = (rank % 2 == 0) ? (rank + 1) : (rank - 1);
  assert(peer >= 0 && peer < size);
  nm_launcher_get_gate(peer, &peer_gate);

  padico_out(puk_verbose_always, "bench-NewMad: size = %d; rank = %d; peer_gate = %p\n", size, rank, peer_gate);

  return 0;
}

void newmad_server(char*buf, size_t len)
{
  nm_sr_request_t request;

  nm_sr_isend(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_swait(p_session, &request);

  nm_sr_irecv(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_rwait(p_session, &request);
}

void newmad_client(char*buf, size_t len)
{
  nm_sr_request_t request;

  nm_sr_irecv(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_rwait(p_session, &request);

  nm_sr_isend(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_swait(p_session, &request);
}

static const struct bench_desc_s newmad_bench =
{
  .name = "NewMad",
  .server = &newmad_server,
  .client = &newmad_client
};

int bench_newmad_run(int argc, char**argv)
{
  the_bench(&newmad_bench);
  return 0;
}
