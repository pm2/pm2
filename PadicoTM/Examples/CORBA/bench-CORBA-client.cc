/* bench-CORBA-client.cc
 */

#include <omniORB4/CORBA.h>
#include <omniORB4/Naming.hh>
#include "bench-CORBA.h"
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>

int main(int argc, char*argv[])
{
  CORBA::ORB_ptr                 orb;
  bench_CORBA_var                bench_ref = NULL;
  PortableServer::POA_var        poa;
  PortableServer::POAManager_var poa_manager;
  CosNaming::NamingContext_var   naming_context;
  PortableServer::ObjectId_var   bench_server_oid;
  CosNaming::Name                bench_server_name;

  orb = CORBA::ORB_init(argc, argv);

  std::cerr << "CORBA- resolving NameService...\n" << std::endl;
  int retryns = 5;
  while(retryns >= 0)
    {
      try
        {
          CORBA::Object_var poa_ref = orb->resolve_initial_references ("RootPOA");
          poa = PortableServer::POA::_narrow (poa_ref);
          poa_manager = poa->the_POAManager();
          poa_manager->activate();

          CORBA::Object_var ns_ref = orb->resolve_initial_references ("NameService");
          naming_context = CosNaming::NamingContext::_narrow (ns_ref);
          break;
        }
      catch(...)
        {
          std::cerr <<  "CORBA- error while resolving NameService.\n" << std::endl;
          if(retryns > 0)
            {
              retryns--;
              sleep(1);
            }
          else
            {
              abort();
            }
        }
    }
  bench_server_name.length (1);
  bench_server_name[0].id = CORBA::string_dup("default");
  bench_server_name[0].kind = CORBA::string_dup("bench_CORBA");

  std::cerr << "CORBA- activating the client...\n" << std::endl;
  try
    {
      CORBA::Object_var server_obj = naming_context->resolve(bench_server_name);
      std::cerr << "CORBA- client ready [" <<  orb->object_to_string(server_obj) << "]\n" << std::endl;
      bench_ref = bench_CORBA::_narrow(server_obj);
    }
  catch(...)
    {
      std::cerr << "CORBA- cannot resolve object.\n" << std::endl;
      abort();
    }

  /* establish connection */
  int retry = 5;
  while(retry >= 0)
    {
      try
        {
          std::cerr << "CORBA- client invoking request\n" << std::endl;
          bench_ref->LT(); /* first shot to establish the link-
                            * if it raises no exception, then the servant is ready */
          retry = -1;
        }
      catch(CORBA::SystemException&e)
        {
          std::cerr << "CORBA- cannot establish connection to benchmark server.\n"
                    << e._rep_id() << " -- " << e.NP_minorString() << std::endl;
          if(retry > 0)
            {
              retry--;
              sleep(1);
            }
          else
            {
              abort();
            }
        }
      catch(CORBA::Exception&e)
        {
          std::cerr << "CORBA- got unkown exception while connecting to server.\n" << std::endl;
          abort();
        }
    }

  /* latency test */
  try
    {
      const int latency_num_roundtrips = 100;
      std::cerr << "CORBA- client performing latency benchmark ("<<latency_num_roundtrips<<" roundtrips), please wait...\n" << std::endl;
      struct timespec t1, t2;
      clock_gettime(CLOCK_MONOTONIC, &t1);
      for(int i = 0; i < latency_num_roundtrips; i++)
        {
          bench_ref->LT();
        }
      clock_gettime(CLOCK_MONOTONIC, &t2);
      const double lat_usec = (1000000.0*(t2.tv_sec - t1.tv_sec) + (t2.tv_nsec - t1.tv_nsec) / 1000.0)
        / (2.0 * latency_num_roundtrips);
      std::cerr << "CORBA- latency: "<<lat_usec<<" microseconds\n" << std::endl;
    }
  catch(...)
    {
      std::cerr << "CORBA- latency test failed.\n" << std::endl;
      abort();
    }

  /* bandwidth test */
  try
    {
      std::cerr << "CORBA- lat. (usec) \t bw (MB/s)" << std::endl;
      int len;

      for(len = 1; len <= 1024*1024; len *=2)
        {
          bench_CORBA::SeqOctet_t msg = bench_CORBA::SeqOctet_t(len);
          msg.length(len);
          struct timespec t1, t2;
          clock_gettime(CLOCK_MONOTONIC, &t1);
          bench_ref->RO(msg);
          clock_gettime(CLOCK_MONOTONIC, &t2);
          const double time_usec = (1000000.0*(t2.tv_sec - t1.tv_sec) + (t2.tv_nsec - t1.tv_nsec) / 1000.0) / (2.0 );
          const double bw_mbps = len / time_usec;
          std::cerr.precision(8);
          std::cerr << len << "\t" << time_usec <<  "   \t" << bw_mbps << std::endl;
        }
    }
  catch(...)
    {
      std::cerr << "CORBA- bandwidth test failed.\n" << std::endl;
      abort();
    }

  std::cerr << "CORBA- finalizing the client...\n" << std::endl;
  //bench_ref->Finish();

  orb->shutdown(true);
  orb->destroy();

  return 0;
}
