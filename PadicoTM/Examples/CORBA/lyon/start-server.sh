#! /bin/sh 

if [ "x$1" = "x" ]; then
    echo "usage: $0 <host>" 1>&2
    echo "  with <host> hostname of the omniNames+rdv server" 1>&2
    exit 1
fi

nameserver="$1"
rdv="$1"

nsconfig=${PWD}/../../NetSelector/lyon.xml

if grep "${rdv}" ${nsconfig} > /dev/null ; then
    echo "host ${rdv} foudn in config." 1>&2
    echo "using config file: ${nsconfig}" 1>&2
else
    echo "host ${rdv} not defined in config ${nsconfig}" 1>&2
exit 1
fi

set -x

padico-launch -v -nodefault -DNS_BASIC_CONF=${nsconfig} -DPADICO_BOOTSTRAP_PORT=30201 -DPADICO_BOOTSTRAP_HOST=${rdv} -iNetSelector-preset-vanilla -iControler-preset-router ../bench-CORBA-server -ORBInitRef NameService=corbaloc:iiop:${nameserver}:2809/NameService

