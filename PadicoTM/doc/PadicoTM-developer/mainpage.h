/** @file
 * @brief Main page for the HTML documentation
 */

/** @mainpage
  @image html padico-small.jpeg

 * @htmlonly

 <h2>PadicoTM developer's guide</h2>

 <p> 
 This documentation is about using PadicoTM in your codes.
 It is intended for developers only.
 </p>

 <ul>
  <li>List of <a href="modules.html">modules</a> for C/C++/Fortran API</li>
  <li>List of <a href="group__PadicoComponent.html">adapters</a> supplied by PadicoTM</li>
  <li><a href="examples.html">Examples and tutorials</a></li>
  <li>List of Java <a href="packages.html">packages</a> for Java API (note: Java support is experimental and probably broken)</li>
  <li>Browse PadicoTM <a href="files.html">files</a></li>
 </ul>


 <h2>Further reading</h2>

 <ul>
   <li>Padico user's manual: see 
   <a href="../../Padico-manual.ps"><tt>Padico-manual.ps</tt></a>
   </li>
   
   <li><a href="http://runtime.futurs.inria.fr/PadicoTM/">PadicoTM homepage</a></li>
 </ul>

 * @endhtmlonly
 */
