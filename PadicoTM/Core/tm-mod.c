/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM Mod manager
 * @ingroup TaskManager
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "PM2.h"
#include "TaskManager-internals.h"

PADICO_MODULE_HOOK(PadicoTM);

/* ********************************************************* */

static void tm_mod_notify(void*arg)
{
  padico_req_t req = (padico_req_t)arg;
  padico_tm_req_notify(req->id, NULL);
}

padico_rc_t padico_tm_mod_action_args(enum padico_mtask_kind_e kind,
                                      const char*mod_name,
                                      int argc,
                                      char**argv)
{
  padico_task_t task = padico_malloc(sizeof(struct padico_task_s));
  padico_req_t req = padico_tm_req_new(NULL, NULL);
  padico_out(40, "entering- mod=%s; req=%d; kind=%d\n", mod_name, req->id, kind);
  task->kind = PADICO_TM_TASK_MOD;
  task->task_req = req;
  task->tinfo.mtask.kind = kind;
  task->tinfo.mtask.mod_name = padico_strdup(mod_name);
  task->tinfo.mtask.argc = argc;
  task->tinfo.mtask.argv = argv;
  padico_out(40, "enqueue task for mod=%s\n", mod_name);
  padico_tm_task_enqueue(task, "mod action");
  task = NULL; /* the taskengine takes ownership of 'task'; it might be freed at anytime */
  padico_out(40, "waiting for task completion- mod=%s...\n", mod_name);
  padico_rc_t rc = padico_tm_req_wait(req);
  if(padico_rc_iserror(rc))
    {
      padico_warning("error %d while processing action item kind = %d; mod = %s (%s)\n",
                     rc->rc, kind, mod_name, padico_rc_gettext(rc));
      padico_rc_show(rc);
    }
  padico_out(40, "exiting- mod=%s\n", mod_name);
  return rc;
}


void padico_tm_mod_process(padico_task_t task)
{
  puk_mod_t mod;
  padico_rc_t rc = NULL;
  switch(task->tinfo.mtask.kind)
    {
    case PADICO_TM_MOD_LOAD:
      {
        padico_out(40, "loading mod=%s\n", task->tinfo.mtask.mod_name);
        rc = padico_puk_mod_resolve(&mod, task->tinfo.mtask.mod_name);
        if(!padico_rc_iserror(rc))
          {
            assert(task->parent != NULL);
            puk_mod_set_ancestor(mod, task->parent);
            rc = padico_puk_mod_load(mod);
            padico_trace("module %s loaded.\n", task->tinfo.mtask.mod_name);
          }
        if(task->task_req)
          {
            padico_tm_req_notify(task->task_req->id, rc);
          }
        else
          {
            padico_rc_delete(rc);
          }
      }
      break;

    case PADICO_TM_MOD_UNLOAD:
      {
        padico_out(40, "unloading mod=%s\n", task->tinfo.mtask.mod_name);
        mod = puk_mod_getbyname(task->tinfo.mtask.mod_name);
        if(mod == NULL)
          {
            rc = padico_rc_error("cannot unload module **%s**: not found.",
                                 task->tinfo.mtask.mod_name);
          }
        else
          {
            rc = padico_puk_mod_unload(mod);
            padico_trace("module %s unloaded.\n", task->tinfo.mtask.mod_name);
          }
        if(task->task_req)
          {
            padico_tm_req_notify(task->task_req->id, rc);
          }
        else
          {
            padico_rc_delete(rc);
          }
      }
      break;

    case PADICO_TM_MOD_RUN:
      {
        padico_out(40, "running mod=%s\n", task->tinfo.mtask.mod_name);
        mod = puk_mod_getbyname(task->tinfo.mtask.mod_name);
        if(mod == NULL)
          {
            rc = padico_rc_error("cannot run module **%s**: not found.", task->tinfo.mtask.mod_name);
          }
        else
          {
            padico_print("running module: %s\n", task->tinfo.mtask.mod_name);
            puk_job_t job = padico_malloc(sizeof(struct puk_job_s));
            job->argc = task->tinfo.mtask.argc;
            job->argv = task->tinfo.mtask.argv;
            job->rc = NULL;
            job->notify = &tm_mod_notify;
            job->notify_key = task->task_req;
            rc = padico_puk_mod_start(mod, job);
            padico_trace("module %s run ok\n", task->tinfo.mtask.mod_name);
          }
      }
      break;

    case PADICO_TM_MOD_START:
      {
        padico_out(40, "starting mod=%s\n", task->tinfo.mtask.mod_name);
        mod = puk_mod_getbyname(task->tinfo.mtask.mod_name);
        if(mod == NULL)
          {
            rc = padico_rc_error("cannot start module **%s**: not found.", task->tinfo.mtask.mod_name);
          }
        else
          {
            padico_trace("starting module: %s\n", task->tinfo.mtask.mod_name);
            puk_job_t job = padico_malloc(sizeof(struct puk_job_s));
            job->argc = task->tinfo.mtask.argc;
            job->argv = task->tinfo.mtask.argv;
            job->rc = NULL;
            job->notify = NULL;
            job->notify_key = NULL;
            rc = padico_puk_mod_start(mod, job);
            padico_trace("module %s start ok\n", task->tinfo.mtask.mod_name);
          }
        if(task->task_req)
          {
            padico_tm_req_notify(task->task_req->id, rc);
          }
        else
          {
            padico_rc_delete(rc);
          }
      }
      break;
    }
  padico_free(task->tinfo.mtask.mod_name);
  return;
}
