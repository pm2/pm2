# ### headers: pthreads and semaphores
s,pthread[.]h,Padico/PM2.h,g
s,semaphore[.]h,Padico/PM2.h,g

# ### pthreads to marcel
s,pthread_,pmarcel_,g
s,PTHREAD,PMARCEL,g

# ### semaphores to marcel
s,sem_t,marcel_sem_t,g
s,sem_wait,marcel_sem_P,g
s,sem_post,marcel_sem_V,g
s/sem_init[ ]*(\(.*\),.*,\(.*\))/marcel_sem_init(\1,\2)/g
