/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM tasks, tasklets, and event channels
 * @note shouldn't be used directly: #include <Padico/PadicoTM.h> instead.
 * @ingroup TaskManager
 */

#ifndef TM_TASKS_H
#define TM_TASKS_H

/** @defgroup PadicoTasks API: PadicoTM tasks, tasklets, and event channels
 * @ingroup TaskManager
 */

/** @addtogroup PadicoTasks
 * @{
 */


/* ******************************************************* */
/* *** padico_tasklet */

/** An abstract tasklet descriptor
 * @note struct padico_tasklet_s is private (implementation-dependant)
 * and needn't be exposed in the API
 */
typedef struct padico_tasklet_s*padico_tasklet_t;

typedef void (*padico_event_listener_t)(void*event);

/** Create a new tasklet */
padico_tasklet_t padico_tasklet_new(const char*name);
/** Stop and destroy a tasklet */
void padico_tasklet_destroy(padico_tasklet_t t);
/** Add an event listener to the tasklet */
void padico_tasklet_subscribe(padico_tasklet_t t, padico_event_listener_t f);
/** Remove an event listener from a tasklet */
void padico_tasklet_unsubscribe(padico_tasklet_t t, padico_event_listener_t f);
/** schedule a new task item to the tasklet */
void padico_tasklet_schedule(padico_tasklet_t t, void*item, const char*name);
void padico_tasklet_acquire(padico_tasklet_t t);
void padico_tasklet_release(padico_tasklet_t t);
void padico_tasklet_flush(padico_tasklet_t t);


/** @} */

#endif /* TM_TASKS_H */
