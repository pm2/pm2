/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM Task Engine
 * @ingroup TaskManager
 */

#include <unistd.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "PM2.h"
#include "TaskManager-internals.h"

PADICO_MODULE_HOOK(PadicoTM);


/** an action item for a tasklet */
PUK_LIST_TYPE(padico_tasklet_action,
              padico_tasklet_t tasklet;
              void*data;
              padico_event_listener_t listener;
              int*listener_num;
              char*name;
              );

#define PADICO_TASKLET_OWNER_NULL ((marcel_t)0x0)

PUK_VECT_TYPE(padico_event_listener, padico_event_listener_t);

/** a tasklet descriptor */
struct padico_tasklet_s
{
  const char*name;
  marcel_t owner;   /**< tid of the thread which has currently access to the tasklet */
  marcel_t worker;  /**< dedicated tasklet worker thread */
  int level;
  marcel_mutex_t lock;   /**< protects the fields: owner, level, and actions */
  marcel_cond_t  list_event;  /**< signals events int the 'actions' list */
  struct padico_event_listener_vect_s subscribers;
  padico_tasklet_action_list_t actions; /**< list of deferred actions to process */
  volatile int running;
};

/* ********************************************************* */

static struct
{
  /* Puk tasks */
  padico_tasklet_t   tasks;
  /* reqs */
  padico_req_id_t    next_req_id;
  marcel_mutex_t     req_lock;
  marcel_cond_t      req_finished;
  struct padico_req_list_s reqs;
  /* task in main thread */
  marcel_mutex_t     main_lock;
  marcel_cond_t      main_cond;
  padico_task_t      main_task;
  /* tasklet */
  marcel_key_t       current_tasklet;
} te =
  {
    .tasks = NULL,
    .next_req_id = 1
  };

static void padico_tm_tasks_process(void*data);
static inline void padico_tasklet_dispatch(padico_tasklet_t t);

/* TASK ENGINE ********************************************* */

void padico_tm_task_engine_init(void)
{
  padico_req_list_init(&te.reqs);
  marcel_mutex_init(&te.req_lock, NULL);
  marcel_cond_init(&te.req_finished, NULL);
  marcel_mutex_init(&te.main_lock, NULL);
  marcel_cond_init(&te.main_cond, NULL);
  te.main_task = NULL;
  marcel_key_create(&te.current_tasklet, NULL);
  te.tasks = padico_tasklet_new("PadicoTM_Task");
  padico_out(40, "subscribing to tasklet.\n");
  padico_tasklet_acquire(te.tasks);
  padico_tasklet_subscribe(te.tasks, &padico_tm_tasks_process);
  padico_tasklet_release(te.tasks);
  padico_out(40, "done.\n");
}

void padico_tm_task_engine_finalize(void)
{
  padico_req_list_destroy(&te.reqs);
  padico_tasklet_destroy(te.tasks);
}

void padico_tm_task_enqueue_from(padico_task_t task, puk_mod_t parent, const char*name)
{
  if(te.tasks == NULL)
    {
      padico_fatal("trying to enqueue a task before starting the task engine.\n");
    }
  task->parent = parent;
  padico_tasklet_t tasklet = te.tasks;
  marcel_mutex_lock(&tasklet->lock);
  marcel_t owner = tasklet->owner;
  const int is_owner = owner == marcel_self();
  marcel_mutex_unlock(&tasklet->lock);
  if(is_owner)
    {
      /* do not schedule task later- since we are dispatching tasklets, this is a actually a dependancy */
      padico_tm_tasks_process(task);
    }
  else
    {
      padico_out(30, "enqueue task kind = %d\n", task->kind);
      padico_tasklet_schedule(te.tasks, task, name);
    }
}

static void padico_tm_tasks_process(void*_data)
{
  padico_task_t task = _data;
  padico_out(50, "processing task %p (kind=%d; req=%d)\n", task, task->kind, task->task_req?task->task_req->id:-1);
  switch(task->kind)
    {
    case PADICO_TM_TASK_EXIT:
      {
        padico_print("- finalizing Puk...\n");
        padico_puk_shutdown();
        padico_print("- ok.\n");
        /* we can't unload PadicoTM. We can only exit. */
        PUK_ABI_PROC_WRAP(exit)(0);
      }
      break;

    case PADICO_TM_TASK_MOD:
      {
        padico_tm_mod_process(task);
      }
      break;

    case PADICO_TM_TASK_FILE:
      {
        const char*basefile = task->tinfo.ftask.file;
        struct puk_parse_entity_s pe = puk_xml_parse_init(basefile);
        padico_rc_t rc = pe.rc;
        if(padico_rc_iserror(rc))
          {
            padico_warning("error while reading init file %s\n", basefile);
            padico_rc_show(rc);
          }
        padico_rc_delete(rc);
        if(task->task_req)
          {
            padico_tm_req_notify(task->task_req->id, rc);
          }
        padico_free(task->tinfo.ftask.file);
      }
      break;

    case PADICO_TM_TASK_CMD:
      {
        struct puk_parse_entity_s pe;
        padico_fatal("XML parsing in TaskManager thread not supported.");
        pe = puk_xml_parse_buffer(task->tinfo.cmdtask.cmd,
                                  strlen(task->tinfo.cmdtask.cmd),
                                  task->tinfo.cmdtask.trust_level);
        padico_rc_t rc = pe.rc;
        padico_rc_delete(rc);
        if(task->task_req)
          {
            padico_tm_req_notify(task->task_req->id, rc);
          }
      }
      break;

    case PADICO_TM_TASK_MAIN:
      {
        padico_req_t req = task->task_req;
        padico_out(40, "task in main.\n");
        marcel_mutex_lock(&te.main_lock);
        te.main_task = task;
        marcel_cond_signal(&te.main_cond);
        while(te.main_task != NULL)
          {
            marcel_cond_wait(&te.main_cond, &te.main_lock);
          }
        marcel_mutex_unlock(&te.main_lock);
        if(req)
          {
            padico_tm_req_notify(req->id, NULL);
          }
      }
      break;

    }
  padico_free(task);
  padico_out(50, "task %p done.\n", task);
}

void padico_tm_exit(void)
{
  padico_task_t task = padico_malloc(sizeof(struct padico_task_s));
  padico_out(40, "exit requested\n");
  task->kind  = PADICO_TM_TASK_EXIT;
  task->task_req   = NULL;
  padico_tm_task_enqueue(task, "exit");
}

void padico_tm_task_engine_run(void)
{
  marcel_mutex_lock(&te.main_lock);
  for(;;)
    {
      while(!te.main_task)
        {
          padico_print("ready- waiting tasks...\n");
          marcel_cond_wait(&te.main_cond, &te.main_lock);
        }
      if(te.main_task)
        {
          padico_task_t t = te.main_task;
          (*t->tinfo.mainthread.func)(t->tinfo.mainthread.argc, t->tinfo.mainthread.argv);
          te.main_task = NULL;
          marcel_cond_signal(&te.main_cond);
        }
    }
  marcel_mutex_unlock(&te.main_lock);

}

/* *** PadicoTM reqs *************************************** */

padico_req_t padico_tm_req_new(padico_task_notifier_t notify, void*key)
{
  padico_req_t req = padico_req_new();
  marcel_mutex_lock(&te.req_lock);
  te.next_req_id++;
  if(te.next_req_id <= PADICO_REQ_ID_NONE)
    te.next_req_id = 1;
  req->id         = te.next_req_id;
  req->completed  = 0;
  req->rc         = NULL;
  req->notify     = notify;
  req->notify_key = key;
  padico_req_list_push_back(&te.reqs, req);
  marcel_mutex_unlock(&te.req_lock);
  padico_out(50, "new req id=%d; notify=%p\n", req->id, notify);
  return req;
}

padico_rc_t padico_tm_req_wait(padico_req_t req)
{
  padico_rc_t rc = NULL;
  if(req == NULL)
    return rc;
  padico_tasklet_t tasklet = marcel_getspecific(te.current_tasklet);
  padico_out(50, "waiting req=%d; tasklet = %p (%s)\n", req->id, tasklet, tasklet ? tasklet->name : NULL);
  marcel_mutex_lock(&te.req_lock);
  while(!req->completed)
    {
      if(tasklet != NULL)
        {
          /* ** waiting in a tasklet worker-
           * process pending work if any, else sleep with tasklet released. */
          marcel_mutex_unlock(&te.req_lock);
          marcel_mutex_lock(&tasklet->lock);
          assert(tasklet->owner == marcel_self());
          if(!padico_tasklet_action_list_empty(tasklet->actions))
            {
              /* ** process tasklet action items while waiting */
              padico_out(20, "blocking req wait while owning tasklet %s.\n", tasklet->name);
              /* acquire tasklet ownership (or recurse one level) */
              tasklet->level++;
              assert(tasklet->owner == marcel_self());
              marcel_mutex_unlock(&tasklet->lock);
              /* process the task */
              padico_out(40, "tasklet=*%s*; processing action...\n", tasklet->name);
              padico_tasklet_dispatch(tasklet);
              padico_out(40, "tasklet=*%s*; processing done.\n", tasklet->name);
              /* release tasklet ownership */
              padico_tasklet_release(tasklet);
            }
          else
            {
              /* ** no pending work- release tasklet, sleep, and re-acquire tasklet */
              const int level = tasklet->level;
              padico_out(20, "blocking req in tasklet worker *%s* level=%d; pending=%d\n",
                         tasklet->name, tasklet->level, padico_tasklet_action_list_size(tasklet->actions));
              /* release tasklet */
              tasklet->level = 0;
              tasklet->owner = PADICO_TASKLET_OWNER_NULL;
              marcel_cond_broadcast(&tasklet->list_event);
              marcel_mutex_unlock(&tasklet->lock);
              /* wait for request completion */
              marcel_mutex_lock(&te.req_lock);
              if(!req->completed)
                marcel_cond_wait(&te.req_finished, &te.req_lock);
              marcel_mutex_unlock(&te.req_lock);
              /* re-acquire tasklet at the same level as before */
              marcel_mutex_lock(&tasklet->lock);
              while(tasklet->level > 0)
                {
                  marcel_cond_wait(&tasklet->list_event, &tasklet->lock);
                }
              tasklet->level = level;
              tasklet->owner = marcel_self();
              marcel_mutex_unlock(&tasklet->lock);
            }
          marcel_mutex_lock(&te.req_lock);
        }
      else
        {
          marcel_cond_wait(&te.req_finished, &te.req_lock);
        }
    }
  rc = req->rc;
  padico_req_list_remove(&te.reqs, req);
  padico_out(60, "unlocked req=%d\n", req->id);
  padico_req_delete(req);
  marcel_mutex_unlock(&te.req_lock);
  return rc;
}

static void*tm_req_async_notifier(void*_req)
{
  padico_req_t req = _req;
  (*(req->notify))(req->rc, req->notify_key);
  padico_req_delete(req);
  return NULL;
}

void padico_tm_req_notify(padico_req_id_t req_id, padico_rc_t rc)
{
  padico_req_itor_t ri;
  padico_out(50, "looking up req=%d\n", req_id);
  marcel_mutex_lock(&te.req_lock);
  for(ri  = padico_req_list_begin(&te.reqs);
      ri != padico_req_list_end(&te.reqs);
      ri  = padico_req_list_next(ri))
    {
      if(ri->id == req_id)
        {
          ri->rc = rc;
          ri->completed = 1;
          if(ri->notify)
            {
              padico_req_list_remove(&te.reqs, ri);
              padico_tm_invoke_later(&tm_req_async_notifier, ri);
            }
          else
            {
              marcel_cond_broadcast(&te.req_finished);
            }
          break;
        }
    }
  marcel_mutex_unlock(&te.req_lock);
}

/* *** Tasklet ********************************************* */

/** dispatch the next ready action in the tasklet
 * @pre tasklet lock not held by calling function
 * @pre tasklet owned by thread
 */
static inline void padico_tasklet_dispatch(padico_tasklet_t tasklet)
{
  marcel_mutex_lock(&tasklet->lock);
  assert(tasklet->level > 0);
  assert(tasklet->owner == marcel_self());
#ifdef DEBUG
  const int init_level = tasklet->level;
#endif
  struct padico_tasklet_action_s*a = padico_tasklet_action_list_pop_front(tasklet->actions);
  marcel_mutex_unlock(&tasklet->lock);
  if(a)
    {
      (*a->listener)(a->data);
      marcel_mutex_lock(&tasklet->lock);
      if(a->listener_num)
        {
          (*a->listener_num)--;
          if(!(*a->listener_num))
            {
              padico_free(a->listener_num);
              padico_free(a->data);
            }
        }
      marcel_mutex_unlock(&tasklet->lock);
      free(a->name);
      padico_tasklet_action_delete(a);
    }
#ifdef DEBUG
  marcel_mutex_lock(&tasklet->lock);
  assert(tasklet->owner == marcel_self());
  assert(tasklet->level == init_level);
  marcel_mutex_unlock(&tasklet->lock);
#endif
}

static void*padico_tasklet_worker(void*arg)
{
  struct padico_tasklet_s*tasklet = (padico_tasklet_t)arg;
  padico_tm_thread_givename(tasklet->name);
  tasklet->running = 1;
  marcel_setspecific(te.current_tasklet, tasklet);
  while(tasklet->running)
    {
      /* acquire the tasklet and pop a task */
      padico_out(40, "tasklet=*%s*; worker locking.\n", tasklet->name);
      marcel_mutex_lock(&tasklet->lock);
      assert((tasklet->level == 0) || (tasklet->owner != marcel_self()));
      while(tasklet->running && ((tasklet->level > 0) || padico_tasklet_action_list_empty(tasklet->actions)))
        {
          padico_out(40, "tasklet=*%s*; worker waiting (level=%d; pending=%d).\n",
                     tasklet->name, tasklet->level, padico_tasklet_action_list_size(tasklet->actions));
          if(!tasklet->running)
            {
              marcel_mutex_unlock(&tasklet->lock);
              return NULL;
            }
          marcel_cond_wait(&tasklet->list_event, &tasklet->lock);

          if(!tasklet->running)
            {
              marcel_mutex_unlock(&tasklet->lock);
              return NULL;
            }

        }
      /* acquire tasklet ownership */
      const int l = tasklet->level;
      const marcel_t o = tasklet->owner;
      if(l != 0 || o != (marcel_t)NULL)
        {
          padico_fatal("bad state for tasklet in worker: level = %d; owner = %p\n", l, (void*)o);
        }
      assert(tasklet->level == 0);
      assert(tasklet->owner == PADICO_TASKLET_OWNER_NULL);
      tasklet->level++;
      tasklet->owner = marcel_self();
      marcel_mutex_unlock(&tasklet->lock);

      /* process the task */
      padico_out(40, "tasklet=*%s*; worker processing action...\n", tasklet->name);
      padico_tasklet_dispatch(tasklet);
      padico_out(40, "tasklet=*%s*; processing done.\n", tasklet->name);

      /* release tasklet ownership */
      padico_tasklet_release(tasklet);
    }
  marcel_exit(NULL);
}

padico_tasklet_t padico_tasklet_new(const char*name)
{
  padico_tasklet_t t = padico_malloc(sizeof(struct padico_tasklet_s));
  marcel_mutex_init(&t->lock, NULL);
  marcel_cond_init(&t->list_event, NULL);
  t->actions = padico_tasklet_action_list_new();
  padico_event_listener_vect_init(&t->subscribers);
  t->level   = 0;
  t->running = 0;
  t->name    = padico_strdup(name);
  t->owner   = PADICO_TASKLET_OWNER_NULL;
  marcel_create(&t->worker, NULL, &padico_tasklet_worker, t);
  return t;
}

void padico_tasklet_destroy(padico_tasklet_t t)
{
  marcel_mutex_lock(&t->lock);
  t->running = 0;
  marcel_cond_broadcast(&t->list_event);
  marcel_mutex_unlock(&t->lock);
  padico_tasklet_acquire(t);
  padico_tasklet_flush(t);
  padico_tasklet_release(t);
  /* TODO- clean tasklet destruction code below. Used to break.
   * Should be fixed now. */
  marcel_join(t->worker, NULL);
  padico_tasklet_acquire(t);
  padico_free((void*)t->name);
  t->name = NULL;
  padico_tasklet_action_list_delete(t->actions);
  padico_event_listener_vect_destroy(&t->subscribers);
  padico_free(t);
}

void padico_tasklet_subscribe(padico_tasklet_t t, padico_event_listener_t f)
{
  marcel_mutex_lock(&t->lock);
  /* assume tasklet is disabled to avoid race conditions */
  assert(t->owner == marcel_self());
  padico_event_listener_vect_push_back(&t->subscribers, f);
  marcel_mutex_unlock(&t->lock);
}

void padico_tasklet_unsubscribe(padico_tasklet_t t, padico_event_listener_t f)
{
  marcel_mutex_lock(&t->lock);
  /* assume tasklet is disabled to avoid race conditions */
  assert(t->owner == marcel_self());
  padico_event_listener_vect_itor_t i = padico_event_listener_vect_find(&t->subscribers, f);
  assert(i != NULL);
  padico_event_listener_vect_erase(&t->subscribers, i);
  marcel_mutex_unlock(&t->lock);
}

void padico_tasklet_schedule(padico_tasklet_t t, void*data, const char*name)
{
  marcel_mutex_lock(&t->lock);
  padico_out(40, "tasklet=*%s*; level=%d; owner=%p; self=%p\n", t->name, t->level, (void*)t->owner, (void*)marcel_self());
  assert( (t->level == 0 && t->owner == PADICO_TASKLET_OWNER_NULL) ||
          (t->level > 0 && t->owner != PADICO_TASKLET_OWNER_NULL) );
  padico_event_listener_vect_itor_t i;
  /* Add an action item for every subscriber */
  int*listener_num = NULL;
  if(t != te.tasks)
    {
      listener_num = padico_malloc(sizeof(int));
      *listener_num = padico_event_listener_vect_size(&t->subscribers);
    }
  puk_vect_foreach(i, padico_event_listener, &t->subscribers)
    {
      padico_event_listener_t f = *i;
      struct padico_tasklet_action_s*a = padico_tasklet_action_new();
      a->tasklet  = t;
      a->data     = data;
      a->listener = f;
      a->listener_num = listener_num;
      a->name = padico_strdup(name);
      padico_tasklet_action_list_push_back(t->actions, a);
      padico_out(40, "tasklet=*%s*; pushback task- now pending actions = %d.\n",
                 t->name, padico_tasklet_action_list_size(t->actions));
    }
  assert( (t->level == 0 && t->owner == PADICO_TASKLET_OWNER_NULL) ||
          (t->level > 0 && t->owner != PADICO_TASKLET_OWNER_NULL) );
  marcel_cond_broadcast(&t->list_event);
  marcel_mutex_unlock(&t->lock);
  /* wake up requests, ifever the tasklet worker is sleeping */
  /* TODO: we should be able to know whether it is really sleeping or not. */
  marcel_mutex_lock(&te.req_lock);
  marcel_cond_broadcast(&te.req_finished);
  marcel_mutex_unlock(&te.req_lock);

}

/** acquire tasklet ownership for the current thread */
void padico_tasklet_acquire(padico_tasklet_t t)
{
  marcel_mutex_lock(&t->lock);
  padico_out(40, "tasklet=*%s*; level=%d; owner=%p; self=%p\n", t->name, t->level, (void*)t->owner, (void*)marcel_self());
  assert( (t->level == 0 && t->owner == PADICO_TASKLET_OWNER_NULL) || (t->level > 0 && t->owner != PADICO_TASKLET_OWNER_NULL) );
  while((t->level > 0) && (t->owner != marcel_self()))
    {
      marcel_cond_wait(&t->list_event, &t->lock);
    }
  t->level++;
  t->owner = marcel_self();
  padico_out(40, "tasklet=*%s*; level=%d; owner=%p - done.\n", t->name, t->level, (void*)t->owner);
  marcel_mutex_unlock(&t->lock);
}

/** release tasklet ownership from the current thread */
void padico_tasklet_release(padico_tasklet_t t)
{
  marcel_mutex_lock(&t->lock);
  padico_out(40, "tasklet=*%s*; level=%d\n", t->name, t->level);
  assert((t->level > 0 && t->owner == marcel_self()));
  /* release the tasklet */
  t->level--;
  if(t->level == 0)
    {
      t->owner = PADICO_TASKLET_OWNER_NULL;
      padico_out(40, "tasklet=*%s*; level=%d; owner set to NULL\n", t->name, t->level);
    }
  assert(t->level >= 0);
  marcel_cond_broadcast(&t->list_event);
  padico_out(40, "tasklet=*%s*; level=%d; done.\n", t->name, t->level);
  marcel_mutex_unlock(&t->lock);
  padico_out(40, "exit.\n");
}

void padico_tasklet_flush(padico_tasklet_t t)
{
  padico_out(40, "tasklet=*%s*; entering\n", t->name);
  /* flush the remaining actions */
  while(!padico_tasklet_action_list_empty(t->actions))
    {
      padico_out(20, "tasklet=*%s*; waiting- remains %d actions in list\n",
                 t->name, padico_tasklet_action_list_size(t->actions));
      padico_tasklet_dispatch(t);
    }
  padico_out(40, "tasklet=*%s*; flushing done\n", t->name);
}
