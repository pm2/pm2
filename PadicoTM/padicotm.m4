dnl -*- mode: Autoconf;-*-


dnl -- PadicoTM root path
dnl --
AC_DEFUN([AC_PADICOTM_ROOT],
         [
           AC_REQUIRE([AC_PADICO_PACKAGE_ROOT])
           AC_MSG_CHECKING([for PADICOTM_ROOT])
           padicotm_root=${prefix}
           AC_DEFINE_UNQUOTED(PADICOTM_ROOT, "${padicotm_root}/", [PadicoTM root install directory])
           AC_MSG_RESULT([${padicotm_root}])
         ])

AC_DEFUN([AC_PADICO_OUT_MODULES],
         [ dnl --- Core/
           AC_PADICO_OUT_MK([Core/Makefile Core/PadicoTM.map])
           AC_PADICO_OUT_SH([Core/padico-marcelize])
           dnl --- PadicoBoot/
           AC_PADICO_OUT_MK([PadicoBoot/Makefile])
           AC_PADICO_OUT_SH([PadicoBoot/padico-d])
           AC_PADICO_OUT_SH([PadicoBoot/padico-launch])
           dnl --- Services/
           AC_PADICO_OUT_MK([Services/Makefile])
           services=`cd ${padico_srcdir}/Services; find * -type d|grep -v CVS|tr '\n' ' '`
           for service in ${services}; do
             if test -r ${padico_srcdir}/Services/${service}/Makefile.in; then
               AC_PADICO_OUT_MK([Services/${service}/Makefile])
             fi
             if test -r ${padico_srcdir}/Services/${service}/mod.mk.in; then
               AC_PADICO_OUT_MK([Services/${service}/mod.mk])
             fi
             if test -r ${padico_srcdir}/Services/${service}/${service}.map.in; then
               AC_PADICO_OUT_MK([Services/${service}/${service}.map])
             fi
           done
           AC_PADICO_OUT_SH([Services/MemMonitor/padico-mem-monitor])
           AC_PADICO_OUT_MK([PadicoTM.pc core_vars.mk])
         ])


# ## check for MX availbility
AC_DEFUN([AC_PADICO_MX],
         [ if test "x$MX_DIR" = "x" ; then
             if test -r /opt/mx/include/myriexpress.h ; then
               MX_DIR=/opt/mx
             fi
           fi
           if test "x$MX_DIR" != "x" ; then
             mx_CFLAGS="-I${MX_DIR}/include"
             mx_LIBS="-Wl,-rpath,${MX_DIR}/lib -L${MX_DIR}/lib -lmyriexpress"
           fi
           AC_CHECK_HEADER(myriexpress.h,
                           [ HAVE_MX=yes ],
                           [ HAVE_MX=no  ])
           AC_SUBST(HAVE_MX)
           AC_SUBST(mx_CFLAGS)
           AC_SUBST(mx_LIBS)
         ])

# ## required and optional features
AC_DEFUN([AC_PADICOTM_FEATURES],
         [ AC_REQUIRE([AC_PADICO_COMPILER])

           dnl - required headers

           AC_CHECK_HEADERS( errno.h pthread.h sys/stat.h sys/wait.h,
             [],
             AC_MSG_ERROR([Missing required header file]))


           dnl - system-dependent headers

           AC_CHECK_HEADER(poll.h,
             [ HAVE_POLL_H="yes"
               AC_DEFINE(HAVE_POLL_H, 1, [Define if you have the <poll.h> header file.])],
             [HAVE_POLL_H="no"])

           AC_CHECK_HEADER(ifaddrs.h,
             [ HAVE_IFADDRS_H="yes"
               AC_DEFINE(HAVE_IFADDRS_H, 1, [Define if you have the <ifaddrs.h> header file.])],
             [HAVE_IFADDRS_H="no"])

           AC_CHECK_HEADER(sys/inotify.h,
             [ HAVE_INOTIFY_H="yes"
               AC_DEFINE(HAVE_INOTIFY_H, 1, [Define if you have the <sys/inotify.h> header file.])],
             [HAVE_INOTIFY_H="no"])

           AC_CHECK_HEADER(linux/netlink.h,
             [ HAVE_LINUX_NETLINK_H="yes"
               AC_DEFINE(HAVE_LINUX_NETLINK_H, 1, [Define if you have the <linux/netlink.h> header file.])],
             [HAVE_LINUX_NETLINK_H="no"])

           AC_CHECK_HEADER(linux/rtnetlink.h,
             [ HAVE_LINUX_RTNETLINK_H="yes"
               AC_DEFINE(HAVE_LINUX_RTNETLINK_H, 1, [Define if you have the <linux/rtnetlink.h> header file.])],
             [HAVE_LINUX_RTNETLINK_H="no"])

           AC_CHECK_HEADER(linux/sockios.h,
             [ HAVE_LINUX_SOCKIOS_H="yes"
               AC_DEFINE(HAVE_LINUX_SOCKIOS_H, 1, [Define if you have the <linux/sockios.h> header file.])],
             [HAVE_LINUX_SOCKIOS_H="no"])

           AC_CHECK_HEADER(linux/ethtool.h,
             [ HAVE_LINUX_ETHTOOL_H="yes"
               AC_DEFINE(HAVE_LINUX_ETHTOOL_H, 1, [Define if you have the <linux/ethtool.h> header file.])],
             [HAVE_LINUX_ETHTOOL_H="no"])

           AC_CHECK_HEADER(linux/if.h,
             [ HAVE_LINUX_IF_H="yes"
               AC_DEFINE(HAVE_LINUX_IF_H, 1, [Define if you have the <linux/if.h> header file.])],
             [HAVE_LINUX_IF_H="no"],
             [#include <netinet/in.h>])


           dnl - optional headers
           AC_PADICO_CHECK_CGRAPH

           dnl - system-dependent symbols

           AC_CHECK_FUNCS([ __fxstat ])

             dnl - mandatory tools (mostly for padico-launch)
           AC_CHECK_PROGS(date, [date],
             AC_MSG_ERROR([Missing required tool: date]))
           AC_CHECK_PROGS(cut, [cut],
             AC_MSG_ERROR([Missing required tool: cut]))
           AC_CHECK_PROGS(md5sum, [md5sum],
             AC_MSG_ERROR([Missing required tool: md5sum]))
           AC_CHECK_PROGS(readlink, [readlink],
             AC_MSG_ERROR([Missing required tool: readlink]))
           AC_CHECK_PROGS(tar, [tar],
             AC_MSG_ERROR([Missing required tool: tar]))

         ])
