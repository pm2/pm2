/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"


#if defined(PIOMAN_TOPOLOGY_MARCEL)

/* ** Marcel topology ************************************** */

piom_ltask_queue_t*piom_topo_get_queue(piom_topo_obj_t obj)
{
  if(obj == NULL)
    {
      obj = piom_topo_current_obj();
    }
  const struct piom_ltask_locality_s*local = obj->ltask_data;
  while((obj != NULL) && (local->queue == NULL))
    {
      obj = obj->father;
      local = obj->ltask_data;
    }
  assert(local->queue != NULL);
  return local->queue;
}

piom_topo_obj_t piom_get_parent_obj(piom_topo_obj_t obj, enum piom_topo_level_e _level)
{
  const enum marcel_topo_level_e level = _level;
  marcel_topo_level_t *l = obj; /* &marcel_topo_levels[marcel_topo_nblevels - 1][marcel_current_vp()]; */
  while(l != NULL && l->type > level)
    {
      l = l->father;
    }
  assert(l != NULL);
  return l;
}

void piom_topo_marcel_init(void)
{
  int i, j;
  for(i = 0; i < marcel_topo_nblevels; i++)
    {
      for(j = 0; j < marcel_topo_level_nbitems[i]; j++)
        {
          struct marcel_topo_level *l = &marcel_topo_levels[i][j];
          piom_ltask_queue_t*queue = padico_malloc(sizeof (piom_ltask_queue_t));
          piom_ltask_queue_init(queue, l);
          /*                queue->parent = (l->father != NULL) ? l->father->ltask_data : NULL; */
          l->ltask_data = queue;
        }
    }
}

void piom_topo_marcel_exit(void)
{
  int i, j;
  for(i = marcel_topo_nblevels -1 ; i >= 0; i--)
    {
      for(j = 0; j < marcel_topo_level_nbitems[i]; j++)
        {
          struct marcel_topo_level *l = &marcel_topo_levels[i][j];
          piom_ltask_queue_exit((piom_ltask_queue_t*)l->ltask_data);
          padico_free(l->ltask_data);
        }
    }
}

#endif /* PIOMAN_TOPOLOGY_MARCEL */
