/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2018 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

#if defined(PIOMAN_TOPOLOGY_NONE)

static struct
{
  /** global queue in case topology is unknown */
  struct piom_ltask_queue global_queue;
} piom_topo_none;

piom_ltask_queue_t*piom_topo_get_queue(piom_topo_obj_t obj)
{
  return &piom_topo_none.global_queue;
}

void piom_topo_none_init(void)
{
  piom_ltask_queue_init(&piom_topo_none.global_queue, piom_topo_full);
}

void piom_topo_none_exit(void)
{
  /* nothing to do- task queue is stopped by piom_exit_ltasks() */
}


#endif /* PIOMAN_TOPOLOGY_NONE */
