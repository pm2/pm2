/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"


#ifdef PIOMAN_MARCEL

/** Polling point. May be called from the application to force polling,
 * from marcel hooks, from timer handler.
 * @return 0 if we didn't need to poll and 1 otherwise
 */
static int piom_polling_hook(unsigned _point)
{
  const int point =
    (_point == MARCEL_SCHEDULING_POINT_TIMER) ? PIOM_POLL_POINT_TIMER :
    (_point == MARCEL_SCHEDULING_POINT_IDLE)  ? PIOM_POLL_POINT_IDLE :
    PIOM_POLL_POINT_FORCED;
  piom_ltask_schedule(point);
  return 1;
}

void piom_ltask_marcel_init(void)
{
  /* marcel: register hooks */
  marcel_register_scheduling_hook(piom_polling_hook, MARCEL_SCHEDULING_POINT_TIMER);
  marcel_register_scheduling_hook(piom_polling_hook, MARCEL_SCHEDULING_POINT_YIELD);
  marcel_register_scheduling_hook(piom_polling_hook, MARCEL_SCHEDULING_POINT_IDLE);
  marcel_register_scheduling_hook(piom_polling_hook, MARCEL_SCHEDULING_POINT_LIB_ENTRY);
  marcel_register_scheduling_hook(piom_polling_hook, MARCEL_SCHEDULING_POINT_CTX_SWITCH);
}

void piom_ltask_marcel_exit(void)
{
  /* TODO */
}

int piom_ltask_submit_in_lwp(struct piom_ltask*task)
{
  return -1; /* unsupported by this backend */
}

#endif /* PIOMAN_MARCEL */
