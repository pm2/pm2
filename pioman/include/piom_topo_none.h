/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_TOPO_NONE_H
#define PIOM_TOPO_NONE_H

#ifdef PIOMAN_TOPOLOGY_NONE
#define piom_topo_obj_t          void*
#define piom_topo_obj_none       NULL
#define piom_topo_full           NULL
#define piom_topo_current_obj()  NULL
enum piom_topo_level_e
  {
    PIOM_TOPO_NONE    = -1,
    PIOM_TOPO_MACHINE,
    PIOM_TOPO_NODE,
    PIOM_TOPO_SOCKET,
    PIOM_TOPO_CORE,
    PIOM_TOPO_PU
  };

void piom_topo_none_init(void);
void piom_topo_none_exit(void);

#define piom_topo_init piom_topo_none_init
#define piom_topo_exit piom_topo_none_exit

#endif /* PIOMAN_TOPOLOGY_NONE */

#endif /* PIOM_TOPO_NONE_H */
