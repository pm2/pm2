/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOMAN_H
#define PIOMAN_H

/** @defgroup pioman PIOMan API
 * @{
 */

#include <piom_config.h>

#if defined(PIOMAN_SIMGRID)
#  include <simgrid/actor.h>
#  include <simgrid/mutex.h>
#  include <simgrid/cond.h>
#  include <simgrid/semaphore.h>
#  define PIOMAN_MULTITHREAD
#  define PIOMAN_TOPOLOGY_NONE 1
#elif defined(PIOMAN_PTHREAD)
#  include <pthread.h>
#  include <semaphore.h>
#  include <sched.h>
#  define PIOMAN_MULTITHREAD
#  ifndef PIOMAN_HAVE_HWLOC
#    define PIOMAN_TOPOLOGY_NONE 1
#  else
#    include <hwloc.h>
#    define PIOMAN_TOPOLOGY_HWLOC 1
#  endif
#  ifdef MCKERNEL
#    define PIOMAN_SEM_COND 1
#  endif /* MCKERNEL */
// #define PIOMAN_SEM_COND 1
#  define PIOMAN_PTHREAD_SPINLOCK
//#  define PIOMAN_MUTEX_SPINLOCK
//#  define PIOMAN_MINI_SPINLOCK
#elif defined(PIOMAN_MARCEL)
#  include <marcel.h>
#  define PIOMAN_MULTITHREAD
#  ifndef MA__NUMA
#    define PIOMAN_TOPOLOGY_NONE 1
#  else
#    define PIOMAN_TOPOLOGY_MARCEL
#  endif
#elif defined(PIOMAN_ABT)
#  include <abt.h>
#  define PIOMAN_TOPOLOGY_NONE 1
#  define PIOMAN_MULTITHREAD
#elif defined(PIOMAN)
#  undef  PIOMAN_MULTITHREAD
#  define PIOMAN_NOTHREAD
#  define PIOMAN_TOPOLOGY_NONE 1
#else
#  error "PIOMan: PIOMAN flags are not defined."
#endif

extern void pioman_init(void);
extern void pioman_exit(void);


/** @defgroup polling_points Polling constants. Define the polling points.
 * @{
 */
/** poll on timer */
#define PIOM_POLL_POINT_TIMER  0x01
/** poll on explicit yield */
#define PIOM_POLL_POINT_YIELD  0x02
/** poll when cpu is idle */
#define PIOM_POLL_POINT_IDLE   0x08
/** poll when explicitely asked for by the application */
#define PIOM_POLL_POINT_FORCED 0x10
/** poll in a busy wait */
#define PIOM_POLL_POINT_BUSY   0x20
/** poll in a hook */
#define PIOM_POLL_POINT_HOOK   0x40
/** poll one local task */
#define PIOM_POLL_POINT_SINGLE 0x80
/** @} */

/** Polling point forced from the application.
 */
extern void piom_polling_force(void);

/** Schedule tasks from local or all queues (depending on 'point')
 * @internal function exported for inlining
 */
extern void piom_ltask_schedule(int point);

/** Set level and logical ids for bound polling threads from the application
 */
extern int piom_ltask_set_bound_thread_indexes(int level, int indexes[], int size);

/** Set level and physical ids for bound polling threads from the application
 */
extern int piom_ltask_set_bound_thread_os_indexes(int level, int indexes[], int size);

/** Bind current thread to a dedicated core
 *  @param core_id the logical id of the core where to bind the thread
 */
extern int piom_bind_current_thread_to_core(int core_id);

#include "piom_log.h"
#include "piom_lock.h"
#include "piom_ltask.h"

/** @} */

#endif /* PIOMAN_H */
