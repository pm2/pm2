/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_TOPO_HWLOC_H
#define PIOM_TOPO_HWLOC_H

#if defined(PIOMAN_TOPOLOGY_HWLOC)
#include <hwloc.h>
#define piom_topo_obj_t    hwloc_obj_t
#define piom_topo_obj_none NULL
#define piom_topo_full     hwloc_get_root_obj(piom_topo_hwloc_get())
enum piom_topo_level_e
  {
    PIOM_TOPO_NONE    = -1,
    PIOM_TOPO_MACHINE = HWLOC_OBJ_MACHINE,
    PIOM_TOPO_NODE    = HWLOC_OBJ_NODE,
    PIOM_TOPO_SOCKET  = HWLOC_OBJ_SOCKET,
    PIOM_TOPO_CORE    = HWLOC_OBJ_CORE,
    PIOM_TOPO_PU      = HWLOC_OBJ_PU
  };
extern hwloc_topology_t piom_topo_hwloc_get(void);
extern piom_topo_obj_t piom_topo_current_obj(void);

void piom_topo_hwloc_init(void);
void piom_topo_hwloc_exit(void);

#define piom_topo_init piom_topo_hwloc_init
#define piom_topo_exit piom_topo_hwloc_exit

#endif /* PIOMAN_TOPOLOGY_HWLOC*/


#endif /* PIOM_TOPO_HWLOC_H */
