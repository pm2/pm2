/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_LOG_H
#define PIOM_LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <Padico/Puk.h>

/** display message in verbose mode, not in quiet mode */
#define PIOM_DISP(str, ...)  padico_out(puk_verbose_info, str, ## __VA_ARGS__)

/** warning message, always displayed */
#define PIOM_WARN(str, ...)  padico_warning(str , ## __VA_ARGS__)

/** fatal error */
#define PIOM_FATAL(str, ...) padico_fatal(str, ## __VA_ARGS__)


#endif /* PIOM_LOG_H */
