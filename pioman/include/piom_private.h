/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2012-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file piom_private.h internal PIOMan definitions.
 * Do not include in applications.
 */

#ifndef PIOM_PRIVATE_H
#define PIOM_PRIVATE_H

#include <Padico/Puk.h>
#include "pioman.h"
#include "piom_log.h"
#include "piom_trace.h"

#ifdef PIOMAN_X86INTRIN
#include <x86intrin.h>
#endif /* PIOMAN_X86INTRIN */

#if defined(PIOMAN_SIMGRID)
#  include <piom_ltask_simgrid.h>
#  define piom_ltask_backend_init piom_ltask_simgrid_init
#  define piom_ltask_backend_exit piom_ltask_simgrid_exit
#elif defined(PIOMAN_PTHREAD)
#  include <piom_ltask_pthread.h>
#  define piom_ltask_backend_init piom_ltask_pthread_init
#  define piom_ltask_backend_exit piom_ltask_pthread_exit
#elif defined(PIOMAN_MARCEL)
#  include <piom_ltask_marcel.h>
#  define piom_ltask_backend_init piom_ltask_marcel_init
#  define piom_ltask_backend_exit piom_ltask_marcel_exit
#elif defined(PIOMAN_ABT)
#  include <piom_ltask_abt.h>
#  define piom_ltask_backend_init piom_ltask_abt_init
#  define piom_ltask_backend_exit piom_ltask_abt_exit
#elif defined(PIOMAN_NOTHREAD)
#  include <piom_ltask_nothread.h>
#  define piom_ltask_backend_init piom_ltask_nothread_init
#  define piom_ltask_backend_exit piom_ltask_nothread_exit
#endif

/** Maximum number of ltasks in a queue.
 * Should be large enough to avoid overflow, but small enough to fit the cache.
 */
#define PIOM_MAX_LTASK 1024

/** type for lock-free queue of ltasks */
PUK_LFQUEUE_TYPE(piom_ltask, struct piom_ltask*, NULL, PIOM_MAX_LTASK);

PUK_DLFQ_TYPE(piom_ltask, struct piom_ltask*, NULL);

/** state for a queue of tasks */
typedef enum
  {
    PIOM_LTASK_QUEUE_STATE_NONE     = 0x00, /**< not initialized yet */
    PIOM_LTASK_QUEUE_STATE_RUNNING  = 0x01, /**< running */
    PIOM_LTASK_QUEUE_STATE_STOPPING = 0x02, /**< stop requested (not stopped yet) */
    PIOM_LTASK_QUEUE_STATE_STOPPED  = 0x03  /**< stopped- empty, not scheduling, not accepting requests anymore */
  } piom_ltask_queue_state_t;

/** an ltask queue- one queue instanciated per hwloc object */
typedef struct piom_ltask_queue
{
  /** the queue of tasks ready to be scheduled */
  struct piom_ltask_dlfq_s          ltask_queue;
  /** separate queue for task submission, to reduce contention */
  struct piom_ltask_lfqueue_s       submit_queue;
  /** whether scheduling for the queue is masked */
  piom_spinlock_t                   mask;
  /** state to control queue lifecycle */
  volatile piom_ltask_queue_state_t state;
  /** where this queue is located */
  piom_topo_obj_t                   binding;
} piom_ltask_queue_t;

/* not a valid queue, but not NULL either */
#define PIOM_LTASK_QUEUE_INVALID  ((struct piom_ltask_queue*)-1)

/** per thread state, to check whether we are called from an ltask context */
struct piom_ltask_threadstate_s
{
  struct piom_ltask_queue*scheduling_queue; /**< queue currently scheduling task for this thread */
};

/** initialize ltask system */
__PUK_SYM_INTERNAL void piom_ltasks_init(void);

/** destroy internal structures, stop task execution, etc. */
__PUK_SYM_INTERNAL void piom_ltasks_exit(void);

__PUK_SYM_INTERNAL void piom_io_task_init(void);
__PUK_SYM_INTERNAL void piom_io_task_stop(void);

__PUK_SYM_INTERNAL void piom_topo_init(void);
__PUK_SYM_INTERNAL void piom_topo_exit(void);
/** Get the queue that matches a topology object
 */
__PUK_SYM_INTERNAL piom_ltask_queue_t*piom_topo_get_queue(piom_topo_obj_t obj);

__PUK_SYM_INTERNAL void piom_ltask_queue_init(piom_ltask_queue_t*queue, piom_topo_obj_t binding);
__PUK_SYM_INTERNAL void piom_ltask_queue_exit(piom_ltask_queue_t*queue);
__PUK_SYM_INTERNAL int piom_ltask_submit_in_lwp(struct piom_ltask*task);
__PUK_SYM_INTERNAL void piom_ltask_blocking_invoke(struct piom_ltask*ltask);

static inline void piom_ltask_preinvoke(struct piom_ltask_queue*queue)
{
  struct piom_ltask_threadstate_s*p_threadstate = piom_ltask_getthreadstate();
  assert(p_threadstate->scheduling_queue == NULL);
  p_threadstate->scheduling_queue = queue;
}

static inline void piom_ltask_postinvoke(void)
{
  struct piom_ltask_threadstate_s*p_threadstate = piom_ltask_getthreadstate();
  assert(p_threadstate != NULL);
  assert(p_threadstate->scheduling_queue != NULL);
  p_threadstate->scheduling_queue = NULL;
}


/* todo: get a dynamic value here !
 * it could be based on:
 * - application hints
 * - the history of previous request
 * - compiler hints
 */

/** pioman internal parameters, tuned through env variables */
extern struct piom_parameters_s
{
  int busy_wait_usec;        /**< time to do a busy wait before blocking, in usec; default: 5 */
  int busy_wait_granularity; /**< number of busy wait loops between timestamps to amortize clock_gettime() */
  int enable_progression;    /**< whether to enable background progression (idle thread and sighandler); default 1 */
  enum piom_topo_level_e binding_level; /**< hierarchy level where to bind queues; default: socket */
  int idle_granularity;      /**< time granularity for polling on idle, in usec. */
  int idle_coef;             /**< multiplicative coefficient applied to idle granularity when no requests are active */
  enum piom_bind_distrib_e
    {
      PIOM_BIND_DISTRIB_NONE = 0, /**< no value given */
      PIOM_BIND_DISTRIB_ALL,      /**< bind on all entities in level (default) */
      PIOM_BIND_DISTRIB_ODD,      /**< bind on odd-numbered entities */
      PIOM_BIND_DISTRIB_EVEN,     /**< bind on even-numbered entities */
      PIOM_BIND_DISTRIB_FIRST,    /**< bind on single (first) entity in level*/
      PIOM_BIND_DISTRIB_LAST      /**< bind on single (last) entity in level*/
    } idle_distrib;     /**< binding distribution for idle threads */
  int timer_period;       /**< period for timer-based polling (in usec); default: 4000 */
  int dedicated;          /**< whether we should use dedicated core for polling; default: 0 */
  enum piom_bind_distrib_e dedicated_distrib; /**< binding distribution for bound threads */
  int dedicated_level;    /**< hierarchy level where to bind polling threads */
  int dedicated_wait;     /**< if dedicated=1, whether we should wait bind infos from an external application; default: 0 */
  int spare_lwp;          /**< number of spare LWPs for blocking calls; default: 0 */
  int mckernel;           /**< whether we are running on mckernel */
} piom_parameters;


#endif /* PIOM_PRIVATE_H */
