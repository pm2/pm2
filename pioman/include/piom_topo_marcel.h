/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2018 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_TOPO_MARCEL_H
#define PIOM_TOPO_MARCEL_H

#ifdef PIOMAN_TOPOLOGY_MARCEL
#define piom_topo_obj_t   struct marcel_topo_level*
#define piom_topo_full    marcel_topo_levels[0]
#define piom_topo_current_obj()  &marcel_topo_levels[marcel_topo_nblevels - 1][marcel_current_vp()]
enum piom_topo_level_e
  {
    PIOM_TOPO_NONE    = -1,
    PIOM_TOPO_MACHINE = MARCEL_LEVEL_MACHINE,
    PIOM_TOPO_NODE    = MARCEL_LEVEL_NODE,
    PIOM_TOPO_SOCKET  = MARCEL_LEVEL_DIE,
    PIOM_TOPO_CORE    = MARCEL_LEVEL_CORE,
    PIOM_TOPO_PU      = MARCEL_LEVEL_PROC
  };

struct piom_ltask_queue*piom_topo_marcel_get_queue(piom_topo_obj_t obj);

piom_topo_obj_t piom_get_parent_obj(piom_topo_obj_t obj, enum piom_topo_level_e _level);

void piom_topo_marcel_init(void);
void piom_topo_marcel_exit(void);

#define piom_topo_init piom_topo_marcel_init
#define piom_topo_exit piom_topo_marcel_exit

#endif /* PIOM_TOPOLOGY_MARCEL */

#endif /* PIOM_TOPO_MARCEL_H */

