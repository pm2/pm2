;; unified emacs style for pioman

((c-mode . ((c-file-style . "gnu")
	    (c-basic-offset . 2)
	    (tab-width . 8)
	    (indent-tabs-mode . nil))))
