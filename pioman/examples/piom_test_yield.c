/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sched.h>
#ifdef __x86_64__
#include <cpuid.h>
#include <x86intrin.h>
#endif

#include <Padico/Puk.h>

#define DEFAULT_LOOPS 10000

#define MAX_SLEEP 500

static unsigned loops = DEFAULT_LOOPS;

static inline int supports_invariant_tsc(void)
{
#ifdef __x86_64__
  unsigned a, b, c, d;
  if (__get_cpuid(0x80000007, &a, &b, &c, &d))
    return (d&(1<<8) ? 1 : 0);
 else
   return 0;
#else
  return 0;
#endif
}

static inline int supports_getcpu(void)
{
  int rc = sched_getcpu();
  return (rc != -1);
}

static inline int supports_rdtscp(void)
{
#ifdef __x86_64__
  unsigned a, b, c, d;
  if (__get_cpuid(0x80000001, &a, &b, &c, &d))
    return (d&(1<<27) ? 1 : 0);
 else
   return 0;
#else
  return 0;
#endif
}

static inline uint64_t rdtscp(uint32_t*aux)
{
  /*
    uint64_t rax,rdx;
    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx), "=c" (*aux) : : );
    return (rdx << 32) + rax;
  */
#ifdef __x86_64__
  return __rdtscp(aux);
#else
  abort();
#endif
}

int main(int argc, char**argv)
{
  fprintf(stderr, "support invarient tsc: %d\n", supports_invariant_tsc());
  fprintf(stderr, "support rdtscp: %d\n", supports_rdtscp());
  if(supports_rdtscp())
    {
      uint32_t aux = -1;
      rdtscp(&aux);
      fprintf(stderr, "  rdtscp aux = %d\n", aux);
    }
  fprintf(stderr, "support getcpu: %d\n", supports_getcpu());
  if(supports_getcpu())
    {
      int rc = sched_getcpu();
      fprintf(stderr, "  sched_getcpu = %d\n", rc);
    }

  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  int i;
  for(i = 0 ; i < loops ; i++)
    {
      sched_yield();
    }
  PUK_GET_TICK(t2);
  double t = PUK_TIMING_DELAY(t1, t2);
  printf("# sched_yield = %f usec.\n", t/(double)loops);

  if(supports_getcpu())
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          sched_getcpu();
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# sched_getcpu = %f usec.\n", t/(double)loops);
    }

  if(supports_rdtscp())
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          uint32_t a = -1;
          rdtscp(&a);
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# rdtscp = %f usec.\n", t/(double)loops);
    }

  int s = 1;
  for(s = 1; s < MAX_SLEEP ; s *= 2)
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          puk_usleep(s);
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# usleep(%d) = %f usec.\n", s, t/(double)loops);
    }

  for(s = 1; s < MAX_SLEEP ; s *= 2)
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          struct timespec ts = { .tv_sec = 0, .tv_nsec = 1000 * s };
          puk_nanosleep(&ts, NULL);
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# nanosleep(%d) = %f usec.\n", s, t/(double)loops);
    }

  for(s = 1; s < MAX_SLEEP ; s *= 2)
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          struct timespec ts = { .tv_sec = 0, .tv_nsec = 1000 * s };
          clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, NULL);
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# clock_nanosleep(%d) = %f usec.\n", s, t/(double)loops);
    }

  for(s = 1; s < MAX_SLEEP ; s *= 2)
    {
      PUK_GET_TICK(t1);
      for(i = 0 ; i < loops ; i++)
        {
          struct timeval tv = { .tv_sec = 0, .tv_usec = s };
          select(0, NULL, NULL, NULL, &tv);
        }
      PUK_GET_TICK(t2);
      t = PUK_TIMING_DELAY(t1, t2);
      printf("# select(%d) = %f usec.\n", s, t/(double)loops);
    }

  return 0;
}
