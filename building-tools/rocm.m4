dnl -*- mode: Autoconf;-*-

AC_DEFUN([AC_PADICO_HIP],
  [
    if test "x${HIP_PATH}" = "x"; then
      HIP_PATH=/opt/rocm/hip
    fi
    AC_ARG_WITH([hip],
      AS_HELP_STRING([--with-hip@<:@=yes|no|DIR@:>@], [prefix where HIP is installed (default=no)]),
      [
        if test "$withval" = "no"; then
          want_hip="no"
        elif test "$withval" = "yes"; then
          want_hip="yes"
        else
          want_hip="yes"
          HIP_PATH=${withval}
        fi
      ],
      [
        want_hip="no" # "check"
      ])

    have_hipconfig=no
    if test "${want_hip}" != "no"; then
      if test -n "${HIP_PATH}"; then
        hipconfig_search_dirs="${PATH}${PATH_SEPARATOR}${HIP_PATH}/bin"
      else
        hipconfig_search_dirs=$PATH
      fi

      AC_PATH_PROG([HIPCONFIG], [hipconfig], [], [${hipconfig_search_dirs}])
      if test -n "${HIPCONFIG}"; then
        have_hipconfig=yes
      fi
    fi

    if test "x${have_hipconfig}" = "xyes"; then
      AC_MSG_CHECKING([for HIP version])
      HIP_VERSION=`${HIPCONFIG} --version`
      AC_MSG_RESULT([${HIP_VERSION}])

      AC_MSG_CHECKING([for HIP path])
      hip_path=`${HIPCONFIG} -p`
      AC_MSG_RESULT(${hip_path})

      # TODO- we assume here that platform=amd

      HIP_CFLAGS="`${HIPCONFIG} --cpp_config`"
      HIP_LIBS="-L${hip_path}/lib -lamdhip64"

      saved_CPPFLAGS="${CPPFLAGS}"
      saved_LIBS="${LIBS}"

      CPPFLAGS="${CPPFLAGS} ${HIP_CFLAGS}"
      LIBS="${LIBS} ${CUDA_LIBS}"

      AC_LANG_PUSH(C)
      AC_MSG_CHECKING([for HIP headers])
      AC_COMPILE_IFELSE(
      [
        AC_LANG_PROGRAM([@%:@include <hip/hip_runtime.h>], [])
      ],
      [
        have_hip_headers="yes"
        AC_MSG_RESULT([yes])
      ],
      [
        have_hip_headers="no"
        AC_MSG_RESULT([not found])
      ])
      AC_LANG_POP(C)

      CPPFLAGS="${saved_CPPFLAGS}"
      LIBS="${saved_LIBS}"
    fi

    if test "${want_hip}" != "no" -a "${have_hip_headers}" = "yes" -a "${have_hipconfig}" = "yes"; then
      have_hip="yes"
      AC_DEFINE(HAVE_HIP, 1, [define if HIP is available])
    else
      have_hip="no"
      if test "${want_hip}" = "yes"; then
        AC_MSG_ERROR([HIP is requested but not available])
      fi
    fi

    HAVE_HIP=${have_hip}
    AC_SUBST([HAVE_HIP])

    AC_SUBST(HIP_CFLAGS)
    AC_SUBST(HIP_LIBS)
    AC_SUBST(HIPCONFIG)

])
