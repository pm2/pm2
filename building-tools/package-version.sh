#! /bin/sh

# $1: building-tools directory to consider
# $2: srcdir
p="$1"
srcdir="$2"

if [ -r "${srcdir}/VERSION" ]; then
    VERSION="`cat ${srcdir}/VERSION`"
elif [ -r "${p}/VERSION" ]; then
    VERSION="`cat ${p}/VERSION`"
else
    exit 1
fi

rev=""
if [ "x${srcdir}" != "x" ]; then
    cd "${srcdir}"
    git rev-parse HEAD > /dev/null 2>&1
    rc=$?
    if [ ${rc} = 0 ]; then
	rev="+`git rev-parse HEAD`"
    fi
fi

spack=""
if [ "x${SPACK_SHORT_SPEC}" != "x" ]; then
    spack="+spack:`echo ${SPACK_SHORT_SPEC} | tr ' ' ';'`"
fi

# strip newline and trailing space for m4 parsing
echo "${VERSION}${rev}${spack}" | tr '\n' ' ' | sed 's/ //g'
