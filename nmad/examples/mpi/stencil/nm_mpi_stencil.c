/*
 * NewMadeleine
 * Copyright (C) 2006-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

//#define STENCIL_DISPLAY 1

static int rank, size;

typedef double stencil_data_t;
#define STENCIL_DATA_TYPE MPI_DOUBLE

static int full_matrix_size_x = 2000;
static int full_matrix_size_y = 2000;
static stencil_data_t*full_matrix = NULL;

static int n_domains_x = 2;
static int n_domains_y = 2;

static const int halo_size = 1;

static int local_domain_size_x = -1;
static int local_domain_size_y = -1;
static int local_domain_buffer = 0; /* current buffer for read */
static stencil_data_t*local_domain[2] = { NULL };

typedef enum stencil_direction_e { STENCIL_DIRECTION_N = 0,
                                   STENCIL_DIRECTION_S,
                                   STENCIL_DIRECTION_E,
                                   STENCIL_DIRECTION_W,
                                   STENCIL_DIRECTION_NO,
                                   STENCIL_DIRECTION_NE,
                                   STENCIL_DIRECTION_SE,
                                   STENCIL_DIRECTION_SW,
                                   _STENCIL_DIRECTION_MAX } stencil_direction_t;

static const int tag_init_scatter  = 0x01;
static const int tag_data_gather   = 0x02;
static const int tag_halo_exchange = 0x04;

static inline stencil_data_t*full_matrix_at(int x, int y)
{
  assert(x >= 0);
  assert(y >= 0);
  assert(x < full_matrix_size_x);
  assert(y < full_matrix_size_y);
  return &full_matrix[x + y * full_matrix_size_x];
}

static inline const stencil_data_t*local_domain_read_at(int x, int y)
{
  assert(x >= -halo_size);
  assert(y >= -halo_size);
  assert(x < local_domain_size_x + halo_size);
  assert(y < local_domain_size_y + halo_size);
  return &local_domain[local_domain_buffer][x + halo_size + (y + halo_size) * (local_domain_size_x + 2 * halo_size)];
}

static inline stencil_data_t*local_domain_write_at(int x, int y)
{
  assert(x >= -halo_size);
  assert(y >= -halo_size);
  assert(x < local_domain_size_x + halo_size);
  assert(y < local_domain_size_y + halo_size);
  return &local_domain[(local_domain_buffer + 1) % 2][x + halo_size + (y + halo_size) * (local_domain_size_x + 2 * halo_size)];
}

static inline void local_domain_switch(void)
{
  local_domain_buffer = (local_domain_buffer + 1) % 2;
  /* reset write buffer */
  int x, y;
  for(y = -halo_size; y < local_domain_size_y + halo_size; y++)
    {
      for(x = -halo_size; x < local_domain_size_x + halo_size; x++)
        {
          *local_domain_write_at(x, y) = -5.0;
        }
    }
}

static inline void rank2pos(int r, int*x, int*y)
{
  assert(r >= 0);
  assert(r < size);
  *x = r % n_domains_x;
  *y = r / n_domains_x;
}

static inline int pos2rank(int x, int y)
{
  assert(x >= 0);
  assert(y >= 0);
  assert(x < n_domains_x);
  assert(y < n_domains_y);
  return y * n_domains_x + x;
}

static inline stencil_data_t*domain_in_full_matrix(int dx, int dy)
{
  assert(dx >= 0);
  assert(dy >= 0);
  assert(dx < n_domains_x);
  assert(dy < n_domains_y);
  return &full_matrix[local_domain_size_x * dx + full_matrix_size_x * dy * local_domain_size_y];
}

static void init_full_matrix(void)
{
  int i;
  for(i = 0; i < full_matrix_size_x * full_matrix_size_y; i++)
    {
      full_matrix[i] = i; /* rand(); */
    }
}

static void full_matrix_scatter(void)
{
  MPI_Datatype full_domain_type;
  MPI_Type_vector(local_domain_size_y, local_domain_size_x, full_matrix_size_x, STENCIL_DATA_TYPE, &full_domain_type);
  MPI_Type_commit(&full_domain_type);
  MPI_Request sreqs[size];
  if(rank == 0)
    {
      int i;
      for(i = 0; i < size; i++)
        {
          int x = -1, y = -1;
          rank2pos(i, &x, &y);
          fprintf(stderr, "# send to rank = %d; pos = (%d, %d).\n", i, x, y);
          MPI_Isend(domain_in_full_matrix(x, y), 1, full_domain_type, i, tag_init_scatter, MPI_COMM_WORLD, &sreqs[i]);
        }
    }
  MPI_Datatype local_domain_type;
  MPI_Type_vector(local_domain_size_y, local_domain_size_x, local_domain_size_x + 2 * halo_size, STENCIL_DATA_TYPE, &local_domain_type);
  MPI_Type_commit(&local_domain_type);
  MPI_Recv((void*)local_domain_read_at(0, 0), 1, local_domain_type, 0, tag_init_scatter, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /* really write in the 'read' zone we are currently initializing */
  if(rank == 0)
    {
      MPI_Waitall(size, sreqs, MPI_STATUSES_IGNORE);
    }
}

static void full_matrix_gather(void)
{
  MPI_Datatype full_domain_type;
  MPI_Type_vector(local_domain_size_y, local_domain_size_x, full_matrix_size_x, STENCIL_DATA_TYPE, &full_domain_type);
  MPI_Type_commit(&full_domain_type);
  MPI_Request rreqs[size];
  if(rank == 0)
    {
      int i;
      for(i = 0; i < size; i++)
        {
          int x = -1, y = -1;
          rank2pos(i, &x, &y);
          fprintf(stderr, "# send to rank = %d; pos = (%d, %d).\n", i, x, y);
          MPI_Irecv(domain_in_full_matrix(x, y), 1, full_domain_type, i, tag_data_gather, MPI_COMM_WORLD, &rreqs[i]);
        }
    }
  MPI_Datatype local_domain_type;
  MPI_Type_vector(local_domain_size_y, local_domain_size_x, local_domain_size_x + 2 * halo_size, STENCIL_DATA_TYPE, &local_domain_type);
  MPI_Type_commit(&local_domain_type);
  MPI_Send((void*)local_domain_read_at(0, 0), 1, local_domain_type, 0, tag_data_gather, MPI_COMM_WORLD);
  if(rank == 0)
    {
      MPI_Waitall(size, rreqs, MPI_STATUSES_IGNORE);
    }
}

static void full_matrix_display(void)
{
  if(rank == 0)
    {
      printf("# ####################################################\n");
      printf("# ## full matrix:\n");
      int x, y;
      for(y = 0; y < full_matrix_size_y; y++)
        {
          for(x = 0; x < full_matrix_size_x; x++)
            {
              printf("%5.2f \t", *full_matrix_at(x, y));
            }
          printf("\n");
        }
      printf("# ####################################################\n");
    }
}

static void local_domain_display(void)
{
  printf("# %d # local domain:\n", rank);
  int x, y;
  for(y = -halo_size; y < local_domain_size_y + halo_size; y++)
    {
      for(x = -halo_size; x < local_domain_size_x + halo_size; x++)
        {
          printf("%5.2f \t", *local_domain_read_at(x, y));
        }
      printf("\n");
    }
}

static void local_domain_all_display(void)
{
  MPI_Barrier(MPI_COMM_WORLD);
  int i;
  for(i = 0; i < size; i++)
    {
      MPI_Barrier(MPI_COMM_WORLD);
      if(i == rank)
        {
          local_domain_display();
        }
      fflush(stdout);
    }
  MPI_Barrier(MPI_COMM_WORLD);
}

static void local_domain_compute_step(void)
{
  int x, y;
  for(x = 0; x < local_domain_size_x; x++)
    {
      for(y = 0; y < local_domain_size_y; y++)
        {
          const double v = (*local_domain_read_at(x, y) +
                            *local_domain_read_at(x - 1, y) +
                            *local_domain_read_at(x + 1, y) +
                            *local_domain_read_at(x, y - 1) +
                            *local_domain_read_at(x, y + 1)) / 5.0;
          *local_domain_write_at(x, y) = v;
        }
    }
}

static void local_domain_halo_exchange(enum stencil_direction_e d, MPI_Request*sreqs, MPI_Request*rreqs)
{
  MPI_Request*sreq = &sreqs[d];
  MPI_Request*rreq = &rreqs[d];
  int dest;
  MPI_Datatype*type;
  stencil_data_t*rptr;
  const stencil_data_t*sptr;
  static MPI_Datatype horiz_type = MPI_DATATYPE_NULL, vert_type = MPI_DATATYPE_NULL;
  int x = -1, y = -1;
  rank2pos(rank, &x, &y);
  if(horiz_type == MPI_DATATYPE_NULL)
    {
      MPI_Type_contiguous(local_domain_size_x, STENCIL_DATA_TYPE, &horiz_type);
      MPI_Type_commit(&horiz_type);
    }
  if(vert_type == MPI_DATATYPE_NULL)
    {
      MPI_Type_vector(local_domain_size_y, 1, 2 * halo_size + local_domain_size_x, STENCIL_DATA_TYPE, &vert_type);
      MPI_Type_commit(&vert_type);
    }
  switch(d)
    {
    case STENCIL_DIRECTION_N:
      rptr = local_domain_write_at(0, -halo_size);
      sptr = local_domain_read_at(0, 0);
      type = &horiz_type;
      dest = pos2rank(x, y - 1);
      break;
    case STENCIL_DIRECTION_S:
      rptr = local_domain_write_at(0, local_domain_size_y);
      sptr = local_domain_read_at(0, local_domain_size_y - 1);
      type = &horiz_type;
      dest = pos2rank(x, y + 1);
      break;
    case STENCIL_DIRECTION_E:
      rptr = local_domain_write_at(local_domain_size_x, 0);
      sptr = local_domain_read_at(local_domain_size_x - 1, 0);
      type = &vert_type;
      dest = pos2rank(x + 1, y);
      break;
    case STENCIL_DIRECTION_W:
      rptr = local_domain_write_at(-halo_size, 0);
      sptr = local_domain_read_at(0, 0);
      type = &vert_type;
      dest = pos2rank(x - 1, y);
      break;
    default:
      abort();
    }
  MPI_Irecv(rptr, 1, *type, dest, tag_halo_exchange, MPI_COMM_WORLD, rreq);
  MPI_Isend(sptr, 1, *type, dest, tag_halo_exchange, MPI_COMM_WORLD, sreq);
}
                                        
static void local_domain_halo_exchange_all(void)
{
  MPI_Request sreqs[_STENCIL_DIRECTION_MAX] = { MPI_REQUEST_NULL };
  MPI_Request rreqs[_STENCIL_DIRECTION_MAX] = { MPI_REQUEST_NULL };
  int x = -1, y = -1;
  rank2pos(rank, &x, &y);
  if(y > 0)
    {
      local_domain_halo_exchange(STENCIL_DIRECTION_N, sreqs, rreqs);
    }
  if(y < n_domains_y - 1)
    {
      local_domain_halo_exchange(STENCIL_DIRECTION_S, sreqs, rreqs);
    }
  if(x > 0)
    {
      local_domain_halo_exchange(STENCIL_DIRECTION_W, sreqs, rreqs);
    }
  if(x < n_domains_x - 1)
    {
      local_domain_halo_exchange(STENCIL_DIRECTION_E, sreqs, rreqs);
    }
  
  MPI_Waitall(_STENCIL_DIRECTION_MAX, sreqs, MPI_STATUSES_IGNORE);
  MPI_Waitall(_STENCIL_DIRECTION_MAX, rreqs, MPI_STATUSES_IGNORE);
}

int main(int argc, char**argv)
{
  const int request = MPI_THREAD_SINGLE; /* MPI_THREAD_MULTIPLE */
  int level = request;
  MPI_Init_thread(&argc, &argv, request, &level);
  assert(level >= request);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  fprintf(stderr, "# nmad stencil- rank = %d; size = %d\n", rank, size);

  /* ** compute domain size */
  if(n_domains_x * n_domains_y != size)
    {
      fprintf(stderr, "# wrong number of node- expected %d (%d x %d).\n",
              n_domains_x * n_domains_y, n_domains_x, n_domains_y);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
  local_domain_size_x = full_matrix_size_x / n_domains_x;
  local_domain_size_y = full_matrix_size_y / n_domains_y;
  if((local_domain_size_x * n_domains_x != full_matrix_size_x) ||
     (local_domain_size_y * n_domains_y != full_matrix_size_y))
    {
      fprintf(stderr, "# matrix size is not divisible by domain size.\n");
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
  
  /* ** allocate full matrix on node 0 */
  if(rank == 0)
    {
      fprintf(stderr, "# allocate matrix; size = %d x %d (%g MB).\n", full_matrix_size_x, full_matrix_size_y,
              (double)sizeof(stencil_data_t) * full_matrix_size_x * full_matrix_size_y / (1024.0 * 1024.0));
      full_matrix = malloc(sizeof(stencil_data_t) * full_matrix_size_x * full_matrix_size_y);
      init_full_matrix();
    }
  /* ** allocate local domain, including halo */
  int pos_x, pos_y;
  rank2pos(rank, &pos_x, &pos_y);
  fprintf(stderr, "# local domain size = %d x %d (%g MB); pos = (%d, %d).\n", local_domain_size_x, local_domain_size_y,
          (double)(2 * halo_size + local_domain_size_x) * (2 * halo_size + local_domain_size_y) * sizeof(stencil_data_t) / (1024.0 * 1024.0),
          pos_x, pos_y);
  int i;
  for(i = 0; i < 2; i++)
    {
      local_domain[i] = malloc(sizeof(stencil_data_t) * (2 * halo_size + local_domain_size_x) * (2 * halo_size + local_domain_size_y));
    }

#ifdef STENCIL_DISPLAY
  full_matrix_display();
#endif /* STENCIL_DISPLAY */
  full_matrix_scatter();

#ifdef STENCIl_DISPLAY
  if(rank == 0)
    fprintf(stderr, "# ## initial state:\n");
  local_domain_halo_exchange_all();
  local_domain_all_display();
#endif /* STENCIL_DISPLAY */
  
  int k;
  for(k = 0; k < 100; k++)
    {
      if(rank == 0)
        {
          fprintf(stderr, "# k = %d\n", k);
        }
      local_domain_halo_exchange_all();
      local_domain_compute_step();
      local_domain_switch();
    }

#ifdef STENCIL_DISPLAY
  local_domain_all_display();
#endif /* STENCIL_DISPLAY */

  full_matrix_gather();
  MPI_Barrier(MPI_COMM_WORLD);

#ifdef STENCIL_DISPLAY
  fflush(stdout);
  MPI_Barrier(MPI_COMM_WORLD);
  full_matrix_display();
#endif /* STENCIL_DISPLAY */
  
  /* ** exit */
  if(full_matrix)
    {
      free(full_matrix);
      full_matrix = NULL;
    }
  for(i = 0; i < 2; i++)
    {
      if(local_domain[0])
        {
          free(local_domain[i]);
          local_domain[i] = NULL;
        }
    }
  MPI_Finalize();
  return 0;
}
