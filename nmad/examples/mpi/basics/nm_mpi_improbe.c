/*
 * NewMadeleine
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define LONG_MSG  (512 * 1024) /* 512 kB */

void nm_mpi_improbe_recv(int source, int tag, int block)
{
  printf("probing from source = %d; tag = %d; blocking = %d\n", source, tag, block);
  int flag = 0;
  MPI_Message msg = MPI_MESSAGE_NULL;
  MPI_Status status;
  if(block)
    {
      MPI_Mprobe(source, tag, MPI_COMM_WORLD, &msg, &status);
    }
  else
    {
      do
        {
          assert(msg == MPI_MESSAGE_NULL);
          MPI_Improbe(source, tag, MPI_COMM_WORLD, &flag, &msg, &status);
        }
      while(flag == 0);
    }
  assert(msg != MPI_MESSAGE_NULL);

  int count = -1;
  MPI_Get_count(&status, MPI_BYTE, &count);
  printf("probed msg of %d bytes from node %d on tag %d\n", count, status.MPI_SOURCE, status.MPI_TAG);
  assert(count >= 0);

  char*buf = malloc(count);
  MPI_Mrecv(buf, count, MPI_BYTE, &msg, MPI_STATUS_IGNORE);
  assert(msg == MPI_MESSAGE_NULL);
  free(buf);
  printf("message received.\n");
}

int main(int argc, char**argv)
{
  int numtasks, rank;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  if(numtasks < 2)
    {
      fprintf(stderr, "need at least 2 processes.\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

  if(rank == 0)
    {
      int block;
      for(block = 0; block <= 1; block++)
        {
          nm_mpi_improbe_recv(1, 2, block);
          nm_mpi_improbe_recv(1, MPI_ANY_TAG, block);
          nm_mpi_improbe_recv(MPI_ANY_SOURCE, 1, block);
          int i;
          for(i = 0; i < (numtasks - 1 ) * 6 - 3; i++)
            {
              nm_mpi_improbe_recv(MPI_ANY_SOURCE, MPI_ANY_TAG, block);
            }
        }
    }
  else
    {
      int i;
      for(i = 0; i < 6; i++)
        {
          const double short_msg = 0.42;
          char*long_msg = malloc(LONG_MSG);
          MPI_Send(&short_msg, 1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
          MPI_Send(long_msg, LONG_MSG, MPI_BYTE, 0, 2, MPI_COMM_WORLD);
          free(long_msg);
        }
    }

  MPI_Finalize();
  exit(0);
}
