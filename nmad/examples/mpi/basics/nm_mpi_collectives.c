/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void my_operation(void*a, void*b, int*r, MPI_Datatype*type);

static void nm_mpi_global_init_global_sum(int reduce[2])
{
  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  reduce[0] = rank * 10;
  reduce[1] = 15 + rank;
}

static void nm_mpi_global_check_global_sum(int global_sum[2], const char*op)
{
  int rank = -1;
  int numtasks = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  int i, sum1 = 0, sum2 = 0;
  for(i = 0; i < numtasks; i++)
    {
      sum1 += i * 10;
      sum2 += 15 + i;
    }
  if(sum1 == global_sum[0] && sum2 == global_sum[1])
    {
      fprintf(stdout, "[%d] Success for %s global sum\n", rank, op);
    }
  else
    {
      fprintf(stdout, "[%d] Error! %s global sum [%d,%d] [%d, %d]\n",
              rank, op, global_sum[0], global_sum[1], sum1, sum2);
    }
}

int main(int argc, char **argv)
{
  int numtasks, rank, provided;
  const int root = 0;

  // Initialise MPI
  MPI_Init_thread(&argc, &argv, MPI_THREAD_SINGLE, &provided);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

  {
    int global_rank = { 0 };
    if(rank == root)
      {
        global_rank = 49;
      }
    MPI_Bcast(&global_rank, 1, MPI_INT, root, MPI_COMM_WORLD);
    if(global_rank == 49)
      {
        fprintf(stdout, "[%d] Success for MPI_Brodcast int\n", rank);
      }
    else
      {
        fprintf(stdout, "[%d] Error! Broadcasted message [%d]\n", rank, global_rank);
      }
  }

  {
    float buffer[2] = { 0.0 };
    if(rank == 0)
      {
        buffer[0] = 12.45;
        buffer[1] = 3.14;
      }
    MPI_Bcast(buffer, 2, MPI_FLOAT, root, MPI_COMM_WORLD);
    if((buffer[0] == (float)12.45) && (buffer[1] == (float)3.14))
      {
        fprintf(stdout, "[%d] Success for MPI_Broadcast floats\n", rank);
      }
    else
      {
        fprintf(stdout, "[%d] Error! Broadcasted message [%f,%f]\n", rank, buffer[0], buffer[1]);
      }
  }

  {
    int reduce[2];
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(reduce);
    MPI_Reduce(reduce, global_sum, 2, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);
    if(rank == root)
      {
        nm_mpi_global_check_global_sum(global_sum, "MPI_Reduce");
      }
  }

  {
    int reduce[2];
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(reduce);
    MPI_Request req;
    MPI_Ireduce(reduce, global_sum, 2, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD, &req);
    MPI_Wait(&req, MPI_STATUS_IGNORE);
    if(rank == root)
      {
        nm_mpi_global_check_global_sum(global_sum, "MPI_Ireduce");
      }
  }

  {
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(global_sum);
    MPI_Request req;
    MPI_Ireduce(MPI_IN_PLACE, global_sum, 2, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD, &req);
    MPI_Wait(&req, MPI_STATUS_IGNORE);
    if(rank == root)
      {
        nm_mpi_global_check_global_sum(global_sum, "MPI_Ireduce MPI_IN_PLACE");
      }
  }

  {
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(global_sum);
    MPI_Reduce(MPI_IN_PLACE, global_sum, 2, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);
    if(rank == root)
      {
        nm_mpi_global_check_global_sum(global_sum, "MPI_Reduce MPI_IN_PLACE");
      }
  }

  {
    int reduce[2];
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(reduce);
    MPI_Allreduce(reduce, global_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    nm_mpi_global_check_global_sum(global_sum, "MPI_Allreduce");
  }

  {
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(global_sum);
    MPI_Allreduce(MPI_IN_PLACE, global_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    nm_mpi_global_check_global_sum(global_sum, "MPI_Allreduce MPI_IN_PLACE");
  }

  {
    int reduce[2];
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(reduce);
    MPI_Request req;
    MPI_Iallreduce(reduce, global_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD, &req);
    MPI_Wait(&req, MPI_STATUS_IGNORE);
    nm_mpi_global_check_global_sum(global_sum, "MPI_Iallreduce");
  }

  {
    int global_sum[2] = { 0 };
    nm_mpi_global_init_global_sum(global_sum);
    MPI_Request req;
    MPI_Iallreduce(MPI_IN_PLACE, global_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD, &req);
    MPI_Wait(&req, MPI_STATUS_IGNORE);
    nm_mpi_global_check_global_sum(global_sum, "MPI_Iallreduce MPI_IN_PLACE");
  }

  {
    long lreduce[2];
    long global_product[2];
    int i;
    long product0=1, product1=1;

    lreduce[0] = (rank + 1) * 2;
    lreduce[1] = rank + 1;
    MPI_Allreduce(lreduce, global_product, 2, MPI_LONG, MPI_PROD, MPI_COMM_WORLD);

    for(i = 0; i < numtasks; i++)
      {
        product0 *= (i+1)*2;
        product1 *= (i+1);
      }
    if(product0 == global_product[0] && product1 == global_product[1])
      {
        fprintf(stdout, "[%d] Success for MPI_Allreduce product\n", rank);
      }
    else
      {
        fprintf(stdout, "[%d] Error! MPI_Allreduce product [%li,%li]\n", rank, global_product[0], global_product[1]);
      }
  }

  {
    int reduce[3] = {rank+1, rank+2, rank+3};
    int global_reduce[3];
    MPI_Op operator;
    int i, reduce0=1, reduce1=2, reduce2=3;

    MPI_Op_create(my_operation, 0, &operator);
    MPI_Allreduce(reduce, global_reduce, 3, MPI_INT, operator, MPI_COMM_WORLD);
    MPI_Op_free(&operator);

    for(i = 1; i < numtasks; i++)
      {
        reduce0 += i+1;
        reduce1 += i+2;
        reduce2 += i+3;
      }
    if(reduce0 == global_reduce[0] && reduce1 == global_reduce[1] && reduce2 == global_reduce[2])
      {
        fprintf(stdout, "[%d] Success for MPI_Allreduce with user-defined operator\n", rank);
      }
    else
      {
        fprintf(stdout, "[%d] Error! MPI_Allreduce with user-defined operator [%d,%d,%d][%d,%d,%d]\n", rank,
                global_reduce[0], global_reduce[1], global_reduce[2],
                reduce0, reduce1, reduce2);
      }
  }

  MPI_Finalize();
  exit(0);
}

void my_operation(void *a, void *b, int *r, MPI_Datatype *type)
{
  int*in = a;
  int*inout = b;
  int i;

  if(*type != MPI_INT)
    {
      fprintf(stdout, "Error: Argument type not recognized\n");
      exit(-1);
    }
  for(i = 0; i < *r ; i++)
    {
      //printf("Inout[%d] = %d\n", i, inout[i]);
      inout[i] += in[i];
      /*    inout[i] *= 2; */
      //printf("Inout[%d] = %d\n", i, inout[i]);
    }
}
