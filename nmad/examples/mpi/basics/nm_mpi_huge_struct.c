/*
 * NewMadeleine
 * Copyright (C) 2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * test communication with struct type larger than MAX_INT.
 * thanks to Nathalie Furmento for the test.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>
#include <limits.h>

int main(int argc, char **argv)
{
  int rank, size;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  fprintf(stderr, "# i am rank %d out of %d nodes\n", rank, size);

  if (size !=  2)
    {
      fprintf(stderr, "need 2 nodes (%d nodes available)\n", size);
      MPI_Finalize();
      return 77;
    }

  {
#define BLOCK_SIZE INT_MAX
#define FULL_SIZE (size_t)2*BLOCK_SIZE+5
    int ret;
    int blocklengths[3] = {BLOCK_SIZE, BLOCK_SIZE, 5};
    MPI_Aint displacements[3] = {0, BLOCK_SIZE, (MPI_Aint)2*BLOCK_SIZE};
    MPI_Datatype types[3] = {MPI_CHAR, MPI_CHAR, MPI_CHAR};
    MPI_Datatype mpi_datatype;

    ret = MPI_Type_create_struct(3, blocklengths, displacements, types, &mpi_datatype);
    assert(ret == MPI_SUCCESS);

    ret = MPI_Type_commit(&mpi_datatype);
    assert(ret == MPI_SUCCESS);

    if (rank == 0)
      {
        MPI_Aint lb, extent;
        MPI_Type_get_extent(mpi_datatype, &lb, &extent);
        printf("size for %zu elements --> %zu lb %zu extent\n", FULL_SIZE, lb, extent);

        printf("# initializing data...\n");
        char l='a';
        char*xx = calloc(1, FULL_SIZE);
        size_t i;
        for(i = 0 ; i < FULL_SIZE ; i++)
          {
            xx[i] = l;
            l ++;
            if (l > 'z') l='a';
          }
        printf("# data ready; sending...\n");
        MPI_Send(xx, 1, mpi_datatype, 1, 29, MPI_COMM_WORLD);
        free(xx);
      }
    else if (rank == 1)
      {
        char*xx = calloc(1, FULL_SIZE);
        MPI_Recv(xx, 1, mpi_datatype, 0, 29, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("# data received; checking...\n");
        char l='a';
        size_t i;
        for(i = 0 ; i < FULL_SIZE ; i++)
          {
            if (xx[i] != l)
              {
                fprintf(stderr, "element %zu received value  %c instead of %c\n", i, xx[i], l);
                assert(xx[i] == l);
              }
            l ++;
            if (l > 'z') l='a';
          }
        free(xx);
      }

    MPI_Type_free(&mpi_datatype);
  }

  MPI_Finalize();
}
