/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define MAXLEN (1024*1024)

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  const int self = nm_comm_rank(p_comm);
  const int size = nm_comm_size(p_comm);
  const nm_session_t p_session = nm_comm_get_session(p_comm);
  const nm_tag_t tag = 0x42;
  int n;
  int root;
  for(n = 1; n <= size; n++)
    {
      if(self < n)
        {
          for(root = 0; root < n; root++)
            {
              if(self == root)
                {
                  printf("starting n = %d; root = %d\n", n, root);
                }
              nm_group_t p_group = nm_group_new();
              int i;
              for(i = 0; i < n; i++)
                {
                  nm_group_add_node(p_group, nm_comm_get_gate(p_comm, i));
                }
              void*sbuf = NULL;
              if(self == root)
                {
                  sbuf = malloc(size * MAXLEN);
                }
              nm_len_t len;
              for(len = 1; len < MAXLEN; len *= 2)
                {
                  char*buf = malloc(len);
                  memset(buf, 0, len);
                  if(self == root)
                    {
                      printf("n = %d; root = %d; len = %ld\n", n, root, len);
                      int i;
                      for(i = 0; i < size; i++)
                        {
                          fill_buffer(sbuf + i * len, len);
                        }
                    }
                  nm_coll_group_barrier(p_session, p_group, self, 1);

                  nm_coll_group_scatter(p_session, p_group, root, self, sbuf, len, buf, len, tag);

                  printf("  checking data.\n");
                  control_buffer(buf, len);
                  printf("ok\n");
                  free(buf);
                }
              if(sbuf)
                free(sbuf);
            }
        }
    }

  nm_examples_exit();
  exit(0);
}
