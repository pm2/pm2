/*
 * NewMadeleine
 * Copyright (C) 2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Gather on rank 0 the hostname of each process and display them, useful to check binding */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define HOSTNAME_LEN 255

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  const int self = nm_comm_rank(p_comm);
  const int size = nm_comm_size(p_comm);
  const int root = 0;
  const nm_tag_t tag = 0x42;
  char local_hostname[HOSTNAME_LEN];

  gethostname(local_hostname, HOSTNAME_LEN);

  if(self == root)
    {
      char* hostnames = malloc(size*HOSTNAME_LEN*sizeof(char));
      int i = 0;

      nm_coll_gather(p_comm, root, local_hostname, HOSTNAME_LEN, hostnames, HOSTNAME_LEN, tag);

      for(i = 0; i < size; i++)
        {
          printf("%d\t%s\n", i, hostnames + i*HOSTNAME_LEN);
        }
      free(hostnames);
    }
  else
    {
      nm_coll_gather(p_comm, root, local_hostname, HOSTNAME_LEN, NULL, 0, tag);
    }

  nm_examples_exit();

  exit(0);
}
