/*
 * NewMadeleine
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define MAXLEN (16*1024*1024)

int main(int argc, char**argv)
{ 
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  const int root = 0;
  const int self = nm_comm_rank(p_comm);
  const int size = nm_comm_size(p_comm);
  const nm_session_t p_session = nm_comm_get_session(p_comm);
  const nm_tag_t tag = 0x42;
  int n;
  for(n = 1; n <= size; n++)
    {
      if(self < n)
        {
          if(self == root)
            {
              printf("starting n = %d\n", n);
            }
          nm_group_t p_group = nm_group_new();
          int i;
          for(i = 0; i < n; i++)
            {
              nm_group_add_node(p_group, nm_comm_get_gate(p_comm, i));
            }
          nm_len_t len;
          for(len = 1; len < MAXLEN; len = _next(len, 1.1, 1))
            {
              char*buf = malloc(len);
              if(self == root)
                {
                  printf("nodes = %d; len = %ld\n", n, len);
                  fill_buffer(buf, len);
                }
              else
                {
                  clear_buffer(buf, len);
                }
              nm_coll_group_barrier(p_session, p_group, self, 1);
              struct nm_data_s data;
              nm_data_contiguous_build(&data, (void*)buf, len);
              nm_coll_group_data_bcast(p_session, p_group, root, self, &data, tag);
              control_buffer(buf, len);
              if(self == root)
                {
                  printf("ok\n");
                }
              free(buf);
            }
          nm_group_free(p_group);
        }
    }
  nm_examples_exit();
  exit(0);
}
