/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

const char *msg = "hello, world";

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  nm_examples_barrier(1);
  nm_group_t p_group = nm_comm_group(p_comm);
  const int self = nm_group_rank(p_group);
  const nm_tag_t tag = 0x42;

  int i;
  for(i = 0; i < nm_group_size(p_group); i++)
    {
      if(self == 0)
        {
          fprintf(stderr, "# sleeping on node #%d\n", i);
        }
      if(self == i)
        {
          puk_sleep(4);
        }
      struct nm_coll_req_s*p_coll_req = nm_coll_group_ibarrier(p_session, p_group, self, tag);
      fprintf(stderr, "# node #%d waiting for round #%d\n", self, i);
      nm_coll_req_wait(p_coll_req);
    }

  nm_examples_exit();
  exit(0);
}
