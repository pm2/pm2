/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define MAXLEN (16*1024*1024)
#define MULT   1.414
#define CALIBRATE 1000
#define ITERATE   100
#define COMPUTEMAX 5000

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  nm_group_t p_group = nm_comm_group(p_comm);
  const int root = 0;
  const int self = nm_group_rank(p_group);
  const nm_tag_t tag = 0x42;

  double barrier_best = -1.0;
  int i;
  for(i = 0; i < CALIBRATE; i++)
    {
      puk_tick_t t1, t2;
      PUK_GET_TICK(t1);
      nm_examples_barrier(1);
      PUK_GET_TICK(t2);
      const double delay = PUK_TIMING_DELAY(t1, t2);
      if((barrier_best < 0) || (delay < barrier_best))
        barrier_best = delay;
    }
  if(self == root)
    {
      fprintf(stderr, "# barrier = %8.2f usec.\n", barrier_best);
    }

  void*buf = malloc(MAXLEN);
  int c;
  for(c = 0; c < COMPUTEMAX; ((c == 0) ? c++ : (c *= 2)))
    {
      if(self == root)
        {
          fprintf(stderr, "\n# compute = %d usec.\n", c);
        }
      int len;
      for(len = 4; len < MAXLEN; len *= MULT)
        {
          puk_tick_t t1, t2;
          struct nm_data_s data;
          double t = -1.0;
          for(i = 0; i < ITERATE; i++)
            {
              nm_examples_barrier(1);
              PUK_GET_TICK(t1);
              nm_data_contiguous_build(&data, (void*)buf, len);
              struct nm_coll_req_s*p_bcast = nm_coll_group_data_ibcast(p_session, p_group, root, self, &data, tag, NULL, NULL);
              compute(c);
              nm_coll_req_wait(p_bcast);
              nm_examples_barrier(1);
              PUK_GET_TICK(t2);
              const double delay = PUK_TIMING_DELAY(t1, t2);
              if((t < 0) || (delay < t))
                t = delay;
            }
          const double bcast_lat = t - barrier_best;
          const double bcast_bw = (len / bcast_lat) * nm_group_size(p_group);
          if(self == root)
            {
              fprintf(stderr, "%8d \t %8.2f \t %8.2f \t %8.2f \t %d\n", len, t, bcast_lat, bcast_bw, c);
            }
        }
    }
  free(buf);

  nm_examples_exit();
  exit(0);
}
