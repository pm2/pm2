#include <stdio.h>
#include <stdlib.h>

#include <nm_coll_interface.h>

static void usage(char*prog_name)
{
  printf("Describe a tree\n");
  printf("Usage: %s tree_kind nb_nodes\n", prog_name);
}

int main(int argc, char**argv)
{
  if(argc != 3)
    {
      usage(argv[0]);
      return EXIT_FAILURE;
    }

  const nm_coll_tree_kind_t tree_kind = nm_coll_tree_kind_by_name(argv[1]);
  const int nb_nodes = atoi(argv[2]);

  int i, step, node, parent, n_children;

  printf("Describing %s tree with %d nodes\n", nm_coll_tree_kind_name(tree_kind), nb_nodes);

  struct nm_coll_tree_info_s*trees = malloc(nb_nodes * sizeof(struct nm_coll_tree_info_s));

  for(i = 0; i < nb_nodes; i++)
    {
      nm_coll_tree_init(&trees[i], tree_kind, nb_nodes, i, /* root: */ 0);
    }

  printf("Max arity: %d\n", trees[0].max_arity);
  printf("Height: %d\n", trees[0].height);

  int*children = malloc(trees[0].max_arity * sizeof(int));

  for(step = 0; step < trees[0].height; step++)
    {
      printf("=== Step %d\n", step);

      for(node = 0; node < nb_nodes; node++)
        {
          nm_coll_tree_step(&trees[node], step, &parent, children, &n_children);
          if(n_children > 0)
            {
              printf("Node %d has %d child%s: %d", node, n_children, n_children > 1 ? "ren" : "", children[0]);
              for(i = 1; i < n_children; i++)
                {
                  printf(", %d", children[i]);
                }
              printf("\n");
            }
        }
    }

  free(children);
  free(trees);

  return EXIT_SUCCESS;
}
