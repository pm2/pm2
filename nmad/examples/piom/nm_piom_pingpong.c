/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#ifdef PIOMAN
#include <pioman.h>
#endif /* PIOMAN */

#ifdef PIOMAN_MULTITHREAD

#define WARMUPS_DEFAULT 10
#define LOOPS_DEFAULT   100000
#define THREADS_DEFAULT 4
#define DATA_CONTROL_ACTIVATED 0

#define MIN_DEFAULT     0
#define MAX_DEFAULT     (64 * 1024 * 1024)
#define MULT_DEFAULT    2
#define INCR_DEFAULT    0

static int loops = LOOPS_DEFAULT;
static int threads = THREADS_DEFAULT;
static int thread_count = -1;
static int warmups = WARMUPS_DEFAULT;
static int bind_offset = -1;
static nm_len_t start_len = MIN_DEFAULT;
static nm_len_t end_len = MAX_DEFAULT;
static double multiplier = MULT_DEFAULT;
static nm_len_t increment = INCR_DEFAULT;
static volatile int go = 0;
static double*lats_all_threads = NULL;


void usage_ping(void)
{
  fprintf(stderr, "-S start_len - starting length [%d]\n", MIN_DEFAULT);
  fprintf(stderr, "-E end_len - ending length [%d]\n", MAX_DEFAULT);
  fprintf(stderr, "-I incr - length increment [%d]\n", INCR_DEFAULT);
  fprintf(stderr, "-M mult - length multiplier [%d]\n", MULT_DEFAULT);
  fprintf(stderr, "\tNext(0)      = 1+increment\n");
  fprintf(stderr, "\tNext(length) = length*multiplier+increment\n");
  fprintf(stderr, "-N iterations - iterations per length [%d]\n", LOOPS_DEFAULT);
  fprintf(stderr, "-T thread - max number of communicating threads, from 1 to T [%d]\n", THREADS_DEFAULT);
  fprintf(stderr, "-t thread - number of communicating threads\n");
  fprintf(stderr, "-W warmup - number of warmup iterations [%d]\n", WARMUPS_DEFAULT);
  fprintf(stderr, "-bind-offset offset - first cpuid to bind threads [not bound]\n");
}

static int comp_double(const void*_a, const void*_b)
{
  const double*a = _a;
  const double*b = _b;
  if(*a < *b)
    return -1;
  else if(*a > *b)
    return 1;
  else
    return 0;
}

static inline nm_len_t _iterations(int iterations, nm_len_t len)
{
  const uint64_t max_data = MAX_DEFAULT;
  if(len <= 0)
    len = 1;
  uint64_t data_size = ((uint64_t)iterations * (uint64_t)len);
  if(data_size  > max_data)
    {
      iterations = (max_data / (uint64_t)len);
      if(iterations < 2)
        iterations = 2;
    }
  return iterations;
}

void server(void* arg)
{
  int    my_pos = *(int*) arg;
  char  *buf    = NULL;
  nm_tag_t tag  = (nm_tag_t) my_pos;
  int    k;
  int iterations = loops;
  uint32_t len;

  if (bind_offset >= 0)
    {
      if (piom_bind_current_thread_to_core(((int) my_pos) + bind_offset) < 0)
        {
          printf("Error while binding server thread %d to core #%d\n", (int) my_pos, ((int) my_pos) + bind_offset);
        }
      else
        {
          printf("Binding server thread %d to core #%d\n", (int) my_pos, ((int) my_pos) + bind_offset);
        }
    }

  while(!go)
    piom_thread_yield();

  for(len = start_len; len <= end_len; len = _next(len, multiplier, increment))
    {
      buf = malloc(len);
      clear_buffer(buf, len);
      iterations = _iterations(iterations, len);

      for(k = 0; k < iterations + warmups; k++)
        {
          nm_sr_request_t request, request2;

          nm_sr_isend(p_session, p_gate, tag, buf, len, &request2);
          nm_sr_irecv(p_session, p_gate, tag, buf, len, &request);
          nm_sr_rwait(p_session, &request);
          nm_sr_swait(p_session, &request2);

#if DATA_CONTROL_ACTIVATED
          control_buffer("réception", buf, len);
#endif
        }
      free(buf);
    }
}

void client(void*arg)
{
  int        my_pos = *(int*)arg;
  nm_tag_t   tag    = (nm_tag_t)my_pos;
  char      *buf    = NULL;
  puk_tick_t t1, t2;
  int        k;
  int iterations = loops;
  uint32_t len;
  double* lats = malloc(sizeof(double) * iterations);


  if (bind_offset >= 0)
    {
      if (piom_bind_current_thread_to_core(((int) my_pos) + bind_offset) < 0)
        {
          printf("Error while binding client thread %d to core #%d\n", (int) my_pos, ((int) my_pos) + bind_offset);
        }
      else
        {
          printf("Binding client thread %d to core #%d\n", (int) my_pos, ((int) my_pos) + bind_offset);
        }
    }


  while(!go)
    piom_thread_yield();

  for(len = start_len; len <= end_len; len = _next(len, multiplier, increment))
    {
      buf = malloc(len);
      clear_buffer(buf, len);
      fill_buffer(buf, len);
      iterations = _iterations(iterations, len);

      for(k = 0; k < warmups; k++)
        {
          nm_sr_request_t request, request2;
#if DATA_CONTROL_ACTIVATED
          control_buffer("envoi", buf, len);
#endif
          nm_sr_isend(p_session, p_gate, tag, buf, len, &request2);
          nm_sr_irecv(p_session, p_gate, tag, buf, len, &request);
          nm_sr_rwait(p_session, &request);
          nm_sr_swait(p_session, &request2);

#if DATA_CONTROL_ACTIVATED
          control_buffer("reception", buf, len);
#endif
        }

      for(k = 0; k < iterations; k++)
        {
          nm_sr_request_t request, request2;
#if DATA_CONTROL_ACTIVATED
          control_buffer("envoi", buf, len);
#endif
          PUK_GET_TICK(t1);
          nm_sr_isend(p_session, p_gate, tag, buf, len, &request2);
          nm_sr_irecv(p_session, p_gate, tag, buf, len, &request);
          nm_sr_rwait(p_session, &request);
          nm_sr_swait(p_session, &request2);
          PUK_GET_TICK(t2);
          lats[k] = PUK_TIMING_DELAY(t1, t2) / 2;
#if DATA_CONTROL_ACTIVATED
          control_buffer("reception", buf, len);
#endif
        }

      qsort(lats, iterations, sizeof(double), &comp_double);
      const double min_lat = lats[0];
      const double max_lat = lats[iterations - 1];
      const double med_lat = lats[(iterations - 1) / 2];
      const double d1_lat  = lats[(iterations - 1) / 10];
      const double d9_lat  = lats[9 * (iterations - 1) / 10];
      double avg_lat = 0.0;
      for(k = 0; k < iterations; k++)
        {
          avg_lat += lats[k];
        }
      avg_lat /= iterations;
      const double bw_million_byte = len / min_lat;
      const double bw_mbyte        = bw_million_byte / 1.048576;

      printf("%2u\t\t%9lld\t%9.3lf\t%9.3f\t%9.3f\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf\n",
             (int) my_pos, (long long) len, min_lat, bw_million_byte, bw_mbyte, d1_lat, med_lat, avg_lat, d9_lat, max_lat);
      fflush(stdout);
      lats_all_threads[my_pos] = med_lat;

      free(buf);
    }

  free(lats);
}

void run_bench(int t)
{
  if(!is_server)
    printf("# starting %d communicating threads...\n", t);

  piom_thread_t*pid = malloc(sizeof(piom_thread_t) * t);
  lats_all_threads = malloc(t * sizeof(double));
  int*pos = malloc(sizeof(int) * t);
  int k;
  for(k = 0; k < t; k++)
    {
      pos[k] = k;
      if(is_server)
        {
          piom_thread_create(&pid[k], NULL, (void*)server, &pos[k]);
        }
      else
        {
          piom_thread_create(&pid[k], NULL, (void*)client, &pos[k]);
        }
    }
  nm_examples_barrier(0x11);
  go = 1;
  for(k = 0; k < t; k++)
    {
      piom_thread_join(pid[k]);
    }

  nm_examples_barrier(0x12);
  if(!is_server)
    {
      qsort(lats_all_threads, t, sizeof(double), &comp_double);
      const double med_lat = lats_all_threads[t / 2];
      printf("# TOTAL %d \t %9.3f\n", t, med_lat);
      fflush(stdout);
    }
  free(pos);
  free(pid);
  free(lats_all_threads);
}

int main(int argc, char**argv)
{
  int i;

  nm_examples_init(&argc, argv);

  if (argc > 1 && !strcmp(argv[1], "--help"))
    {
      usage_ping();
      nm_examples_exit();
      exit(0);
    }

  for(i = 1; i < argc; i++)
    {
      if(strcmp(argv[i], "-S") == 0)
        {
          start_len = atoll(argv[++i]);
        }
      else if(strcmp(argv[i], "-E") == 0)
        {
          end_len = atoll(argv[++i]);
        }
      else if(strcmp(argv[i], "-I") == 0)
        {
          increment = atoll(argv[++i]);
        }
      else if(strcmp(argv[i], "-M") == 0)
        {
          multiplier = atof(argv[++i]);
        }
      else if(strcmp(argv[i], "-N") == 0)
        {
          loops = atoi(argv[++i]);
        }
      else if (!strcmp(argv[i], "-T"))
        {
          threads = atoi(argv[++i]);
        }
      else if (!strcmp(argv[i], "-t"))
        {
          thread_count = atoi(argv[++i]);
        }
      else if (!strcmp(argv[i], "-W"))
        {
          warmups = atoi(argv[++i]);
        }
      else if (!strcmp(argv[i], "-bind-offset"))
        {
          bind_offset = atoi(argv[++i]);
        }
      else
        {
          fprintf(stderr, "Illegal argument %s\n", argv[i]);
          usage_ping();
          nm_examples_exit();
          exit(0);
        }
    }

  if(!is_server)
    {
      printf("# times in us\n");
      printf("# thread | size (bytes) \t|  latency \t| 10^6 B/s \t| MB/s   \t| d1     \t| median  \t| avg    \t| d9     \t| max\n");
    }

  if(thread_count > 0)
    {
      run_bench(thread_count);
    }
  else
    {
      for(i = 1; i <= threads; i++)
        {
          run_bench(i);
        }
    }

  if(!is_server)
    printf("##### Benchmark Done! ####\n");

  nm_examples_exit();
  exit(0);
}

#else /* PIOMAN_MULTITHREAD */
int main(){ return -1;}
#endif /* PIOMAN_MULTITHREAD */
