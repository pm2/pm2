/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* This example / test shows how to receive a data sent with an mcast when the
 * size of the data is in the data itself with iovec. */


#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define NB_EXCHANGES 10

struct recv_req
{
  nm_sr_request_t request;
  nm_len_t size;
  char* buffer;
  nm_session_t session;
  nm_cond_status_t cond_received;

  struct nm_datav_s datav;
  struct nm_data_s data;
};


static void recv_callback(nm_sr_event_t event, const nm_sr_event_info_t* p_info, void* ref)
{
  assert(!((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA))); // Both event can't be triggered at the same time !

  struct recv_req* req = (struct recv_req*) ref;

  if (event & NM_SR_EVENT_RECV_DATA)
    {
      // Header arrived, so get the size and store it in req->size:
      struct nm_data_s data_header;
      nm_data_contiguous_build(&data_header, &req->size, sizeof(nm_len_t));
#ifndef NDEBUG
      int ret =
#endif
      nm_sr_recv_peek(req->session, &req->request, &data_header);
      assert(ret == NM_ESUCCESS);

      // Now we know the size, allocate the buffer:
      req->buffer = malloc(req->size);

      /* Last step: give this buffer to NewMadeleine to receive data
       * We need to use an nm_datav to easily take into account the offset used
       * during the peek. */
      nm_datav_init(&req->datav);
      nm_datav_add_chunk(&req->datav, &req->size, sizeof(nm_len_t));
      nm_datav_add_chunk(&req->datav, req->buffer, req->size);
      nm_data_datav_build(&req->data, &req->datav);
      nm_sr_recv_offset(req->session, &req->request, sizeof(nm_len_t));
      nm_sr_recv_unpack_data(req->session, &req->request, &req->data);
    }
  else if (event & NM_SR_EVENT_FINALIZED)
    {
      nm_cond_signal(&req->cond_received, 1);
      nm_datav_destroy(&req->datav);
    }
}

int main(int argc, char** argv)
{
  const nm_tag_t tag = 0xbeef;
  const char hello[] = "Hello, world!";
  nm_len_t hello_size = strlen(hello) + 1;
  int worldsize;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          struct iovec v[2] = {
            [0] = { .iov_base = &hello_size, .iov_len = sizeof(nm_len_t) },
            [1] = { .iov_base = (void*) hello, .iov_len = hello_size }
          };
          struct nm_data_s data;
          nm_data_iov_build(&data, v, 2);

          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, sizeof(nm_len_t), NM_COLL_TREE_CHAIN);

          free(dests);
        }
      else
        {
          struct recv_req req;
          req.session = nm_comm_get_session(p_comm);
          nm_cond_init(&req.cond_received, 0);

          /* we post a recv without giving a buffer because we don't know the required size of this buffer,
           * the buffer will be allocated and provided to nmad when the header of data will be received,
           * in recv_callback() */
          nm_sr_recv_init(req.session, &req.request);
          nm_sr_request_set_ref(&req.request, &req);
          nm_sr_request_monitor(req.session, &req.request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
              &recv_callback);
          nm_sr_recv_irecv(req.session, &req.request, NM_ANY_GATE, tag, NM_TAG_MASK_FULL);

          // We cannot wait with nm_sr_rwait() because the request has a monitor.
          nm_cond_wait(&req.cond_received, 1, nm_core_get_singleton());
          nm_cond_destroy(&req.cond_received);

          if (strcmp(req.buffer, hello))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", req.buffer, hello);
            }

          free(req.buffer);
        }
      nm_coll_barrier(p_comm, 0xF2);
    }

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}
