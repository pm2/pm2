/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example / test of the mcast interface with many mcast_send */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define NX (256*256)
#define NB_MCASTS 10

int main(int argc, char** argv)
{
  int i, j, worldsize, rank;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  nm_launcher_get_rank(&rank);

  nm_gate_t* gates = malloc(worldsize*sizeof(nm_gate_t));

  int* dests = malloc(sizeof(int) * (worldsize - 1));
  j = 0;
  for (i = 0; i < worldsize; i++)
    {
      nm_launcher_get_gate(i, &gates[i]);

      if (i != rank)
        {
          dests[j++] = i;
        }
    }

  float** vectors = malloc(NB_MCASTS*worldsize*sizeof(float*));
  nm_sr_request_t* requests = malloc(NB_MCASTS*worldsize*sizeof(nm_sr_request_t));
  for (i = 0; i < NB_MCASTS*worldsize; i++)
    {
      vectors[i] = malloc(NX*sizeof(float));
      for (j = 0; j < NX; j++)
        {
          vectors[i][j] = 1.0f;
        }
    }

  nm_mcast_t mcasts[NB_MCASTS];
  struct nm_data_s handles[NB_MCASTS];



  int sender_rank = 0;
  for (sender_rank = 0; sender_rank < worldsize; sender_rank++)
    {
      for (i = 0; i < NB_MCASTS; i++)
        {
          int tag = sender_rank*NB_MCASTS+i;
          assert(tag < worldsize*NB_MCASTS);

          if (rank == sender_rank)
            {
              /*printf("[%d] posting coop with tag %d\n", rank, tag);*/
              nm_data_contiguous_build(&handles[i], (float*) vectors[tag], NX*sizeof(float));
              nm_mcast_send_init(mcast_service, &mcasts[i]);
              nm_mcast_isend(&mcasts[i], p_comm, dests, NULL, worldsize-1, tag, &handles[i], 0, NM_COLL_TREE_DEFAULT);
            }
          else
            {
              /*printf("[%d] posting recv from %d with tag %d\n", rank, sender_rank, tag);*/
              nm_sr_irecv(nm_comm_get_session(p_comm), gates[sender_rank], tag, vectors[tag], NX*sizeof(int), &requests[tag]);
            }
        }
    }

  for (sender_rank = 0; sender_rank < worldsize; sender_rank++)
    {
      for (i = 0; i < NB_MCASTS; i++)
        {
          int tag = sender_rank*NB_MCASTS+i;
          assert(tag < worldsize*NB_MCASTS);

          if (rank == sender_rank)
            {
              nm_mcast_wait(&mcasts[i]);
              nm_mcast_send_destroy(&mcasts[i]);
              /*printf("[%d] end of coop with tag %d\n", rank, tag);*/
            }
          else
            {
              nm_sr_rwait(nm_comm_get_session(p_comm), &requests[tag]);
              /*printf("[%d] end of recv from %d with tag %d\n", rank, sender_rank, tag);*/
            }
        }
    }


  nm_coll_barrier(p_comm, 0xF3);

  printf("success\n");

  for (i = 0; i < NB_MCASTS*worldsize; i++)
    {
        free(vectors[i]);
    }
  free(vectors);
  free(requests);
  free(dests);
  free(gates);

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}
