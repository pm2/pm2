/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Basic example / test using mcast interface to do a broadcast, received by a
 * regular recv but receives are blocking and posted in opposite order as sends. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"


int main(int argc, char** argv)
{
  const char msg_a[] = "Hello, world A!";
  const char msg_b[] = "Hello, world B!";
  const nm_len_t msg_size = strlen(msg_a) + 1;
  int worldsize;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  if (is_server)
    {
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      int i;
      for (i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      struct nm_data_s data_a;
      nm_data_contiguous_build(&data_a, (char*) msg_a, msg_size);

      struct nm_data_s data_b;
      nm_data_contiguous_build(&data_b, (char*) msg_b, msg_size);

      // First send, not blocking, tag 2:
      nm_mcast_t mcast_a;
      nm_mcast_send_init(mcast_service, &mcast_a);
      nm_mcast_isend(&mcast_a, p_comm, dests, NULL, worldsize-1, 2, &data_a, 0, NM_COLL_TREE_DEFAULT);

      // Second send, not blocking, tag 3:
      nm_mcast_t mcast_b;
      nm_mcast_send_init(mcast_service, &mcast_b);
      nm_mcast_isend(&mcast_b, p_comm, dests, NULL, worldsize-1, 3, &data_b, 0, NM_COLL_TREE_DEFAULT);

      nm_mcast_wait(&mcast_a);
      nm_mcast_wait(&mcast_b);

      nm_mcast_send_destroy(&mcast_a);
      nm_mcast_send_destroy(&mcast_b);

      free(dests);
    }
  else
    {
      char* buffer_b = malloc(msg_size);
      char* buffer_a = malloc(msg_size);

      // First blocking receive, tag 3:
      nm_sr_recv(nm_comm_get_session(p_comm), 0, 3, buffer_b, msg_size);

      // Second blocking receive, tag 2:
      nm_sr_recv(nm_comm_get_session(p_comm), 0, 2, buffer_a, msg_size);

      if (strcmp(buffer_a, msg_a))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer_a, msg_a);
        }
      if (strcmp(buffer_b, msg_b))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer_b, msg_b);
        }

      free(buffer_a);
      free(buffer_b);
    }

  nm_coll_barrier(p_comm, 0xF6);

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}
