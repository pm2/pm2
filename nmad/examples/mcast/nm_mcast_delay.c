/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example / test of the mcast interface when the recv is posted after a
 * delay.*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define N_TESTS 4
#define DELAY 2 // seconds


int main(int argc, char** argv)
{
  int i, j, worldsize;
  const nm_tag_t tag = 0xbeef;
  const char msg[] = "Hello, world!";

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  if (is_server)
    {
      printf("Warning: this test waits %d seconds before posting the recv and makes %d iterations, so it is a little bit long, not blocked !\n",
          DELAY, N_TESTS);

      int worldsize;
      nm_launcher_get_size(&worldsize);

      int* dests = malloc(sizeof(int) * (worldsize - 1));
      for (i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      for (j = 0; j < N_TESTS; j++)
        {
          nm_coll_barrier(p_comm, 0xF5);

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, sizeof(msg));
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);
        }

      free(dests);
    }
  else
    {
      for (j = 0; j < N_TESTS; j++)
        {
          nm_coll_barrier(p_comm, 0xF5);

          // sleep to post the recv later: we want to be in the unexpected case:
          puk_sleep(DELAY);

          char* received_msg = malloc(sizeof(msg));
          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, (void*) received_msg, sizeof(msg));

          if (strcmp(msg, received_msg))
            {
              NM_FATAL("Test %d: received_msg = '%s' (should be '%s')\n", j, received_msg, msg);
            }

          free(received_msg);
        }
    }

  printf("success\n");

  nm_coll_barrier(p_comm, 0xF5);

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}
