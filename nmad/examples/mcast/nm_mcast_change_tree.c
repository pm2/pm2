/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Basic example / test using mcast interface to do broadcasts, received by a
 * regular recv, we change the type of tree used by the broadcasts. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define SERVER_PRINTF(msg, ...) do { if (is_server) printf(msg, ## __VA_ARGS__); } while(0)

static nm_mcast_service_t mcast_service;

void broadcast(nm_coll_tree_kind_t tree_type)
{
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;
  const nm_tag_t tag = 0xbeef;

  if (is_server)
    {
      int worldsize;
      nm_launcher_get_size(&worldsize);

      int* dests = malloc(sizeof(int) * (worldsize - 1));
      int i;
      for (i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      struct nm_data_s data;
      nm_data_contiguous_build(&data, (char*) msg, msg_size);
      nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, tree_type);

      free(dests);
    }
  else
    {
      char* buffer = malloc(msg_size);

      nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

      if (strcmp(buffer, msg))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
        }

      free(buffer);
    }

  nm_coll_barrier(p_comm, 0xF3);
}


int main(int argc, char** argv)
{
  int worldsize;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  mcast_service = nm_mcast_init(p_comm);

  SERVER_PRINTF("Start default\n");
  broadcast(NM_COLL_TREE_DEFAULT);
  SERVER_PRINTF("End default\n");

  SERVER_PRINTF("Start chain\n");
  broadcast(NM_COLL_TREE_CHAIN);
  SERVER_PRINTF("End chain\n");

  SERVER_PRINTF("Start binary\n");
  broadcast(NM_COLL_TREE_BINARY);
  SERVER_PRINTF("End binary\n");

  SERVER_PRINTF("Start binomial\n");
  broadcast(NM_COLL_TREE_BINOMIAL);
  SERVER_PRINTF("End binomial\n");

  SERVER_PRINTF("Start chain\n");
  broadcast(NM_COLL_TREE_CHAIN);
  SERVER_PRINTF("End chain\n");

  SERVER_PRINTF("Start default\n");
  broadcast(NM_COLL_TREE_DEFAULT);
  SERVER_PRINTF("End default\n");

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}
