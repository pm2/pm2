/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Basic example / test using mcast interface to do a broadcast on a
 * subcommunicator, received by a regular recv. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

int main(int argc, char** argv)
{
  const nm_tag_t tag = 0xbeef;
  const char msg_a[] = "Hello, world A!";
  const char msg_b[] = "Hello, world B!";
  const nm_len_t msg_size = strlen(msg_a) + 1;
  int worldsize, rank, i;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);
  nm_launcher_get_size(&worldsize);
  nm_launcher_get_rank(&rank);

  if (worldsize < 8 || worldsize % 2 == 1)
    {
      fprintf(stderr, "Error: we need an even number of processes, greater or equal than 8.\n");
      goto exit;
    }

  nm_group_t group = nm_group_new();

  for (i = 0; i < worldsize; i++)
    {
      nm_gate_t gate;
      nm_launcher_get_gate(i, &gate);

      if (rank % 2 == i % 2)
        {
          nm_group_add_node(group, gate);
        }
    }

  nm_comm_t sub_comm = nm_comm_create(p_comm, group);
  const int sub_rank = nm_comm_rank(sub_comm);
  const char* expected_msg = (rank % 2 == 0) ? msg_a : msg_b;

  if (sub_rank == 0)
    {
      const int comm_size = nm_comm_size(sub_comm);
      int* dests = malloc(sizeof(int) * (comm_size - 1));
      for (i = 1; i < comm_size; i++)
        {
          dests[i-1] = i;
        }

      struct nm_data_s data;
      nm_data_contiguous_build(&data, (char*) expected_msg, msg_size);
      nm_mcast_send(mcast_service, sub_comm, dests, NULL, comm_size-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);

      free(dests);
    }
  else
    {
      char* buffer = malloc(msg_size);

      nm_sr_recv(nm_comm_get_session(sub_comm), 0, tag, buffer, msg_size);

      if (strcmp(buffer, expected_msg))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, expected_msg);
        }

      free(buffer);
    }

  nm_comm_destroy(sub_comm);
  nm_group_free(group);

  nm_coll_barrier(p_comm, 0xF7);

  printf("success\n");

exit:
  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return 0;
}
