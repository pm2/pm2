/*
 * NewMadeleine
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_coll_interface.h>
#include "nm_bench_coll_generic.h"

static const int root = 0;
static const nm_tag_t tag1 = 0x42, tag2 = 0x43;

static void nm_bench_coll_2ring_run(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)buf, len);
  nm_coll_bcast_2trees(nm_comm_get_session(p_common->p_comm), nm_comm_group(p_common->p_comm),
                       root, nm_comm_rank(p_common->p_comm),
                       &data, tag1, tag2, 0, NM_COLL_TREE_CHAIN);
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_2ring",
    .name     = "broadcast 2ring",
    .init     = NULL,
    .run      = &nm_bench_coll_2ring_run,
    .finalize = NULL
  };
