/*
 * NewMadeleine
 * Copyright (C) 2019-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Each MPI process is the root of a mcast simultaneously, which means there
 * are p concurrent mcast */


#include <nm_mcast_interface.h>
#include "nm_bench_coll_generic.h"

static const nm_tag_t tag = 0x42;
static nm_mcast_service_t mcast_service;

static void**p_rbuf = NULL;

static void nm_bench_coll_multi_mcast_init(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  mcast_service = nm_mcast_init(p_common->p_comm);
  const int size = nm_comm_size(p_common->p_comm);
  p_rbuf = padico_malloc(size * sizeof(void*));
  int i;
  for(i = 0; i < size; i++)
    {
      p_rbuf[i] = padico_malloc(len);
    }
}

static void nm_bench_coll_multi_mcast_run(struct nm_bench_coll_common_s*p_common, void*p_sbuf, nm_len_t len)
{
  const int size = nm_comm_size(p_common->p_comm);
  nm_sr_request_t*reqs = malloc(size * sizeof(nm_sr_request_t));
  int i, j;
  for(i = 0; i < size - 1; i++)
    {
      nm_sr_irecv(p_common->p_comm->p_session, NM_ANY_GATE, tag, p_rbuf[i], len, &reqs[i]);
    }

  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)p_sbuf, len);
  const int worldsize = nm_comm_size(p_common->p_comm);
  int*dests = malloc(sizeof(int) * (worldsize - 1));
  for(i = 0, j = 0; i < worldsize; i++)
    {
      if(i != nm_comm_rank(p_common->p_comm))
        {
          dests[j] = i;
          j++;
        }
    }
  nm_mcast_send(mcast_service, p_common->p_comm, dests, NULL, worldsize - 1, tag, &data, 0 /* hlen */, NM_COLL_TREE_DEFAULT);
  free(dests);

  for(i = 0; i < size - 1; i++)
    {
      nm_sr_rwait(p_common->p_comm->p_session, &reqs[i]);
    }
  free(reqs);
}

static void nm_bench_coll_multi_mcast_finalize(struct nm_bench_coll_common_s*p_common)
{
  nm_mcast_finalize(mcast_service);
  const int size = nm_comm_size(p_common->p_comm);
  int i;
  for(i = 0; i < size; i++)
    {
      padico_free(p_rbuf[i]);
    }
  padico_free(p_rbuf);
  p_rbuf = NULL;
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_multi_mcast",
    .name     = "multiple multicast",
    .init     = &nm_bench_coll_multi_mcast_init,
    .run      = &nm_bench_coll_multi_mcast_run,
    .finalize = &nm_bench_coll_multi_mcast_finalize
  };
