/*
 * NewMadeleine
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_coll_interface.h>
#include <nm_sync_clocks_interface.h>
#include "nm_bench_coll_generic.h"

static nm_sync_clocks_t p_clocks = NULL;

static void nm_bench_coll_sync_barrier_init(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  assert(p_clocks == NULL);
  p_clocks = nm_sync_clocks_init(p_common->p_comm);
}

static void nm_bench_coll_sync_barrier_run(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  assert(p_clocks != NULL);
  nm_sync_clocks_barrier(p_clocks, NULL);
}

static void nm_bench_coll_sync_barrier_finalize(struct nm_bench_coll_common_s*p_common)
{
  assert(p_clocks != NULL);
  nm_sync_clocks_shutdown(p_clocks);
  p_clocks = NULL;
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_sync_barrier",
    .name     = "sync_clockc barrier",
    .init     = &nm_bench_coll_sync_barrier_init,
    .run      = &nm_bench_coll_sync_barrier_run,
    .finalize = &nm_bench_coll_sync_barrier_finalize
  };
