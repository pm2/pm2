/*
 * NewMadeleine
 * Copyright (C) 2019-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_coll_interface.h>
#include <nm_sync_clocks_interface.h>
#include "nm_bench_coll_generic.h"
#include "../common/nm_examples_helper.h"

#define MIN_DEFAULT   1
#define MAX_DEFAULT   (16*1024*1024)
#define LOOPS_DEFAULT 50
#define INCR_DEFAULT  1
#define MULT_DEFAULT  1.4
#define NODE_INCREMENT 1

static struct nm_bench_coll_common_s common =
  {
    .p_comm    = NULL,
    .p_clocks  = NULL
  };

extern const struct nm_bench_coll_s nm_bench_coll;

static int comp_double(const void*_a, const void*_b)
{
  const double*a = _a;
  const double*b = _b;
  if(*a < *b)
    return -1;
  else if(*a > *b)
    return 1;
  else
    return 0;
}

static void usage(void)
{
  fprintf(stderr, "-S start_len - starting length [%d]\n", MIN_DEFAULT);
  fprintf(stderr, "-E end_len - ending length [%d]\n", MAX_DEFAULT);
  fprintf(stderr, "-I incr - length increment [%d]\n", INCR_DEFAULT);
  fprintf(stderr, "-M mult - length multiplier [%f]\n", MULT_DEFAULT);
  fprintf(stderr, "\tNext(0)      = 1+increment\n");
  fprintf(stderr, "\tNext(length) = length*multiplier+increment\n");
  fprintf(stderr, "-N iterations - iterations per length [%d]\n", LOOPS_DEFAULT);
  fprintf(stderr, "-P incr - number of nodes increment [%d]\n", NODE_INCREMENT);
}

int main(int argc, char**argv)
{
  nm_len_t start_len      = MIN_DEFAULT;
  nm_len_t end_len        = MAX_DEFAULT;
  double   multiplier     = MULT_DEFAULT;
  nm_len_t increment      = INCR_DEFAULT;
  int      iterations     = LOOPS_DEFAULT;
  int      node_increment = NODE_INCREMENT;
  int i;
  for(i = 1; i < argc; i += 2)
    {
      if(strcmp(argv[i], "-S") == 0)
        {
          start_len = atoll(argv[i+1]);
        }
      else if(strcmp(argv[i], "-E") == 0)
        {
          end_len = atoll(argv[i+1]);
        }
      else if(strcmp(argv[i], "-I") == 0)
        {
          increment = atoll(argv[i+1]);
        }
      else if(strcmp(argv[i], "-M") == 0)
        {
          multiplier = atof(argv[i+1]);
        }
      else if(strcmp(argv[i], "-N") == 0)
        {
          iterations = atoi(argv[i+1]);
        }
      else if(strcmp(argv[i], "-P") == 0)
        {
          node_increment = atoi(argv[i+1]);
        }
      else
        {
          fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
          usage();
          exit(1);
        }
    }

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_comm_t p_commworld = nm_comm_world("nm_bench_coll_generic");
  const int commworldsize = nm_comm_size(p_commworld);
  const int commworldrank = nm_comm_rank(p_commworld);
  if(commworldsize < 2)
    {
      fprintf(stderr, "%s: need at least 2 nodes.\n", argv[0]);
      exit(1);
    }

  if(commworldrank == 0)
    {
      printf("# start_len  = %lu\n", start_len);
      printf("# end_len    = %lu\n", end_len);
      printf("# increment  = %lu\n", increment);
      printf("# multiplier = %f\n", multiplier);
      printf("# iterations = %d\n", iterations);
      printf("# bench: %s begin\n", nm_bench_coll.label);
      printf("# n.nodes  length        min.lat.          median         average        max.lat. \n");
    }
  int nodes;
  for(nodes = 2; nodes <= commworldsize; nodes += node_increment)
    {
      if(commworldrank == 0)
        {
          printf("# starting %d nodes...\n", nodes);
        }
      if(nm_comm_rank(p_commworld) >= nodes)
        {
          continue;
        }
      /* node is participating this round */
      nm_group_t p_group = nm_group_new();
      int n;
      for(n = 0; n < nodes; n++)
        {
          nm_gate_t p_gate = nm_comm_get_gate(p_commworld, n);
          nm_group_add_node(p_group, p_gate);
        }
      common.p_comm = nm_comm_create_group(nm_comm_get_session(p_commworld), p_group, p_group);
      const int rank = nm_comm_rank(common.p_comm);
      const int commsize = nm_comm_size(common.p_comm);

      nm_len_t len;
      for(len = start_len; len < end_len; len = _next(len, multiplier, increment))
        {
          char*buf = malloc(len);
          common.p_clocks = nm_sync_clocks_init(common.p_comm);
          if(nm_bench_coll.init != NULL)
            (*nm_bench_coll.init)(&common, buf, len);
          double*lats = (rank == 0) ? malloc(iterations * sizeof(double)) : NULL;
          int k;
          for(k = 0; k < iterations; k++)
            {
              int*rc_all = (rank == 0) ? malloc(commsize * sizeof(int)) : NULL;
              double local_lat = -1.0;
              int rc = 0;
              do
                {
                  const int b = nm_sync_clocks_barrier(common.p_clocks, NULL);
                  rc = (b < 0.0);
                  const double t_begin = nm_sync_clocks_get_time_usec(common.p_clocks);
                  (*nm_bench_coll.run)(&common, buf, len);
                  const double t_end = nm_sync_clocks_get_time_usec(common.p_clocks);
                  local_lat = t_end - t_begin;
                  /* collect sync barrier success */
                  nm_coll_gather(common.p_comm, 0, &rc, sizeof(rc), rc_all, sizeof(int), 0x01);
                  if(rank == 0)
                    {
                      int i;
                      for(i = 0; i < commsize; i++)
                        {
                          rc |= rc_all[i];
                        }
                    }
                  nm_coll_bcast(common.p_comm, 0, &rc, sizeof(rc), 0x02);
                }
              while(rc != 0);
              /* find maximum latency accross nodes */
              double*lat_all = (rank == 0) ? malloc(commsize * sizeof(double)) : NULL;
              nm_coll_gather(common.p_comm, 0, &local_lat, sizeof(local_lat), lat_all, sizeof(double), 0x02);
              if(rank == 0)
                {
                  int i;
                  double max_lat = 0.0;
                  for(i = 0; i < commsize; i++)
                    {
                      if(lat_all[i] > max_lat)
                        {
                          max_lat = lat_all[i];
                        }
                    }
                  lats[k] = max_lat;
                  free(rc_all);
                  free(lat_all);
                }
            }
          if(nm_bench_coll.finalize != NULL)
            (*nm_bench_coll.finalize)(&common);
          /* compute time stats accross iterations */
          if(rank == 0)
            {
              qsort(lats, iterations, sizeof(double), &comp_double);
              const double min_lat = lats[0];
              const double max_lat = lats[iterations - 1];
              const double med_lat = lats[(iterations - 1) / 2];
              double avg_lat = 0.0;
              for(k = 0; k < iterations; k++)
                {
                  avg_lat += lats[k];
                }
              avg_lat /= iterations;
              printf("%4d\t%9lu\t%9.3lf\t%9.3lf\t%9.3lf\t%9.3lf \n", nodes, len, min_lat, med_lat, avg_lat, max_lat);
              fflush(stdout);
              free(lats);
            }
          free(buf);
          nm_sync_clocks_shutdown(common.p_clocks);
          common.p_clocks = NULL;
        }
      nm_comm_destroy(common.p_comm);
      common.p_comm = NULL;
    }
  if(commworldrank == 0)
    {
      printf("# bench: %s end\n", nm_bench_coll.label);
    }
  nm_comm_destroy(p_commworld);
  nm_examples_exit();
  return 0;
}
