/*
 * NewMadeleine
 * Copyright (C) 2016-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 *
 * This example is a draft showing how to build and use diffusion trees for
 * multicast.
 * The data to send is wrapped within a structure containing:
 * - the data itself
 * - ids of the nodes to which the data needs to be forwarded
 * - a header containing the size of the data and the number of nodes
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

#include <nm_launcher_interface.h>
#include <nm_coll_dynamic_interface.h>

#include "../common/nm_examples_helper.h"


#define NX 100


int main(int argc, char**argv)
{
  MPI_Init(&argc, &argv); // Just for eztrace

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_coll_dynamic_init_multicast();

  int* data = malloc(NX * sizeof(int));
  for(int i = 0; i < NX; i++)
    {
      data[i] = -1;
    }

  for (int j = 0; j < 10; j++)
  {
    if(is_server)
      {
        for(int i = 0; i < NX; i++)
          {
            data[i] = j*i;
          }

        int n;
        nm_launcher_get_size(&n);

        int nb_dest_nodes = n - 1;

        int* nodes = malloc(nb_dest_nodes * sizeof(int)); // freed by nm_coll_dynamic_send
        int* prios = calloc(nb_dest_nodes, sizeof(int)); // freed by nm_coll_dynamic_send

        for(int i = 1; i < n; i++)
          {
            nodes[i-1] = i;
          }

        nm_coll_dynamic_send(nodes, prios, nb_dest_nodes, 0, (void*) data, NX * sizeof(int), NULL, NULL);
      }
    else
      {
        nm_sr_request_t request;
        piom_cond_t cond_wait_recv;
        piom_cond_init(&cond_wait_recv, 0);

        nm_coll_dynamic_recv_register_recv(p_session, 0, 0, data, NX * sizeof(int), &request, 
                                           &cond_wait_recv, NULL, NULL, NULL, NULL);

        piom_cond_wait(&cond_wait_recv, 1);
        piom_cond_destroy(&cond_wait_recv);

        for(int i = 0; i < NX; i++)
          {
            assert(data[i] == j*i);
          }
      }

    nm_coll_barrier(p_comm, 123);
  }

  printf("success\n");

  free(data);
  nm_coll_dynamic_shutdown();
  nm_examples_exit();
  return 0;

}
