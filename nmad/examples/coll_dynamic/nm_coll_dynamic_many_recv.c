/*
 * NewMadeleine
 * Copyright (C) 2016-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

#include <nm_launcher_interface.h>
#include <nm_coll_dynamic_interface.h>

#include "../common/nm_examples_helper.h"

#define NX 100
#define NB_EXCHANGES 20


int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_coll_dynamic_init_multicast();

  int* vectors[NB_EXCHANGES];

  for(int i = 0; i < NB_EXCHANGES; i++)
    {
        vectors[i] = malloc(NX * sizeof(int));
        for(int j = 0; j < NX; j++)
          {
            vectors[i][j] = -1;
          }
    }

  if(is_server)
    {
        int n;
        nm_launcher_get_size(&n);

        int nb_dest_nodes = n - 1;


        for(int i = 0; i < NB_EXCHANGES; i++)
            {
                for(int j = 0; j < NX; j++)
                    {
                        vectors[i][j] = i;
                    }
            }

        for(int i = 0; i < NB_EXCHANGES; i++)
            {
                int* nodes = malloc(nb_dest_nodes * sizeof(int)); // freed by nm_coll_dynamic_send
                int* prios = calloc(nb_dest_nodes, sizeof(int)); // freed by nm_coll_dynamic_send

                for(int j = 1; j < n; j++)
                    {
                        nodes[j-1] = j;
                    }

                nm_coll_dynamic_send(nodes, prios, nb_dest_nodes, i, (void*) vectors[i], NX * sizeof(int), NULL, NULL);
            }
      }
    else
      {
          nm_sr_request_t request[NB_EXCHANGES];
          piom_cond_t cond_wait_recv[NB_EXCHANGES];

          for(int i = 0; i < NB_EXCHANGES; i++)
            {
                piom_cond_init(&cond_wait_recv[i], 0);
                nm_coll_dynamic_recv_register_recv(p_session, 0, i, vectors[i], NX * sizeof(int), request + i, 
                                                &cond_wait_recv[i], NULL, NULL, NULL, NULL);

            }

          for(int i = 0; i < NB_EXCHANGES; i++)
            {
                piom_cond_wait(&cond_wait_recv[i], 1);
                piom_cond_destroy(&cond_wait_recv[i]);

                for(int j = 0; j < NX; j++)
                  {
                    assert(vectors[i][j] == i);
                  }
            }
      }


    nm_coll_barrier(p_comm, 123);

  for(int i = 0; i < NB_EXCHANGES; i++)
    {
        free(vectors[i]);
    }

  printf("success\n");

  nm_coll_dynamic_shutdown();
  nm_examples_exit();
  return 0;

}
