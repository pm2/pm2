import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap, BoundaryNorm
from matplotlib.lines import Line2D
import math
import sys
import os
import string

METHOD = 0
NODES_IN_COMM = 1
SIZE = 2
MAIN_TASK_MIN = 3
MAIN_TASK_MED = 4
MAIN_TASK_AVG = 5
MAIN_TASK_MAX = 6

ERRORBAR_SHIFT_STEP = 0.03

ERRORBAR_OPT = "--errorbar"
SLIDE_FORMAT_OPT = "--slide-format"

display_errorbar = False
slide_format = False

METHODS_COLOR = ['blue', 'red', 'green', 'orange']


def convert_number(e):
    if '.' in e:
        return float(e)
    else:
        return int(e)


def parse_options(options):
  global display_errorbar
  global slide_format

  for opt in options:
    if opt == ERRORBAR_OPT:
      display_errorbar = True
    elif opt == SLIDE_FORMAT_OPT:
      slide_format = True
    else:
      print("Unknown option '{}'" % opt)


def plot_time_size_curve(x, y, symbol, color, label, min_data, max_data, ax, errorbar_line_style=None, curve_function=plt.plot, errorbar_shift=0.00):
  curve_function(x, y, symbol + '-', color=color, label=label, markersize=4)

  if display_errorbar:
    for i in range(len(x)):
      (_, _, barline) = ax.errorbar(x[i] + errorbar_shift, y[i], 
                                    yerr=[[round(y[i] - min_data[i], 3)], [round(max_data[i] - y[i], 3)]], 
                                    elinewidth=0.8, capsize=5, ecolor=color)

      if errorbar_line_style is not None:
        barline[0].set_linestyle(errorbar_line_style)



def plot_time_nb_nodes(results, curve_function):
  data_size = [e[SIZE] for e in results if e[METHOD] == results[0][METHOD] and e[NODES_IN_COMM] == results[0][NODES_IN_COMM]]
  cm = plt.get_cmap("jet", len(data_size))
  fig, ax = plt.subplots()

  filename = ""
  title = "Median value of whole task length"
  if curve_function == plt.semilogy:
    filename += "log_"
  filename += "time_nb_nodes"
  if len(data_size) == 1:
    filename += "_" + str(data_size[0]) + "KB"
    title += " " + str(data_size[0]) + "KB"
  filename += ".png"

  for i in range(len(data_size)):
    errorbar_shift = ((-ERRORBAR_SHIFT_STEP) * (len(found_methods) - 1)) / 2

    for m in range(len(found_methods)-1, -1, -1):
      y_time_nb_nodes_med = [e[MAIN_TASK_MED] for e in results if e[METHOD] == m and e[SIZE] == data_size[i]]

      # Error bars:
      y_main_task_min = None
      y_main_task_max = None
      if display_errorbar:
        y_main_task_min = [e[MAIN_TASK_MIN] for e in results if e[METHOD] == m and e[SIZE] == data_size[i]]
        y_main_task_max = [e[MAIN_TASK_MAX] for e in results if e[METHOD] == m and e[SIZE] == data_size[i]]

      plot_time_size_curve(nb_nodes[:len(y_time_nb_nodes_med)], y_time_nb_nodes_med, symbols[m], cm(i) if len(data_size) > 1 else METHODS_COLOR[m], methods[m] + str(data_size[i]), y_main_task_min, y_main_task_max, ax, '--' if m == 0 else None, curve_function, errorbar_shift)

      errorbar_shift += ERRORBAR_SHIFT_STEP

  if len(nb_nodes) > 50:
      plt.xticks(nb_nodes[::5])
  elif len(nb_nodes) > 16:
    plt.xticks(nb_nodes[::3])
  else:
    plt.xticks(nb_nodes)

  #ax.set(xlabel='Nb nodes in communication', ylabel='Time (ms)', title=title)
  ax.set(xlabel='Nombre de noeuds dans la collective', title=title)
  ax.text(-0.01, 1, 'Temps\n(ms)', horizontalalignment='right', verticalalignment='top', transform=ax.transAxes)
  ax.set_ylim(bottom=0)

  if len(data_size) > 1:
    sm = plt.cm.ScalarMappable(cmap=cm, norm=BoundaryNorm([1.5 * d for d in data_size], len(data_size)))
    sm._A = []

    cbar = fig.colorbar(sm, ticks=data_size)
    cbar.set_label("Size of sent data (KB)")

  ax.legend(handles=legend_elements_colored if len(data_size) == 1 else legend_elements, handletextpad=1, borderpad=1, borderaxespad=1, handlelength=0)
  ax.grid()

  if not slide_format:
    fig.set_size_inches(15, 9)

  plt.savefig(os.path.join(working_directory, filename), dpi=100)
  plt.show()


def plot_time_size_all_nb_nodes(results):
  nb_nodes = [e[NODES_IN_COMM] for e in results if e[METHOD] == results[0][METHOD] and e[SIZE] == results[0][SIZE]]
  assert len(nb_nodes) > 1

  title = "Median value of whole task length"
  filename = "task_length.png"

  cm = plt.get_cmap("jet", len(nb_nodes))
  fig, ax = plt.subplots()

  for i in range(0, len(nb_nodes)):
    for m in range(len(methods)):
      current_x_size = [e[SIZE] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[i]]
      y_main_task_med = [e[MAIN_TASK_MED] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[i]]

      # Error bars:
      y_main_task_min = None
      y_main_task_max = None
      if display_errorbar:
        y_main_task_min = [e[MAIN_TASK_MIN] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[i]]
        y_main_task_max = [e[MAIN_TASK_MAX] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[i]]

      plot_time_size_curve(current_x_size, y_main_task_med, symbols[m], cm(i), methods[m] + str(nb_nodes[i]), y_main_task_min, y_main_task_max, ax, '--' if m == 0 else None, curve_function=plt.loglog)


  ax.set(xlabel='Data size (KB)', ylabel='Time (ms)', title=title)

  sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=nb_nodes[0], vmax=nb_nodes[-1]))
  sm._A = []
  cbar = fig.colorbar(sm, ticks=nb_nodes)
  cbar.set_label("Number of nodes in the communication")

  ax.legend(handles=legend_elements, handletextpad=1, borderpad=1, borderaxespad=1, handlelength=0)
  ax.grid()

  if not slide_format:
    fig.set_size_inches(15, 9)

  plt.savefig(os.path.join(working_directory, filename), dpi=100)
  plt.show()


def plot_time_size_one_nb_nodes(results):
  nb_nodes = [e[NODES_IN_COMM] for e in results if e[METHOD] == results[0][METHOD] and e[SIZE] == results[0][SIZE]]
  assert len(nb_nodes) == 1

  title = "Median value of whole task length " + str(nb_nodes[0]) + " nodes"
  filename = "task_length_" + str(nb_nodes[0]) + "nodes.png"

  cm = plt.get_cmap("jet", len(nb_nodes))
  fig, ax = plt.subplots()

  for m in range(len(methods)):
      current_x_size = [e[SIZE] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[0]]
      y_main_task_med = [e[MAIN_TASK_MED] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[0]]
  
      # Error bars:
      y_main_task_min = None
      y_main_task_max = None
      if display_errorbar:
        y_main_task_min = [e[MAIN_TASK_MIN] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[0]]
        y_main_task_max = [e[MAIN_TASK_MAX] for e in results if e[METHOD] == m and e[NODES_IN_COMM] == nb_nodes[0]]
  
      plot_time_size_curve(current_x_size, y_main_task_med, symbols[m], METHODS_COLOR[m], methods[m] + str(nb_nodes[0]), y_main_task_min, y_main_task_max, ax, '--' if m == 0 else None, curve_function=plt.loglog)


  ax.set(xlabel='Data size (KB)', ylabel='Time (ms)', title=title)

  ax.legend(handles=legend_elements_colored, handletextpad=1, borderpad=1, borderaxespad=1, handlelength=0)
  ax.grid()

  if not slide_format:
    fig.set_size_inches(15, 9)

  plt.savefig(os.path.join(working_directory, filename), dpi=100)
  plt.show()


def remove_line(line):
    # remove line if line doesn't start with '#<digit>'
    # and contains letters or '-' or '..'

    is_method_line = (line[0] == '#' and line[1] in string.digits)
    is_useless = ('-' in line or '..' in line or any(c.isalpha() for c in line))

    return (is_useless and not is_method_line)


results = []
methods = []
symbols = ['+', 'o', 'v', 's', 'p']


if len(sys.argv) < 2:
  print("Specify data file !")
  print("Options:")
  print(ERRORBAR_OPT + "\tPlot error bars (may be slow for big sets of data)")
  print(SLIDE_FORMAT_OPT + "\tChange graph format for a more readable one for using in slides")
  sys.exit(1)

working_directory = os.path.dirname(sys.argv[1])

with open(sys.argv[1], 'r') as file:
  lines = [l for l in file.readlines() if remove_line(l) == False]

  i = 0
  while lines[i][0] == '#':
      methods.append(lines[i][lines[i].find(': ')+2:-1])
      i += 1

  for line in lines[i:]:
      if len(line) > 1:
        results.append([convert_number(e) for e in line.strip().replace(" ", "|").replace("\t", "|").split("|") if len(e) > 0])
        results[-1][MAIN_TASK_MIN:MAIN_TASK_MAX+1] = [e / 1000 for e in results[-1][MAIN_TASK_MIN:MAIN_TASK_MAX+1]]

found_methods = []

for e in results:
    if not e[METHOD] in found_methods:
        found_methods.append(e[METHOD])

if len(methods) == 0:
  print("Warning: methods not specified, using default [no coop, coop]")
  methods = ["No coop", "Coop"]
elif len(found_methods) > len(methods):
    print("All methods are not defined !")
    sys.exit(1)


parse_options(sys.argv[2:])


nb_nodes = [e[NODES_IN_COMM] for e in results if e[METHOD] == results[0][METHOD] and e[SIZE] == results[0][SIZE]]
nb_datasize = len([e[SIZE] for e in results if e[METHOD] == results[0][METHOD] and e[NODES_IN_COMM] == results[0][NODES_IN_COMM]])

for m in found_methods:
    for n in nb_nodes:
        if len([e for e in results if e[METHOD] == m and e[NODES_IN_COMM] == n]) != nb_datasize:
            print("Warning: number of data size different for method " + str(m) + " and " + str(n) + " nodes")


legend_elements = [Line2D([0], [0], marker=symbols[m], label=methods[m]) for m in found_methods]
legend_elements_colored = [Line2D([0], [0], marker=symbols[m], label=methods[m], color=METHODS_COLOR[m]) for m in found_methods]


if nb_datasize > 1:
  plot_time_size_all_nb_nodes(results)

if len(nb_nodes) != 1:
  if nb_datasize > 1:
    results_min_nodes = [e for e in results if e[NODES_IN_COMM] == results[0][NODES_IN_COMM]]
    plot_time_size_one_nb_nodes(results_min_nodes)

    results_max_nodes = [e for e in results if e[NODES_IN_COMM] == results[-1][NODES_IN_COMM]]
    plot_time_size_one_nb_nodes(results_max_nodes)

  # all sizes:
  # plot_time_nb_nodes(results, plt.semilogy)
  # plot_time_nb_nodes(results, plt.plot)

  if nb_datasize > 1:
    # min size:
    results_min_size = [e for e in results if e[SIZE] == results[0][SIZE]]
    plot_time_nb_nodes(results_min_size, plt.semilogy)
    plot_time_nb_nodes(results_min_size, plt.plot)

    # max size:
    results_max_size = [e for e in results if e[SIZE] == results[-1][SIZE]]
    plot_time_nb_nodes(results_max_size, plt.semilogy)
    plot_time_nb_nodes(results_max_size, plt.plot)
