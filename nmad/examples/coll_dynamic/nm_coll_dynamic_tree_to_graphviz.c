#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <nm_coll_interface.h>

enum plot_type {
  PLOT_TYPE_BINOMIAL,
  PLOT_TYPE_SEQUENTIAL,
  PLOT_TYPE_BOTH
};

struct options {
  int nb_nodes;
  char* output_file;
  unsigned no_legend;
  unsigned no_sublegend;
  unsigned no_frame;
  enum plot_type type_plot;
};

static void usage(char* prog_name)
{
  fprintf(stderr, "Generate GraphViz file describing binomial tree used for broadcasting data in NewMadeleine.\n");
  fprintf(stderr, "You can then display the generated graph with xdot.\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage: %s [options] nb_nodes\n", prog_name);
  fprintf(stderr, "\n");
  fprintf(stderr, "Parameters:\n");
  fprintf(stderr, "\tnb_nodes            number of nodes in the communication (initial sender included)\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "Options:\n");
  fprintf(stderr, "\t-o <output file>    specify an output file (without this option, data is written in stdout\n");
  fprintf(stderr, "\t-no-legend          doesn't display global legend for the graph\n");
  fprintf(stderr, "\t-no-sublegend       doesn't display legend for each subgraph\n");
  fprintf(stderr, "\t-no-frame           doesn't display frame around each graph\n");
  fprintf(stderr, "\t-t <binomial|seq>   plot only broadcasting tree for binomial or sequential tree (default is both)\n");
  fprintf(stderr, "\t-h                  display this help\n");
}

static int parse_args(int argc, char **argv, struct options* opts)
{
  if(argc == 1)
    {
      usage(argv[0]);
      return -1;
    }

  for(int i = 1; i < argc; i++)
    {
      if(strcmp(argv[i], "-o") == 0)
        {
          opts->output_file = argv[++i];
        }
      else if(strcmp(argv[i], "-no-legend") == 0)
        {
          opts->no_legend = 1;
        }
      else if(strcmp(argv[i], "-no-sublegend") == 0)
        {
          opts->no_sublegend = 1;
        }
      else if(strcmp(argv[i], "-no-frame") == 0)
        {
          opts->no_frame = 1;
        }
      else if(strcmp(argv[i], "-t") == 0)
        {
          i++;

          if(strcmp(argv[i], "binomial") == 0)
            {
              opts->type_plot = PLOT_TYPE_BINOMIAL;
            }
          else if(strcmp(argv[i], "seq") == 0)
            {
              opts->type_plot = PLOT_TYPE_SEQUENTIAL;
            }
          else
            {
              usage(argv[0]);
              return -1;
            }
        }
      else if(strcmp(argv[i], "-h") == 0)
        {
          usage(argv[0]);
          return -1;
        }
      else
        {
          opts->nb_nodes = atoi(argv[i]);
        }
    }

  if(opts->nb_nodes <= 0)
    {
      usage(argv[0]);
      return -1;
    }

  return 0;
}


int main(int argc, char**argv)
{
  int i;

  struct options opts = {
    .nb_nodes = 0,
    .output_file = NULL,
    .no_legend = 0,
    .no_sublegend = 0,
    .no_frame = 0,
    .type_plot = PLOT_TYPE_BOTH
  };

  int ret = parse_args(argc, argv, &opts);
  if(ret)
    {
      return ret;
    }

  FILE* out = NULL;
  if(opts.output_file != NULL)
    {
      out = fopen(opts.output_file, "w");
    }
  else
    {
      out = stdout;
    }

  struct nm_coll_tree_info_s* trees = malloc(opts.nb_nodes * sizeof(struct nm_coll_tree_info_s));
  int** children = malloc(opts.nb_nodes * sizeof(int*));

  char* last_displayed = malloc(opts.nb_nodes * sizeof(char));

  for(i = 0; i < opts.nb_nodes; i++)
    {
      last_displayed[i] = -1;

      nm_coll_tree_init(&trees[i], NM_COLL_TREE_BINOMIAL, opts.nb_nodes, i, 0);
      children[i] = malloc(sizeof(int) * trees[i].max_arity);
    }

  fprintf(out, "digraph G {\n");

  if(!opts.no_legend)
    {
      fprintf(out, "\tlabel = \"Broadcasting data to %d nodes\";\n\n", opts.nb_nodes-1);
    }

  if(opts.type_plot == PLOT_TYPE_BINOMIAL || opts.type_plot == PLOT_TYPE_BOTH)
    {
      fprintf(out, "\tsubgraph cluster_coop {\n");

      if(opts.no_frame)
        {
          fprintf(out, "\t\tpencolor = \"transparent\";\n");
        }

      if(opts.no_sublegend)
        {
          fprintf(out, "\t\tlabel = \"\";\n\n");
        }
      else
        {
          fprintf(out, "\t\tlabel = \"Broadcast with binomial tree\";\n\n");
        }
      
      int step;
      int* node_child = malloc(opts.nb_nodes * trees[0].height * sizeof(int));
      for(i = 0; i < (opts.nb_nodes * trees[0].height); i++)
        {
          node_child[i] = -1;
        }

      for(step = 0; step < trees[0].height; step++)
        {
          for(i = 0; i < opts.nb_nodes; i++)
            {
              int parent = -1;
              int n_children = 0;
              int child;

              nm_coll_tree_step(&trees[i], step, &parent, children[i], &n_children);
              
              child = (n_children > 0) ? *children[i] : -1;

              if(child != -1)
                {
                  fprintf(out, "\t\t# step %d: %d sends to %d:\n", step, i, child);

                  fprintf(out, "\t\t\"coop-%d-%d\" [label=\"%d\"];\n", step+1, child, child);
                  fprintf(out, "\t\t\"coop-%d-%d\" -> \"coop-%d-%d\";\n", step, i, step+1, child);
                  node_child[(step * opts.nb_nodes) + i] = child;

                  if(last_displayed[i] != -1 && last_displayed[i] != step)
                    {
                      fprintf(
                        out,
                        "\t\t\"coop-%d-%d\" -> \"coop-%d-%d\" [style=\"dashed\"];\n",
                        last_displayed[i], i, step, i
                      );
                      fprintf(
                        out,
                        "\t\t\"coop-%d-%d\" [label=\"%d\", style=\"dashed\"];\n",
                        step, i, i
                      );

                      if(node_child[(last_displayed[i] * opts.nb_nodes) + i] != -1)
                        {
                          fprintf(
                            out, 
                            "\t\t{rank=same; \"coop-%d-%d\" -> \"coop-%d-%d\" [style=\"invis\"]};\n", 
                            step, i, step, node_child[(last_displayed[i] * opts.nb_nodes) + i]
                          );
                        }
                    }
                  else
                    {
                      fprintf(out, "\t\t\"coop-%d-%d\" [label=\"%d\"];\n", step, i, i);
                    }

                  last_displayed[i] = step;
                  last_displayed[child] = step + 1;

                  if(i < (opts.nb_nodes-1))
                    {
                      fprintf(out, "\n");
                    }
                }
            }
        }

      free(node_child);

      fprintf(out, "\t}\n");    
    }
  
  if(opts.type_plot == PLOT_TYPE_BOTH)
    {
      fprintf(out, "\n");
    }

  if(opts.type_plot == PLOT_TYPE_SEQUENTIAL || opts.type_plot == PLOT_TYPE_BOTH)
    {
      fprintf(out, "\tsubgraph cluster_seq {\n");

      if(opts.no_frame)
      {
        fprintf(out, "\t\tpencolor = \"transparent\";\n");
      }

      if(opts.no_sublegend)
      {
        fprintf(out, "\t\tlabel = \"\";\n\n");
      }
    else
      {
        fprintf(out, "\t\tlabel = \"Sequential broadcast\";\n\n");
      }

      fprintf(out, "\t\t\"seq-0-0\" [label=\"0\"];\n");

      for(i = 1; i < opts.nb_nodes; i++)
        {
          if(i < (opts.nb_nodes-1))
            {
              fprintf(out, "\t\t\"seq-%d-0\" [label=\"0\", style=\"dashed\"];\n", i);
              fprintf(out, "\t\t\"seq-%d-0\" -> \"seq-%d-0\" [style=\"dashed\"];\n", i-1, i);
            }
          
          fprintf(out, "\t\t\"seq-%d-%d\" [label=\"%d\"];\n", i, i, i);
          fprintf(out, "\t\t\"seq-%d-0\" -> \"seq-%d-%d\";\n", i-1, i, i);
        }

      fprintf(out, "\t}\n");
    }
    
  fprintf(out, "}\n");

  if(opts.output_file != NULL)
  {
    fclose(out);
  }

  for(i = 0; i < opts.nb_nodes; i++)
    {
      free(children[i]);
    }

  free(last_displayed);
  free(trees);
  free(children);
  
  return 0;
}

/*fprintf(
                out, 
                "\t\t{rank=same; \"seq-%d-0\" -> \"seq-%d-%d\" [style=\"invis\"]};\n", 
                i, i, i
              );*/
