/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file nm_sr_hello2.c
 * Basic hello world example.
 */

#include <nm_launcher_interface.h>
#include <nm_sendrecv_interface.h>

const char *msg = "hello, world!";

int main(int argc, char**argv)
{
  /* ** init nmad */
  nm_launcher_init(&argc, argv);
  int rank;
  nm_launcher_get_rank(&rank);
  int size;
  nm_launcher_get_size(&size);
  if(size != 2)
    {
      NM_FATAL("this examples requires 2 nodes.\n");
    }

  /* ** open session */
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "sr_hello2");

  /* ** allocate buffers */
  uint32_t len = 1 + strlen(msg);
  char *buf = malloc(len*sizeof(char));
  memset(buf, 0, len);

  /* ** find peer node */
  const int peer = 1 - rank;
  nm_gate_t gate;
  nm_launcher_get_gate(peer, &gate);

  const nm_tag_t tag = 0;
  nm_sr_request_t s_request, r_request;
  if(rank == 0)
    {
      nm_sr_irecv(p_session, NM_ANY_GATE, tag, buf, len, &r_request);
      nm_sr_rwait(p_session, &r_request);

      nm_sr_isend(p_session, gate, tag, buf, len, &s_request);
      nm_sr_swait(p_session, &s_request);
    }
  else if (rank == 1)
    {
      strcpy(buf, msg);

      nm_sr_isend(p_session, gate, tag, buf, len, &s_request);
      nm_sr_swait(p_session, &s_request);

      nm_sr_irecv(p_session, gate, tag, buf, len, &r_request);
      nm_sr_rwait(p_session, &r_request);

      printf("buffer contents: <%s>\n", buf);
    }
  free(buf);

  /* ** close sesion */
  nm_session_close(p_session);

  /* ** close nmad */
  nm_launcher_exit();
  exit(0);
}
