/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */




#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <nm_launcher_interface.h>
#include <nm_sendrecv_interface.h>
#include <nm_coll_interface.h>
#include <Padico/Puk.h>

/** @file
 * @brief Test to see the behavior of the priorities when every node
 * send message to every other one. You need to have Traces enable to
 * verify the result.
 */

 /** Number of message each node will send in total. */
#define MSG_NUM 30

 /** The maximum value a priority can get. Priorities are cycling. */
#define MAX_PRIO 15

/** The size of every message.*/
#define MSG_SIZE (1 * 1024 * 1024L  )

int main(int argc, char**argv)
{
  int my_rank, rank = 0, size;
  nm_session_t p_session;
  nm_launcher_init(&argc, argv);
  nm_session_open(&p_session, "nm_example");
  nm_launcher_get_rank(&my_rank);
  nm_launcher_get_size(&size);

  void *buffer = malloc(2 * MSG_NUM * MSG_SIZE);
  printf("malloc 1 \n");
  nm_sr_request_t*p_reqs = malloc(2 * MSG_NUM * sizeof(nm_sr_request_t));
  printf("malloc2\n");
  int *value = buffer;
  int i, max_prio = MAX_PRIO * size, nb_msg_rank = MSG_NUM/(size-1), cpt = 0;

  printf("Use trace to see if the priorities are working\n");
  printf("Sending %d messages, from %d nodes\n", MSG_NUM, size);

  for(i = 0; i < MSG_NUM; i++, cpt++)
  {
    if(rank == my_rank || cpt == nb_msg_rank)
    {
      rank++;
      cpt=0;
    }

    value = buffer + MSG_SIZE * MSG_NUM + i * MSG_SIZE;
    nm_tag_t tag = rank*100000+my_rank*1000+i%nb_msg_rank;

    nm_gate_t p_gate;
    nm_launcher_get_gate(rank, &p_gate);
    nm_sr_recv_init(p_session, &p_reqs[MSG_NUM + i]);
    nm_sr_recv_unpack_contiguous(p_session, &p_reqs[MSG_NUM + i], value, MSG_SIZE);
    nm_sr_recv_irecv(p_session, &p_reqs[MSG_NUM + i], p_gate, tag, NM_TAG_MASK_FULL);


    value = buffer + i * MSG_SIZE;
    nm_prio_t prio = i % max_prio;
    tag = my_rank*100000+rank*1000+i%nb_msg_rank;
    nm_len_t len = MSG_SIZE;

    nm_sr_send_init(p_session, &p_reqs[i]);
    nm_sr_send_pack_contiguous(p_session, &p_reqs[i], value, len);
    nm_sr_send_set_priority(p_session, &p_reqs[i], prio);
    nm_sr_send_isend(p_session, &p_reqs[i], p_gate, tag);
  }

  printf("Juste avant waitall()\n");
  for(i = 0; i < 2 * MSG_NUM; i++)
  {
    nm_sr_request_wait(&p_reqs[i]);
  }
  printf("Communication complete\n");


  free(buffer);
  free(p_reqs);

  nm_session_close(p_session);
  nm_launcher_exit();

  exit(0);
}
