/*
 * NewMadeleine
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @fileexample that trigger NM_ETRUNCATED error.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define LEN (64 * 1024)

int main(int argc, char**argv)
{ 
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_PAIRS);

  char*buf = malloc(LEN);
  fill_buffer(buf, LEN);
  
  if(is_server)
    {
      int i;
      for(i = 0; i < 5; i++)
        {
          int rc = nm_sr_recv(p_session, NM_ANY_GATE, i /* tag */, buf, 1 /* size */);
          printf("recv rc = %d (%s)\n", rc, nm_strerror(rc));
          assert(rc == -NM_ETRUNCATED);
        }
    }
  else
    {
      int rc = nm_sr_send(p_session, p_gate, 0, buf, 2);
      check_rc(rc);
      rc = nm_sr_send(p_session, p_gate, 1, buf, 200);
      check_rc(rc);
      rc = nm_sr_send(p_session, p_gate, 2, buf, 2000);
      check_rc(rc);
      rc = nm_sr_send(p_session, p_gate, 3, buf, 10000);
      check_rc(rc);
      rc = nm_sr_send(p_session, p_gate, 4, buf, LEN);
      check_rc(rc);
    }
 
  free(buf);
  nm_examples_exit();
  exit(0);
}
