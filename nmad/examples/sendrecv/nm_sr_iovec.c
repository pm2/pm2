/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <err.h>
#include <stdio.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#define OVERRUN 32

static void nm_test_iovec_fill(char*sbuf, char*rbuf, int iov_count, int entry_size)
{
  int i;
  for(i = 0; i < iov_count; i++)
    {
      int j;
      for(j = 0; j < entry_size; j++)
        {
          sbuf[i*entry_size + j] = iov_count - i - 1 + j;
          rbuf[i*entry_size + j] = -1;
        }
    }
}

static void nm_test_iovec_check(char*rbuf, int iov_count, int entry_size)
{
  int i;
  for(i = 0; i < iov_count; ++i)
    {
      int j;
      for(j = 0; j < entry_size; j++)
        {
          const char expected = i + j;
          if(rbuf[i*entry_size + j] != expected)
            {
              printf("data corruption detected- index %d; value %d; expected = %d\n", i, rbuf[i*entry_size+j], expected);
              abort();
            }
        }
    }
}

static void nm_test_iovec_build(struct iovec*v, int iov_count, int entry_size, char*buf, int overrun, int shuffle)
{
  int i;
  for(i = 0; i < iov_count; i++)
    {
      if(!shuffle)
        {
          v[i].iov_base = &buf[i * entry_size];
        }
      else
        {
          v[i].iov_base = &buf[(iov_count - i - 1) * entry_size];
        }
      v[i].iov_len = entry_size;
    }
  v[iov_count - 1].iov_len += overrun;
  v[iov_count].iov_base = NULL;
}

/* symmetrical iovec, n x 4 bytes, shuffled entries
 * force client -> server as unexpected, server -> client as expected
 */
static void nm_test_iovec_symmetrical(char*sbuf, char*rbuf, int iov_count, int entry_size, nm_gate_t src, int overrun)
{
  struct iovec riov[iov_count + 1], siov[iov_count + 1];
  nm_sr_request_t sreq, rreq;

  if(is_server)
    printf("iovec test, %d x %d bytes -> %d x %d bytes %s%s\n", iov_count, entry_size, iov_count, entry_size,
           (src == NM_ANY_GATE?", anysrc":""),
           ((overrun > 0) ? ", rbuf larger":""));

  nm_test_iovec_fill(sbuf, rbuf, iov_count, entry_size);
  nm_test_iovec_build(siov, iov_count, entry_size, sbuf, 0, 0);
  nm_test_iovec_build(riov, iov_count, entry_size, rbuf, overrun, 1);

  if(is_server)
    {
      puk_usleep(100*1000);
      nm_sr_isend_iov(p_session, p_gate, 42, siov, iov_count, &sreq);
      nm_sr_swait(p_session, &sreq);
      nm_sr_irecv_iov(p_session, src, 42, riov, iov_count, &rreq);
    }
  else
    {
      nm_sr_irecv_iov(p_session, src, 42, riov, iov_count, &rreq);
      nm_sr_isend_iov(p_session, p_gate, 42, siov, iov_count, &sreq);
      nm_sr_swait(p_session, &sreq);
    }
  nm_sr_rwait(p_session, &rreq);
  nm_test_iovec_check(rbuf, iov_count, entry_size);
  if(is_server)
    printf("  ok.\n");
}

static void nm_test_iovec_sparse_to_contiguous(char*sbuf, char*rbuf, int iov_count, int entry_size, nm_gate_t src, int overrun)
{
  struct iovec siov[iov_count + 1];
  nm_sr_request_t sreq, rreq;

  if(is_server)
    printf("iovec test, %d x %d bytes -> %d bytes%s%s\n", iov_count, entry_size, iov_count * entry_size + overrun,
           (src == NM_ANY_GATE?", anysrc":""),
           ((overrun > 0) ? ", rbuf larger":""));

  nm_test_iovec_fill(sbuf, rbuf, iov_count, entry_size);
  nm_test_iovec_build(siov, iov_count, entry_size, sbuf, 0, 1);

  if(is_server)
    {
      puk_usleep(100*1000);
      nm_sr_isend_iov(p_session, p_gate, 42, siov, iov_count, &sreq);
      nm_sr_swait(p_session, &sreq);
      nm_sr_irecv(p_session, src, 42, rbuf, iov_count * entry_size + overrun, &rreq);
    }
  else
    {
      nm_sr_irecv(p_session, src, 42, rbuf, iov_count * entry_size + overrun, &rreq);
      nm_sr_isend_iov(p_session, p_gate, 42, siov, iov_count, &sreq);
      nm_sr_swait(p_session, &sreq);
    }
  nm_sr_rwait(p_session, &rreq);
  nm_test_iovec_check(rbuf, iov_count, entry_size);
  if(is_server)
    printf("  ok.\n");
}

static void nm_test_iovec_contiguous_to_sparse(char*sbuf, char*rbuf, int iov_count, int entry_size, nm_gate_t src, int overrun)
{
  struct iovec riov[iov_count + 1];
  nm_sr_request_t sreq, rreq;

  if(is_server)
    printf("iovec test, %d bytes -> %d x %d bytes%s%s\n",  iov_count * entry_size, iov_count, entry_size,
           (src == NM_ANY_GATE?", anysrc":""),
           ((overrun > 0) ? ", rbuf larger":""));

  nm_test_iovec_fill(sbuf, rbuf, iov_count, entry_size);
  nm_test_iovec_build(riov, iov_count, entry_size, rbuf, overrun, 1);

  if(is_server)
    {
      puk_usleep(100*1000);
      nm_sr_isend(p_session, p_gate, 42, sbuf, iov_count * entry_size, &sreq);
      nm_sr_swait(p_session, &sreq);
      nm_sr_irecv_iov(p_session, src, 42, riov, iov_count, &rreq);
    }
  else
    {
      nm_sr_irecv_iov(p_session, src, 42, riov, iov_count, &rreq);
      nm_sr_isend(p_session, p_gate, 42, sbuf, iov_count * entry_size, &sreq);
      nm_sr_swait(p_session, &sreq);
    }
  nm_sr_rwait(p_session, &rreq);
  nm_test_iovec_check(rbuf, iov_count, entry_size);
  if(is_server)
    printf("  ok.\n");
}

static void nm_test_iovec_full(int iov_count, int entry_size)
{
  char*sbuf = malloc(iov_count * entry_size + OVERRUN);
  char*rbuf = malloc(iov_count * entry_size + OVERRUN);
  nm_test_iovec_symmetrical(sbuf, rbuf, iov_count, entry_size, p_gate, 0);
  nm_test_iovec_symmetrical(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, 0);
  nm_test_iovec_symmetrical(sbuf, rbuf, iov_count, entry_size, p_gate, OVERRUN);
  nm_test_iovec_symmetrical(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, OVERRUN);

  nm_test_iovec_sparse_to_contiguous(sbuf, rbuf, iov_count, entry_size, p_gate, 0);
  nm_test_iovec_sparse_to_contiguous(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, 0);
  nm_test_iovec_sparse_to_contiguous(sbuf, rbuf, iov_count, entry_size, p_gate, OVERRUN);
  nm_test_iovec_sparse_to_contiguous(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, OVERRUN);

  nm_test_iovec_contiguous_to_sparse(sbuf, rbuf, iov_count, entry_size, p_gate, 0);
  nm_test_iovec_contiguous_to_sparse(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, 0);
  nm_test_iovec_contiguous_to_sparse(sbuf, rbuf, iov_count, entry_size, p_gate, OVERRUN);
  nm_test_iovec_contiguous_to_sparse(sbuf, rbuf, iov_count, entry_size, NM_ANY_GATE, OVERRUN);

  free(sbuf);
  free(rbuf);
}

int main(int argc, char ** argv)
{

  nm_examples_init(&argc, argv);

  nm_test_iovec_full(1, 4);
  nm_test_iovec_full(4, 4);
  nm_test_iovec_full(32, 4);
  nm_test_iovec_full(1024, 4);
  nm_test_iovec_full(8*1024, 4);
  nm_test_iovec_full(32*1024, 4);

  nm_test_iovec_full(1, 256);
  nm_test_iovec_full(8, 256);
  nm_test_iovec_full(32, 256);
  nm_test_iovec_full(256, 256);
  nm_test_iovec_full(8192, 256);

  nm_test_iovec_full(1, 65*1024);
  nm_test_iovec_full(8, 65*1024);
  nm_test_iovec_full(32, 65*1024);

  nm_test_iovec_full(2, 1024*1024);
  nm_test_iovec_full(4, 512*1024);
  nm_test_iovec_full(32, 256*1024);

  nm_examples_exit();
  return 0;
}
