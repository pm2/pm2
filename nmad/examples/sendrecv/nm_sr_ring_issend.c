/*
 * NewMadeleine
 * Copyright (C) 2022-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* This example/test performs a ring with the issend instruction and mimics the
 * working of nmad in StarPU.
 *
 * Inspired (a lot) from StarPU/mpi/tests/ring_sync.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"
#include <nm_launcher_interface.h>

#define NITER 128
#define REQ_FINALIZED 0x1

static int rank = -1;

struct req_s {
  int completed;
  nm_tag_t tag;
  nm_cond_status_t cond;
};

void recv_callback(nm_sr_event_t event, const nm_sr_event_info_t *event_info, void *ref)
{
  struct req_s* req = (struct req_s*) ref;

  if(event & NM_SR_EVENT_FINALIZED)
    {
      req->completed = 1;
      nm_cond_signal(&(req->cond), REQ_FINALIZED);
    }
}

void custom_wait(struct req_s *req)
{
  while(!req->completed || !nm_cond_test_locked(&(req->cond), REQ_FINALIZED))
    {
      nm_cond_wait(&(req->cond), REQ_FINALIZED, nm_core_get_singleton());
    }
}

int main(int argc, char **argv)
{
  nm_examples_init(&argc, argv);

  int nloops = NITER;
  int loop = 0;
  int token = -1;
  int worldsize = -1;

  nm_launcher_get_size(&worldsize);
  nm_launcher_get_rank(&rank);

  for(loop = 0; loop < nloops; loop++)
    {
      nm_tag_t tag = loop*worldsize + rank;

      if(loop == 0 && rank == 0)
        {
          token = 0;
        }
      else
        {
          int dst = (rank+worldsize-1) % worldsize;
          nm_gate_t src_gate = NULL;
          nm_launcher_get_gate(dst, &src_gate);

          nm_sr_request_t request;
          struct req_s req;
          req.completed = 0;
          req.tag = tag;
          nm_cond_init(&(req.cond), 0);

          nm_sr_recv_init(p_session, &request);
          nm_sr_recv_unpack_contiguous(p_session, &request, &token, sizeof(token));
          nm_sr_request_set_ref(&request, &req);
          nm_sr_request_monitor(p_session, &request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_COMPLETED, recv_callback);
          nm_sr_recv_irecv(p_session, &request, src_gate, req.tag, NM_TAG_MASK_FULL);

          custom_wait(&req);

          nm_cond_destroy(&(req.cond));
        }

      token++;

      if(loop != (nloops-1) || rank != (worldsize-1))
        {
          int dst = (rank+1) % worldsize;
          nm_gate_t dst_gate = NULL;
          nm_launcher_get_gate(dst, &dst_gate);

          nm_sr_request_t request;
          struct req_s req;
          req.completed = 0;
          req.tag = tag + 1;
          nm_cond_init(&(req.cond), 0);

          nm_sr_send_init(p_session, &request);
          nm_sr_send_pack_contiguous(p_session, &request, &token, sizeof(token));
          nm_sr_request_set_ref(&request, &req);
          nm_sr_request_monitor(p_session, &request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_COMPLETED, recv_callback);
          nm_sr_send_issend(p_session, &request, dst_gate, req.tag);

          custom_wait(&req);

          nm_cond_destroy(&(req.cond));
        }
    }

  if(rank == (worldsize-1))
    {
      int expected = nloops * worldsize;
      printf("token = %d (should be %d)\n", token, expected);
      if(token != expected)
        {
          nm_examples_exit();
          return EXIT_FAILURE;
        }
    }

  nm_examples_exit();

  return EXIT_SUCCESS;
}
