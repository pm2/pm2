#! /usr/bin/gnuplot
# ## usage:
# # gnuplot -c nm_sync_clocks_drift.gp "output.dat"

filename  = ARG1
outfile   = filename . ".png"

print "# filename:  " . filename
print "# outfile:   " . outfile

set term pngcairo noenhanced fontscale 1 size 1280,960 font "arial 12"

set xlabel "Rounds"
set ylabel "Drift (usec.)"
set title "Clock drift"
set grid

set output outfile

# note: we draw NaN just to get color legend for other graphs than 0
plot filename using 1:5:2 with points linecolor variable title "1", NaN title "2", NaN title "3", NaN title "4", NaN title "5", NaN title "6", NaN title "7"

