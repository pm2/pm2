/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file measure drift of our sync_clocks over a long period
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <nm_launcher_interface.h>
#include <nm_sync_clocks_interface.h>

#define ROUNDS 100
#define ROUNDS_TIME_SECONDS 5
#define ROUNDTRIPS 1000

struct nm_clock_entry_s
{
  double t_master;
  double t_slave;
  long double skew;
};
struct nm_clock_drift_s
{
  struct nm_clock_entry_s entries[ROUNDS];
};

int main(int argc, char**argv)
{
  int rank = -1, worldsize = -1;
  static nm_session_t p_session = NULL;
  nm_gate_t p_gate = NM_GATE_NONE;

  nm_launcher_init(&argc, argv);
  nm_session_open(&p_session, "nm_example");
  nm_launcher_get_rank(&rank);
  nm_launcher_get_size(&worldsize);

  nm_comm_t p_comm = nm_comm_world("sync_clocks_drift");
  nm_sync_clocks_t p_clocks = nm_sync_clocks_init(p_comm);

  struct nm_clock_drift_s*p_drift = malloc(worldsize * sizeof(struct nm_clock_drift_s));

  int round, i;
  for(round = 0; round < ROUNDS; round++)
    {
      fprintf(stderr, "# ## round = %d / %d; %d seconds\n", round, ROUNDS, ROUNDS_TIME_SECONDS);
      nm_coll_barrier(p_comm, 0);
      if(rank == 0)
        {
          for(i = 1; i < worldsize; i++)
            {
              /* pingpong to get a common event on both master time and slave time */
              double min_lat = -1.0;
              nm_launcher_get_gate(i, &p_gate);
              int j;
              for(j = 0; j < ROUNDTRIPS; j++)
                {
                  double t_dummy = -1.0, t_mid = -1.0;
                  const double t_begin = nm_sync_clocks_get_time_usec(p_clocks);
                  nm_sr_send(p_session, p_gate, 1, &t_dummy, sizeof(t_dummy));
                  nm_sr_recv(p_session, p_gate, 1, &t_mid, sizeof(t_mid));
                  const double t_end = nm_sync_clocks_get_time_usec(p_clocks);
                  const double lat = (t_end - t_begin) / 2.0;
                  if(min_lat < 0 || lat < min_lat)
                    {
                      min_lat = lat;
                      p_drift[i].entries[round].t_master = t_begin;
                      p_drift[i].entries[round].t_slave = t_mid - min_lat;
                    }
                }
            }
        }
      else
        {
          double t_mid;
          nm_launcher_get_gate(0, &p_gate);
          int j;
          for(j = 0; j < ROUNDTRIPS; j++)
            {
              double t_dummy = -1.0;
              nm_sr_recv(p_session, p_gate, 1, &t_dummy, sizeof(t_dummy));
              t_mid = nm_sync_clocks_get_time_usec(p_clocks);
              nm_sr_send(p_session, p_gate, 1, &t_mid, sizeof(t_mid));
            }
        }
      /* measure time for synchronizing clocks */
      fprintf(stderr, "# synchronizing offsets...\n");
      const double t_sync_begin = nm_sync_clocks_get_time_usec(p_clocks);
      nm_sync_clocks_synchronize(p_clocks);
      const double t_sync_end = nm_sync_clocks_get_time_usec(p_clocks);
      const double delay_sync = t_sync_end - t_sync_begin;
      fprintf(stderr, "# synchronizong done; time = %.2lf usec. (%.2lf usec./node)\n",
              delay_sync, delay_sync / (double)(worldsize - 1.0));
      if(rank == 0)
        {
          for(i = 0; i < worldsize; i++)
            {
              const long double skew = nm_sync_clocks_get_skew(p_clocks, i);
              p_drift[i].entries[round].skew = skew;
            }
        }
      puk_sleep(ROUNDS_TIME_SECONDS);
    }

  /* final sync */
  nm_sync_clocks_synchronize(p_clocks);
  if(rank == 0)
    {
      printf("# round; dest; t_master; t_slave; drift; skew\n");
      for(round = 0; round < ROUNDS; round++)
        {
          for(i = 1; i < worldsize; i++)
            {
              const double t_master = p_drift[i].entries[round].t_master;
              const double t_slave = nm_sync_clocks_remote_to_global(p_clocks, i, p_drift[i].entries[round].t_slave);
              const long double skew = p_drift[i].entries[round].skew;
              printf("%d \t %d \t %.3lf \t %.3lf \t %.3lf \t %.16Lf\n", round, i, t_master, t_slave, t_slave - t_master, skew);
            }
        }
      for(i = 0; i < worldsize; i++)
        {
          fprintf(stderr, "# skew with rank %d = %.16Lg\n", i, nm_sync_clocks_get_skew(p_clocks, i));
        }
    }

  nm_sync_clocks_shutdown(p_clocks);
  nm_launcher_exit();

  free(p_drift);

  return 0;
}
