/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Benchmark to test the mecanism of coll_dynamic:
 * - use custom datatype
 * - submit header
 * - use nm_sr_recv_peek()
 * - use handlers to set where received data will be stored
 */

#include "nm_bench_generic.h"
#include <nm_sendrecv_interface.h>

#define DATA_TAG 0x05
static const nm_tag_t data_tag = DATA_TAG;

#define NB_NODES 32
#define SRC_NODE 13 // random value, not important for the benchmark

/** header of mutlicast requests */
struct multicast_header_s
{
  nm_len_t body_len;
  int nb_nodes;
  int src_node;
}  __attribute__((packed));

struct nm_multicast_req_s
{
  nm_sr_request_t* request;
  struct nm_data_s body_data;
  struct nm_data_s nodes_data;
  int* ref_nodes;
  struct multicast_header_s* ref_header;
  nm_cond_status_t* cond;
  struct nm_data_s header_data;
  struct nm_data_s multicast_data;
  nm_len_t body_len;
};

struct nm_multicast_content_s
{
  void* header_ptr;
  const struct nm_data_s* p_nodes;
  const struct nm_data_s* p_body;
};

static void nm_multicast_traversal(const void*_content, nm_data_apply_t apply, void*_context);
const struct nm_data_ops_s nm_multicast_ops =
  {
    .p_traversal = &nm_multicast_traversal
  };

PUK_ALLOCATOR_TYPE(nm_multicast_req, struct nm_multicast_req_s);

static nm_multicast_req_allocator_t nm_multicast_req_allocator = NULL;

NM_DATA_TYPE(multicast, struct nm_multicast_content_s, &nm_multicast_ops);

static void nm_multicast_traversal(const void*_content, nm_data_apply_t apply, void*_context)
{

  const struct nm_multicast_content_s*p_content = _content;

  (*apply)(p_content->header_ptr, sizeof(struct multicast_header_s), _context);

  nm_data_traversal_apply(p_content->p_nodes, apply, _context);
  nm_data_traversal_apply(p_content->p_body, apply, _context);
}

void nm_multicast_data_build(struct nm_data_s* p_multicast_data, void* hptr,
                             const struct nm_data_s* p_nodes, const struct nm_data_s* p_body)
{
  nm_data_multicast_set(p_multicast_data, (struct nm_multicast_content_s)
      {
        .header_ptr = hptr,
        .p_nodes    = p_nodes,
        .p_body     = p_body
      });
}

void monitor_recv(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void* _ref)
{
  struct nm_multicast_req_s* p_req = _ref;

  assert(! ((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA)));

  if(event & NM_SR_EVENT_RECV_DATA)
    {
      int rc = nm_sr_recv_peek(nm_bench_common.p_session, p_req->request, &p_req->header_data);

      if(rc != NM_ESUCCESS)
        {
          fprintf(stderr, "# nm_multicast: rc = %d in nm_sr_recv_peek()\n", rc);
          abort();
        }

      const int nb_nodes = p_req->ref_header->nb_nodes;

      assert(nb_nodes == NB_NODES);
      assert(p_req->ref_header->body_len == p_req->body_len);
      assert(p_req->ref_header->src_node == SRC_NODE);

      p_req->ref_nodes = malloc(nb_nodes * sizeof(int));
      nm_data_contiguous_build(&p_req->nodes_data, p_req->ref_nodes, nb_nodes * sizeof(int));
      nm_multicast_data_build(&p_req->multicast_data, p_req->ref_header, &p_req->nodes_data, &p_req->body_data);

      nm_sr_recv_unpack_data(nm_bench_common.p_session, p_req->request, &p_req->multicast_data);
    }

  if(event & NM_SR_EVENT_FINALIZED)
    {
      for (int i = 0; i < p_req->ref_header->nb_nodes; i++)
        {
          assert(p_req->ref_nodes[i] == (i + 16));
        }

      free(p_req->request);
      free(p_req->ref_header);
      free(p_req->ref_nodes);

      nm_cond_signal(p_req->cond, NM_STATUS_FINALIZED);
    }
}

static void sr_recv(void*buf, nm_len_t len)
{
  nm_cond_status_t cond;
  nm_cond_init(&cond, 0);

  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->ref_header = malloc(sizeof(struct multicast_header_s));
  nm_data_contiguous_build(&p_req->header_data, p_req->ref_header, sizeof(struct multicast_header_s));
  nm_data_contiguous_build(&p_req->body_data, buf, len);
  p_req->body_len = len;
  p_req->cond = &cond;
  p_req->request = malloc(sizeof(nm_sr_request_t));
  nm_sr_recv_init(nm_bench_common.p_session, p_req->request);
  nm_sr_request_set_ref(p_req->request, p_req);
  nm_sr_request_monitor(nm_bench_common.p_session, p_req->request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
                        &monitor_recv);
  nm_sr_recv_irecv(nm_bench_common.p_session, p_req->request, NM_ANY_GATE, data_tag, NM_TAG_MASK_FULL);

  nm_cond_wait(&cond, NM_STATUS_FINALIZED, nm_core_get_singleton());

  nm_multicast_req_free(nm_multicast_req_allocator, p_req);
}

void monitor_send(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_multicast_req_s* p_req = (struct nm_multicast_req_s*) _ref;

  free(p_req->ref_header);
  free(p_req->ref_nodes);
  free(p_req->request);

  nm_cond_signal(p_req->cond, NM_STATUS_FINALIZED);
}

static void sr_send(void*buf, nm_len_t len)
{
  nm_cond_status_t cond;
  nm_cond_init(&cond, 0);

  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->ref_header = malloc(sizeof(struct multicast_header_s));
  p_req->ref_header->body_len = len;
  p_req->ref_header->nb_nodes = NB_NODES;
  p_req->ref_header->src_node = SRC_NODE;
  p_req->body_len = len;
  p_req->cond = &cond;

  p_req->ref_nodes = malloc(NB_NODES * sizeof(int));
  for (int i = 0; i < NB_NODES; i++)
    {
      p_req->ref_nodes[i] = i + 16; // random value, not important for the benchmark
    }

  nm_data_contiguous_build(&p_req->nodes_data, p_req->ref_nodes, NB_NODES * sizeof(int));
  nm_data_contiguous_build(&p_req->body_data, buf, len);

  struct nm_data_s multicast_data;
  nm_multicast_data_build(&multicast_data, p_req->ref_header, &p_req->nodes_data, &p_req->body_data);
  p_req->request = malloc(sizeof(nm_sr_request_t));
  nm_sr_send_init(nm_bench_common.p_session, p_req->request);
  nm_sr_send_pack_data(nm_bench_common.p_session, p_req->request, &multicast_data);
  nm_sr_request_set_ref(p_req->request, p_req);
  nm_sr_request_monitor(nm_bench_common.p_session, p_req->request, NM_SR_EVENT_FINALIZED, &monitor_send);
  nm_sr_send_dest(nm_bench_common.p_session, p_req->request, nm_bench_common.p_gate, data_tag);
  nm_sr_send_header(nm_bench_common.p_session, p_req->request,
                    sizeof(struct multicast_header_s) + NB_NODES * sizeof(int));
  nm_sr_send_submit(nm_bench_common.p_session, p_req->request);

  nm_cond_wait(&cond, NM_STATUS_FINALIZED, nm_core_get_singleton());

  nm_multicast_req_free(nm_multicast_req_allocator, p_req);
}

static void nm_bench_peek_datatype_handler_init(void*buf, nm_len_t len)
{
  if (nm_multicast_req_allocator == NULL)
    {
      nm_multicast_req_allocator = nm_multicast_req_allocator_new(8);
    }
}

static void nm_bench_peek_datatype_handler_server(void*buf, nm_len_t len)
{
  sr_recv(buf, len);
  sr_send(buf, len);
}

static void nm_bench_peek_datatype_handler_client(void*buf, nm_len_t len)
{
  sr_send(buf, len);
  sr_recv(buf, len);
}

const struct nm_bench_s nm_bench =
  {
    .name   = "sendrecv peek with custom datatype (coll_dynamic)",
    .init   = &nm_bench_peek_datatype_handler_init,
    .server = &nm_bench_peek_datatype_handler_server,
    .client = &nm_bench_peek_datatype_handler_client
  };
