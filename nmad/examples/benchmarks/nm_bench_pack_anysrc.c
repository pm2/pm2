/*
 * NewMadeleine
 * Copyright (C) 2015-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_bench_generic.h"
#include <nm_pack_interface.h>

static const nm_tag_t tag = 0x01;

static void nm_bench_pack_anysrc_server(void*buf, nm_len_t len)
{
  nm_pack_cnx_t cnx;
  nm_begin_unpacking(nm_bench_common.p_session, NM_ANY_GATE, tag, &cnx);
  nm_unpack(&cnx, buf, len);
  nm_end_unpacking(&cnx);

  nm_begin_packing(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &cnx);
  nm_pack(&cnx, buf, len);
  nm_end_packing(&cnx);
}

static void nm_bench_pack_anysrc_client(void*buf, nm_len_t len)
{
  nm_pack_cnx_t cnx;
  nm_begin_packing(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &cnx);
  nm_pack(&cnx, buf, len);
  nm_end_packing(&cnx);

  nm_begin_unpacking(nm_bench_common.p_session, NM_ANY_GATE, tag, &cnx);
  nm_unpack(&cnx, buf, len);
  nm_end_unpacking(&cnx);
}

const struct nm_bench_s nm_bench =
  {
    .name = "pack anysrc",
    .server = &nm_bench_pack_anysrc_server,
    .client = &nm_bench_pack_anysrc_client
  };

