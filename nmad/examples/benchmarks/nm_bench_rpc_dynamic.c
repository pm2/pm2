/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * Benchmark that mimics the communications performed by coll_dynamic.
 */

#include "nm_bench_generic.h"
#include <nm_rpc_interface.h>

#define DATA_TAG 0x05
static const nm_tag_t data_tag = DATA_TAG;
static void*volatile rbuf = NULL;
static nm_rpc_service_t p_service = NULL;
static nm_cond_status_t cond;

#define NNODES 1
static int nodes[NNODES] = { 0 };

struct bench_rpc_dynamic_header_s
{
  nm_len_t body_len;
  int nb_nodes;
  int src_node;
  int seq;
};

static void bench_rpc_dynamic_handler(nm_rpc_token_t p_token)
{
  assert(rbuf != NULL);
  struct bench_rpc_dynamic_header_s header;
  nm_rpc_recv_header(p_token, &header, sizeof(struct bench_rpc_dynamic_header_s));
  assert(header.nb_nodes == NNODES);
  nm_rpc_irecv_body(p_token, nodes, header.nb_nodes * sizeof(int));
  nm_rpc_irecv_body(p_token, rbuf, header.body_len);
}

static void bench_rpc_dynamic_finalizer(nm_rpc_token_t p_token)
{
  nm_cond_signal(&cond, NM_STATUS_FINALIZED);
}

static void bench_rpc_dynamic_send(void*buf, nm_len_t len)
{
  struct bench_rpc_dynamic_header_s header;
  header.body_len = len;
  header.src_node = -1;
  header.nb_nodes = NNODES;

  nm_rpc_req_t p_req = nm_rpc_req_init(p_service, nm_bench_common.p_gate, data_tag);
  nm_rpc_req_pack_header(p_req, &header, sizeof(struct bench_rpc_dynamic_header_s));
  nm_rpc_req_pack_body(p_req, nodes, NNODES * sizeof(int));
  nm_rpc_req_pack_body(p_req, buf, len);
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);
}

static void bench_rpc_dynamic_recv(void*buf, nm_len_t len)
{
  nm_cond_wait(&cond, NM_STATUS_FINALIZED, nm_core_get_singleton());
  nm_cond_init(&cond, 0); /* re-init for next round */
}

static void bench_rpc_dynamic_server(void*buf, nm_len_t len)
{
  bench_rpc_dynamic_recv(buf, len);
  bench_rpc_dynamic_send(buf, len);
}

static void bench_rpc_dynamic_client(void*buf, nm_len_t len)
{
  bench_rpc_dynamic_send(buf, len);
  bench_rpc_dynamic_recv(buf, len);
}

static void bench_rpc_dynamic_init(void*buf, nm_len_t len)
{
  rbuf = buf;
  if(p_service == NULL)
    p_service = nm_rpc_register(nm_bench_common.p_session, data_tag, NM_TAG_MASK_FULL,
                                &bench_rpc_dynamic_handler, &bench_rpc_dynamic_finalizer, NULL);
  nm_cond_init(&cond, 0);
}

const struct nm_bench_s nm_bench =
  {
    .name = "rpc 3-part messages",
    .server = &bench_rpc_dynamic_server,
    .client = &bench_rpc_dynamic_client,
    .init   = &bench_rpc_dynamic_init
  };
