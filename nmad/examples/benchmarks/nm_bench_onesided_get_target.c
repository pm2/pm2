/*
 * NewMadeleine
 * Copyright (C) 2019-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_bench_generic.h"
#include <nm_onesided_interface.h>

static const nm_tag_t tag = 0x42;
static nm_onesided_t p_onesided = NULL;
static uintptr_t rbuf = (uintptr_t)NULL;

static void bench_onesided_get_server(void*p_buf, nm_len_t len)
{
  nm_onesided_request_t req;
  nm_onesided_iget_with_target(p_onesided, nm_bench_common.p_gate, p_buf, len, rbuf, NULL, 0, &req);
  nm_onesided_req_wait(&req);
  nm_onesided_target_request_t target_req;
  nm_onesided_target_irecv(p_onesided, nm_bench_common.p_gate, NULL, 0, &target_req);
  nm_onesided_target_wait(&target_req);
}

static void bench_onesided_get_client(void*p_buf, nm_len_t len)
{
  nm_onesided_target_request_t target_req;
  nm_onesided_target_irecv(p_onesided, nm_bench_common.p_gate, NULL, 0, &target_req);
  nm_onesided_target_wait(&target_req);
  nm_onesided_request_t req;
  nm_onesided_iget_with_target(p_onesided, nm_bench_common.p_gate, p_buf, len, rbuf, NULL, 0, &req);
  nm_onesided_req_wait(&req);
}

static void bench_onesided_get_init(void*p_buf, nm_len_t len)
{
  if(p_onesided == NULL)
    {
      nm_onesided_init(nm_bench_common.p_session, &p_onesided);
    }
  const uintptr_t buf = (uintptr_t)p_buf;
  nm_sr_send(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &buf, sizeof(uintptr_t));
  nm_sr_recv(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &rbuf, sizeof(uintptr_t));
}

const struct nm_bench_s nm_bench =
  {
    .name   = "onesided interface - get with target-side completion",
    .server = &bench_onesided_get_server,
    .client = &bench_onesided_get_client,
    .init   = &bench_onesided_get_init
  };
