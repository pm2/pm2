/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* This file is an example / test to receive and delay a first RPC request,
 * receive a second RPC request, and then complete the first one. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

const char msg_a[] = "Hello, world A!";
const char msg_b[] = "Hello, world B!";
const nm_tag_t tag = 0x02;

static volatile int handler_called = 0;
static volatile int finalized = 0;
static nm_rpc_service_t rpc_service;
static nm_cond_status_t cond_finalized;

struct delayed_req {
  nm_len_t rlen;
  nm_rpc_token_t token;
};

struct delayed_req delayed_req;
char* buf_second;

/** handler called upon incoming RPC request */
static void rpc_hello_handler(nm_rpc_token_t p_token)
{
  nm_len_t rlen = NM_LEN_UNDEFINED;
  nm_rpc_recv_header(p_token, &rlen, sizeof(rlen));

  if(handler_called == 0)
    {
      // First request, we delay it:
      delayed_req.rlen = rlen;
      delayed_req.token = p_token;
      nm_rpc_token_delay(p_token);
    }
  else
    {
      // Second request, we receive it immediately:
      buf_second = malloc(rlen);
      nm_rpc_token_set_ref(p_token, buf_second);
      nm_rpc_irecv_body(p_token, buf_second, rlen);
    }

  handler_called++;
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_hello_finalizer(nm_rpc_token_t p_token)
{
  nm_cond_signal(&cond_finalized, ++finalized);
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_hello");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (1 - rank) : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_hello");

  delayed_req.rlen = NM_LEN_UNDEFINED;
  delayed_req.token = NULL;
  buf_second = NULL;

  /* register the RPC service */
  rpc_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL, &rpc_hello_handler, &rpc_hello_finalizer, NULL);

  nm_len_t len = strlen(msg_a) + 1; /* header */

  if(rank == 0)
    {
      // First RPC request:
      nm_rpc_req_t p_req = nm_rpc_req_init(rpc_service, p_gate, tag);
      nm_rpc_req_pack_header(p_req, &len, sizeof(len));
      nm_rpc_req_pack_body(p_req, msg_a, len);
      nm_rpc_req_isend(p_req);

      // Second RPC request:
      nm_rpc_req_t p_req2 = nm_rpc_req_init(rpc_service, p_gate, tag);
      nm_rpc_req_pack_header(p_req2, &len, sizeof(len));
      nm_rpc_req_pack_body(p_req2, msg_b, len);
      nm_rpc_req_isend(p_req2);
    }
  else
    {
      nm_cond_init(&cond_finalized, 0);

      // Wait for the end of a RPC request (the second one, since the first was delayed):
      nm_cond_wait(&cond_finalized, 1, nm_core_get_singleton());

      assert(buf_second != NULL);
      if(strcmp(buf_second, msg_b))
        {
          printf("Received: '%s' (should be '%s')\n", buf_second, msg_b);
        }

      // Now complete the first RPC request, which was delayed:
      assert(delayed_req.rlen > 0);
      char*buf_first = malloc(delayed_req.rlen);
      nm_rpc_irecv_body(delayed_req.token, buf_first, delayed_req.rlen);
      nm_rpc_token_complete(delayed_req.token);

      nm_cond_wait(&cond_finalized, 2, nm_core_get_singleton());

      if(strcmp(buf_first, msg_a))
        {
          printf("Received: '%s' (should be '%s')\n", buf_first, msg_a);
        }

      free(buf_first);
      free(buf_second);
      nm_cond_destroy(&cond_finalized);
    }

  nm_coll_barrier(p_comm, 0xF2);

  printf("success\n");

  /* generic nmad termination */
  nm_rpc_unregister(rpc_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}
