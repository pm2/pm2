/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file test for RPC with delayed body receive (not posted in handler)
 * using nm_rpc_token_delay()
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_core_interface.h>
#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

const nm_tag_t tag = 0x42;
#define MAXLEN (1024 * 1024)
#define NLOOPS 1000

struct rpc_delayed_packet_s
{
  nm_len_t len;
  nm_rpc_token_t p_token;
  nm_cond_status_t received;
};

static struct
{
  nm_spinlock_t lock;
  int next_pkt;
  struct rpc_delayed_packet_s*packets;
} rpc_delayed;

/** handler called upon incoming RPC request */
static void rpc_delayed_handler(nm_rpc_token_t p_token)
{
  nm_spin_lock(&rpc_delayed.lock);
  assert(rpc_delayed.next_pkt < NLOOPS);
  struct rpc_delayed_packet_s*p_pkt = &rpc_delayed.packets[rpc_delayed.next_pkt];
  rpc_delayed.next_pkt++;
  p_pkt->p_token = p_token;
  assert(!nm_cond_test(&p_pkt->received, 0x01));
  nm_spin_unlock(&rpc_delayed.lock);
  nm_rpc_token_set_ref(p_token, p_pkt);
  nm_rpc_recv_header(p_token, &p_pkt->len, sizeof(nm_len_t)); /* receive header */
  nm_rpc_token_delay(p_token); /* not ready to receive body yet */
  nm_cond_signal(&p_pkt->received, 0x01); /* signal we received header */
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_delayed_finalizer(nm_rpc_token_t p_token)
{
  struct rpc_delayed_packet_s*p_pkt = nm_rpc_token_get_ref(p_token);
  nm_cond_signal(&p_pkt->received, 0x02);
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_delayed");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (1 - rank) : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_delayed");
  char*buf = malloc(MAXLEN);

  /* register the RPC service */
  nm_rpc_service_t rpc_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL,
                                                 &rpc_delayed_handler, &rpc_delayed_finalizer, NULL);

  nm_len_t len = 0;
  for(len = 1; len < MAXLEN; len *= 2)
    {
      printf("starting len = %ld\n", len);
      nm_coll_barrier(p_comm, 0xF1);
      nm_spin_init(&rpc_delayed.lock);
      rpc_delayed.next_pkt = 0;
      rpc_delayed.packets = malloc(sizeof(struct rpc_delayed_packet_s) * NLOOPS);
      int i;
      for(i = 0; i < NLOOPS; i++)
        {
          rpc_delayed.packets[i].len = NM_LEN_UNDEFINED;
          rpc_delayed.packets[i].p_token = NULL;
          nm_cond_init(&rpc_delayed.packets[i].received, 0);
        }
      nm_coll_barrier(p_comm, 0xF2);
      for(i = 0; i < NLOOPS; i++)
        {
          if(rank == 0)
            {
              /* create rpc request */
              nm_rpc_req_t p_req = nm_rpc_req_init(rpc_service, p_gate, tag);
              nm_rpc_req_pack_header(p_req, &len, sizeof(len));
              nm_rpc_req_pack_body(p_req, buf, len);
              nm_rpc_req_isend(p_req);
              nm_rpc_req_wait(p_req);
            }
          else
            {
              struct rpc_delayed_packet_s*p_pkt = &rpc_delayed.packets[i];
              nm_cond_wait(&p_pkt->received, 0x01, nm_core_get_singleton()); /* wait for header */
              assert(p_pkt->p_token != NULL);
              assert(!nm_cond_test(&p_pkt->received, 0x02)); /* ensure pkt not already received */
              nm_rpc_irecv_body(p_pkt->p_token, buf, p_pkt->len); /* post recv for body */
              nm_rpc_token_complete(p_pkt->p_token); /* signal all recvs are posted for this token */
              nm_cond_wait(&p_pkt->received, 0x02, nm_core_get_singleton()); /* wait for body */
            }
        }
      free(rpc_delayed.packets);
      rpc_delayed.packets = NULL;
    }

  free(buf);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF3);
  nm_rpc_unregister(rpc_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}
