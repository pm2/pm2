/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

PADICO_MODULE_ATTR(trace, "NMAD_TRACE", "Control the content of traces; default is: all,^core",
                   string, "all,^core");

PADICO_MODULE_ATTR(trace_suffix, "NMAD_TRACE_SUFFIX", "Add a suffix to the default trace file name",
                   string, "");

PADICO_MODULE_ATTR(trace_path, "NMAD_TRACE_PATH", "Directory where to save the trace file; default is '.'",
                   string, ".");

PADICO_MODULE_ATTR(trace_max, "NMAD_TRACE_MAX", "Maximum number of trace events; default is 8000000",
                   unsigned, 8000000);

PADICO_MODULE_ATTR(nmad_trace_reduced, "NMAD_TRACE_REDUCED", "Reduce the number of containers in traces",
                   unsigned, 0);

#ifdef NMAD_TRACE

#include <nm_coll_interface.h>
#include <nm_sync_clocks_interface.h>
#include <stdio.h>
#include <string.h>
#ifdef NMAD_ENABLE_GTG
#include <GTG.h>
#endif /* NMAD_ENABLE_GTG */
#ifdef NMAD_ENABLE_PALLAS
#include <pallas/pallas.h>
#include <pallas/pallas_archive.h>
#include <pallas/pallas_write.h>
#include <pallas/pallas_record.h>
#endif /* NMAD_ENABLE_PALLAS */


PUK_VECT_TYPE(nm_trace_object_container, uintptr_t);

/** a trace event, as captured */
struct nm_trace_capture_entry_s
{
  double local_time;       /**< time of event in local time */
  nm_trace_event_t event;
  uintptr_t value;
  nm_gate_t p_gate;
  nm_drv_t p_drv;
  nm_core_tag_t core_tag;
  nm_seq_t seq;
  nm_len_t len;            /**< length of data in request; NM_LEN_UNDEFINED if unknown */
};

/** a packed trace event */
struct nm_trace_packed_entry_s
{
  double global_time;
  nm_trace_event_t event;
  uintptr_t value;
  int gate_id;
  int driver_id;
  nm_core_tag_t core_tag;
  nm_seq_t seq;
  nm_len_t len;
};

struct nm_trace_packed_s
{
  struct nm_trace_packed_entry_s*p_entries;
  int n_entries;
  char**p_drivers;
  int n_drivers;
  int n_gates;
  int rank;
};

static struct
{
  nm_sync_clocks_t p_clocks;
  int tracing;   /**< whether we are currently tracing */
  struct nm_trace_capture_entry_s*p_entries;
  unsigned trace_max;
  unsigned nmad_trace_reduced;
  int last;      /**< last allocated entry in the table */
  int n_gates;   /**< number of gates; 0 .. n_gates - 1 */
  nm_comm_t p_comm;
  int rank;
  int size;
  int filters;
} nm_trace = { .p_clocks = NULL, .tracing = 0, .trace_max = -1, .last = 0, .filters = 0 };


/* ** trace capture **************************************** */

static struct nm_trace_capture_entry_s*nm_trace_get_entry(void)
{
  if(nm_trace.tracing)
    {
      const int i = __sync_fetch_and_add(&nm_trace.last, 1);
      assert(nm_trace.trace_max > 0);
      if(i >= nm_trace.trace_max)
        {
          nm_trace.tracing = 0;
          nm_trace.last = nm_trace.trace_max;
          NM_WARN("trace buffer full. Stop capturing traces.\n");
          return NULL;
        }
      return &nm_trace.p_entries[i];
    }
  else
    {
      return NULL;
    }
}


void nm_trace_event(nm_trace_event_t event, void*value, nm_gate_t p_gate, nm_drv_t p_drv, nm_core_tag_t core_tag, nm_seq_t seq, nm_len_t len)
{
  if((event & nm_trace.filters) != 0)
    {
      struct nm_trace_capture_entry_s*p_entry = nm_trace_get_entry();
      if(p_entry)
        {
          p_entry->local_time = nm_sync_clocks_get_time_usec(nm_trace.p_clocks);
          p_entry->event  = event;
          p_entry->value  = (uintptr_t)value;
          p_entry->p_gate = p_gate;
          p_entry->p_drv  = p_drv;
          p_entry->core_tag = core_tag;
          p_entry->seq    = seq;
          p_entry->len    = len;
        }
    }
}

void nm_trace_state(nm_trace_event_t event, nm_gate_t p_gate, nm_drv_t p_drv)
{
  nm_trace_event(event, NULL, p_gate, p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
}

void nm_trace_var(nm_trace_event_t event, int _value, nm_gate_t p_gate, nm_drv_t p_drv)
{
  uintptr_t value = (uintptr_t)_value;
  nm_trace_event(event, (void*)value, p_gate, p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
}

void nm_trace_add_synchro_point(void)
{
  nm_trace_event(NM_TRACE_EVENT_CLOCK_SYNCHRONIZE, NULL, NM_GATE_NONE, NULL, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
}


static struct nm_trace_packed_s*nm_trace_pack(nm_core_t p_core)
{
  struct nm_trace_packed_s*p_packed = padico_malloc(sizeof(struct nm_trace_packed_s));
  p_packed->rank = nm_trace.rank;
  p_packed->p_entries = padico_malloc(sizeof(struct nm_trace_packed_entry_s) * nm_trace.last);
  p_packed->n_entries = nm_trace.last;
  p_packed->n_gates = nm_trace.size;
  p_packed->n_drivers = 0;
  p_packed->p_drivers = NULL;
  nm_drv_t p_drv;
  NM_FOR_EACH_DRIVER(p_drv, p_core)
    {
      p_packed->n_drivers++;
      p_packed->p_drivers = realloc(p_packed->p_drivers, p_packed->n_drivers * sizeof(char*));
      p_packed->p_drivers[p_packed->n_drivers - 1] = padico_strdup(p_drv->props.nickname);
    }
  int i;
  for(i = 0; i < nm_trace.last; i++)
    {
      struct nm_trace_packed_entry_s*p = &p_packed->p_entries[i];
      const struct nm_trace_capture_entry_s*c = &nm_trace.p_entries[i];
      p->global_time = nm_sync_clocks_local_to_global(nm_trace.p_clocks, c->local_time);
      p->event = c->event;
      p->value = c->value;
      if(c->p_gate != NULL)
        {
          p->gate_id = nm_comm_get_dest(nm_trace.p_comm, c->p_gate);
          if(p->gate_id == -1)
            {
              NM_WARN("failed to resolve p_gate = %p as a rank; event = 0x%x.\n", c->p_gate, c->event);
            }
        }
      else
        {
          p->gate_id = -1;
        }
      p->driver_id = -1;
      if(c->p_drv != NULL)
        {
          int d;
          for(d = 0; d < p_packed->n_drivers; d++)
            {
              if(strcmp(p_packed->p_drivers[d], c->p_drv->props.nickname) == 0)
                {
                  p->driver_id = d;
                  break;
                }
            }
        }
      p->core_tag = c->core_tag;
      p->seq = c->seq;
      p->len = (c->len == NM_LEN_UNDEFINED) ? -1 : c->len;
    }
  return p_packed;
}

static void nm_trace_packed_free(struct nm_trace_packed_s*p_packed)
{
  int d;
  for(d = 0; d < p_packed->n_drivers; d++)
    {
      padico_free( p_packed->p_drivers[d]);
    }
  padico_free(p_packed->p_entries);
  padico_free(p_packed->p_drivers);
  padico_free(p_packed);
}

static void nm_trace_packed_send(struct nm_trace_packed_s*p_packed, nm_gate_t p_gate)
{
  nm_session_t p_session = nm_comm_get_session(nm_trace.p_comm);
  nm_sr_send(p_session, p_gate, 0, p_packed, sizeof(struct nm_trace_packed_s));
  int i;
  for(i = 0; i < p_packed->n_drivers; i++)
    {
      nm_sr_send(p_session, p_gate, 0, p_packed->p_drivers[i], strlen(p_packed->p_drivers[i]) + 1);
    }
  nm_sr_send(p_session, p_gate, 0, p_packed->p_entries, p_packed->n_entries * sizeof(struct nm_trace_packed_entry_s));
}

static struct nm_trace_packed_s*nm_trace_packed_recv(nm_gate_t p_gate)
{
  struct nm_trace_packed_s*p_packed = padico_malloc(sizeof(struct nm_trace_packed_s));
  nm_session_t p_session = nm_comm_get_session(nm_trace.p_comm);
  nm_sr_recv(p_session, p_gate, 0, p_packed, sizeof(struct nm_trace_packed_s));
  p_packed->p_drivers = padico_malloc(sizeof(char*) * p_packed->n_drivers);
  int i;
  for(i = 0; i < p_packed->n_drivers; i++)
    {
      nm_sr_request_t req;
      nm_sr_recv_init(p_session, &req);
      nm_sr_recv_match(p_session, &req, p_gate, 0, NM_TAG_MASK_FULL);
      nm_sr_recv_post(p_session, &req);
      nm_sr_recv_data_size_wait(p_session, &req);
      nm_len_t out_len;
      nm_sr_request_get_expected_size(&req, &out_len);
      p_packed->p_drivers[i] = padico_malloc(out_len);
      nm_sr_recv_unpack_contiguous(p_session, &req, p_packed->p_drivers[i], out_len);
    }
  p_packed->p_entries = padico_malloc(sizeof(struct nm_trace_packed_entry_s) * p_packed->n_entries);
  nm_sr_recv(p_session, p_gate, 0, p_packed->p_entries, p_packed->n_entries * sizeof(struct nm_trace_packed_entry_s));
  return p_packed;
}

/* ** trace backends *************************************** */

struct nm_trace_gen_rgb_s
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
};
#define NM_TRACE_GEN_RGB(R, G, B) ((struct nm_trace_gen_rgb_s){ .r = R, .g = G, .b = B})
#define NM_TRACE_GEN_BLACK      NM_TRACE_GEN_RGB(0, 0, 0)
#define NM_TRACE_GEN_GREY       NM_TRACE_GEN_RGB(85, 85, 85)
#define NM_TRACE_GEN_WHITE      NM_TRACE_GEN_RGB(255, 255, 255)
#define NM_TRACE_GEN_RED        NM_TRACE_GEN_RGB(255, 0, 0)
#define NM_TRACE_GEN_GREEN      NM_TRACE_GEN_RGB(0, 255, 0)
#define NM_TRACE_GEN_BLUE       NM_TRACE_GEN_RGB(0, 0, 255)
#define NM_TRACE_GEN_LIGHTBROWN NM_TRACE_GEN_RGB(170, 130, 130)
#define NM_TRACE_GEN_PINK       NM_TRACE_GEN_RGB(255, 0, 255)
#define NM_TRACE_GEN_ORANGE     NM_TRACE_GEN_RGB(255, 160, 0)
#define NM_TRACE_GEN_YELLOW     NM_TRACE_GEN_RGB(255, 255, 0)
#define NM_TRACE_GEN_TEAL       NM_TRACE_GEN_RGB(0, 255, 255)
#define NM_TRACE_GEN_PURPLE     NM_TRACE_GEN_RGB(153, 25, 230)

#if defined(NMAD_ENABLE_GTG)

/* ** GTG */

static inline void nm_gtg_dict_destructor(char*p_key, char*p_value)
{
  padico_free(p_key);
  padico_free(p_value);
}

PUK_HASHTABLE_TYPE(nm_gtg_dict, char*, char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &nm_gtg_dict_destructor);

static struct
{
  struct nm_gtg_dict_hashtable_s dict; /**< dictionnary to translate full name to short names */
} nm_gtg;

static inline const char*nm_gtg_symbol(const char*p_name)
{
  const char*p_val = nm_gtg_dict_hashtable_lookup(&nm_gtg.dict, p_name);
  if(p_val)
    {
      return p_val;
    }
  else
    {
      NM_WARN("unknown symbol %s\n", p_name);
      return p_name;
    }
}

static inline void nm_gtg_dict_insert(const char*p_name, const char*p_short_name)
{
  if(nm_gtg_dict_hashtable_probe(&nm_gtg.dict, p_name))
    {
      NM_FATAL("duplicate entry in GTG dictionnary: %s\n", p_name);
    }
  nm_gtg_dict_hashtable_insert(&nm_gtg.dict, padico_strdup(p_name), padico_strdup(p_short_name));
}

static void nm_trace_gen_init(const char*trace_name)
{
  setTraceType(PAJE);
  initTrace(trace_name, 0, GTG_FLAG_NONE);
  nm_gtg_dict_hashtable_init(&nm_gtg.dict);
}

static void nm_trace_gen_end(void)
{
  endTrace();
  nm_gtg_dict_hashtable_destroy(&nm_gtg.dict);
}

static void nm_trace_gen_comment(const char*p_text)
{
  AddComment(p_text);
}

static void nm_trace_gen_container_type(const char*p_name, const char*p_short_name, const char*p_parent_type)
{
  if(p_parent_type == NULL) /* root container */
    p_parent_type = "0";
  addContType(p_short_name, p_parent_type, p_name);
  nm_gtg_dict_insert(p_name, p_short_name);
}

static void nm_trace_gen_container_add(double t, const char*p_name, const char*p_short_name, const char*p_type,
                                       const char*p_parent, int leaf)
{
  addContainer(t, p_short_name, p_type, p_parent, p_name, p_parent?:"0");
  nm_gtg_dict_insert(p_name, p_short_name);
}

static void nm_trace_gen_container_add_string(double t, padico_string_t name, padico_string_t short_name,
                                              padico_string_t parent, const char*p_type, int leaf)
{
  addContainer(t, padico_string_get(short_name), p_type, padico_string_get(parent), padico_string_get(name), "0");
  nm_gtg_dict_insert(padico_string_get(name), padico_string_get(short_name));
  padico_string_delete(name);
  padico_string_delete(short_name);
  padico_string_delete(parent);
}

static void nm_trace_gen_link_type(const char*p_name, const char*p_short_name,
                                   const char*p_cont_type, const char*p_src_type, const char*p_dest_type)
{
  addLinkType(p_short_name, p_name, p_cont_type, p_src_type, p_dest_type);
  nm_gtg_dict_insert(p_name, p_short_name);
}

static void nm_trace_gen_link_start(double t, const char*p_link_type, const char*p_container,
                                    const char*p_src, const char*p_dest, const char*p_value, padico_string_t p_key)
{
  startLink(t, p_link_type, p_container, p_src, p_dest, p_value, padico_string_get(p_key));
  padico_string_delete(p_key);
}

static void nm_trace_gen_link_end(double t, const char*p_link_type, const char*p_container,
                                  const char*p_src, const char*p_dest, const char*p_value, padico_string_t p_key)
{
  endLink(t, p_link_type, p_container, p_src, p_dest, p_value, padico_string_get(p_key));
  padico_string_delete(p_key);
}

static void nm_trace_gen_state_type(const char*p_name, const char*p_short_name, const char*p_cont_type)
{
  addStateType(p_short_name, p_cont_type, p_name);
  nm_gtg_dict_insert(p_name, p_short_name);
}

static void nm_trace_gen_state_set(double t, const char*p_state_type, const char*p_cont_name, const char*p_state_name)
{
  setState(t, nm_gtg_symbol(p_state_type), nm_gtg_symbol(p_cont_name), nm_gtg_symbol(p_state_name));
}

static void nm_trace_gen_entity_declare(const char*p_name, const char*p_short_name, const char*p_cont_type, struct nm_trace_gen_rgb_s rgb)
{
  padico_string_t s_color = padico_string_new_printf("%x%x%x", rgb.r, rgb.g, rgb.b);
  gtg_color_t gtg_color = gtg_color_create(padico_string_get(s_color), rgb.r, rgb.g, rgb.b);
  addEntityValue(p_short_name, nm_gtg_symbol(p_cont_type), p_name, gtg_color);
  nm_gtg_dict_insert(p_name, p_short_name);
  gtg_color_free(gtg_color);
  padico_string_delete(s_color);
}

static void nm_trace_gen_var_type(const char*p_name, const char*p_short_name, const char*p_cont_type)
{
  addVarType(p_short_name, p_name, p_cont_type);
  nm_gtg_dict_insert(p_name, p_short_name);
}

static void nm_trace_gen_var_set(double t, const char*p_var_name, const char*p_cont_name, int value)
{
  setVar(t, nm_gtg_symbol(p_var_name), nm_gtg_symbol(p_cont_name), value);
}

#elif defined(NMAD_ENABLE_PALLAS)

/* ** Pallas */

static inline void nm_pallas_stringref_destructor(char*p_key, StringRef ref)
{
  padico_free(p_key);
}
static inline void nm_pallas_locationgroup_destructor(char*p_key, LocationGroupId id)
{
  padico_free(p_key);
}
static inline void nm_pallas_location_destructor(char*p_key, ThreadId id)
{
  padico_free(p_key);
}

PUK_HASHTABLE_TYPE(nm_pallas_stringref, char*, StringRef,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &nm_pallas_stringref_destructor);

PUK_HASHTABLE_TYPE(nm_pallas_locationgroup, char*, LocationGroupId,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &nm_pallas_locationgroup_destructor);

PUK_HASHTABLE_TYPE(nm_pallas_location, char*, ThreadId,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &nm_pallas_location_destructor);

PUK_VECT_TYPE(nm_pallas_archive, struct Archive*);

PUK_VECT_TYPE(nm_pallas_threadwriter, struct ThreadWriter*);

static struct
{
  struct GlobalArchive*global_archive;
  struct nm_pallas_archive_vect_s archives;            /**< Archives, by LocationGroupId */
  struct nm_pallas_threadwriter_vect_s thread_writers; /**< ThreadWriters, by ThreadId */
  StringRef next_ref;
  struct nm_pallas_stringref_hashtable_s stringrefs;   /**< stringrefs, hashed by string  */
  struct nm_pallas_locationgroup_hashtable_s locationgroups; /**< LocationGroupId, hashed by container name */
  struct nm_pallas_location_hashtable_s locations;
  char*p_trace_name;
} nm_pallas = { .global_archive = NULL, .next_ref = 0, .p_trace_name = NULL };

static inline StringRef nm_pallas_register_string(const char*p_string)
{
  if(nm_pallas_stringref_hashtable_probe(&nm_pallas.stringrefs, p_string))
    {
      return nm_pallas_stringref_hashtable_lookup(&nm_pallas.stringrefs, p_string);
    }
  else
    {
      StringRef ref = nm_pallas.next_ref++;
      pallas_archive_register_string(nm_pallas.global_archive, ref, p_string);
      nm_pallas_stringref_hashtable_insert(&nm_pallas.stringrefs, padico_strdup(p_string), ref);
      return ref;
    }
}

static void nm_trace_gen_init(const char*trace_name)
{
  nm_pallas.p_trace_name = padico_strdup(trace_name);
  nm_pallas_stringref_hashtable_init(&nm_pallas.stringrefs);
  nm_pallas_locationgroup_hashtable_init(&nm_pallas.locationgroups);
  nm_pallas_location_hashtable_init(&nm_pallas.locations);
  nm_pallas_archive_vect_init(&nm_pallas.archives);
  nm_pallas_threadwriter_vect_init(&nm_pallas.thread_writers);
  nm_pallas.global_archive = pallas_global_archive_new();
  pallas_write_global_archive_open(nm_pallas.global_archive, trace_name, "main");
}

static void nm_trace_gen_end(void)
{
  nm_pallas_threadwriter_vect_itor_t t;
  puk_vect_foreach(t, nm_pallas_threadwriter, &nm_pallas.thread_writers)
    {
      pallas_write_thread_close(*t);
    }
  nm_pallas_archive_vect_itor_t a;
  puk_vect_foreach(a, nm_pallas_archive, &nm_pallas.archives)
    {
      pallas_write_archive_close(*a);
    }
  pallas_write_global_archive_close(nm_pallas.global_archive);
  padico_free(nm_pallas.p_trace_name);
}

static void nm_trace_gen_comment(const char*p_text)
{
}

static void nm_trace_gen_container_type(const char*p_name, const char*p_short_name, const char*p_parent_type)
{
}

static void nm_trace_gen_container_add(double t, const char*p_name, const char*p_short_name, const char*p_type,
                                       const char*p_parent, int leaf)
{
  LocationGroupId parent_id;
  if(p_parent == NULL)
    {
      /* no parent; root container */
      parent_id = PALLAS_LOCATION_GROUP_ID_INVALID;
    }
  else
    {
      if(!nm_pallas_locationgroup_hashtable_probe(&nm_pallas.locationgroups, p_parent))
        {
          NM_FATAL("cannot find LocationGroup %s.\n", p_parent);
        }
      parent_id = nm_pallas_locationgroup_hashtable_lookup(&nm_pallas.locationgroups, p_parent);
    }
  if(leaf)
    {
      ThreadId thread_id = nm_pallas_threadwriter_vect_size(&nm_pallas.thread_writers);
      StringRef thread_name = nm_pallas_register_string(p_name);
      pallas_write_define_location(nm_pallas.global_archive, thread_id, thread_name, parent_id);
      LocationGroupId process_id = nm_pallas_locationgroup_hashtable_lookup(&nm_pallas.locationgroups, p_parent);
      assert(process_id >= 0);
      assert(process_id < nm_pallas_archive_vect_size(&nm_pallas.archives));
      struct Archive*archive = nm_pallas_archive_vect_at(&nm_pallas.archives, process_id);
      assert(archive != NULL);
      struct ThreadWriter*thread_writer = pallas_thread_writer_new();
      pallas_write_thread_open(archive, thread_writer, thread_id);
      fprintf(stderr, "# ## create thread '%s'; thread_id = %d; parent = %d\n", p_name, thread_id, parent_id);
      nm_pallas_location_hashtable_insert(&nm_pallas.locations, padico_strdup(p_name), thread_id);
      nm_pallas_threadwriter_vect_push_back(&nm_pallas.thread_writers, thread_writer);
    }
  else
    {
      LocationGroupId process_id = nm_pallas_archive_vect_size(&nm_pallas.archives);
      StringRef process_name = nm_pallas_register_string(p_name);
      struct Archive*archive = pallas_archive_new();
      fprintf(stderr, "# ## create container '%s'; process_id = %d; parent = %d\n", p_name, process_id, parent_id);
      pallas_write_archive_open(archive, nm_pallas.p_trace_name, p_name, process_id);
      pallas_write_define_location_group(nm_pallas.global_archive, process_id, process_name, parent_id);
      nm_pallas_locationgroup_hashtable_insert(&nm_pallas.locationgroups, padico_strdup(p_name), process_id);
      nm_pallas_archive_vect_push_back(&nm_pallas.archives, archive);
    }
}

static void nm_trace_gen_container_add_string(double t, padico_string_t name, padico_string_t short_name,
                                              padico_string_t parent, const char*p_type, int leaf)
{
  nm_trace_gen_container_add(t, padico_string_get(name), padico_string_get(short_name), p_type, padico_string_get(parent), leaf);
  padico_string_delete(name);
  padico_string_delete(short_name);
  padico_string_delete(parent);
}

static void nm_trace_gen_link_type(const char*p_name, const char*p_short_name,
                                   const char*p_cont_type, const char*p_src_type, const char*p_dest_type)
{
}

static void nm_trace_gen_link_start(double t, const char*p_link_type, const char*p_container,
                                    const char*p_src, const char*p_dest, const char*p_value, padico_string_t p_key)
{
  padico_string_delete(p_key);
}

static void nm_trace_gen_link_end(double t, const char*p_link_type, const char*p_container,
                                  const char*p_src, const char*p_dest, const char*p_value, padico_string_t p_key)
{
  padico_string_delete(p_key);
}

static void nm_trace_gen_state_type(const char*p_name, const char*p_short_name, const char*p_cont_type)
{
}

static void nm_trace_gen_state_set(double t, const char*p_state_type, const char*p_cont_name, const char*p_state_name)
{
  if(!nm_pallas_location_hashtable_probe(&nm_pallas.locations, p_cont_name))
    {
      NM_FATAL("cannot find container %s for state %s\n", p_cont_name, p_state_name);
    }
  ThreadId thread_id = nm_pallas_location_hashtable_lookup(&nm_pallas.locations, p_cont_name);
  if(thread_id < 0 || thread_id > nm_pallas_threadwriter_vect_size(&nm_pallas.thread_writers))
    {
      NM_FATAL("bad ThreadId %d\n", thread_id);
    }
  struct ThreadWriter*thread_writer = nm_pallas_threadwriter_vect_at(&nm_pallas.thread_writers, thread_id);
  StringRef event_name = nm_pallas_register_string(p_state_name);
  pallas_timestamp_t time_ns = t * 1000.0;
  pallas_record_generic(thread_writer, NULL /* attribute_list */, time_ns, event_name);
}

static void nm_trace_gen_entity_declare(const char*p_name, const char*p_short_name, const char*p_cont_type, struct nm_trace_gen_rgb_s rgb)
{
  padico_string_t s_color = padico_string_new_printf("%x%x%x", rgb.r, rgb.g, rgb.b);
  padico_string_delete(s_color);
}

static void nm_trace_gen_var_type(const char*p_name, const char*p_short_name, const char*p_cont_type)
{
}

static void nm_trace_gen_var_set(double t, const char*p_var_name, const char*p_cont_name, int value)
{
}

#endif

/* ** trace output ***************************************** */

static nm_trace_object_container_vect_itor_t nm_trace_object_find_slot(struct nm_trace_object_container_vect_s*p_objects, uintptr_t value,
                                                                       int rank, const char*object_type, const char*container_type,
                                                                       padico_string_t parent_container)
{
   nm_trace_object_container_vect_itor_t i = nm_trace_object_container_vect_find(p_objects, value);
   if(i == NULL)
     {
       i = nm_trace_object_container_vect_find(p_objects, (uintptr_t)NULL);
       if(i == NULL)
         {
           padico_string_t cont_obj = padico_string_new_printf("n%d_%s#%d", rank, object_type,
                                                               nm_trace_object_container_vect_size(p_objects));
           nm_trace_gen_container_add_string(0.0, cont_obj, padico_string_dup(cont_obj), padico_string_dup(parent_container), container_type, 1);
           i = nm_trace_object_container_vect_push_back(p_objects, value);
         }
       else
         {
           *i = value;
         }
     }
   return i;
}

static padico_string_t nm_trace_link_key(int from, int to, nm_core_tag_t tag, nm_seq_t seq)
{
  return padico_string_new_printf("from = %d; to = %d; tag = %x:%x; seq = %u",
                                  from, to, tag.hashcode, tag.tag, seq);
}

/** gen trace global header */
static void nm_trace_header(void)
{
  nm_trace.nmad_trace_reduced = padico_module_attr_nmad_trace_reduced_getvalue();
  nm_trace_gen_comment("nmad build string: " NMAD_BUILD_STRING);
  nm_trace_gen_comment("nmad config: " NMAD_CONFIG_STRING);
  nm_trace_gen_comment("NMAD_ROOT = " NMAD_ROOT);

  /* container types */
  nm_trace_gen_container_type("Container_World",  "CWo", NULL);
  nm_trace_gen_container_type("Container_Node",   "CNo", "Container_World");
  nm_trace_gen_container_type("Container_Pack",   "CPa", "Container_Node");
  nm_trace_gen_container_type("Container_Unpack", "CUn", "Container_Node");
  nm_trace_gen_container_type("Container_Core",   "CCo", "Container_Node");
  nm_trace_gen_container_type("Container_Driver", "CDr", "Container_Node");
  nm_trace_gen_container_type("Container_Pw",     "CPw", "Container_Driver");

  /* links */
  nm_trace_gen_link_type("nm_core_pack_link", "nm_core_pack_link", "Container_World", "Container_Node", "Container_Node");
  if(nm_trace.nmad_trace_reduced)
    {
      nm_trace_gen_link_type("nm_rtr_link", "rtr_link", "Container_World", "Container_Node", "Container_Node");
      nm_trace_gen_link_type("nm_rdv_link", "rdv_link", "Container_World", "Container_Node", "Container_Node");
    }

  /* pack */
  nm_trace_gen_state_type("Pack_state", "StP", "Container_Pack");
  nm_trace_gen_entity_declare("pack_submit",    "pa0", "Pack_state", NM_TRACE_GEN_LIGHTBROWN);
  nm_trace_gen_entity_declare("pack_flush",     "pa1", "Pack_state", NM_TRACE_GEN_GREEN);
  nm_trace_gen_entity_declare("pack_rdv",       "pa2", "Pack_state", NM_TRACE_GEN_PINK);
  nm_trace_gen_entity_declare("pack_rtr",       "pa3", "Pack_state", NM_TRACE_GEN_ORANGE);
  nm_trace_gen_entity_declare("pack_in_pw",     "pa4", "Pack_state", NM_TRACE_GEN_BLUE);
  nm_trace_gen_entity_declare("pack_pw_posted", "pa5", "Pack_state", NM_TRACE_GEN_RED);
  nm_trace_gen_entity_declare("pack_completed", "pa6", "Pack_state", NM_TRACE_GEN_BLACK);

  /* unpack */
  nm_trace_gen_state_type("Unpack_state", "StU", "Container_Unpack");
  nm_trace_gen_entity_declare("unpack_submit",      "un0", "Unpack_state", NM_TRACE_GEN_GREEN);
  nm_trace_gen_entity_declare("unpack_rdv",         "un1", "Unpack_state", NM_TRACE_GEN_YELLOW);
  nm_trace_gen_entity_declare("unpack_rtr",         "un2", "Unpack_state", NM_TRACE_GEN_ORANGE);
  nm_trace_gen_entity_declare("unpack_match_first", "un3", "Unpack_state", NM_TRACE_GEN_TEAL);
  nm_trace_gen_entity_declare("unpack_match_last",  "un4", "Unpack_state", NM_TRACE_GEN_BLUE);
  nm_trace_gen_entity_declare("unpack_completed",   "un5", "Unpack_state", NM_TRACE_GEN_BLACK);

  /* core */
  nm_trace_gen_state_type("Core_Task", "StC", "Container_Core");
  nm_trace_gen_entity_declare("core_none",         "co0", "Core_Task", NM_TRACE_GEN_BLACK);
  nm_trace_gen_entity_declare("core_strategy",     "co1", "Core_Task", NM_TRACE_GEN_GREEN);
  nm_trace_gen_entity_declare("core_polling",      "co2", "Core_Task", NM_TRACE_GEN_RED);
  nm_trace_gen_entity_declare("core_task_flush",   "co3", "Core_Task", NM_TRACE_GEN_BLUE);
  nm_trace_gen_entity_declare("core_dispatching",  "co4", "Core_Task", NM_TRACE_GEN_PURPLE);
  nm_trace_gen_entity_declare("core_unpack_next",  "co5", "Core_Task", NM_TRACE_GEN_ORANGE);
  nm_trace_gen_entity_declare("core_completed_pw", "co6", "Core_Task", NM_TRACE_GEN_YELLOW);
  nm_trace_gen_entity_declare("core_pack_submit",  "co7", "Core_Task", NM_TRACE_GEN_WHITE);
  nm_trace_gen_entity_declare("core_handler",      "co8", "Core_Task", NM_TRACE_GEN_PINK);

  nm_trace_gen_var_type("Var_n_packs",   "Vp", "Container_Core");
  nm_trace_gen_var_type("Var_n_unpacks", "Vu", "Container_Core");

  /* pw */
  nm_trace_gen_state_type("Pw_state", "StPw", "Container_Pw");
  nm_trace_gen_entity_declare("pw_send_post",      "pws1", "Pw_state", NM_TRACE_GEN_RED);
  nm_trace_gen_entity_declare("pw_send_complete",  "pws2", "Pw_state", NM_TRACE_GEN_GREY);
  nm_trace_gen_entity_declare("pw_send_destroy",   "pws9", "Pw_state", NM_TRACE_GEN_BLACK);
  nm_trace_gen_entity_declare("pw_recv_ready",     "pwr0", "Pw_state", NM_TRACE_GEN_ORANGE);
  nm_trace_gen_entity_declare("pw_recv_post",      "pwr1", "Pw_state", NM_TRACE_GEN_GREEN);
  nm_trace_gen_entity_declare("pw_recv_complete",  "pwr2", "Pw_state", NM_TRACE_GEN_GREY);
  nm_trace_gen_entity_declare("pw_recv_destroy",   "pwr9", "Pw_state", NM_TRACE_GEN_BLACK);

  /* root container */
  nm_trace_gen_container_add(0.0, "World", "World", "Container_World", NULL, 0);
}

/** write trace for one node.
 */
static void nm_trace_packed_flush(struct nm_trace_packed_s*p_packed)
{
  /* ** create main containers */

  padico_string_t p_container_world  = padico_string_new_printf("World");
  padico_string_t p_container_node   = padico_string_new_printf("Node_%d", p_packed->rank);
  padico_string_t p_container_pack   = padico_string_new_printf("n%d_Pack", p_packed->rank);
  padico_string_t p_container_unpack = padico_string_new_printf("n%d_Unpack", p_packed->rank);
  padico_string_t p_container_core   = padico_string_new_printf("n%d_Core", p_packed->rank);

  /* node */
  nm_trace_gen_container_add_string(0.0, padico_string_dup(p_container_node),
                                    padico_string_new_printf("c_n%d", p_packed->rank),
                                    padico_string_dup(p_container_world),
                                    "Container_Node", 0);

  /* reqs */
  nm_trace_gen_container_add_string(0.0, padico_string_dup(p_container_pack),
                                    padico_string_new_printf("c_n%dp", p_packed->rank),
                                    padico_string_dup(p_container_node),
                                    "Container_Pack", 0);

  struct nm_trace_object_container_vect_s packs;
  nm_trace_object_container_vect_init(&packs);

  nm_trace_gen_container_add_string(0.0, padico_string_dup(p_container_unpack),
                                    padico_string_new_printf("c_n%du", p_packed->rank),
                                    padico_string_dup(p_container_node),
                                    "Container_Unpack", 0);

  struct nm_trace_object_container_vect_s unpacks;
  nm_trace_object_container_vect_init(&unpacks);

  /* core */
  nm_trace_gen_container_add_string(0.0, padico_string_dup(p_container_core),
                                    padico_string_new_printf("c_n%dc", p_packed->rank),
                                    padico_string_dup(p_container_node),
                                    "Container_Core", 1);

  /* drivers & pw */
  struct nm_trace_object_container_vect_s pws[p_packed->n_drivers];
  struct nm_trace_object_container_vect_s pwr[p_packed->n_drivers];
  padico_string_t p_container_drivers[p_packed->n_drivers];
  int d;
  for(d = 0; d < p_packed->n_drivers; d++)
    {
      const char*p_suffix = "";
      int k;
      for(k = 0; k < d; k++)
        {
          if(strcmp(p_packed->p_drivers[d], p_packed->p_drivers[k]) == 0)
            {
              /* multiple drivers with the same name */
              p_suffix = "#2";
            }
        }
      p_container_drivers[d] = padico_string_new_printf("n%d_Driver_%s%s", p_packed->rank, p_packed->p_drivers[d], p_suffix);
      nm_trace_gen_container_add_string(0.0, padico_string_dup(p_container_drivers[d]),
                                        padico_string_new_printf("c_n%d_%s", p_packed->rank, p_packed->p_drivers[d]),
                                        padico_string_dup(p_container_node),
                                        "Container_Driver", 0);
      nm_trace_object_container_vect_init(&pws[d]);
      nm_trace_object_container_vect_init(&pwr[d]);
    }


  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  /* ** write entries */
  int i;
  for(i = 0; i < p_packed->n_entries; i++)
    {
      /* ** compute value & container */
      const struct nm_trace_packed_entry_s* e = &p_packed->p_entries[i];
      char container[128];
      padico_string_t v = padico_string_new();
      switch(e->event)
        {
          /* pw-related events */
        case NM_TRACE_EVENT_PW_RECV_PENDING:
        case NM_TRACE_EVENT_PW_SEND_POST:
        case NM_TRACE_EVENT_PW_RECV_POST:
        case NM_TRACE_EVENT_PW_SEND_POLL:
        case NM_TRACE_EVENT_PW_SEND_DESTROY:
        case NM_TRACE_EVENT_PW_RECV_POLL:
        case NM_TRACE_EVENT_PW_SEND_COMPLETE:
        case NM_TRACE_EVENT_PW_RECV_COMPLETE:
        case NM_TRACE_EVENT_PW_RECV_DESTROY:
          {
            padico_string_printf(v, "p_pw = %p", e->value);
            if( (e->driver_id >= 0) &&
                (e->driver_id < p_packed->n_drivers) )
              {
                nm_trace_object_container_vect_itor_t i;
                if((e->event & NM_TRACE_FILTER_PW_SEND) == NM_TRACE_FILTER_PW_SEND)
                  {
                    padico_string_t p_token = padico_string_new_printf("d%d_pws", e->driver_id);
                    i = nm_trace_object_find_slot(&pws[e->driver_id], e->value,
                                                  p_packed->rank, padico_string_get(p_token), "Container_Pw", p_container_drivers[e->driver_id]);
                    sprintf(container, "n%d_%s#%d", p_packed->rank, padico_string_get(p_token), nm_trace_object_container_vect_rank(&pws[e->driver_id], i));
                    padico_string_delete(p_token);
                  }
                else if((e->event & NM_TRACE_FILTER_PW_RECV) == NM_TRACE_FILTER_PW_RECV)
                  {
                    padico_string_t p_token = padico_string_new_printf("d%d_pwr", e->driver_id);
                    i = nm_trace_object_find_slot(&pwr[e->driver_id], e->value,
                                                  p_packed->rank, padico_string_get(p_token), "Container_Pw", p_container_drivers[e->driver_id]);
                    sprintf(container, "n%d_%s#%d", p_packed->rank, padico_string_get(p_token), nm_trace_object_container_vect_rank(&pwr[e->driver_id], i));
                    padico_string_delete(p_token);
                  }
                else
                  {
                    NM_FATAL("pw event 0x%x is neither send nor recv.\n", e->event);
                  }
                if( (e->event == NM_TRACE_EVENT_PW_SEND_DESTROY) ||
                    (e->event == NM_TRACE_EVENT_PW_RECV_DESTROY) )
                  {
                    *i = (uintptr_t)NULL;
                  }
              }
            else
              {
                NM_FATAL("no driver for pw event 0x%x\n", e->event);
              }
          }
          break;
        case NM_TRACE_EVENT_CORE_PACK_RTR:
          {
            if(nm_trace.nmad_trace_reduced)
              sprintf(container, padico_string_get(p_container_pack));
          }
          break;
        case NM_TRACE_EVENT_CORE_PACK_RDV:
        case NM_TRACE_EVENT_CORE_PACK_PW_POSTED:
          {
            // If nmad_trace_reduced we always use the same container otherwise we use the same code as for the other PACK cases.
            if(nm_trace.nmad_trace_reduced)
            {
              sprintf(container, padico_string_get(p_container_pack));
              break;
            }
          }
        case NM_TRACE_EVENT_CORE_PACK_SUBMIT:
        case NM_TRACE_EVENT_CORE_PACK_FLUSH:
        case NM_TRACE_EVENT_CORE_PACK_IN_PW:
        case NM_TRACE_EVENT_CORE_PACK_COMPLETED:
          {
            if(!nm_trace.nmad_trace_reduced)
            {
              nm_trace_object_container_vect_itor_t i = nm_trace_object_find_slot(&packs, e->value,
                                                                                  p_packed->rank, "Packs", "Container_Pack", p_container_pack);
              padico_string_printf(v, "gate = %d; driver = %s; tag = %x:%x; len = %lld",
                                   e->gate_id, ((e->driver_id >= 0) ? p_packed->p_drivers[e->driver_id] : "none"),
                                   e->core_tag.hashcode, e->core_tag.tag, e->len);
              sprintf(container, "n%d_Packs#%d", p_packed->rank, nm_trace_object_container_vect_rank(&packs, i));
              if(e->event == NM_TRACE_EVENT_CORE_PACK_COMPLETED)
                {
                  *i = (uintptr_t)NULL;
                }
            }
          }
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_RDV:
        case NM_TRACE_EVENT_CORE_UNPACK_RTR:
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST:
        // If nmad_trace_reduced we always use the same container otherwise we use the same code as for the other UNPACK cases.
            if(nm_trace.nmad_trace_reduced)
            {
              sprintf(container, padico_string_get(p_container_unpack));
              break;
            }
        case NM_TRACE_EVENT_CORE_UNPACK_SUBMIT:
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST:
        case NM_TRACE_EVENT_CORE_UNPACK_COMPLETED:
          {
            if(!nm_trace.nmad_trace_reduced)
            {
              nm_trace_object_container_vect_itor_t i = nm_trace_object_find_slot(&unpacks, e->value,
                                                                                  p_packed->rank, "Unpacks", "Container_Unpack", p_container_unpack);
              padico_string_printf(v, "gate = %d; driver = %s; tag = %x:%x; len = %lld",
                                   e->gate_id, ((e->driver_id >= 0) ? p_packed->p_drivers[e->driver_id] : "none"),
                                   e->core_tag.hashcode, e->core_tag.tag, e->len);
              sprintf(container, "n%d_Unpacks#%d", p_packed->rank, nm_trace_object_container_vect_rank(&unpacks, i));

              if(e->event == NM_TRACE_EVENT_CORE_UNPACK_COMPLETED)
                {
                  *i = (uintptr_t)NULL;
                }
            }
          }
          break;
        case NM_TRACE_EVENT_VAR_N_PACKS:
        case NM_TRACE_EVENT_VAR_N_UNPACKS:
          sprintf(container, padico_string_get(p_container_core));
          break;
        case NM_TRACE_STATE_CORE_NONE:
        case NM_TRACE_STATE_CORE_STRATEGY:
        case NM_TRACE_STATE_CORE_POLLING:
        case NM_TRACE_STATE_CORE_TASK_FLUSH:
        case NM_TRACE_STATE_CORE_DISPATCHING:
        case NM_TRACE_STATE_CORE_UNPACK_NEXT:
        case NM_TRACE_STATE_CORE_COMPLETED_PW:
        case NM_TRACE_STATE_CORE_PACK_SUBMIT:
        case NM_TRACE_STATE_CORE_HANDLER:
          sprintf(container, padico_string_get(p_container_core));
          break;

        default:
          sprintf(container, padico_string_get(p_container_node));
          break;
        }

      /* ** generate event */
      const char*value = padico_string_get(v);
      switch(e->event)
        {
        case NM_TRACE_EVENT_CONNECT:
          break;
        case NM_TRACE_EVENT_DISCONNECT:
          break;

          /* core pack / unpack */
        case NM_TRACE_EVENT_CORE_PACK_SUBMIT:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_submit");
          break;
        case NM_TRACE_EVENT_CORE_PACK_FLUSH:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_flush");
          break;
        case NM_TRACE_EVENT_CORE_PACK_RDV:
          {
          if(nm_trace.nmad_trace_reduced)
            {
              padico_string_t target_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
              padico_string_t link_key = nm_trace_link_key(p_packed->rank, e->gate_id, e->core_tag, e->seq);
              nm_trace_gen_link_start(e->global_time, "nm_rdv_link", "World", container, padico_string_get(target_container),
                        "nm_rdv_link", link_key);
              padico_string_delete(target_container);
            }
          else
          {
            nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_rdv");
          }
          break;
          }
        case NM_TRACE_EVENT_CORE_PACK_RTR:
          {
            if(nm_trace.nmad_trace_reduced)
            {
              padico_string_t src_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
              padico_string_t link_key = nm_trace_link_key(e->gate_id, p_packed->rank, e->core_tag, e->seq);
              nm_trace_gen_link_end(e->global_time, "nm_rtr_link", "World", padico_string_get(src_container), container,
                      "nm_rtr_link", link_key);
              padico_string_delete(src_container);
            }
            break;
          }
        case NM_TRACE_EVENT_CORE_PACK_IN_PW:
            if(!nm_trace.nmad_trace_reduced)
            {
              nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_rtr");
            }
          break;
        case NM_TRACE_EVENT_CORE_PACK_PW_POSTED:
          {
            nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_pw_posted");
            const char*p_value = nm_trace.nmad_trace_reduced ? "nm_core_pack_link" : value;
            padico_string_t target_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
            padico_string_t link_key = nm_trace_link_key(p_packed->rank, e->gate_id, e->core_tag, e->seq);
            nm_trace_gen_link_start(e->global_time, "nm_core_pack_link", "World", container, padico_string_get(target_container),
                                    p_value, link_key);
            padico_string_delete(target_container);
          }
          break;
        case NM_TRACE_EVENT_CORE_PACK_COMPLETED:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Pack_state", container, "pack_completed");
          break;

        case NM_TRACE_EVENT_CORE_UNPACK_SUBMIT:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_submit");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_RDV:
          {
          if(nm_trace.nmad_trace_reduced)
            {
              padico_string_t src_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
              padico_string_t link_key = nm_trace_link_key(e->gate_id, p_packed->rank, e->core_tag, e->seq);
              nm_trace_gen_link_end(e->global_time, "nm_rdv_link", "World", padico_string_get(src_container), container,
                      "nm_rdv_link", link_key);
              padico_string_delete(src_container);
            }
          else
          {
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_rdv");
          }
          break;
          }
        case NM_TRACE_EVENT_CORE_UNPACK_RTR:
        if(nm_trace.nmad_trace_reduced)
          {
            padico_string_t target_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
            padico_string_t link_key = nm_trace_link_key(p_packed->rank, e->gate_id, e->core_tag, e->seq);
            nm_trace_gen_link_start(e->global_time, "nm_rtr_link", "World", container, padico_string_get(target_container),
                      "nm_rtr_link", link_key);
            padico_string_delete(target_container);
          }
          else
          {
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_rtr");
          }
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST:
          {
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_match_first");
            if(e->gate_id != -1)
              {
                const char*p_value = nm_trace.nmad_trace_reduced ? "nm_core_pack_link" : value;
                padico_string_t src_container = padico_string_new_printf("n%d_Gate_%d\n", e->gate_id, p_packed->rank);
                padico_string_t link_key = nm_trace_link_key(e->gate_id, p_packed->rank, e->core_tag, e->seq);
                nm_trace_gen_link_end(e->global_time, "nm_core_pack_link", "World", padico_string_get(src_container), container,
                        p_value, link_key);
                padico_string_delete(src_container);
              }
          }
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_match_last");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_COMPLETED:
          if(!nm_trace.nmad_trace_reduced)
            nm_trace_gen_state_set(e->global_time, "Unpack_state", container, "unpack_completed");
          break;

        case NM_TRACE_EVENT_VAR_N_PACKS:
          nm_trace_gen_var_set(e->global_time, "Var_n_packs", container, (int)e->value);
          break;
        case NM_TRACE_EVENT_VAR_N_UNPACKS:
          nm_trace_gen_var_set(e->global_time, "Var_n_unpacks", container, (int)e->value);
          break;

          /* drivers */
        case NM_TRACE_EVENT_PW_SEND_POST:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_send_post");
          break;
        case NM_TRACE_EVENT_PW_SEND_COMPLETE:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_send_complete");
          break;
        case NM_TRACE_EVENT_PW_SEND_DESTROY:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_send_destroy");
          break;

        case NM_TRACE_EVENT_PW_RECV_PENDING:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_recv_ready");
          break;
        case NM_TRACE_EVENT_PW_RECV_POST:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_recv_post");
          break;
        case NM_TRACE_EVENT_PW_RECV_DESTROY:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_recv_destroy");
          break;
        case NM_TRACE_EVENT_PW_RECV_COMPLETE:
          nm_trace_gen_state_set(e->global_time, "Pw_state", container,  "pw_recv_complete");
          break;

          /* core states */
        case NM_TRACE_STATE_CORE_NONE:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_none");
          break;
        case NM_TRACE_STATE_CORE_STRATEGY:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_strategy");
          break;
        case NM_TRACE_STATE_CORE_POLLING:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_polling");
          break;
        case NM_TRACE_STATE_CORE_TASK_FLUSH:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_task_flush");
          break;
        case NM_TRACE_STATE_CORE_DISPATCHING:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_dispatching");
          break;
        case NM_TRACE_STATE_CORE_UNPACK_NEXT:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_unpack_next");
          break;
        case NM_TRACE_STATE_CORE_COMPLETED_PW:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_completed_pw");
          break;
        case NM_TRACE_STATE_CORE_PACK_SUBMIT:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_pack_submit");
          break;
        case NM_TRACE_STATE_CORE_HANDLER:
          nm_trace_gen_state_set(e->global_time, "Core_Task", container, "core_handler");
          break;

        case NM_TRACE_EVENT_CLOCK_SYNCHRONIZE:
          nm_trace_gen_state_set(e->global_time, "World", container,  "Clock_synchronize"); /* TODO- switch to event, not state */
          break;

        default:
          break;
        }
      padico_string_delete(v);
    }
  PUK_GET_TICK(t2);
  const double t = PUK_TIMING_DELAY(t1, t2);
  const double t_seconds = t / 1000000.0;
  padico_out(puk_verbose_notice, "trace flushed %d entries in %f seconds (%d entries / second.)\n",
             p_packed->n_entries, t_seconds, (int)((double)p_packed->n_entries / t_seconds));
  nm_trace_object_container_vect_destroy(&packs);
  nm_trace_object_container_vect_destroy(&unpacks);
  for(d = 0; d < p_packed->n_drivers; d++)
    {
      padico_string_delete(p_container_drivers[d]);
      nm_trace_object_container_vect_destroy(&pws[d]);
      nm_trace_object_container_vect_destroy(&pwr[d]);
    }
  padico_string_delete(p_container_world);
  padico_string_delete(p_container_node);
  padico_string_delete(p_container_pack);
  padico_string_delete(p_container_unpack);
  padico_string_delete(p_container_core);
}

static void nm_trace_flush(nm_core_t p_core)
{
  static int flushed = 0;
  if(flushed)
    return;
  padico_out(puk_verbose_notice, "flush traces (%d entries)\n", nm_trace.last);
  flushed = 1;
  /* check if there is any trace to write */
  int*n_entries = padico_malloc(sizeof(int) * nm_trace.size);
  nm_coll_gather(nm_trace.p_comm, 0 /* root */, &nm_trace.last, sizeof(int), n_entries, sizeof(int), 1 /* tag */);
  nm_coll_bcast(nm_trace.p_comm, 0 /* root */, n_entries, sizeof(int) * nm_trace.size, 2 /* tag */);
  int total = 0;
  int k;
  for(k = 0; k < nm_trace.size; k++)
    {
      total += n_entries[k];
    }
  padico_free(n_entries);
  if(total == 0)
    {
      padico_out(puk_verbose_notice, "trace empty, not writting file.\n");
      return;
    }
  struct nm_trace_packed_s*p_packed = nm_trace_pack(p_core);

  if(nm_trace.rank == 0)
    {
      /* init GTG */
      padico_string_t trace_name =
        padico_string_new_printf("%s/nmad_%s_%d%s",
                                 padico_module_attr_trace_path_getvalue(),
                                 padico_hostname(), getpid(), padico_module_attr_trace_suffix_getvalue());
      nm_trace_gen_init(padico_string_get(trace_name));
      padico_out(puk_verbose_notice, "writing trace file %s\n", padico_string_get(trace_name));
      padico_string_delete(trace_name);
      nm_trace_header();
      nm_trace_packed_flush(p_packed);
      nm_trace_packed_free(p_packed);

      int i;
      for(i = 1; i < nm_trace.size; i++)
        {
          padico_out(puk_verbose_notice, "writing trace for node #%d\n", i);
          nm_gate_t p_gate = nm_comm_get_gate(nm_trace.p_comm, i);
          p_packed = nm_trace_packed_recv(p_gate);
          nm_trace_packed_flush(p_packed);
          nm_trace_packed_free(p_packed);
        }
      nm_trace_gen_end();
    }
  else
    {
      nm_gate_t p_gate = nm_comm_get_gate(nm_trace.p_comm, 0);
      nm_trace_packed_send(p_packed, p_gate);
      nm_trace_packed_free(p_packed);
    }
}

void nm_trace_init(nm_core_t p_core)
{
  nm_trace.trace_max = padico_module_attr_trace_max_getvalue();
  char*s_trace = padico_strdup(padico_module_attr_trace_getvalue()); /* make a copy since strtok writes in the buffer */
  padico_out(puk_verbose_notice, "trace enabled- NMAD_TRACE = %s; NMAD_TRACE_MAX = %u\n", s_trace, nm_trace.trace_max);
  nm_trace.p_entries = padico_malloc(sizeof(struct nm_trace_capture_entry_s) * nm_trace.trace_max);
  char*saveptr = NULL;
  char*s = strtok_r(s_trace, ",", &saveptr);
  while(s != NULL)
    {
      if(strcmp(s, "none") == 0)
        {
          nm_trace.filters = 0;
        }
      else if(strcmp(s, "all") == 0)
        {
          nm_trace.filters = NM_TRACE_FILTER_ALL;
        }
      else if(strcmp(s, "core") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_CORE;
        }
      else if(strcmp(s, "driver") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_DRIVER;
        }
      else if(strcmp(s, "pack") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_PACK;
        }
      else if(strcmp(s, "link") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_LINK;
        }
      else if(strcmp(s, "^core") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_CORE;
        }
      else if(strcmp(s, "^driver") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_DRIVER;
        }
      else if(strcmp(s, "^pack") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_PACK;
        }
      else if(strcmp(s, "^link") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_LINK;
        }
      else
        {
          NM_FATAL("unknown trace filter: %s\n", s);
        }
      s = strtok_r(NULL, ",", &saveptr);
    }
  padico_free(s_trace);
  nm_trace.p_comm = nm_comm_world("nm_trace");
  nm_trace.p_clocks = nm_sync_clocks_init(nm_trace.p_comm);
  nm_trace.tracing = 1;
  nm_trace.rank = nm_comm_rank(nm_trace.p_comm);
  nm_trace.size = nm_comm_size(nm_trace.p_comm);
}

void nm_trace_exit(nm_core_t p_core)
{
  nm_trace.tracing = 0;
  nm_sync_clocks_synchronize(nm_trace.p_clocks);
  nm_trace_flush(p_core);
  nm_sync_clocks_shutdown(nm_trace.p_clocks);
  nm_comm_destroy(nm_trace.p_comm);
  padico_free(nm_trace.p_entries);
}


#endif /* NMAD_TRACE */
