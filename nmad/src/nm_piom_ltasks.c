/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

#ifdef PIOMAN

struct nm_ltask_policy_s
{
  enum
    {
      NM_POLICY_NONE = 0, /**< not initialized */
      NM_POLICY_APP,      /**< near the application (current location) */
      NM_POLICY_DEV,      /**< near the network device */
      NM_POLICY_ANY       /**< anywhere, no policy */
    } location;
};

static struct nm_ltask_policy_s ltask_policy =
  {
    .location = NM_POLICY_NONE
  };

/** retrieve the binding policy */
void nm_ltask_set_policy(void)
{
  const char*policy = getenv("PIOM_BINDING_POLICY");
  if(!policy)
    {
      const char*padico_host_count = getenv("PADICO_HOST_COUNT");
      int mcount = padico_host_count ? atoi(padico_host_count) : 1;
      policy = (mcount > 1) ? "app" : "dev";
      NM_DISPF("use default pioman binding policy for %d process/node.\n", mcount);
    }

  NM_DISPF("set pioman binding policy = %s\n", policy);
  if(strcmp(policy, "app") == 0)
    {
      ltask_policy.location = NM_POLICY_APP;
    }
  else if(strcmp(policy, "dev") == 0)
    {
      ltask_policy.location = NM_POLICY_DEV;
    }
  else if(strcmp(policy, "any") == 0)
    {
      ltask_policy.location = NM_POLICY_ANY;
    }
  else
    {
      NM_FATAL("unknown pioman binding policy %s.\n", policy);
    }
}

static piom_topo_obj_t nm_piom_driver_binding(nm_drv_t p_drv)
{
  piom_topo_obj_t binding = NULL;
#if defined(NMAD_HWLOC) && defined(PIOMAN_TOPOLOGY_HWLOC)
  hwloc_cpuset_t cpuset = p_drv->props.profile.cpuset;
  if(cpuset != NULL)
    {
      hwloc_obj_t o = hwloc_get_obj_covering_cpuset(piom_topo_hwloc_get(), cpuset);
      if(o == NULL)
        {
          /* cpuset given by network driver doesn't correspond to any hwloc ogject. */
          char*s_cpuset = NULL;
          hwloc_bitmap_asprintf(&s_cpuset, cpuset);
          NM_WARN("cannot find hwloc object for network '%s'; cpuset = %s.\n", p_drv->assembly->name, s_cpuset);
          return piom_topo_full;
        }
      binding = o;
      char s_binding[128];
      hwloc_obj_type_snprintf(s_binding, sizeof(s_binding), binding, 0);
      NM_DISPF("network %s binding: %s #%d.\n", p_drv->assembly->name, s_binding, binding->logical_index);
    }
  if(binding == NULL)
    {
      binding = NULL;
    }
#else /* NMAD_HWLOC && PIOMAN_TOPOLOGY_HWLOC */
  binding = piom_topo_full;
#endif /* NMAD_HWLOC && PIOMAN_TOPOLOGY_HWLOC */
  return binding;
}

static piom_topo_obj_t nm_get_binding_policy(nm_drv_t p_drv)
{
  if(p_drv->ltask_binding == NULL)
    {
      p_drv->ltask_binding = NULL;
      switch(ltask_policy.location)
        {
        case NM_POLICY_APP:
          p_drv->ltask_binding = piom_topo_current_obj();
          break;

        case NM_POLICY_DEV:
          p_drv->ltask_binding = nm_piom_driver_binding(p_drv);
          break;

        case NM_POLICY_ANY:
          p_drv->ltask_binding = NULL;
          break;

        default:
          NM_FATAL("pioman binding policy not defined.\n");
          break;
        }
    }
  return p_drv->ltask_binding;
}


/* ********************************************************* */
/* ** tasks */

static int nm_ltask_pw_recv(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_recv_progress(p_pw);
  return NM_ESUCCESS;
}

static int nm_ltask_pw_recv_prefetch(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_recv_prefetch(p_pw);
  return NM_ESUCCESS;
}

static int nm_ltask_pw_block_recv(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_recv_wait(p_pw);
  return NM_ESUCCESS;
}

static int nm_ltask_pw_send(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_send_progress(p_pw);
  return NM_ESUCCESS;
}

static int nm_ltask_pw_send_prefetch(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_send_prefetch(p_pw);
  return NM_ESUCCESS;
}

static int nm_task_pw_block_send(void*_pw)
{
  struct nm_pkt_wrap_s*p_pw = _pw;
  nm_pw_send_wait(p_pw);
  return NM_ESUCCESS;
}

static int nm_ltask_core_progress(void*_p_core)
{
  struct nm_core*p_core = _p_core;
  int ret = NM_ESUCCESS;
  if(nm_core_trylock(p_core))
    {
      /* progress of locked tasks */
      nm_core_progress(p_core);
      /* flush pending prefetch */
      struct nm_pkt_wrap_s*p_pw, *p_pw2;
      puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->prefetch_large_send)
        {
          nm_pkt_wrap_list_remove(&p_core->prefetch_large_send, p_pw);
          nm_pkt_wrap_list_push_back(&p_pw->p_gate->pending_large_send, p_pw);
          nm_ltask_submit_pw_send_prefetch(p_pw);
        }
      nm_core_unlock(p_core);
    }
  /* progress of unlocked tasks */
  nm_core_events_dispatch(p_core);
  return ret;
}

static void nm_ltask_destructor(struct piom_ltask*p_ltask)
{
  struct nm_pkt_wrap_s*p_pw = nm_container_of(p_ltask, struct nm_pkt_wrap_s, ltask);
  nm_pw_ref_dec(p_pw);
}

void nm_ltask_submit_pw_recv_prefetch(struct nm_pkt_wrap_s*p_pw)
{
  piom_topo_obj_t ltask_binding = (p_pw->p_drv) ? nm_get_binding_policy(p_pw->p_drv) : piom_topo_obj_none;
  piom_ltask_create(&p_pw->ltask, &nm_ltask_pw_recv_prefetch,  p_pw,
                    PIOM_LTASK_OPTION_ONESHOT | PIOM_LTASK_OPTION_NOWAIT);
  piom_ltask_set_binding(&p_pw->ltask, ltask_binding);
  piom_ltask_set_name(&p_pw->ltask, "nmad: pw_recv_prefetch");
  piom_ltask_submit(&p_pw->ltask);
}

void nm_ltask_submit_pw_recv(struct nm_pkt_wrap_s*p_pw)
{
  piom_topo_obj_t ltask_binding = (p_pw->p_drv) ? nm_get_binding_policy(p_pw->p_drv) : piom_topo_obj_none;
  piom_ltask_create(&p_pw->ltask, &nm_ltask_pw_recv,  p_pw,
                    PIOM_LTASK_OPTION_REPEAT | PIOM_LTASK_OPTION_NOWAIT);
  piom_ltask_set_binding(&p_pw->ltask, ltask_binding);
  piom_ltask_set_name(&p_pw->ltask, "nmad: pw_recv");
  piom_ltask_set_destructor(&p_pw->ltask, &nm_ltask_destructor);
  nm_pw_ref_inc(p_pw);
  if(p_pw->p_drv->props.capabilities.prefers_recv_wait ||
     ((p_pw->flags & NM_PW_POLL_ANY) && p_pw->p_drv->props.capabilities.prefers_wait_any) )
    {
      const int delay_usec = 0;  /* 2 * p_pw->p_drv->props.profile.latency */
      p_pw->flags |= NM_PW_BLOCKING_CALL;
      piom_ltask_set_blocking(&p_pw->ltask, &nm_ltask_pw_block_recv, delay_usec);
    }
  piom_ltask_submit(&p_pw->ltask);
}

void nm_ltask_submit_pw_send_prefetch(struct nm_pkt_wrap_s*p_pw)
{
  piom_topo_obj_t ltask_binding = (p_pw->p_drv) ? nm_get_binding_policy(p_pw->p_drv) : piom_topo_obj_none;
  piom_ltask_create(&p_pw->ltask, &nm_ltask_pw_send_prefetch, p_pw,
                    PIOM_LTASK_OPTION_ONESHOT | PIOM_LTASK_OPTION_NOWAIT);
  piom_ltask_set_binding(&p_pw->ltask, ltask_binding);
  piom_ltask_set_name(&p_pw->ltask, "nmad: pw_send_prefetch");
  piom_ltask_submit(&p_pw->ltask);
}

void nm_ltask_submit_pw_send(struct nm_pkt_wrap_s*p_pw)
{
  piom_topo_obj_t ltask_binding = (p_pw->p_drv) ? nm_get_binding_policy(p_pw->p_drv) : piom_topo_obj_none;
  piom_ltask_create(&p_pw->ltask, &nm_ltask_pw_send, p_pw,
                    PIOM_LTASK_OPTION_REPEAT | PIOM_LTASK_OPTION_NOWAIT);
  piom_ltask_set_binding(&p_pw->ltask, ltask_binding);
  piom_ltask_set_name(&p_pw->ltask, "nmad: pw_send");
  piom_ltask_set_destructor(&p_pw->ltask, &nm_ltask_destructor);
  nm_pw_ref_inc(p_pw);
  if((p_pw->p_drv != NULL) && p_pw->p_drv->props.capabilities.prefers_send_wait)
    {
      const int delay_usec = 0;  /* 2 * p_pw->p_drv->props.profile.latency */
      p_pw->flags |= NM_PW_BLOCKING_CALL;
      piom_ltask_set_blocking(&p_pw->ltask, &nm_task_pw_block_send, delay_usec);
    }
  piom_ltask_submit(&p_pw->ltask);
}

void nm_ltask_submit_core_progress(struct nm_core*p_core)
{
  piom_topo_obj_t ltask_binding = NULL;
  piom_ltask_create(&p_core->ltask, &nm_ltask_core_progress, p_core,
                    PIOM_LTASK_OPTION_REPEAT | PIOM_LTASK_OPTION_NOWAIT);
  piom_ltask_set_binding(&p_core->ltask, ltask_binding);
  piom_ltask_set_name(&p_core->ltask, "nmad: core_progress");
  piom_ltask_submit(&p_core->ltask);
}


#else /* PIOMAN */

#endif /* PIOMAN */
