#! /bin/sh -e

export M4PATH=../building-tools:./building-tools:${M4PATH}

${AUTOCONF:-autoconf}
${AUTOHEADER:-autoheader} -f

if [ ! -r ./interfaces/sync_clocks/include/nm_sync_clocks_generic.h ]; then
    if [ -r ../mpi_sync_clocks ]; then
	ln -s ../../../../mpi_sync_clocks/include/sync_clocks_generic.h ./interfaces/sync_clocks/include/nm_sync_clocks_generic.h
    else
	echo "Cannot find mpi_sync_clocks sub-module"
	exit 1
    fi
fi
if [ ! -r ./interfaces/sync_clocks/include/sync_clocks_clocks.h ]; then
    if [ -r ../mpi_sync_clocks ]; then
	ln -s ../../../../mpi_sync_clocks/include/sync_clocks_clocks.h ./interfaces/sync_clocks/include/sync_clocks_clocks.h
    else
	echo "Cannot find mpi_sync_clocks sub-module"
	exit 1
    fi
fi
