/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>

/** @ingroup nm_public
 * @{
 * @file
 * This file contains some types and constants used throughout NewMad
 * core and interfaces.
 * @}
 */

#ifndef NM_TYPES_H
#define NM_TYPES_H

#include <Padico/Puk.h>

/** @ingroup nm_public
 * @{
 */

/* ** Gates ************************************************ */

/** a gate; opaque type to designate a peer node */
typedef struct nm_gate_s*nm_gate_t;

/** no gate */
#define NM_GATE_NONE ((nm_gate_t)NULL)

#define NM_ANY_GATE NM_GATE_NONE

/** Get the user-registered per-gate data */
void*nm_gate_ref_get(nm_gate_t p_gate);

/** Set the user-registered per-gate data */
void nm_gate_ref_set(nm_gate_t p_gate, void*ref);

/** hash ranks by gate */
PUK_HASHTABLE_TYPE(nm_gate_reverse, nm_gate_t, int,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

/* ** Tags ************************************************* */

/** user tags, 64 bits, contained in indirect hashtable */
typedef uint64_t nm_tag_t;
/** maximum usable nmad tag */
#define NM_TAG_MAX UINT64_MAX

/** tag mask that matches all bits */
#define NM_TAG_MASK_FULL ((nm_tag_t)-1)
/** tag mask that matches no bits */
#define NM_TAG_MASK_NONE ((nm_tag_t)0)

/* ** packets length and offsets *************************** */

/** data length used by nmad */
typedef uint64_t nm_len_t;

/** length is undefined */
#define NM_LEN_UNDEFINED  ((nm_len_t)-1)
/** maximum length usable in nmad. */
#define NM_LEN_MAX        ((nm_len_t)-2)

/* ** priorities ******************************************* */

/** message priority */
typedef int32_t nm_prio_t;

/** @}
 */

/* ** Tracks *********************************************** */

/** ID of a track, assigned in order */
typedef int8_t nm_trk_id_t;
struct nm_trk_s;

typedef enum nm_trk_kind_e
  {
    nm_trk_undefined,
    nm_trk_small, /**< small packets with headers & parsing */
    nm_trk_large  /**< large packets with rdv, no header */
  } nm_trk_kind_t;

/* ** Misc ************************************************* */

/** protocol flags- not part of the public API, but needed for inline */
typedef uint8_t nm_proto_t;

/** Sequence number for packets on a given gate/tag */
typedef uint32_t nm_seq_t;

/** Reserved sequence number never used by real packets */
#define NM_SEQ_NONE ((nm_seq_t)0)

/** largest sequence number */
#define NM_SEQ_MAX ((nm_seq_t)-1)

/** sequence number for requests */
typedef uint64_t nm_req_seq_t;


#endif /* NM_TYPES_H */
