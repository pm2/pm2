/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_STRATEGY_H
#define NM_STRATEGY_H

/** Driver for 'NewMad_Strategy' component interface
 */
struct nm_strategy_iface_s
{
  /** strategy capabilities, not supposed to be used by nmad core,
   * but may be usefull for the user to check that config is right. */
  struct nm_strategy_capabilities_s
  {
    int supports_priorities; /**< whether the strategy supports priority-based scheduling */
  } capabilities;

  /** init the strategy context (shared across instances) */
  void (*init)(puk_context_t p_context);

  /** close the strategy context */
  void (*close)(puk_context_t p_context);

  /** schedule all events:
   *    - generate a pw from chunks if needed
   *    - process rdv requests
   */
  void (*schedule)(puk_context_t p_context);

  /** submit an outgoing req chunk to the strategy; req chunk is enqueued
   * in gate req_chunk_list if this function is NULL. */
  void (*submit_req_chunk)(puk_context_t p_context, struct nm_req_chunk_s*p_req_chunk, int front);

  /** submit an outgoing ctrl chunk to the strategy; ctrl_chunk is enqueued
   * in gate ctrl_chunk_list if this function is NULL. */
  void (*submit_ctrl_chunk)(puk_context_t p_context, struct nm_ctrl_chunk_s*p_ctrl_chunk);

  /** selects the drivers to actually use, among the list of available drivers for the given gate */
  nm_drv_vect_t (*connect)(void*_status, nm_gate_t p_gate, nm_drv_vect_t p_available_drvs);

  /** Compute and apply the best possible packet rearrangement, then
      return next packet to send */
  void (*try_and_commit)(void*_status, nm_gate_t p_gate);

  /** Emit RTR series for received RDV requests. */
  void (*rdv_accept)(void*_status, nm_gate_t p_gate);

  /** process strat private protocol */
  void (*proto)(void*_status, nm_gate_t p_gate, struct nm_pkt_wrap_s*p_pw, const void*ptr, nm_len_t len);

  /** schedule the given pw for sending */
  void (*pw_send_post)(void*_status, struct nm_pkt_wrap_s*p_pw);

  /** notify send completion for pw */
  void (*pw_send_complete)(void*_status, struct nm_pkt_wrap_s*p_pw);

  /** notify recv completion for pw */
  void (*pw_recv_complete)(void*_status, struct nm_pkt_wrap_s*p_pw);

};

PUK_IFACE_TYPE(NewMad_Strategy, struct nm_strategy_iface_s);

#endif /* NM_STRATEGY_H */
