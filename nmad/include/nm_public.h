/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_PUBLIC_H
#define NM_PUBLIC_H

/** @defgroup nm_public Public definitions.
 * Public definitions from nmad core and common to all nmad interfaces
 * It contains mainly:
 *   - definition of **gates**, the type to designate a peer: ::nm_gate_t. User is
 *     expected to get gates using @ref launcher_interface.
 *   - definition of **tags**: ::nm_tag_t. Tag size is usually at least 64 bits wide.
 *     Maximum available tag is #NM_TAG_MAX.
 *   - definition of **data descriptors**, the internal data representation in nmad:
 *     @ref nm_data
 *
 * **Sessions** are the basic multiplexing facility in NewMadeleine. Messages and tags
 * from different sessions are isolated from each other, allonwing multiple
 * libraries to use NewMadeleine at the same time, or a given code to use multiple
 * nmad interfaces at the same time. While they are used throughout all nmad
 * interfaces, sessions (::nm_session_t) are not defined in core but in
 * the internal @ref session_interface. User is expected to open and close sessions using
 * the @ref launcher_interface.
 */

/** @ingroup nm_public
 * @{
 * @file
 * This is the common public header for NewMad. It includes publics headers
 * wich define common types and constants, but no API by itself.
 *
 * The API for end-users are defined in the 'interfaces/' directory.
 * The low-level 'core' API is defined in nm_core_interface.h.
 * @}
 */

/** @ingroup core_interface
 * @{
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <nm_config.h>
#include <nm_errno.h>
#include <nm_log.h>
#include <nm_types.h>
#include <nm_data.h>
#include <Padico/Puk.h>
#if defined(NMAD_PIOMAN)
#  include <pioman.h>
#endif

/* ** Config sanity checks ********************************* */

#if !defined(NMAD)
#  error "nmad CFLAGS not defined; please compile with CFLAGS returned by 'pkg-config --cflags nmad'"
#endif /* NMAD */

#if defined(NMAD_PIOMAN) && !defined(PIOMAN)
#  error "nmad was configured with pioman support; cannot build without pioman flags."
#endif

#if defined(PIOMAN) && !defined(NMAD_PIOMAN)
#  error "nmad was configured without pioman support; cannot build with pioman flags."
#endif

#if defined(NMAD_PUKABI) && !defined(PUKABI)
#  error "nmad was configured with PukABI support; cannot build without PukABI flags."
#endif

#if defined(PUKABI) && !defined(NMAD_PUKABI)
#  error "nmad was configured without PukABI support; cannot build with PukABI flags."
#endif

#if defined(PIOMAN_TOPOLOGY_HWLOC) && !defined(NMAD_HWLOC)
#  error "pioman was configured with hwloc, but nmad was configured without hwloc."
#endif

#if defined(NMAD_SIMGRID) && !defined(PUK_SIMGRID)
#  error "nmad was configured with simgrid, but Puk was configured without simgrid."
#endif

#if defined(PUK_SIMGRID) && !defined(NMAD_SIMGRID)
#  error "Puk was configured with simgrid, but nmad was configured without simgrid."
#endif

#if defined(NMAD_SIMGRID) && (defined(PIOMAN) && !defined(PIOMAN_SIMGRID))
#  error "nmad was configured with simgrid, but pioman was configured without simgrid."
#endif


/* ** ABI config ******************************************* */

#ifdef NMAD_PIOMAN
#  define NMAD_CONFIG_PIOMAN 1
#else
#  define NMAD_CONFIG_PIOMAN 0
#endif

#ifdef NMAD_HWLOC
#  define NMAD_CONFIG_HWLOC 1
#else
#  define NMAD_CONFIG_HWLOC 0
#endif

#ifdef NMAD_PUKABI
#  define NMAD_CONFIG_PUKABI 1
#else
#  define NMAD_CONFIG_PUKABI 0
#endif

#ifdef NMAD_DEBUG
#  define NMAD_CONFIG_DEBUG 1
#else
#  define NMAD_CONFIG_DEBUG 0
#endif

#ifdef NDEBUG
#  define NMAD_CONFIG_NDEBUG 1
#else
#  define NMAD_CONFIG_NDEBUG 0
#endif

#ifdef NMAD_SIMGRID
#  define NMAD_CONFIG_SIMGRID 1
#else
#  define NMAD_CONFIG_SIMGRID 0
#endif

#ifdef NMAD_CUDA
#  define NMAD_CONFIG_CUDA 1
#else
#  define NMAD_CONFIG_CUDA 0
#endif

#ifdef NMAD_HIP
#  define NMAD_CONFIG_HIP 1
#else
#  define NMAD_CONFIG_HIP 0
#endif

#define nm_tostring2(s) #s
#define nm_tostring(s) nm_tostring2(s)

/** nmad config options
 */
#define NMAD_CONFIG_STRING                             \
  " pioman = "  nm_tostring(NMAD_CONFIG_PIOMAN)        \
  " hwloc = "   nm_tostring(NMAD_CONFIG_HWLOC)         \
  " pukabi = "  nm_tostring(NMAD_CONFIG_PUKABI)        \
  " debug = "   nm_tostring(NMAD_CONFIG_DEBUG)         \
  " ndebug = "  nm_tostring(NMAD_CONFIG_NDEBUG)        \
  " simgrid = " nm_tostring(NMAD_CONFIG_SIMGRID)       \
  " cuda = "    nm_tostring(NMAD_CONFIG_CUDA)          \
  " hip = "     nm_tostring(NMAD_CONFIG_HIP)

/** config options that impact nmad ABI
 * (content of structures exposed in API or used in inline code)
 */
#define NMAD_ABI_CONFIG_STRING                         \
  " pioman = "  nm_tostring(NMAD_CONFIG_PIOMAN)        \
  " hwloc = "   nm_tostring(NMAD_CONFIG_HWLOC)         \
  " pukabi = "  nm_tostring(NMAD_CONFIG_PUKABI)        \
  " debug = "   nm_tostring(NMAD_CONFIG_DEBUG)         \
  " simgrid = " nm_tostring(NMAD_CONFIG_SIMGRID)       \
  " cuda = "    nm_tostring(NMAD_CONFIG_CUDA)          \
  " hip = "     nm_tostring(NMAD_CONFIG_HIP)

void nm_abi_config_check_internal(const char*p_config);

/** Check consistency of ABI config between nmad and application.
 */
static inline void nm_abi_config_check(void)
{
  /* capture ABI config in application context
   * and compare with nmad builtin ABI config */
  nm_abi_config_check_internal(NMAD_ABI_CONFIG_STRING);
}

/** @} */


#endif /* NM_PUBLIC_H */
