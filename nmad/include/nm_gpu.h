/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_GPU_H
#define NM_GPU_H

/** @ingroup nmad_private
 * @file
 * Functions dedicated to interactions with GPU.
 */

#include "nm_private_config.h"

#ifdef NMAD_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif /* NMAD_CUDA */

#ifdef NMAD_HIP
#include <hip/hip_runtime.h>
#include <hip/hip_runtime_api.h>
#endif /* NMAD_HIP */


/* ** GPU frontend for nm_data ***************************** */

static inline int nm_data_is_gpu(const struct nm_data_properties_s*p_props)
{
#ifdef NMAD_CUDA
  return p_props->is_cuda;
#elif defined(NMAD_HIP)
  return p_props->is_hip;
#endif /* NMAD_HIP */
  return 0;
}

/** fill in the GPU part of data properties, following pointer 'p_ptr' */
static inline void nm_data_properties_gpu_fill(struct nm_data_properties_s*p_props, const void*p_ptr)
{
#ifdef NMAD_CUDA
  if(nm_core_get_singleton()->enable_gpu_support)
    {
      struct cudaPointerAttributes attrs;
      cudaError_t rc = cudaPointerGetAttributes(&attrs, p_ptr);
      if(rc != cudaSuccess)
        {
          NM_FATAL("error %d (%s) in cudaPointerGetAttributes\n", rc, cudaGetErrorString(rc));
        }
      if((attrs.type == cudaMemoryTypeHost) || (attrs.type == cudaMemoryTypeManaged) || (attrs.type == cudaMemoryTypeUnregistered))
        {
          if(attrs.type == cudaMemoryTypeHost)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_host);
            }
          else if(attrs.type == cudaMemoryTypeManaged)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_managed);
            }
          else if(attrs.type == cudaMemoryTypeUnregistered)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_unregistered);
            }
          p_props->is_cuda = 0;
        }
      else if(attrs.type == cudaMemoryTypeDevice)
        {
          nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_device);
          p_props->is_cuda = 1;
        }
      else
        NM_FATAL("unknown CUDA memory type %d\n", attrs.type);
    }
  else
    {
      nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_unregistered);
      p_props->is_cuda = 0;
    }
#elif defined(NMAD_HIP)
  if(nm_core_get_singleton()->enable_gpu_support)
    {
      if(p_ptr == NULL) /* NULL is not a GPU pointer; do not even test */
        {
          p_props->is_hip = 0;
          return;
        }
      hipPointerAttribute_t attrs;
      hipError_t rc = hipPointerGetAttributes(&attrs, p_ptr);
      if(rc != hipSuccess)
        {
          NM_FATAL("error %d (%s) in hipPointerGetAttributes; p_ptr = %p\n", rc, hipGetErrorString(rc), p_ptr);
        }
      if((attrs.type == hipMemoryTypeHost) || (attrs.type == hipMemoryTypeManaged) || (attrs.type == hipMemoryTypeUnregistered))
        {
          if(attrs.type == hipMemoryTypeHost)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_host);
            }
          else if(attrs.type == hipMemoryTypeManaged)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_managed);
            }
          else if(attrs.type == hipMemoryTypeUnregistered)
            {
              nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_unregistered);
            }
          p_props->is_hip = 0;
        }
      else if(attrs.type == hipMemoryTypeDevice)
        {
          nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_device);
          p_props->is_hip = 1;
        }
      else
        NM_FATAL("unknown HIP memory type %d\n", attrs.type);
    }
  else
    {
      nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_unregistered);
      p_props->is_hip = 0;
    }
#else /* NMAD_HIP */
  nm_profile_inc(nm_core_get_singleton()->profiling.n_memory_unregistered);
#endif /* NMAD_HIP */
}

/** memcpy without knowing whether memory is on GPU or not; assume unified addressing */
static inline void nm_data_memcpy(void*p_ptr_dest, const void*p_ptr_from, const nm_len_t len)
{
  if(nm_core_get_singleton()->enable_gpu_support)
    {
#if defined(NMAD_CUDA)
      cudaError_t rc = cudaMemcpy(p_ptr_dest, p_ptr_from, len, cudaMemcpyDefault); /* assume unified addressing */
      if(rc != cudaSuccess)
        {
          NM_FATAL("CUDA cannot copy %ld bytes; error %s\n", len, cudaGetErrorString(rc));
        }
#elif defined(NMAD_HIP)
      hipError_t rc = hipMemcpy(p_ptr_dest, p_ptr_from, len, hipMemcpyDefault); /* assume unified addressing */
      if(rc != hipSuccess)
        {
          NM_FATAL("HIP cannot copy %ld bytes; error %s\n", len, hipGetErrorString(rc));
        }
#else
      memcpy(p_ptr_dest, p_ptr_from, len);
#endif
    }
  else
    {
      memcpy(p_ptr_dest, p_ptr_from, len);
    }
}

/** copy chunks of data.
 * p_src is in a nm_data, maybe in GPU memory
 * p_dest is in host memory
 */
static inline void nm_data_memcpy_from(void*p_dest, const void*p_src, nm_len_t len, const struct nm_data_properties_s*p_props)
{
#if defined(NMAD_CUDA)
  if(p_props->is_cuda)
    {
      cudaError_t rc = cudaMemcpy(p_dest /* host */, p_src /* gpu */, len, cudaMemcpyDeviceToHost);
      if(rc != cudaSuccess)
        {
          NM_FATAL("CUDA cannot copy %ld bytes from GPU; error %s\n", len, cudaGetErrorString(rc));
        }
      return;
    }
#endif /* NMAD_CUDA */

#if defined(NMAD_HIP)
  if(p_props->is_hip)
    {
      hipError_t rc = hipMemcpy(p_dest /* host */, p_src /* gpu */, len, hipMemcpyDeviceToHost);
      if(rc != hipSuccess)
        {
          NM_FATAL("HIP cannot copy %ld bytes from GPU; error %s\n", len, hipGetErrorString(rc));
        }
      return;
    }
#endif /* NMAD_HIP */

  /* default case: host memory */
  memcpy(p_dest, p_src, len);
}

/** copy chunks of data.
 * p_dest is in a nm_data, maybe in GPU memory
 * p_src is in host memory
 */
static inline void nm_data_memcpy_to(void*p_dest, const void*p_src, nm_len_t len, const struct nm_data_properties_s*p_props)
{
#if defined(NMAD_CUDA)
  if(p_props->is_cuda)
    {
      cudaError_t rc = cudaMemcpy(p_dest /* gpu */, p_src /* src */, len, cudaMemcpyHostToDevice);
      if(rc != cudaSuccess)
        {
          NM_FATAL("CUDA cannot copy %ld bytes from GPU; error %s\n", len, cudaGetErrorString(rc));
        }
      return;
    }
#endif /* NMAD_CUDA */

#if defined(NMAD_HIP)
  if(p_props->is_hip)
    {
      hipError_t rc = hipMemcpy(p_dest /* gpu */, p_src /* host */, len, hipMemcpyHostToDevice);
      if(rc != hipSuccess)
        {
          NM_FATAL("HIP cannot copy %ld bytes from GPU; error %s\n", len, hipGetErrorString(rc));
        }
      return;
    }
#endif /* NMAD_HIP */

  /* default case: host memory */
  memcpy(p_dest, p_src, len);
}


/* ** GPU frontend for pw ********************************** */

/** fill the GPU flags of a pw.
 */
static inline void nm_pw_gpu_fill(struct nm_pkt_wrap_s*p_pw, const struct nm_data_properties_s*p_props)
{
#ifdef NMAD_CUDA
  if(p_props->is_cuda)
    {
      p_pw->flags |= NM_PW_DATA_IS_CUDA;
    }
#endif /* NMAD_CUDA */
#ifdef NMAD_HIP
  if(p_props->is_hip)
    {
      p_pw->flags |= NM_PW_DATA_IS_HIP;
    }
#endif /* NMAD_CUDA */
}


#endif /* NM_GPU_H */
