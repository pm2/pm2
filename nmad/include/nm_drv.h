/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_DRV_H
#define NM_DRV_H

#ifndef NM_PRIVATE_H
#  error "Cannot include this file directly. Please include <nm_private.h>"
#endif


PUK_LIST_DECLARE_TYPE(nm_drv);

/** Driver.
 */
struct nm_drv_s
{
  /** link to insert this driver into the driver_list in core */
  PUK_LIST_LINK(nm_drv);

  /** unique ID for the driver, in the form component/network_id */
  const char*driver_id;
  /** Component assembly associated to the driver */
  puk_component_t assembly;
  /** driver contexts for given component */
  puk_context_t minidriver_context;
  /** Driver interface, for use when no instance is needed */
  const struct nm_minidriver_iface_s*driver;

  /** global recv request if driver supports recv_any */
  struct nm_pkt_wrap_s*p_pw_recv_any;

  /** driver properties (profile & capabilities) */
  struct nm_minidriver_properties_s props;

  /** driver url, as string */
  const char*url;

#ifdef PIOMAN
  /** binding for the pioman ltask */
  piom_topo_obj_t ltask_binding;
#endif  /* PIOMAN */
  /* NM core object. */
  struct nm_core *p_core;

#ifdef NMAD_PROFILE
  struct
  {
    unsigned long long n_recv_packets;       /**< number of received packet on driver */
    unsigned long long n_recv_bytes;         /**< amount of received bytes on driver */
    unsigned long long n_send_packets;       /**< number of sent packet on driver */
    unsigned long long n_send_bytes;         /**< amount of sent bytes on driver */
    unsigned long long n_send_prefetch;      /**< number of prefetched pw (including those which enventually got unfetched) */
    unsigned long long n_send_unfetch;       /**< number of unfetched pw */
    unsigned long long n_recv_prefetch;
    unsigned long long n_recv_prefetch_bytes;
    unsigned long long n_recv_unfetch;
    double total_send_post_usecs;            /**< total time spent in send posts operations */
    double total_send_poll_usecs;            /**< total time spent in send polling operations */
    double total_send_usecs;                 /**< total time between send begin and completion */
    double total_recv_post_usecs;            /**< total time spent in recv posts operations */
    double total_recv_poll_usecs;            /**< total time spent in recv polling operations */
    double total_send_prefetch_usecs;        /**< total time spent in send prefetch */
    double total_recv_prefetch_usecs;        /**< total time spent in recv prefetch */
    double average_send_bw_Mbps;             /**< average send bandwidth in MB/s */
    double average_send_prefetch_bw_Mbps;
    double average_recv_prefetch_bw_Mbps;
  } profiling;
#endif /* NMAD_PROFILE */
};

PUK_LIST_CREATE_FUNCS(nm_drv);

#define NM_FOR_EACH_DRIVER(p_drv, p_core)               \
  puk_list_foreach(nm_drv, p_drv, &(p_core)->driver_list)


#endif /* NM_DRV_H */
