/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_PRIVATE_H
#define NM_PRIVATE_H

/** @defgroup nmad_private nmad internal interfaces.
 * End-users are not supposed to use it. The interface is
 * not usable outside of nmad core.
 * @{
 */

#include "nm_private_config.h"

#ifndef NMAD_BUILD
#  error "NMAD_BUILD flag not set while including nm_private.h. This header is not part of API and cannot be used outside of nmad."
#endif /* NMAD_BUILD */


/* ** external headers */

#include <Padico/Puk.h>
#include <Padico/Module.h>

#ifdef PIOMAN
#include <pioman.h>
#endif

#if defined(PUKABI)
#include <Padico/Puk-ABI.h>
#endif /* PUKABI */
#if defined(PUKABI_ENABLE_FSYS)
#define NM_SYS(SYMBOL) PUK_ABI_FSYS_WRAP(SYMBOL)
#else  /* PUKABI_ENABLE_FSYS */
#define NM_SYS(SYMBOL) SYMBOL
#endif /* PUKABI_ENABLE_FSYS */

#ifdef NMAD_HWLOC
#include <hwloc.h>
#endif

#ifdef NMAD_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif /* NMAD_CUDA */

#ifdef NMAD_HIP
#include <hip/hip_runtime.h>
#include <hip/hip_runtime_api.h>
#endif /* NMAD_HIP */


/* ** nmad public headers */

#include <nm_public.h>
#include <nm_core_interface.h>
#include <nm_parameters.h>
#include <nm_trace.h>


/* ** nmad private headers */

#include <nm_core_types.h>
#include <nm_tags.h>
#include <nm_minidriver.h>
#include <nm_pkt_wrap.h>
#include <nm_drv.h>
#ifdef PIOMAN
#include <nm_piom_ltasks.h>
#endif
#include <nm_headers.h>
#include <nm_strategy.h>
#include <nm_gate.h>
#include <nm_core.h>
#include <nm_lock.h>
#include <nm_core_private.h>
#include <nm_core_inline.h>
#include <nm_gpu.h>
#include <nm_tactics.h>
#include <nm_sampling.h>

/** @} */

#endif /* NM_PRIVATE_H */
