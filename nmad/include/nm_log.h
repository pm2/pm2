/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_LOG_H
#define NM_LOG_H

/** @ingroup nmad_private
 * @file
 * Basic primitives to display info & warnings
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <Padico/Puk.h>

#define NM_DISPF(str, ...)      padico_out(puk_verbose_info, str, ## __VA_ARGS__)

#define NM_TRACEF(str, ...)     padico_trace(str, ## __VA_ARGS__)

#define NM_WARN(format, ...)    padico_warning(format, ## __VA_ARGS__)

#define NM_FATAL(format, ...)   padico_fatal(format, ## __VA_ARGS__)


#endif /* NM_LOG_H */
