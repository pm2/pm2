/*
 * NewMadeleine
 * Copyright (C) 2014-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file Cross-Memory Attach driver with 'minidriver' interface.
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <nm_minidriver.h>
#include <Padico/Shm.h>

/* ********************************************************* */

static void*nm_minidriver_cma_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_minidriver_cma_destroy(void*);

static const struct puk_component_driver_s nm_minidriver_cma_component =
  {
    .instantiate = &nm_minidriver_cma_instantiate,
    .destroy     = &nm_minidriver_cma_destroy
  };

static void nm_minidriver_cma_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_minidriver_cma_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_minidriver_cma_close(puk_context_t context);
static void nm_minidriver_cma_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_minidriver_cma_connect_async(void*_status, const void*remote_url, size_t url_size);
static int  nm_minidriver_cma_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_minidriver_cma_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_minidriver_cma_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_minidriver_cma_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);


static const struct nm_minidriver_iface_s nm_minidriver_cma_minidriver =
  {
    .getprops       = &nm_minidriver_cma_getprops,
    .init           = &nm_minidriver_cma_init,
    .close          = &nm_minidriver_cma_close,
    .connect        = &nm_minidriver_cma_connect,
    .connect_async  = &nm_minidriver_cma_connect_async,
    .send_pkt_post  = &nm_minidriver_cma_send_pkt_post,
    .send_pkt_poll  = &nm_minidriver_cma_send_pkt_poll,
    .recv_pkt_post  = &nm_minidriver_cma_recv_pkt_post,
    .recv_pkt_poll  = &nm_minidriver_cma_recv_pkt_poll
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_CMA,
  puk_component_declare("Minidriver_CMA",
                        puk_component_provides("PadicoComponent", "component", &nm_minidriver_cma_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_minidriver_cma_minidriver),
                        puk_component_attr("network", "localhost")));

/* ********************************************************* */

/** 'cma' per-context data.
 */
struct nm_minidriver_cma_context_s
{
  struct padico_shm_s*shm;                 /**< shm segment */
  char*url;
};

/** 'cma' per-instance status.
 */
struct nm_minidriver_cma_s
{
  struct padico_shm_node_s*dest;           /**< destination node */
  int dest_rank;                           /**< destination rank in shm directory */
  struct nm_minidriver_cma_context_s*shm_context;
  struct padico_shm_short_lfqueue_s queue; /**< per-connection receive queue */
  struct
  {
    int block_num;
    struct nm_pkt_s*p_pkt;                 /**< pending pkt */
  } recv;
};

#define NM_MINIDRIVER_CMA_IOVEC_SIZE 256

/** packet to exchange iovec accross processes */
struct nm_minidriver_cma_packet_s
{
  struct iovec v[NM_MINIDRIVER_CMA_IOVEC_SIZE];
  int n;
  volatile int done;
};

/** metadata attached to rdv */
struct nm_minidriver_cma_rdv_data_s
{
  int block_num; /**< index of the block containing packet description */
};

/* ********************************************************* */

static void*nm_minidriver_cma_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_minidriver_cma_s*status = padico_malloc(sizeof(struct nm_minidriver_cma_s));
  assert(context != NULL);
  status->dest = NULL;
  status->dest_rank = -1;
  padico_shm_short_lfqueue_init(&status->queue);
  status->shm_context = puk_context_get_status(context);
  assert(status->shm_context != NULL);
  status->recv.block_num = -1;
  status->recv.p_pkt = NULL;
  return status;

}

static void nm_minidriver_cma_destroy(void*_status)
{
  struct nm_minidriver_cma_s*status = _status;
  status->dest = NULL;
  padico_free(_status);
}

/* ********************************************************* */

static void nm_minidriver_cma_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  p_props->profile.latency = 200;
  p_props->profile.bandwidth = 10000;
  p_props->capabilities.supports_data = 0;
  p_props->capabilities.supports_iovec = 1;
  p_props->capabilities.needs_rdv_data = 1;
  p_props->capabilities.trk_rdv = 1;
  p_props->capabilities.max_iovecs = NM_MINIDRIVER_CMA_IOVEC_SIZE;
  p_props->capabilities.max_msg_size = INT_MAX / 2; /* CMA silently fails for blocks larger than this */
  p_props->capabilities.max_pkt_sends = 1;
  p_props->capabilities.max_pkt_recvs = 1;
  p_props->nickname = "cma";
}

static void nm_minidriver_cma_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_minidriver_cma_context_s*shm_context = padico_malloc(sizeof(struct nm_minidriver_cma_context_s));
  padico_string_t segment_name = padico_string_new();
  padico_string_printf(segment_name, "minidriver-%s", puk_context_getattr(context, "network"));
  shm_context->shm = padico_shm_init(padico_string_get(segment_name));
  puk_context_set_status(context, shm_context);
  shm_context->url = padico_strdup((char*)padico_topo_node_getuuid(padico_topo_getlocalnode()));
  padico_string_delete(segment_name);
  *drv_url = shm_context->url;
  *url_size = strlen(shm_context->url);
  if(sizeof(struct nm_minidriver_cma_packet_s) > PADICO_SHM_BLOCKSIZE)
    {
      padico_fatal("CMA packet does not fit Shm block.\n");
    }
}

static void nm_minidriver_cma_close(puk_context_t context)
{
  struct nm_minidriver_cma_context_s*shm_context = puk_context_get_status(context);
  padico_shm_close(shm_context->shm);
  puk_context_set_status(context, NULL);
  padico_free(shm_context->url);
  padico_free(shm_context);
}

static void nm_minidriver_cma_connect(void*_status, const void*remote_url, size_t url_size)
{
  nm_minidriver_cma_connect_async(_status, remote_url, url_size);
}

static void nm_minidriver_cma_connect_async(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_minidriver_cma_s*status = (struct nm_minidriver_cma_s*)_status;
  const char*remote_uuid = remote_url;
  padico_topo_node_t remote_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)remote_uuid);
  assert(status->shm_context->shm->seg != NULL);
  status->dest = padico_shm_directory_node_lookup(status->shm_context->shm, remote_node);
  if(status->dest == NULL)
    {
      padico_fatal("cannot find peer node %s in shm directory.\n", remote_uuid);
    }
  else
    {
      status->dest_rank = status->dest->rank;
      assert(status->dest_rank >= 0);
      assert(status->dest_rank < PADICO_SHM_NUMNODES);
    }
}

static int nm_minidriver_cma_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  assert(p_pkt->iov.v != NULL); /* need data as iovec */
  struct nm_minidriver_cma_s*p_status = _status;
  /* read block_num from rdv_data */
  const struct nm_minidriver_cma_rdv_data_s*p_rdv_data =
    (const struct nm_minidriver_cma_rdv_data_s*)&p_pkt->rdv_data.content[0];
  assert(p_rdv_data->block_num >= 0);
  const int block_num = p_rdv_data->block_num;
  /* sanity check */
  if(p_pkt->iov.n > NM_MINIDRIVER_CMA_IOVEC_SIZE)
    {
      return -NM_EINVAL;
    }
  if(block_num == -1)
    {
      return -NM_EINVAL;
    }
  struct nm_minidriver_cma_packet_s*block = padico_shm_short_get_ptr(p_status->shm_context->shm, block_num);
  assert(block->n > 0);
  assert(block->n < NM_MINIDRIVER_CMA_IOVEC_SIZE);
  int rc = process_vm_writev(p_status->dest->pid,
                             p_pkt->iov.v, p_pkt->iov.n,
                             block->v, block->n,
                             0);
  if(rc < 0)
    {
      padico_fatal("error '%s' in process_vm_writev.\n", strerror(errno));
    }
  /* signal completion */
  block->done = 1;
  return NM_ESUCCESS;
}

static int nm_minidriver_cma_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  return NM_ESUCCESS;
}

static int nm_minidriver_cma_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_minidriver_cma_s*p_status = _status;
  assert(p_pkt->iov.v != NULL);
  if(p_pkt->iov.n > NM_MINIDRIVER_CMA_IOVEC_SIZE)
    return -NM_EINVAL;
  const int block_num = padico_shm_block_alloc(p_status->shm_context->shm);
  if(block_num == -1)
    {
      /* no block available; store request for later */
      p_status->recv.p_pkt = p_pkt;
      p_status->recv.block_num = -1;
      return -NM_EINPROGRESS;
    }
  else
    {
      assert(p_status->recv.block_num == -1);
      p_status->recv.block_num = block_num;
      struct nm_minidriver_cma_packet_s*block = padico_shm_short_get_ptr(p_status->shm_context->shm, block_num);
      int i;
      for(i = 0; i < p_pkt->iov.n; i++)
        {
          block->v[i] = p_pkt->iov.v[i];
        }
      block->n = p_pkt->iov.n;
      block->done = 0;
      /* write rdv_data */
      assert(p_status->recv.block_num != -1);
      struct nm_minidriver_cma_rdv_data_s*p_rdv_data =
        (struct nm_minidriver_cma_rdv_data_s*)&p_pkt->rdv_data.content[0];
      assert(sizeof(struct nm_minidriver_cma_rdv_data_s) <= NM_RDV_DATA_SIZE);
      p_rdv_data->block_num = p_status->recv.block_num;
    }
  return NM_ESUCCESS;
}

static int nm_minidriver_cma_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_minidriver_cma_s*p_status = _status;
  if(p_status->recv.block_num == -1)
    {
      /* no block was available when posting; try again */
      const int block_num = padico_shm_block_alloc(p_status->shm_context->shm);
      if(block_num == -1)
        {
          return -NM_EINPROGRESS;
        }
      p_status->recv.block_num = block_num;
      struct nm_minidriver_cma_packet_s*block =
        padico_shm_short_get_ptr(p_status->shm_context->shm, block_num);
      int i;
      for(i = 0; i < p_status->recv.p_pkt->iov.n; i++)
        {
          block->v[i] = p_status->recv.p_pkt->iov.v[i];
        }
      block->n = p_status->recv.p_pkt->iov.n;
      block->done = 0;
      /* write rdv_data */
      assert(p_status->recv.block_num != -1);
      struct nm_minidriver_cma_rdv_data_s*p_rdv_data =
        (struct nm_minidriver_cma_rdv_data_s*)&p_pkt->rdv_data.content[0];
      assert(sizeof(struct nm_minidriver_cma_rdv_data_s) <= NM_RDV_DATA_SIZE);
      p_rdv_data->block_num = p_status->recv.block_num;
      return -NM_EAGAIN; /* signal posting is complete */
    }
  else
    {
      struct nm_minidriver_cma_packet_s*block =
        padico_shm_short_get_ptr(p_status->shm_context->shm, p_status->recv.block_num);
      if(block->done)
        {
          padico_shm_block_free(p_status->shm_context->shm, p_status->recv.block_num);
          p_status->recv.block_num = -1;
          return NM_ESUCCESS;
        }
      else
        {
          return -NM_EAGAIN;
        }
    }
}
