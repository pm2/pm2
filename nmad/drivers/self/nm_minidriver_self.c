/*
 * NewMadeleine
 * Copyright (C) 2010-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <sys/uio.h>
#include <sys/poll.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <nm_private.h>

#include <Padico/Module.h>


static void*nm_self_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_self_destroy(void*status);

static const struct puk_component_driver_s nm_self_component =
  {
    .instantiate = &nm_self_instantiate,
    .destroy     = &nm_self_destroy
  };

static void nm_self_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_self_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_self_close(puk_context_t context);
static void nm_self_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_self_connect_async(void*_status, const void*remote_url, size_t url_size);
static int  nm_self_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_self_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_self_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_self_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_self_recv_pkt_cancel(void*_status, struct nm_pkt_s*p_pkt);

static const struct nm_minidriver_iface_s nm_self_minidriver =
  {
    .getprops        = &nm_self_getprops,
    .init            = &nm_self_init,
    .close           = &nm_self_close,
    .connect         = &nm_self_connect,
    .connect_async   = &nm_self_connect_async,
    .send_pkt_post   = &nm_self_send_pkt_post,
    .send_pkt_poll   = &nm_self_send_pkt_poll,
    .recv_pkt_post   = &nm_self_recv_pkt_post,
    .recv_pkt_poll   = &nm_self_recv_pkt_poll,
    .recv_pkt_cancel = &nm_self_recv_pkt_cancel
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_self,
  puk_component_declare("Minidriver_self",
                        puk_component_provides("PadicoComponent", "component", &nm_self_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_self_minidriver),
                        puk_component_attr("network", "auto")));

/* ********************************************************* */

/** 'self' driver per-context data. */
struct nm_self_context_s
{
  char*url;
};

/** 'self' per-instance status (singleton, actually). */
struct nm_self_s
{
  struct nm_self_context_s*p_self_context;
  struct
  {
    struct nm_pkt_s*p_pkt;
  } recv;
  struct
  {
    struct nm_pkt_s*p_pkt;
    volatile int consumed;
    volatile int posted;
  } send;
};

/* ********************************************************* */

static void*nm_self_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_self_s*p_status = padico_malloc(sizeof(struct nm_self_s));
  struct nm_self_context_s*p_self_context = puk_context_get_status(context);
  p_status->p_self_context = p_self_context;
  p_status->send.p_pkt     = NULL;
  p_status->send.consumed  = 0;
  p_status->send.posted    = 0;
  p_status->recv.p_pkt     = NULL;
  return p_status;
}

static void nm_self_destroy(void*_status)
{
  struct nm_self_s*p_status = _status;
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_self_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  p_props->profile.latency = 200;
  p_props->profile.bandwidth = 20000;
  p_props->capabilities.supports_data = 1;
  p_props->capabilities.self = 1;
  p_props->capabilities.trk_rdv = 1;
  p_props->capabilities.max_pkt_sends = 1;
  p_props->capabilities.max_pkt_recvs = 1;
  p_props->nickname = "self";
}

static void nm_self_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_self_context_s*p_self_context = padico_malloc(sizeof(struct nm_self_context_s));
  p_self_context->url = padico_strdup("-");
  puk_context_set_status(context, p_self_context);
  *drv_url = p_self_context->url;
  *url_size = strlen(p_self_context->url) + 1;
}

static void nm_self_close(puk_context_t context)
{
  struct nm_self_context_s*p_self_context = puk_context_get_status(context);
  puk_context_set_status(context, NULL);
  padico_free(p_self_context->url);
  padico_free(p_self_context);
}

static void nm_self_connect(void*_status, const void*remote_url, size_t url_size)
{
  /* empty */
}

static void nm_self_connect_async(void*_status, const void*remote_url, size_t url_size)
{
  /* empty */
}

static int nm_self_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_self_s*p_status = _status;
  assert(p_pkt->data.p_data != NULL);
  p_status->send.consumed = 0;
  p_status->send.p_pkt = p_pkt;
  nm_mem_fence();
  p_status->send.posted = 1;
  return NM_ESUCCESS;
}

static int nm_self_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_self_s*p_status = _status;
  if(p_status->send.consumed)
    {
      p_status->send.p_pkt = NULL;
      p_status->send.consumed = 0;
      /* send.posted was reset in receiver to prevent race condition
       * (receive same packet twice) */
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static int nm_self_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_self_s*p_status = _status;
  p_status->recv.p_pkt = p_pkt;
  return NM_ESUCCESS;
}

static int nm_self_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_self_s*p_status = _status;
  if(p_status->send.posted)
    {
      assert(p_status->send.p_pkt != NULL);
      nm_data_copy(p_pkt->data.p_data, p_status->send.p_pkt->data.p_data);
      p_status->send.posted = 0;
      nm_mem_fence();
      p_status->send.consumed = 1;
      p_status->recv.p_pkt = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static int nm_self_recv_pkt_cancel(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_self_s*p_status = _status;
  if(p_status->recv.p_pkt != NULL)
    {
      p_status->recv.p_pkt = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_ENOTPOSTED;
    }
}
