/*
 * NewMadeleine
 * Copyright (C) 2013-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

#include <ucp/api/ucp.h>

static void*nm_ucx_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ucx_destroy(void*status);

static const struct puk_component_driver_s nm_ucx_component =
  {
    .instantiate = &nm_ucx_instantiate,
    .destroy     = &nm_ucx_destroy
  };

static void nm_ucx_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_ucx_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_ucx_close(puk_context_t context);
static void nm_ucx_connect_async(void*_status, const void*remote_url, size_t url_size);
static void nm_ucx_connect_wait(void*_status);
static void nm_ucx_disconnect(void*_status);
static int  nm_ucx_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ucx_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ucx_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_ucx_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ucx_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ucx_recv_pkt_cancel(void*_status, struct nm_pkt_s*p_pkt);

static const struct nm_minidriver_iface_s nm_ucx_minidriver =
  {
    .getprops         = &nm_ucx_getprops,
    .init             = &nm_ucx_init,
    .close            = &nm_ucx_close,
    .connect          = NULL,
    .connect_async    = &nm_ucx_connect_async,
    .connect_wait     = &nm_ucx_connect_wait,
    .disconnect       = &nm_ucx_disconnect,
    .send_pkt_post    = &nm_ucx_send_pkt_post,
    .send_pkt_poll    = &nm_ucx_send_pkt_poll,
    .recv_pkt_post    = &nm_ucx_recv_pkt_post,
    .recv_pkt_poll    = &nm_ucx_recv_pkt_poll,
    .recv_pkt_cancel  = &nm_ucx_recv_pkt_cancel,
    .recv_probe_any   = &nm_ucx_recv_probe_any,
    .recv_wait_any    = NULL,
    .recv_cancel_any  = NULL
  };

static int nm_ucx_convert_status(ucs_status_t status);
static void nm_ucx_err_handler(void*arg, ucp_ep_h ep, ucs_status_t status);

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_ucx,
  puk_component_declare("Minidriver_ucx",
                        puk_component_provides("PadicoComponent", "component", &nm_ucx_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_ucx_minidriver), puk_component_attr("wait_any", "auto")));

/* ********************************************************* */

PUK_VECT_TYPE(nm_ucx_status, struct nm_ucx_s*);

#define NM_UCX_IOV_PREALLOC 8

struct nm_ucx_send_recv_ctx_s
{
  ucs_status_t status;
  void*p_req;
  struct ucp_dt_iov*payload;
  struct ucp_dt_iov payload_prealloc[NM_UCX_IOV_PREALLOC];
};

static inline void nm_ucx_iov_alloc(struct nm_ucx_send_recv_ctx_s*p_req, size_t n)
{
  if(n > NM_UCX_IOV_PREALLOC)
    {
      p_req->payload = padico_malloc(sizeof(struct ucp_dt_iov) * n);
    }
  else
    {
      p_req->payload = p_req->payload_prealloc;
    }
}
static inline void nm_ucx_iov_free(struct nm_ucx_send_recv_ctx_s*p_req)
{
  if(p_req->payload != p_req->payload_prealloc)
    {
      padico_free(p_req->payload);
    }
}

/** 'ucx' driver per-context data. */
struct nm_ucx_context_s
{
  ucp_context_h ucp_context;       /**< ucp context */
  ucp_worker_h  ucp_worker;        /**< ucp worker for communication */
  int local_rank;
  void*p_url;
  ucp_address_t*local_addr;
  size_t local_addr_len;
  struct nm_ucx_status_vect_s p_statuses; /**< array of statuses, indexed by local_tag (remote_rank if known, dynamic otherwise) */
  nm_len_t request_size;           /**< size of ucp requests */
};

/* ********************************************************* */

/** 'ucx' per-instance status. */
struct nm_ucx_s
{
  ucp_ep_h ep;                            /**< ucp endpoint */
  ucp_tag_t local_tag;                    /**< local tag for the peer, used to match recv tags */
  ucp_tag_t remote_tag;                   /**< our tag in the remote host, used for sending */
  struct nm_ucx_context_s*p_ucx_context;  /**< per-context data */
  struct nm_ucx_send_recv_ctx_s send;
  struct nm_ucx_send_recv_ctx_s recv;
  struct
  {
    ucs_status_ptr_t send_status;
    ucs_status_ptr_t recv_status;
  } conn; /**< used for connection establishment only */
};

/* ********************************************************* */

static void*nm_ucx_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ucx_s*p_status = padico_malloc(sizeof(struct nm_ucx_s));
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);
  if(p_ucx_context == NULL)
    {
      NM_FATAL("trying to instantiate before context init.");
    }
  p_status->p_ucx_context = p_ucx_context;
  p_status->send.p_req = padico_malloc(p_ucx_context->request_size);
  p_status->recv.p_req = padico_malloc(p_ucx_context->request_size);
  if(p_ucx_context->local_rank < 0)
    {
      p_status->local_tag = nm_ucx_status_vect_size(&p_ucx_context->p_statuses);
      nm_ucx_status_vect_push_back(&p_ucx_context->p_statuses, p_status);
    }
  return p_status;
}

static void nm_ucx_destroy(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;

  /* Remove instance from global polling */
  nm_ucx_status_vect_itor_t it = nm_ucx_status_vect_find(&p_ucx_context->p_statuses, p_status);
  nm_ucx_status_vect_erase(&p_ucx_context->p_statuses, it);
  padico_free(p_status->send.p_req);
  padico_free(p_status->recv.p_req);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_ucx_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ucx_context_s*p_ucx_context = padico_malloc(sizeof(struct nm_ucx_context_s));
  puk_context_set_status(context, p_ucx_context);

  p_props->capabilities.supports_recv_any = 1;
  p_props->capabilities.supports_data     = 0;
  p_props->capabilities.supports_buf_send = 0;
  p_props->capabilities.supports_buf_recv = 0;
  p_props->capabilities.supports_wait_any = 0;
  p_props->capabilities.supports_iovec    = 1;
  p_props->capabilities.max_pkt_sends     = 1;
  p_props->capabilities.max_pkt_recvs     = 1;
  p_props->profile.latency = 500;
  p_props->profile.bandwidth = 8000;
  if(p_props->hints.kind == nm_trk_large)
    {
      p_props->capabilities.trk_rdv = 1;
    }
  p_props->nickname = "ucx";
}

static void nm_ucx_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);

  ucp_config_t*p_config;
  ucs_status_t status = ucp_config_read(NULL, NULL, &p_config);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_config_read() failed (%s).\n", ucs_status_string(status));
  }

  ucp_params_t ucp_params;
  memset(&ucp_params, 0, sizeof(ucp_params));
  ucp_params.field_mask =
    UCP_PARAM_FIELD_FEATURES |
    UCP_PARAM_FIELD_MT_WORKERS_SHARED;
  ucp_params.features = UCP_FEATURE_TAG | UCP_FEATURE_STREAM;
  ucp_params.mt_workers_shared = 0;
  const char*s_size = puk_context_getattr(context, "session_size");
  int session_size = -1;
  if(s_size != NULL)
    {
      session_size = atoi(s_size);
      if(session_size > 0)
        {
          NM_DISPF("setting hint size = %d\n", session_size);
          ucp_params.field_mask |= UCP_PARAM_FIELD_ESTIMATED_NUM_EPS;
          ucp_params.estimated_num_eps = session_size;
        }
    }
  status = ucp_init(&ucp_params, p_config, &p_ucx_context->ucp_context);

  ucp_config_release(p_config);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_init() failed (%s).\n", ucs_status_string(status));
  }

  ucp_context_attr_t ucp_attr;
  ucp_attr.field_mask = UCP_ATTR_FIELD_REQUEST_SIZE;
  status = ucp_context_query(p_ucx_context->ucp_context, &ucp_attr);
  if(status != UCS_OK)
  {
    NM_FATAL("faild to query context (%s).\n", ucs_status_string(status));
  }
  p_ucx_context->request_size = ucp_attr.request_size;

  ucp_worker_params_t worker_params;
  memset(&worker_params, 0, sizeof(worker_params));
  worker_params.field_mask  = UCP_WORKER_PARAM_FIELD_THREAD_MODE;
  worker_params.thread_mode = UCS_THREAD_MODE_SINGLE;
  status = ucp_worker_create(p_ucx_context->ucp_context, &worker_params, &p_ucx_context->ucp_worker);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_worker_create() failed (%s).\n", ucs_status_string(status));
  }

  status = ucp_worker_get_address(p_ucx_context->ucp_worker,
                                  &p_ucx_context->local_addr,
                                  &p_ucx_context->local_addr_len);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_worker_get_address() failed (%s).\n", ucs_status_string(status));
  }

  nm_ucx_status_vect_init(&p_ucx_context->p_statuses);

  p_ucx_context->local_rank = -1;
  const char*s_rank = puk_context_getattr(context, "rank");
  if( (session_size == -1 && s_rank != NULL) ||
      (session_size != -1 && s_rank == NULL))
    {
      NM_FATAL("inconsistencies in 'session_size' and 'rank' hint in ucx driver.\n");
    }
  if(session_size == -1 && s_rank == NULL)
    {
      NM_WARN("no hint available; using non-scalable connection method.\n");
    }
  if(s_rank != NULL)
    {
      const int rank = atoi(s_rank);
      assert(rank >= 0);
      assert(session_size >= 1);
      p_ucx_context->local_rank = rank;
      nm_ucx_status_vect_reserve(&p_ucx_context->p_statuses, session_size + 1);
      int i;
      for(i = 0; i < session_size; i++)
        {
          nm_ucx_status_vect_put(&p_ucx_context->p_statuses, NULL, 0);
        }
    }
  p_ucx_context->p_url = padico_malloc(sizeof(int) + p_ucx_context->local_addr_len);
  memcpy(p_ucx_context->p_url, &p_ucx_context->local_rank, sizeof(int));
  memcpy(p_ucx_context->p_url + sizeof(int), p_ucx_context->local_addr, p_ucx_context->local_addr_len);
  *drv_url = p_ucx_context->p_url;
  *url_size = sizeof(int) + p_ucx_context->local_addr_len;
}

static void nm_ucx_close(puk_context_t context)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);
  puk_context_set_status(context, NULL);
  padico_free(p_ucx_context->p_url);
  ucp_worker_release_address(p_ucx_context->ucp_worker, p_ucx_context->local_addr);
  ucp_worker_destroy(p_ucx_context->ucp_worker);
  ucp_cleanup(p_ucx_context->ucp_context);
  nm_ucx_status_vect_destroy(&p_ucx_context->p_statuses);
  padico_free(p_ucx_context);
}

static void nm_ucx_connect_async(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  const int remote_rank = *(int*)remote_url;
  ucs_status_t status;
  ucp_ep_params_t ep_params;
  ep_params.field_mask      =
    UCP_EP_PARAM_FIELD_REMOTE_ADDRESS |
    UCP_EP_PARAM_FIELD_ERR_HANDLING_MODE |
    UCP_EP_PARAM_FIELD_ERR_HANDLER |
    UCP_EP_PARAM_FIELD_USER_DATA;
  ep_params.address         = remote_url + sizeof(int);
  ep_params.err_mode        = UCP_ERR_HANDLING_MODE_NONE;
  ep_params.err_handler.cb  = &nm_ucx_err_handler;
  ep_params.err_handler.arg = NULL;
  ep_params.user_data       = p_status;
  status = ucp_ep_create(p_ucx_context->ucp_worker, &ep_params, &p_status->ep);
  if(status != UCS_OK)
    {
      NM_FATAL("ucp_ep_create() failed (%s).\n", ucs_status_string(status));
    }
  if((remote_rank >= 0) && (p_ucx_context->local_rank >= 0))
    {
      /* hints available; tags are computed without exchange */
      p_status->local_tag = remote_rank;
      p_status->remote_tag = p_ucx_context->local_rank;
      nm_ucx_status_vect_put(&p_ucx_context->p_statuses, p_status, remote_rank);
    }
  else if((remote_rank < 0) && (p_ucx_context->local_rank < 0))
    {
      /* ** exchange tags */
      ucp_request_param_t sparam = { .op_attr_mask = 0 };
      p_status->conn.send_status = ucp_stream_send_nbx(p_status->ep, &p_status->local_tag, sizeof(ucp_tag_t), &sparam);
      if(UCS_PTR_IS_ERR(p_status->conn.send_status))
        {
          NM_FATAL("error while sending tag (%s)\n", ucs_status_string(UCS_PTR_STATUS(p_status->conn.send_status)));
        }
      size_t length;
      ucp_request_param_t rparam =
        {
          .op_attr_mask = UCP_OP_ATTR_FIELD_FLAGS,
          .flags = UCP_STREAM_RECV_FLAG_WAITALL
        };
      p_status->conn.recv_status = ucp_stream_recv_nbx(p_status->ep, &p_status->remote_tag, sizeof(ucp_tag_t), &length, &rparam);
      if(UCS_PTR_IS_ERR(p_status->conn.recv_status))
        {
          NM_FATAL("error while receiving tag (%s)\n", ucs_status_string(UCS_PTR_STATUS(p_status->conn.recv_status)));
        }
    }
  else
    {
      NM_FATAL("inconsistency in ranks while connecting UCX; local_rank = %d; remote_rank = %d.\n",
               p_ucx_context->local_rank, remote_rank);
    }
}

static void nm_ucx_connect_wait(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  if(p_ucx_context->local_rank < 0)
    {
      /* wait for completion of tag exchange */
      ucs_status_t status;
      if(p_status->conn.send_status != NULL)
        {
          do
            {
              ucp_worker_progress(p_ucx_context->ucp_worker);
              status = ucp_request_check_status(p_status->conn.send_status);
            }
          while(status == UCS_INPROGRESS);
          ucp_request_free(p_status->conn.send_status);
          if(status != UCS_OK)
            {
              NM_FATAL("error while sending tag (%d)\n", status);
            }
        }
      if(p_status->conn.recv_status != NULL)
        {
          do
            {
              ucp_worker_progress(p_ucx_context->ucp_worker);
              status = ucp_request_check_status(p_status->conn.recv_status);
            }
          while(status == UCS_INPROGRESS);
          ucp_request_free(p_status->conn.recv_status);
          if(status != UCS_OK)
            {
              NM_FATAL("error while receiving tag (%d)\n", status);
            }
        }
    }
}

static void nm_ucx_disconnect(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  void*close_req = ucp_ep_close_nb(p_status->ep, UCP_EP_CLOSE_MODE_FLUSH);
  if(UCS_PTR_IS_PTR(close_req))
  {
    ucs_status_t status;
    do
    {
      ucp_worker_progress(p_ucx_context->ucp_worker);
      status = ucp_request_check_status(close_req);
    }
    while(status == UCS_INPROGRESS);
    ucp_request_free(close_req);
  }
  else if(UCS_PTR_STATUS(close_req) != UCS_OK)
  {
    ucs_status_t status = UCS_PTR_STATUS(close_req);
    NM_FATAL("failed to close ep %p (%s)\n", (void*)p_status->ep, ucs_status_string(status));
  }
}

static int nm_ucx_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  assert(p_pkt->iov.v != NULL);
  int i;
  nm_ucx_iov_alloc(&p_status->send, p_pkt->iov.n);
  if(p_pkt->iov.n > 1)
    {
      for(i = 0; i < p_pkt->iov.n; i++)
        {
          p_status->send.payload[i].buffer = p_pkt->iov.v[i].iov_base;
          p_status->send.payload[i].length = p_pkt->iov.v[i].iov_len;
        }
      p_status->send.status = ucp_tag_send_nbr(p_status->ep, p_status->send.payload, p_pkt->iov.n, UCP_DATATYPE_IOV,
                                               p_status->remote_tag,
                                               p_status->send.p_req + p_ucx_context->request_size);
    }
  else
    {
      assert(p_pkt->iov.n == 1);
      p_status->send.status = ucp_tag_send_nbr(p_status->ep, p_pkt->iov.v[0].iov_base, p_pkt->iov.v[0].iov_len, ucp_dt_make_contig(1),
                                               p_status->remote_tag,
                                               p_status->send.p_req + p_ucx_context->request_size);
    }
  if((p_status->send.status != UCS_OK) && (p_status->send.status != UCS_INPROGRESS))
    {
      NM_WARN("error %d while posting send (%s)\n",
              p_status->send.status, ucs_status_string(p_status->send.status));
      return nm_ucx_convert_status(p_status->send.status);
    }
  else
    {
      return NM_ESUCCESS;
    }
}

static int nm_ucx_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;

  /* Operation was completed immediately */
  if(p_status->send.status == UCS_OK)
  {
    nm_ucx_iov_free(&p_status->send);
    return NM_ESUCCESS;
  }
  ucp_worker_progress(p_ucx_context->ucp_worker);
  p_status->send.status = ucp_request_check_status(p_status->send.p_req + p_ucx_context->request_size);
  if(p_status->send.status == UCS_INPROGRESS)
    {
      return -NM_EAGAIN;
    }
  else
    {
      nm_ucx_iov_free(&p_status->send);
      if(p_status->send.status == UCS_OK)
        {
          return NM_ESUCCESS;
        }
      else
        {
          NM_WARN("error %d while sending (%s)\n",
                  p_status->send.status, ucs_status_string(p_status->send.status));
          return nm_ucx_convert_status(p_status->send.status);
        }
    }
}

static int nm_ucx_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  assert(p_pkt->iov.v != NULL);
  nm_ucx_iov_alloc(&p_status->recv, p_pkt->iov.n);
  if(p_pkt->iov.n > 1)
    {
      int i;
      for(i = 0; i < p_pkt->iov.n; i++)
        {
          p_status->recv.payload[i].buffer = p_pkt->iov.v[i].iov_base;
          p_status->recv.payload[i].length = p_pkt->iov.v[i].iov_len;
        }
      p_status->recv.status = ucp_tag_recv_nbr(p_ucx_context->ucp_worker,
                                               p_status->recv.payload, p_pkt->iov.n, UCP_DATATYPE_IOV,
                                               p_status->local_tag, (ucp_tag_t)0xFFFFFFFF,
                                               p_status->recv.p_req + p_ucx_context->request_size);
    }
  else
    {
      p_status->recv.status = ucp_tag_recv_nbr(p_ucx_context->ucp_worker,
                                               p_pkt->iov.v[0].iov_base, p_pkt->iov.v[0].iov_len, ucp_dt_make_contig(1),
                                               p_status->local_tag, (ucp_tag_t)0xFFFFFFFF,
                                               p_status->recv.p_req + p_ucx_context->request_size);
    }
  if((p_status->recv.status != UCS_OK) && (p_status->recv.status != UCS_INPROGRESS))
    {
      NM_WARN("error %d while posting receive (%s)\n",
              p_status->recv.status, ucs_status_string(p_status->recv.status));
      return nm_ucx_convert_status(p_status->recv.status);
    }
  else
    {
      return NM_ESUCCESS;
    }
}

static int nm_ucx_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  p_status->recv.status = ucp_request_check_status(p_status->recv.p_req + p_ucx_context->request_size);
  if(p_status->recv.status == UCS_OK)
  {
#ifdef DEBUG
    ucp_tag_recv_info_t info;
    ucs_status_t status = ucp_tag_recv_request_test(p_status->recv.p_req + p_ucx_context->request_size, &info);
    assert(status == UCS_OK);
    assert(info.sender_tag == p_status->local_tag);
#endif /* DEBUG */
    nm_ucx_iov_free(&p_status->recv);
    return NM_ESUCCESS;
  }
  ucp_worker_progress(p_ucx_context->ucp_worker);
  p_status->recv.status = ucp_request_check_status(p_status->recv.p_req + p_ucx_context->request_size);
  if(p_status->recv.status == UCS_INPROGRESS)
    {
      return -NM_EAGAIN;
    }
  else
    {
      nm_ucx_iov_free(&p_status->recv);
      if(p_status->recv.status == UCS_OK)
        {
          return NM_ESUCCESS;
        }
      else
        {
          NM_WARN("error %d while receiving (%s)\n",
                  p_status->recv.status, ucs_status_string(p_status->recv.status));
          return nm_ucx_convert_status(p_status->recv.status);
        }
    }
}

static int nm_ucx_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(p_context);
  ucp_worker_progress(p_ucx_context->ucp_worker);
  ucp_tag_recv_info_t info;
  ucp_tag_message_h msg = ucp_tag_probe_nb(p_ucx_context->ucp_worker, 0 /* tag */, 0 /* tag_mask */,
                                           0 /* remove */, &info);
  if(msg == NULL)
    {
      return -NM_EAGAIN;
    }
  else
    {
      const ucp_tag_t tag = info.sender_tag;
      if((tag < 0) || (tag >= nm_ucx_status_vect_size(&p_ucx_context->p_statuses)))
        return -NM_EUNKNOWN;
      struct nm_ucx_s*p_status = nm_ucx_status_vect_at(&p_ucx_context->p_statuses, tag);
      *_status = p_status;
      return NM_ESUCCESS;
    }
}

static int nm_ucx_recv_pkt_cancel(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  ucp_request_cancel(p_ucx_context->ucp_worker, p_status->recv.p_req);
  ucs_status_t status = UCS_PTR_STATUS(p_status->recv.p_req);
  if(status == UCS_OK || status == UCS_INPROGRESS)
    {
      /* receive is in progress, it is too late to cancel */
      //      return -NM_EALREADY;
      return NM_ESUCCESS;
    }
  else if(status == UCS_ERR_CANCELED)
    {
      /* cancellation ok */
      return NM_ESUCCESS;
    }
  else
    {
      return nm_ucx_convert_status(status);
    }
}

/******************** Sub functions ********************/


static void nm_ucx_err_handler(void*arg, ucp_ep_h ep, ucs_status_t status)
{
  char*str = arg;
  NM_FATAL("[%s] error handling callback was invoked with status %d (%s)\n",
          str, status, ucs_status_string(status));
}

static int nm_ucx_convert_status(ucs_status_t status)
{
  switch(status)
  {
    case UCS_OK:
      return NM_ESUCCESS;

    case UCS_INPROGRESS:
      return -NM_EAGAIN;

    case UCS_ERR_CANCELED:
      return -NM_ECANCELED;

    case UCS_ERR_TIMED_OUT:
      return -NM_ETIMEDOUT;

    case UCS_ERR_NO_MEMORY:
      NM_WARN("message aborted. status: (%s)\n", ucs_status_string(status));
      return -NM_ENOMEM;

    default:  /* Any problem happened. */
      NM_WARN("message aborted. status: (%s)\n", ucs_status_string(status));
      return -NM_EUNKNOWN;
  }
}
