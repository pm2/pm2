/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <nm_private.h>
#ifdef NMAD_HWLOC
#include <hwloc.h>
#include <hwloc/openfabrics-verbs.h>
#endif /* NMAD_HWLOC */

PADICO_MODULE_HOOK(NewMad_ibverbs_common);

/* ** IB UD ************************************************ */

void nm_ibverbs_ud_ep_open(struct nm_ibverbs_ud_ep_s*p_ud_ep, struct nm_ibverbs_hca_s*p_hca)
{
  p_ud_ep->p_hca = p_hca;

  /* init inbound CQ */
  p_ud_ep->if_cq = ibv_create_cq(p_ud_ep->p_hca->context, NM_IBVERBS_UD_DEPTH, NULL, NULL, 0);
  if(p_ud_ep->if_cq == NULL)
    {
      NM_FATAL("ibverbs: cannot create in CQ (%s).\n", strerror(errno));
    }
  /* init outbound CQ */
  p_ud_ep->of_cq = ibv_create_cq(p_ud_ep->p_hca->context, NM_IBVERBS_UD_DEPTH, NULL, NULL, 0);
  if(p_ud_ep->of_cq == NULL)
    {
      NM_FATAL("ibverbs: cannot create out CQ (%s).\n", strerror(errno));
    }
  /* create QP */
  struct ibv_qp_init_attr qp_init_attr =
    {
      .send_cq = p_ud_ep->of_cq,
      .recv_cq = p_ud_ep->if_cq,
      .srq     = NULL,
      .cap     = {
        .max_send_wr     = NM_IBVERBS_TX_DEPTH,
        .max_recv_wr     = NM_IBVERBS_UD_DEPTH,
        .max_send_sge    = NM_IBVERBS_MAX_SG_SQ,
        .max_recv_sge    = NM_IBVERBS_MAX_SG_RQ,
        .max_inline_data = NM_IBVERBS_MAX_INLINE
      },
      .qp_type = IBV_QPT_UD
    };
  p_ud_ep->qp = ibv_create_qp(p_ud_ep->p_hca->pd, &qp_init_attr);
  if(p_ud_ep->qp == NULL)
    {
      NM_FATAL("ibverbs: couldn't create UD QP (%s)\n", strerror(errno));
    }
  /* get local address */
  memset(&p_ud_ep->local_addr, 0, sizeof(struct nm_ibverbs_addr_s)); /* clear struct padding, if any */
  p_ud_ep->local_addr.lid = p_ud_ep->p_hca->lid;
  p_ud_ep->local_addr.qpn = p_ud_ep->qp->qp_num;
  p_ud_ep->local_addr.psn = lrand48() & 0xffffff;

  NM_DISPF("IB UD create- lid = %d; qpn = %d; psn = %d (%d)\n",
           p_ud_ep->local_addr.lid, p_ud_ep->local_addr.qpn, p_ud_ep->local_addr.psn,
           (int)sizeof(struct nm_ibverbs_addr_s));

  /* modify QP to state RESET */
  struct ibv_qp_attr attr_qp_reset =
    {
      .qp_state      = IBV_QPS_RESET
    };
  int rc = ibv_modify_qp(p_ud_ep->qp, &attr_qp_reset,
                         IBV_QP_STATE);
  if(rc != 0)
    {
      NM_FATAL("failed to modify QP to RESET (rc=%d; %s).\n", rc, strerror(rc));
    }
  /* modify QP to state INIT */
  struct ibv_qp_attr attr_qp_init =
    {
      .qp_state      = IBV_QPS_INIT,
      .pkey_index    = 0,
      .port_num      = p_ud_ep->p_hca->port,
      .qkey          = 0x11111111
    };
  rc = ibv_modify_qp(p_ud_ep->qp, &attr_qp_init,
                     IBV_QP_STATE              |
                     IBV_QP_PKEY_INDEX         |
                     IBV_QP_PORT               |
                     IBV_QP_QKEY);
  if(rc != 0)
    {
      NM_FATAL("failed to modify QP to INIT (rc=%d; %s).\n", rc, strerror(rc));
    }
  /* modify QP to state RTR */
  struct ibv_qp_attr attr_qp_rtr =
    {
      .qp_state      = IBV_QPS_RTR
    };
  rc = ibv_modify_qp(p_ud_ep->qp, &attr_qp_rtr,
                     IBV_QP_STATE);
  if(rc != 0)
    {
      NM_FATAL("dev = %s; port = %d; failed to modify QP to RTR (%s)\n",
               ibv_get_device_name(p_ud_ep->p_hca->ib_dev),
               p_ud_ep->p_hca->port, strerror(rc));
    }
  /* modify QP to state RTS */
  struct ibv_qp_attr attr_qp_rts =
    {
      .qp_state      = IBV_QPS_RTS,
      .sq_psn        = p_ud_ep->local_addr.psn
    };
  rc = ibv_modify_qp(p_ud_ep->qp, &attr_qp_rts,
                     IBV_QP_STATE              |
                     IBV_QP_SQ_PSN);
  if(rc != 0)
    {
      NM_FATAL("failed to modify QP to RTS (rc=%d; %s)\n", rc, strerror(rc));
    }
}

void nm_ibverbs_ud_ep_close(struct nm_ibverbs_ud_ep_s*p_ud_ep)
{
  /* TODO- close QP & CQs */
}

void nm_ibverbs_ud_send_post(struct nm_ibverbs_ud_send_s*p_send,
                             struct nm_ibverbs_ud_ep_s*p_ud_ep, void*buf, size_t size,
                             const struct nm_ibverbs_addr_s*p_remote_addr)
{
  p_send->p_ud_ep = p_ud_ep;
  /* create AH */
  struct ibv_ah_attr ah_attr =
    {
     .is_global      = 0,
     .dlid           = p_remote_addr->lid,
     .sl             = 0,
     .src_path_bits  = 0,
     .port_num       = p_ud_ep->p_hca->port
    };
  p_send->ah = ibv_create_ah(p_ud_ep->p_hca->pd, &ah_attr);
  if(!p_send->ah)
    {
      NM_FATAL("failed to create AH for UD send (%s)\n", strerror(errno));
    }
  p_send->mr = ibv_reg_mr(p_ud_ep->p_hca->pd, buf, size + GRH_OFFSET,
                          IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE);
  if(!p_send->mr)
    {
      NM_FATAL("couldn't allocate MR (%s)\n", strerror(errno));
    }
  /* create WR & post */
  struct ibv_sge list =
    {
      .addr    = (uintptr_t) buf + GRH_OFFSET,
      .length  = size,
      .lkey    = p_send->mr->lkey
    };
  struct ibv_send_wr wr =
    {
      .wr_id      = NM_IBVERBS_WRID_UD_SEND,
      .sg_list    = &list,
      .num_sge    = 1,
      .opcode     = IBV_WR_SEND,
      .next       = NULL,
      .send_flags = IBV_SEND_SIGNALED,
      .wr.ud.ah          = p_send->ah,
      .wr.ud.remote_qpn  = p_remote_addr->qpn,
      .wr.ud.remote_qkey = 0x11111111
    };
  struct ibv_send_wr*bad_wr;
  int rc = ibv_post_send(p_ud_ep->qp, &wr, &bad_wr);
  if(rc)
    {
      NM_FATAL("post send failed (%d; %s)\n", rc, strerror(rc));
    }
}

int nm_ibverbs_ud_send_poll(struct nm_ibverbs_ud_send_s*p_send)
{
  /* poll on CQ */
  struct ibv_wc wc;
  int ne = ibv_poll_cq(p_send->p_ud_ep->of_cq, 1, &wc);
  if(ne < 0)
    {
      NM_FATAL("poll out CQ failed.\n");
    }
  if(ne == 0)
    {
      return -1;
    }
  if(wc.status != IBV_WC_SUCCESS)
    {
      NM_FATAL("WC send failed (status=%d; %s)\n",
              wc.status, nm_ibverbs_status_strings[wc.status]);
    }
  ibv_destroy_ah(p_send->ah);
  ibv_dereg_mr(p_send->mr);
  return 0;
}

void nm_ibverbs_ud_send(struct nm_ibverbs_ud_ep_s*p_ud_ep, void*buf, size_t size,
                        const struct nm_ibverbs_addr_s*p_remote_addr)
{
  struct nm_ibverbs_ud_send_s send;
  nm_ibverbs_ud_send_post(&send, p_ud_ep, buf, size, p_remote_addr);
  int rc = -1;
  do
    {
      rc = nm_ibverbs_ud_send_poll(&send);
    }
  while(rc != 0);
}

void nm_ibverbs_ud_recv_post(struct nm_ibverbs_ud_recv_s*p_recv,
                             struct nm_ibverbs_ud_ep_s*p_ud_ep, void*buf, size_t size)
{
  p_recv->p_ud_ep = p_ud_ep;
  p_recv->mr = ibv_reg_mr(p_ud_ep->p_hca->pd, buf, size + GRH_OFFSET,
                          IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE);
  if(!p_recv->mr)
    {
      NM_FATAL("couldn't allocate MR\n");
    }
  /* create WR & post */
  struct ibv_sge list =
    {
      .addr   = (uintptr_t) buf,
      .length = size + GRH_OFFSET,
      .lkey   = p_recv->mr->lkey
    };
  struct ibv_recv_wr wr =
    {
      .wr_id    = NM_IBVERBS_WRID_UD_RECV,
      .sg_list  = &list,
      .num_sge  = 1,
      .next     = NULL
    };
  struct ibv_recv_wr*bad_wr_recv;
  int rc = ibv_post_recv(p_recv->p_ud_ep->qp, &wr, &bad_wr_recv);
  if(rc)
    {
      NM_FATAL("post recv failed.\n");
    }
}

int nm_ibverbs_ud_recv_poll(struct nm_ibverbs_ud_recv_s*p_recv)
{
  /* poll on CQ */
  struct ibv_wc wc;
  int ne = ibv_poll_cq(p_recv->p_ud_ep->if_cq, 1, &wc);
  if(ne < 0)
    {
      NM_FATAL("poll in CQ failed.\n");
    }
  if(ne == 0)
    {
      return -1;
    }
  if(ne != 1 || wc.status != IBV_WC_SUCCESS)
    {
      NM_FATAL("WC recv failed (status=%d; %s)\n",
              wc.status, nm_ibverbs_status_strings[wc.status]);
    }
  ibv_dereg_mr(p_recv->mr);
  return 0;
}

void nm_ibverbs_ud_recv(struct nm_ibverbs_ud_ep_s*p_ud_ep, void*buf, size_t size)
{
  struct nm_ibverbs_ud_recv_s recv;
  nm_ibverbs_ud_recv_post(&recv, p_ud_ep, buf, size);
  int rc = -1;
  do
    {
      rc = nm_ibverbs_ud_recv_poll(&recv);
    }
  while(rc != 0);
}

void nm_ibverbs_ud_recv_pool_post(struct nm_ibverbs_ud_recv_pool_s*p_recv,
                                  struct nm_ibverbs_ud_ep_s*p_ud_ep, void*buf, size_t size, int depth)
{
  p_recv->p_ud_ep = p_ud_ep;
  p_recv->buf = buf;
  p_recv->size = size;
  p_recv->depth = depth;
  p_recv->mr = ibv_reg_mr(p_ud_ep->p_hca->pd, p_recv->buf, depth * (size + GRH_OFFSET),
                          IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE);
  if(!p_recv->mr)
    {
      NM_FATAL("couldn't allocate MR\n");
    }
  /* create WR & post */
  assert(depth < NM_IBVERBS_WRID_UD_RECV_POOL_LAST - NM_IBVERBS_WRID_UD_RECV_POOL_FIRST);
  int i;
  for(i = 0; i < depth; i++)
    {
      nm_ibverbs_ud_recv_pool_reload(p_recv, i);
    }
}

int nm_ibverbs_ud_recv_pool_poll(struct nm_ibverbs_ud_recv_pool_s*p_recv)
{
  /* poll on CQ */
  struct ibv_wc wc = { 0 };
  int ne = ibv_poll_cq(p_recv->p_ud_ep->if_cq, 1, &wc);
  if(ne < 0)
    {
      NM_FATAL("poll in CQ failed.\n");
    }
  if(ne == 0)
    {
      return -1;
    }
  if(ne != 1 || wc.status != IBV_WC_SUCCESS)
    {
      NM_FATAL("WC recv failed (status=%d; %s)\n",
              wc.status, nm_ibverbs_status_strings[wc.status]);
    }
  const int i = wc.wr_id - NM_IBVERBS_WRID_UD_RECV_POOL_FIRST;
  return i;
}

void nm_ibverbs_ud_recv_pool_reload(struct nm_ibverbs_ud_recv_pool_s*p_recv, int i)
{
  struct ibv_sge list =
    {
      .addr   = (uintptr_t)(p_recv->buf + i * (p_recv->size + GRH_OFFSET)),
      .length = p_recv->size + GRH_OFFSET,
      .lkey   = p_recv->mr->lkey
    };
  struct ibv_recv_wr wr =
    {
      .wr_id    = NM_IBVERBS_WRID_UD_RECV_POOL_FIRST + i,
      .sg_list  = &list,
      .num_sge  = 1,
      .next     = NULL
    };
  struct ibv_recv_wr*bad_wr_recv;
  int rc = ibv_post_recv(p_recv->p_ud_ep->qp, &wr, &bad_wr_recv);
  if(rc)
    {
      NM_FATAL("post recv failed.\n");
    }
}

void nm_ibverbs_ud_recv_pool_delete(struct nm_ibverbs_ud_recv_pool_s*p_recv)
{
  /* TODO: send to self to cancel all pre-posted requests before we can dereg memory */
  if(p_recv->mr)
    {
      ibv_dereg_mr(p_recv->mr);
    }
  p_recv->mr = NULL;
}
