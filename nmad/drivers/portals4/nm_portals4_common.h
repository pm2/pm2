/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <portals4.h>

#ifndef NM_PORTALS4_COMMON_H
#define NM_PORTALS4_COMMON_H

/* portals4 cheat sheet
 *
 * CT counting event
 * EQ event queue
 * LE list entry
 * MD memory descriptor
 * NI network interface
 * PT portal table
 * ME matched entry
 */


struct nm_portals4_addr_s
{
  ptl_process_t process_id;
  ptl_pt_index_t pt_index;
};


static inline const char*nm_portals4_strerror(int err)
{
  switch(err)
    {
    case PTL_OK:
      return "PTL_OK- Success";
      break;
    case PTL_ARG_INVALID:
      return "PTL_ARG_INVALID- One of the arguments is invalid.";
      break;
    case PTL_CT_NONE_REACHED:
      return "PTL_CT_NONE_REACHED- Timeout reached before any counting event reached the test.";
      break;
    case PTL_EQ_DROPPED:
      return "PTL_EQ_DROPPED- At least one event has been dropped.";
      break;
    case PTL_EQ_EMPTY:
      return "PTL_EQ_EMPTY- No events available in an event queue.";
      break;
    case PTL_FAIL:
      return "PTL_FAIL- Non-specific error";
      break;
    case PTL_IGNORED:
      return "PTL_IGNORED- Logical map set failed.";
      break;
    case PTL_IN_USE:
      return "PTL_IN_USE- The specified resource is currently in use.";
      break;
    case PTL_INTERRUPTED:
      return "PTL_INTERRUPTED- Wait/get operation was interrupted.";
      break;
    case PTL_LIST_TOO_LONG:
      return "PTL_LIST_TOO_LONG- The resulting list is too long (interface-dependent).";
      break;
    case PTL_NO_INIT:
      return "PTL_NO_INIT- Init has not yet completed successfully.";
      break;
    case PTL_NO_SPACE:
      return "PTL_NO_SPACE- Sufficient memory for action was not available.";
      break;
    case PTL_PID_IN_USE:
      return "PTL_PID_IN_USE- PID is in use.";
      break;
    case PTL_PT_FULL:
      return "PTL_PT_FULL- Portal table has no empty entries.";
      break;
    case PTL_PT_EQ_NEEDED:
      return "PTL_PT_EQ_NEEDED- Flow control is enabled and there is no EQ provided.";
      break;
    case PTL_PT_IN_USE:
      return "PTL_PT_IN_USE- Portal table index is busy.";
    break;
    default:
      return "unknown error";
      break;
    }
}

#define nm_portals4_check_error(RC, MSG)                                \
  {                                                                     \
    if((RC) != PTL_OK)                                                  \
      {                                                                 \
        padico_string_t s_err = padico_string_new();                    \
        padico_string_printf(s_err, "%s- rc = %d (%s)\n", (MSG), rc, nm_portals4_strerror(rc)); \
        NM_FATAL(padico_string_get(s_err));                             \
        padico_string_delete(s_err);                                    \
      }                                                                 \
  }                                                                     \

static inline const char*nm_portals4_nifail_strerror(ptl_ni_fail_t ni_fail)
{
  switch(ni_fail)
    {
    case PTL_NI_OK:
      return "PTL_NI_OK- Used in successful end events to indicate that there has been no failure";
      break;
    case PTL_NI_UNDELIVERABLE:
      return "PTL_NI_UNDELIVERABLE- Indicates a system failure that prevents message delivery.";
      break;
    case PTL_NI_DROPPED:
      return "PTL_NI_DROPPED- Indicates that a message was dropped for some reason.";
      break;
    case PTL_NI_PT_DISABLED:
      return "PTL_NI_PT_DISABLED- Indicates that the portal table entry at the target was disabled "
        "and did not process the operation, either because the entry was "
        "disabled or because the entry provides flow control and a resource "
        "has been exhausted.";
      break;
    case PTL_NI_PERM_VIOLATION:
      return "PTL_NI_PERM_VIOLATION- Indicates that the remote Portals addressing indicated a "
        "permissions violation for this message.";
      break;
    case PTL_NI_OP_VIOLATION:
      return "PTL_NI_OP_VIOLATION- Indicates that the remote Portals addressing indicated an "
        "operations violation for this message.";
      break;
    case PTL_NI_NO_MATCH:
      return "PTL_NI_NO_MATCH- Indicates that the search did not find an entry in the unexpected list.";
      break;
    case PTL_NI_SEGV:
      return "PTL_NI_SEGV- Indicates that a message attempted to access inaccessible memory";
      break;
    default:
      return "unknown";
      break;
    }
}


#endif /* NM_PORTALS4_COMMON_H */
