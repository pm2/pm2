/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <portals4.h>

#include "nm_portals4_common.h"
#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

/** size of the event queue for recv- this queue is shared accross all connections.
 */
#define NM_PORTALS4_LARGE_RECVQUEUE_LEN 10000

/** size of the event queue for send- this queue is per connection
 */
#define NM_PORTALS4_LARGE_SENDQUEUE_LEN 16

static void*nm_portals4_large_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_portals4_large_destroy(void*status);

static const struct puk_component_driver_s nm_portals4_large_component =
  {
    .instantiate = &nm_portals4_large_instantiate,
    .destroy     = &nm_portals4_large_destroy
  };

static void nm_portals4_large_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_portals4_large_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_portals4_large_close(puk_context_t context);
static void nm_portals4_large_connect_async(void*_status, const void*remote_url, size_t url_size);
static void nm_portals4_large_disconnect(void*_status);
static void nm_portals4_large_send_pkt_prefetch(void*_status, struct nm_pkt_s*p_pkt);
static void nm_portals4_large_send_pkt_unfetch(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_portals4_large_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_portals4_large_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_portals4_large_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_portals4_large_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);

static const struct nm_minidriver_iface_s nm_portals4_large_minidriver =
  {
    .getprops          = &nm_portals4_large_getprops,
    .init              = &nm_portals4_large_init,
    .close             = &nm_portals4_large_close,
    .connect           = NULL,
    .connect_async     = &nm_portals4_large_connect_async,
    .connect_wait      = NULL,
    .disconnect        = &nm_portals4_large_disconnect,
    .send_pkt_prefetch = &nm_portals4_large_send_pkt_prefetch,
    .send_pkt_unfetch  = &nm_portals4_large_send_pkt_unfetch,
    .send_pkt_post     = &nm_portals4_large_send_pkt_post,
    .send_pkt_poll     = &nm_portals4_large_send_pkt_poll,
    .recv_pkt_post     = &nm_portals4_large_recv_pkt_post,
    .recv_pkt_poll     = &nm_portals4_large_recv_pkt_poll
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_portals4_large,
  puk_component_declare("Minidriver_portals4_large",
                        puk_component_provides("PadicoComponent", "component", &nm_portals4_large_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_portals4_large_minidriver)));

/* ********************************************************* */

/** an entry of the list of the pre-registered MD */
struct nm_portals4_large_md_entry_s
{
  struct nm_pkt_s*p_pkt;
  ptl_handle_md_t md_handle;                /**< memory descriptor used for sending */
};


/** 'portals4' driver per-context data. */
struct nm_portals4_large_context_s
{
  ptl_ni_limits_t ni_limits;
  ptl_handle_ni_t ni_handle;        /**< handle to the network interface */
  struct nm_portals4_addr_s local;  /**< local address */
  ptl_handle_eq_t global_eq_handle; /**< global event queue for recv */
#ifdef NMAD_PROFILE
  struct
  {
    unsigned long long n_prefetch_success;
    unsigned long long n_prefetch_missed;
    unsigned long long n_pt_disabled;
  } profiling;
#endif /* NMAD_PROFILE */
};

/** 'portals4' per-instance status. */
struct nm_portals4_large_s
{
  struct nm_portals4_large_context_s*p_portals4_context;
  struct nm_portals4_addr_s peer;
  struct
  {
    nm_len_t length;            /**< length of message beeing sent */
    ptl_handle_eq_t eq_handle;  /**< event queue for send events */
    struct nm_portals4_large_md_entry_s*p_entry;   /**< MD entry used for sending */
    struct nm_portals4_large_md_entry_s md_entry;  /**< preallocated MD entry, used in case of cache miss */
  } send;
  struct
  {
    ptl_handle_me_t me_handle;  /**< matching entry for receive */
    int done;
    int posted;                 /**< whether the recv is already posted (i.e. ME linked) */
  } recv;
};

/* ********************************************************* */

static void*nm_portals4_large_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_portals4_large_s*p_status = padico_malloc(sizeof(struct nm_portals4_large_s));
  struct nm_portals4_large_context_s*p_portals4_context = puk_context_get_status(context);
  p_status->p_portals4_context = p_portals4_context;
  p_status->send.length = NM_LEN_UNDEFINED;
  return p_status;
}

static void nm_portals4_large_destroy(void*_status)
{
  struct nm_portals4_large_s*p_status = _status;
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_portals4_large_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_portals4_large_context_s*p_portals4_context = padico_malloc(sizeof(struct nm_portals4_large_context_s));
  puk_context_set_status(context, p_portals4_context);
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &p_portals4_context->profiling.n_prefetch_success, 0,
                       "Minidriver_portals4_large", "number of packets that were successfuly prefetched before send",
                       "Minidriver_portals4_large.context-%p.n_prefetch_success", p_portals4_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_portals4_context->profiling.n_prefetch_missed, 0,
                       "Minidriver_portals4_large", "number of packets that were not prefetched before send",
                       "Minidriver_portals4_large.context-%p.n_prefetch_missed", p_portals4_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_portals4_context->profiling.n_pt_disabled, 0,
                       "Minidriver_portals4_large", "number of times PT was disabled by flow-control",
                       "Minidriver_portals4_large.context-%p.n_pt_disabled", p_portals4_context);
#endif /* NMAD_PROFILE */
  int rc = PtlInit();
  nm_portals4_check_error(rc, "failed to initialise portals4 library");

  rc = PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PTL_PID_ANY, NULL,
                 &p_portals4_context->ni_limits, &p_portals4_context->ni_handle);
  nm_portals4_check_error(rc, "failed to initialise portals4 network interface");

  NM_DISPF("portals4_large init- max_unexpected_headers = %d; max_volatile_size = %d; max_msg_size = %d; max_mds = %d\n",
           p_portals4_context->ni_limits.max_unexpected_headers, (int)p_portals4_context->ni_limits.max_volatile_size,
           (int)p_portals4_context->ni_limits.max_msg_size, p_portals4_context->ni_limits.max_mds);

  PtlGetPhysId(p_portals4_context->ni_handle, &p_portals4_context->local.process_id);

  p_props->profile.latency = 2000;
  p_props->profile.bandwidth = 10000;
  p_props->capabilities.supports_iovec = 1;
  p_props->capabilities.supports_send_prefetch = 1;
  p_props->capabilities.max_msg_size = p_portals4_context->ni_limits.max_msg_size;
  p_props->capabilities.trk_rdv = 1; /* needs rdv for flow control */
  p_props->capabilities.max_pkt_sends = 1;
  p_props->capabilities.max_pkt_recvs = 1;
  p_props->nickname = "portals4-large";
}

static void nm_portals4_large_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_portals4_large_context_s*p_portals4_context = puk_context_get_status(context);
  int rc = PtlEQAlloc(p_portals4_context->ni_handle, NM_PORTALS4_LARGE_RECVQUEUE_LEN, &p_portals4_context->global_eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");

  rc = PtlPTAlloc(p_portals4_context->ni_handle, 0 /* PTL_PT_FLOWCTRL */, /* no flow control needed, since we have rdv */
                  p_portals4_context->global_eq_handle,
                  PTL_PT_ANY, &p_portals4_context->local.pt_index);
  nm_portals4_check_error(rc, "failed to allocate portal table");

  *drv_url = &p_portals4_context->local;
  *url_size = sizeof(p_portals4_context->local);
}

static void nm_portals4_large_close(puk_context_t context)
{
  struct nm_portals4_large_context_s*p_portals4_context = puk_context_get_status(context);
  PtlNIFini(p_portals4_context->ni_handle);
  PtlFini();
  puk_context_set_status(context, NULL);
  padico_free(p_portals4_context);
}

static void nm_portals4_large_connect_async(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_portals4_large_s*p_status = _status;
  struct nm_portals4_large_context_s*p_portals4_context = p_status->p_portals4_context;
  assert(url_size == sizeof(struct nm_portals4_addr_s));
  const struct nm_portals4_addr_s*p_peer = remote_url;
  p_status->peer = *p_peer;
  int rc = PtlEQAlloc(p_portals4_context->ni_handle, NM_PORTALS4_LARGE_SENDQUEUE_LEN, &p_status->send.eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");
}

static void nm_portals4_large_disconnect(void*_status)
{
  struct nm_portals4_large_s*p_status = _status;
  PtlEQFree(p_status->send.eq_handle);
}

static void nm_portals4_large_send_register(struct nm_portals4_large_s*p_status, const struct iovec*v, int n,
                                            struct nm_portals4_large_md_entry_s*p_entry)
{
  /* copy v since PtlMDBind write into it, and another copy
   * of the same iovec won't be recognized as identical. */
  const ptl_md_t md =
    {
      .start     = (void*)v,
      .length    = n,
      .options   = PTL_IOVEC,
      .eq_handle = p_status->send.eq_handle,
      .ct_handle = PTL_CT_NONE
    };
  int rc = PtlMDBind(p_status->p_portals4_context->ni_handle, &md, &p_entry->md_handle);
  nm_portals4_check_error(rc, "failed to bind memory descriptor");
}

static void nm_portals4_large_send_pkt_prefetch(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_s*p_status = _status;
  struct nm_portals4_large_md_entry_s*p_entry = padico_malloc(sizeof(struct nm_portals4_large_md_entry_s));
  assert(p_pkt->iov.v != NULL);
  assert(p_pkt->iov.n > 0);
  p_entry->p_pkt = p_pkt;
  nm_portals4_large_send_register(p_status, p_pkt->iov.v, p_pkt->iov.n, p_entry);
  p_pkt->send_prefetch.p_entry = p_entry;
}

static void nm_portals4_large_send_pkt_unfetch(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_md_entry_s*p_entry = p_pkt->send_prefetch.p_entry;
  if(p_entry == NULL)
    {
      NM_FATAL("cannot find MD entry to unfetch.\n");
    }
  int rc = PtlMDRelease(p_entry->md_handle);
  nm_portals4_check_error(rc, "failed to release memory descriptor");
  p_pkt->send_prefetch.p_entry = NULL;
  padico_free(p_entry);
}

static inline void nm_portals4_large_send(struct nm_portals4_large_s*p_status)
{
  int rc = PtlPut(p_status->send.p_entry->md_handle,
                  0 /* local_offset */, p_status->send.length,
                  PTL_ACK_REQ /* ack_req */,
                  p_status->peer.process_id,
                  p_status->peer.pt_index,
                  1 /* match_bits */,
                  0 /* remote_offset */,
                  NULL /* user_ptr */,
                  0 /* hdr_data */);
  nm_portals4_check_error(rc, "failed to send data");
}

static int nm_portals4_large_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_s*p_status = _status;
  struct nm_portals4_large_md_entry_s*p_entry = p_pkt->send_prefetch.p_entry;
  if(p_entry == NULL)
    {
      nm_portals4_large_send_register(p_status, p_pkt->iov.v, p_pkt->iov.n, &p_status->send.md_entry);
      p_status->send.p_entry = &p_status->send.md_entry;
      nm_profile_inc(p_status->p_portals4_context->profiling.n_prefetch_missed);
    }
  else
    {
      p_pkt->send_prefetch.p_entry = NULL;
      p_status->send.p_entry = p_entry;
      nm_profile_inc(p_status->p_portals4_context->profiling.n_prefetch_success);
    }
  int i;
  nm_len_t l = 0;
  for(i = 0; i < p_pkt->iov.n; i++)
    l += p_pkt->iov.v[i].iov_len;
  p_status->send.length = l;
  nm_portals4_large_send(p_status);
  return NM_ESUCCESS;
}

static int nm_portals4_large_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_s*p_status = _status;

  ptl_event_t event;
  int rc = PtlEQGet(p_status->send.eq_handle, &event);
  if(rc == PTL_OK)
    {
      if(event.ni_fail_type == PTL_NI_PT_DISABLED)
        {
          NM_WARN("send poll- NI failed with PTL_NI_PT_DISABLED; it shouldn't happen; re-trying send.\n");
          puk_usleep(1000);
          nm_profile_inc(p_status->p_portals4_context->profiling.n_pt_disabled);
          /* flow control has disabled PT on remote side; try again */
          nm_portals4_large_send(p_status);
        }
      else if(event.ni_fail_type == PTL_NI_OK)
        {
          if(event.type == PTL_EVENT_ACK) /* PTL_EVENT_SEND */
            {
              int rc = PtlMDRelease(p_status->send.p_entry->md_handle);
              nm_portals4_check_error(rc, "failed to release memory descriptor");
              if(p_status->send.p_entry != &p_status->send.md_entry)
                {
                  padico_free(p_status->send.p_entry);
                }
              p_status->send.p_entry = NULL;
              p_status->send.length = NM_LEN_UNDEFINED;
              return NM_ESUCCESS;
            }
        }
      else
        {
          NM_FATAL("event NI fail type = %d (%s)\n",
                  event.ni_fail_type, nm_portals4_nifail_strerror(event.ni_fail_type));
        }
      return -NM_EAGAIN;
    }
  else if(rc == PTL_EQ_EMPTY)
    {
      return -NM_EAGAIN;
    }
  else if(rc == PTL_EQ_DROPPED)
    {
      /* TODO- manage dropped events */
      NM_FATAL("detected dropped event\n");
    }
  else
    {
      nm_portals4_check_error(rc, "error while polling for events");
      return -NM_EUNKNOWN;
    }
}

static void nm_portals4_large_recv_poll_all(struct nm_portals4_large_context_s*p_portals4_context)
{
  ptl_event_t event;
  int rc;
  do
    {
      rc = PtlEQGet(p_portals4_context->global_eq_handle, &event);
      if(rc == PTL_OK)
        {
          if(event.ni_fail_type == PTL_NI_OK)
            {
              if(event.type == PTL_EVENT_PUT)
                {
                  struct nm_portals4_large_s*p_status = event.user_ptr;
                  p_status->recv.done = 1;
                }
              else if(event.type == PTL_EVENT_PT_DISABLED)
                {
                  NM_WARN("recv poll- got event PTL_EVENT_PT_DISABLED; it shouldn't happen; re-enabling PT\n");
                  PtlPTEnable(p_portals4_context->ni_handle, p_portals4_context->local.pt_index);
                }
              else if(event.type == PTL_EVENT_LINK)
                {
                  struct nm_portals4_large_s*p_status = event.user_ptr;
                  assert(p_status->recv.posted == 0);
                  p_status->recv.posted = 1;
                }
            }
          else if(event.ni_fail_type == PTL_NI_PT_DISABLED)
            {
              NM_WARN("recv poll- NI failed with PTL_NI_PT_DISABLED\n");
            }
          else if(event.ni_fail_type != PTL_NI_OK)
            {
              NM_FATAL("event NI fail type = %d (%s)\n",
                       event.ni_fail_type, nm_portals4_nifail_strerror(event.ni_fail_type));
            }
        }
      else if(rc == PTL_EQ_EMPTY)
        {
          /* no event; do nothing */
        }
      else if(rc == PTL_EQ_DROPPED)
        {
          /* TODO- manage dropped events */
          NM_FATAL("detected dropped event\n");
        }
      else
        {
          nm_portals4_check_error(rc, "error while polling for events");
        }
    }
  while(rc != PTL_EQ_EMPTY);
}

static int nm_portals4_large_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_s*p_status = _status;
  p_status->recv.done = 0;
  p_status->recv.posted = 0;
  ptl_me_t me =
    {
      .start       = (void*)p_pkt->iov.v,
      .length      = p_pkt->iov.n,
      .uid         = PTL_UID_ANY,
      .match_id    = p_status->peer.process_id,
      .match_bits  = 1,
      .ignore_bits = 0,
      .min_free    = 0,
      .options     = PTL_ME_OP_PUT | PTL_ME_USE_ONCE | PTL_IOVEC,
      .ct_handle   = PTL_CT_NONE
    };
  int rc = PtlMEAppend(p_status->p_portals4_context->ni_handle, p_status->p_portals4_context->local.pt_index,
                       &me, PTL_PRIORITY_LIST, p_status /* user_ptr */, &p_status->recv.me_handle);
  nm_portals4_check_error(rc, "error while posting matching request");

  nm_portals4_large_recv_poll_all(p_status->p_portals4_context);
  if(p_status->recv.posted)
    {
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EINPROGRESS;
    }
}

static int nm_portals4_large_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_portals4_large_s*p_status = _status;
  nm_portals4_large_recv_poll_all(p_status->p_portals4_context);
  if(p_status->recv.done)
    {
      assert(p_status->recv.posted);
      return NM_ESUCCESS;
    }
  else if(p_status->recv.posted)
    {
      return -NM_EAGAIN;
    }
  else
    {
      return -NM_EINPROGRESS;
    }
}
