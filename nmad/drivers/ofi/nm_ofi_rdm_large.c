/*
 * NewMadeleine
 * Copyright (C) 2006-2025 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <Padico/Module.h>
#include <nm_minidriver.h>
#include <nm_private.h>

#include "nm_ofi.h"

static void*nm_ofi_rdmlarge_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ofi_rdmlarge_destroy(void*status);

static const struct puk_component_driver_s nm_ofi_rdmlarge_component =
  {
    .instantiate = &nm_ofi_rdmlarge_instantiate,
    .destroy     = &nm_ofi_rdmlarge_destroy
  };

static void nm_ofi_rdmlarge_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_ofi_rdmlarge_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_ofi_rdmlarge_close(puk_context_t context);
static void nm_ofi_rdmlarge_connect_async(void* _status, const void*remote_url, size_t url_size);
static int  nm_ofi_rdmlarge_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ofi_rdmlarge_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ofi_rdmlarge_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt);
static int  nm_ofi_rdmlarge_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt);

static const struct nm_minidriver_iface_s nm_ofi_rdmlarge_minidriver =
  {
    .getprops         = &nm_ofi_rdmlarge_getprops,
    .init             = &nm_ofi_rdmlarge_init,
    .close            = &nm_ofi_rdmlarge_close,
    .connect          = NULL,
    .connect_async    = &nm_ofi_rdmlarge_connect_async,
    .connect_wait     = NULL,
    .disconnect       = NULL,
    .send_pkt_post    = &nm_ofi_rdmlarge_send_pkt_post,
    .send_pkt_poll    = &nm_ofi_rdmlarge_send_pkt_poll,
    .recv_pkt_post    = &nm_ofi_rdmlarge_recv_pkt_post,
    .recv_pkt_poll    = &nm_ofi_rdmlarge_recv_pkt_poll
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_ofi_rdm_large,
    puk_component_declare("Minidriver_ofi_rdm_large",
                          puk_component_attr("provider", "auto"),
                          puk_component_attr("fabric", "auto"),
                          puk_component_attr("domain", "auto"),
                          puk_component_provides("PadicoComponent", "component", &nm_ofi_rdmlarge_component),
                          puk_component_provides("NewMad_minidriver", "minidriver", &nm_ofi_rdmlarge_minidriver)));

/* ********************************************************* */

/** 'ofi_rdmlarge' driver per-context data. */
struct nm_ofi_rdmlarge_context_s
{
  struct nm_ofi_common_context_s ofi;
  int next_key;         /**< next requested registration key */
  int need_mr;
  int supports_iovec;
  struct fid_ep*ep;   /**< per-context endpoint */
  struct fid_av*av;   /**< address vector for RDM type */
  struct fid_cq*send_cq;     /**< completion queue for send */
  struct fid_cq*recv_cq;     /**< completion queue for recv */
  void*url;
  size_t url_size;
};

/** 'ofi_rdmlarge' per-instance status. */
struct nm_ofi_rdmlarge_s
{
  struct nm_ofi_rdmlarge_context_s*p_ofi_context;
  fi_addr_t peer_addr;
  void*p_remote_url;         /**< remote url, for async connect */
  struct
  {
    int posted;
    int completed;
    struct nm_pkt_s*p_pkt; /**< data to send */
    struct fid_mr*mr;
    struct nm_ofi_context context;
  } send;
  struct
  {
    int posted;
    int completed;
    struct fid_mr*mr;
    struct nm_ofi_context context;
  } recv;
};

/* ********************************************************* */

static void* nm_ofi_rdmlarge_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ofi_rdmlarge_s*p_status = padico_malloc(sizeof(struct nm_ofi_rdmlarge_s));
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = puk_context_get_status(context);
  assert(p_ofi_context != NULL);
  p_status->p_ofi_context = p_ofi_context;
  p_status->p_remote_url = NULL;
  p_status->recv.posted = 0;
  p_status->recv.mr = NULL;
  p_status->send.posted = 0;
  p_status->send.p_pkt = NULL;
  p_status->send.mr = NULL;
  return p_status;
}

static void nm_ofi_rdmlarge_destroy(void*_status)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  NM_TRACEF("destroy()\n");
  if(p_status->p_remote_url != NULL)
    padico_free(p_status->p_remote_url);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_ofi_rdmlarge_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = padico_malloc(sizeof(struct nm_ofi_rdmlarge_context_s));
  puk_context_set_status(context, p_ofi_context);

  /* fill properties */
  p_props->profile.latency = 500;
  p_props->profile.bandwidth = 8000;
  p_props->capabilities.supports_data = 0;
  p_props->capabilities.prefers_wait_any = 0;
  p_props->capabilities.supports_recv_any = 0;
  p_props->nickname = "ofi_rdm_large";
  p_props->capabilities.supports_buf_send = 0;
  p_props->capabilities.supports_buf_recv = 0;
  int flags = FI_DIRECTED_RECV;
  const int gpu = nm_ofi_gpu_test(context, FI_EP_RDM, flags);
#ifdef NMAD_CUDA
  padico_out(puk_verbose_notice, "OFI: CUDA GPUDirect enabled.\n");
  p_props->capabilities.supports_cuda = gpu;
  flags |= FI_HMEM;
#endif /* NMAD_CUDA */
#ifdef NMAD_HIP
  padico_out(puk_verbose_notice, "OFI: HIP GPUDirect enabled.\n");
  p_props->capabilities.supports_hip  = gpu;
  flags |= FI_HMEM;
#endif /* NMAD_HIP */
  /* global ofi init */
  nm_ofi_common_init(&p_ofi_context->ofi, context, FI_EP_RDM, flags);
  const int max_iovec_tx = p_ofi_context->ofi.fi->tx_attr->iov_limit;
  const int max_iovec_rx = p_ofi_context->ofi.fi->rx_attr->iov_limit;
  const int max_iovec_mr = p_ofi_context->ofi.fi->domain_attr->mr_iov_limit;
  int max_iovec = max_iovec_tx;
  if(max_iovec_rx < max_iovec)
    max_iovec = max_iovec_rx;
  if(max_iovec_mr < max_iovec)
    max_iovec = max_iovec_mr;
  if(max_iovec > 1)
    {
      p_props->capabilities.supports_iovec = 1;
      p_props->capabilities.max_iovecs = max_iovec;
    }
  else
    {
      p_props->capabilities.supports_iovec = 0;
    }
  p_props->capabilities.max_msg_size = p_ofi_context->ofi.fi->ep_attr->max_msg_size;
  p_props->capabilities.max_pkt_sends = 1;
  p_props->capabilities.max_pkt_recvs = 1;
  p_ofi_context->supports_iovec = p_props->capabilities.supports_iovec;
  p_ofi_context->need_mr = p_ofi_context->ofi.need_mr;
}

static void nm_ofi_rdmlarge_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = puk_context_get_status(context);

  nm_ofi_common_open(&p_ofi_context->ofi);

  int rc = fi_endpoint(p_ofi_context->ofi.domain, p_ofi_context->ofi.fi, &p_ofi_context->ep, NULL);
  NM_OFI_CHECK_RC(rc, "fi_endpoint");
  const char*s_size = puk_context_getattr(context, "session_size");
  int session_size = 0;
  if(s_size != NULL)
    {
      session_size = atoi(s_size);
    }
  struct fi_av_attr av_attr =
    {
      .type        = FI_AV_MAP,
      .rx_ctx_bits = 0,
      .count       = session_size,
      .ep_per_node = 0, /* TODO- optimize */
      .name        = NULL, /* TODO- padico UUID */
      .map_addr    = NULL,
      .flags       = 0
    };
  rc = fi_av_open(p_ofi_context->ofi.domain, &av_attr, &p_ofi_context->av, NULL);
  NM_OFI_CHECK_RC(rc, "fi_av_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->av->fid, 0);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");

  /* open send/recv CQ */
  struct fi_cq_attr cq_attr;
  memset(&cq_attr, 0, sizeof(cq_attr));
  cq_attr.size = p_ofi_context->ofi.fi->tx_attr->size;
  cq_attr.flags = 0;
  cq_attr.format = FI_CQ_FORMAT_MSG;
  cq_attr.wait_obj = FI_WAIT_NONE;
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_ofi_context->send_cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->send_cq->fid, FI_TRANSMIT);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_ofi_context->recv_cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->recv_cq->fid, FI_RECV);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  rc = fi_enable(p_ofi_context->ep);
  NM_OFI_CHECK_RC(rc, "fi_enable");

  /* get address */
  p_ofi_context->url_size = 0;
  rc = fi_getname(&p_ofi_context->ep->fid, NULL, &p_ofi_context->url_size);
  if(rc != -FI_ETOOSMALL)
    {
      NM_OFI_CHECK_RC(rc, "fi_getname");
    }
  assert(p_ofi_context->url_size != 0);
  p_ofi_context->url = padico_malloc(p_ofi_context->url_size);
  rc = fi_getname(&p_ofi_context->ep->fid, p_ofi_context->url, &p_ofi_context->url_size);
  NM_OFI_CHECK_RC(rc, "fi_getname");

  *drv_url = p_ofi_context->url;
  *url_size = p_ofi_context->url_size;
  NM_TRACEF("# nm_ofi- init done; url_size = %zd.\n", p_ofi_context->url_size);
}

static void nm_ofi_rdmlarge_close(puk_context_t context)
{
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = puk_context_get_status(context);
  NM_TRACEF("# nm_ofi- closing...\n");
  puk_context_set_status(context, NULL);
  padico_free(p_ofi_context->url);
  fi_close(&p_ofi_context->send_cq->fid);
  fi_close(&p_ofi_context->recv_cq->fid);
  nm_ofi_common_close(&p_ofi_context->ofi);
  padico_free(p_ofi_context);
}

static void nm_ofi_rdmlarge_connect_async(void*_status, const void*p_remote_url, size_t url_size)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = p_status->p_ofi_context;
  NM_TRACEF("# nm_ofi- connecting...\n");
  p_status->p_remote_url = padico_malloc(url_size);
  memcpy(p_status->p_remote_url, p_remote_url, url_size); /* copy url since fi_av_insert is asynchronous */

  /* TODO- vectored connection */
  const int count = 1;
  int rc = fi_av_insert(p_ofi_context->av, p_status->p_remote_url, count, &p_status->peer_addr, 0, NULL);
  if(rc != count)
    {
      NM_FATAL("error in fi_av_insert().\n");
    }
}

static int nm_ofi_rdmlarge_send_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = p_status->p_ofi_context;
  p_status->send.posted = 0;
  assert(p_status->send.p_pkt == NULL);
  p_status->send.p_pkt = p_pkt;
  if(p_ofi_context->need_mr)
    {
      if(p_pkt->iov.n > 1)
        {
          assert(p_ofi_context->supports_iovec);
          int rc = fi_mr_regv(p_ofi_context->ofi.domain, p_pkt->iov.v, p_pkt->iov.n, FI_MSG, 0,
                              p_ofi_context->next_key++, 0, &p_status->send.mr, NULL);
          NM_OFI_CHECK_RC(rc, "fi_mr_regv");
        }
      else
        {
          int rc = fi_mr_reg(p_ofi_context->ofi.domain, p_pkt->iov.v[0].iov_base, p_pkt->iov.v[0].iov_len, FI_MSG, 0,
                             p_ofi_context->next_key++, 0, &p_status->send.mr, NULL);
          NM_OFI_CHECK_RC(rc, "fi_mr_reg");
        }
    }
  return NM_ESUCCESS;
}

static void nm_ofi_rdmlarge_send_progress(struct nm_ofi_rdmlarge_context_s*p_ofi_context)
{
  struct fi_cq_msg_entry entry;
  int rc = fi_cq_read(p_ofi_context->send_cq, &entry, 1);
  if(rc == 1)
    {
      NM_TRACEF("# nm_ofi- send SUCCESS; len = %d.\n", (int)entry.len);
      assert(entry.flags & FI_MSG);
      assert(entry.flags & FI_SEND);
      struct nm_ofi_rdmlarge_s*p_status = nm_container_of(entry.op_context, struct nm_ofi_rdmlarge_s, send.context);
      p_status->send.completed = 1;
      if(p_ofi_context->need_mr)
        {
          fi_close(&p_status->send.mr->fid);
        }
    }
  else if(rc == -FI_EAVAIL)
    {
      struct fi_cq_err_entry err;
      fi_cq_readerr(p_ofi_context->send_cq, &err, 0 /* flags */);
      NM_FATAL("fi_cq_read()- asynchronous operation failed; err = %d (%s); prov_errno = %d (%s).",
               err.err, fi_strerror(err.err),
               err.prov_errno, fi_cq_strerror(p_ofi_context->send_cq, err.prov_errno, err.err_data, err.buf, err.len));
    }
  else if(rc != -FI_EAGAIN)
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_rdmlarge_send_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = p_status->p_ofi_context;
  if(!p_status->send.posted)
    {
      int rc = FI_SUCCESS;
      void*mr_desc = p_ofi_context->need_mr ? fi_mr_desc(p_status->send.mr) : NULL;
      if(p_status->send.p_pkt->iov.n > 1)
        {
          assert(p_ofi_context->supports_iovec);
          rc = fi_sendv(p_ofi_context->ep, p_status->send.p_pkt->iov.v, mr_desc, p_status->send.p_pkt->iov.n,
                        p_status->peer_addr, &p_status->send.context);
        }
      else
        {
          rc = fi_send(p_ofi_context->ep, p_status->send.p_pkt->iov.v[0].iov_base, p_status->send.p_pkt->iov.v[0].iov_len,
                       mr_desc, p_status->peer_addr, &p_status->send.context);
        }
      if(rc == FI_SUCCESS)
        {
          p_status->send.posted = 1;
          p_status->send.completed = 0;
        }
      else  if(rc == -FI_EAGAIN)
        {
          return -NM_EAGAIN;
        }
      else
        {
          NM_OFI_CHECK_RC(rc, "fi_send");
        }
    }
  nm_ofi_rdmlarge_send_progress(p_ofi_context);
  if(p_status->send.completed)
    {
      if(p_status->send.mr)
        fi_close(&p_status->send.mr->fid);
      p_status->send.mr = NULL;
      p_status->send.p_pkt = NULL;
      p_status->send.posted = 0;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static int nm_ofi_rdmlarge_recv_pkt_post(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = p_status->p_ofi_context;
  void*mr_desc = NULL;
  assert(p_status->recv.posted == 0);
  if(p_pkt->iov.n > 1)
    {
      assert(p_ofi_context->supports_iovec);
      if(p_ofi_context->need_mr)
        {
          int rc = fi_mr_regv(p_ofi_context->ofi.domain, p_pkt->iov.v, p_pkt->iov.n, FI_MSG, 0,
                              p_ofi_context->next_key++, 0, &p_status->recv.mr, NULL);
          NM_OFI_CHECK_RC(rc, "fi_mr_regv");
          mr_desc = fi_mr_desc(p_status->recv.mr);
        }
      int rc = fi_recvv(p_ofi_context->ep, p_pkt->iov.v, mr_desc, p_pkt->iov.n, p_status->peer_addr, &p_status->recv.context);
      NM_OFI_CHECK_RC(rc, "fi_recvv");
    }
  else
    {
      assert(p_pkt->iov.n == 1);
      if(p_ofi_context->need_mr)
        {
          int rc = fi_mr_reg(p_ofi_context->ofi.domain, p_pkt->iov.v[0].iov_base, p_pkt->iov.v[0].iov_len, FI_MSG, 0,
                             p_ofi_context->next_key++, 0, &p_status->recv.mr, NULL);
          NM_OFI_CHECK_RC(rc, "fi_mr_reg");
          mr_desc = fi_mr_desc(p_status->recv.mr);
        }
      int rc = fi_recv(p_ofi_context->ep, p_pkt->iov.v[0].iov_base, p_pkt->iov.v[0].iov_len, mr_desc, p_status->peer_addr, &p_status->recv.context);
      NM_OFI_CHECK_RC(rc, "fi_recvv");
    }
  p_status->recv.posted = 1;
  p_status->recv.completed = 0;
  return NM_ESUCCESS;
}

static void nm_ofi_rdmlarge_recv_progress(struct nm_ofi_rdmlarge_context_s*p_ofi_context)
{
  struct fi_cq_msg_entry entry;
  int rc = fi_cq_read(p_ofi_context->recv_cq, &entry, 1);
  if(rc == 1)
    {
      NM_TRACEF("# recv SUCCESS - len = %d.\n", (int)entry.len);
      struct nm_ofi_rdmlarge_s*p_status = nm_container_of(entry.op_context, struct nm_ofi_rdmlarge_s, recv.context);
      p_status->recv.completed = 1;
      if(p_ofi_context->need_mr)
        {
          fi_close(&p_status->recv.mr->fid);
        }
    }
  else if(rc == -FI_EAVAIL)
    {
      struct fi_cq_err_entry err;
      fi_cq_readerr(p_ofi_context->recv_cq, &err, 0);
      NM_FATAL("fi_cq_read()- asynchronous operation failed- err = %d (%s); prov_errno = %d (%s); buf = %p; len = %lu; overflow = %lu.",
               err.err, fi_strerror(err.err),
               err.prov_errno, fi_cq_strerror(p_ofi_context->send_cq, err.prov_errno, err.err_data, err.buf, err.len), err.buf, err.len, err.olen);
    }
  else if(rc != -FI_EAGAIN)
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_rdmlarge_recv_pkt_poll(void*_status, struct nm_pkt_s*p_pkt)
{
  struct nm_ofi_rdmlarge_s*p_status = _status;
  struct nm_ofi_rdmlarge_context_s*p_ofi_context = p_status->p_ofi_context;
  if(!p_status->recv.completed)
    {
      nm_ofi_rdmlarge_recv_progress(p_ofi_context);
    }
  if(p_status->recv.completed)
    {
      if(p_status->recv.mr)
        fi_close(&p_status->recv.mr->fid);
      p_status->recv.mr = NULL;
      p_status->recv.posted = 0;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}
