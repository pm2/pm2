/*
 * NewMadeleine
 * Copyright (C) 2012-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>

#include <nm_private.h>

#include <Padico/Module.h>
#include <Padico/Topology.h>
#include "shm.h"

/* ********************************************************* */

#define STRAT_SHM_BLOCKS_MAX 128
/** lists of blocks, for pw destructor */
struct strat_shm_blocks_s
{
  int n;
  int blocks[STRAT_SHM_BLOCKS_MAX];
};
PUK_ALLOCATOR_TYPE(strat_shm_blocks, struct strat_shm_blocks_s);

static struct
{
  int aggreg;
  int total;
}  nm_strat_shm_stats = { .total = 0, .aggreg = 0 };

/* Components structures:
 */

static void strat_shm_pack_data(void*_status, struct nm_req_s*p_pack, nm_len_t len, nm_len_t chunk_offset);
static int  strat_shm_pack_ctrl(void*, nm_gate_t , const union nm_header_ctrl_generic_s*);
static int  strat_shm_try_and_commit(void*, nm_gate_t );
static void strat_shm_rdv_accept(void*, nm_gate_t );
static void strat_shm_proto(void*_status, nm_gate_t p_gate, struct nm_pkt_wrap_s*p_pw, const void*ptr, nm_len_t len);

static const struct nm_strategy_iface_s nm_strat_shm_driver =
  {
    .pack_data       = &strat_shm_pack_data,
    .pack_ctrl       = &strat_shm_pack_ctrl,
    .try_and_commit  = &strat_shm_try_and_commit,
    .rdv_accept      = &strat_shm_rdv_accept,
    .proto           = &strat_shm_proto
};

static void*strat_shm_instantiate(puk_instance_t, puk_context_t);
static void strat_shm_destroy(void*);

static const struct puk_component_driver_s nm_strat_shm_component_driver =
  {
    .instantiate = &strat_shm_instantiate,
    .destroy     = &strat_shm_destroy
  };

/* ********************************************************* */

/** Per-gate status for strat_shm instances
 */
struct nm_strat_shm
{
  int max_small;
};

PADICO_MODULE_COMPONENT(NewMad_Strategy_shm,
  puk_component_declare("NewMad_Strategy_shm",
                        puk_component_provides("PadicoComponent", "component", &nm_strat_shm_component_driver),
                        puk_component_provides("NewMad_Strategy", "strat", &nm_strat_shm_driver),
                        puk_component_attr("max_small", NULL)));

/** node descriptor for strat_shm
 */
struct nm_strat_shm_node_s
{
  padico_topo_node_t node;             /**< the node described in this entry */
  nm_gate_t p_gate;             /**< nm gate connected to the node */
  struct padico_shm_node_s*shm_node; /**< node in the shm segment- NULL if node is remote */
  int rank;                          /**< global nm rank */
  struct nm_strat_shm_node_s*leader; /**< leader node on the host (first node in the host, in nm launcher rank) */
};

/** block of global state */
static struct
{
  struct padico_shm_s*shm;  /**< shm segment for strat_shm */
  puk_hashtable_t gate_map; /**< nodes hashed by gate */
  puk_hashtable_t node_map; /**< nodes hash by uuid */
  struct nm_strat_shm_node_s*self_node;
  struct strat_shm_blocks_allocator_s*blocks_allocator;
} nm_strat_shm_global =
  {
    .shm = NULL,
    .gate_map = NULL,
    .node_map = NULL,
    .self_node = NULL,
    .blocks_allocator = NULL
  };

/** header used to forward messages */
struct strat_shm_header_s
{
  struct nm_header_strat_s strat_header;
  char from[PADICO_TOPO_UUID_SIZE];
  char dest[PADICO_TOPO_UUID_SIZE];
};

static int topo_uuid_eq(const void*uuid1, const void*uuid2)
{
  return (memcmp(uuid1, uuid2, PADICO_TOPO_UUID_SIZE) == 0);
}
static uint32_t topo_uuid_hash(const void*uuid)
{
  return puk_hash_default(uuid, PADICO_TOPO_UUID_SIZE);
}

/* ********************************************************* */

#warning TODO- clean header
extern nm_gate_t newmadico_launcher_get_gate(int rank);
extern padico_topo_node_t newmadico_launcher_get_node(int rank);

static void nm_strat_shm_init(void)
{
  if(nm_strat_shm_global.shm == NULL)
    {
      fprintf(stderr, "# nmad strat_shm: init- building node map.\n");
      nm_strat_shm_global.shm = padico_shm_init("nm_strat_shm");
      nm_strat_shm_global.node_map = puk_hashtable_new(&topo_uuid_hash, &topo_uuid_eq);
      nm_strat_shm_global.gate_map = puk_hashtable_new_ptr();
      int i;
      for(i = 0 ; ; i++)
        {
          /* build gate <-> node mapping */
          nm_gate_t p_gate = newmadico_launcher_get_gate(i);
          padico_topo_node_t node = newmadico_launcher_get_node(i);
          if(p_gate == NULL || node == NULL)
            break;
          struct nm_strat_shm_node_s*leader = NULL;
          puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(nm_strat_shm_global.node_map);
          struct nm_strat_shm_node_s*n = puk_hashtable_enumerator_next_data(e);
          while(n != NULL)
            {
              if(padico_topo_node_gethost(node) == padico_topo_node_gethost(n->node))
                {
                  leader = n->leader;
                  break;
                }
              n = puk_hashtable_enumerator_next_data(e);
            }
          puk_hashtable_enumerator_delete(e);
          struct nm_strat_shm_node_s*shm_node = padico_malloc(sizeof(struct nm_strat_shm_node_s));
          if(leader == NULL)
            {
              leader = shm_node;
            }
          if(padico_topo_node_gethost(node) == padico_topo_node_gethost(padico_topo_getlocalnode()))
            {
              shm_node->shm_node = NULL;
              while(shm_node->shm_node == NULL)
                {
                  shm_node->shm_node = padico_shm_directory_node_lookup(nm_strat_shm_global.shm, node);
                  if(shm_node->shm_node == NULL)
                    {
                      fprintf(stderr, "# shm: waiting node #%d...\n", i);
                      puk_sleep(1);
                    }
                }
            }
          else
            {
              shm_node->shm_node = NULL;
            }
          shm_node->node   = node;
          shm_node->p_gate = p_gate;
          shm_node->rank   = i;
          shm_node->leader = leader;
          puk_hashtable_insert(nm_strat_shm_global.gate_map, shm_node->p_gate, shm_node);
          puk_hashtable_insert(nm_strat_shm_global.node_map, padico_topo_node_getuuid(shm_node->node), shm_node);
          fprintf(stderr, "# strat_shm: #%d- %s -> gate = %p; route = %s; shm_node=%p\n",
                  i, (const char*)padico_topo_node_getuuid(node), p_gate, (const char*)padico_topo_node_getuuid(leader->node), shm_node->shm_node);
          if(node == padico_topo_getlocalnode())
            {
              nm_strat_shm_global.self_node = shm_node;
            }
          /* build routing table */
          if(node != padico_topo_getlocalnode())
            {
              padico_topo_network_vect_t networks = NULL;
              networks = padico_topo_network_vect_new();
              padico_topo_node_getnetworks(node, networks);

            }
        }
      assert(nm_strat_shm_global.self_node != NULL);
      fprintf(stderr, "# strat_shm: self = %s; is_leader = %d\n",
              (const char*)padico_topo_node_getuuid(nm_strat_shm_global.self_node->node),
              nm_strat_shm_global.self_node == nm_strat_shm_global.self_node->leader);
      nm_strat_shm_global.blocks_allocator = strat_shm_blocks_allocator_new(16);
    }
}

/* ********************************************************* */

/** Initialize the gate storage for strat_shm.
 */
static void*strat_shm_instantiate(puk_instance_t ai, puk_context_t context)
{
  struct nm_strat_shm *status = TBX_MALLOC(sizeof(struct nm_strat_shm));
  const char*max_small = puk_instance_getattr(ai, "max_small");
  status->max_small = max_small ? atoi(max_small) : (NM_SO_MAX_UNEXPECTED - NM_HEADER_DATA_SIZE);
  return status;
}

/** Cleanup the gate storage for strat_shm.
 */
static void strat_shm_destroy(void*status)
{
  TBX_FREE(status);
}

/* ********************************************************* */

static void nm_strat_shm_pw_complete(struct nm_pkt_wrap_s*p_pw)
{
  struct strat_shm_blocks_s*blocks = p_pw->destructor_key;
  int i;
  for(i = 0; i < blocks->n; i++)
    {
      padico_shm_block_free(nm_strat_shm_global.shm, blocks->blocks[i]);
    }
  strat_shm_blocks_free(nm_strat_shm_global.blocks_allocator, blocks);
}


/** Add a new control "header" to the flow of outgoing packets.
 *
 *  @param _status the strat_shm instance status.
 *  @param p_ctrl a pointer to the ctrl header.
 *  @return The NM status.
 */
static int strat_shm_pack_ctrl(void*_status, nm_gate_t p_gate, const union nm_header_ctrl_generic_s*p_ctrl)
{
  struct nm_trk_s*p_trk_small = &p_gate->trks[NM_TRK_SMALL];
  padico_trace("direct ctrl send.\n");
  struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_global_header(p_gate->p_core, p_trk_small);
  nm_so_pw_add_control(p_pw, p_ctrl);
  nm_core_post_send(p_pw, p_gate, NM_TRK_SMALL);
  padico_trace("direct ctrl send- done.\n");
  return NM_ESUCCESS;
}

static void nm_strat_shm_poll_direct(struct nm_core*p_core)
{



  /* deliver direct packets (driver-bypass) */
  const int block_num = padico_shm_short_recv_poll(nm_strat_shm_global.shm, NULL);
  if(block_num > -1)
    {
      padico_trace("direct recv- block = %d\n", block_num);
      struct padico_shm_short_header_s*shm_header = padico_shm_block_get_ptr(nm_strat_shm_global.shm, block_num);
      const int from = shm_header->from;
      const int size = shm_header->size;
      const void*uuid = nm_strat_shm_global.shm->seg->directory.nodes[from].node_uuid;
      struct nm_strat_shm_node_s*shm_node = puk_hashtable_lookup(nm_strat_shm_global.node_map, uuid);
      assert(shm_node != NULL);
      nm_gate_t p_from_gate = shm_node->p_gate;
      assert(p_from_gate != NULL);
      void*base = padico_shm_short_get_ptr(nm_strat_shm_global.shm, block_num);
      NM_TRACEF("direct decode- size = %d.\n", size);
      struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_noheader(p_core);
      nm_so_pw_add_raw(p_pw, base, size, 0 /* offset */);
      struct strat_shm_blocks_s*blocks = strat_shm_blocks_malloc(nm_strat_shm_global.blocks_allocator);
      blocks->n = 1;
      blocks->blocks[0] = block_num;
      p_pw->destructor = &nm_strat_shm_pw_complete;
      p_pw->destructor_key = blocks;
      p_pw->p_gate = p_from_gate;
      nm_decode_header_chunk(p_from_gate->p_core, base, p_pw, p_from_gate);
      nm_pw_ref_dec(p_pw);
      padico_trace("direct recv- block = %d done size = %d.\n", block_num, size);
    }
}


static void strat_shm_indirect_send(struct nm_pkt_wrap_s*p_pw, struct nm_strat_shm_node_s*dest)
{
  const char*dest_uuid = (const char*)padico_topo_node_getuuid(dest->node);
  assert(dest_uuid != NULL);
  const int block = padico_shm_block_alloc(nm_strat_shm_global.shm);
  void*ptr = padico_shm_block_get_ptr(nm_strat_shm_global.shm, block);
  /* pack strat header */
  struct strat_shm_header_s*shm_header = ptr;
  shm_header->strat_header.proto_id = NM_PROTO_STRAT;
  shm_header->strat_header.size = sizeof(struct strat_shm_header_s) + p_pw->v[0].iov_len;
  memcpy(shm_header->dest, dest_uuid, PADICO_TOPO_UUID_SIZE);
  memcpy(shm_header->from, padico_topo_node_getuuid(padico_topo_getlocalnode()), PADICO_TOPO_UUID_SIZE);
  /* copy data */
  void*buf_ptr = ptr + sizeof(struct strat_shm_header_s);
  memcpy(buf_ptr, p_pw->v[0].iov_base, p_pw->v[0].iov_len);
  int rc = -1;
  while(rc != 0)
    {
      rc = padico_shm_block_lfqueue_enqueue(&nm_strat_shm_global.shm->seg->buffer.pending_blocks[dest->leader->rank], block);
    }
  /* immediate completion */
  nm_so_process_complete_send(p_pw->p_gate->p_core, p_pw);
}

static void strat_shm_indirect_recv()
{
}

static void strat_shm_indirect_progress(struct nm_strat_shm_node_s*shm_node, nm_drv_t p_drv)
{
  struct padico_shm_block_lfqueue_s*queue = &nm_strat_shm_global.shm->seg->buffer.pending_blocks[shm_node->rank];
  struct nm_pkt_wrap_s *p_pw = NULL;
  struct strat_shm_blocks_s*blocks = NULL;
  int block = padico_shm_block_lfqueue_dequeue_single_reader(queue);
  while(block != -1)
    {
      NM_TRACEF("processing block = %d\n", block);
      nm_strat_shm_stats.total++;
      struct strat_shm_header_s*header = padico_shm_block_get_ptr(nm_strat_shm_global.shm, block);
      if(p_pw == NULL)
        {
          p_pw = nm_pw_alloc_noheader(p_drv->p_core);
          blocks = strat_shm_blocks_malloc(nm_strat_shm_global.blocks_allocator);
          blocks->n = 0;
          p_pw->destructor = &nm_strat_shm_pw_complete;
          p_pw->destructor_key = blocks;
        }
      nm_so_pw_add_raw(p_pw, header, header->strat_header.size, 0 /* chunk_offset */);
      blocks->blocks[blocks->n] = block;
      blocks->n++;

      block = padico_shm_block_lfqueue_dequeue_single_reader(queue);
      if(block != -1)
        {
          struct strat_shm_header_s*next_header = padico_shm_block_get_ptr(nm_strat_shm_global.shm, block);
          if(next_header->strat_header.size >= nm_so_pw_remaining_buf(p_pw))
            {
              padico_shm_block_lfqueue_enqueue(queue, block);
              break;
            }
          else
            {
              nm_strat_shm_stats.aggreg++;
              /*
                if(nm_strat_shm_stats.aggreg % 1000 == 0)
                fprintf(stderr, "# nm_strat_shm: aggreg= %d / %d (%2.2g%%)\n",
                nm_strat_shm_stats.aggreg, nm_strat_shm_stats.total,
                (100.0*nm_strat_shm_stats.aggreg/nm_strat_shm_stats.total));
              */
            }
        }
    }
  if(p_pw != NULL)
    {
      nm_core_post_send(p_pw, shm_node->p_gate, NM_TRK_SMALL);
      p_pw = NULL;
    }

}


static int strat_shm_todo(void* _status, nm_gate_t p_gate)
{
  if(nm_strat_shm_global.shm == NULL)
    nm_strat_shm_init();
  nm_strat_shm_poll_direct(p_gate->p_core);
  if(nm_strat_shm_global.self_node == nm_strat_shm_global.self_node->leader)
    {
      struct nm_strat_shm_node_s*shm_node = puk_hashtable_lookup(nm_strat_shm_global.gate_map, p_gate);
      if(shm_node->leader == shm_node)
        return padico_shm_block_lfqueue_empty(&nm_strat_shm_global.shm->seg->buffer.pending_blocks[shm_node->rank]);
    }
  return 0;
}

/** push message chunk */
static void strat_shm_pack_data(void*_status, struct nm_req_s*p_pack, nm_len_t chunk_len, nm_len_t chunk_offset)
{
  struct nm_strat_shm*status = _status;
  if(nm_strat_shm_global.shm == NULL)
    nm_strat_shm_init();
  const nm_gate_t p_gate = p_pack->p_gate;
  struct nm_core*p_core = p_gate->p_core;
  struct nm_trk_s*p_trk_small = &p_gate->trks[NM_TRK_SMALL];
  struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_alloc(p_core);
  nm_req_chunk_init(p_req_chunk, p_pack, chunk_offset, chunk_len);
  if(chunk_len <= status->max_small)
    {
      struct nm_strat_shm_node_s*strat_shm_node = puk_hashtable_lookup(nm_strat_shm_global.gate_map, p_gate);
      assert(strat_shm_node != NULL);
      assert(chunk_len + NM_HEADER_DATA_SIZE <= PADICO_SHM_BLOCKSIZE);
      /* ** small pack */
      struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_global_header(p_core, p_trk_small);
      nm_pw_add_req_chunk(p_pw, p_req_chunk, NM_REQ_CHUNK_FLAG_NONE);
      if(strat_shm_node->leader == nm_strat_shm_global.self_node->leader)
        {
          /* ** direct send through shm- no external driver involved ' */
          nm_core_post_send(p_pw, p_gate, NM_TRK_SMALL);
        }
      else
        {
          /* ** indirect send through shm */
          padico_trace("indirect send- offset = %d; len = %d.\n", chunk_offset, chunk_len);
          p_pw->p_gate = p_gate;
          strat_shm_indirect_send(p_pw, strat_shm_node);
        }
    }
  else
    {
      padico_trace("pack rdv- len = %d\n", chunk_len);
      const int is_lastchunk = (p_req_chunk->chunk_offset + p_req_chunk->chunk_len == p_pack->pack.len);
      struct nm_pkt_wrap_s*p_large_pw = nm_pw_alloc_noheader(p_core);
      nm_pw_add_req_chunk(p_large_pw, p_req_chunk, NM_REQ_CHUNK_FLAG_NONE);
      nm_pkt_wrap_list_push_back(&p_gate->pending_large_send, p_large_pw);
      union nm_header_ctrl_generic_s rdv;
      nm_header_init_rdv(&rdv, p_pack, p_req_chunk->chunk_len, p_req_chunk->chunk_offset,
                         is_lastchunk ? NM_PROTO_FLAG_LASTCHUNK : 0);
      struct nm_pkt_wrap_s*p_rdv_pw = nm_pw_alloc_global_header(p_core, p_trk_small);
      nm_pw_add_control(p_rdv_pw, &rdv);
      nm_core_post_send(p_rdv_pw, p_gate, NM_TRK_SMALL);
    }
}


/** Compute and apply the best possible packet rearrangement, then
 *  return next packet to send.
 *
 *  @param p_gate a pointer to the gate object.
 *  @return The NM status.
 */
static int strat_shm_try_and_commit(void*_status, nm_gate_t p_gate)
{
  if(nm_strat_shm_global.shm == NULL)
    nm_strat_shm_init();
  nm_strat_shm_poll_direct(p_gate->p_core);
  if(nm_strat_shm_global.self_node == nm_strat_shm_global.self_node->leader)
    {
      struct nm_strat_shm_node_s*shm_node = puk_hashtable_lookup(nm_strat_shm_global.gate_map, p_gate);
      assert(shm_node != NULL);
      if(shm_node->leader == shm_node)
        {
          /* src and dest are node leaders */
          nm_drv_t p_drv = nm_drv_default(p_gate);
          struct nm_trk_s*p_trk_small = &p_gate->trks[NM_TRK_SMALL];
          NM_TRACEF("node is leader. trk #0 state: %p\n", p_trk_small->p_pw_send);
          if(p_trk_small->p_pw_send == NULL)
            {
              strat_shm_indirect_progress(shm_node, p_drv);

            }
        }
    }
  return NM_ESUCCESS;
}

static void strat_shm_proto(void*_status, nm_gate_t p_gate, struct nm_pkt_wrap_s*p_pw, const void*ptr, nm_len_t len)
{
  const struct strat_shm_header_s*shm_header = ptr;
  if(memcmp(shm_header->dest, padico_topo_node_getuuid(padico_topo_getlocalnode()), PADICO_TOPO_UUID_SIZE) == 0)
    {
      /* deliver to local node */
      struct nm_strat_shm_node_s*shm_from_node = puk_hashtable_lookup(nm_strat_shm_global.node_map, shm_header->from);
      assert(shm_from_node != NULL);
      nm_gate_t p_from_gate = shm_from_node->p_gate;
      assert(p_from_gate != NULL);
      nm_decode_header_chunk(p_gate->p_core, ptr + sizeof(struct strat_shm_header_s), p_pw, p_from_gate);
    }
  else
    {
      NM_TRACEF("forwarding message.\n");
      struct nm_strat_shm_node_s*shm_dest_node = puk_hashtable_lookup(nm_strat_shm_global.node_map, shm_header->dest);
      const int block = padico_shm_block_alloc(nm_strat_shm_global.shm);
      void*block_ptr = padico_shm_short_get_ptr(nm_strat_shm_global.shm, block);
      memcpy(block_ptr, ptr, len);
      struct padico_shm_node_s*dest = shm_dest_node->shm_node;
      assert(dest != NULL);
      padico_shm_short_send_commit(nm_strat_shm_global.shm, dest, block, len, 1);
    }
}

/** Accept or refuse a RDV on the suggested (driver/track/gate).
 *
 *  @warning @p drv_id and @p trk_id are IN/OUT parameters. They initially
 *  hold values "suggested" by the caller.
 *  @param p_gate a pointer to the gate object.
 *  @param drv_id the suggested driver id.
 *  @param trk_id the suggested track id.
 *  @return The NM status.
 */
static void strat_shm_rdv_accept(void*_status, nm_gate_t p_gate)
{
  struct nm_strat_shm*status = _status;
  struct nm_pkt_wrap_s*p_pw = nm_pkt_wrap_list_begin(&p_gate->pending_large_recv);
  if(p_pw != NULL)
    {
      nm_drv_t p_drv = nm_drv_default(p_gate);
      if(p_pw->length > status->max_small)
        {
          struct nm_trk_s*p_trk_large = &p_gate->trks[NM_TRK_LARGE];
          if(p_trk_large->p_pw_recv == NULL)
            {
              /* The large-packet track is available- post recv and RTR */
              struct nm_rdv_chunk chunk =
                { .len = p_pw->length, .trk_id = NM_TRK_LARGE };
              nm_pkt_wrap_list_remove(&p_gate->pending_large_recv, p_pw);
              nm_tactic_rtr_pack(p_gate->p_core, p_pw, 1, &chunk);
            }
        }
      else
        {
          /* small chunk in a large packet- send on trk#0 */
          struct nm_rdv_chunk chunk =
            { .len = p_pw->length, .trk_id = NM_TRK_SMALL };
          nm_pkt_wrap_list_remove(&p_gate->pending_large_recv, p_pw);
          nm_tactic_rtr_pack(p_gate->p_core, p_pw, 1, &chunk);
        }
    }
}
