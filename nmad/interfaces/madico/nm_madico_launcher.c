/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * launcher for NewMad using PadicoTM topology and control channel.
 */

#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoControl.h>
#include <Padico/AddrDB.h>

#include <nm_public.h>
#include <nm_private.h>
#include <nm_launcher.h>

#include <nm_madico_launcher.h>


/** An instance of NewMad_Launcher_madico
 */
struct nm_madico_launcher_status_s
{
  padico_group_t group;   /**< group of nodes in the session */
  const char*local_url;   /**< nmad url for local node */
  struct nm_launcher_gates_s gates; /**< remote nodes, with the url and gate, in the same order as in 'group'. */
};

/** singleton for NewMad_Launcher_madico status */
static struct nm_madico_launcher_status_s*nm_madico_launcher_status = NULL;

/* ********************************************************* */

static void*nm_madico_launcher_instantiate(puk_instance_t, puk_context_t);
static void nm_madico_launcher_destroy(void*_status);

static const struct puk_component_driver_s nm_madico_launcher_component =
  {
    .instantiate = &nm_madico_launcher_instantiate,
    .destroy     = &nm_madico_launcher_destroy
  };

/* ********************************************************* */

static void nm_madico_launcher_init(void*_status, int*argc, char**argv, const char*group_name);
static void nm_madico_launcher_barrier(void*_status);
static void nm_madico_launcher_get_gates(void*_status, nm_gate_t*gates);
static void nm_madico_launcher_abort(void*_status, int rc);

static const struct nm_launcher_driver_s nm_madico_launcher_driver =
  {
    .init        = &nm_madico_launcher_init,
    .barrier     = &nm_madico_launcher_barrier,
    .get_gates   = &nm_madico_launcher_get_gates,
    .abort       = &nm_madico_launcher_abort
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(NewMad_Launcher_madico,
  puk_component_declare("NewMad_Launcher_madico",
                        puk_component_provides("PadicoComponent", "component",  &nm_madico_launcher_component),
                        puk_component_provides("NewMad_Launcher", "launcher", &nm_madico_launcher_driver ))
                        );

/* ********************************************************* */

static void*nm_madico_launcher_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_madico_launcher_status_s*status = padico_malloc(sizeof(struct nm_madico_launcher_status_s));
  status->group = NULL;
  assert(nm_madico_launcher_status == NULL);
  nm_madico_launcher_status = status;
  return status;
}

static void nm_madico_launcher_destroy(void*_status)
{
  struct nm_madico_launcher_status_s*status = _status;
  nm_launcher_gates_destroy(&status->gates);

  padico_group_destroy(status->group);
  padico_free(_status);

  struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
  event->kind = PADICO_CONTROL_EVENT_SHUTDOWN;
  padico_tasklet_schedule(padico_control_get_event_channel(), event, "event shutdown");

}

/* ********************************************************* */

static void nm_madico_launcher_init(void*_status, int*argc, char**argv, const char*group_name)
{
  struct nm_madico_launcher_status_s*status = _status;
  padico_control_event_sync();
  padico_group_t g = padico_group_lookup(group_name);
  if(status->group)
    {
      status->group = padico_group_dup(g, "Madico_Launcher");
    }
  else
    {
      const char*session_id = padico_topo_node_getsession(padico_topo_getlocalnode());
      padico_topo_network_t session_network = padico_topo_session_getnetwork(session_id);
      if(!session_network)
        {
          padico_fatal("cannot resolve PadicoTM session '%s'. NewMadeleine binaries are supposed to be launched with 'mpirun'.\n", session_id);
        }
      /* create group from session network */
      const size_t session_size = padico_topo_network_size(session_network);
      padico_group_t session_group = padico_group_init(group_name);
      int i;
      for(i = 0; i < session_size; i++)
        {
          padico_topo_node_t node = padico_topo_session_getnodebyrank(session_id, i);
          padico_group_node_add(session_group, node);
        }
      padico_group_store(session_group);
      const char*distrib = getenv("NM_LAUNCHER_DISTRIB");
      if(distrib == NULL)
        {
          status->group = session_group;
        }
      else
        {
          status->group = padico_group_topology(session_group, distrib);
        }
    }
  padico_group_barrier(status->group);
  padico_control_event_sync();
  const int size = padico_group_size(status->group);
  const int rank = padico_group_rank(status->group, padico_topo_getlocalnode());
  nm_launcher_gates_init(&status->gates, size, rank);
  const struct nm_launcher_info_s info = { .size = size, .rank = rank, .wide_url_support = 1 };
  nm_launcher_set_info(&info);

  padico_topo_node_t*p_peers = padico_malloc(sizeof(padico_topo_node_t) * size);
  int i;
  for(i = 0; i < size; i++)
    {
      p_peers[i] = padico_group_node(status->group, i);
    }
  nm_launcher_gates_populate_ns(&status->gates, p_peers);
  padico_free(p_peers);

  /* init drivers */
  int rc = nm_launcher_get_url(&status->local_url);
  if(rc != NM_ESUCCESS)
    {
      padico_fatal("nm_launcher_get_url()- rc = %d\n", rc);
    }

  padico_print("nm_session_init done- url = %s; size = %d; rank = %d\n",
               status->local_url, size, rank);

  /* ** exchange addresses */
  const char*addr_component = padico_module_self_name();
  const char*addr_instance = group_name;
  const int addr_instance_size = strlen(group_name) + 1;
  int steps = 1; /* number of steps = size rounded to power of two */
  while(steps < size)
    {
      steps *= 2;
    }
  int s;
  for(s = 0; s < steps; s++)
    {
      const int dest = rank ^ s; /* pave the (step, rank) matrix with XOR pattern to exchange pairwise */
      assert(dest >= 0);
      assert(dest <= steps);
      padico_topo_node_t peer = padico_group_node(status->group, dest);
      if(dest >= size)
        {
          /* dest may be larger than comm size if size is not a power of two */
          continue;
        }
      else if(peer != padico_topo_getlocalnode())
        {
          padico_out(50, "sending url to node #%d (%p)\n", dest, peer);
          padico_addrdb_publish(peer, addr_component,
                                addr_instance, addr_instance_size,
                                status->local_url, strlen(status->local_url)+1);
          size_t url_size = 0;
          padico_req_t req = padico_tm_req_new(NULL, NULL);
          padico_addrdb_dynget(peer, addr_component,
                               addr_instance, addr_instance_size,
                               (void**)&status->gates.p_gates[dest].p_url, &url_size, req);
          padico_out(50, "waiting for url from node #%d (%p)...\n", dest, peer);
          padico_tm_req_wait(req);
          padico_out(50, "received url from node #%d- connecting...\n", dest);
        }
      else
        {
          status->gates.p_gates[dest].p_url = padico_strdup(status->local_url);
        }
    }

  /* ** connect gates */
  nm_launcher_gates_connect(&status->gates);
  padico_print("connected.\n");
}

static void nm_madico_launcher_barrier(void*_status)
{
  struct nm_madico_launcher_status_s*status = _status;
  padico_group_barrier(status->group);
}

static void nm_madico_launcher_get_gates(void*_status, nm_gate_t*gates)
{
  struct nm_madico_launcher_status_s*status = _status;
  int i;
  for(i = 0; i < padico_group_size(status->group); i++)
    {
      gates[i] = status->gates.p_gates[i].p_gate;
      padico_out(50, "get_gates()- i = %d; gate = %p\n", i, gates[i]);
    }
}

static void nm_madico_launcher_abort(void*_status, int rc)
{
  struct nm_madico_launcher_status_s*status = _status;
  padico_group_kill(status->group, rc);
  exit(rc);
}

/* ********************************************************* */
/* ** madico launcher extension */

nm_gate_t newmadico_launcher_get_gate(int rank)
{
  struct nm_madico_launcher_status_s*status = nm_madico_launcher_status;
  const int size = padico_group_size(status->group);
  if(rank < size)
    return status->gates.p_gates[rank].p_gate;
  else
    return NULL;
}

padico_topo_node_t newmadico_launcher_get_node(int rank)
{
  struct nm_madico_launcher_status_s*status = nm_madico_launcher_status;
  const int size = padico_group_size(status->group);
  if(rank < size)
    return padico_group_node(status->group, rank);
  else
    return NULL;
}
