/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <Padico/Puk.h>
#include <nm_public.h>
#include <nm_private.h>
#include <nm_launcher.h>
#include <Padico/Module.h>


static int nm_single_launcher_declare(void);
static void nm_single_launcher_finalize(void);

PADICO_MODULE_BUILTIN(NewMad_Launcher_single, &nm_single_launcher_declare, NULL, &nm_single_launcher_finalize);

/* ** Cmd line launcher ************************************ */

static void*nm_single_launcher_instantiate(puk_instance_t i, puk_context_t c);
static void nm_single_launcher_destroy(void*_status);

static const struct puk_component_driver_s nm_single_launcher_component_driver =
  {
    .instantiate = &nm_single_launcher_instantiate,
    .destroy     = &nm_single_launcher_destroy
  };

static void nm_single_launcher_init(void*_status, int*argc, char**argv, const char*group_name);
static void nm_single_launcher_barrier(void*_status);
static void nm_single_launcher_get_gates(void*_status, nm_gate_t*gates);
static void nm_single_launcher_abort(void*_status, int rc);

static const struct nm_launcher_driver_s nm_single_launcher_driver =
  {
    .init         = &nm_single_launcher_init,
    .barrier      = &nm_single_launcher_barrier,
    .get_gates    = &nm_single_launcher_get_gates,
    .abort        = &nm_single_launcher_abort
  };

static struct
{
  puk_component_t component;
} nm_single_launcher = { .component = NULL };

static int nm_single_launcher_declare(void)
{
  nm_single_launcher.component =
    puk_component_declare("NewMad_Launcher_single",
                          puk_component_provides("PadicoComponent", "component", &nm_single_launcher_component_driver),
                          puk_component_provides("NewMad_Launcher", "launcher", &nm_single_launcher_driver ));
  return 0;
}

static void nm_single_launcher_finalize(void)
{
  puk_component_destroy(nm_single_launcher.component);
}


/* ********************************************************* */

struct nm_single_launcher_status_s
{
  nm_gate_t p_self_gate;
};

static void*nm_single_launcher_instantiate(puk_instance_t i, puk_context_t c)
{
  struct nm_single_launcher_status_s*p_status = padico_malloc(sizeof(struct nm_single_launcher_status_s));
  p_status->p_self_gate = NM_GATE_NONE;
  return p_status;
}

static void nm_single_launcher_destroy(void*_status)
{
  struct nm_single_launcher_status_s*status = _status;
  padico_free(status);
}

/* ********************************************************* */

static void nm_single_launcher_get_gates(void*_status, nm_gate_t*p_gates)
{
  struct nm_single_launcher_status_s*p_status = _status;
  p_gates[0] = p_status->p_self_gate;
}

void nm_single_launcher_init(void*_status, int*argc, char**argv, const char*p_label)
{
  struct nm_single_launcher_status_s*p_status = _status;
  padico_out(puk_verbose_notice, "using single node launcher, without mpirun.\n");
  struct nm_launcher_info_s info = { .size = 1, .rank = 0, .wide_url_support = 1 };
  nm_launcher_set_info(&info);
  const char*local_session_url = NULL;
  int err = nm_launcher_get_url(&local_session_url);
  if (err != NM_ESUCCESS)
    {
      NM_FATAL("nm_launcher_get_url returned err = %d\n", err);
    }
  err = nm_launcher_connect(&p_status->p_self_gate, local_session_url);
  if (err != NM_ESUCCESS)
    {
      NM_FATAL("self nm_launcher_connect returned err = %d\n", err);
    }
}

static void nm_single_launcher_barrier(void*_status)
{
  /* barrier is no-op for single process */
}

static void nm_single_launcher_abort(void*_status, int rc)
{
  exit(rc);
}
