/*
 * NewMadeleine
 * Copyright (C) 2016-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_RPC_PRIVATE_H
#define NM_RPC_PRIVATE_H

/** @file nm_rpc_private.h
 * Private header for the RPC interface; this is not part of the
 * public API and is not supposed to be used by end-users.
 */

#include <assert.h>
#include <nm_log.h>
#include <nm_core_interface.h>

/** an outgoing rpc request */
struct nm_rpc_req_s
{
  nm_sr_request_t request;            /**< the sendrecv request, actually allocated here */
  struct nm_rpc_service_s*p_service;
  struct nm_datav_s headerv;          /**< vector of user-supplied chunks of headers */
  struct nm_datav_s bodyv;            /**< vector of user-supplied chunks of body */
  nm_len_t extra_hlen;                /**< size added to default header size */
  nm_rpc_req_notifier_t p_notifier;   /**< notification function to call uppon req completion */
  void*p_notifier_ref;                /**< user-supplied parameter for the notifier */
  nm_prio_t priority;
};

PUK_LIST_DECLARE_TYPE(nm_rpc_token);
/** an incoming rpc request */
struct nm_rpc_token_s
{
  PUK_LIST_LINK(nm_rpc_token);
  nm_sr_request_t request;            /**< the sr request used for the full rpc_body_data */
  struct nm_datav_s headerv;
  struct nm_datav_s bodyv;
  struct nm_rpc_service_s*p_service;  /**< service this token belongs to */
  void*ref;                           /**< user ref for the token */
  int delayed;                        /**< if set to 1, unpack data later */
  nm_cond_status_t done;
};
PUK_LIST_CREATE_FUNCS(nm_rpc_token);

/** descriptor for a registered RPC service */
struct nm_rpc_service_s
{
  struct nm_rpc_token_list_s token_list;        /**< tokens given to rpc handlers */
  nm_spinlock_t token_list_lock;                /**< lock for the above list */
  nm_tag_t tag;                                 /**< tag(s) to listen to for this service */
  nm_tag_t tag_mask;                            /**< tag mask to apply to above tag */
  nm_rpc_handler_t p_handler;                   /**< user-supplied function, called upon header arrival */
  nm_rpc_finalizer_t p_finalizer;               /**< user-supplied function, called upon data body arrival */
  nm_session_t p_session;                       /**< session used to send/recv requests */
  void*ref;                                     /**< user-supplied ref for the service */
  int running;                                  /**< whether the service is currently running */
};


/* ** inline function ************************************** */

/** @internal private, exposed for inlining */
void nm_rpc_req_delete(nm_rpc_req_t p_rpc_req);

static inline void nm_rpc_send(nm_rpc_service_t p_service, nm_gate_t p_gate, nm_tag_t tag,
                               void*hptr, nm_len_t hlen, struct nm_data_s*p_body)
{
  nm_rpc_req_t p_req = nm_rpc_isend(p_service, p_gate, tag, hptr, hlen, p_body);
  nm_rpc_req_wait(p_req);
}

static inline void nm_rpc_req_set_priority(nm_rpc_req_t p_req, nm_prio_t priority)
{
  p_req->priority = priority;
}

static inline void nm_rpc_req_wait(nm_rpc_req_t p_req)
{
  nm_sr_swait(p_req->request.p_session, &p_req->request);
  nm_rpc_req_delete(p_req);
}

static inline void nm_rpc_req_wait_all(nm_rpc_req_t*p_reqs, int n)
{
  assert((void*)&p_reqs[0]->request == (void*)p_reqs[0]); /* nm_sr_request_t must be the first field in nm_rpc_req_s */
  nm_sr_request_wait_all((nm_sr_request_t**)p_reqs, n);
  int i;
  for(i = 0; i < n; i++)
    {
      if(p_reqs[i] != NULL)
        nm_rpc_req_delete(p_reqs[i]);
    }
}

static inline void*nm_rpc_service_get_ref(struct nm_rpc_service_s*p_service)
{
  return p_service->ref;
}

static inline nm_gate_t nm_rpc_get_source(struct nm_rpc_token_s*p_token)
{
  nm_gate_t p_gate = nm_sr_request_get_gate(&p_token->request);
  return p_gate;
}

static inline nm_tag_t nm_rpc_get_tag(struct nm_rpc_token_s*p_token)
{
  nm_tag_t tag = nm_sr_request_get_tag(&p_token->request);
  return tag;
}

static inline nm_rpc_service_t nm_rpc_get_service(struct nm_rpc_token_s*p_token)
{
  return p_token->p_service;
}

static inline void*nm_rpc_token_get_ref(struct nm_rpc_token_s*p_token)
{
  return p_token->ref;
}

static inline void nm_rpc_token_set_ref(struct nm_rpc_token_s*p_token, void*ref)
{
  p_token->ref = ref;
}

static inline void nm_rpc_token_delay(struct nm_rpc_token_s*p_token)
{
  p_token->delayed = 1;
}

static inline void nm_rpc_recv_header_data(nm_rpc_token_t p_token, struct nm_data_s*p_header)
{
  assert(nm_datav_size(&p_token->bodyv) == 0); /* all headers must be unpacked before body chunks */
  const nm_len_t peek_offset = nm_datav_size(&p_token->headerv); /* compute size of headers so far */
  const nm_len_t peek_size = nm_data_size(p_header);
  struct nm_data_s datav;
  nm_datav_add_chunk_data(&p_token->headerv, p_header);
  nm_data_datav_build(&datav, &p_token->headerv); /* build data type for full header */
  int rc = nm_sr_recv_peek_offset(p_token->p_service->p_session, &p_token->request, &datav, peek_offset, peek_size);
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("# nm_rpc: rc = %d in nm_sr_recv_peek()\n", rc);
    }
  nm_datav_uncommit(&p_token->headerv);
  nm_sr_recv_offset(p_token->p_service->p_session, &p_token->request, peek_offset + peek_size);
}

static inline void nm_rpc_recv_header(nm_rpc_token_t p_token, void*hptr, nm_len_t hlen)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)hptr, hlen);
  nm_rpc_recv_header_data(p_token, &data);
}

static inline void nm_rpc_irecv_body_data(struct nm_rpc_token_s*p_token, struct nm_data_s*p_body)
{
  nm_datav_add_chunk_data(&p_token->bodyv, p_body);
}

static inline void nm_rpc_irecv_body(nm_rpc_token_t p_token, void*ptr, nm_len_t len)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)ptr, len);
  nm_rpc_irecv_body_data(p_token, &data);
}

#endif /* NM_RPC_PRIVATE_H */
