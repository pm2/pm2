/*
 * NewMadeleine
 * Copyright (C) 2016-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_RPC_INTERFACE_H
#define NM_RPC_INTERFACE_H

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>

/** @defgroup rpc_interface RPC interface
 * This is the RPC-based interface, for remote procedure calls.
 *
 * Rationale of the RPC interface:
 *
 * RPC are remote procedure calls, i.e. function calls received from the
 * network, in background, independantly from application execution flow.
 * A service must be first registered, listening on a given tag, which sets
 * handlers called when a request is received and when it's finalized.
 * The first handler is called when a request arrives; this user-supplied
 * handler receives headers, with data available immediately, and based on
 * the content of headers, posts receive operations for the body. The data
 * for body will be available only in the finalizer.
 *
 * Both handlers and finalizers are called from within the main nmad event
 * loop (in non-threaded flavor) or from within a pioman ltask (threaded
 * flavor) and cannot make use of any blocking lock primitive nor call
 * any blocking nmad function not to block nmad progression. If in doubt
 * about what nmad primitive is allowed, most of them check the context in
 * which they are called in debug mode onyl, so run your code at least once
 * with a debug flavor of nmad to check whether it triggers a failed
 * assertion. However, nmad cannot check for incorrect use of mutex or
 * semaphores. For an interface similar to RPC, but with explicit receive
 * and blocking operation, please see the @ref pack_interface.
 *
 * Handlers get an opaque token that must be passed to functions to get
 * data (header & body) and meta-informations (source, tag, ...)
 *
 * To send rpc requests, the interface contains primitives to incrementaly
 * build a request (pack header, pack body) then send it in a non-blocking way.
 * A request may contain 0, 1 or multiple header chunks, and 0, 1, or multiple
 * body chunks. The content of headers will be available in the RPC handler;
 * the content of body chunks will be available in finalizer. Headers must be
 * shorter than the rendez-vous threshold (a few kilobytes, depends on driver).
 * The user _must_ wait the request completion or set a notifier; failling to
 * do so would result in memory leak. Data (header + body) must stay
 * valid until the request is notified as completed; failling to do so would
 * lead to data corruption.
 *
 * @example nm_rpc_hello.c
 */

/** @ingroup rpc_interface
 * @{
 */

/** a RPC registered service */
typedef struct nm_rpc_service_s*nm_rpc_service_t;

/** a RPC send request */
typedef struct nm_rpc_req_s*nm_rpc_req_t;

/** a RPC invocation token given to handlers */
typedef struct nm_rpc_token_s*nm_rpc_token_t;

/** a RPC handler called upon request invocation */
typedef void (*nm_rpc_handler_t)(nm_rpc_token_t p_token);

/** a RPC finalizer, called when all data has been received */
typedef void (*nm_rpc_finalizer_t)(nm_rpc_token_t p_token);

/** a RPC req notifier, called when the request send operation is completed */
typedef void (*nm_rpc_req_notifier_t)(nm_rpc_req_t p_req, void*ref);

/* ** service registration ********************************* */

/** register a new RPC service, listenning on the given tag/tag_mask.
 * The function is _not_ collective, there is no need to call it as the same
 * time on all involved nodes. If requests arrive before RPC service is
 * registered, they will be buffered and delivered as soon as the service
 * is registered */
nm_rpc_service_t nm_rpc_register(nm_session_t p_session, nm_tag_t tag, nm_tag_t tag_mask,
                                 nm_rpc_handler_t p_handler, nm_rpc_finalizer_t p_finalizer,
                                 void*ref);

/** stop listenning from RPC requests on given service */
void nm_rpc_unregister(nm_rpc_service_t p_service);

/** get a user reference given upon service registration */
static inline void*nm_rpc_service_get_ref(nm_rpc_service_t p_service);


/* ** send requests **************************************** */

/** initializes a new rpc send request */
nm_rpc_req_t nm_rpc_req_init(nm_rpc_service_t p_service,  nm_gate_t p_gate, nm_tag_t tag);

/** set priority of the rpc send request */
static inline void nm_rpc_req_set_priority(nm_rpc_req_t p_req, nm_prio_t priority);

/** pack a header chunk into an rpc request; may be called multiple times for multiple header chunks.
 * Data must remain valid until rpc send is completed.
 */
void nm_rpc_req_pack_header(nm_rpc_req_t p_rpc_req, const void*ptr, nm_len_t len);

/** pack a body chunk into an rpc request; may be called multiple times for multiple body chunks.
 * Data must remain valid until rpc send is completed.
 */
void nm_rpc_req_pack_body(nm_rpc_req_t p_rpc_req, const void*ptr, nm_len_t len);

/** pack a body chunk into an rpc request (using an nm_data type);
 * may be called multiple times for multiple body chunk.
 * Data must remain valid until rpc send is completed.
 */
void nm_rpc_req_pack_body_data(nm_rpc_req_t p_rpc_req, const struct nm_data_s*p_data);


/** add extra size to the default hlen, so as to consider beginning of body as header.
 * should be usefull only for tunneling or very specific use cases */
void nm_rpc_req_set_hlen(nm_rpc_req_t p_rpc_req, nm_len_t hlen);

/** send an rpc request once it is built; non-blocking operation, user must
 * set a notifier or explicitely wait for completion (but not both at the same time) */
void nm_rpc_req_isend(nm_rpc_req_t p_req);

/** wait for a send request completion */
static inline void nm_rpc_req_wait(nm_rpc_req_t p_req);

/** wait for completion of a vector of send requests */
static inline void nm_rpc_req_wait_all(nm_rpc_req_t*p_reqs, int n);

/** set a handler called upon RPC send completion */
void nm_rpc_req_set_notifier(nm_rpc_req_t p_req, nm_rpc_req_notifier_t p_notifier, void*ref);

/** Legacy function to send an rpc request with one header and one nm_data body; non-blocking */
nm_rpc_req_t nm_rpc_isend(nm_rpc_service_t p_service, nm_gate_t p_gate, nm_tag_t tag,
                          void*hptr, nm_len_t hlen, struct nm_data_s*p_body);

/** Legacy function to send an rpc request with one header and one nm_data body; blocking */
static inline void nm_rpc_send(nm_rpc_service_t p_service, nm_gate_t p_gate, nm_tag_t tag,
                               void*hptr, nm_len_t hlen, struct nm_data_s*p_body);


/* ** receive requests ************************************* */

/** get the source for the received request; to be called from a handler */
static inline nm_gate_t nm_rpc_get_source(nm_rpc_token_t p_token);

/** get the tag for the received request; to be called from a handler */
static inline nm_tag_t nm_rpc_get_tag(nm_rpc_token_t p_token);

/** get the service on which the given request has arrived */
static inline nm_rpc_service_t nm_rpc_get_service(nm_rpc_token_t p_token);

/** attach a user reference to a token, to transmit information between rpc handler and finalizer */
static inline void nm_rpc_token_set_ref(nm_rpc_token_t p_token, void*ref);

/** get a user reference previously attached to the token */
static inline void*nm_rpc_token_get_ref(nm_rpc_token_t p_token);

/** receive header; to be called from a handler; data is available immediately upon function return */
static inline void nm_rpc_recv_header(nm_rpc_token_t p_token, void*hptr, nm_len_t hlen);

/** receive header; to be called from a handler */
static inline void nm_rpc_recv_header_data(nm_rpc_token_t p_token, struct nm_data_s*p_header);

/** asynchronously posts the recv for the body of a received request; to be called from a handler;
 * there is no guarantee that data is available when this function completes.
 * data will be actually received in 'finalizer' */
static inline void nm_rpc_irecv_body(nm_rpc_token_t p_token, void*ptr, nm_len_t len);

/** asynchronously posts the recv for the body of a received request; to be called from a handler;
 * same as above, using nm_data instead of raw pointer
 */
static inline void nm_rpc_irecv_body_data(nm_rpc_token_t p_token, struct nm_data_s*p_body);

/** delay the unpacking of received body data. To be called in a handler. Then, to receive data,
 *  call @ref nm_rpc_token_complete.
 */
static inline void nm_rpc_token_delay(struct nm_rpc_token_s*p_token);

/** complete a delayed RPC request */
void nm_rpc_token_complete(struct nm_rpc_token_s*p_token);

/** @} */


/* ********************************************************* */

/** @internal include private header for inlining */
#include <nm_rpc_private.h>

#endif /* NM_RPC_INTERFACE_H */
