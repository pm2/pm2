/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_core_interface.h>
#include <nm_session_interface.h>
#include <nm_session_private.h>
#include <Padico/Puk.h>

#include <string.h>
#include <assert.h>

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_session, NULL, NULL, NULL);


static int nm_session_code_eq(const nm_session_hash_t key1, const nm_session_hash_t key2);
static uint32_t nm_session_code_hash(const nm_session_hash_t key);


PUK_HASHTABLE_TYPE(nm_session, nm_session_hash_t, nm_session_t,
                   nm_session_code_hash, nm_session_code_eq, NULL);

static struct
{
  nm_session_hashtable_t sessions;  /**< active sessions, hashed by session hashcode */

} nm_session =
  {
    .sessions = NULL,
  };


/* ********************************************************* */

static void nm_session_exit(void) __attribute__((destructor));

static void nm_session_exit(void)
{
  if(nm_session.sessions != NULL)
    {
      if(nm_session_hashtable_size(nm_session.sessions) > 0)
        {
#ifdef NMAD_DEBUG
          fprintf(stderr, "# nm_session: %zd pending sessions on exit. nmad core not properly destroyed.\n",
                  nm_session_hashtable_size(nm_session.sessions));
          nm_session_hashtable_enumerator_t e = nm_session_hashtable_enumerator_new(nm_session.sessions);
          nm_session_hash_t hash;
          nm_session_t p_session;
          nm_session_hashtable_enumerator_next2(e, &hash, &p_session);
          while(p_session != NULL)
            {
              fprintf(stderr, "# nm_session: hash = 0x%x; label = %s\n", p_session->hash_code, p_session->label);
              nm_session_hashtable_enumerator_next2(e, &hash, &p_session);
            }
          nm_session_hashtable_enumerator_delete(e);
#endif /* NMAD_DEBUG */
        }
      nm_session_hashtable_delete(nm_session.sessions);
      nm_session.sessions = NULL;
    }
}

static int nm_session_code_eq(const nm_session_hash_t key1, const nm_session_hash_t key2)
{
  return (key1 == key2);
}

static uint32_t nm_session_code_hash(const nm_session_hash_t key)
{
  return key;
}


/* ********************************************************* */
/* ** Session interface implementation */

int nm_session_open(nm_session_t*pp_session, const char*label)
{
  if(!nm_session.sessions)
    {
      /* lazy init */
      nm_session.sessions = nm_session_hashtable_new();
    }
  const int len = strlen(label);
  const nm_session_hash_t hash_code = puk_checksum_siphash((void*)label, len);
  struct nm_session_s*p_session = nm_session_hashtable_lookup(nm_session.sessions, hash_code);
  if(p_session != NULL)
    {
      if(strcmp(label, p_session->label) != 0)
        {
          NM_FATAL("hash collision detected (%s / %s).\n", label, p_session->label);
        }
      else
        {
          NM_WARN("trying to open duplicate session %s\n", label);
        }
      *pp_session = NULL;
      return -NM_EALREADY;
    }
  p_session = padico_malloc(sizeof(struct nm_session_s));
  p_session->label = padico_strdup(label);
  p_session->hash_code = hash_code;
  p_session->p_sr_session = NULL;
  p_session->p_sr_destructor = NULL;
  nm_session_hashtable_insert(nm_session.sessions, p_session->hash_code, p_session);
  *pp_session = p_session;
  return NM_ESUCCESS;
}


#ifdef NMAD_DEBUG
static void nm_session_check_reqs_list(nm_session_t p_session, struct nm_req_list_s*p_req_list)
{
  if(!nm_req_list_empty(p_req_list))
    {
      nm_req_itor_t i;
      puk_list_foreach(nm_req, i, p_req_list)
        {
          NM_WARN("closing session %s with pending request %p; p_gate = %p; tag = %lx:%x; tag_mask = %lx:%x; expected_len = %lu\n",
                  p_session->label, i, i->p_gate, i->tag.tag, i->tag.hashcode, i->unpack.tag_mask.tag, i->unpack.tag_mask.hashcode, i->unpack.expected_len);
        }
    }
}
/** check pending requests upon session close */
static void nm_session_check_pending_reqs(nm_session_t p_session)
{
  const uint32_t hashcode = p_session->hash_code;
  nm_core_t p_core = nm_core_get_singleton();
  nm_core_lock(p_core);
  /* get reqs on wildcard queue */
  nm_core_tag_t session_tag = nm_core_tag_build(hashcode, 0);
  struct nm_matching_wildcard_s*p_wildcard = nm_matching_wildcard_bytag(p_core, session_tag);
  nm_session_check_reqs_list(p_session, &p_wildcard->unpacks);
  /* reqs on tags queues- iterate on all tags */
  struct nm_matching_tag_table_s*p_tag_table = &p_core->tag_table;
  nm_core_tag_vect_t p_tags = nm_matching_tag_get_tags(p_tag_table);
  nm_core_tag_vect_itor_t itag;
  puk_vect_foreach(itag, nm_core_tag, p_tags)
    {
      if(itag->hashcode == hashcode)
        {
          struct nm_matching_tag_s*p_matching_tag = nm_matching_tag_get(p_tag_table, *itag);
          nm_session_check_reqs_list(p_session, &p_matching_tag->unpacks);
        }
    }
  nm_core_tag_vect_delete(p_tags);
  /* reqs on gsession queues- iterate on all gates */
  nm_gate_t p_gate = NULL;
  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      struct nm_matching_gsession_s*p_matching_gsession = nm_matching_gsession_bytag(p_gate, session_tag);
      nm_session_check_reqs_list(p_session, &p_matching_gsession->unpacks);
    }
  /* reqs on gtag queues- iterate on all gate/tag */
  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      p_tags = nm_gtag_get_tags(&p_gate->tags);
      puk_vect_foreach(itag, nm_core_tag, p_tags)
        {
          if(itag->hashcode == hashcode)
            {
              struct nm_gtag_s*p_gtag = nm_gtag_get(&p_gate->tags, *itag);
              nm_session_check_reqs_list(p_session, &p_gtag->unpacks);
            }
        }
      nm_core_tag_vect_delete(p_tags);
    }
  /* unexpected- all chunks are in a single queue */
  if(!nm_unexpected_wildcard_list_empty(&p_wildcard->unexpected))
    {
      NM_WARN("closing session %s with %d pending unexpected messages.\n", p_session->label, nm_unexpected_wildcard_list_size(&p_wildcard->unexpected));
    }
  nm_core_unlock(p_core);
}
#endif /* NMAD_DEBUG */

int nm_session_close(nm_session_t p_session)
{
  uint32_t hashcode = p_session->hash_code;
  if(nm_session_hashtable_lookup(nm_session.sessions, hashcode) == NULL)
    {
      NM_FATAL("cannot find session hash=%d; p_session=%p in session base- cannot destroy.\n",
               hashcode, p_session);
    }

  /* ** detect pending req & unexpected */
#ifdef NMAD_DEBUG
  nm_session_check_pending_reqs(p_session);
#endif /* NMAD_DEBUG */

  if(p_session->p_sr_destructor)
    {
      (*p_session->p_sr_destructor)(p_session);
    }
  nm_session_hashtable_remove(nm_session.sessions, p_session->hash_code);
  padico_free((void*)p_session->label);
  padico_free(p_session);
  return NM_ESUCCESS;
}

nm_session_t nm_session_lookup(nm_session_hash_t hashcode)
{
  struct nm_session_s*p_session = nm_session_hashtable_lookup(nm_session.sessions, hashcode);
  return p_session;
}

const char*nm_session_get_name(nm_session_t p_session)
{
  return p_session->label;
}
