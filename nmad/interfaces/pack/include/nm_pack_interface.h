/*
 * NewMadeleine
 * Copyright (C) 2006-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_PACK_INTERFACE_H
#define NM_PACK_INTERFACE_H

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>

/** @defgroup pack_interface Pack interface
 * This is the nmad Pack interface, an nmad interface with incremental 
 * packing/unpacking of messages, and fully blocking interface.
 * It is similar, although not indentical, to the old interface from mad3.
 *
 * @example nm_pack_headers.c
 */


/** Content for the request in the pack interface.
 */
struct nm_pack_cnx_s
{
  nm_session_t p_session;    /**< session used for the connexion. */
  nm_gate_t gate;            /**< Source or destination gate. */
  nm_tag_t tag;              /**< Message tag. */
  nm_sr_request_t req;       /**< sendrecv request */
  struct nm_datav_s express; /**< vector of data in expresss mode */
  struct nm_datav_s datav;   /**< vector of regular data */
};

/** @ingroup pack_interface
 * @{
 */

/** A request for the pack interface.
 */
typedef struct nm_pack_cnx_s nm_pack_cnx_t;

/** Start building a new message for sending.
 *  @param p_session a pointer to a nmad session object.
 *  @param p_gate the gate to the destination.
 *  @param tag the message tag.
 *  @param p_cnx pointer to a pack request allocated by user
 *  @return The NM status.
 */
extern int nm_begin_packing(nm_session_t p_session, nm_gate_t p_gate, nm_tag_t tag, nm_pack_cnx_t*p_cnx);

/** Append a data fragment to the current message.
 *  @param p_cnx a NM/SO connection pointer.
 *  @param ptr a pointer to the data fragment.
 *  @param len the length of the data fragment.
 *  @return The NM status.
 */
extern int nm_pack(nm_pack_cnx_t*p_cnx, const void*ptr, nm_len_t len);

/** Append a data fragment to the current message, in express mode:
 * data will be aavailable immediately after unpack in receiver side. */
extern int nm_pack_express(nm_pack_cnx_t*p_cnx, const void*ptr, nm_len_t len);

/** End building and send the current message.
 *  @param p_cnx a NM/SO connection pointer.
 *  @return The NM status.
 */
extern int nm_end_packing(nm_pack_cnx_t*p_cnx);

/** Start receiving and extracting a new message.
 *  @param p_session a pointer to a nmad session object.
 *  @param p_gate the gate of the source or -1 for receiving from any source.
 *  @param tag the message tag.
 *  @param p_cnx pointer to a pack request allocated by user
 *  @return The NM status.
 */
extern int nm_begin_unpacking(nm_session_t p_session, nm_gate_t p_gate, nm_tag_t tag, nm_pack_cnx_t*p_cnx);

/** Extract a data fragment from the current message; data will actually
 * be available after end_unpacking.
 *  @param p_cnx a NM/SO connection pointer.
 *  @param ptr a pointer to the data fragment.
 *  @param len the length of the data fragment.
 *  @return The NM status.
 */
extern int nm_unpack(nm_pack_cnx_t*p_cnx, void*ptr, nm_len_t len);

/** Extract a data fragment from the current message in express mode: data
 * will be available upon function return.
 * Note: data must have been sent with pack_express primitive. */
extern int nm_unpack_express(nm_pack_cnx_t*p_cnx, void*ptr, nm_len_t len);

/** End receiving and flush extraction of the current message.
 *  @param p_cnx a NM/SO connection pointer.
 *  @return The NM status.
 */
extern int nm_end_unpacking(nm_pack_cnx_t*p_cnx);

/** @} */

#endif /* NM_PACK_INTERFACE_H */
