/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>

#include <Padico/Puk.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_pack_interface.h>

PADICO_MODULE_HOOK(nmad);


/* ********************************************************* */

struct nm_pack_data_content_s
{
  struct nm_pack_cnx_s*p_cnx;
};

static void nm_pack_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context);
const struct nm_data_ops_s nm_pack_data_ops =
  {
    .p_traversal = &nm_pack_data_traversal
  };
NM_DATA_TYPE(pack, struct nm_pack_data_content_s, &nm_pack_data_ops);

static void nm_pack_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context)
{
  const struct nm_pack_data_content_s*p_content = _content;
  const struct nm_pack_cnx_s*p_cnx = p_content->p_cnx;
  int i;
  for(i = 0; i < p_cnx->express.n_data; i++)
    {
      nm_data_traversal_apply(&p_cnx->express.p_data[i], p_apply, _context);
    }
  for(i = 0; i < p_cnx->datav.n_data; i++)
    {
      nm_data_traversal_apply(&p_cnx->datav.p_data[i], p_apply, _context);
    }
}
void nm_pack_data_build(struct nm_data_s*p_pack_data, struct nm_pack_cnx_s*p_cnx)
{
  nm_data_pack_set(p_pack_data, (struct nm_pack_data_content_s)
                   {
                     .p_cnx = p_cnx
                   });
}

/* ********************************************************* */

static inline void nm_pack_cnx_init(nm_session_t p_session, nm_gate_t gate, nm_tag_t tag, nm_pack_cnx_t*p_cnx)
{
  p_cnx->p_session = p_session;
  p_cnx->gate      = gate;
  p_cnx->tag       = tag;
  nm_datav_init(&p_cnx->datav);
  nm_datav_init(&p_cnx->express);
}

int nm_begin_packing(nm_session_t p_session, nm_gate_t gate, nm_tag_t tag, nm_pack_cnx_t*p_cnx)
{
  nm_pack_cnx_init(p_session, gate, tag, p_cnx);
  nm_sr_send_init(p_cnx->p_session, &p_cnx->req);
  nm_sr_send_dest(p_cnx->p_session, &p_cnx->req, p_cnx->gate, p_cnx->tag);
  return NM_ESUCCESS;
}

int nm_pack(nm_pack_cnx_t*p_cnx, const void*ptr, nm_len_t len)
{
  nm_datav_add_chunk(&p_cnx->datav, ptr, len);
  return NM_ESUCCESS;
}

int nm_pack_express(nm_pack_cnx_t*p_cnx, const void*ptr, nm_len_t len)
{
  nm_datav_add_chunk(&p_cnx->express, ptr, len);
  return NM_ESUCCESS;
}

int nm_end_packing(nm_pack_cnx_t*p_cnx)
{
  const nm_len_t express_size = nm_datav_size(&p_cnx->express);
  struct nm_data_s data;
  nm_pack_data_build(&data, p_cnx);
  nm_sr_send_pack_data(p_cnx->p_session, &p_cnx->req, &data);
  nm_sr_send_header(p_cnx->p_session, &p_cnx->req, express_size);
  nm_sr_send_submit(p_cnx->p_session, &p_cnx->req);
  int rc = nm_sr_swait(p_cnx->p_session, &p_cnx->req);
  nm_datav_destroy(&p_cnx->datav);
  nm_datav_destroy(&p_cnx->express);
  return rc;
}

int nm_begin_unpacking(nm_session_t p_session, nm_gate_t gate, nm_tag_t tag, nm_pack_cnx_t*p_cnx)
{
  nm_pack_cnx_init(p_session, gate, tag, p_cnx);
  nm_sr_recv_init(p_cnx->p_session, &p_cnx->req);
  nm_sr_recv_match(p_cnx->p_session, &p_cnx->req, p_cnx->gate, p_cnx->tag, NM_TAG_MASK_FULL);
  nm_sr_recv_post(p_cnx->p_session, &p_cnx->req);
  return NM_ESUCCESS;
}

int nm_unpack(nm_pack_cnx_t*p_cnx, void*ptr, nm_len_t len)
{
  nm_datav_add_chunk(&p_cnx->datav, ptr, len);
  return NM_ESUCCESS;
}

int nm_unpack_express(nm_pack_cnx_t*p_cnx, void*ptr, nm_len_t len)
{
  nm_datav_add_chunk(&p_cnx->express, ptr, len);
  struct nm_data_s data;
  nm_data_datav_build(&data, &p_cnx->express);
  nm_sr_recv_data_wait(p_cnx->p_session, &p_cnx->req);
  int rc = nm_sr_recv_peek_offset(p_cnx->p_session, &p_cnx->req, &data, nm_data_size(&data) - len, len);
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("nm_unpack_express()- peek rc = %d\n", rc);
    }
  const nm_len_t offset = nm_datav_size(&p_cnx->express);
  nm_sr_recv_offset(p_cnx->p_session, &p_cnx->req, offset);
  nm_datav_uncommit(&p_cnx->express);
  return NM_ESUCCESS;
}

int nm_end_unpacking(nm_pack_cnx_t*p_cnx)
{
  struct nm_data_s data;
  nm_pack_data_build(&data, p_cnx);
  nm_sr_recv_unpack_data(p_cnx->p_session, &p_cnx->req, &data);
  int rc = nm_sr_rwait(p_cnx->p_session, &p_cnx->req);
  nm_datav_destroy(&p_cnx->datav);
  nm_datav_destroy(&p_cnx->express);
  return rc;
}
