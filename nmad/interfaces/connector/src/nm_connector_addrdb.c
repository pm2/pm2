/*
 * NewMadeleine
 * Copyright (C) 2011-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_connector.h"
#include <nm_private_config.h>
#include <nm_public.h>
#include <Padico/Module.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef NMAD_PADICOTM

#include <Padico/AddrDB.h>

struct nm_connector_addrdb_s
{
  int addr_len;
};

static void*nm_connector_addrdb_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_connector_addrdb_destroy(void*_status);

static const struct puk_component_driver_s nm_connector_addrdb_component =
  {
    .instantiate = &nm_connector_addrdb_instantiate,
    .destroy     = &nm_connector_addrdb_destroy
  };

static void nm_connector_addrdb_create(void*_status, int addr_len, const char**url);
static int  nm_connector_addrdb_exchange(void*_status,
                                         const char*local_connector_url, const char*remote_connector_url,
                                         const void*local_cnx_addr, void*remote_cnx_addr);

static const struct nm_connector_iface_s nm_connector_addrdb =
  {
    .create   = &nm_connector_addrdb_create,
    .exchange = &nm_connector_addrdb_exchange
  };

PADICO_MODULE_COMPONENT(NewMad_Connector_addrdb,
  puk_component_declare("NewMad_Connector_addrdb",
                        puk_component_provides("PadicoComponent", "component", &nm_connector_addrdb_component),
                        puk_component_provides("NewMad_Connector", "connector", &nm_connector_addrdb)));

/* ********************************************************* */

static void*nm_connector_addrdb_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_connector_addrdb_s*p_connector_addrdb = padico_malloc(sizeof(struct nm_connector_addrdb_s));
  return p_connector_addrdb;
}

static void nm_connector_addrdb_destroy(void*_status)
{
  struct nm_connector_addrdb_s*p_connector_addrdb = _status;
  padico_free(p_connector_addrdb);
}

static void nm_connector_addrdb_create(void*_status, int addr_len, const char**url)
{
  struct nm_connector_addrdb_s*p_connector_addrdb = _status;
  p_connector_addrdb->addr_len = addr_len;
  padico_string_t s_url = padico_string_new();
  padico_string_printf(s_url, "%s:%p",
                       (const char*)padico_topo_node_getuuid(padico_topo_getlocalnode()),
                       p_connector_addrdb);
  *url = padico_string_get(s_url);
}

static int nm_connector_addrdb_exchange(void*_status,
                                        const char*local_connector_url, const char*remote_connector_url,
                                        const void*local_cnx_addr, void*remote_cnx_addr)
{
  struct nm_connector_addrdb_s*p_connector_addrdb = _status;
  const char*component_id = "NewMad_Connector_addrdb";
  char*remote_url = padico_strdup(remote_connector_url);
  char*saveptr = NULL;
  char*s_uuid = strtok_r(remote_url, ":", &saveptr);
  padico_topo_node_t remote_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)s_uuid);
  assert(remote_node != NULL);
  padico_addrdb_publish(remote_node, component_id,
                        local_connector_url, strlen(local_connector_url),
                        local_cnx_addr, p_connector_addrdb->addr_len);
  padico_req_t req = padico_tm_req_new(NULL, NULL);
  padico_addrdb_get(remote_node, component_id,
                    remote_connector_url, strlen(remote_connector_url),
                    remote_cnx_addr, p_connector_addrdb->addr_len, req);
  padico_tm_req_wait(req);
  padico_free(remote_url);
  return NM_ESUCCESS;
}

#else /* NMAD_PADICOTM */

PADICO_MODULE_INIT(nm_connector_addrdb_init);

static void nm_connector_addrdb_init(void)
{
  NM_FATAL("nm_connector_addrdb: cannot use AddrDB, nmad is built without PadicoTM.\n");
}

#endif /* NMAD_PADICOTM */
