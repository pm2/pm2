/*
 * NewMadeleine
 * Copyright (C) 2011-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_connector.h"
#include <nm_private.h>

#include <Padico/Module.h>

PADICO_MODULE_DECLARE(NewMad_Connector, NULL, NULL, NULL);

struct nm_connector_s
{
  puk_component_t component;
  puk_instance_t instance;
  struct puk_receptacle_NewMad_Connector_s r;
};

/* ********************************************************* */

struct nm_connector_s*nm_connector_create(int addr_len, const char**url)
{
  struct nm_connector_s*p_connector = padico_malloc(sizeof(struct nm_connector_s));
  const char*env_connector = getenv("NM_CONNECTOR");
  if(env_connector)
    {
      NM_DISPF("nm_connector: connector = %s forced by user.\n", env_connector);
    }
  else
    {
      env_connector = "udp";
    }
  padico_string_t s_connector = padico_string_new();
  padico_string_printf(s_connector, "NewMad_Connector_%s", env_connector);
  p_connector->component = puk_component_resolve(padico_string_get(s_connector));
  padico_string_delete(s_connector);
  p_connector->instance = puk_component_instantiate(p_connector->component);
  puk_instance_indirect_NewMad_Connector(p_connector->instance, NULL, &p_connector->r);
  (*p_connector->r.driver->create)(p_connector->r._status, addr_len, url);
  return p_connector;
}

void nm_connector_destroy(struct nm_connector_s*p_connector)
{
  puk_instance_destroy(p_connector->instance);
  p_connector->instance = NULL;
  p_connector->component = NULL;
  padico_free(p_connector);
}

int nm_connector_exchange(struct nm_connector_s*p_connector,
                          const char*local_connector_url, const char*remote_connector_url,
                          const void*local_cnx_addr, void*remote_cnx_addr)
{
  return (*p_connector->r.driver->exchange)(p_connector->r._status,
                                            local_connector_url, remote_connector_url,
                                            local_cnx_addr, remote_cnx_addr);
}
