/*
 * NewMadeleine
 * Copyright (C) 2006-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_AML_H
#define NM_AML_H

#include <nm_mpi.h>

//MPI-like init,finalize calls
extern int  aml_init(int *,char***);
extern void aml_finalize(void);
//barrier which ensures that all AM sent before the barrier are completed everywhere after the barrier
extern void aml_barrier( void );
//register active message function(collective call)
extern void aml_register_handler(void(*f)(int,void*,int),int n);
//send AM to another(myself is ok) node
//execution of AM might be delayed till next aml_barrier() call
extern void aml_send(void *srcaddr, int type,int length, int node );

// rank and size
extern int aml_my_pe( void );
extern int aml_n_pes( void );

#define my_pe aml_my_pe
#define num_pes aml_n_pes

#define aml_time() MPI_Wtime()
#define aml_long_allsum(p) MPI_Allreduce(MPI_IN_PLACE,p,1,MPI_LONG_LONG,MPI_SUM,MPI_COMM_WORLD)
#define aml_long_allmin(p) MPI_Allreduce(MPI_IN_PLACE,p,1,MPI_LONG_LONG,MPI_MIN,MPI_COMM_WORLD)
#define aml_long_allmax(p) MPI_Allreduce(MPI_IN_PLACE,p,1,MPI_LONG_LONG,MPI_MAX,MPI_COMM_WORLD)

#endif /* NM_AML_H */

