/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_aml.h"

#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>
#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <mpi.h>

#define NM_AML_BUFFER_SIZE 4096

static struct
{
  nm_session_t p_session;
  int size;
  int rank;
  nm_comm_t p_comm;
  void(*handler)(int from, void*data, int size);
  struct nm_data_s data;
  void*p_buf;
  nm_len_t buf_size;
  nm_sr_request_t request;
  int*send_msg_count;
  int*recv_msg_count;
  int barrier_in_progress;
  int posted;
} nm_aml =
  {
    .size = -1,
    .rank = -1,
    .p_session = NULL,
    .buf_size = NM_LEN_UNDEFINED,
    .send_msg_count = NULL,
    .recv_msg_count = NULL,
    .barrier_in_progress = 0,
    .posted = 0
  };

static const nm_tag_t handler_tag = 0x01;
static const nm_tag_t barrier_tag = 0xF2;
static const nm_tag_t register_tag = 0xF1;
static const nm_tag_t barrier_count_tag = 0xF3;

static void nm_aml_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);

//MPI-like init,finalize calls
extern int aml_init(int*argc,char***argv)
{
  fprintf(stderr, "# using nmad AML\n");
  MPI_Init(argc, argv);
  nm_launcher_init(argc, *argv);
  nm_session_open(&nm_aml.p_session, "aml");
  nm_launcher_get_size(&nm_aml.size);
  nm_launcher_get_rank(&nm_aml.rank);
  nm_aml.p_comm = nm_comm_world("aml-sync");
  nm_aml.p_buf = padico_malloc(NM_AML_BUFFER_SIZE);
  nm_aml.send_msg_count = padico_malloc(sizeof(int) * nm_aml.size);
  nm_aml.recv_msg_count = padico_malloc(sizeof(int) * nm_aml.size);
  int i;
  for(i = 0; i < nm_aml.size; i++)
    {
      nm_aml.send_msg_count[i] = 0;
      nm_aml.recv_msg_count[i] = 0;
    }
  return 0;
}

extern void aml_finalize(void)
{
  nm_comm_destroy(nm_aml.p_comm);
  nm_session_close(nm_aml.p_session);
  MPI_Finalize();
  nm_launcher_exit();
  padico_free(nm_aml.p_buf);
}

static void nm_aml_flush_epoch(void)
{
  int peer_msg_count[nm_aml.size];
  int i;
  for(i = 0; i < nm_aml.size; i++)
    {
      peer_msg_count[i] = 0;
      nm_gate_t p_gate;
      nm_launcher_get_gate(i, &p_gate);
      nm_sr_send(nm_aml.p_session, p_gate, barrier_count_tag, &nm_aml.send_msg_count[i], sizeof(int));
      nm_sr_recv(nm_aml.p_session, p_gate, barrier_count_tag, &peer_msg_count[i], sizeof(int));
      fprintf(stderr, "# %d # -> %d; recv_count = %d; peer_send_count = %d\n",
              nm_aml.rank, i, nm_aml.recv_msg_count[i], peer_msg_count[i]);
      if(nm_aml.recv_msg_count[i] != peer_msg_count[i])
        {
          fprintf(stderr, "# ********************************************************\n");
          abort();
        }
    }
    for(i = 0; i < nm_aml.size; i++)
      {
        nm_aml.send_msg_count[i] = 0;
        nm_aml.recv_msg_count[i] = 0;
      }
}

//barrier which ensures that all AM sent before the barrier are completed everywhere after the barrier
extern void aml_barrier(void)
{
  fprintf(stderr, "# %d # aml_barrier() ########\n", nm_aml.rank);
#warning TODO- cancel pendant le flush!
  nm_sr_flush(nm_aml.p_session);
  nm_coll_barrier(nm_aml.p_comm, barrier_tag);
  nm_sr_flush(nm_aml.p_session);
  nm_aml.barrier_in_progress = 1;

  nm_aml_flush_epoch();

  nm_sr_flush(nm_aml.p_session);
  nm_coll_barrier(nm_aml.p_comm, barrier_tag);
  nm_aml.barrier_in_progress = 0;
  nm_coll_barrier(nm_aml.p_comm, barrier_tag);
  fprintf(stderr, "# %d # aml_barrier() done. ########\n", nm_aml.rank);
}

static inline void nm_aml_rpc_reload(void)
{
  if(!nm_aml.posted)
    {
      nm_sr_recv_init(nm_aml.p_session, &nm_aml.request);
      nm_data_contiguous_build(&nm_aml.data, nm_aml.p_buf, NM_AML_BUFFER_SIZE);
      nm_sr_recv_unpack_data(nm_aml.p_session, &nm_aml.request, &nm_aml.data);
      nm_sr_request_set_ref(&nm_aml.request, NULL);
      nm_sr_request_monitor(nm_aml.p_session, &nm_aml.request, NM_SR_EVENT_FINALIZED, &nm_aml_handler);
      nm_sr_recv_irecv(nm_aml.p_session, &nm_aml.request, NM_ANY_GATE, handler_tag, NM_TAG_MASK_FULL);
      nm_aml.posted = 1;
    }
}

static void nm_aml_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  if(nm_aml.barrier_in_progress)
    {
      fprintf(stderr, "# ## ##### nm_aml_handler()- barrier in progress.\n");
    }
  assert(event & NM_SR_EVENT_FINALIZED);
  assert(nm_aml.buf_size == NM_LEN_UNDEFINED);
  nm_sr_request_get_size(&nm_aml.request, &nm_aml.buf_size);
  if(event & NM_SR_EVENT_FINALIZED)
    {
      nm_gate_t p_gate = nm_sr_request_get_gate(&nm_aml.request);
      int from = -1;
      nm_launcher_get_dest(p_gate, &from);
      nm_aml.recv_msg_count[from]++;
      (*nm_aml.handler)(from, nm_aml.p_buf, nm_aml.buf_size);
      nm_aml.posted = 0;
      nm_aml_rpc_reload();
    }
  nm_aml.buf_size = NM_LEN_UNDEFINED;
}

//register active message function(collective call)
extern void aml_register_handler(void(*f)(int,void*,int),int n)
{
  fprintf(stderr, "# %d # aml_register_handler()- n = %d\n", nm_aml.rank, n);
  nm_sr_flush(nm_aml.p_session);
  nm_coll_barrier(nm_aml.p_comm, register_tag);
  nm_sr_flush(nm_aml.p_session);
  assert(n == 1);
  nm_aml.handler = f;
  nm_aml_flush_epoch();
  nm_aml_rpc_reload();
  nm_coll_barrier(nm_aml.p_comm, register_tag);
}

//send AM to another(myself is ok) node
//execution of AM might be delayed till next aml_barrier() call
extern void aml_send(void*srcaddr, int type, int length, int node)
{
  nm_aml.send_msg_count[node]++;
  /*
    fprintf(stderr, "# %d # aml_send()- length = %d; node = %d; n = %d\n",
    nm_aml.rank, length, node, nm_aml.msg_count[node]);
  */
  assert(type == 1);
  assert(length <= NM_AML_BUFFER_SIZE);
  nm_gate_t p_gate;
  nm_launcher_get_gate(node, &p_gate);
  const nm_tag_t tag = handler_tag;
  nm_sr_send(nm_aml.p_session, p_gate, tag, srcaddr, length);
}

// rank and size
extern int aml_my_pe(void)
{
  assert(nm_aml.rank != -1);
  return nm_aml.rank;
}
extern int aml_n_pes(void)
{
  assert(nm_aml.size != -1);
  return nm_aml.size;
}
