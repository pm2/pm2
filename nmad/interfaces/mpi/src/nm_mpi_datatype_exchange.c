/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ** Hashtable types definition *************************** */

static inline nm_mpi_datatype_hash_t nm_mpi_datatype_hash_common(const nm_mpi_datatype_t*p_datatype);
static        uint32_t nm_mpi_datatype_hash(const nm_mpi_datatype_hash_t*p_hash);
static        int      nm_mpi_datatype_eq(const nm_mpi_datatype_hash_t*p_hash1, const nm_mpi_datatype_hash_t*p_hash2);
static void nm_mpi_datatype_hash_destructor(nm_mpi_datatype_hash_t*hash, struct nm_mpi_datatype_s*p_datatype);
static inline void     nm_mpi_datatype_hashtable_remove_hash(nm_mpi_datatype_hash_t hash);

PUK_HASHTABLE_TYPE(nm_mpi_datatype_hash, nm_mpi_datatype_hash_t*, struct nm_mpi_datatype_s*,
                   &nm_mpi_datatype_hash, &nm_mpi_datatype_eq, &nm_mpi_datatype_hash_destructor);

/* ** Hashtable types definition *************************** */

struct nm_mpi_datatype_exchange_hash_key_s
{
  nm_gate_t target;
  MPI_Datatype datatype; /**< local MPI_Datatype id */
};
struct nm_mpi_datatype_exchange_hash_entry_s
{
  struct nm_mpi_datatype_exchange_hash_key_s key;
  nm_mpi_datatype_hash_t hash;
};

static uint32_t nm_mpi_datatype_exchange_hash(const struct nm_mpi_datatype_exchange_hash_key_s*p_key);
static int nm_mpi_datatype_exchange_eq(const struct nm_mpi_datatype_exchange_hash_key_s*p_key1,
                                                   const struct nm_mpi_datatype_exchange_hash_key_s*p_key2);
static void nm_mpi_datatype_exchange_destructor(struct nm_mpi_datatype_exchange_hash_key_s*p_key,
                                                struct nm_mpi_datatype_exchange_hash_entry_s*p_entry);

PUK_HASHTABLE_TYPE(nm_mpi_exchanged_datatypes, struct nm_mpi_datatype_exchange_hash_key_s*,
                   struct nm_mpi_datatype_exchange_hash_entry_s*,
                   &nm_mpi_datatype_exchange_hash,
                   &nm_mpi_datatype_exchange_eq,
                   &nm_mpi_datatype_exchange_destructor);

static inline void     nm_mpi_datatype_exchange_insert(nm_gate_t target, MPI_Datatype datatype, nm_mpi_datatype_hash_t hash);
static inline void     nm_mpi_datatype_exchange_remove(nm_gate_t target, MPI_Datatype datatype);

/* ********************************************************* */

static struct
{
  nm_mpi_spinlock_t lock;
  struct nm_mpi_datatype_hash_hashtable_s hashed_datatypes;  /**< store datatypes hashed by hash */
  struct nm_mpi_exchanged_datatypes_hashtable_s exchanged_datatypes; /**< datatypes sent to the given target */
} nm_mpi_datatype_exchange;

/* ** Datatype exchanging interface ************************ */

static        void     nm_mpi_datatype_request_recv(nm_sr_event_t event, const nm_sr_event_info_t*info,
                                                    void*ref);
static        void     nm_mpi_datatype_request_monitor(nm_sr_event_t event,
                                                       const nm_sr_event_info_t*info, void*ref);
static        void     nm_mpi_datatype_request_free(nm_sr_event_t event, const nm_sr_event_info_t*info,
                                                    void*ref);

static struct nm_sr_monitor_s nm_mpi_datatype_requests_monitor =
  { .p_notifier = &nm_mpi_datatype_request_recv,
    .event_mask = NM_SR_EVENT_RECV_UNEXPECTED,
    .p_gate     = NM_ANY_GATE,
    .tag        = NM_MPI_TAG_PRIVATE_TYPE_ADD,
    .tag_mask   = NM_MPI_TAG_PRIVATE_BASE | 0xFF };


static void nm_mpi_datatype_wrapper_traversal_apply(const void*_content, nm_data_apply_t apply, void*_context);
static void nm_mpi_datatype_serial_traversal_apply(const void*_content, nm_data_apply_t apply, void*_context);

const struct nm_data_ops_s nm_mpi_datatype_wrapper_ops =
  {
    .p_traversal = &nm_mpi_datatype_wrapper_traversal_apply
  };

const struct nm_data_ops_s nm_mpi_datatype_serialize_ops =
  {
    .p_traversal = &nm_mpi_datatype_serial_traversal_apply
  };

/* ********************************************************* */

__PUK_SYM_INTERNAL
void nm_mpi_datatype_exchange_init(void)
{
  nm_mpi_spin_init(&nm_mpi_datatype_exchange.lock);
  /* Initialize hash -> datatype hashtable */
  nm_mpi_datatype_hash_hashtable_init(&nm_mpi_datatype_exchange.hashed_datatypes);
  /* Initialize exchanged datatypes hashtable */
  nm_mpi_exchanged_datatypes_hashtable_init(&nm_mpi_datatype_exchange.exchanged_datatypes);

  /* Initialize the asynchronous mecanism for datatype exchange */
  nm_mpi_comm_lazy_init();
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(MPI_COMM_WORLD);
  nm_session_t       p_session = nm_mpi_communicator_get_session(p_comm);
  nm_sr_session_monitor_set(p_session, &nm_mpi_datatype_requests_monitor);
}

__PUK_SYM_INTERNAL
void nm_mpi_datatype_exchange_exit(void)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(MPI_COMM_WORLD);
  nm_session_t       p_session = nm_mpi_communicator_get_session(p_comm);
  nm_sr_session_monitor_remove(p_session, &nm_mpi_datatype_requests_monitor);
  nm_mpi_comm_lazy_exit();
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  nm_mpi_datatype_hash_hashtable_destroy(&nm_mpi_datatype_exchange.hashed_datatypes);
  nm_mpi_exchanged_datatypes_hashtable_destroy(&nm_mpi_datatype_exchange.exchanged_datatypes);
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
}

/* ********************************************************* */
/* ** Datatype hash table management *********************** */


static void nm_mpi_datatype_hash_destructor(nm_mpi_datatype_hash_t*hash, struct nm_mpi_datatype_s*p_datatype)
{
  if(p_datatype->id >= _NM_MPI_DATATYPE_OFFSET)
    nm_mpi_datatype_ref_dec(p_datatype, NULL);
}

static uint32_t nm_mpi_datatype_hash(const nm_mpi_datatype_hash_t*p_hash)
{
  assert(sizeof(nm_mpi_datatype_hash_t) == sizeof(uint32_t));
  return *p_hash;
}

static int nm_mpi_datatype_eq(const nm_mpi_datatype_hash_t*p_hash1, const nm_mpi_datatype_hash_t*p_hash2)
{
  const int eq = (*p_hash1 == *p_hash2);
  return eq;
}

__PUK_SYM_INTERNAL
void nm_mpi_datatype_hashtable_insert(nm_mpi_datatype_t*p_datatype)
{
  nm_mpi_datatype_hash_t hash = p_datatype->hash;
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  if(!nm_mpi_datatype_hash_hashtable_probe(&nm_mpi_datatype_exchange.hashed_datatypes, &hash))
    {
      nm_mpi_datatype_hash_hashtable_insert(&nm_mpi_datatype_exchange.hashed_datatypes, &p_datatype->hash, p_datatype);
      nm_mpi_datatype_properties_compute(p_datatype);
    }
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
}

static inline void nm_mpi_datatype_hashtable_remove_hash(nm_mpi_datatype_hash_t hash)
{
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  struct nm_mpi_datatype_s*p_datatype = nm_mpi_datatype_hash_hashtable_lookup(&nm_mpi_datatype_exchange.hashed_datatypes, &hash);
  nm_mpi_datatype_hash_hashtable_remove(&nm_mpi_datatype_exchange.hashed_datatypes, &hash);
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
  if(p_datatype->id >= _NM_MPI_DATATYPE_OFFSET)
    nm_mpi_datatype_ref_dec(p_datatype, NULL);
}

__PUK_SYM_INTERNAL
nm_mpi_datatype_t*nm_mpi_datatype_hashtable_get(nm_mpi_datatype_hash_t hash)
{
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  struct nm_mpi_datatype_s*p_datatype = nm_mpi_datatype_hash_hashtable_lookup(&nm_mpi_datatype_exchange.hashed_datatypes, &hash);
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
  return p_datatype;
}

/* ** Request monitoring functions for datatypes *********** */

static void nm_mpi_datatype_request_monitor(nm_sr_event_t event, const nm_sr_event_info_t*info, void*ref)
{
  /* Retrieve parameters */
  assert(ref);
  nm_mpi_request_t  *p_req = (nm_mpi_request_t*)ref;
  nm_sr_request_t*p_nm_req = info->req.p_request;
  nm_tag_t tag = nm_sr_request_get_tag(p_nm_req);
  assert(tag);
  nm_session_t p_session = nm_sr_request_get_session(p_nm_req);
  assert(p_session);
  /* Deserialize the datatypes */
  void*tmp;
  const void*end = p_req->rbuf + p_req->count;
  MPI_Datatype new_datatype;
  nm_mpi_datatype_ser_t*p_ser_datatype;
  for(tmp = p_req->rbuf; tmp < end; tmp += p_ser_datatype->ser_size)
    {
      p_ser_datatype = tmp;
      nm_mpi_datatype_deserialize(p_ser_datatype, &new_datatype);
    }
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(new_datatype);
  assert(p_datatype);
  nm_mpi_datatype_exchange_insert(p_req->gate, p_datatype->id, p_datatype->hash);
  /* Send back a echo to unlock */
  tag += 1; /* Keeps the sequence number */
  nm_mpi_request_t        *p_req_resp = nm_mpi_request_alloc();
  p_req_resp->gate                    = p_req->gate;
  p_req_resp->rbuf                    = NULL;
  p_req_resp->count                   = 0;
  p_req_resp->user_tag                = tag;
  p_req_resp->p_datatype              = nm_mpi_datatype_get(MPI_BYTE);
  nm_mpi_datatype_ref_inc(p_req_resp->p_datatype, p_req_resp);
  p_req_resp->request_type            = NM_MPI_REQUEST_RECV;
  nm_sr_send_init(p_session, &p_req_resp->request_nmad);
  nm_sr_send_pack_contiguous(p_session, &p_req_resp->request_nmad, NULL, 0);
  nm_sr_request_set_ref(&p_req_resp->request_nmad, p_req_resp);
  nm_sr_request_monitor(p_session, &p_req_resp->request_nmad, NM_SR_EVENT_FINALIZED,
                        &nm_mpi_datatype_request_free);
  nm_sr_send_isend(p_session, &p_req_resp->request_nmad, p_req_resp->gate, tag);
  FREE_AND_SET_NULL(p_req->rbuf);
  nm_mpi_request_free(p_req);
}

static void nm_mpi_datatype_request_recv(nm_sr_event_t event, const nm_sr_event_info_t*info, void*ref)
{
  const nm_tag_t             tag = info->recv_unexpected.tag;
  if(NM_MPI_TAG_PRIVATE_TYPE_ADD == (tag & (NM_MPI_TAG_PRIVATE_BASE | 0xFF)))
    {
      const nm_gate_t           from = info->recv_unexpected.p_gate;
      const size_t               len = info->recv_unexpected.len;
      const nm_session_t   p_session = info->recv_unexpected.p_session;
      nm_mpi_request_t        *p_req = nm_mpi_request_alloc();
      p_req->gate                    = from;
      p_req->rbuf                    = padico_malloc(len);
      p_req->count                   = len;
      p_req->user_tag                = tag;
      p_req->p_datatype              = nm_mpi_datatype_get(MPI_BYTE);
      nm_mpi_datatype_ref_inc(p_req->p_datatype, p_req);
      p_req->request_type            = NM_MPI_REQUEST_RECV;
      nm_sr_recv_init(p_session, &p_req->request_nmad);
      nm_sr_recv_unpack_contiguous(p_session, &p_req->request_nmad, p_req->rbuf, p_req->count);
      nm_sr_request_set_ref(&p_req->request_nmad, p_req);
      nm_sr_request_monitor(p_session, &p_req->request_nmad, NM_SR_EVENT_FINALIZED,
                            &nm_mpi_datatype_request_monitor);
      nm_sr_recv_match_event(p_session, &p_req->request_nmad, info);
      nm_sr_recv_post(p_session, &p_req->request_nmad);
    }
  else
    {
      NM_MPI_FATAL_ERROR("How did you get here? You shouldn't have...\n");
    }
}

__PUK_SYM_INTERNAL
int nm_mpi_datatype_send(nm_gate_t gate, nm_mpi_datatype_t*p_datatype)
{
  assert(gate);
  assert(p_datatype);
  if(p_datatype->id < _NM_MPI_DATATYPE_OFFSET)
    {
      return MPI_SUCCESS;
    }
  struct nm_mpi_datatype_exchange_hash_key_s key = (struct nm_mpi_datatype_exchange_hash_key_s)
    { .target = gate, .datatype = p_datatype->id };
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  struct nm_mpi_datatype_exchange_hash_entry_s*p_entry =
    nm_mpi_exchanged_datatypes_hashtable_lookup(&nm_mpi_datatype_exchange.exchanged_datatypes, &key);
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
  if(p_entry && p_entry->hash == p_datatype->hash)
    {
      return MPI_SUCCESS;
    }
  nm_mpi_datatype_exchange_insert(gate, p_datatype->id, p_datatype->hash);
  static uint16_t sequence = 0;
  uint16_t seq = __sync_fetch_and_add(&sequence, 1);
  int err = MPI_SUCCESS;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(MPI_COMM_WORLD);
  nm_session_t       p_session = nm_mpi_communicator_get_session(p_comm);
  nm_sr_request_t req;
  struct nm_data_s data;
  /* Send datatypes */
  nm_data_mpi_datatype_serial_set(&data, p_datatype);
  nm_tag_t nm_tag = NM_MPI_TAG_PRIVATE_TYPE_ADD | nm_mpi_rma_seq_to_tag(seq);
  nm_sr_send_init(p_session, &req);
  nm_sr_send_pack_data(p_session, &req, &data);
  err = nm_sr_send_isend(p_session, &req, gate, nm_tag);
  nm_sr_swait(p_session, &req);
  /* Recieve echo */
  nm_tag += 1; /* Keeps the sequence number */
  nm_sr_recv_init(p_session, &req);
  nm_sr_recv_unpack_contiguous(p_session, &req, NULL, 0);
  nm_sr_recv_irecv(p_session, &req, gate, nm_tag, NM_MPI_TAG_PRIVATE_BASE | 0xFFFFFF);
  err = nm_sr_rwait(p_session, &req);
  /* Insert into exchanged hashtable */
  nm_mpi_datatype_exchange_insert(gate, p_datatype->id, p_datatype->hash);
  return NM_MPI_ERROR_INTERNAL(err);
}

static void nm_mpi_datatype_request_free(nm_sr_event_t event, const nm_sr_event_info_t*info, void*ref)
{
  if(ref)
    {
      nm_mpi_request_free(ref);
    }
}

/* ** Exchanged datatypes hashtable functions ************** */

static void nm_mpi_datatype_exchange_destructor(struct nm_mpi_datatype_exchange_hash_key_s*p_key,
                                                struct nm_mpi_datatype_exchange_hash_entry_s*p_entry)
{
  FREE_AND_SET_NULL(p_entry);
}

static uint32_t nm_mpi_datatype_exchange_hash(const struct nm_mpi_datatype_exchange_hash_key_s*p_key)
{
  uint32_t z = puk_hash_oneatatime((const void*)&p_key->target, sizeof(nm_gate_t)) +
    puk_hash_oneatatime((const void*)&p_key->datatype, sizeof(MPI_Datatype));
  return z;
}

static int nm_mpi_datatype_exchange_eq(const struct nm_mpi_datatype_exchange_hash_key_s*p_key1,
                                       const struct nm_mpi_datatype_exchange_hash_key_s*p_key2)
{
  const int eq = ((p_key1->target == p_key2->target) && (p_key1->datatype == p_key2->datatype));
  return eq;
}

static inline void nm_mpi_datatype_exchange_insert(nm_gate_t target, MPI_Datatype datatype, nm_mpi_datatype_hash_t hash)
{
  struct nm_mpi_datatype_exchange_hash_key_s key = { .target = target, .datatype = datatype };
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  struct nm_mpi_datatype_exchange_hash_entry_s*p_entry =
    nm_mpi_exchanged_datatypes_hashtable_lookup(&nm_mpi_datatype_exchange.exchanged_datatypes, &key);
  if(p_entry == NULL)
    {
      p_entry = padico_malloc(sizeof(struct nm_mpi_datatype_exchange_hash_entry_s));
      p_entry->key = key;
      p_entry->hash = hash;
      nm_mpi_exchanged_datatypes_hashtable_insert(&nm_mpi_datatype_exchange.exchanged_datatypes, &p_entry->key, p_entry);
    }
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
}

static inline void nm_mpi_datatype_exchange_remove(nm_gate_t target, MPI_Datatype datatype)
{
  struct nm_mpi_datatype_exchange_hash_key_s key = { .target = target, .datatype = datatype };
  nm_mpi_spin_lock(&nm_mpi_datatype_exchange.lock);
  struct nm_mpi_datatype_exchange_hash_entry_s*p_entry =
    nm_mpi_exchanged_datatypes_hashtable_lookup(&nm_mpi_datatype_exchange.exchanged_datatypes, &key);
  nm_mpi_exchanged_datatypes_hashtable_remove(&nm_mpi_datatype_exchange.exchanged_datatypes, &key);
  nm_mpi_spin_unlock(&nm_mpi_datatype_exchange.lock);
  FREE_AND_SET_NULL(p_entry);
}


/* ********************************************************* */

/** Apply a function to every chunk of data in datatype and to the corresponding header
 */
static void nm_mpi_datatype_wrapper_traversal_apply(const void*_content, nm_data_apply_t apply,
                                                    void*_context)
{
  const struct nm_data_mpi_datatype_wrapper_s*const p_data = _content;
  (*apply)((void*)&p_data->header, sizeof(struct nm_data_mpi_datatype_header_s), _context);
  nm_mpi_datatype_traversal_apply((void*)&p_data->data, apply, _context);
}

/** apply a serialization function to a datatype
 */
static void nm_mpi_datatype_serial_traversal_apply(const void*_content, nm_data_apply_t apply, void*_context)
{
  int i;
  const struct nm_mpi_datatype_s*const p_datatype = *(void**)_content;
  switch(p_datatype->combiner)
    {
    case MPI_COMBINER_NAMED:
      return;
    case MPI_COMBINER_CONTIGUOUS:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->CONTIGUOUS.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_DUP:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->DUP.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_RESIZED:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->RESIZED.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_VECTOR:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->VECTOR.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_HVECTOR:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->HVECTOR.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_INDEXED:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->INDEXED.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_HINDEXED:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->HINDEXED.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_INDEXED_BLOCK:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->INDEXED_BLOCK.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_HINDEXED_BLOCK:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->HINDEXED_BLOCK.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_SUBARRAY:
      {
        nm_mpi_datatype_serial_traversal_apply(&p_datatype->SUBARRAY.p_old_type, apply, _context);
      }
      break;
    case MPI_COMBINER_STRUCT:
      for(i = 0; i < p_datatype->count; ++i)
        {
          nm_mpi_datatype_serial_traversal_apply(&p_datatype->STRUCT.p_map[i].p_old_type, apply, _context);
        }
      break;
    default:
      NM_MPI_FATAL_ERROR("madmpi: cannot serialize datatype with combiner %d\n", p_datatype->combiner);
    }
  assert(p_datatype->p_serialized);
  (*apply)((void*)p_datatype->p_serialized, p_datatype->ser_size, _context);
}

/** deserialize a datatype
 */
__PUK_SYM_INTERNAL
void nm_mpi_datatype_deserialize(nm_mpi_datatype_ser_t*p_datatype, MPI_Datatype*newtype)
{
  switch(p_datatype->combiner)
    {
    case MPI_COMBINER_CONTIGUOUS:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.CONTIGUOUS.old_type);
        mpi_type_contiguous_c(p_datatype->count, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_DUP:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.DUP.old_type);
        mpi_type_dup(p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_RESIZED:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.RESIZED.old_type);
        mpi_type_create_resized_c(p_oldtype->id, p_datatype->p.RESIZED.lb, p_datatype->p.RESIZED.extent, newtype);
      }
      break;
    case MPI_COMBINER_VECTOR:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.VECTOR.old_type);
        mpi_type_vector_c(p_datatype->count, p_datatype->p.VECTOR.blocklength, p_datatype->p.VECTOR.stride, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_HVECTOR:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.HVECTOR.old_type);
        mpi_type_create_hvector_c(p_datatype->count, p_datatype->p.HVECTOR.blocklength, p_datatype->p.HVECTOR.hstride, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_INDEXED:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.INDEXED.old_type);
        MPI_Count*aob = (MPI_Count*)&p_datatype->p.INDEXED.DATA;
        MPI_Count*aod = aob + p_datatype->count;
        mpi_type_indexed_c(p_datatype->count, aob, aod, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_HINDEXED:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.HINDEXED.old_type);
        MPI_Count*aob = (MPI_Count*)&p_datatype->p.INDEXED.DATA;
        MPI_Count*aod = (MPI_Count*)(aob + p_datatype->count);
        mpi_type_create_hindexed_c(p_datatype->count, aob, aod, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_INDEXED_BLOCK:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.INDEXED_BLOCK.old_type);
        MPI_Count*aod = (void*)&p_datatype->p.INDEXED_BLOCK.DATA;
        mpi_type_create_indexed_block_c(p_datatype->count, p_datatype->p.INDEXED_BLOCK.blocklength, aod, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_HINDEXED_BLOCK:
      {
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.HINDEXED_BLOCK.old_type);
        MPI_Count*aod = (void*)&p_datatype->p.HINDEXED_BLOCK.DATA;
        mpi_type_create_hindexed_block_c(p_datatype->count, p_datatype->p.HINDEXED_BLOCK.blocklength, aod, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_SUBARRAY:
      {
        MPI_Count*sizes = (void*)&p_datatype->p.SUBARRAY.DATA;;
        MPI_Count*subsizes = sizes + p_datatype->p.SUBARRAY.ndims;
        MPI_Count*starts = subsizes + p_datatype->p.SUBARRAY.ndims;
        nm_mpi_datatype_t*p_oldtype = nm_mpi_datatype_hashtable_get(p_datatype->p.SUBARRAY.old_type);
        mpi_type_create_subarray_c(p_datatype->p.SUBARRAY.ndims, sizes, subsizes, starts, p_datatype->p.SUBARRAY.order, p_oldtype->id, newtype);
      }
      break;
    case MPI_COMBINER_STRUCT:
      {
        void*tmp = (void*)&p_datatype->p.STRUCT.DATA;
        nm_mpi_datatype_hash_t*old_hashes = tmp;
        MPI_Datatype aodt[p_datatype->count];
        nm_mpi_count_t i;
        nm_mpi_datatype_t*p_oldtype;
        for(i = 0; i < p_datatype->count; ++i)
          {
            p_oldtype = nm_mpi_datatype_hashtable_get(old_hashes[i]);
            aodt[i] = p_oldtype->id;
          }
        tmp += p_datatype->count * sizeof(MPI_Datatype);
        MPI_Count*aob = tmp;
        tmp += p_datatype->count * sizeof(MPI_Count);
        MPI_Count*aod = tmp;
        mpi_type_create_struct_c(p_datatype->count, aob, aod, aodt, newtype);
      }
      break;
    default:
      NM_MPI_FATAL_ERROR("madmpi: cannot deserialize datatype with combiner %d\n", p_datatype->combiner);
    }
  nm_mpi_datatype_t*p_newtype = nm_mpi_datatype_get(*newtype);
  nm_mpi_datatype_hash_t newtype_hash = p_newtype->hash;
  nm_mpi_datatype_hashtable_insert(p_newtype);
  nm_mpi_datatype_t*p_saved_type = nm_mpi_datatype_hashtable_get(newtype_hash);
  *newtype = p_saved_type->id;
  if(p_saved_type != p_newtype)
    {
      assert(p_newtype->id >= _NM_MPI_DATATYPE_OFFSET);
      nm_mpi_datatype_ref_dec(p_newtype, NULL);
    }
}
