/*
 * NewMadeleine
 * Copyright (C) 2015-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include "nm_mpi_not_implemented.h"

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ** buffered send */
NM_MPI_ALIAS(MPI_Bsend_init, mpi_bsend_init);
NM_MPI_ALIAS(MPI_Ibsend, mpi_ibsend);

/* ** graph topology */
NM_MPI_ALIAS(MPI_Cart_map, mpi_cart_map);
NM_MPI_ALIAS(MPI_Graph_create, mpi_graph_create);
NM_MPI_ALIAS(MPI_Graph_get, mpi_graph_get);
NM_MPI_ALIAS(MPI_Graph_map, mpi_graph_map);
NM_MPI_ALIAS(MPI_Graph_neighbors, mpi_graph_neighbors);
NM_MPI_ALIAS(MPI_Graph_neighbors_count, mpi_graph_neighbors_count);
NM_MPI_ALIAS(MPI_Graphdims_get, mpi_graphdims_get);
NM_MPI_ALIAS(MPI_Dist_graph_create, mpi_dist_graph_create);
NM_MPI_ALIAS(MPI_Dist_graph_create_adjacent, mpi_dist_graph_create_adjacent);
NM_MPI_ALIAS(MPI_Dist_graph_neighbors, mpi_dist_graph_neighbors);

/* ** unsupported collectives */
NM_MPI_ALIAS(MPI_Exscan, mpi_exscan);
NM_MPI_ALIAS(MPI_Alltoallw, mpi_alltoallw);

/* ** unsupported non-blocking collectives */
NM_MPI_ALIAS(MPI_Iallgatherv, mpi_iallgatherv);
NM_MPI_ALIAS(MPI_Igatherv, mpi_igatherv);
NM_MPI_ALIAS(MPI_Iscatter, mpi_iscatter);
NM_MPI_ALIAS(MPI_Iscatterv, mpi_iscatterv);
NM_MPI_ALIAS(MPI_Iscan, mpi_iscan);
NM_MPI_ALIAS(MPI_Iexscan, mpi_iexscan);
NM_MPI_ALIAS(MPI_Ialltoallw, mpi_ialltoallw);
NM_MPI_ALIAS(MPI_Ireduce_scatter, mpi_ireduce_scatter);
NM_MPI_ALIAS(MPI_Ireduce_scatter_block, mpi_ireduce_scatter_block);

/* ** persistent collectives */
NM_MPI_ALIAS(MPI_Scatter_init,        mpi_scatter_init);
NM_MPI_ALIAS(MPI_Scatterv_init,       mpi_scatterv_init);
NM_MPI_ALIAS(MPI_Gatherv_init,        mpi_gatherv_init);
NM_MPI_ALIAS(MPI_Reduce_scatter_init, mpi_reduce_scatter_init);
NM_MPI_ALIAS(MPI_Allgatherv_init,     mpi_allgatherv_init);
NM_MPI_ALIAS(MPI_Alltoallw_init,      mpi_alltoallw_init);

/* ** Neighborhood collectives */
NM_MPI_ALIAS(MPI_Neighbor_allgather,   mpi_neighbor_allgather);
NM_MPI_ALIAS(MPI_Neighbor_allgatherv,  mpi_neighbor_allgatherv);
NM_MPI_ALIAS(MPI_Neighbor_alltoall,    mpi_neighbor_alltoall);
NM_MPI_ALIAS(MPI_Neighbor_alltoallv,   mpi_neighbor_alltoallv);
NM_MPI_ALIAS(MPI_Neighbor_alltoallw,   mpi_neighbor_alltoallw);

NM_MPI_ALIAS(MPI_Ineighbor_alltoall,   mpi_ineighbor_alltoall);
NM_MPI_ALIAS(MPI_Ineighbor_alltoallv,  mpi_ineighbor_alltoallv);
NM_MPI_ALIAS(MPI_Ineighbor_alltoallw,  mpi_ineighbor_alltoallw);
NM_MPI_ALIAS(MPI_Ineighbor_allgather,  mpi_ineighbor_allgather);
NM_MPI_ALIAS(MPI_Ineighbor_allgatherv, mpi_ineighbor_allgatherv);

NM_MPI_ALIAS(MPI_Comm_spawn, mpi_comm_spawn);


int mpi_bsend_init(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}

int mpi_cart_map(MPI_Comm comm, int ndims, const int dims[], const int periods[], int*newrank)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}

int mpi_graph_create(MPI_Comm comm_old, int nnodes, const int indx[],const int edges[], int reorder, MPI_Comm*comm_graph)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_graph_get(MPI_Comm comm, int maxindex, int maxedges, int indx[], int edges[])
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_graph_map(MPI_Comm comm_old, int nnodes, const int*index, const int*edges, int*newrank)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_graph_neighbors(MPI_Comm comm, int rank, int maxneighbors, int neighbors[])
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_graph_neighbors_count(MPI_Comm comm, int rank, int *nneighbors)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_graphdims_get(MPI_Comm comm, int*nnodes, int*nedges)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}
int mpi_dist_graph_create(MPI_Comm comm_old, int n, const int sources[], const int degrees[], const int destinations[],
                          const int weights[], MPI_Info info, int reorder, MPI_Comm *comm_dist_graph)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_dist_graph_create_adjacent(MPI_Comm comm_old, int indegree, const int sources[], const int sourceweights[],
                                   int outdegree, const int destinations[], const int destweights[], MPI_Info info,
                                   int reorder, MPI_Comm *comm_dist_graph)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_dist_graph_neighbors(MPI_Comm comm,
                             int maxindegree, int sources[], int sourceweights[],
                             int maxoutdegree, int destinations[], int destweights[])
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}

int mpi_ibsend(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("ERROR: function not implemented\n");
  return -1;
}

int mpi_iallgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, const int recvcounts[],
                    const int displs[], MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_igatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                 void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                 int root, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_iscatter(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, int root,
                 MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_iscatterv(const void*sendbuf, const int sendcounts[], const int displs[],
                  MPI_Datatype sendtype, void*recvbuf, int recvcount,
                  MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_iscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
              MPI_Op op, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_exscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
               MPI_Op op, MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_iexscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
                MPI_Op op, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_alltoallw(const void*sendbuf, const int sendcounts[],
                   const int sdispls[], const MPI_Datatype sendtypes[],
                   void *recvbuf, const int recvcounts[], const int rdispls[],
                   const MPI_Datatype recvtypes[], MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_ialltoallw(const void*sendbuf, const int sendcounts[],
                   const int sdispls[], const MPI_Datatype sendtypes[],
                   void *recvbuf, const int recvcounts[], const int rdispls[],
                   const MPI_Datatype recvtypes[], MPI_Comm comm,
                   MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_ireduce_scatter(const void*sendbuf, void*recvbuf, const int recvcounts[],
                        MPI_Datatype datatype, MPI_Op op, MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_ireduce_scatter_block(const void*sendbuf, void*recvbuf, int recvcount,
                              MPI_Datatype datatype, MPI_Op op, MPI_Comm comm,
                              MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}


int mpi_scatter_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                     void*recvbuf, int recvcount, MPI_Datatype recvtype, int root,
                     MPI_Comm comm, MPI_Info info, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_scatterv_init(const void*sendbuf, const int sendcounts[], const int displs[], MPI_Datatype sendtype,
                      void*recvbuf, int recvcount, MPI_Datatype recvtype,
                      int root, MPI_Comm comm, MPI_Info info, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_gatherv_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                     void*recvbuf, const int recvcounts[], const int displs[],
                     MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Info info,
                     MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_reduce_scatter_init(const void*sendbuf, void*recvbuf,
                            const int recvcounts[], MPI_Datatype datatype, MPI_Op op,
                            MPI_Comm comm, MPI_Info info, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_allgatherv_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                        void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                        MPI_Comm comm, MPI_Info info, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_alltoallw_init(const void*sendbuf, const int sendcounts[], const int sdispls[], const MPI_Datatype sendtypes[],
                       void*recvbuf, const int recvcounts[], const int rdispls[], const MPI_Datatype recvtypes[],
                       MPI_Comm comm, MPI_Info info, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}



int mpi_neighbor_alltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                           void*recvbuf, int recvcount, MPI_Datatype recvtype,
                           MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_neighbor_alltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                            void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype,
                            MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_neighbor_alltoallw(const void*sendbuf, const int sendcounts[], const MPI_Aint sdispls[], const MPI_Datatype sendtypes[],
                            void*recvbuf, const int recvcounts[], const MPI_Aint rdispls[], const MPI_Datatype recvtypes[],
                            MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_neighbor_allgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                            void*recvbuf, int recvcount, MPI_Datatype recvtype,
                            MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_neighbor_allgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                             void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                             MPI_Comm comm)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}


int mpi_ineighbor_alltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                           void*recvbuf, int recvcount, MPI_Datatype recvtype,
                           MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_ineighbor_alltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                            void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype,
                            MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_ineighbor_alltoallw(const void*sendbuf, const int sendcounts[], const MPI_Aint sdispls[], const MPI_Datatype sendtypes[],
                            void*recvbuf, const int recvcounts[], const MPI_Aint rdispls[], const MPI_Datatype recvtypes[],
                            MPI_Comm comm, MPI_Request *request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}

int mpi_ineighbor_allgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                            void*recvbuf, int recvcount, MPI_Datatype recvtype,
                            MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
int mpi_ineighbor_allgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                             void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                             MPI_Comm comm, MPI_Request*request)
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}



int mpi_comm_spawn(const char*command, char*argv[], int maxprocs, MPI_Info info,
                   int root, MPI_Comm comm, MPI_Comm *intercomm, int array_of_errcodes[])
{
  NM_MPI_FATAL_ERROR("Function %s not implemented.\n", __func__);
  return -1;
}
