/*
 * NewMadeleine
 * Copyright (C) 2012-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi.h"
#include <stddef.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

#ifndef NMAD_FORTRAN_TARGET_NONE

MPI_Fint*MPI_F_STATUS_IGNORE = NULL;
MPI_Fint*MPI_F_STATUSES_IGNORE = NULL;

void*NM_MPI_F_MPI_BOTTOM = NULL;
void*NM_MPI_F_MPI_IN_PLACE = NULL;
void*NM_MPI_F_MPI_UNWEIGHTED = NULL;
void*NM_MPI_F_MPI_WEIGHTS_EMPTY = NULL;
void*NM_MPI_F_MPI_ERRCODES_IGNORE = NULL;
void*NM_MPI_F_MPI_ARGV_NULL = NULL;
void*NM_MPI_F_MPI_ARGVS_NULL = NULL;


/** This function is called in nm_mpi_initf(Fortran code) to get values of Fortran constants */
void nm_mpi_initc_(void*bottom, void*inplace, void*unweighted, void*weights_empty,
                   void*errcodes_ignore, void*statuses_ignore, void*status_ignore,
                   void*argvs_null, void*argv_null)
{
  MPI_F_STATUS_IGNORE          = (MPI_Fint *)status_ignore;
  MPI_F_STATUSES_IGNORE        = (MPI_Fint *)statuses_ignore;
  NM_MPI_F_MPI_BOTTOM          = bottom;
  NM_MPI_F_MPI_IN_PLACE        = inplace;
  NM_MPI_F_MPI_UNWEIGHTED      = unweighted;
  NM_MPI_F_MPI_WEIGHTS_EMPTY   = weights_empty;
  NM_MPI_F_MPI_ERRCODES_IGNORE = errcodes_ignore;
  NM_MPI_F_MPI_ARGVS_NULL      = argvs_null;
  NM_MPI_F_MPI_ARGV_NULL       = argv_null;
}

#endif /* NMAD_FORTRAN_TARGET_NONE */
