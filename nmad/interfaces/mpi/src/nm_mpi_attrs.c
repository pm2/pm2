/*
 * NewMadeleine
 * Copyright (C) 2014-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include <Padico/Module.h>

PADICO_MODULE_HOOK(MadMPI);

NM_MPI_HANDLE_TYPE(keyval, struct nm_mpi_keyval_s, _NM_MPI_ATTR_OFFSET, 16);

static struct
{
  int refcount;
  struct nm_mpi_handle_keyval_s keyvals;
} nm_mpi_attr = { .refcount = 0, .keyvals = NM_MPI_HANDLE_NULL };

static void nm_mpi_keyval_destroy(struct nm_mpi_keyval_s*p_keyval);

/* ********************************************************* */

/* ********************************************************* */

__PUK_SYM_INTERNAL
void nm_mpi_attrs_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_attr.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      nm_mpi_handle_keyval_init(&nm_mpi_attr.keyvals);
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_attrs_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_attr.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      nm_mpi_handle_keyval_finalize(&nm_mpi_attr.keyvals, &nm_mpi_keyval_destroy);
    }
}

static void nm_mpi_keyval_destroy(struct nm_mpi_keyval_s*p_keyval)
{
  if(p_keyval != NULL)
    {
      nm_mpi_handle_keyval_free(&nm_mpi_attr.keyvals, p_keyval);
    }
}

static void nm_mpi_keyval_ref_dec(struct nm_mpi_keyval_s**p_keyval, const void*p_holder)
{
  const int count = nm_mpi_refcount_dec(&(*p_keyval)->refcount, p_holder);
  if(count == 0)
    {
      nm_mpi_handle_keyval_free(&nm_mpi_attr.keyvals, *p_keyval);
      *p_keyval = NULL;
    }
}

static inline void nm_mpi_attr_remove(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval)
{
  nm_mpi_attr_hashtable_remove(&p_attrs->table, p_keyval);
  nm_mpi_keyval_ref_dec(&p_keyval, p_attrs);
}

/* ********************************************************* */

__PUK_SYM_INTERNAL
struct nm_mpi_keyval_s*nm_mpi_keyval_get(int id)
{
  assert(nm_mpi_handle_keyval_initialized(&nm_mpi_attr.keyvals));
  struct nm_mpi_keyval_s*p_keyval = nm_mpi_handle_keyval_get(&nm_mpi_attr.keyvals, id);
  return p_keyval;
}

/** allocate an empty keyval */
__PUK_SYM_INTERNAL
struct nm_mpi_keyval_s*nm_mpi_keyval_new(void)
{
  nm_mpi_attrs_lazy_init();
  struct nm_mpi_keyval_s*p_keyval = nm_mpi_handle_keyval_alloc(&nm_mpi_attr.keyvals);
  p_keyval->copy_fn = NULL;;
  p_keyval->delete_fn = NULL;
  p_keyval->copy_subroutine = NULL;
  p_keyval->delete_subroutine = NULL;
  p_keyval->extra_state = NULL;
  nm_mpi_refcount_init(&p_keyval->refcount, "keyval", p_keyval->id);
  return p_keyval;
}

__PUK_SYM_INTERNAL
void nm_mpi_keyval_delete(struct nm_mpi_keyval_s*p_keyval)
{
  nm_mpi_keyval_ref_dec(&p_keyval, NULL);
  nm_mpi_attrs_lazy_exit();
}

__PUK_SYM_INTERNAL
void nm_mpi_attr_get(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval, void**p_attr_value, int*flag)
{
  if((p_attrs != NULL) && nm_mpi_attr_hashtable_probe(&p_attrs->table, p_keyval))
    {
      *p_attr_value = nm_mpi_attr_hashtable_lookup(&p_attrs->table, p_keyval);
      *flag = 1;
    }
  else
    {
      *flag = 0;
    }
}

__PUK_SYM_INTERNAL
int nm_mpi_attr_put(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval, void*attr_value)
{
  if(nm_mpi_attr_hashtable_probe(&p_attrs->table, p_keyval))
    {
      int err = nm_mpi_attr_delete(p_attrs, p_keyval);
      if(err != MPI_SUCCESS)
        return err;
    }
  nm_mpi_refcount_inc(&p_keyval->refcount, p_attrs);
  nm_mpi_attr_hashtable_insert(&p_attrs->table, p_keyval, attr_value);
  return MPI_SUCCESS;
}

__PUK_SYM_INTERNAL
void nm_mpi_attrs_create(struct nm_mpi_attrs_s*p_attrs, int id)
{
  nm_mpi_attr_hashtable_init(&p_attrs->table);
  p_attrs->id = id;
}

/** fill the new attributes table with content from old attrs */
__PUK_SYM_INTERNAL
int nm_mpi_attrs_copy(struct nm_mpi_attrs_s*p_old_attrs, struct nm_mpi_attrs_s*p_new_attrs)
{
  nm_mpi_attr_hashtable_enumerator_t e = nm_mpi_attr_hashtable_enumerator_new(&p_old_attrs->table);
  struct nm_mpi_keyval_s*p_keyval = NULL;
  void*p_value = NULL;
  do
    {
      nm_mpi_attr_hashtable_enumerator_next2(e, &p_keyval, &p_value);
      if(p_keyval != NULL)
        {
          int flag = 0;
          void*out = NULL;
          int err = MPI_SUCCESS;
          if((p_keyval->copy_fn == MPI_NULL_COPY_FN) && (p_keyval->copy_subroutine == NULL))
            {
              err = MPI_SUCCESS;
              flag = 0;
            }
          else if((p_keyval->copy_fn == MPI_DUP_FN) || (p_keyval->copy_subroutine == (nm_mpi_copy_subroutine_t*)MPI_DUP_FN))
            {
              err = MPI_SUCCESS;
              out = p_value;
              flag = 1;
            }
          else if(p_keyval->copy_fn != NULL)
            {
              err = (*p_keyval->copy_fn)(p_old_attrs->id, p_keyval->id, p_keyval->extra_state, p_value, &out, &flag);
            }
          else if(p_keyval->copy_subroutine != NULL)
            {
              (*p_keyval->copy_subroutine)(&p_old_attrs->id, &p_keyval->id, p_keyval->extra_state, &p_value, &out, &flag, &err);
            }
          if(err != MPI_SUCCESS)
            {
              NM_MPI_WARNING("copy_fn returned and error while copying attribute.\n");
              return err;
            }
          if(flag)
            {
              nm_mpi_attr_put(p_new_attrs, p_keyval, out);
            }
        }
    }
  while(p_keyval != NULL);
  nm_mpi_attr_hashtable_enumerator_delete(e);
  return MPI_SUCCESS;
}

/** destructor for hashtable of attributes */
__PUK_SYM_INTERNAL
void nm_mpi_attr_destructor(struct nm_mpi_keyval_s*p_keyval, void*p_value)
{
  NM_MPI_FATAL_ERROR("attr destructor called by hashtable destructor, even though hashtable was emptied before being freed. We shouldn't be here.");
}

/** destroy an attributes table */
__PUK_SYM_INTERNAL
void nm_mpi_attrs_destroy(struct nm_mpi_attrs_s*p_old_attrs)
{
  assert(p_old_attrs->id >= 0);
  while(nm_mpi_attr_hashtable_size(&p_old_attrs->table) > 0)
    {
      /* init a new enumerator on each round, since
       * nm_mpi_attr_delete() modifies the table. */
      struct nm_mpi_attr_hashtable_enumerator_s e;
      nm_mpi_attr_hashtable_enumerator_init(&e, &p_old_attrs->table);
      struct nm_mpi_keyval_s*p_keyval = nm_mpi_attr_hashtable_enumerator_next_key(&e);
      nm_mpi_attr_hashtable_enumerator_destroy(&e);
      nm_mpi_attr_delete(p_old_attrs, p_keyval);
    }
  p_old_attrs->id = -1;
  nm_mpi_attr_hashtable_destroy(&p_old_attrs->table);
}

/** delete one attribute from table */
__PUK_SYM_INTERNAL
int nm_mpi_attr_delete(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval)
{
  int err = MPI_SUCCESS;
  if(p_attrs != NULL && nm_mpi_attr_hashtable_probe(&p_attrs->table, p_keyval))
    {
      void*v = nm_mpi_attr_hashtable_lookup(&p_attrs->table, p_keyval);
      if(p_keyval->delete_fn != NULL)
        {
          err = (p_keyval->delete_fn)(p_attrs->id, p_keyval->id, v, p_keyval->extra_state);
        }
      else if(p_keyval->delete_subroutine != NULL)
        {
          (p_keyval->delete_subroutine)(&p_attrs->id, &p_keyval->id, &v, p_keyval->extra_state, &err);
        }
      if(err == MPI_SUCCESS)
        {
          nm_mpi_attr_remove(p_attrs, p_keyval);
        }
      else
        {
          NM_MPI_WARNING("delete_fn returned an error while deleting attribute.\n");
        }
    }
  return err;
}
