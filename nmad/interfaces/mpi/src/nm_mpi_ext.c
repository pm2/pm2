/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include "mpi-ext.h"

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPIX_Query_cuda_support, mpix_query_cuda_support);
NM_MPI_ALIAS(MPIX_Query_rocm_support, mpix_query_rocm_support);
NM_MPI_ALIAS(MPIX_GPU_query_support,  mpix_gpu_query_support);

/* ********************************************************* */

int mpix_query_cuda_support(void)
{
#ifdef NMAD_CUDA
  if(nm_core_get_singleton()->enable_gpu_support)
    {
      return 1;
    }
  else
    {
      padico_warning("CUDA support is built into MadMPI but NMAD_GPU_SUPPORT is not set.\n");
      return 0;
    }
#else /* NMAD_CUDA */
  padico_out(puk_verbose_notice, "MPIX_Query_cuda_support() called, but MadMPI is built without CUDA support.\n");
  return 0;
#endif /* NMAD_CUDA */
}

int mpix_query_rocm_support(void)
{
#ifdef NMAD_HIP
  if(nm_core_get_singleton()->enable_gpu_support)
    {
      return 1;
    }
  else
    {
      padico_warning("HIP support is built into MadMPI but NMAD_GPU_SUPPORT is not set.\n");
      return 0;
    }
#else /* NMAD_HIP */
  padico_out(puk_verbose_notice, "MPIX_Query_rocm_support() called, but MadMPI is built without HIP support.\n");
  return 0;
#endif /* NMAD_HIP */
}

int mpix_gpu_query_support(int gpu_type, int*is_supported)
{
  switch(gpu_type)
    {
    case MPIX_GPU_SUPPORT_CUDA:
#ifdef NMAD_CUDA
      if(nm_core_get_singleton()->enable_gpu_support)
        {
          is_supported = 1;
        }
      else
        {
          padico_warning("CUDA support is built into MadMPI but NMAD_GPU_SUPPORT is not set.\n");
          is_supported = 0;
        }
#else /* NMAD_CUDA */
      padico_out(puk_verbose_notice, "MPIX_GPU_query_support() called, but MadMPI is built without CUDA support.\n");
      is_supported = 0;
#endif /* NMAD_CUDA */
      break;
    case MPIX_GPU_SUPPORT_HIP:
#ifdef NMAD_HIP
      if(nm_core_get_singleton()->enable_gpu_support)
        {
          is_supported = 1;
        }
      else
        {
          padico_warning("HIP support is built into MadMPI but NMAD_GPU_SUPPORT is not set.\n");
          is_supported = 0;
        }
#else /* NMAD_HIP */
      padico_out(puk_verbose_notice, "MPIX_GPU_query_support() called, but MadMPI is built without HIP support.\n");
      is_supported = 0;
#endif /* NMAD_HIP */
      break;
    case MPIX_GPU_SUPPORT_ZE:
      is_supported = 0;
      break;
    default:
      return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
      break;
    }
  return MPI_SUCCESS;
}
