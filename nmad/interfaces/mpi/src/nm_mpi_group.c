/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

NM_MPI_HANDLE_TYPE(group, nm_mpi_group_t, _NM_MPI_GROUP_OFFSET, 16);

static struct
{
  int refcount;
  struct nm_mpi_handle_group_s groups;
} nm_mpi_group = { .groups = NM_MPI_HANDLE_NULL, .refcount = 0 };

static void nm_mpi_group_destroy(nm_mpi_group_t*p_group);

/* ********************************************************* */

NM_MPI_ALIAS(MPI_Group_size,            mpi_group_size);
NM_MPI_ALIAS(MPI_Group_rank,            mpi_group_rank);
NM_MPI_ALIAS(MPI_Group_union,           mpi_group_union);
NM_MPI_ALIAS(MPI_Group_intersection,    mpi_group_intersection);
NM_MPI_ALIAS(MPI_Group_difference,      mpi_group_difference);
NM_MPI_ALIAS(MPI_Group_compare,         mpi_group_compare);
NM_MPI_ALIAS(MPI_Group_incl,            mpi_group_incl);
NM_MPI_ALIAS(MPI_Group_range_incl,      mpi_group_range_incl);
NM_MPI_ALIAS(MPI_Group_excl,            mpi_group_excl);
NM_MPI_ALIAS(MPI_Group_range_excl,      mpi_group_range_excl);
NM_MPI_ALIAS(MPI_Group_free,            mpi_group_free);
NM_MPI_ALIAS(MPI_Group_translate_ranks, mpi_group_translate_ranks);

/* ********************************************************* */

__PUK_SYM_INTERNAL
void nm_mpi_group_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_group.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      nm_mpi_handle_group_init(&nm_mpi_group.groups);
      /* built-in group */
      nm_mpi_group_t*p_group_empty = nm_mpi_handle_group_store(&nm_mpi_group.groups, MPI_GROUP_EMPTY);
      p_group_empty->p_nm_group = nm_gate_vect_new();
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_group_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_group.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      nm_mpi_handle_group_finalize(&nm_mpi_group.groups, &nm_mpi_group_destroy);
    }
}

__PUK_SYM_INTERNAL
nm_mpi_group_t*nm_mpi_group_get(MPI_Group group)
{
  nm_mpi_group_t*p_group = nm_mpi_handle_group_get(&nm_mpi_group.groups, group);
  return p_group;
}

__PUK_SYM_INTERNAL
nm_mpi_group_t*nm_mpi_group_alloc(void)
{
  nm_mpi_group_lazy_init();
  nm_mpi_group_t*p_group = nm_mpi_handle_group_alloc(&nm_mpi_group.groups);
  p_group->p_nm_group = NULL;
  return p_group;
}

static void nm_mpi_group_destroy(nm_mpi_group_t*p_group)
{
  if(p_group != NULL)
    {
      const int id = p_group->id;
      nm_group_free(p_group->p_nm_group);
      nm_mpi_handle_group_free(&nm_mpi_group.groups, p_group);
      if(id != MPI_GROUP_EMPTY)
        nm_mpi_group_lazy_exit();
    }
}

/* ********************************************************* */

int mpi_group_size(MPI_Group group, int*size)
{
  nm_mpi_group_t*p_group = nm_mpi_group_get(group);
  NM_MPI_GROUP_CHECK(p_group);
  *size = nm_group_size(p_group->p_nm_group);
  return MPI_SUCCESS;
}

int mpi_group_rank(MPI_Group group, int*rank)
{
  nm_mpi_group_t*p_group = nm_mpi_group_get(group);
  NM_MPI_GROUP_CHECK(p_group);
  const int r = nm_group_rank(p_group->p_nm_group);
  if(r == -1)
    {
      *rank = MPI_UNDEFINED;
    }
  else
    {
      *rank = r;
    }
  return MPI_SUCCESS;
}

int mpi_group_union(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup)
{
  nm_mpi_group_t*p_group1 = nm_mpi_group_get(group1);
  nm_mpi_group_t*p_group2 = nm_mpi_group_get(group2);
  NM_MPI_GROUP_CHECK(p_group1);
  NM_MPI_GROUP_CHECK(p_group2);
  nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
  p_new_group->p_nm_group =  nm_group_union(p_group1->p_nm_group, p_group2->p_nm_group);
  MPI_Group new_id = p_new_group->id;
  *newgroup = new_id;
  return MPI_SUCCESS;
}

int mpi_group_intersection(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup)
{
  nm_mpi_group_t*p_group1 = nm_mpi_group_get(group1);
  nm_mpi_group_t*p_group2 = nm_mpi_group_get(group2);
  NM_MPI_GROUP_CHECK(p_group1);
  NM_MPI_GROUP_CHECK(p_group2);
  nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
  p_new_group->p_nm_group =  nm_group_intersection(p_group1->p_nm_group, p_group2->p_nm_group);
  MPI_Group new_id = p_new_group->id;;
  *newgroup = new_id;
  return MPI_SUCCESS;
}

int mpi_group_difference(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup)
{
  nm_mpi_group_t*p_group1 = nm_mpi_group_get(group1);
  nm_mpi_group_t*p_group2 = nm_mpi_group_get(group2);
  NM_MPI_GROUP_CHECK(p_group1);
  NM_MPI_GROUP_CHECK(p_group2);
  nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
  p_new_group->p_nm_group = nm_group_difference(p_group1->p_nm_group, p_group2->p_nm_group);
  MPI_Group new_id = p_new_group->id;
  *newgroup = new_id;
  return MPI_SUCCESS;
}

int mpi_group_compare(MPI_Group group1, MPI_Group group2, int*result)
{
  nm_mpi_group_t*p_group1 = nm_mpi_group_get(group1);
  nm_mpi_group_t*p_group2 = nm_mpi_group_get(group2);
  NM_MPI_GROUP_CHECK(p_group1);
  NM_MPI_GROUP_CHECK(p_group2);
  int r = nm_group_compare(p_group1->p_nm_group, p_group2->p_nm_group);
  if(r == NM_GROUP_IDENT)
    *result = MPI_IDENT;
  else if(r == NM_GROUP_SIMILAR)
    *result = MPI_SIMILAR;
  else
    *result = MPI_UNEQUAL;
  return MPI_SUCCESS;
}

int mpi_group_incl(MPI_Group group, int n, const int*ranks, MPI_Group*newgroup)
{
  if((n == 0) || (group == MPI_GROUP_EMPTY))
    {
      *newgroup = MPI_GROUP_EMPTY;
    }
  else
    {
      nm_mpi_group_t*p_group = nm_mpi_group_get(group);
      NM_MPI_GROUP_CHECK(p_group);
      nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
      p_new_group->p_nm_group = nm_group_incl(p_group->p_nm_group, n, ranks);
      MPI_Group new_id = p_new_group->id;
      *newgroup = new_id;
    }
  return MPI_SUCCESS;
}

int mpi_group_range_incl(MPI_Group group, int n, int ranges[][3], MPI_Group*newgroup)
{
  nm_mpi_group_t*p_group = nm_mpi_group_get(group);
  NM_MPI_GROUP_CHECK(p_group);
  int size = nm_group_size(p_group->p_nm_group);
  int*ranks = padico_malloc(sizeof(int) * size);
  int k = 0;
  int i;
  for(i = 0; i < n; i++)
    {
      const int first  = ranges[i][0];
      const int last   = ranges[i][1];
      const int stride = ranges[i][2];
      if(stride == 0)
        {
          return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
        }
      const int e = (last - first) / stride;
      int j;
      for(j = 0; j <= e; j++)
        {
          const int r = first + j * stride;
          if(r >= 0 && r < size)
            {
              ranks[k] = r;
              k++;
            }
        }
    }
  int err = mpi_group_incl(group, k, ranks, newgroup);
  padico_free(ranks);
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_group_excl(MPI_Group group, int n, const int*ranks, MPI_Group*newgroup)
{
  if((n == 0) || (group == MPI_GROUP_EMPTY))
    {
      *newgroup = MPI_GROUP_EMPTY;
    }
  else
    {
      nm_mpi_group_t*p_group = nm_mpi_group_get(group);
      NM_MPI_GROUP_CHECK(p_group);
      nm_group_t p_nm_group = nm_group_excl(p_group->p_nm_group, n, ranks);
      if(nm_group_size(p_nm_group) == 0)
        {
          *newgroup = MPI_GROUP_EMPTY;
        }
      else
        {
          nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
          p_new_group->p_nm_group = p_nm_group;
          MPI_Group new_id = p_new_group->id;
          *newgroup = new_id;
        }
    }
  return MPI_SUCCESS;
}

int mpi_group_range_excl(MPI_Group group, int n, int ranges[][3], MPI_Group*newgroup)
{
  nm_mpi_group_t*p_group = nm_mpi_group_get(group);
  NM_MPI_GROUP_CHECK(p_group);
  int size = nm_group_size(p_group->p_nm_group);
  int*ranks = padico_malloc(sizeof(int) * size);
  int k = 0;
  int i;
  for(i = 0; i < n; i++)
    {
      const int first  = ranges[i][0];
      const int last   = ranges[i][1];
      const int stride = ranges[i][2];
      if(stride == 0)
        {
          return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
        }
      const int e = (last - first) / stride;
      int j;
      for(j = 0; j <= e; j++)
        {
          const int r = first + j * stride;
          if(r >= 0 && r < size)
            {
              ranks[k] = r;
              k++;
            }
        }
    }
  int err = mpi_group_excl(group, k, ranks, newgroup);
  padico_free(ranks);
  return NM_MPI_ERROR_WORLD(err);
}


int mpi_group_free(MPI_Group*group)
{
  const int id = *group;
  if(id != MPI_GROUP_NULL && id != MPI_GROUP_EMPTY)
    {
      nm_mpi_group_t*p_group = nm_mpi_group_get(id);
      NM_MPI_GROUP_CHECK(p_group);
      nm_mpi_group_destroy(p_group);
    }
  *group = MPI_GROUP_NULL;
  return MPI_SUCCESS;
}

int mpi_group_translate_ranks(MPI_Group group1, int n, const int*ranks1, MPI_Group group2, int*ranks2)
{
  nm_mpi_group_t*p_group1 = nm_mpi_group_get(group1);
  nm_mpi_group_t*p_group2 = nm_mpi_group_get(group2);
  NM_MPI_GROUP_CHECK(p_group1);
  NM_MPI_GROUP_CHECK(p_group2);
  int err = nm_group_translate_ranks(p_group1->p_nm_group, n, ranks1, p_group2->p_nm_group, ranks2);
  if(err != 0)
    return NM_MPI_ERROR_WORLD(MPI_ERR_RANK);
  /* translate undefined ranks */
  int i;
  for(i = 0; i < n; i++)
    {
      if(ranks1[i] == MPI_PROC_NULL)
        ranks2[i] = MPI_PROC_NULL;
      else if(ranks2[i] == -1)
        ranks2[i] = MPI_UNDEFINED;
    }
  return MPI_SUCCESS;
}
