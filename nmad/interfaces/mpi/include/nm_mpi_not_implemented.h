/*
 * NewMadeleine
 * Copyright (C) 2012-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_NOT_IMPLEMENTED_H
#define NM_MPI_NOT_IMPLEMENTED_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for unimplemented functions
 * @{
 */


/** @name Functions: the implementation of these functions is missing but the prototypes are required to ensure compatibility with other tools/applications
 * @{ */

int MPI_Bsend_init(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);
int MPI_Buffer_attach(void*buffer, int size);
int MPI_Buffer_detach(void *buffer_addr, int *size);

int MPI_Cart_map(MPI_Comm comm, int ndims, const int dims[], const int periods[], int*newrank);

int MPI_Graph_create(MPI_Comm comm_old, int nnodes, const int indx[],const int edges[], int reorder, MPI_Comm*comm_graph);
int MPI_Graph_get(MPI_Comm comm, int maxindex, int maxedges, int indx[], int edges[]);
int MPI_Graph_map(MPI_Comm comm_old, int nnodes, const int*index, const int*edges, int*newrank);
int MPI_Graph_neighbors(MPI_Comm comm, int rank, int maxneighbors, int neighbors[]);
int MPI_Graph_neighbors_count(MPI_Comm comm, int rank, int *nneighbors);
int MPI_Graphdims_get(MPI_Comm comm, int*nnodes, int*nedges);
int MPI_Dist_graph_create(MPI_Comm comm_old, int n, const int sources[], const int degrees[], const int destinations[],
                          const int weights[], MPI_Info info, int reorder, MPI_Comm *comm_dist_graph);
int MPI_Dist_graph_create_adjacent(MPI_Comm comm_old, int indegree, const int sources[], const int sourceweights[],
                                   int outdegree, const int destinations[], const int destweights[], MPI_Info info,
                                   int reorder, MPI_Comm *comm_dist_graph);
int MPI_Dist_graph_neighbors(MPI_Comm comm,
                             int maxindegree, int sources[], int sourceweights[],
                             int maxoutdegree, int destinations[], int destweights[]);

int MPI_Ibsend(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Iallgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, const int recvcounts[],
                    const int displs[], MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request);

int MPI_Igatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                 void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                 int root, MPI_Comm comm, MPI_Request*request);
int MPI_Iscatter(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, int root,
                 MPI_Comm comm, MPI_Request*request);
int MPI_Iscatterv(const void*sendbuf, const int sendcounts[], const int displs[],
                  MPI_Datatype sendtype, void*recvbuf, int recvcount,
                  MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Request*request);
int MPI_Iscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
              MPI_Op op, MPI_Comm comm, MPI_Request*request);
int MPI_Exscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
               MPI_Op op, MPI_Comm comm);
int MPI_Iexscan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype,
                MPI_Op op, MPI_Comm comm, MPI_Request*request);
int MPI_Alltoallw(const void*sendbuf, const int sendcounts[],
                  const int sdispls[], const MPI_Datatype sendtypes[],
                  void *recvbuf, const int recvcounts[], const int rdispls[],
                  const MPI_Datatype recvtypes[], MPI_Comm comm);
int MPI_Ialltoallw(const void*sendbuf, const int sendcounts[],
                   const int sdispls[], const MPI_Datatype sendtypes[],
                   void *recvbuf, const int recvcounts[], const int rdispls[],
                   const MPI_Datatype recvtypes[], MPI_Comm comm,
                   MPI_Request*request);
int MPI_Ireduce_scatter(const void*sendbuf, void*recvbuf, const int recvcounts[],
                        MPI_Datatype datatype, MPI_Op op, MPI_Comm comm, MPI_Request*request);
int MPI_Ireduce_scatter_block(const void*sendbuf, void*recvbuf, int recvcount,
                              MPI_Datatype datatype, MPI_Op op, MPI_Comm comm,
                              MPI_Request*request);


int MPI_Scatter_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                     void*recvbuf, int recvcount, MPI_Datatype recvtype, int root,
                     MPI_Comm comm, MPI_Info info, MPI_Request*request);
int MPI_Scatterv_init(const void*sendbuf, const int sendcounts[], const int displs[], MPI_Datatype sendtype,
                      void*recvbuf, int recvcount, MPI_Datatype recvtype,
                      int root, MPI_Comm comm, MPI_Info info, MPI_Request*request);
int MPI_Gatherv_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                     void*recvbuf, const int recvcounts[], const int displs[],
                     MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Info info,
                     MPI_Request*request);
int MPI_Reduce_scatter_init(const void*sendbuf, void*recvbuf,
                            const int recvcounts[], MPI_Datatype datatype, MPI_Op op,
                            MPI_Comm comm, MPI_Info info, MPI_Request*request);
int MPI_Allgatherv_init(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                        void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                        MPI_Comm comm, MPI_Info info, MPI_Request*request);
int MPI_Alltoallw_init(const void*sendbuf, const int sendcounts[], const int sdispls[], const MPI_Datatype sendtypes[],
                       void*recvbuf, const int recvcounts[], const int rdispls[], const MPI_Datatype recvtypes[],
                       MPI_Comm comm, MPI_Info info, MPI_Request*request);

int MPI_Neighbor_alltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                           void*recvbuf, int recvcount, MPI_Datatype recvtype,
                           MPI_Comm comm);
int MPI_Neighbor_alltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                            void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype,
                            MPI_Comm comm);
int MPI_Neighbor_alltoallw(const void*sendbuf, const int sendcounts[], const MPI_Aint sdispls[], const MPI_Datatype sendtypes[],
                            void*recvbuf, const int recvcounts[], const MPI_Aint rdispls[], const MPI_Datatype recvtypes[],
                            MPI_Comm comm);
int MPI_Neighbor_allgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                            void*recvbuf, int recvcount, MPI_Datatype recvtype,
                            MPI_Comm comm);
int MPI_Neighbor_allgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                             void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                             MPI_Comm comm);

int MPI_Ineighbor_alltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                           void*recvbuf, int recvcount, MPI_Datatype recvtype,
                           MPI_Comm comm, MPI_Request*request);
int MPI_Ineighbor_alltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                            void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype,
                            MPI_Comm comm, MPI_Request*request);
int MPI_Ineighbor_alltoallw(const void*sendbuf, const int sendcounts[], const MPI_Aint sdispls[], const MPI_Datatype sendtypes[],
                            void*recvbuf, const int recvcounts[], const MPI_Aint rdispls[], const MPI_Datatype recvtypes[],
                            MPI_Comm comm, MPI_Request*request);
int MPI_Ineighbor_allgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                            void*recvbuf, int recvcount, MPI_Datatype recvtype,
                            MPI_Comm comm, MPI_Request*request);
int MPI_Ineighbor_allgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                             void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype,
                             MPI_Comm comm, MPI_Request*request);


int MPI_Comm_spawn(const char*command, char*argv[], int maxprocs, MPI_Info info,
                   int root, MPI_Comm comm, MPI_Comm *intercomm, int array_of_errcodes[]);

#define MPI_ARGV_NULL (NULL)

/** @}*/
/** @}*/

#endif /* NM_MPI_NOT_IMPLEMENTED_H */
