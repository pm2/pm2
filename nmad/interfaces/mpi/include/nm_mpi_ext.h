/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_EXT_H
#define NM_MPI_EXT_H

/* ** GPU aware support, OpenMPI API */

#define MPIX_CUDA_AWARE_SUPPORT 1

#define MPIX_ROCM_AWARE_SUPPORT 1

/**< returns 1 if there is CUDA aware support, 0 otherwise. */
int MPIX_Query_cuda_support(void);

/**< returns 1 if there is rocm aware support, 0 otherwise. */
int MPIX_Query_rocm_support(void);


/* ** GPU aware support, MPICH API */

/** GPU aware for nvidia CUDA */
#define MPIX_GPU_SUPPORT_CUDA 2

/** GPU aware for AMD HIP */
#define MPIX_GPU_SUPPORT_HIP  3

/** GPU aware for Intel OneAPI level Zero */
#define MPIX_GPU_SUPPORT_ZE   4

int MPIX_GPU_query_support(int gpu_type, int*is_supported);


#endif /* NM_MPI_EXT_H */
