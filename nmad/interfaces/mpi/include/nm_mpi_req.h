/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_REQ_H
#define NM_MPI_REQ_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI requests
 * @{
 */

/** @name Functions: Request-related functions
 * @{ */

/**
 * Marks for cancellation a pending, nonblocking communication
 * operation (send or receive).
 * @param request communication request handle
 * @return MPI status
 */
int MPI_Cancel(MPI_Request*request);

/**
 * Marks the request object for deallocation and set request to
 * MPI_REQUEST_NULL. An ongoing communication that is associated with
 * the request will be allowed to complete. The request will be
 * deallocated only after its completion.
 * @param request communication request handle
 * @return MPI status
 */
int MPI_Request_free(MPI_Request*request);

/**
 * Returns when the operation identified by request is complete.
 * @param request request
 * @param status status object
 * @return MPI status
 */
int MPI_Wait(MPI_Request*request, MPI_Status*status);

/**
 * Returns when all the operations identified by requests are complete.
 * @param count lists length
 * @param array_of_requests array of requests
 * @param array_of_statuses array of status objects
 * @return MPI status
 */
int MPI_Waitall(int count, MPI_Request*array_of_requests, MPI_Status*array_of_statuses);

/**
 * Blocks until one of the operations associated with the active
 * requests in the array has completed. If more then one operation is
 * enabled and can terminate, one is arbitrarily chosen. Returns in
 * index the index of that request in the array and returns in status
 * the status of the completing communication. If the request was
 * allocated by a nonblocking communication operation, then it is
 * deallocated and the request handle is set to MPI_REQUEST_NULL.
 * @param count list length
 * @param array_of_requests array of requests
 * @param index index of handle for operation that completed
 * @param status status object
 * @return MPI status
 */
int MPI_Waitany(int count, MPI_Request*array_of_requests, int*index, MPI_Status*status);

int MPI_Waitsome(int incount, MPI_Request*array_of_requests, int*outcount, int*array_of_indices, MPI_Status*array_of_statuses);

/**
 * Returns flag = true if the operation identified by request is
 * complete.
 * @param request communication request handle
 * @param flag true if operation completed
 * @param status status object
 * @return MPI status
 */
int MPI_Test(MPI_Request*request, int *flag, MPI_Status*status);

/**
 * Tests for completion of one communication operation associated
 * with requests in the array.
 * @param count list length
 * @param array_of_requests array of request handles
 * @param index index of request handle that completed
 * @param flag true if one has completed
 * @param status status object
 * @return MPI status
 */
int MPI_Testany(int count, MPI_Request*array_of_requests, int*index, int*flag, MPI_Status*status);

/**
 * Tests for completion of several communication operations associated
 * with requests in the array.
 * @param count list length
 * @param array_of_requests array of request handles
 * @param outcount is the number of request handle completed
 * @param indices are the indices or request handle completed in array_of_requests
 * @param statuses array of statuses of the completed operations
 * @return MPI status
 */
int MPI_Testsome(int count, MPI_Request*array_of_requests, int*outcount, int*indices, MPI_Status*statuses);
/**
 * Tests for the completion of all previously initiated requests.
 * @param count list length
 * @param array_of_requests array of request handles
 * @param flag  True if all requests have completed; false otherwise (logical)
 * @param statuses array of statuses of the completed operations
 * @return MPI status
 */
int MPI_Testall(int count, MPI_Request*array_of_requests, int*flag, MPI_Status*statuses);

int MPI_Test_cancelled(const MPI_Status*status, int*flag);

/* @} */

/** @name Functions: Persistent communications */
/* @{ */

/**
 * Creates a persistent communication request for a standard mode send
 * operation, and binds to it all the arguments of a send operation.
 * @param buf initial address of send buffer
 * @param count number of elements sent
 * @param datatype type of each element
 * @param dest rank of destination
 * @param tag message tag
 * @param comm communicator
 * @param request communication request handle
 * @return MPI status
 */
int MPI_Send_init(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Send_init_c(const void*buf, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Rsend_init(const void*buf, int count, MPI_Datatype datatype,
                   int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Rsend_init_c(const void*buf, MPI_Count count, MPI_Datatype datatype,
                     int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Ssend_init(const void*buf, int count, MPI_Datatype datatype,
                   int dest, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Ssend_init_c(const void*buf, MPI_Count count, MPI_Datatype datatype,
                     int dest, int tag, MPI_Comm comm, MPI_Request*request);

/**
 * Creates a persistent communication request for a receive operation.
 * @param buf initial address of receive buffer
 * @param count number of elements received
 * @param datatype type of each element
 * @param source rank of source or MPI_ANY_SOURCE
 * @param tag message tag or MPI_ANY_TAG
 * @param comm communicator
 * @param request communication request handle
 * @return MPI status
 */
int MPI_Recv_init(void*buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Recv_init_c(void*buf, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request);

/**
 * Initiates a persistent request. The associated request should be
 * inactive. The request becomes active once the call is made. If the
 * request is for a send with ready mode, then a matching receive
 * should be posted before the call is made. The communication buffer
 * should not be accessed after the call, and until the operation
 * completes.
 * @param request communication request handle
 * @return MPI status
 */
int MPI_Start(MPI_Request*request);

/**
 * Start all communications associated with requests in
 * array_of_requests.
 * @param count list length
 * @param array_of_requests array of requests
 * @return MPI status
 */
int MPI_Startall(int count, MPI_Request*array_of_requests);

int MPI_Grequest_start(MPI_Grequest_query_function*query_fn,
                       MPI_Grequest_free_function*free_fn,
                       MPI_Grequest_cancel_function*cancel_fn, void*extra_state,
                       MPI_Request*request);

int MPI_Grequest_complete(MPI_Request request);

int MPI_Status_set_elements(MPI_Status*status, MPI_Datatype datatype, int count);
int MPI_Status_set_elements_x(MPI_Status*status, MPI_Datatype datatype, MPI_Count count);
int MPI_Status_set_elements_c(MPI_Status*status, MPI_Datatype datatype, MPI_Count count);
int MPI_Status_set_cancelled(MPI_Status*status, int flag);

/**
 * Computes the number of entries received.
 * @param status return status of receive operation
 * @param datatype datatype of each receive buffer entry
 * @param count number of received entries
 * @return MPI status
 */
int MPI_Get_count(MPI_Status*status, MPI_Datatype datatype, int*count);

int MPI_Get_count_c(MPI_Status*status, MPI_Datatype datatype, MPI_Count*count);

int MPI_Get_elements(const MPI_Status*status, MPI_Datatype datatype, int*count);

int MPI_Get_elements_c(const MPI_Status*status, MPI_Datatype datatype, MPI_Count*count);

int MPI_Get_elements_x(const MPI_Status*status, MPI_Datatype datatype, MPI_Count*count);

/** @}*/
/** @}*/


#endif /* NM_MPI_REQ_H */
