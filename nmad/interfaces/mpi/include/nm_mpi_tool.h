/*
 * NewMadeleine
 * Copyright (C) 2022-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_TOOL_H
#define NM_MPI_TOOL_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI tool
 * @{
 */

/** @name Functions: functions related to tool support
 * @{ */

int MPI_T_init_thread(int required, int*provided);

int MPI_T_finalize(void);

int MPI_T_enum_get_info(MPI_T_enum enumtype, int*num, char*name, int*name_len);

int MPI_T_enum_get_item(MPI_T_enum enumtype, int index, int*value, char*name, int*name_len);

/* ** cvar ************************************************* */

int MPI_T_cvar_get_num(int*num_cvar);

int MPI_T_cvar_get_info(int cvar_index, char*name, int*name_len,
                        int*verbosity, MPI_Datatype *datatype, MPI_T_enum*enumtype,
                        char*desc, int*desc_len, int*bind, int*scope);

int MPI_T_cvar_get_index(const char*name, int*cvar_index);

int MPI_T_cvar_handle_alloc(int cvar_index, void*obj_handle, MPI_T_cvar_handle*handle, int*count);

int MPI_T_cvar_handle_free(MPI_T_cvar_handle*handle);

int MPI_T_cvar_read(MPI_T_cvar_handle handle, void*buf);

int MPI_T_cvar_write(MPI_T_cvar_handle handle, const void*buf);


/* ** pvar ************************************************* */

int MPI_T_pvar_get_num(int*num_pvar);

int MPI_T_pvar_get_info(int pvar_index, char*name, int*name_len,
                        int*verbosity, int*var_class, MPI_Datatype*datatype,
                        MPI_T_enum*enumtype, char*desc, int*desc_len, int*bind,
                        int*readonly, int*continuous, int*atomic);

int MPI_T_pvar_get_index(const char*name, int var_class, int*pvar_index);

int MPI_T_pvar_session_create(MPI_T_pvar_session*pe_session);

int MPI_T_pvar_session_free(MPI_T_pvar_session*pe_session);

int MPI_T_pvar_handle_alloc(MPI_T_pvar_session pe_session, int pvar_index,
                            void*obj_handle, MPI_T_pvar_handle*handle, int*count);

int MPI_T_pvar_handle_free(MPI_T_pvar_session pe_session, MPI_T_pvar_handle*handle);

int MPI_T_pvar_start(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle);

int MPI_T_pvar_stop(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle);

int MPI_T_pvar_read(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle, void*buf);

int MPI_T_pvar_write(MPI_T_pvar_session pe_session,
                     MPI_T_pvar_handle handle, const void*buf);

int MPI_T_pvar_reset(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle);

int MPI_T_pvar_readreset(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle, void*buf);


/* ** categories ******************************************* */

int MPI_T_category_get_num(int*num_cat);

int MPI_T_category_get_info(int cat_index, char*name, int*name_len,
                            char*desc, int*desc_len, int*num_cvars, int*num_pvars,
                            int*num_categories);

int MPI_T_category_get_num_events(int cat_index, int*num_events);

int MPI_T_category_get_index(const char*name, int*cat_index);

int MPI_T_category_get_cvars(int cat_index, int len, int indices[]);

int MPI_T_category_get_pvars(int cat_index, int len, int indices[]);

int MPI_T_category_get_events(int cat_index, int len, int indices[]);

int MPI_T_category_get_categories(int cat_index, int len, int indices[]);

int MPI_T_category_changed(int*update_number);


/** @}*/
/** @}*/


#endif /* NM_MPI_TOOL_H */
