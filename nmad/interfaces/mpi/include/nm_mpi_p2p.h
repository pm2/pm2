/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_P2P_H
#define NM_MPI_P2P_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI point to point operations
 * @{
 */

/** @name Functions: Point-to-point communications
 * @{ */

/**
 * Performs a standard-mode, blocking send.
 * @param buffer initial address of send buffer
 * @param count number of elements in send buffer
 * @param datatype datatype of each send buffer element
 * @param dest rank of destination
 * @param tag message tag
 * @param comm communicator
 * @return MPI status
 */
int MPI_Send(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Send_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);


/**
 * Posts a standard-mode, non blocking send.
 * @param buffer initial address of send buffer
 * @param count number of elements in send buffer
 * @param datatype datatype of each send buffer element
 * @param dest rank of destination
 * @param tag message tag
 * @param comm communicator
 * @param request pointer to request
 * @return MPI status
 */
int MPI_Isend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);

int MPI_Isend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);

int MPI_Issend(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);

int MPI_Issend_c(const void*buf, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);

/**
 * Performs a ready-mode, blocking send.
 * @param buffer initial address of send buffer
 * @param count number of elements in send buffer
 * @param datatype datatype of each send buffer element
 * @param dest rank of destination
 * @param tag message tag
 * @param comm communicator
 * @return MPI status
 */
int MPI_Rsend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Rsend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

/**
 * Performs a synchronous-mode, blocking send.
 * @param buffer initial address of send buffer
 * @param count number of elements in send buffer
 * @param datatype datatype of each send buffer element
 * @param dest rank of destination
 * @param tag message tag
 * @param comm communicator
 * @return MPI status
 */
int MPI_Ssend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Ssend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Bsend(const void *buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Bsend_c(const void *buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

int MPI_Irsend(const void*buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);
int MPI_Irsend_c(const void*buf, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request);

/**
 * Performs a standard-mode, blocking receive.
 * @param buffer initial address of receive buffer
 * @param count number of elements in receive buffer
 * @param datatype datatype of each receive buffer element
 * @param source rank of source
 * @param tag message tag
 * @param comm communicator
 * @param status status object
 * @return MPI status
 */
int MPI_Recv(void*buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status*status);

int MPI_Recv_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status*status);

/**
 * Posts a nonblocking receive.
 * @param buffer initial address of receive buffer
 * @param count number of elements in receive buffer
 * @param datatype datatype of each receive buffer element
 * @param source rank of source
 * @param tag message tag
 * @param comm communicator
 * @param request pointer to request
 * @return MPI status
 */
int MPI_Irecv(void*buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request);

int MPI_Irecv_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request);


/**
 * Executes a blocking send and receive operation.
 * @param sendbuf initial address of send buffer
 * @param sendcount number of elements in send buffer
 * @param sendtype type of elements in send buffer
 * @param dest rank of destination
 * @param sendtag send tag
 * @param recvbuf initial address of receive buffer
 * @param recvcount number of elements in receive buffer
 * @param recvtype type of elements in receive buffer
 * @param source rank of source
 * @param recvtag receive tag
 * @param comm communicator
 * @param status status object
 * @return MPI status
 */
int MPI_Sendrecv(const void*sendbuf, int sendcount, MPI_Datatype sendtype, int dest, int sendtag,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, int source, int recvtag,
                 MPI_Comm comm, MPI_Status*status);

int MPI_Sendrecv_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, int dest, int sendtag,
                   void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int source, int recvtag,
                   MPI_Comm comm, MPI_Status*status);

int MPI_Sendrecv_replace(void*buf, int count, MPI_Datatype datatype, int dest, int sendtag, int source, int recvtag,
                         MPI_Comm comm, MPI_Status*status);

int MPI_Sendrecv_replace_c(void*buf, MPI_Count count, MPI_Datatype datatype, int dest, int sendtag, int source, int recvtag,
                         MPI_Comm comm, MPI_Status*status);



/**
 * Nonblocking operation that returns flag = true if there is a
 * message that can be received and that matches the message envelope
 * specified by source, tag and comm.
 * @param source source rank, or MPI_ANY_SOURCE
 * @param tag tag value or MPI_ANY_TAG
 * @param comm communicator
 * @param flag true if operation has completed
 * @param status status object
 * @return MPI status
 */
int MPI_Iprobe(int source, int tag, MPI_Comm comm, int*flag, MPI_Status*status);

/**
 * Blocks and returns only after a message that matches the message
 * envelope specified by source, tag and comm can be received.
 * @param source source rank, or MPI_ANY_SOURCE
 * @param tag tag value or MPI_ANY_TAG
 * @param comm communicator
 * @param status status object
 * @return MPI status
 */
int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status*status);

int MPI_Improbe(int source, int tag, MPI_Comm comm, int*flag, MPI_Message*message, MPI_Status*status);

int MPI_Mprobe(int source, int tag, MPI_Comm comm, MPI_Message*message, MPI_Status*status);

int MPI_Mrecv(void*buf, int count, MPI_Datatype datatype, MPI_Message*message, MPI_Status*status);

int MPI_Mrecv_c(void*buf, MPI_Count count, MPI_Datatype datatype, MPI_Message*message, MPI_Status*status);

int MPI_Imrecv(void* buf, int count, MPI_Datatype datatype, MPI_Message*message, MPI_Request*request);

int MPI_Imrecv_c(void* buf, MPI_Count count, MPI_Datatype datatype, MPI_Message*message, MPI_Request*request);


/* ** Partitioned communication **************************** */

int MPI_Psend_init(const void*buf, int partitions, MPI_Count count,
                   MPI_Datatype datatype, int dest, int tag, MPI_Comm comm,
                   MPI_Info info, MPI_Request*request);
int MPI_Precv_init(void*buf, int partitions, MPI_Count count,
                   MPI_Datatype datatype, int source, int tag, MPI_Comm comm,
                   MPI_Info info, MPI_Request*request);
int MPI_Pready(int partition, MPI_Request request);
int MPI_Pready_range(int partition_low, int partition_high, MPI_Request request);
int MPI_Pready_list(int length, const int array_of_partitions[], MPI_Request request);
int MPI_Parrived(MPI_Request request, int partition, int*flag);


/** @}*/


/** @}*/


#endif /* NM_MPI_P2P_H */
