/*
 * NewMadeleine
 * Copyright (C) 2019-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/*
 * nm_coll_dynamic_interface
 * --------------------------
 *
 *  Deprecated: use the mcast interface instead.
 *
 *  Provide abstraction to broadcast data to several nodes in a dynamic fashion:
 *  - only sender node has to know all the recipient nodes
 *  - nodes can receive data from broadcasts or from regular sends
 *
 *
 *  Basic use:
 *  1. Initialize the interface by calling nm_coll_dynamic_init_multicast()
 *  2. Send broadcasts with nm_coll_dynamic_send*() functions
 *  3. Receive data with nm_coll_dynamic_register_recv*() functions
 *  4. Shutdown the interface by calling nm_coll_dynamic_shutdown()
 *
 *  See examples/coll_dynamic/nm_coll_dynamic_basic.c for a basic example.
 *
 *
 *  Three types of tree can be used for broadcasting: binomial, binary or chained
 *  trees.
 *  Binomial tree is the default scheme.
 *  You can set the type of tree to use by calling nm_coll_dynamic_set_tree_kind()
 *  of by defining the environment variable NMAD_COLL_DYNAMIC_TREE to `binary`,
 *  `binomial` or `chain`.
 *
 *
 *  When receiving a broadcast message, default behaviour is forwarding data to
 *  other nodes and then return received data to the application. This behaviour
 *  can be reversed: when a broadcast message is received, data is copied to an
 *  internal buffer, data is returned to the application and then data stored in
 *  the internal buffer is forwarded to other nodes. To do so, you have to define
 *  the environment variable NMAD_COLL_DYNAMIC_COPY to `1`.
 *
 *
 *  This interface use the RPC interface, with a dedicated session, so you can use
 *  the whole space of tags for your application.
 */

#ifndef NM_COLL_DYNAMIC_H
#define NM_COLL_DYNAMIC_H

#include <nm_sendrecv_interface.h>
#include <nm_coll_interface.h>

#ifdef PIOMAN

typedef void (*callback_function)(void*);
typedef void (*callback_function_come_from_coop)(struct nm_data_s*, nm_len_t, void*);
typedef void (*callback_function_normal_recv)(nm_sr_request_t*, void*);


void nm_coll_dynamic_init_multicast();


/**
 * If user provides cond_wait_recv, he has to initialize it before and free it after use.
 */
void nm_coll_dynamic_recv_register_recv(nm_session_t p_session, int src_node, nm_tag_t tag, void *data, 
                                        nm_len_t len, nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv, 
                                        callback_function_come_from_coop come_from_coop_callback,
                                        callback_function end_recv_callback, callback_function_normal_recv normal_recv_callback,
                                        void* arg);
void nm_coll_dynamic_recv_register_recv_data(nm_session_t p_session, int src_node, nm_tag_t tag, struct nm_data_s* data, 
                                        nm_len_t len, nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv, 
                                        callback_function_come_from_coop come_from_coop_callback,
                                        callback_function end_recv_callback, callback_function_normal_recv normal_recv_callback,
                                        void* arg);

/**
 * coll_dynamic version of nm_sr_rcancel()
 * Returns same error codes
 */
int nm_coll_dynamic_remove_recv(int src_node, nm_tag_t tag);


/**
 * To respect priorities, array `prios` has to be sorted in descending order (and array `nodes`
 * ordered such as prios[i] is the priority of the send to the node nodes[i])
 */
void nm_coll_dynamic_send(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, void* data, nm_len_t body_len, callback_function end_send_callback, void* arg);
void nm_coll_dynamic_send_data(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, struct nm_data_s* body_data, nm_len_t body_len, callback_function end_send_callback, void* arg);

void nm_coll_dynamic_shutdown(void);

void nm_coll_dynamic_set_tree_kind(nm_coll_tree_kind_t kind);

#else /* PIOMAN */
#  error "cannot use interface coll_dynamic without pioman"
#endif /* PIOMAN */

#endif /* NM_COLL_DYNAMIC_H */

