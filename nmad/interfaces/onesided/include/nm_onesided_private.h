/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * internal definitions for interface 'onesided'.
 * Not part of the public API. Included ion the interface header
 * only for inlining purpose.
 */

#ifndef NM_ONESIDED_PRIVATE_H
#define NM_ONESIDED_PRIVATE_H

#include <nm_sendrecv_interface.h>

#include <stdint.h>

typedef uint32_t nm_onesided_flag_t;

#define NM_ONESIDED_OP_PUT                 ((nm_onesided_flag_t)0x01)
#define NM_ONESIDED_OP_GET                 ((nm_onesided_flag_t)0x02)
#define NM_ONESIDED_OP_FENCE               ((nm_onesided_flag_t)0x03)
#define NM_ONESIDED_OP_QUEUE               ((nm_onesided_flag_t)0x04)
#define NM_ONESIDED_OP_MASK                ((nm_onesided_flag_t)0x0F)
/** generate a completion on the target side */
#define NM_ONESIDED_FLAG_TARGET_COMPLETION ((nm_onesided_flag_t)0x10)


/** header used on the wire for onesided requests */
struct nm_onesided_header_s
{
  uintptr_t addr;            /**< remote address (or queue id for enqueue) */
  nm_len_t size;             /**< size of the onsided data (not incuding target-side completion) */
  nm_onesided_flag_t flags;
  nm_seq_t seq;              /**< seq number for target-side completion */
  uint8_t target_size;       /**< size of target-side completion data */
} __attribute__((packed));

/** a send request for onesided */
struct nm_onesided_request_s
{
  nm_sr_request_t sr_req;
  struct nm_onesided_header_s header;
  struct nm_data_s user_data;
  struct nm_data_s target_data;
};

PUK_ALLOCATOR_TYPE(nm_sr_req, nm_sr_request_t);

/** a request used for target-side completions */
struct nm_onesided_target_request_s
{
  nm_sr_request_t local_req;  /**< local request to inject data into the target-side completion */
  nm_len_t size;              /**< size of the onesided operation */
  nm_onesided_flag_t op;      /**< opcode of the associated onesided operation */
  uintptr_t addr;             /**< address of the onesided operation */
};

/** maximum size for target-side completion data */
#define NM_ONESIDED_TARGET_COMPLETION_MAX 64

/** status of a target-side completion */
struct nm_onesided_target_completion_s
{
  struct nm_onesided_s*p_onesided;
  nm_len_t op_size;           /**< size of the onesided operation */
  nm_onesided_flag_t op;      /**< opcode of the associated onesided operation */
  uintptr_t addr;             /**< address of the onesided operation */
  nm_len_t target_size;       /**< size of the completion data */
  char target_buf[NM_ONESIDED_TARGET_COMPLETION_MAX]; /**< data for the completion request */
};

PUK_ALLOCATOR_TYPE(nm_onesided_target_completion, struct nm_onesided_target_completion_s);

PUK_DLFQ_TYPE(nm_onesided_queue_entry, void*, NULL);
PUK_VECT_TYPE(nm_onesided_queue, struct nm_onesided_queue_entry_dlfq_s*);

/** an instance of interface 'onesided' */
struct nm_onesided_s
{
  nm_session_t p_session;
  struct nm_onesided_request_s request; /**< persistent request for recv */
  struct nm_onesided_target_completion_s*p_target; /**< target-side completion for the current request */
  struct nm_data_s data; /** data descriptor for recv; persistant accross handlers for RECV_DATA and FINALIZED */
  nm_sr_req_allocator_t req_allocator;
  nm_onesided_target_completion_allocator_t completion_allocator;
  struct nm_onesided_queue_vect_s queues;
  int running;
  nm_cond_status_t done;
};

#endif /* NM_ONESIDED_PRIVATE_H */
