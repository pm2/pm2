/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/* Routines to reorder nodes in a tree.
 *
 * These functions reorder nodes in array according to an array of their
 * respective priority.
 */

#ifndef NM_MCAST_REORDER_TREE_H
#define NM_MCAST_REORDER_TREE_H

/** reverse comparison function for (prio; dest) pairs
 * (reverse to get descending order)
 */
static int nm_mcast_pairs_compare(const void*_p1, const void*_p2)
{
  const int*p_pair1 = _p1;
  const int*p_pair2 = _p2;
  if(p_pair1[0] < p_pair2[0])
    return 1;
  else if(p_pair1[0] > p_pair2[0])
    return -1;
  else return 0;
}

/** sort p_dests & p_prios by prio descending order */
static inline void _nm_mcast_generic_reorder_nodes(const int*p_dests, const int*p_prios,
                                                   int*p_ordered_dests, int*p_ordered_prios,
                                                   int n, int reverse)
{
  int*p_pairs = padico_malloc(sizeof(int) * n * 2);
  int i;
  for(i = 0; i < n; i++)
    {
      p_pairs[2 * i] = p_prios[i];
      p_pairs[2 * i + 1] = p_dests[i];
    }
  qsort(p_pairs, n, sizeof(int) * 2, &nm_mcast_pairs_compare);
  if (reverse)
    {
      for(i = 0; i < n; i++)
        {
          p_ordered_prios[n-i-1] = p_pairs[2 * i];
          p_ordered_dests[n-i-1] = p_pairs[2 * i + 1];
        }
    }
  else
    {
      for(i = 0; i < n; i++)
        {
          p_ordered_prios[i] = p_pairs[2 * i];
          p_ordered_dests[i] = p_pairs[2 * i + 1];
        }
    }
  padico_free(p_pairs);
}

/** sort p_dests & p_prios by prio descending order (ie, to respect priorities) */
static inline void nm_mcast_generic_reorder_nodes(const int*p_dests, const int*p_prios,
                                                  int*p_ordered_dests, int*p_ordered_prios,
                                                  int n)
{
  _nm_mcast_generic_reorder_nodes(p_dests, p_prios, p_ordered_dests, p_ordered_prios, n, /* not reversed */ 0);
}

/** sort p_dests & p_prios by prio ascending order (ie, opposite of respecting priorities) */
static inline void nm_mcast_generic_reorder_nodes_reverse(const int*p_dests, const int*p_prios,
                                                          int*p_ordered_dests, int*p_ordered_prios,
                                                          int n)
{
  _nm_mcast_generic_reorder_nodes(p_dests, p_prios, p_ordered_dests, p_ordered_prios, n, /* reversed */ 1);
}

/** shuffle (p_dests, p_prios) couples  */
static inline void nm_mcast_generic_reorder_nodes_shuffle(const int*p_dests, const int*p_prios,
                                                          int*p_ordered_dests, int*p_ordered_prios,
                                                          int n)
{
  /* Implementation of Fisher–Yates shuffle */
  int tmp_dest;
  int tmp_prio;

  memcpy(p_ordered_dests, p_dests, n * sizeof(int));
  memcpy(p_ordered_prios, p_prios, n * sizeof(int));

  for (int i = n-1; i > 0; i--)
    {
      int j = rand() % (i+1);
      tmp_dest = p_ordered_dests[j];
      tmp_prio = p_ordered_prios[j];
      p_ordered_dests[j] = p_ordered_dests[i];
      p_ordered_prios[j] = p_ordered_prios[i];
      p_ordered_dests[i] = tmp_dest;
      p_ordered_prios[i] = tmp_prio;
    }
}

#endif /* NM_MCAST_REORDER_TREE_H */
