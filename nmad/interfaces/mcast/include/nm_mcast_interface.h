/* * NewMadeleine
 * Copyright (C) 2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MCAST_H
#define NM_MCAST_H

#include <nm_coll_interface.h>


/** @defgroup mcast_interface Multicast interface
 *
 * Provide abstraction to multicast data to several nodes in a dynamic fashion:
 * - only sender node has to know all the recipient nodes,
 * - no need to build a communicator containing all recipients of the the multicast,
 * - whatever the way data was sent (with a point-to-point send or a
 *   multicast), it will be transparent for receivers: they just have to use a
 *   regular point-to-point receive function.
 *
 *  Basic use:
 *  1. Initialize the interface by calling nm_mcast_init()
 *  2. Send multicasts with nm_mcast_*send() functions
 *  3. Receive data as if the data will come from a regular point-to-point
 *     exchange: with a nm_sr_recv() function. Be sure to use the session of
 *     the communicator used in nm_mcast_send().
 *  4. Shutdown the interface by calling nm_mcast_finalize()
 *
 * See examples/mcast/nm_mcast_basic.c for a basic example.
 *
 * Three types of tree can be used for multicasting: binomial, binary or chained
 * trees.
 * Binomial tree is the default scheme.
 * You can set the default type of tree to use by calling
 * nm_mcast_set_default_tree_kind() of by defining the environment variable
 * NMAD_MCAST_TREE to `binary`, `binomial` or `chain`. Moreover you can change
 * the type of tree directly when calling nm_mcast_send().
 *
 * @example nm_mcast_basic.c
 */

/** @ingroup mcast_interface
 * @{
 */

/** Public abstraction for a mcast service */
typedef struct nm_mcast_service_s*nm_mcast_service_t;

/** Public abstraction for the callback called when the mcast_send is completed */
typedef void (*nm_mcast_send_notifier_t)(void*p_ref);

/** A multicast send request */
struct nm_mcast_req_s
{
  nm_cond_status_t cond_mcast_ended;
  int initialized;                      /**< @internal just to be sure nm_mcast_send_init was called */
  nm_mcast_send_notifier_t p_notifier;  /**< notification function to call uppon mcast completion */
  void*p_notifier_ref;                  /**< user-supplied parameter for the notifier */
  nm_mcast_service_t p_mcast_service;
};

/** Public abstraction for nm_mcast_req_s */
typedef struct nm_mcast_req_s nm_mcast_t;

/** Initializes the mcast interface
 *  @param p_comm communicator used for the RPC service for mcast
 */
nm_mcast_service_t nm_mcast_init(nm_comm_t p_comm);

/** Release the mcast interface */
void nm_mcast_finalize(nm_mcast_service_t nm_mcast);

/** Send data in a blocking mode.
 *  @param nm_mcast a mcast service initiliazed with nm_mcast_init().
 *  @param p_recv_comm a communicator (must be the same as the one used by the nm_sr_recv).
 *  @param dests an array of ranks which are recipients of the multicast. This
 *               array can be a subset of the communicator p_comm.
 *  @param prios an array providing priorities for each recipient. Can be set
 *               to NULL. To respect priorities, prios has to be sorted in descending order
 *               (and array `dests` ordered such as prios[i] is the priority of the send to
 *               the node dests[i]). Higher is the integer, higher is the priority.
 *  @param n_dests the number of recipients of the multicast (size of the array dests).
 *  @param tag the message tag.
 *  @param p_data iterator on the data to send, the memory targeted by this
 *                pointer has to be valid until the mcast ends.
 *  @param hlen size of the user header.
 *  @param tree_kind type of tree to broadcast the data. If the user provides
 *                   NM_COLL_TREE_DEFAULT, the type set by nm_mcast_set_default_tree_kind() will
 *                   be used.
 */
void nm_mcast_send(nm_mcast_service_t nm_mcast, nm_comm_t p_recv_comm, int*p_dests, int*p_prios, int n_dests,
                   nm_tag_t tag, struct nm_data_s*p_data, nm_len_t hlen, nm_coll_tree_kind_t tree_kind);

/** Initialize a multicast send.
 * Useful to then wait the end of the multicast send or to set a callback.
 * To be released with nm_mcast_send_destroy().
 */
void nm_mcast_send_init(nm_mcast_service_t nm_mcast, nm_mcast_t*p_mcast);

/** Attach a notifier to mcast_send, to be called when the root node of the multicast ends its sends. */
void nm_mcast_send_set_notifier(nm_mcast_t*p_mcast, nm_mcast_send_notifier_t notifier, void*p_ref);

/** Send data in a non-blocking mode.
 * @param p_mcast has to initialized with nm_mcast_send_init().
 * See nm_mcast_send() for details about other parameters.
 */
void nm_mcast_isend(nm_mcast_t*p_mcast, nm_comm_t p_comm, int*p_dests, int*p_prios,
                    int n_dests, nm_tag_t tag, struct nm_data_s*p_data, nm_len_t hlen, nm_coll_tree_kind_t tree_kind);

/** Wait for a non-blocking multicast send */
void nm_mcast_wait(nm_mcast_t*p_mcast);

/** Free data allocated by nm_mcast_send_init() */
void nm_mcast_send_destroy(nm_mcast_t*p_mcast);

/** Change the default tree used for routing data. It will be the rooting
 * algorithm used if the user provides NM_COLL_TREE_DEFAULT to nm_mcast_*send
 * functions. */
void nm_mcast_set_default_tree_kind(nm_mcast_service_t nm_mcast, const nm_coll_tree_kind_t kind);

/** @} */


#endif /* NM_MCAST_H */

