/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** File descriptor translation from ABI to real/virtual numbering
 * @ingroup PukABI
 */

#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <execinfo.h>

#include "Puk-ABI-internals.h"
#include "Puk-ABI.h"

#ifdef PUKABI_ENABLE_FSYS

/* File descriptor virtualization.  */

struct puk_fd_s
{
  int kind; /* 0: empty; 1: real; 2: virtual */
};
#define PUK_FDS_NONE 0
#define PUK_FDS_REAL 1
#define PUK_FDS_VFD  2
#define PUK_FDS_RREAL 5

static struct puk_fd_s puk_abi_fds[PUK_ABI_FD_MAX] =
  {
    [0] = { .kind = PUK_FDS_REAL }, /* stdin  */
    [1] = { .kind = PUK_FDS_REAL }, /* stdout */
    [2] = { .kind = PUK_FDS_REAL }, /* stderr */
    [3 ... PUK_ABI_FD_MAX-1] = { .kind = PUK_FDS_NONE } /* set remaining entries to NONE */
  };


/* ********************************************************* */

/** virtualize a given fd */
int puk_fds_alloc(const int kind, const int vfd, const char*caller)
{
  int fd = vfd;
  puk_abi_trace("# PukABI: puk_fds_alloc()- kind = %d; fd = %d; caller = %s", kind, fd, caller);
  if(fd == -1)
    {
      /* allocate a dummy fd as a placeholder for vfd */
      puk_abi_assert(kind == PUK_FDS_VFD);
      fd = PUK_ABI_WRAP(open)("/dev/null", 0);
      if(fd == -1)
        {
          puk_abi_fatal("# PukABI: cannot virtualize file descriptor.\n");
        }
    }
  if( puk_abi_fds[fd].kind == PUK_FDS_RREAL)
    {
      puk_abi_fds[fd].kind = PUK_FDS_NONE;
    }
  if(fd >= PUK_ABI_FD_MAX || fd < 0)
    {
      puk_abi_fatal("# PukABI: out of file descriptors (MAX=%d)\n", PUK_ABI_FD_MAX);
    }
  puk_abi_assert(fd >= 0 && fd < PUK_ABI_FD_MAX);
  puk_abi_assert(kind == PUK_FDS_REAL || kind == PUK_FDS_VFD || kind == PUK_FDS_RREAL );
  if(puk_abi_fds[fd].kind != PUK_FDS_NONE && puk_abi_fds[fd].kind != kind)
    {
      if(kind == PUK_FDS_VFD)
        {
          puk_abi_print("# PukABI: WARNING- fd = %d already registered as real. Override.", fd);
          puk_abi_fds[fd].kind = PUK_FDS_NONE;
        }
      else
        {
          puk_abi_fatal("# PukABI: cannot register fd = %d: already registered with different kind.\n", fd);
        }
    }
  puk_abi_fds[fd] = (struct puk_fd_s){ .kind = kind };
  puk_abi_trace("# PukABI: adding FD: %d; kind = %d (%s)", fd, kind, caller);
  return fd;
}

/** free an entry for a given fd */
void puk_fds_free(int abi_fd)
{
  puk_abi_trace("# PukABI: deleting FD abi: %d", abi_fd);
  if(puk_abi_fds[abi_fd].kind == PUK_FDS_VFD)
    {
      PUK_ABI_WRAP(close)(abi_fd);
    }
  puk_abi_assert(abi_fd >= 0 && abi_fd < PUK_ABI_FD_MAX);
  puk_abi_fds[abi_fd].kind = PUK_FDS_NONE;
}

static void puk_fds_show_info(int fd) __attribute__((unused));
static void puk_fds_show_info(int fd)
{
  char path[256];
  snprintf(path, 1024, "/proc/%d/fd/%d", getpid(), fd);
  puk_abi_print("# PukABI: reading info for fd = %d from %s", fd, path);
  char buf[256];
  int rc = readlink(path, buf, 256);
  buf[255] = '\0';
  if(rc > 0)
    {
      puk_abi_print("# PukABI: %s -> %s", path, buf);
    }
  puk_abi_print("# PukABI: backtrace-");
  void*btbuf[100];
  rc = backtrace(btbuf, 100);
  char**symbols = backtrace_symbols(btbuf, rc);
  if (symbols)
    {
      int i;
      for(i = 0; i < rc; i++)
        {
          puk_abi_print("# PukABI: frame #%2d: %s", i, symbols[i]);
        }
    }
  puk_abi_print("# PukABI: done.");
}

int puk_fds_isreal(int abi_fd)
{
  if(abi_fd < 0)
    {
      /** @note This should be an 'abort()'. However, libresolv performs
       * 'read()' on fd=-1 on purpose. Give a sensible answer to make
       * libresolv happy :-/
       */
      return 1;
    }
  puk_abi_assert(abi_fd < PUK_ABI_FD_MAX);
  switch(puk_abi_fds[abi_fd].kind)
    {
    case PUK_FDS_REAL:
      return 1;
      break;
    case PUK_FDS_RREAL:
      return 1;
      break;
    case PUK_FDS_VFD:
      return 0;
      break;
    case PUK_FDS_NONE:
      /* recover automatically on unkown fd */
#ifdef PUK_ABI_SHOW_RECOVERY
      puk_abi_print("# PukABI: automatic recovery for unknown fd = %d", abi_fd);
      puk_fds_show_info(abi_fd);
#endif /* PUK_ABI_SHOW_RECOVERY */
      puk_fds_alloc(PUK_FDS_RREAL, abi_fd, __FUNCTION__);
      return 1;
      break;
    default:
      break;
    }
  return 0;
}

/** allocate a vfd- used in VIO */
extern int puk_abi_vfd_alloc(void)
{
  return puk_fds_alloc(PUK_FDS_VFD, -1, __FUNCTION__);
}

/** free a vfs- used in VIO */
extern void puk_abi_vfd_free(int abi_fd)
{
  puk_fds_free(abi_fd);
}



/* ********************************************************* */
/* ** fd_set */

int puk_fdset_isreal(const void*fds1, const void*fds2, const void*fds3, int fdmax)
{
  int isreal = -1;
  const fd_set*f1 = fds1;
  const fd_set*f2 = fds2;
  const fd_set*f3 = fds3;
  int i;
  for(i = 0; i < fdmax; i++)
    {
      if((f1 && FD_ISSET(i, f1)) ||
         (f2 && FD_ISSET(i, f2)) ||
         (f3 && FD_ISSET(i, f3)))
        {
          if(isreal != -1 && puk_fds_isreal(i) != isreal)
            {
              while(1)
                ;

              puk_abi_fatal("# PukABI: inconsistency detected while translating fdset at fd #%d (isreal=%d; fd_isreal=%d)\n",
                            i, isreal, puk_fds_isreal(i));
            }
          isreal = puk_fds_isreal(i);
        }
    }
  return isreal;
}


/* ********************************************************* */
/* ** pollfd */

int puk_pollfd_isreal(void*ptr, int nfds)
{
  int n_real = 0, n_virtual = 0;
  int i;
  struct pollfd* fds = ptr;
  for(i = 0; i < nfds; i++)
    {
      if(puk_fds_isreal(fds[0].fd))
        n_real++;
      else
        n_virtual++;
    }
  if((n_real != 0) && (n_virtual != 0))
    {
      puk_abi_fatal("# PukABI: poll()- cannot manage mixed fdset real = %d; virtual = %d.\n", n_real, n_virtual);
    }

  return fds[0].fd;
}

#endif /* PUKABI_ENABLE_FSYS */
