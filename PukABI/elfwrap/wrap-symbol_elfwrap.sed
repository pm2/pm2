#! /bin/sed
s/^\(.*\)$/\1 puk_abi_wrap__\1/
# h
# s/^\(_*[a-z]_*[a-z]\).*$/\1/
# y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/
# G
# s/^\(.*\)\n\(.*\)$/\2 \1/
# G
# s/^\([^ ]*\) \(_*[A-Z]_*[A-Z]\)\n_*[a-z]_*[a-z]\(.*\)$/\1 \2\3/
