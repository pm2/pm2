#! /bin/sh -e

echo "Initializing PukABI..."

export M4PATH=../building-tools:./building-tools:${M4PATH}

${AUTOCONF:-autoconf} 
${AUTOHEADER:-autoheader} -f

echo "Done."
