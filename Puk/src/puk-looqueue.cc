/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * Stub to use C++ LOO queue from C code.
 */


/* do not include full Puk.h to avoid collision on symbol CACHE_LINE_SIZE
 * from other lfqueue external implementations */
#include "Puk-config.h"

#ifdef PUK_ENABLE_LOOQUEUE
#include "Puk-looqueue.h"

#include "../externals/looqueue/include/looqueue/queue.hpp"

extern "C" {

  void puk_looqueue_init(void**_q)
  {
    loo::queue<uintptr_t>**q = (loo::queue<uintptr_t>**)_q;
    *q = new loo::queue<uintptr_t>();
  }
  void puk_looqueue_enqueue(void*_q, void*v)
  {
    loo::queue<uintptr_t>*q = (loo::queue<uintptr_t>*)_q;
    uintptr_t*_v = (uintptr_t*)v;
    q->enqueue(_v);
  }
  void*puk_looqueue_dequeue(void*_q)
  {
    loo::queue<uintptr_t>*q = (loo::queue<uintptr_t>*)_q;
    uintptr_t*_v = q->dequeue();
    void*v = (void*)_v;
    return v;
  }
  void puk_looqueue_free(void*_q)
  {
    loo::queue<uintptr_t>*q = (loo::queue<uintptr_t>*)_q;
    delete q;
  }

}

#endif /* PUK_ENABLE_LOOQUEUE */
