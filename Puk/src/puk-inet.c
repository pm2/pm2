/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* @file
*/

#include "Module.h"
#include "Puk-internals.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <limits.h>

#ifdef __linux__
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#endif

#ifdef HAVE_LINUX_SOCKIOS_H
#include <linux/sockios.h>
#endif
#ifdef HAVE_LINUX_ETHTOOL_H
#include <linux/ethtool.h>
#endif
#ifdef HAVE_LINUX_IF_H
#include <linux/if.h>
#else
#include <net/if.h>
#endif /* HAVE_LINUX_IF_H */

PADICO_MODULE_HOOK(Puk);

PADICO_MODULE_ATTR(inet_iface, "PUK_INET_IFACE", "use the address of the given network interface to publish our own address; set to * for any interface", string, "*");

PADICO_MODULE_ATTR(inet_resolve, "PUK_INET_RESOLVE", "use the DNS entry obtained when resolving the hostname to publish our own address", bool, 0);

/* ********************************************************* */

/** get local IPv4 address */
struct in_addr puk_inet_getaddr(void)
{
  struct in_addr inaddr;
  inaddr.s_addr = INADDR_ANY;
  if(puk_mckernel())
    {
#ifdef __linux__
      /* create netlink socket */
      int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
      if(fd < 0)
        {
          padico_fatal("cannot open netlink socket (socket: %s).\n", strerror(errno));
        }
      struct sockaddr_nl nladdr = {
        .nl_family = AF_NETLINK,
        .nl_pad    = 0,
        .nl_pid    = getpid(),
        .nl_groups = 0
      };
      int rc = bind(fd, (struct sockaddr*)&nladdr, sizeof(nladdr));
      if(rc < 0)
        {
          padico_fatal("cannot bind netlink socket.\n");
        }
      /* netlink request */
      struct
      {
        struct nlmsghdr nl; /**< netlink header */
        struct ifaddrmsg ifa;
        char            buf[8192];
      } req;
      memset(&req, 0, sizeof(req));
      req.nl.nlmsg_len   = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
      req.nl.nlmsg_type  = RTM_GETADDR;
      req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
      req.nl.nlmsg_seq   = 0;
      req.nl.nlmsg_pid   = getpid();
      req.ifa.ifa_family = AF_INET;
      struct iovec iov = { .iov_base = (void*)&req.nl, .iov_len = req.nl.nlmsg_len };
      /* send request */
      struct sockaddr_nl peer = {
        .nl_family = AF_NETLINK,
        .nl_pad    = 0,
        .nl_pid    = 0, /* 0 = kernel */
        .nl_groups = 0
      };
      struct msghdr msg = {
        .msg_iov     = &iov,
        .msg_iovlen  = 1,
        .msg_name    = (void*)&peer,
        .msg_namelen = sizeof(peer)
      };
      rc = sendmsg(fd, &msg, 0);
      if(rc < 0)
        padico_fatal("error while sending netlink request.\n");
      /* receive reply */
      char buf[8192];
      memset(buf, 0, sizeof(buf));
      char*p = buf;
      int nll = 0;
      while(1)
        {
          rc = recv(fd, p, sizeof(buf) - nll, 0);
          struct nlmsghdr*nlp = (struct nlmsghdr*)p;
          if(nlp->nlmsg_type == NLMSG_DONE)
            break;
          p += rc;
          nll += rc;
        }
      /* parse reply */
      const struct nlmsghdr*nlp = (struct nlmsghdr*)buf;
      while(NLMSG_OK(nlp, nll))
        {
          const struct ifaddrmsg*ifa = (struct ifaddrmsg*)NLMSG_DATA(nlp);
          const struct rtattr*rtap = (struct rtattr*)IFA_RTA(ifa);
          int rtl = IFA_PAYLOAD(nlp);
          while(RTA_OK(rtap, rtl))
            {
              switch(rtap->rta_type)
                {
                case IFA_LOCAL:
                  {
                    if((ifa->ifa_family == AF_INET) && !(ifa->ifa_flags & IFF_LOOPBACK))
                      {
                        const struct in_addr*_inaddr = (const struct in_addr*)RTA_DATA(rtap);
                        inaddr = *_inaddr;
                      }
                  }
                  break;
                default:
                  break;
                }
              rtap = RTA_NEXT(rtap,rtl);
            }
          nlp = NLMSG_NEXT(nlp, nll);
        }
      close(fd);
#else
      padico_fatal("internal error- running on mckernel, without linux headers.");
#endif
    }
  else
    {
      /* !MCKERNEL */
      const char*puk_inet_iface = padico_module_attr_inet_iface_getvalue();
      if(strcmp(puk_inet_iface, "*") == 0)
        {
          puk_inet_iface = NULL;
        }
      const int puk_inet_resolve = padico_module_attr_inet_resolve_getvalue();
      if(puk_inet_resolve && puk_inet_iface)
        {
          padico_fatal("cannot give PUK_INET_RESOLVE & PUK_INET_IFACE at the same time.\n");
        }
      if(puk_inet_resolve)
        {
          /* DNS-based address detection */
          char hostname[HOST_NAME_MAX];
          gethostname(hostname, HOST_NAME_MAX);
          const struct hostent*he = gethostbyname2(hostname, AF_INET);
          if(he != NULL)
            {
              const struct in_addr**addr = (const struct in_addr**)he->h_addr_list;
              inaddr.s_addr = (*addr)->s_addr;
            }
        }
      else
        {
          /* regular method: getifaddrs */
          struct ifaddrs*ifa_list = NULL;
          int rc = getifaddrs(&ifa_list);
          if(rc == 0)
            {
              struct ifaddrs*i;
              for(i = ifa_list; i != NULL; i = i->ifa_next)
                {
                  const int is_loopback = !!(i->ifa_flags & IFF_LOOPBACK);
                  const int is_up = !!(i->ifa_flags & IFF_UP);
                  if( is_up && (!is_loopback) &&
                      (i->ifa_addr != NULL) &&
                      i->ifa_addr->sa_family == AF_INET)
                    {
                      if((puk_inet_iface == NULL) || (strcmp(puk_inet_iface, i->ifa_name) == 0))
                        {
                          struct sockaddr_in*saddr_in = (struct sockaddr_in*)i->ifa_addr;
                          inaddr = saddr_in->sin_addr;
                          break;
                        }
                    }
                }
              freeifaddrs(ifa_list);
              ifa_list = NULL;
            }
        }
    }
  if(inaddr.s_addr == INADDR_ANY)
    {
      padico_fatal("unable to detect IP address.\n");
    }
  return inaddr;
}

char*puk_inet_get_ifname_byaddr(struct in_addr*in_addr)
{
  /* resolve ifname from in_addr */
  struct ifaddrs*ifa_list = NULL;
  int rc = getifaddrs(&ifa_list);
  if(rc == 0)
    {
      struct ifaddrs*i;
      for(i = ifa_list; i != NULL; i = i->ifa_next)
        {
          if(i->ifa_addr->sa_family == AF_INET)
            {
              struct sockaddr_in*saddr_in = (struct sockaddr_in*)i->ifa_addr;
              if(in_addr->s_addr == saddr_in->sin_addr.s_addr)
                {
                  char*ifname = padico_strdup(i->ifa_name);
                  freeifaddrs(ifa_list);
                  return ifname;
                }
            }
        }
      freeifaddrs(ifa_list);
      ifa_list = NULL;
    }
  padico_warning("failed to get ifname for given address.\n");
  return NULL;
}

/** get ethernet link speed in MB/s */
int puk_inet_get_link_speed(const char*ifname)
{
  int link_speed = -1;
  int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
   if(sock < 0)
     {
       padico_warning("cannot open raw IP socket for ifname = %s\n", ifname);
       return -1;
     }
   struct ifreq ifr;
   struct ethtool_cmd edata;
   strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
   ifr.ifr_data = (void*)&edata;
   edata.cmd = ETHTOOL_GSET;
   int rc = ioctl(sock, SIOCETHTOOL, &ifr);
   if (rc < 0)
     {
       padico_warning("cannot get link speed for ifname = %s\n", ifname);
       goto close;
     }
   link_speed = edata.speed / 8;
 close:
   close(sock);
   return link_speed;
}
