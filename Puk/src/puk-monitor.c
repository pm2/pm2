/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Implementation of some basic monitoring tools (lsmod, lsdriver)
 * @ingroup Puk
 */

#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);


/* *** Module inspectors *********************************** */

padico_rc_t puk_mod_lsmod(void)
{
  padico_rc_t rc = NULL;
  padico_modID_vect_t modIDs = puk_mod_getmodIDs();
  padico_modID_vect_itor_t m;
  puk_vect_foreach(m, padico_modID, modIDs)
    {
      padico_rc_t line = padico_rc_msg("\n%s", *m);
      rc = padico_rc_cat(rc, line);
      padico_rc_delete(line);
    }
  return rc;
}

padico_rc_t puk_mod_inspectmod(char*mod_name)
{
  padico_rc_t rc = NULL;
  puk_mod_t mod = puk_mod_getbyname(mod_name);
  if(mod == NULL)
    {
      rc = padico_rc_error("Module not found: %s", mod_name);
      return rc;
    }
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "\n<defmod name=\"%s\" driver=\"%s\">\n", mod->mod_name, mod->driver->name);
  /* dump attrs */
  puk_mod_attr_vect_itor_t a;
  puk_vect_foreach(a, puk_mod_attr, mod->attrs)
    {
      const char*value = puk_mod_attr_get_value(a);
      if(a->opt != NULL)
        {
          padico_string_catf(s, "  <attr label=\"%s\" env=\"%s\" type=\"%s\">%s</attr>\n",
                             a->label, a->opt->env_var, puk_opt_type_name(a->opt->type), value);
        }
      else
        {
          padico_string_catf(s, "  <attr label=\"%s\">%s</attr>\n", a->label, value);
        }
    }

  /* dump refs */
  puk_mod_vect_itor_t ref;
  puk_vect_foreach(ref, puk_mod, &mod->requires)
    {
      padico_string_catf(s, "  <requires>%s</requires>\n", (*ref)->mod_name);
    }
  /* dump units */
  puk_unit_vect_itor_t unit;
  puk_vect_foreach(unit, puk_unit, &mod->units)
    {
      padico_string_catf(s, "  <unit>%s</unit>\n", unit->name);
    }
  padico_string_cat(s, "</defmod>\n");
  rc = padico_rc_msg(padico_string_get(s));
  padico_string_delete(s);
  return rc;
}


/* ********************************************************* */

static void lsmod_end_handler(puk_parse_entity_t e)
{
  puk_parse_set_rc(e, puk_mod_lsmod());
}
static const struct puk_tag_action_s lsmod_action =
{
  .xml_tag        = "lsmod",
  .start_handler  = NULL,
  .end_handler    = &lsmod_end_handler,
  .required_level = PUK_TRUST_OUTSIDE
};

/* ********************************************************* */

static void inspectmod_end_handler(puk_parse_entity_t e)
{
  puk_parse_set_rc(e, puk_mod_inspectmod(puk_parse_get_text(e)));
}
static const struct puk_tag_action_s inspectmod_action =
{
  .xml_tag        = "inspectmod",
  .start_handler  = NULL,
  .end_handler    = &inspectmod_end_handler,
  .required_level = PUK_TRUST_OUTSIDE
};


/* ********************************************************* */

void padico_puk_monitor_init()
{
  puk_xml_add_action(lsmod_action);
  puk_xml_add_action(inspectmod_action);
}
