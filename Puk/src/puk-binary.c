/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Provides "binary" module driver
 * @ingroup Puk
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);

/* ********************************************************* */

static padico_rc_t unit_binary_load  (puk_unit_t unit);
static padico_rc_t unit_binary_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t unit_binary_unload(puk_unit_t unit);

static const struct padico_loader_s binary_driver =
{
  .name   = "binary",
  .load   = &unit_binary_load,
  .start  = &unit_binary_start,
  .unload = &unit_binary_unload
};

static puk_component_t binary_driver_component = NULL;

__PUK_SYM_INTERNAL
void padico_puk_binary_init(void)
{
  binary_driver_component = puk_component_declare("PadicoLoader-binary", puk_component_provides("PadicoLoader", "loader", &binary_driver));
}

__PUK_SYM_INTERNAL
void padico_puk_binary_finalize(void)
{
  puk_component_destroy(binary_driver_component);
}


/* ********************************************************* */

/** a library handler, used as driver_specific for 'binary' */
struct puk_lib_s
{
  char*filename;  /**< library file name */
  void*dl_handle; /**< handle for dlopen */
  struct puk_mod_binary_s*binary; /**< binary module descriptor, declared in the module itself */
};
PUK_AUTO_TYPE(puk_lib)
static padico_rc_t   puk_lib_open   (puk_lib_t* lib, const char*lib_name, puk_mod_t mod);
static int           puk_lib_init   (puk_lib_t lib);
static padico_rc_t   puk_lib_unload (puk_lib_t lib, puk_mod_t mod);

#ifndef RTLD_DEFAULT
#  define RTLD_DEFAULT NULL
#endif
#ifndef RTLD_NEXT
# define RTLD_NEXT ((void *) -1l)
#endif

#define PUK_DYLIB_DEFAULT_MODE   (RTLD_NOW | RTLD_GLOBAL)
#define PUK_DYLIB_DEFAULT_HANDLE  RTLD_DEFAULT
#define PUK_DYLIB_NEXT_HANDLE     RTLD_NEXT

static void*puk_dylib_open(const char *pathname, int mode);
static int  puk_dylib_close(void *handle);
static char*puk_dylib_error(void);
static void*puk_dylib_resolve(void*handle, const char*symbol);


/* ********************************************************* */

static padico_rc_t unit_binary_load(puk_unit_t unit)
{
#ifdef PUK_BUILTIN_MODS
  padico_warning("loading dynamic binary module %s while configured for builtin modules.\n", unit->name);
#endif /* PUK_BUILTIN_MODS */
  padico_rc_t rc = NULL;
  puk_lib_t lib = NULL;
  if(unit->driver_specific)
    {
      rc = padico_rc_error("binary driver: load error- unit **%s** is already loaded",
                           unit->name);
      return rc;
    }
  padico_string_t s_lib_file = padico_string_new();
  if(unit->mod->mod_path)
    {
      padico_string_printf(s_lib_file, "%s/", unit->mod->mod_path);
    }
  padico_string_catf(s_lib_file, "binary/%s", unit->name);
  rc = puk_lib_open(&lib, padico_string_get(s_lib_file), unit->mod);
  if(padico_rc_iserror(rc))
    {
      padico_warning("*%s* (%s) load error %s.\n", unit->name, padico_string_get(s_lib_file),
                     padico_rc_gettext(rc));
    }
  else
    {
      unit->driver_specific = (void*)lib;
      int err = puk_lib_init(lib);
      if(err)
        {
          padico_warning("*%s* init error.\n", unit->name);
          rc = padico_rc_error("binary driver: unit **%s** load error- module initialization rc=%d",
                               unit->name, err);
        }
    }
  padico_string_delete(s_lib_file);
  return rc;
}

static padico_rc_t unit_binary_start(puk_unit_t unit, puk_job_t job)
{
  padico_rc_t rc = NULL; /* rc for the *start* operation (not for the job) */
  puk_lib_t lib  = (puk_lib_t)unit->driver_specific;
  if(lib != NULL)
    {
      puk_run_fun_t fun = lib->binary->funcs->run;
      if(fun != NULL)
        {
          int i;
          padico_out(50, "starting unit %s\n", unit->name);

          if(!job->argv)
            {
              job->argc = 1;
              job->argv = padico_malloc(2 * sizeof(char*));
              job->argv[0] = padico_strdup(unit->name);
              job->argv[1] = NULL;
            }

          padico_out(50, "argc = %d\n", job->argc);
          for(i = 0; i < job->argc + 1; i++)
            padico_out(50, " %d : %s\n", i, job->argv[i]);

          int rc = (*(fun))(job->argc, job->argv);
          if(rc)
            {
              job->rc = padico_rc_new();
              job->rc->rc  = rc;
              job->rc->msg = padico_string_new();
            }
          else
            {
              job->rc = padico_rc_ok();
            }
          puk_job_notify(job);
        }
      else
        {
          rc = padico_rc_error("binary driver: cannot start unit **%s** (not runnable)",
                               unit->name);
        }
    }
  else
    {
      rc = padico_rc_error("binary driver: cannot start unit **%s** (not properly initialized)",
                           unit->name);
    }
  return rc;
}

static padico_rc_t unit_binary_unload(puk_unit_t unit)
{
  const puk_lib_t lib = (puk_lib_t)unit->driver_specific;
  padico_rc_t rc = NULL;
  if(lib != NULL)
    {
      rc = puk_lib_unload(lib, unit->mod);
    }
  else
    {
      rc = padico_rc_error("NULL lib");
      padico_warning("lib is NULL in unit %s\n", unit->name);
    }
  unit->driver_specific = NULL;
  return rc;
}


/* ***** Puk dylib ****************************************** */

static inline void puk_dylib_lock(void)
{
  puk_lock_acquire();
}
static inline void puk_dylib_unlock(void)
{
  puk_lock_release();
}

void*puk_dylib_open(const char *pathname, int mode)
{
  static int first = 1;
  static int nodelete = 0;
  if(first)
    {
      const char*dylib_close = getenv("PUK_DYLIB_CLOSE");
      if((dylib_close != NULL) && (atoi(dylib_close) == 0))
        {
          nodelete = 1;
          padico_warning("PUK_DYLIB_CLOSE=0 forced by environment; using RTLD_NODELETE. Keep objects in memory.\n");
        }
      else if((dylib_close != NULL) && (atoi(dylib_close) != 0))
        {
          nodelete = 0;
          padico_warning("PUK_DYLIB_CLOSE=1 forced by environment; not using RTLD_NODELETE. Unloading objects on close.\n");
        }
      else if(PUK_VG_RUNNING_ON_VALGRIND)
        {
          padico_warning("using RTLD_NODELETE by default on valgrind to keep dynamic objects in memory. Define PUK_DYLIB_CLOSE=1 to disable this behavior.\n");
          nodelete = 1;
        }
      first = 0;
    }
  if(nodelete)
    {
      mode |= RTLD_NODELETE;
    }
  puk_dylib_lock();
  void*p = dlopen(pathname, mode);
  if(p == NULL)
    {
      int retry = 10;
      while(p == NULL && retry-- > 0)
        {
          puk_usleep(100000);
          p = dlopen(pathname, mode);
        }
    }
  puk_dylib_unlock();
  return p;
}

int puk_dylib_close(void *handle)
{
  puk_dylib_lock();
  int rc = dlclose(handle);
  puk_dylib_unlock();
  return rc;
}

char*puk_dylib_error(void)
{
  puk_dylib_lock();
  char*rc = dlerror();
  puk_dylib_unlock();
  return rc;
}

void*puk_dylib_resolve(void*handle, const char*symbol)
{
  void*rc;
  const int symbol_maxlen = 256;
  char mangled_symbol[symbol_maxlen];
  snprintf(mangled_symbol, symbol_maxlen, "%s%s", SYMBOL_PREFIX, symbol);
  puk_dylib_lock();
  rc = dlsym(handle, mangled_symbol);
  if(rc == NULL)
    {
      rc = dlsym(handle, symbol);
    }
  puk_dylib_unlock();
  return rc;
}


/* ********************************************************** */

padico_rc_t puk_lib_open(puk_lib_t*lib, const char*lib_file, puk_mod_t mod)
{
  padico_rc_t rc = NULL;
  *lib = puk_lib_new();
  (*lib)->filename = padico_strdup(lib_file);
  padico_out(20, "LIB LOAD loading %s\n", lib_file);

  if(access((*lib)->filename, R_OK) != 0)
    {
      rc = padico_rc_error("LIB load error: %s not found", lib_file);
      return rc;
    }

  (*lib)->dl_handle = puk_dylib_open((*lib)->filename, PUK_DYLIB_DEFAULT_MODE);
  if(!(*lib)->dl_handle)
    {
      rc = padico_rc_error("LIB load error: puk_dylib_open() failed for %s (%s)",
                           lib_file, puk_dylib_error());
      return rc;
    }
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "padico_mod_binary__%s", mod->mod_name);
  padico_string_replace(s, '-', '_');
  struct puk_mod_binary_s*binary = puk_dylib_resolve((*lib)->dl_handle, padico_string_get(s));
  if(binary == NULL)
    {
      rc = padico_rc_error("LIB load error: cannot find symbol %s in lib %s",
                           padico_string_get(s), lib_file);
      return rc;
    }
  (*lib)->binary = binary;
  puk_profile_mem_set_pvars(&binary->profile, mod->mod_name);
  binary->mod = mod;
  padico_string_delete(s);
  padico_out(20, "LIB LOAD %s ok\n", lib_file);
  return rc;
}

int puk_lib_init(puk_lib_t lib)
{
  puk_init_fun_t init_fun = lib->binary->funcs->init;
  int rc = 0;
  if(init_fun != NULL)
    {
      rc = (*init_fun)();
    }
  return rc;
}

padico_rc_t puk_lib_unload(puk_lib_t lib, puk_mod_t mod)
{
  padico_rc_t rc = NULL;
  if(lib == NULL)
    {
      rc = padico_rc_error("LIB UNLOAD: lib %s was not succesfully initialized."
                           " Cannot unload.\n", lib->filename);
      return rc;
    }
  puk_finalize_fun_t fun_ptr = lib->binary->funcs->finalize;
  if (fun_ptr != NULL)
    {
      (*fun_ptr)();
    }
  /* destroy all components of this mod */
  puk_component_home_flush(mod);
  padico_out(50, "LIB closing lib.\n");
  int err = puk_dylib_close(lib->dl_handle);
  padico_out(50, "LIB UNLOAD %s (dlclose rc=%d)\n", lib->filename, err);
  if(err)
    {
      char*msg = puk_dylib_error();
      padico_warning("puk_dylib_close() error *%s*\n", msg);
      rc = padico_rc_error("LIB unload error: dlclose() failed for lib %s", lib->filename);
      return rc;
    }
  padico_out(50, "LIB UNLOAD %s ok\n",
               lib->filename);
  puk_lib_delete(lib);
  return rc;
}


/***************************************************/

void puk_lib_ctor(puk_lib_t l)
{
  l->filename = NULL;
  l->dl_handle = NULL;
}

void puk_lib_dtor(puk_lib_t l)
{
  if(l->filename)
    padico_free(l->filename);
}
