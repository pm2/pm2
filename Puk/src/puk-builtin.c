/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Provides "builtin" module driver
 * @ingroup Puk
 */

#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);

/******************************************************/

PUK_VECT_TYPE(puk_mod_builtin, struct puk_mod_binary_s*);

static struct
{
  puk_mod_builtin_vect_t builtins; /**< vector of builtins modules (already loaded in binary) */
  puk_component_t builtin_loader;
} puk_builtin = { .builtins = NULL };

/******************************************************/

static padico_rc_t puk_unit_builtin_load  (puk_unit_t unit);
static padico_rc_t puk_unit_builtin_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t puk_unit_builtin_unload(puk_unit_t unit);

static const struct padico_loader_s puk_builtin_driver =
{
  .name   = "builtin",
  .load   = &puk_unit_builtin_load,
  .start  = &puk_unit_builtin_start,
  .unload = &puk_unit_builtin_unload,
};

void padico_puk_builtin_init(void)
{
  puk_builtin.builtin_loader =
    puk_component_declare("PadicoLoader-builtin", puk_component_provides("PadicoLoader", "loader", &puk_builtin_driver));
}

void padico_puk_builtin_finalize(void)
{
  /*
    puk_mod_builtin_vect_itor_t i;
    puk_vect_foreach(i, puk_mod_builtin, puk_builtin.builtins)
    {
    const struct puk_mod_builtin_s*const b = *i;
    }
  */
  puk_mod_builtin_vect_delete(puk_builtin.builtins);
}


/* *** 'builtin' toolbox *********************************** */

/* declares a builtin module- used internally by Module.h macros
 * WARNING- called as static initializer before puk_init()
 */
void puk_mod_builtin_declare(struct puk_mod_binary_s*builtin)
{
  if(puk_builtin.builtins == NULL)
    puk_builtin.builtins = puk_mod_builtin_vect_new();
  puk_mod_builtin_vect_push_back(puk_builtin.builtins, builtin);
}

/** tries to load module as built-in.
 */
void puk_mod_builtin_tryload(puk_mod_t*mod, const char*mod_name)
{
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "%s", mod_name);
  padico_string_replace(s, '-', '_');
  const char*mod_name2 = padico_string_get(s);
  puk_lock_acquire();
  padico_out(20, "trying to resolve mod = *%s* as builtin...\n", mod_name);
  if(puk_builtin.builtins != NULL)
    {
      puk_mod_builtin_vect_itor_t i;
      puk_vect_foreach(i, puk_mod_builtin, puk_builtin.builtins)
        {
          struct puk_mod_binary_s*const b = *i;
          if( (strcmp(b->name, mod_name) == 0) ||
              (strcmp(b->name, mod_name2) == 0) )
            {
              *mod = puk_mod_new(mod_name, "builtin");
              padico_string_t unit_name = padico_string_new();
              padico_string_printf(unit_name, "built-in unit %s", mod_name);
              struct puk_unit_s unit =
                {
                  .name            = padico_strdup(padico_string_get(unit_name)),
                  .mod             = *mod,
                  .driver_specific = (void*)b->funcs
                };
              padico_string_delete(unit_name);
              b->mod = *mod;
              puk_profile_mem_set_pvars(&b->profile, mod_name);
              puk_unit_vect_push_back(&(*mod)->units, unit);
              padico_out(20, "mod = *%s* found as builtin! n_deps = %d\n", mod_name, b->n_deps - 1);
              const struct puk_mod_dep_cell_s*d = b->auto_deps;
              while(d != NULL)
                {
                  if( (d->mod_name != NULL) &&
                      (strcmp(d->mod_name, mod_name) != 0) &&
                      (strcmp(d->mod_name, mod_name2) != 0) )
                    {
                      padico_modID_vect_push_back(&(*mod)->unresolved_deps, padico_strdup(d->mod_name));
                      padico_out(20, "mod = %s; auto dep = %s\n", mod_name, d->mod_name);
                    }
                  d = d->link;
                }
              int k;
              for(k = 0; k < b->n_deps; k++)
                {
                  if(b->deps[k] != NULL)
                    {
                      padico_out(20, "mod = %s; deps[%d] = %s\n", mod_name, k, b->deps[k]);
                      int found = 0;
                      padico_modID_vect_itor_t j;
                      puk_vect_foreach(j, padico_modID, &(*mod)->unresolved_deps)
                        {
                          if(strcmp(*j, b->deps[k]) == 0)
                            {
                              found = 1;
                              break;
                            }
                        }
                      if(!found)
                        {
                          padico_modID_vect_push_back(&(*mod)->unresolved_deps, padico_strdup(b->deps[k]));
                        }
                    }
                }
              goto out;
            }
        }
    }
  padico_out(20, "mod = %s not found as builtin.\n", mod_name);
 out:
  puk_lock_release();
  padico_string_delete(s);
  return;
}


/* *** driver: "builtin" *********************************** */

static padico_rc_t puk_unit_builtin_load(puk_unit_t unit)
{
  padico_rc_t rc = NULL;
  const struct puk_mod_funcs_s*funcs = unit->driver_specific;
  if(funcs->init)
    {
      int err = (*funcs->init)();
      if(err)
        {
          padico_warning("built-in module %s init error.\n", unit->mod->mod_name);
          rc = padico_rc_error("built-in module *%s* load error- module initialization rc=%d",
                               unit->mod->mod_name, err);
        }
    }
  return rc;
}

static padico_rc_t puk_unit_builtin_start(puk_unit_t unit, puk_job_t job __attribute__((unused)))
{
  padico_rc_t rc = NULL;
  const struct puk_mod_funcs_s*funcs = unit->driver_specific;
  if(funcs->run)
    {
      (*funcs->run)(0, NULL);
    }
  return rc;
}

static padico_rc_t puk_unit_builtin_unload(puk_unit_t unit)
{
  padico_rc_t rc = NULL;
  const struct puk_mod_funcs_s*funcs = unit->driver_specific;
  if(funcs->finalize)
    {
      (*funcs->finalize)();
    }
  unit->driver_specific = NULL;
  padico_free(unit->name);
  unit->name = NULL;
  return rc;
}
