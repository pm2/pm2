/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * header for functions to manage simulation.
 * not defined in libPadicoPuk but directly in puk-mult.c
 */

#ifndef PUK_MULT_H
#define PUK_MULT_H

/** get number of nodes in simulation */
extern int puk_mult_getsize(void);

/** publish a key/value, shared between all nodes in the process  */
extern void puk_mult_publish(const char*label, const char*value);

/** lookup for key/value */
extern char*puk_mult_lookup(const char*label);

/** barrier accross all nodes in the process */
extern void puk_mult_barrier(void);

#endif /* PUK_MULT_H */
