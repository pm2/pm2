#! /bin/sh

# input :
# $1 : Module name
# $2 : .a .o .so .opt ...

# ${mod_name}/meta.xml            -- meta-info
#            /lib/*.so            -- linked libs
#            /binary/*.{so|dylib} -- binary units
#            /class/*.class       -- java units
#            /bin/*.sh            -- sh units


usage() {
    cat <<EOF
Usage: padico-mkmod [options] [files] [driver specific flags]
Options:
  -o <module>           Place the output into module <module>
  -D<label>=<value>     Defines an attribute <label> with the value <value>
  -Xlinker              Ignored (for compatibility with other linkers)
  --driver=<driver>     Use Puk driver <driver> for the module
  --option=<opt>        Driver-specific option <opt> given to driver
  --requires <module>   Set a dependency on module <module>
  --quiet               Do not show progress information
  -h|--help             Display this information
Files:
  *.o *.a *.so          For binary driver
  *.class               For java/class driver
  *.sh                  For bin/sh driver
  <module>              For pkg and multi drivers
Driver specific flags:
  -L... -l...           For binary driver
  -cp                   For java/class driver

Note:
  -o is mandatory.
EOF
}

convert_abs() {
    firstchar=`echo "$1" | cut -c 1`
    if [ $firstchar = "/" ]; then
	echo "$1"
    else
	echo "`pwd`/$1"
    fi
}

check_exist_read() {
    if [ ! -r $1 ]; then
	echo "$1 is unreadable!"
	exit 2
    fi
}

check_exist_write() {
    if [ -w $1 ]; then
	rm -f $1
    fi
    touch $1
    if [ ! -w $1 ]; then
	echo "$1 not writable!"
	exit 3
    fi
}

get_soname() {
# $1 : abs lib.so path
    if [ "@binary_format@" = "elf" ]; then
      padico-readsoname $1
      if [ $? -eq 1 ]; then
        exit 1
      fi
    else
      echo "(null)"
      exit 1
    fi
}

search_in_path() {
# $1: file name
# $2: colon separated search path
    file="$1"
    search_path="$2"
    old_ifs=$IFS
    IFS=:
    for dir in $search_path; do
	if [ -r $dir/$file ]; then echo $dir/$file; exit 0; fi
    done
    IFS=$old_ifs
    exit 1
}

mkmod_findlib() {
# $1: library name
# $2: library path (colon separated)
    file="$1"
    libpath="$2"
    found="`search_in_path ${file} ${libpath}`"
    if [ "x$found" = "x" ]; then
	found="`env LD_LIBRARY_PATH=${libpath} @CC@ -print-file-name=${file}`"
    fi
    if [ -r "${found}" ]; then
	echo ${found}
    fi
}

show_progress() {
  if [ "x$quiet" = "xno" ]; then
    echo $*
  fi
}

# ### Globals
# ############

# tmp working directory
tmpdir="/tmp/padico-mkmod-tmp-${USER}-${$}"
mkdir -p ${tmpdir}
trap "rm -rf $tmpdir" 0

# ### Common vars (all drivers) filled-in by the command-line parsing
# module name
mod_name=
# required modules
requires=
# attributes
attrs=
# units of the module
units=

# ### special flags
# current driver
driver="binary:nowrap"
# don't show any info on output
quiet="no"
# options for driver
driver_options=

# ### Java driver only
# Classpath for java
cp_path=

# ### Binary driver only
# Current path for lib search (separator=':')
binary_ld_path=${LD_LIBRARY_PATH}
# object to put in main unit
binary_objs=
# static libs
binary_static_libs=
# static lib mode (convert, link)
binary_static_mode="convert"
# dynamic lib mod (import, link)
binary_dynamic_mode="link"
# dynamic libs to link with (without leading -l)
binary_dynamic_libs=
# flags for the link
ldflags="@LDFLAGS@"
# optimize flags (mandatory to have same LTO config for build & link)
if [ "x@ENABLE_OPTIMIZE_FLAG@" = "xyes" ]; then
    ldflags="${ldflags} @OPT_CFLAGS@ @OPT_LDFLAGS@"
fi


# ### Parse command-line
# #######################

while [ x"$1" != x ]; do
  case $1 in
    --option=*)
      option="`echo $1 | sed 's/--option=\(.*\)/\1/'`"
      driver_options="${driver_options}%${option}"
      shift
      continue;;
    # ### binary-related flags
    *@DYNLIB_EXT@)
      # *.so *.dylib -> copy as a unit
      lib=$1
      show_progress "Adding dynamic library as unit: $lib"
      check_exist_read ${lib}
      units="${units} ${lib}"
      shift
      continue;;
    *.o|*.c|*.cc|*.pic|*.lds)
      # *.o -> copy into the main unit
      file="$1"
      show_progress "Adding object file in main unit: $file"
      if [ "x${driver}" = "xAUTO" ]; then driver="binary:nowrap"; fi
      check_exist_read "$file"
      binary_objs="${binary_objs} `convert_abs $file`"
      shift
      continue;;
    *.lo)
      # *.lo -> copy the PIC libtoolized object into the main unit
      file="$1"
      pic_object=`/bin/bash -c ". $file ; echo \$pic_object"`
      show_progress "Adding libtoolized object file in main unit: $file ($pic_object)"
      if [ "x${driver}" = "xAUTO" ]; then driver="binary:nowrap"; fi
      check_exist_read "$pic_object"
      binary_objs="${binary_objs} `convert_abs $pic_object`"
      shift
      continue;;
    *.a)
      # *.a -> action according to $binary_static_mode
      file="$1"
      show_progress "Adding static library: $file"
      if [ "x${driver}" = "xAUTO" ]; then driver="binary:nowrap"; fi
      check_exist_read "$file"
      binary_static_libs="${binary_static_libs} `convert_abs $file`"
      shift
      continue;;
    -L*)
      # -L -> add to the ldpath for the _following_ libraries (-l)
      dir="`echo $1 | sed 's/-L\(.*\)/\1/'`"
      show_progress "Adding directory in search path: $dir"
      binary_ld_path="${dir}:${binary_ld_path}"
      ld_flags="${ld_flags} $1"
      shift
      continue;;
    -lPadicoPuk*|-lPukABI*)
      # Ignore -lPadicoPuk and -lPukABI that may be given by PM2.
      # We don't want to import another copy of our internal libs!
      show_progress "Ignore Puk & PukABI internal libs while linking module."
      shift
      continue;;
    -l*)
      # -l -> add the library. 1. dynlib: as unit; 2. static: as object in main unit
      lib="`echo $1 | sed 's/-l\(.*\)/\1/'`"
      show_progress "Looking up library: $lib"
      case ${binary_dynamic_mode} in
	  link)
	      binary_dynamic_libs="${binary_dynamic_libs} ${lib}"
	      ;;
	  import)
	      lib_so="`mkmod_findlib @DYNLIB_PREFIX@${lib}@DYNLIB_EXT@ ${binary_ld_path}`"
	      lib_a=""
	      if [ @ATOSO_OK@ = yes ]; then
		  lib_a="`mkmod_findlib  @DYNLIB_PREFIX@${lib}.a       ${binary_ld_path}`"
	      fi
	      if [ -n "${lib_so}" ]; then
		  show_progress "Found as dynamic library: $lib_so"
		  binary_dynamic_libs="${binary_dynamic_libs} ${lib}"
	      elif [ -n "${lib_a}" ]; then
		  show_progress "Found as static library: $lib_a"
		  binary_static_libs="${binary_static_libs} ${lib_a}"
	      else
		  echo "*** Library not found: ${lib}"
		  exit 1
	      fi
	      ;;
	  esac
      shift
      continue;;
    # ### Java-related flags
    -cp) # class directory for java/class driver
      file=$2
      show_progress "Adding java directory $file to classpath"
      cp_path="$file:$cp_path"
      shift 2
      continue;;
    # ### all drivers
    -D*=*)
      label="`echo $1 | sed 's/-D\([^=]*\)=\(.*\)/\1/'`"
      value="`echo $1 | sed 's/-D\([^=]*\)=\(.*\)/\2/'`"
      show_progress "Adding attribute ${label}=${value}"
      attrs="${attrs}  <attr label=\"${label}\">${value}</attr>\n"
      shift
      continue;;
    --driver=*)
      driver="`echo $1 | sed 's/--driver=\(.*\)/\1/'`"
      shift
      continue;;
    -o)
      mod_name="$2"
      shift 2
      continue;;
    --requires)
      requires="${requires} $2"
      show_progress "Module requires: $2"
      shift 2
      continue;;
    --quiet)
      quiet="yes"
      shift
      continue;;
    -Xlinker)
      echo "Ignore linker option: $1 $2"
      shift 2
      continue;;
    -Wl,-rpath,*)
      ldflags="${ldflags} $1"
      shift
      continue;;
    -pthread|-Wl*|-shared)
      echo "Ignore linker option: $1"
      shift
      continue;;
    -I*|-O*|-f*|-rdynamic|-g|-D*)
      echo "Ignore compiler option: $1"
      shift
      continue;;
    -h|--help)
      usage
      exit 0;;
    -*)
      echo "padico-mkmod: option $1 not recognized"
      usage
      exit 1;;
    *)
      # default: consider the argument as a unit
      units="${units} $1"
      shift
      continue;;
  esac
done

# ### check mandatory "-o"
if [ x{$mod_name} = x ]; then
   echo "${0}: Error: missing -o parameter" 1>&2
   usage
   exit 1
fi

# ### Driver options
# short syntax: --driver=<driver>[[:<opt1>],<opt2>]...
# long syntax:  --driver=<driver>[:<opt>...> --option=<long_opt>
case ${driver} in
  *:*)
    options="`echo ${driver}|sed -e 's/[^:]*:\(.*\)/\1/'|tr ',' '%'`"
    driver_name="`echo ${driver}|sed -e 's/\([^:]*\):.*/\1/'`"
    driver_options="${driver_options}%${options}"
  ;;
  *)
    driver_name=${driver}
  ;;
esac


# ### Generate the module
# ########################
mod_dir="${mod_name}"
mkdir -p ${mod_dir}

# ### Init .xml file
dest_xml=${mod_dir}/meta.xml
show_progress "Writing ${dest_xml}"

# ### Write headers
cat > ${dest_xml} <<EOF
<?xml version="1.0"?>
<!-- Padico module description. This file is `basename $dest_xml`
     Automaticaly generated by `basename $0`; do not edit!
     Built on: `uname -a`
  -->
<defmod name="$mod_name" driver="${driver_name}">
EOF

# ### Write attributes section
if [ "$attrs"x != x ]; then
  printf "$attrs" >> ${dest_xml} # printf is needed because of "\n" in attrs
fi

# ### Write "requires" section
for m in ${requires}; do
  echo "  <requires>$m</requires>"  >> ${dest_xml}
done

# ### Write "units" section and generates units
case $driver_name in

  # ### binary driver (binary)
  # ########################################################

  binary|binary:*)
    # ### Process driver options
    # when we reach here, all options are in 'driver_options' as a %-separated list.
    #
    OLDIFS="$IFS"
    IFS="%"
    for o in ${driver_options}; do
      case $o in
	"")
          # ignore useless separators
	;;
	wrap)
	  echo " - symbol wrapping enabled (method: @SYMBOL_WRAPPING_METHOD@)."
	  pre_ld="@padico_builddir@/PukABI/puk-abi-wrap.sh --cmd ${mod_dir}/lib"
	;;
        nowrap)
          show_progress " - symbol wrapping disabled."
	  pre_ld=
        ;;
        ldflags:*)
          ldflags="${ldflags} `echo $o|sed -e 's/^ldflags:\(.*\)/\1/'`"
          show_progress " - forcing linker flags: $ldflags"
        ;;
        c++)
          requires="${requires} LibStdC++"
          show_progress " - binary driver option: C++"
        ;;
        g2c)
          requires="${requires} LibG2C"
          show_progress " - binary driver option: Fortran support"
        ;;
        static-mode:*)
          binary_static_mode="`echo ${o} | sed 's/static-mode:\(.*\)/\1/'`"
          show_progress " - set static library mode to: ${binary_static_mode}"
        ;;
        *)
          echo "Unknown option \"$o\" for driver $driver_name"
          exit 1
        ;;
      esac
    done
    IFS="${OLDIFS}"
    # create repository
    lib_dir=${mod_dir}/lib
    mkdir -p ${lib_dir}
    binary_dir=${mod_dir}/binary
    mkdir -p ${binary_dir}
    # process dynamic libraries
    for l in ${binary_dynamic_libs}; do
	case ${binary_dynamic_mode} in
	    link)
		ld_flags="${ld_flags} -l${l}"
		;;
	    import)
		root_name="${l}"
		lib_abs="`mkmod_findlib @DYNLIB_PREFIX@${root_name}@DYNLIB_EXT@ ${binary_ld_path}`"
		lib_rel="`basename ${lib_abs}`"
		show_progress "- importing dynamic library as dependancy: ${lib_rel}"
		cp -f ${lib_abs} ${lib_dir}
                # sym-link aliases (existing symlinks pointing to lib)
		canonical=$( readlink -f ${lib_abs} )
		for a in ${lib_abs}.*; do
		    if [ -h ${a} ]; then
			c=$( readlink -f ${a} )
			sleep 2
			if [ ${c} = ${canonical} ]; then
			    a_base=`basename $a`
			    echo "- symlinking ${a_base} -> ${lib_rel}"
			    ( cd ${lib_dir} ; ln -s ${lib_rel} ${a_base} )
			fi
		    fi
		done
                # sym-link the so.name if available
		so_name=`get_soname ${lib_abs}`
		if [ "$so_name" != "(null)" -a "$so_name" != "$lib_rel" ]; then
		    show_progress " - symlinking $so_name -> $lib_rel (so name)"
		    ( cd $lib_dir ; [ -r $so_name ] && rm $so_name ; ln -s $lib_rel $so_name )
		fi
		echo "  <file>lib/${lib_rel}</file>" >> ${dest_xml}
		echo "    <!-- linked against dynamic library ${lib_rel} (so name: ${so_name}) -->" >> ${dest_xml}
		ld_flags="${ld_flags} -l${root_name}"
		;;
	esac
    done
    # static libraries
    case ${binary_static_mode} in
      convert)
        # convert each static archive into a dynamic shared object
        for l in ${binary_static_libs}; do
          lib_abs="${l}"
          lib_rel="`basename ${lib_abs}`"
          root_name=`echo ${lib_rel} | sed -e 's/@DYNLIB_PREFIX@\(.*\)\.a.*/\1/'`
	  target=@DYNLIB_PREFIX@${root_name}.A@DYNLIB_EXT@
	  show_progress "- converting static library ${lib_rel} into ${target}..."
          ( mkdir -p ${tmpdir}/${root_name} ; cd ${tmpdir}/${root_name} ; ar x ${lib_abs} ; rm -f __.* )
          @DYNLIB_LD@ @LDNOSTDLIB@ ${ldflags} -o ${lib_dir}/${target} ${tmpdir}/${root_name}/*
          echo "  <file>lib/${target}</file>" >> ${dest_xml}
          echo "    <!-- linked against ${target} converted from static library ${lib_rel} -->" >> ${dest_xml}
          ld_flags="${ld_flags} -l${root_name}.A"
        done
      ;;
      link)
        # link the main object with the .a -- unused symbols are lost!
        binary_objs="${binary_objs} ${binary_static_libs}"
      ;;
      extract)
        # extract the .o from the .a, then relink into a big main dynamic object file
        # all symbols are kept, namespace is flat; does not scale well for huge archives
        for l in ${binary_static_libs}; do
          lib_abs="${l}"
          lib_rel="`basename ${lib_abs}`"
          root_name=`echo ${lib_rel} | sed -e 's/@DYNLIB_PREFIX@\(.*\)\.a.*/\1/'`
	  show_progress "- extracting static library ${lib_rel}..."
          ( mkdir -p ${tmpdir}/${root_name} ; cd ${tmpdir}/${root_name} ; ar x ${lib_abs} ; rm -f __.* )
	  binary_objs="${binary_objs} ${tmpdir}/${root_name}/*"
        done
      ;;
      *)
        echo "Invalid static library mode: ${binary_static_mode}"
	exit 1
      ;;
    esac
    # copy units
    for u in ${units}; do
      u_name="`basename ${u}`"
      show_progress "- importing shared object ${u} as unit: ${u_name}"
      cp -f ${u} ${binary_dir}
      echo "  <unit>${u_name}</unit>"   >> ${dest_xml}
    done
    # module main unit
    dest_lib=${mod_name}@DSO_EXT@
    if [ "x${binary_objs}" = "x" -a "x${ld_flags}" != "x" ]; then
      show_progress "- generating dummy unit ${dest_lib}..."
      mod_c_name="`echo ${mod_name} | sed -e 's/[^0-9a-zA-Z_]/_/g'`"
      cat > ${tmpdir}/${mod_name}.c <<EOF
#include<Padico/Module.h>
PADICO_MODULE_DECLARE(${mod_c_name});
EOF
      padico-cc -c -o ${tmpdir}/${mod_name}.o ${tmpdir}/${mod_name}.c
      binary_objs="${binary_objs} ${tmpdir}/${mod_name}.o"
    fi
    if [ "x${binary_objs}" != "x" ]; then
      show_progress "- building main unit ${dest_lib}"
      ${pre_ld} @DSO_LD@ @LDNOSTDLIB@ ${binary_objs} ${ldflags} -o ${binary_dir}/${dest_lib} -L${lib_dir} ${ld_flags}
      err=$?
      if [ $err -ne 0 ] ; then exit $err; fi
      echo "  <unit>${dest_lib}</unit>" >> ${dest_xml}
    elif [ "x${units}" = "x" ]; then
      echo "Empty module!"
      exit 1
    fi
    ;;

  # ########################################################
  # ### binary driver -end-



  # ### driver="virtual"
  virtual)
    if [ "x${units}" != "x" ]; then
      echo "*** virtual module not empty!"
      exit 1
    fi
    ;;

  # ### driver="pkg"
  pkg)
    for u in $units; do
      echo "  <unit>${u}</unit>"   >> ${dest_xml}
    done
    ;;

  # ### driver="multi"
  multi)
    for u in $units; do
      echo "  <unit>${u}</unit>"   >> ${dest_xml}
    done
    ;;

  # ### Java driver
  java/class)
    for u in $units; do
      file=`echo $u | sed 's/[.]/\//g' `
      dir=`dirname $file`
      class=`basename $file`".class"

      dest=${mod_dir}/classes/$dir
      mkdir -p $dest

      p=`search_in_path $dir/$class $cp_path`
      if [ $? -eq 0 ]; then
	 cp $p $dest/
      else
	 echo "Unable to find $class in $cp_path"
         exit -1
      fi

      echo "  <unit>${u}</unit>"   >> ${dest_xml}
    done
    ;;

  # ### sh driver
  bin/sh)
    for u in $units; do
      dest=${mod_dir}/sh
      mkdir -p $dest

     if [ -e $u ]; then
	 cp $u $dest/
	 chmod +x $dest/$u
      else
	 echo "Unable to find file $u"
         exit -1
      fi

      echo "  <unit>${u}</unit>"   >> ${dest_xml}
    done
    ;;

  # ### unknown
  *)
    echo "Unknown driver: ${driver}"
    exit 1
    ;;
esac

echo "</defmod>" >> ${dest_xml}

# clean directory that may be empty
rmdir "${mod_dir}/lib" 2> /dev/null
rmdir "${mod_dir}/binary" 2> /dev/null

if [ $quiet = "no" ]; then
  echo "-------------------------------------------"
  cat ${dest_xml}
  echo "-------------------------------------------"
fi

# ### Copy all the files into the real repository
show_progress "- Module ${mod_name} ok."

