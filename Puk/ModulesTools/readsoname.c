/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "puk_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>

#ifdef HAVE_ELF_H
#include <elf.h>
#endif

#ifdef sun
#include <sys/elf.h>
#include <sys/link.h>
#include <sys/machelf.h>
#endif

#if ( SIZEOF_LONG == 4)
#  define ElfXX Elf32
#  define ElfXX_Ehdr  Elf32_Ehdr
#  define ElfXX_Shdr  Elf32_Shdr
#  define ElfXX_Phdr  Elf32_Phdr
#  define ElfXX_Addr  Elf32_Addr
#  define ElfXX_Word  Elf32_Word
#  define ElfXX_Sword Elf32_Sword
#  define ElfXX_Sym   Elf32_Sym
#  define ElfXX_Dyn   Elf32_Dyn
#  define ELFXX_ST_TYPE(I) ELF32_ST_TYPE(I)
#elif ( SIZEOF_LONG == 8)
#  define ElfXX Elf64
#  define ElfXX_Ehdr  Elf64_Ehdr
#  define ElfXX_Shdr  Elf64_Shdr
#  define ElfXX_Phdr  Elf64_Phdr
#  define ElfXX_Addr  Elf64_Addr
#  define ElfXX_Word  Elf64_Word
#  define ElfXX_Sword Elf64_Sword
#  define ElfXX_Sym   Elf64_Sym
#  define ElfXX_Dyn   Elf64_Dyn
#  define ELFXX_ST_TYPE(I) ELF64_ST_TYPE(I)
#else
#  error "SIZEOF_LONG undefined."
#endif

struct needed_tab
{
  char *soname;
  int type;
};

char *readsoname(char *name, FILE *infile) {
  ElfXX_Ehdr  *epnt;
  ElfXX_Phdr *ppnt;
  unsigned int i;
  char *header;
  unsigned int dynamic_addr = 0;
  unsigned int dynamic_size = 0;
  int strtab_val = 0;
  int needed_val;
  int loadaddr = -1;
  ElfXX_Dyn *dpnt;
  struct stat st;
  char *needed;
  char *soname = NULL;


  if (fstat(fileno(infile), &st))
    return NULL;
  header = mmap(0, st.st_size, PROT_READ, MAP_SHARED, fileno(infile), 0);
  if (header == (void*)-1)
    return NULL;

  epnt = (ElfXX_Ehdr *)header;
  if ((intptr_t)(epnt+1) >
      (intptr_t)(header + st.st_size))
    goto skip;

  ppnt = (ElfXX_Phdr *)&header[epnt->e_phoff];
  if ((intptr_t)ppnt < (intptr_t)header ||
      (intptr_t)(ppnt+epnt->e_phnum) > (intptr_t)(header + st.st_size))
    goto skip;

  for(i = 0; i < epnt->e_phnum; i++)
    {
      if (loadaddr == -1 && ppnt->p_type == PT_LOAD)
        loadaddr = (ppnt->p_vaddr & 0xfffff000) - (ppnt->p_offset & 0xfffff000);
      if(ppnt->p_type == PT_DYNAMIC)
        {
          dynamic_addr = ppnt->p_offset;
          dynamic_size = ppnt->p_filesz;
        };
      ppnt++;
    };

  dpnt = (ElfXX_Dyn *) &header[dynamic_addr];
  dynamic_size = dynamic_size / sizeof(ElfXX_Dyn);
  if ((intptr_t)dpnt < (intptr_t)header ||
      (intptr_t)(dpnt+dynamic_size) > (intptr_t)(header + st.st_size))
    goto skip;

  for (i = 0; i < dynamic_size; i++)
    {
      if (dpnt->d_tag == DT_STRTAB)
        strtab_val = dpnt->d_un.d_val;
      dpnt++;
    };

  if (!strtab_val)
    goto skip;

  dpnt = (ElfXX_Dyn *) &header[dynamic_addr];
  for (i = 0; i < dynamic_size; i++)
    {
      if (dpnt->d_tag == DT_SONAME || dpnt->d_tag == DT_NEEDED)
        {
          needed_val = dpnt->d_un.d_val;
          if (needed_val + strtab_val - loadaddr >= 0 ||
              needed_val + strtab_val - loadaddr < st.st_size)
            {
              needed = (char *) (header + strtab_val + needed_val - loadaddr );

              if (dpnt->d_tag == DT_SONAME) {
                soname = strdup(needed);
              }

            }
        }
      dpnt++;
    };


 skip:
  munmap(header, st.st_size);

  return soname;
}

int main(int argc, char *argv[]) {
  char null_str[]="(null)";
  FILE *f;
  char *name=argv[1];
  char *n;
  if ((f=fopen(name, "r")) == NULL) {
    fprintf(stderr,"# %s: file not found %s\n", argv[0], name);
    exit(1);
  }
  n=readsoname(name,f);
  if (n==NULL) {
    n=null_str;
  }
  printf("%s\n",n);
  return 0;
}
