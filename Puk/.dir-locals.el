;; unified emacs style for Puk

((c-mode . ((c-file-style . "gnu")
	    (c-basic-offset . 2)
	    (tab-width . 8)
	    (indent-tabs-mode . nil))))
