/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief interface for timing
 */

#ifndef PUK_TIMING_H
#define PUK_TIMING_H

#include "Puk-config.h"
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#if defined(MCKERNEL) && defined (PUK_HAVE_CLOCK_GETTIME)
#  undef PUK_HAVE_CLOCK_GETTIME
#endif

#if defined(PUK_SIMGRID)
#  include <simgrid/engine.h>
#  include <simgrid/actor.h>
#  define PUK_TIMING_USE_SIMGRID
#elif defined(PUK_HAVE_CLOCK_GETTIME)
#  define PUK_TIMING_USE_CLOCK_GETTIME
#elif defined (PUK_HAVE_MACH_ABSOLUTE_TIME)
#  define PUK_TIMING_USE_MACH_ABSOLUTE_TIME
#elif defined(__x86_64__)
#  define PUK_TIMING_USE_RDTSC
#else
#  define PUK_TIMING_USE_GETTIMEOFDAY
#endif



/* ** timing with clock_gettime **************************** */

#if defined(PUK_HAVE_CLOCK_GETTIME)

typedef struct timespec puk_timing_clock_gettime_tick_t;

#ifdef CLOCK_MONOTONIC_RAW
#  define PUK_CLOCK_TYPE CLOCK_MONOTONIC_RAW
#else
#  define PUK_CLOCK_TYPE CLOCK_MONOTONIC
#endif

#define PUK_TIMING_CLOCK_GETTIME_GET_TICK(t)    \
  clock_gettime(PUK_CLOCK_TYPE, &(t))
#define PUK_TIMING_CLOCK_GETTIME_TICK_RAW_DIFF(start, end)      \
  puk_timing_clock_gettime_tick_raw_diff(&(start), &(end))

/** compute a tick diff from 2 absolute ticks */
static inline puk_timing_clock_gettime_tick_t
puk_timing_clock_gettime_tick_raw_diff(const puk_timing_clock_gettime_tick_t*const t1,
                                       const puk_timing_clock_gettime_tick_t*const t2)
{
  puk_timing_clock_gettime_tick_t diff;
  if(t2->tv_nsec > t1->tv_nsec)
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec;
      diff.tv_nsec = t2->tv_nsec - t1->tv_nsec;
    }
  else
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec - 1;
      diff.tv_nsec = t2->tv_nsec - t1->tv_nsec + 1000000000L;
    }
  return diff;
}

/** converts a tick diff to microseconds */
static inline double puk_timing_clock_gettime_tick2usec_raw(puk_timing_clock_gettime_tick_t t)
{
  double raw_delay = 1000000.0 * t.tv_sec + t.tv_nsec / 1000.0;
  return raw_delay;
}

#endif

/* ** timing in simgrid ************************************ */

#if defined(PUK_SIMGRID)

/** tick type for simgrid: directly use usecs as ticks */
typedef double puk_timing_simgrid_tick_t;
#define PUK_TIMING_SIMGRID_GET_TICK(t)                  \
  do { t = simgrid_get_clock() * 1000000.0; } while(0)
#define PUK_TIMING_SIMGRID_TICK_RAW_DIFF(start, end)    \
  (end - start)

static inline double puk_timing_simgrid_tick2usec_raw(puk_timing_simgrid_tick_t t)
{
  return t;
}

#endif /* PUK_SIMGRID */

/* ** timing with mach_absolute_time *********************** */

#if defined(PUK_HAVE_MACH_ABSOLUTE_TIME)

#include <mach/mach_time.h>

typedef long long puk_timing_mach_tick_t;

#define PUK_TIMING_MACH_GET_TICK(t)             \
  t = mach_absolute_time()
#define PUK_TIMING_MACH_TICK_RAW_DIFF(start, end)       \
  ((end) - (start))

static inline double puk_timing_mach_tick2usec_raw(puk_timing_mach_tick_t t)
{
  mach_timebase_info_data_t info;
  mach_timebase_info(&info);
  double raw_delay = ((double)t * info.numer / info.denom / 1000.0);
  return raw_delay;
}

#endif


/* ** timing with rdtsc ************************************ */

#if defined(__x86_64__)

typedef unsigned long long puk_timing_rdtsc_tick_t;

/* old 32 bits x86 code */
/*
#define PUK_GET_TICK(t)                                 \
  __asm__ volatile("rdtsc" : "=A" (t))
*/
#define PUK_TIMING_RDTSC_GET_TICK(t)                            \
  do {                                                          \
    unsigned int __a,__d;                                       \
    asm volatile("rdtsc" : "=a" (__a), "=d" (__d));             \
    (t) = ((unsigned long)__a) | (((unsigned long)__d)<<32);    \
  } while(0)
#define PUK_TIMING_RDTSC_TICK_RAW_DIFF(t1, t2)          \
  ((t2) - (t1))

static inline double puk_timing_rdtsc_tick2usec_raw(puk_timing_rdtsc_tick_t t)
{
  static double scale = -1.0;
  if(scale < 0)
    {
      puk_timing_rdtsc_tick_t t1, t2;
      struct timespec ts = { 0, 100000000 }; /* 100 ms */
      PUK_TIMING_RDTSC_GET_TICK(t1);
      nanosleep(&ts, NULL);
      PUK_TIMING_RDTSC_GET_TICK(t2);
      scale = (ts.tv_sec * 1e6 + ts.tv_nsec / 1e3) / (double)(PUK_TIMING_RDTSC_TICK_RAW_DIFF(t1, t2));
    }
  double raw_delay = (t * scale);
  return raw_delay;
}

#endif


/* ** timing with gettimeofday ***************************** */

typedef struct timeval puk_timing_gettimeofday_tick_t;

#define PUK_TIMING_GETTIMEOFDAY_GET_TICK(t)     \
  gettimeofday(&(t), NULL)
#define PUK_TIMING_GETTIMEOFDAY_TICK_RAW_DIFF(start, end)       \
  puk_timing_gettimeofday_tick_raw_diff(&(start), &(end))

static inline puk_timing_gettimeofday_tick_t
puk_timing_gettimeofday_tick_raw_diff(const puk_timing_gettimeofday_tick_t*const t1,
                                      const puk_timing_gettimeofday_tick_t*const t2)
{
  puk_timing_gettimeofday_tick_t diff;
  if(t2->tv_usec > t1->tv_usec)
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec;
      diff.tv_usec = t2->tv_usec - t1->tv_usec;
    }
  else
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec - 1;
      diff.tv_usec = t2->tv_usec - t1->tv_usec + 1000000L;
    }
  return diff;
}

static inline double puk_timing_gettimeofday_tick2usec_raw(puk_timing_gettimeofday_tick_t t)
{
  double raw_delay = 1000000.0 *t.tv_sec + t.tv_usec;
  return raw_delay;
}


/* ** HPET counter ***************************************** */

/* HPET are never used automatically by default; they must
 * be used explicitely, if available. HPET is slow and coarser
 * grain that rdtsc, but it's _stable_. Usually on production
 * machines, permissions on /dev/hpet allow only root to access
 * the device */

#if defined(__gnu_linux__) && (defined(__i386__) || defined(__x86_64__))
#define HPET_SUPPORT
#define HPET_MMAP_SIZE                  1024
#define HPET_CAPS                       0x000
#define HPET_PERIOD                     0x004
#define HPET_COUNTER                    0x0f0
#define HPET_CAPS_COUNTER_64BIT         (1 << 13)
#if defined(__x86_64__)
typedef uint64_t puk_hpet_counter_t;
#else
typedef uint32_t puk_hpet_counter_t;
#endif

typedef double puk_timing_hpet_tick_t;

static inline double puk_timing_hpet_get_microseconds(void)
{
  static unsigned char*hpet_ptr = NULL;
  static uint32_t hpet_period; /* period length in femto secs */
  static uint64_t hpet_offset = 0;
  static uint64_t hpet_wrap;
  static puk_hpet_counter_t hpet_previous = 0;

  static int init_done = 0;
  static long double hpet_orig = 0.0;

  if(!init_done)
    {
      uint32_t hpet_caps;
      int hpet_fd = open ("/dev/hpet", O_RDONLY);
      if(hpet_fd < 0)
        {
          fprintf(stderr, "This system has no accessible HPET device (%s)", strerror (errno));
          abort();
          return -1;
        }
      hpet_ptr = (unsigned char*)mmap(NULL, HPET_MMAP_SIZE, PROT_READ, MAP_SHARED, hpet_fd, 0);
      if(hpet_ptr == MAP_FAILED)
        {
          fprintf(stderr, "This system has no mappable HPET device (%s)", strerror (errno));
          close (hpet_fd);
          abort();
          return -1;
        }

      /* this assumes period to be constant. if needed,
       * it can be moved to the clock access function
       */
      hpet_period = *((uint32_t*)(hpet_ptr + HPET_PERIOD));
      hpet_caps = *((uint32_t*)(hpet_ptr + HPET_CAPS));
      hpet_wrap = ((hpet_caps & HPET_CAPS_COUNTER_64BIT) &&
                   (sizeof(puk_hpet_counter_t) == sizeof(uint64_t))) ?
        0 : ((uint64_t)1 << 32);
      fprintf(stderr, "# Puk: HPET period = %u; wrap = %" PRIu64 "; caps = 0x%x (64 bits = %d); sizeof counter = %zu\n",
              hpet_period, hpet_wrap, hpet_caps, !!(hpet_caps & HPET_CAPS_COUNTER_64BIT), sizeof(puk_hpet_counter_t));
    }

  const puk_hpet_counter_t hpet_counter = *((puk_hpet_counter_t*)(hpet_ptr + HPET_COUNTER));
  if(hpet_counter < hpet_previous)
    {
      if(hpet_wrap == 0)
        {
          hpet_wrap = ((uint64_t)1 << 32);
        }
      hpet_offset += hpet_wrap;
      fprintf(stderr, "# Puk: hpet wrap; prev = %lu; counter = %lu; wrap = %" PRIu64 "\n",
              (unsigned long)hpet_previous, (unsigned long)hpet_counter, hpet_wrap);
    }
  hpet_previous = hpet_counter;
  const long double hpet_time = (long double)(hpet_offset + hpet_counter) *
    (long double)hpet_period * (long double)1e-9;
  if(!init_done)
    {
      hpet_orig = hpet_time;
      init_done = 1;
    }
  return (double)(hpet_time + 0.5 - hpet_orig) ;
}

#endif /* defined(__gnu_linux__) && (__i386__ || __x86_64__) */



/* ** common interface ************************************* */

#if defined(PUK_TIMING_USE_SIMGRID)
#  define PUK_TIMING_METHOD  "simgrid"
#  define puk_tick_t         puk_timing_simgrid_tick_t
#  define PUK_GET_TICK       PUK_TIMING_SIMGRID_GET_TICK
#  define PUK_TICK_RAW_DIFF  PUK_TIMING_SIMGRID_TICK_RAW_DIFF
#  define puk_tick2usec_raw  puk_timing_simgrid_tick2usec_raw
#elif defined(PUK_TIMING_USE_CLOCK_GETTIME)
#  define PUK_TIMING_METHOD  "clock_gettime"
#  define puk_tick_t         puk_timing_clock_gettime_tick_t
#  define PUK_GET_TICK       PUK_TIMING_CLOCK_GETTIME_GET_TICK
#  define PUK_TICK_RAW_DIFF  PUK_TIMING_CLOCK_GETTIME_TICK_RAW_DIFF
#  define puk_tick2usec_raw  puk_timing_clock_gettime_tick2usec_raw
#elif defined(PUK_TIMING_USE_MACH_ABSOLUTE_TIME)
#  define PUK_TIMING_METHOD  "mach_absolute_time"
#  define puk_tick_t         puk_timing_mach_tick_t
#  define PUK_GET_TICK       PUK_TIMING_MACH_GET_TICK
#  define PUK_TICK_RAW_DIFF  PUK_TIMING_MACH_TICK_RAW_DIFF
#  define puk_tick2usec_raw  puk_timing_mach_tick2usec_raw
#elif defined(PUK_TIMING_USE_RDTSC)
#  define PUK_TIMING_METHOD  "rdtsc"
#  define puk_tick_t         puk_timing_rdtsc_tick_t
#  define PUK_GET_TICK       PUK_TIMING_RDTSC_GET_TICK
#  define PUK_TICK_RAW_DIFF  PUK_TIMING_RDTSC_TICK_RAW_DIFF
#  define puk_tick2usec_raw  puk_timing_rdtsc_tick2usec_raw
#elif defined(PUK_TIMING_USE_GETTIMEOFDAY)
#  define PUK_TIMING_METHOD  "gettimeofday"
#  define puk_tick_t         puk_timing_gettimeofday_tick_t
#  define PUK_GET_TICK       PUK_TIMING_GETTIMEOFDAY_GET_TICK
#  define PUK_TICK_RAW_DIFF  PUK_TIMING_GETTIMEOFDAY_TICK_RAW_DIFF
#  define puk_tick2usec_raw  puk_timing_gettimeofday_tick2usec_raw
#else
#  error "no timing method"
#endif

extern double puk_ticks2delay(const puk_tick_t *t1, const puk_tick_t *t2);

extern double puk_tick2usec(puk_tick_t t);

/** compute delay between two ticks, result given in microsecond as a double */
#define PUK_TIMING_DELAY(start, end) puk_ticks2delay(&(start), &(end))

/** get absolute timestamp, with an arbitrary origin */
extern double puk_timing_stamp(void);

/* ** compatibility layer for padico_timing_* API ********** */

typedef puk_tick_t padico_timing_t;
static inline void padico_timing_get_tick(padico_timing_t*t)
{
  PUK_GET_TICK(*t);
}
static inline double padico_timing_diff_usec(padico_timing_t*t1, padico_timing_t*t2)
{
  return PUK_TIMING_DELAY(*t1, *t2);
}

/* ** sleeping ********************************************* */

#ifdef PUK_SIMGRID
static inline void puk_sleep(double duration_seconds)
{
  sg_actor_sleep_for(duration_seconds);
}
#define puk_usleep(T) puk_sleep((double)(T) / 1000000.0)
static inline int puk_nanosleep(const struct timespec*req, struct timespec*rem)
{
  const double seconds = (double)req->tv_sec + (double)req->tv_nsec / 1000000000.0;
  sg_actor_sleep_for(seconds);
  return 0;
}
#else /* PUK_SIMGRID */
#define puk_usleep usleep
#define puk_sleep sleep
#define puk_nanosleep nanosleep
#endif /* PUK_SIMGRID */

#endif /* PUK_TIMING_H */
