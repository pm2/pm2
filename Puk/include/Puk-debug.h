/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk traces and various debugging tools
 * It does not depend on any other Puk header so as to be included early.
 */

/** @defgroup PukMessage API: Puk message -- reporting error, trace, logs.
 * @ingroup Puk
 */


/* *** Puk message *************************************** */

/** @addtogroup PukMessage
 * @{ */

/** verbosity levels */
enum puk_verbosity_level_e
  {
    puk_verbose_always         = -1, /**< message always shown (asked by the user, output of the code, etc.) */
    puk_verbose_fatal          = 0,  /**< error is fatal, cannot continue */
    puk_verbose_critical       = 1,  /**< possible to continue, but behavior is probably wrong */
    puk_verbose_warning        = 5,  /**< warn the user about something possibly wrong */
    puk_verbose_notice         = 8,  /**< something noticeable happened */
    puk_verbose_info           = 10, /**< info, usually at init */
    puk_verbose_trace_default  = 50, /**< default level for traces */
    puk_verbose_trace_detailed = 60, /**< detailed traces */
    puk_verbose_trace_polling  = 95, /**< traces including polling (very verbose) */
    puk_verbose_max            = 99
  };

/** @internal */
void padico_out_func(int level,
                     const char*mod_name,
                     const char*function,
                     const char*file,
                     int line,
                     const char*format, ...) __attribute__ ((format (printf, 6, 7)));

enum puk_verbosity_level_e puk_verbose_level(void);

/** @internal */
static inline void padico_trace_empty(int level __attribute__((unused)), char*format __attribute__((unused)), ...)
{
}

typedef void*(*puk_trace_plugin_t)(void);
void puk_trace_setplugin(puk_trace_plugin_t plugin);

void puk_trace_setnameext(const char*name_ext);

/* ** action for trace filters */
#define PUK_TRACE_ACTION_DEFAULT 1 /**< apply global policy */
#define PUK_TRACE_ACTION_HIDE    2 /**< hide this trace */
#define PUK_TRACE_ACTION_SHOW    3 /**< show this trace */

/** add a filter to the current trace policy */
void puk_trace_add_filter(int action, int level, const char*mod, const char*func, const char*file, int line);

/** dump the current trace_policy */
char*puk_trace_policy_serialize(void);

#ifdef PUK_DEBUG
#define PUK_TRACE_MAX_LEVEL 99
#else
/** maximum level kept at compilation */
#define PUK_TRACE_MAX_LEVEL 10
#endif
/** default level displayed if no policy is defined */
#define PUK_TRACE_DEFAULT_LEVEL 5

/** @hideinitializer */
#define padico_out(level, format, ...)                                  \
  do { if(level <= PUK_TRACE_MAX_LEVEL)                                 \
      padico_out_func(level, padico_module_self_name(), __FUNCTION__, __FILE__, __LINE__, format, ## __VA_ARGS__);  } while(0)

/** display a trace message with the default verbosity level for traces */
#define padico_trace(format, ...) \
  padico_out(puk_verbose_trace_default, format, ## __VA_ARGS__)

/** display a message with the verbosity level 'WARNING' */
#define padico_warning(format, ...) \
  padico_out(puk_verbose_warning, format, ## __VA_ARGS__)

/** display a message with the verbosity level 'INFO' */
#define padico_print(format, ...) \
  padico_out(puk_verbose_info, format, ## __VA_ARGS__)

/** Abort the processus with a fatal error.
 * @param format Error message in "printf" format
 */
#define padico_fatal(format, ...)                                       \
  padico_fatal_func(padico_module_self_name(), __FUNCTION__, __FILE__, __LINE__, format, ## __VA_ARGS__)


/** @internal */
void padico_fatal_func(const char*mod_name,
                       const char*function,
                       const char*file,
                       int line,
                       const char*format, ...) __attribute__ ((noreturn, format (printf, 5, 6)));



/** Use this function when gdb needs an already loaded function for a breakpoint
 */
void puk_trace_breakpoint(void);

void puk_debug_gdb_attach(void);

/** Dump backtrace from all threads
 */
void puk_debug_gdb_backtrace(void);

void puk_notimplemented(void);


/** @} */ /* group PukMessage */
