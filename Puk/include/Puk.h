/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 ** @brief Contains basic types and functions common between all modules.
 * @ingroup Puk
 */

#ifndef PUK_H
#define PUK_H

#ifdef PACKAGE_BUGREPORT
#undef PACKAGE_BUGREPORT
#endif

#ifdef PACKAGE_NAME
#undef PACKAGE_NAME
#endif

#ifdef PACKAGE_STRING
#undef PACKAGE_STRING
#endif

#ifdef PACKAGE_TARNAME
#undef PACKAGE_TARNAME
#endif

#ifdef PACKAGE_VERSION
#undef PACKAGE_VERSION
#endif

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "Puk-config.h"
#include "Puk-types.h"
#include "Puk-module-local.h"
#include "Puk-debug.h"
#include "Puk-base.h"
#include "Puk-mem.h"
#include "Puk-string.h"
#include "Puk-opt.h"
#include "Puk-monitor.h"
#include "Puk-list.h"
#include "Puk-vect.h"
#include "Puk-hashtable.h"
#include "Puk-profile.h"
#include "Puk-lfstack.h"
#include "Puk-lfqueue.h"
#include "Puk-allocator.h"
#include "Puk-prio.h"
#include "Puk-components.h"
#include "Puk-mod.h"
#include "Puk-xml.h"
#include "Puk-checksum.h"
#include "Puk-timing.h"
#include "Puk-base64.h"
#include "Puk-trees.h"
#include "Module.h"

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* PUK_H */
