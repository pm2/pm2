/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 ** @brief Base types and Puk management code.
 * @ingroup Puk
 */

#include <stddef.h>
#include <malloc.h>
#include <string.h>


/* *** Typing ********************************************** */

/* use these macros to display size_t and ssize_t in
 * a portable (32/64 bits) way */
#define puk_ulong(N) ((unsigned long)(N))
#define puk_slong(N) ((long)(N))
/** convert a long to pointer */
#define puk_long2ptr(N) ((void*)(long)N)
/** convert a pointer to a long */
#define puk_ptr2long(P) ((long)(P))


/* *** Linking ********************************************* */

#define __PUK_SYM_INTERNAL __attribute__ ((visibility("internal")))


  /** Initializes Puk */
  extern void padico_puk_init(void);

  /** add the given path to Puk global search path */
  extern void padico_puk_add_path(const char*path);

  /** Start Puk bootstrap module */
  extern void padico_puk_start(void);

  /** Shutdowns Puk */
  extern void padico_puk_shutdown(void);

  /** Tests whether Puk is already initialized */
  extern int padico_puk_initialized(void);

  /** Tests whether Puk is finalized (or shutdown in progress) */
  extern int padico_puk_finalized(void);


  /* ** register locking handlers for Puk shared structures */
  void puk_lock_set_handlers(void(*_acquire)(void), void(*_release)(void));



/** check that VAR is of type TYPE */
#define PUK_CHECK_TYPE(TYPE, VAR)                                       \
  { typedef void (*type_t)(TYPE);                                       \
    type_t tmp = (type_t)0;                                             \
    if(0) tmp(VAR); }

  /** Sets the value of an environment variable. */
  void padico_setenv(const char *var, const char *val);

  /** Returns the value of an environment variable. */
  const char*padico_getenv(const char*var);

  /** Remove padico environment variables*/
  void padico_cleanenv(void);

  /** whether we are running on mckernel */
  int puk_mckernel(void);

  const char*padico_hostname(void);

  struct in_addr puk_inet_getaddr(void);

  char*puk_inet_get_ifname_byaddr(struct in_addr*in_addr);

  int puk_inet_get_link_speed(const char*ifname);

/** return the library name where Puk resides */
const char*puk_getlibfile(void);

/** get prefix added to symbols for simulation; NULL if none */
const char*puk_getsymprefix(void);

/** get the rank of local instance in mult mode; return value is in the range 0..N-1 */
int puk_mult_getnum(void);

/** acquire exclusive mode in mult; weak symbol always declared, only available in mult mode */
void puk_mult_exclusive_lock(void) __attribute__((weak));
extern void puk_mult_exclusive_lock(void);

/** release exclusive mode in mult; weak symbol always declared, only available in mult mode */
void puk_mult_exclusive_unlock(void) __attribute__((weak));
extern void puk_mult_exclusive_unlock(void);
