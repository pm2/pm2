/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk hashtables
 * @ingroup Puk
 */

#include "Puk.h"
#ifdef PUK_HAVE_STDINT_H
#include <stdint.h>
#else
#include <sys/types.h>
#endif
#include <assert.h>
#include <string.h>

#ifndef PUK_HASHTABLE_H
#define PUK_HASHTABLE_H

typedef uint32_t (*puk_hash_hfunc_t)(const void*key);
typedef int      (*puk_hash_eqfunc_t)(const void*key1, const void*key2);

/* ********************************************************* */
/* *** hashtable */

typedef struct puk_hashtable_s*puk_hashtable_t;

/** allocate a new hashtable object, using given hash and eq functions  */
puk_hashtable_t puk_hashtable_new(puk_hash_hfunc_t hash_func, puk_hash_eqfunc_t eq_func);
/** delete a hashtable- destructor is called on each object; NULL for no destructor */
void puk_hashtable_delete(puk_hashtable_t h, void (*destructor)(void*key, void*data));
/** insert <key,data> entry into the hashtable; overrides value if an ehtry with the same key already exists */
void puk_hashtable_insert(puk_hashtable_t h, const void*key, void*data);
/** returns value associated with key, NULL if not found (cannot distinguish between non-existent value and data==NULL) */
void*puk_hashtable_lookup(puk_hashtable_t h, const void*key);
/** returns <key,value> associated with given key, outkey is set to NULL if key not found  */
void puk_hashtable_lookup2(puk_hashtable_t h, const void*key, void*outkey, void*data);
/** probes whether an entry with given key exists in the hashtable */
int  puk_hashtable_probe(puk_hashtable_t h, const void*key);
/** remove entry with given key from hashtable; does nothing if key not found */
void puk_hashtable_remove(puk_hashtable_t h, const void*key);
/** returns the number of elements in the hashtable */
size_t puk_hashtable_size(puk_hashtable_t h);

/* ********************************************************* */
/* *** hashtable enumerator */

struct puk_hashtable_enumerator_s
{
  puk_hashtable_t h;
  struct puk_hash_entry_s*current;
  ssize_t i;
};
typedef struct puk_hashtable_enumerator_s*puk_hashtable_enumerator_t;

void puk_hashtable_enumerator_init(puk_hashtable_enumerator_t e, puk_hashtable_t h);
void puk_hashtable_enumerator_next2(puk_hashtable_enumerator_t e, void*key, void*data);

static inline puk_hashtable_enumerator_t puk_hashtable_enumerator_new(puk_hashtable_t h)
{
  puk_hashtable_enumerator_t e = (struct puk_hashtable_enumerator_s*)
    padico_malloc(sizeof(struct puk_hashtable_enumerator_s));
  puk_hashtable_enumerator_init(e, h);
  return e;
}
static inline void puk_hashtable_enumerator_delete(puk_hashtable_enumerator_t e)
{
  padico_free(e);
}

static inline const void*puk_hashtable_enumerator_next_key(puk_hashtable_enumerator_t e)
{
  const void*_key;
  puk_hashtable_enumerator_next2(e, &_key, NULL);
  return _key;
}
static inline void*puk_hashtable_enumerator_next_data(puk_hashtable_enumerator_t e)
{
  void*_data;
  puk_hashtable_enumerator_next2(e, NULL, &_data);
  return _data;
}

/* ********************************************************* */
/* *** hashing functions */

uint32_t puk_hash_oneatatime(const unsigned char*key, int len);
int      puk_hash_string_eq(const void*key1, const void*key2);
uint32_t puk_hash_string(const void*key);
int      puk_hash_pointer_eq(const void*key1, const void*key2);
uint32_t puk_hash_pointer(const void*key);
int      puk_hash_int_eq(const void*key1, const void*key2);
uint32_t puk_hash_int(const void*key);

/** creates a new hashtable with keys as strings */
#define puk_hashtable_new_string() \
  puk_hashtable_new(&puk_hash_string, &puk_hash_string_eq)

/** creates a new hashtable with keys as pointers */
#define puk_hashtable_new_ptr() \
  puk_hashtable_new(&puk_hash_pointer, &puk_hash_pointer_eq)

/** creates a new hashtable with keys as integers */
#define puk_hashtable_new_int() \
  puk_hashtable_new(&puk_hash_int, &puk_hash_int_eq)


static inline uint32_t puk_hash_string_default_hash(const char*s)
{
  return puk_hash_string((void*)s);
}
static inline int puk_hash_string_default_eq(const char*s1, const char*s2)
{
  return !strcmp(s1, s2);
}

static inline int puk_hash_pointer_default_eq(const void*key1, const void*key2)
{
  return (key1 == key2);
}
static inline uint32_t puk_hash_pointer_default_hash(const void*key)
{
  return puk_hash_pointer(key);
}

static inline int puk_hash_int_default_eq(const int key1, const int key2)
{
  return (key1 == key2);
}
static inline uint32_t puk_hash_int_default_hash(const int key)
{
  return puk_hash_int(&key);
}


/* ********************************************************* */
/** MurmurHash64A, by Austin Appleby
 * 64-bit version of MurmurHash2A, folded to 32 bits.
 * Reference implementation in public domain, converted from C++ to C.
 */
static inline uint32_t puk_hash_murmurhash64a(const void*_data, size_t len)
{
  const uint64_t seed = 0;
  const uint64_t m = 0xc6a4a7935bd1e995ULL;
  const int r = 47;
  const uint64_t*data = (const uint64_t *)_data;
  const uint64_t*end = data + (len / sizeof(*data));
  uint64_t h = seed ^ (len * m);
  while(data != end)
    {
      uint64_t k = *data++;
      k *= m;
      k ^= k >> r;
      k *= m;
      h ^= k;
      h *= m;
    }
  const unsigned char*data2 = (const unsigned char*)data;
  switch(len & 7)
    {
    case 7:
      h ^= (uint64_t)(data2[6]) << 48;
    case 6:
      h ^= (uint64_t)(data2[5]) << 40;
    case 5:
      h ^= (uint64_t)(data2[4]) << 32;
    case 4:
      h ^= (uint64_t)(data2[3]) << 24;
    case 3:
      h ^= (uint64_t)(data2[2]) << 16;
    case 2:
      h ^= (uint64_t)(data2[1]) << 8;
    case 1:
      h ^= (uint64_t)(data2[0]);
      h *= m;
    };
  h ^= h >> r;
  h *= m;
  h ^= h >> r;
  /* xor-fold the 64 bit hash into 32 bits */
  return (uint32_t)((h & 0xFFFFFFFF) ^ ((h & 0xFFFFFFFF00000000ULL) >> 32));
}

/** MurmurHash2A, by Austin Appleby
 * Reference implementation in public domain, from:
 * http://sites.google.com/site/murmurhash/
 */
#define puk_hash_mmix(h,k) { k *= m; k ^= k >> r; k *= m; h *= m; h ^= k; }
static inline uint32_t puk_hash_murmurhash2a (const void *_data, size_t len)
{
  const unsigned char *data = (const unsigned char *)_data;
  const unsigned int m = 0x5bd1e995;
  const int r = 24;
  unsigned int t = 0;
  unsigned int h = 0;
  unsigned int l = len;
  while(len >= 4)
    {
      unsigned int k = *(unsigned int*)data;
      puk_hash_mmix(h,k);
      data += 4;
      len -= 4;
    }
  switch (len)
    {
    case 3:
      t ^= (unsigned int)(data[2] << 16);
    case 2:
      t ^= (unsigned int)(data[1] << 8);
    case 1:
      t ^= data[0];
    }
  puk_hash_mmix(h,t);
  puk_hash_mmix(h,l);
  h ^= h >> 13;
  h *= m;
  h ^= h >> 15;
  return h;
}

static inline uint32_t puk_hash_default(const void*key, size_t len)
{
  return puk_hash_murmurhash2a(key, len);
}

/* ********************************************************* */

static inline uint32_t puk_hash_to_index(uint32_t z, uint32_t size)
{
  const uint32_t r = z % size;
  assert(r < size);
  return r;
}

#define PUK_HASH_PROGRESSIVE_REHASH 16

#define PUK_HASH_INIT_SIZE 11


/* ********************************************************* */
/* typed hashtables */

#define PUK_HASHTABLE_TYPE(ENAME, KEY_TYPE, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  PUK_HASHTABLE_TYPE2(ENAME, KEY_TYPE, ((KEY_TYPE)0), DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC)

#define PUK_HASHTABLE_TYPE2(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  PUK_HASHTABLE_TYPE_DECLARE(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC); \
  PUK_HASHTABLE_TYPE_HELPER(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC); \
  PUK_HASHTABLE_TYPE_FUNCS(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC); \
  PUK_HASHTABLE_TYPE_ENUMERATOR(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC);

/* ** declare types **************************************** */

#define PUK_HASHTABLE_TYPE_DECLARE(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  typedef uint32_t (*ENAME##_hash_hfunc_t)(KEY_TYPE key);               \
  typedef int (*ENAME##_hash_eqfunc_t)(KEY_TYPE key1, KEY_TYPE key2);   \
  typedef void (*ENAME##_hash_destructor_t)(KEY_TYPE key, DATA_TYPE data); \
  /** stores an entry in the hashtable */                               \
  struct ENAME##_hash_entry_s                                           \
  {                                                                     \
    KEY_TYPE key;                     /**< key to index the entry- must be non-NULL */ \
    DATA_TYPE data;                   /**< data value associated with key */ \
    struct ENAME##_hash_entry_s*next; /**< next entry in the same cell, in case of hash collision */ \
  };                                                                    \
  PUK_ALLOCATOR_TYPE(ENAME##_hash_entry, struct ENAME##_hash_entry_s);  \
  struct ENAME##_hashtable_s                                            \
  {                                                                     \
    /* main hashtable  */                                               \
    struct ENAME##_hash_entry_s*table; /**< table to store entries */   \
    ssize_t size;                      /**< size of above table */      \
    ssize_t load;                      /**< number of elements stored in the table */ \
    /* old hashtable for progressive rehashing */                       \
    ssize_t rehashed;                                                   \
    struct ENAME##_hash_entry_s*oldtable;                               \
    ssize_t oldsize;                                                    \
    /* cell allocator */                                                \
    struct ENAME##_hash_entry_allocator_s entry_allocator;              \
  };                                                                    \
  typedef struct ENAME##_hashtable_s*ENAME##_hashtable_t;               \

/* ** API functions **************************************** */

#define PUK_HASHTABLE_TYPE_FUNCS(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  /** initialize a new hashtable */                                     \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_init(ENAME##_hashtable_t h)      \
  {                                                                     \
    h->size = 0;                                                        \
    h->load = 0;                                                        \
    h->table = NULL;                                                    \
    h->oldtable = NULL;                                                 \
    ENAME##_hash_entry_allocator_init(&h->entry_allocator, PUK_HASH_INIT_SIZE); \
  }                                                                     \
  /** allocate a new hashtable */                                       \
  __attribute__((unused))                                               \
  static inline ENAME##_hashtable_t ENAME##_hashtable_new(void)         \
  {                                                                     \
    ENAME##_hashtable_t h = (ENAME##_hashtable_t)padico_malloc(sizeof(struct ENAME##_hashtable_s)); \
    ENAME##_hashtable_init(h);                                          \
    return h;                                                           \
  }                                                                     \
  /** destroy a hashtable */                                            \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_destroy(ENAME##_hashtable_t h)   \
  {                                                                     \
    const ENAME##_hash_destructor_t destructor = (DEL_FUNC);            \
    ENAME##_hash_entry_rehash_flush(h);                                 \
    ssize_t i;                                                          \
    for(i = 0; i < h->size; i++)                                        \
      {                                                                 \
        struct ENAME##_hash_entry_s*e = h->table + i;                   \
        if((e->key != KEY_NIL) && (destructor != NULL))                 \
          {                                                             \
            (*destructor)(e->key, e->data);                             \
          }                                                             \
        e = e->next;                                                    \
        while(e)                                                        \
          {                                                             \
            struct ENAME##_hash_entry_s*n = e->next;                    \
            if(destructor != NULL)                                      \
              {                                                         \
                (*destructor)(e->key, e->data);                         \
              }                                                         \
            ENAME##_hash_entry_free(&h->entry_allocator, e);            \
            e = n;                                                      \
          }                                                             \
      }                                                                 \
    ENAME##_hash_entry_allocator_destroy(&h->entry_allocator);          \
    if(h->table != NULL)                                                \
      padico_free(h->table);                                            \
    h->table = NULL;                                                    \
  }                                                                     \
  /** deallocate a hashtable */                                         \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_delete(ENAME##_hashtable_t h)    \
  {                                                                     \
    ENAME##_hashtable_destroy(h);                                       \
    padico_free(h);                                                     \
  }                                                                     \
  /** insert an entry into a hashtable */                               \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_insert(ENAME##_hashtable_t h, KEY_TYPE key, DATA_TYPE data) \
  {                                                                     \
    ENAME##_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH);        \
    assert(key != KEY_NIL);                                             \
    struct ENAME##_hash_entry_s*e = ENAME##_hash_entry_lookup(h, key);  \
    if(e)                                                               \
      {                                                                 \
        e->key  = key;                                                  \
        e->data = data;                                                 \
      }                                                                 \
    else                                                                \
      {                                                                 \
        if(h->load >= h->size * 3 / 4)                                  \
          {                                                             \
            ENAME##_hash_rehash_trigger(h);                             \
          }                                                             \
        ENAME##_hash_entry_insert(h, key, data);                        \
        h->load++;                                                      \
      }                                                                 \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline DATA_TYPE ENAME##_hashtable_lookup(ENAME##_hashtable_t h, const KEY_TYPE key) \
  {                                                                     \
    struct ENAME##_hash_entry_s*e = ENAME##_hash_entry_lookup(h, key);  \
    return e ? e->data : (DATA_TYPE)0;                                  \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline int ENAME##_hashtable_probe(ENAME##_hashtable_t h, const KEY_TYPE key) \
  {                                                                     \
    struct ENAME##_hash_entry_s*e = ENAME##_hash_entry_lookup(h, key);  \
    return (e != NULL);                                                 \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_lookup2(ENAME##_hashtable_t h, const KEY_TYPE key, KEY_TYPE*outkey, DATA_TYPE*data) \
   {                                                                    \
     struct ENAME##_hash_entry_s*e = ENAME##_hash_entry_lookup(h, key); \
     if(e != NULL)                                                      \
       {                                                                \
         if(outkey)                                                     \
           *outkey = e->key;                                            \
         if(data)                                                       \
           *data = e->data;                                             \
       }                                                                \
     else                                                               \
       {                                                                \
         if(outkey)                                                     \
           *outkey = KEY_NIL;                                           \
         if(data)                                                       \
           *data = (DATA_TYPE)0;                                        \
       }                                                                \
   }                                                                    \
   /** */                                                               \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_remove(ENAME##_hashtable_t h, const KEY_TYPE key) \
   {                                                                    \
     assert(key != KEY_NIL);                                            \
     if(h->table == NULL)                                               \
       return;                                                          \
     const uint32_t z = (*(HASH_FUNC))(key);                            \
     struct ENAME##_hash_entry_s*e = NULL;                              \
     struct ENAME##_hash_entry_s*prev = NULL;                           \
     do                                                                 \
       {                                                                \
         e = h->table + puk_hash_to_index(z, h->size);                  \
         prev = NULL;                                                   \
         while(e && (e->key != KEY_NIL))                                \
           {                                                            \
             if((*(EQ_FUNC))(key, e->key))                              \
               {                                                        \
                 if(e->next)                                            \
                   {                                                    \
                     struct ENAME##_hash_entry_s*next_e = e->next;      \
                     *e = *next_e;                                      \
                     ENAME##_hash_entry_free(&h->entry_allocator, next_e); \
                     e = e->next;                                       \
                   }                                                    \
                 else                                                   \
                   {                                                    \
                     if(prev)                                           \
                       {                                                \
                         assert(prev->next == e);                       \
                         prev->next = NULL;                             \
                         ENAME##_hash_entry_free(&h->entry_allocator, e); \
                       }                                                \
                     else                                               \
                       {                                                \
                         e->key = KEY_NIL;                              \
                         e->data = (DATA_TYPE)0;                        \
                       }                                                \
                   }                                                    \
                 h->load--;                                             \
                 return;                                                \
               }                                                        \
             else                                                       \
               {                                                        \
                 prev = e;                                              \
                 e = e->next;                                           \
               }                                                        \
           }                                                            \
       }                                                                \
     while(ENAME##_hash_entry_oldprobe(h, key)); /* retry as long as rehashing is in progress */ \
   }                                                                    \
  __attribute__((unused))                                               \
  static inline ssize_t ENAME##_hashtable_size(ENAME##_hashtable_t h)   \
  {                                                                     \
    return h->load;                                                     \
  }

/* ** helpers functions ************************************ */

#define PUK_HASHTABLE_TYPE_HELPER(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  static inline struct ENAME##_hash_entry_s*ENAME##_hash_entry_lookup(ENAME##_hashtable_t h, const KEY_TYPE key); \
  static inline void ENAME##_hash_entry_insert(ENAME##_hashtable_t h, KEY_TYPE key, DATA_TYPE data); \
  static inline int ENAME##_hash_entry_oldprobe(ENAME##_hashtable_t h, const KEY_TYPE key); \
  static inline void ENAME##_hash_entry_rehash_row(ENAME##_hashtable_t h, ssize_t z); \
  static inline void ENAME##_hash_entry_rehash_n(ENAME##_hashtable_t h, ssize_t n); \
  static inline void ENAME##_hash_entry_rehash_flush(ENAME##_hashtable_t h); \
  /** lookup in the oldtable; if found, migrate row and return cell in regular table */ \
  __attribute__((unused))                                               \
  static inline int ENAME##_hash_entry_oldprobe(ENAME##_hashtable_t h, const KEY_TYPE key) \
  {                                                                     \
    if(h->oldtable)                                                     \
      {                                                                 \
        const uint32_t z = (*(HASH_FUNC))(key);                         \
        const ssize_t index = puk_hash_to_index(z, h->oldsize);         \
        struct ENAME##_hash_entry_s*e = h->oldtable + index;            \
        while(e && (e->key != KEY_NIL))                                 \
          {                                                             \
            if((*(EQ_FUNC))(key, e->key))                               \
              {                                                         \
                ENAME##_hash_entry_rehash_row(h, index); /* rehash row of the considered key */ \
                ENAME##_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH); /* make progressive rehash progress */ \
                return 1;                                               \
              }                                                         \
            e = e->next;                                                \
          }                                                             \
      }                                                                 \
    return 0;                                                           \
  }                                                                     \
  /** force rehashing of given row */                                   \
  __attribute__((unused))                                               \
  static inline void ENAME##_hash_entry_rehash_row(ENAME##_hashtable_t h, ssize_t z) \
  {                                                                     \
    assert(h->oldtable != NULL);                                        \
    struct ENAME##_hash_entry_s*source = h->oldtable + z;               \
    if(source->key != KEY_NIL)                                          \
      {                                                                 \
        ENAME##_hash_entry_insert(h, source->key, source->data);        \
        source->key = KEY_NIL;                                          \
        ssize_t first = 1;                                              \
        while(source->next)                                             \
          {                                                             \
            struct ENAME##_hash_entry_s*n = source->next;               \
            ENAME##_hash_entry_insert(h, n->key, n->data);              \
            n->key = KEY_NIL;                                           \
            if(!first)                                                  \
              ENAME##_hash_entry_free(&h->entry_allocator, source);     \
            source = n;                                                 \
            first = 0;                                                  \
          }                                                             \
      }                                                                 \
  }                                                                     \
  /** make progress on rehash for n rows */                             \
  __attribute__((unused))                                               \
  static inline void ENAME##_hash_entry_rehash_n(ENAME##_hashtable_t h, ssize_t n) \
  {                                                                     \
    if(h->oldtable)                                                     \
      {                                                                 \
        ssize_t k;                                                      \
        for(k = 0; k < n; k++)                                          \
          {                                                             \
            ENAME##_hash_entry_rehash_row(h, h->rehashed + k);          \
            if(h->rehashed + k == h->oldsize - 1)                       \
              {                                                         \
                padico_free(h->oldtable);                               \
                h->oldtable = NULL;                                     \
                h->rehashed = 0;                                        \
                return;                                                 \
              }                                                         \
          }                                                             \
        h->rehashed += k;                                               \
      }                                                                 \
  }                                                                     \
  /** insert an entry in the given table, without rehashing  */         \
  __attribute__((unused))                                               \
  static inline void ENAME##_hash_entry_insert(ENAME##_hashtable_t h, KEY_TYPE key, DATA_TYPE data) \
  {                                                                     \
    const uint32_t z = (*(HASH_FUNC))(key);                             \
    assert(h->table != NULL);                                           \
    struct ENAME##_hash_entry_s*e = h->table + puk_hash_to_index(z, h->size); \
    if(e->key != KEY_NIL)                                               \
      {                                                                 \
        while(e->next)                                                  \
          {                                                             \
            e = e->next;                                                \
          }                                                             \
        e->next = ENAME##_hash_entry_malloc(&h->entry_allocator);       \
        e = e->next;                                                    \
      }                                                                 \
    e->key  = key;                                                      \
    e->data = data;                                                     \
    e->next = NULL;                                                     \
  }                                                                     \
  /** lookup an entry in the hashtable */                               \
  __attribute__((unused))                                               \
  static inline struct ENAME##_hash_entry_s*                            \
  ENAME##_hash_entry_lookup(ENAME##_hashtable_t h, const KEY_TYPE key)  \
  {                                                                     \
    if(h->table == NULL)                                                \
      return NULL;                                                      \
    const uint32_t z = (*(HASH_FUNC))(key);                             \
    struct ENAME##_hash_entry_s*e = NULL;                               \
    do                                                                  \
      {                                                                 \
        e = h->table + puk_hash_to_index(z, h->size);                   \
        while(e && (e->key != KEY_NIL))                                 \
          {                                                             \
            if((*(EQ_FUNC))(key, e->key))                               \
              {                                                         \
                return e;                                               \
              }                                                         \
            e = e->next;                                                \
          }                                                             \
      }                                                                 \
    while(ENAME##_hash_entry_oldprobe(h, key));                         \
    return NULL;                                                        \
  }                                                                     \
  /** flush all rows of a pending rehash */                             \
  __attribute__((unused))                                               \
  static inline void ENAME##_hash_entry_rehash_flush(ENAME##_hashtable_t h) \
  {                                                                     \
    if(h->oldtable)                                                     \
      {                                                                 \
        ENAME##_hash_entry_rehash_n(h, h->oldsize);                     \
      }                                                                 \
    assert(h->oldtable == NULL);                                        \
  }                                                                     \
  /** trigger progressive rehashing */                                  \
  __attribute__((unused))                                               \
  static void ENAME##_hash_rehash_trigger(ENAME##_hashtable_t h)        \
  {                                                                     \
    static const ssize_t hash_primes[] =                                \
      {                                                                 \
       11, 23, 53, 97, 193, 389, 769, 1543, 3079, 6151, 12289,          \
       24593, 49157, 98317, 196613, 393241, 786433, 1572869,            \
       3145739, 6291469, 12582917, 25165843, 50331653, 100663319,       \
       201326611, 402653189, 805306457, /* 16106212741,*/ -1            \
      };                                                                \
    assert(hash_primes[0] == PUK_HASH_INIT_SIZE);                       \
    assert(h->oldtable == NULL);                                        \
    h->oldtable = h->table;                                             \
    h->oldsize  = h->size;                                              \
    h->rehashed = 0;                                                    \
    int i = 0;                                                          \
    while(h->size >= hash_primes[i])                                    \
      {                                                                 \
        i++;                                                            \
      }                                                                 \
    h->size = hash_primes[i];                                           \
    assert(h->size > 0);                                                \
    assert(h->size > h->oldsize);                                       \
    h->table = (struct ENAME##_hash_entry_s*)padico_malloc(h->size * sizeof(struct ENAME##_hash_entry_s)); \
    for(i = 0; i < h->size; i++)                                        \
      {                                                                 \
        h->table[i].key = KEY_NIL;                                      \
        h->table[i].data = (DATA_TYPE)0;                                \
        h->table[i].next = NULL;                                        \
      }                                                                 \
    ENAME##_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH);        \
  }                                                                     \


#define PUK_HASHTABLE_TYPE_ENUMERATOR(ENAME, KEY_TYPE, KEY_NIL, DATA_TYPE, HASH_FUNC, EQ_FUNC, DEL_FUNC) \
  struct ENAME##_hashtable_enumerator_s                                 \
  {                                                                     \
    ENAME##_hashtable_t h;                                              \
    struct ENAME##_hash_entry_s*current;                                \
    ssize_t i;                                                          \
  };                                                                    \
  typedef struct ENAME##_hashtable_enumerator_s*ENAME##_hashtable_enumerator_t; \
                                                                        \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_enumerator_init(ENAME##_hashtable_enumerator_t e, ENAME##_hashtable_t h) \
  {                                                                     \
    ENAME##_hash_entry_rehash_flush(h);                                 \
    e->h = h;                                                           \
    e->i = -1;                                                          \
    e->current = NULL;                                                  \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_enumerator_next2(ENAME##_hashtable_enumerator_t e, KEY_TYPE*key, DATA_TYPE*data) \
  {                                                                     \
  assert(e->h->oldtable == NULL);                                       \
  if(e->current && e->current->next)                                    \
    {                                                                   \
      /* bump one bucket */                                             \
      e->current = e->current->next;                                    \
    }                                                                   \
  else                                                                  \
    {                                                                   \
      do                                                                \
        {                                                               \
          e->i++;                                                       \
        }                                                               \
      while(e->i < 0 || (e->i < e->h->size && !e->h->table[e->i].key)); \
      e->current = e->h->table + e->i;                                  \
    }                                                                   \
  if(e->i < e->h->size)                                                 \
    {                                                                   \
      if(key)                                                           \
        *key = e->current->key;                                         \
      if(data)                                                          \
        *data = e->current->data;                                       \
    }                                                                   \
  else                                                                  \
    {                                                                   \
      if(key)                                                           \
        *key = KEY_NIL;                                                 \
      if(data)                                                          \
        *data = (DATA_TYPE)0;                                           \
    }                                                                   \
  }                                                                     \
  /***/                                                                 \
  __attribute__((unused))                                               \
  static inline ENAME##_hashtable_enumerator_t ENAME##_hashtable_enumerator_new(ENAME##_hashtable_t h) \
  {                                                                     \
    ENAME##_hashtable_enumerator_t e = (struct ENAME##_hashtable_enumerator_s*) \
      padico_malloc(sizeof(struct ENAME##_hashtable_enumerator_s));     \
    ENAME##_hashtable_enumerator_init(e, h);                            \
    return e;                                                           \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_enumerator_destroy(ENAME##_hashtable_enumerator_t e __attribute__((unused))) \
  {                                                                     \
    /* does nothing for now */                                          \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline void ENAME##_hashtable_enumerator_delete(ENAME##_hashtable_enumerator_t e) \
  {                                                                     \
    ENAME##_hashtable_enumerator_destroy(e);                            \
    padico_free(e);                                                     \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline KEY_TYPE ENAME##_hashtable_enumerator_next_key(ENAME##_hashtable_enumerator_t e) \
  {                                                                     \
    KEY_TYPE _key;                                                      \
    ENAME##_hashtable_enumerator_next2(e, &_key, (DATA_TYPE*)0);        \
    return _key;                                                        \
  }                                                                     \
  /** */                                                                \
  __attribute__((unused))                                               \
  static inline DATA_TYPE ENAME##_hashtable_enumerator_next_data(ENAME##_hashtable_enumerator_t e) \
   {                                                                    \
     DATA_TYPE _data;                                                   \
     ENAME##_hashtable_enumerator_next2(e, (KEY_TYPE*)0, &_data);       \
     return _data;                                                      \
   }                                                                    \





#endif /* PUK_HASTABLE_H */
