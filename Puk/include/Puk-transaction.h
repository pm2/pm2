/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file lock-free FIFO using transactional memory
 */

#ifndef PUK_TRANSACTION_H
#define PUK_TRANSACTION_H

#if defined(PUK_ENABLE_GNU_TM)

/** builds a transactional memory based FIFO type and functions.
 *  ENAME is base name for symbols
 *  TYPE is the type of elements in queue- must be atomic (1, 2, 4, or 8 bytes long)
 *  LFQUEUE_NULL is a nil value to fill and detect empty cells
 *  LFQUEUE_SIZE is the array size
 *  Only 'init', 'enqueue', 'dequeue' operations are available.
 *  Others operations such as 'empty', 'size' are useless for lock-free
 *  Iterators are not possible.
 * Conventions:
 *   _head: next cell to enqueue
 *   _tail: next cell to dequeue
 *   _head == _tail     => queue empty
 *   _head == _tail - 1 => queue full
 */
#define PUK_LFQUEUE_TM_TYPE(ENAME, TYPE, QUEUE_NULL, QUEUE_SIZE)        \
  typedef TYPE ENAME ## _lfqueue_elem_t;                                \
                                                                        \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    ENAME ## _lfqueue_elem_t _queue[(QUEUE_SIZE)];                      \
    unsigned _head;                                                     \
    unsigned _tail;                                                     \
  };                                                                    \
                                                                        \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    int i;                                                              \
    for(i = 0; i < (QUEUE_SIZE); i++)                                   \
      {                                                                 \
        queue->_queue[i] = (QUEUE_NULL);                                \
      }                                                                 \
    queue->_head = 0;                                                   \
    queue->_tail = 0;                                                   \
  }                                                                     \
                                                                        \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  {                                                                     \
    __transaction_relaxed                                               \
      {                                                                 \
        const unsigned head = queue->_head;                             \
        const unsigned tail = queue->_tail;                             \
        if(((head + (unsigned)(QUEUE_SIZE) - tail) % (QUEUE_SIZE)) >= (QUEUE_SIZE - 2)) \
          {                                                             \
            /* Queue full, abort enqueue */                             \
            return -1;                                                  \
          }                                                             \
        queue->_head = (head + 1) % (QUEUE_SIZE);                       \
        assert(queue->_queue[head] == (QUEUE_NULL));                    \
        /* store value in reserved slot */                              \
        queue->_queue[head] = e;                                        \
      }                                                                 \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
                                                                        \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    ENAME ## _lfqueue_elem_t e = (QUEUE_NULL);                          \
    __transaction_relaxed                                               \
      {                                                                 \
        const unsigned tail = queue->_tail;                             \
        if(tail == queue->_head)                                        \
          {                                                             \
            /* queue was empty, abort dequeue */                        \
            return e;                                                   \
          }                                                             \
        queue->_tail = (tail + 1) % (QUEUE_SIZE);                       \
        e = queue->_queue[tail];                                        \
        queue->_queue[tail] = (QUEUE_NULL);                             \
      }                                                                 \
    assert(e != (QUEUE_NULL));                                          \
    return e;                                                           \
  }                                                                     \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
                                                                        \
  /** hint whether the queue is empty.                                  \
   *  Do not rely on it: state may have changed before dequeue          \
   *  Usefull to reduce active polling, though */                       \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    int y;                                                              \
    __transaction_relaxed                                               \
    {                                                                   \
      y = queue->_tail == queue->_head;                                 \
    }                                                                   \
    return y;                                                           \
  }                                                                     \
  /** hint about the number of elements in queue .                      \
   * Do not rely on it: state may have changed before function return.  \
   */                                                                   \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return __transaction_relaxed((QUEUE_SIZE + queue->_head - queue->_tail) % QUEUE_SIZE); \
  }                                                                     \
  /** get the allocated queue size */                                   \
  static inline int ENAME ## _lfqueue_size(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return (QUEUE_SIZE);                                                \
  }

#endif /* PUK_ENABLE_GNU_TM */


#if defined(PUK_ENABLE_RTM)
#include <x86intrin.h>

/** lfqueue using Intel RTM */
#define PUK_LFQUEUE_RTM_TYPE(ENAME, TYPE, QUEUE_NULL, QUEUE_SIZE)       \
  typedef TYPE ENAME ## _lfqueue_elem_t;                                \
                                                                        \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    ENAME ## _lfqueue_elem_t _queue[(QUEUE_SIZE)];                      \
    unsigned _head;                                                     \
    unsigned _tail;                                                     \
    int _slowpath;                                                      \
  };                                                                    \
                                                                        \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    fprintf(stderr, "# lfqueue_init() RTM- %s\n", #ENAME);              \
    int i;                                                              \
    for(i = 0; i < (QUEUE_SIZE); i++)                                   \
      {                                                                 \
        queue->_queue[i] = (QUEUE_NULL);                                \
      }                                                                 \
    queue->_head = 0;                                                   \
    queue->_tail = 0;                                                   \
    queue->_slowpath = 0;                                               \
  }                                                                     \
  static inline void ENAME ## _lfqueue_lock(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    int r = 1;                                                          \
    while(!__sync_bool_compare_and_swap(&queue->_slowpath, 0, 1))       \
      {                                                                 \
        puk_lfbackoff(r++);                                             \
      }                                                                 \
  }                                                                     \
  static inline void ENAME ## _lfqueue_unlock(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    queue->_slowpath = 0;                                               \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_internal(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  {                                                                     \
    const unsigned head = queue->_head;                                 \
    const unsigned tail = queue->_tail;                                 \
    if(((head + (unsigned)(QUEUE_SIZE) - tail) % (QUEUE_SIZE)) >= (QUEUE_SIZE - 2)) \
      {                                                                 \
        /* Queue full, abort enqueue */                                 \
        return -1;                                                      \
      }                                                                 \
    queue->_head = (head + 1) % (QUEUE_SIZE);                           \
    assert(queue->_queue[head] == (QUEUE_NULL));                        \
    /* store value in reserved slot */                                  \
    queue->_queue[head] = e;                                            \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  {                                                                     \
    unsigned status = _xbegin();                                        \
    if((status == _XBEGIN_STARTED) && !queue->_slowpath)                \
      {                                                                 \
        int rc = ENAME ## _lfqueue_enqueue_internal(queue, e);          \
        _xend();                                                        \
        return rc;                                                      \
      }                                                                 \
    else                                                                \
      {                                                                 \
        ENAME ## _lfqueue_lock(queue);                                  \
        int rc = ENAME ## _lfqueue_enqueue_internal(queue, e);          \
        ENAME ## _lfqueue_unlock(queue);                                \
        return rc;                                                      \
      }                                                                 \
    return -1;                                                          \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
                                                                        \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue_internal(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    ENAME ## _lfqueue_elem_t e = (QUEUE_NULL);                          \
    const unsigned tail = queue->_tail;                                 \
    if(tail == queue->_head)                                            \
      {                                                                 \
        /* queue was empty, abort dequeue */                            \
        return e;                                                       \
      }                                                                 \
    queue->_tail = (tail + 1) % (QUEUE_SIZE);                           \
    e = queue->_queue[tail];                                            \
    queue->_queue[tail] = (QUEUE_NULL);                                 \
    return e;                                                           \
  }                                                                     \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    ENAME ## _lfqueue_elem_t e = (QUEUE_NULL);                          \
    unsigned status = _xbegin();                                        \
    if((status == _XBEGIN_STARTED) && !queue->_slowpath)                \
      {                                                                 \
        e = ENAME ## _lfqueue_dequeue_internal(queue);                  \
        _xend();                                                        \
        return e;                                                       \
      }                                                                 \
    else                                                                \
      {                                                                 \
        ENAME ## _lfqueue_lock(queue);                                  \
        e = ENAME ## _lfqueue_dequeue_internal(queue);                  \
        ENAME ## _lfqueue_unlock(queue);                                \
        return e;                                                       \
      }                                                                 \
    return e;                                                           \
  }                                                                     \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
                                                                        \
  /** hint whether the queue is empty.                                  \
   *  Do not rely on it: state may have changed before dequeue          \
   *  Usefull to reduce active polling, though */                       \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    int y = queue->_tail == queue->_head;                               \
    return y;                                                           \
  }                                                                     \
  /** hint about the number of elements in queue .                      \
   * Do not rely on it: state may have changed before function return.  \
   */                                                                   \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return ((QUEUE_SIZE + queue->_head - queue->_tail) % QUEUE_SIZE);   \
  }                                                                     \
  /** get the allocated queue size */                                   \
  static inline int ENAME ## _lfqueue_size(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return (QUEUE_SIZE);                                                \
  }


#endif /* PUK_ENABLE_RTM */

#endif /* PUK_TRANSACTION_H */
