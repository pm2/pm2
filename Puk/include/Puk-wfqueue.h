/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief wait-free FIFO based on wfqueue from
 * https://github.com/chaoran/fast-wait-free-queue
 */

#ifndef PUK_WFQUEUE_H
#define PUK_WFQUEUE_H

#include <stddef.h>
#include <unistd.h>
#include <pthread.h>

#include "queue.h"

#define PUK_LFQUEUE_WFQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  struct ENAME ## _wfqueue_handle_s                                     \
  {                                                                     \
    handle_t h;                                                         \
  };                                                                    \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    queue_t q;                                                          \
    pthread_key_t key;                                                  \
  };                                                                    \
  /** @internal resolve local thread state; create it lazily if needed */ \
  static inline struct ENAME ## _wfqueue_handle_s* ENAME ## _wfqueue_get_handle(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _wfqueue_handle_s*th = pthread_getspecific(q->key); \
    if(th == NULL)                                                      \
      {                                                                 \
        th = malloc(sizeof(struct ENAME ## _wfqueue_handle_s));         \
        queue_register(&q->q, &th->h, -1);                              \
        pthread_setspecific(q->key, th);                                \
      }                                                                 \
    return th;                                                          \
  }                                                                     \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    queue_init(&q->q, 512);                                             \
    pthread_key_create(&q->key, NULL /* TODO- missing destructor */);   \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    struct ENAME ## _wfqueue_handle_s*th = ENAME ## _wfqueue_get_handle(q); \
    enqueue(&q->q, &th->h, val);                                        \
    return 0; /* always sucess; never full */                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _wfqueue_handle_s*th = ENAME ## _wfqueue_get_handle(q); \
    void*v = dequeue(&q->q, &th->h);                                    \
    if(v == EMPTY)                                                      \
      return NULL;                                                      \
    else                                                                \
      return v;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }



#endif /* PUK_WFQUEUE_H */
