/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * Stub to use ConcurrencyFreaks C++ queues from C code.
 */

#ifndef PUK_CFQUEUES_H
#define PUK_CFQUEUES_H

#ifdef PUK_ENABLE_CFQUEUES

#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PUK_CFQUEUE_DECLS(CFQUEUE)                                      \
  void puk_cfqueue_ ## CFQUEUE ## _init(void**q);                       \
  void puk_cfqueue_ ## CFQUEUE ## _enqueue(void*q, int tid, void*v);    \
  void*puk_cfqueue_ ## CFQUEUE ## _dequeue(void*q, int tid);            \
  void puk_cfqueue_ ## CFQUEUE ## _free(void*q, int tid);


#define PUK_LFQUEUE_CRTURN_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)        \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, CRTurnQueue);

#define PUK_LFQUEUE_KOGANPETRANK_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)  \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, KoganPetrankQueueCHP);

#define PUK_LFQUEUE_LCRQ_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)  \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, LCRQueue);

#define PUK_LFQUEUE_MSQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, MichaelScottQueue);

#define PUK_LFQUEUE_CRDOUBLELINK_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)  \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, CRDoubleLinkQueue);

#define PUK_LFQUEUE_BITNEXT_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, BitNextQueue);

#define PUK_LFQUEUE_LINEARARRAY_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  PUK_CFQUEUE_TYPE(ENAME, TYPE, LinearArrayQueue);


#define PUK_CFQUEUE_TYPE(ENAME, TYPE, CFQUEUE)                          \
  PUK_CFQUEUE_DECLS(CFQUEUE);                                           \
  struct ENAME ## _cfqueue_handle_s                                     \
  {                                                                     \
    int tid;                                                            \
  };                                                                    \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    void*q;                                                             \
    pthread_key_t key;                                                  \
    int next_id;                                                        \
  };                                                                    \
  /** @internal resolve local thread state; create it lazily if needed */ \
  static inline int ENAME ## _cfqueue_get_tid(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _cfqueue_handle_s*th = pthread_getspecific(q->key); \
    if(th == NULL)                                                      \
      {                                                                 \
        th = malloc(sizeof(struct ENAME ## _cfqueue_handle_s));         \
        th->tid = __sync_fetch_and_add(&q->next_id, 1);                 \
        pthread_setspecific(q->key, th);                                \
      }                                                                 \
    return th->tid;                                                     \
  }                                                                     \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    q->next_id = 0;                                                     \
    puk_cfqueue_ ## CFQUEUE ## _init(&q->q);                            \
    pthread_key_create(&q->key, NULL /* TODO- missing destructor */);   \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    const int tid = ENAME ## _cfqueue_get_tid(q);                       \
    puk_cfqueue_ ## CFQUEUE ## _enqueue(q->q, tid, val);               \
    return 0; /* always sucess; never full */                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    const int tid = ENAME ## _cfqueue_get_tid(q);                       \
    TYPE val = puk_cfqueue_ ## CFQUEUE ## _dequeue(q->q, tid);         \
    return val;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0; /* isempty is only a hint; it's ok to always pretend queue is not empty */ \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    const int tid = ENAME ## _cfqueue_get_tid(q);                       \
    puk_cfqueue_ ## CFQUEUE ## _free(q->q, tid);                        \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }

#ifdef __cplusplus
} /* extern "C" */
#endif

#else /* PUK_ENABLE_CFQUEUES */
#warning "PUK_ENABLE_CFQUEUES undefined but still including Puk-cfqueues.h"
#endif /* PUK_ENABLE_CFQUEUES */

#endif /* PUK_CFQUEUES_H */
