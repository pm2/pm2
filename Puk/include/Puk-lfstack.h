/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief lock-free stacks as chained list
 */

#ifndef PUK_LFSTACK_H
#define PUK_LFSTACK_H

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#if defined(__x86_64__) || defined (__aarch64__)

#define PUK_HAVE_TAGGED_PTR 1

/* this code assumes:
 *   -- 48 bits virtual address space and 64 bits pointers
 *   -- __sync_bool_compare_and_swap is availablke as intrinsic
 * This is true at least on x86_64 & ARM64 (with different normalization scheme).
 */

extern void puk_lfbackoff(int p);

/** whether to display a warning each time an ABA error is detected */
static const int puk_lfstack_display_aba = 0;

#define PUK_LFSTACK_ALIGN 16

/** use low bits in tagged pointers; works only if pointer is aligned */
//#define PUK_TAGGED_PTR_LOW_BITS 1

/** a tagged pointer
 * a 18-bits tag is inserted in the unused bits of pointer:
 *   - in the 2 lowest bits, unused when alignement is 4
 *   - in the 16 highest bits, unused on 64 bits machine that actually
 *     have a 48 bits virtual address space.
 */
typedef void*puk_tagged_ptr_t;

#ifdef PUK_TAGGED_PTR_LOW_BITS
#define PUK_TAGGED_PTR_TAG_MAX 0x0003FFFF
#else
#define PUK_TAGGED_PTR_TAG_MAX 0x0000FFFF
#endif

static inline void*puk_tagged_ptr_get_ptr(puk_tagged_ptr_t tptr)
{
  /* mask tag bits */
#ifdef PUK_TAGGED_PTR_LOW_BITS
  uintptr_t uptr = (uintptr_t)tptr & 0x0000FFFFFFFFFFFC;
#else
  uintptr_t uptr = (uintptr_t)tptr & 0x0000FFFFFFFFFFFF;
#endif
  /* normalize pointer */
#if defined(__x86_64__)
  /* x86_64 normalization: sign bit expansion */
  if(uptr & ((uintptr_t)0x01 << 47))
    uptr |= (uintptr_t)0xFFFF000000000000;
#elif (defined __aarch64__)
  /* arm64 normalization: clear bits */
  uptr &= 0x0000FFFFFFFFFFFF;
#endif
  return (void*)uptr;
}

static inline uintptr_t puk_tagged_ptr_get_tag(puk_tagged_ptr_t tptr)
{
  uintptr_t uptr = (uintptr_t)tptr;
#ifdef PUK_TAGGED_PTR_LOW_BITS
  uintptr_t tag = ((uptr & (uintptr_t)0xFFFF000000000000) >> 46) | (uptr & (uintptr_t)0x03);
#else
  uintptr_t tag = ((uptr & (uintptr_t)0xFFFF000000000000) >> 48);
#endif
  return tag;
}

static inline puk_tagged_ptr_t puk_tagged_ptr_build(void*ptr, uintptr_t tag)
{
  const uintptr_t uptr = (uintptr_t)ptr;
#ifdef PUK_TAGGED_PTR_LOW_BITS
  /* check that low bits are free */
  assert((uptr & 0x03) == 0);
#endif
  /* check that pointer is normalized */
#if defined(__x86_64__)
  /* check x86_64 normalization */
  assert( ( ((uptr & ((uintptr_t)0x01 << 47)) != 0x00) &&
            ((uptr & (uintptr_t)0xFFFF000000000000)) == (uintptr_t)0xFFFF000000000000) ||
          ( ((uptr & ((uintptr_t)0x01 << 47)) == 0x00) &&
            ((uptr & (uintptr_t)0xFFFF000000000000)) == 0x00) );
#elif (defined __aarch64__)
  /* check arm64 normalization */
  assert(((uptr & (uintptr_t)0xFFFF000000000000)) == (uintptr_t)0x0000000000000000);
#endif
#ifdef PUK_TAGGED_PTR_LOW_BITS
  /* check that tag is 18 bits */
  assert((tag & (uintptr_t)0x0003FFFF) == tag);
  const uintptr_t tptr = ((tag & (uintptr_t)0x0003FFFC) << 46) | uptr | (tag & 0x03);
#else
  /* check that tag is 16 bits */
  assert((tag & (uintptr_t)0x0000FFFF) == tag);
  const uintptr_t tptr = ((tag & (uintptr_t)0x0000FFFF) << 48) | uptr;
#endif
  /*
    fprintf(stderr, "# puk_tagged_ptr_build()- ptr = %p; tag = 0x%lx; tptr = 0x%p\n", ptr, tag, (void*)tptr);
  */
  assert(tag == puk_tagged_ptr_get_tag((void*)tptr));
  assert(ptr == puk_tagged_ptr_get_ptr((void*)tptr));

  return (void*)tptr;
}

#define PUK_LFSTACK_TYPE(ENAME, DECL)                                   \
  struct ENAME ## _lfstack_cell_s                                       \
  {                                                                     \
    struct ENAME ## _lfstack_cell_s*_next;                              \
    DECL                                                                \
  } __attribute__((aligned(16)));                                       \
  struct ENAME ## _lfstack_s                                            \
  {                                                                     \
    struct ENAME ## _lfstack_cell_s*_head;                              \
  };                                                                    \
  typedef struct ENAME ## _lfstack_s ENAME ## _lfstack_t;               \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_aba_check(ENAME ## _lfstack_t*_stack, \
                                                 struct ENAME ## _lfstack_cell_s*_old_cell) \
  {                                                                     \
    if( (puk_tagged_ptr_get_ptr(_stack->_head) == puk_tagged_ptr_get_ptr(_old_cell)) && \
        (puk_tagged_ptr_get_tag(_stack->_head) != puk_tagged_ptr_get_tag(_old_cell)) && \
        puk_lfstack_display_aba )                                       \
      {                                                                 \
        fprintf(stderr, "# WARNING- %s_lfstack: ABA error detected; ptr tag = %d; old tag = %d.\n", \
                #ENAME,                                                 \
                (int)puk_tagged_ptr_get_tag(_stack->_head),             \
                (int)puk_tagged_ptr_get_tag(_old_cell));                \
      }                                                                 \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_init(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    /* check that align is a power of 2 */                              \
    assert((PUK_LFSTACK_ALIGN & (PUK_LFSTACK_ALIGN - 1)) == 0);         \
    _stack->_head = NULL;                                               \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_destroy(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    _stack->_head = NULL;                                               \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_push_ext(ENAME ## _lfstack_t*_stack, struct ENAME ## _lfstack_cell_s*_cell, int single) \
  {                                                                     \
    int r = 0;                                                          \
    do                                                                  \
      {                                                                 \
        struct ENAME ## _lfstack_cell_s*_next = _stack->_head;          \
        _cell->_next = _next;                                           \
        const uintptr_t _orig_tag = puk_tagged_ptr_get_tag(_next);      \
        const uintptr_t _cell_tag = (_orig_tag + 1) & (uintptr_t)PUK_TAGGED_PTR_TAG_MAX; \
        void*_cell_with_tag = puk_tagged_ptr_build((void*)_cell, _cell_tag); \
        if(single)                                                      \
          {                                                             \
            assert(_stack->_head == _next);                             \
            _stack->_head = (struct ENAME ## _lfstack_cell_s*)_cell_with_tag; \
            break;                                                      \
          }                                                             \
        else                                                            \
          {                                                             \
            if(!__sync_bool_compare_and_swap(&_stack->_head, _next, (struct ENAME ## _lfstack_cell_s*)_cell_with_tag)) \
              { ENAME ## _lfstack_aba_check(_stack, _next); puk_lfbackoff(r++); } \
            else                                                        \
              { r = 0; }                                                \
          }                                                             \
      }                                                                 \
    while(r != 0);                                                      \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_push(ENAME ## _lfstack_t*_stack, struct ENAME ## _lfstack_cell_s*_cell) \
  {                                                                     \
    ENAME ## _lfstack_push_ext(_stack, _cell, 0);                       \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_push_single(ENAME ## _lfstack_t*_stack, struct ENAME ## _lfstack_cell_s*_cell) \
  {                                                                     \
    ENAME ## _lfstack_push_ext(_stack, _cell, 1);                       \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline struct ENAME ## _lfstack_cell_s* ENAME ## _lfstack_pop_ext(ENAME ## _lfstack_t*_stack, int single) \
  {                                                                     \
    int r = 0;                                                          \
    struct ENAME ## _lfstack_cell_s*_cell_ptr = NULL;                   \
    do                                                                  \
      {                                                                 \
        const puk_tagged_ptr_t _cell_with_tag = (puk_tagged_ptr_t)_stack->_head; \
        _cell_ptr = (struct ENAME ## _lfstack_cell_s*)puk_tagged_ptr_get_ptr(_cell_with_tag); \
        if(_cell_ptr == NULL) return NULL;                              \
        const uintptr_t _cell_tag = puk_tagged_ptr_get_tag(_cell_with_tag); \
        const puk_tagged_ptr_t _next = (puk_tagged_ptr_t)_cell_ptr->_next; \
        const puk_tagged_ptr_t _new_head = puk_tagged_ptr_build(puk_tagged_ptr_get_ptr(_next), \
                                                                ((_cell_tag) + 1) & PUK_TAGGED_PTR_TAG_MAX);\
        struct ENAME##_lfstack_cell_s*_new_head_ptr = (struct ENAME##_lfstack_cell_s*)_new_head; \
        if(single)                                                      \
          {                                                             \
            assert(_stack->_head == _cell_with_tag);                    \
            _stack->_head = (struct ENAME ## _lfstack_cell_s*)_new_head_ptr; \
            break;                                                      \
          }                                                             \
        else                                                            \
          {                                                             \
            if(!__sync_bool_compare_and_swap(&_stack->_head, (struct ENAME##_lfstack_cell_s*)_cell_with_tag, _new_head_ptr)) \
              { ENAME ## _lfstack_aba_check(_stack, (struct ENAME ## _lfstack_cell_s*)_cell_with_tag); puk_lfbackoff(r++); } \
            else                                                        \
              { r = 0; }                                                \
          }                                                             \
      }                                                                 \
    while(r != 0);                                                      \
    _cell_ptr->_next = NULL;                                            \
    return _cell_ptr;                                                   \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline struct ENAME ## _lfstack_cell_s* ENAME ## _lfstack_pop(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    return ENAME ## _lfstack_pop_ext(_stack, 0);                        \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline struct ENAME ## _lfstack_cell_s* ENAME ## _lfstack_pop_single(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    return ENAME ## _lfstack_pop_ext(_stack, 1);                        \
  }


#else /* __x86_64__ || __aarch64__ */

/* compatibility mode for non x86_64: use a lock; assume pthread */

#include <pthread.h>
#ifdef MARCEL
#error non x86_64 build supports only pthread (for now)
#endif

#define PUK_LFSTACK_TYPE(ENAME, DECL)                                   \
  struct ENAME ## _lfstack_cell_s                                       \
  {                                                                     \
    struct ENAME ## _lfstack_cell_s*_next;                              \
    DECL                                                                \
  } __attribute__((aligned(16)));                                       \
  struct ENAME ## _lfstack_s                                            \
  {                                                                     \
    struct ENAME ## _lfstack_cell_s*_head;                              \
    pthread_spinlock_t _lock;                                           \
  };                                                                    \
  typedef struct ENAME ## _lfstack_s ENAME ## _lfstack_t;               \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_init(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    _stack->_head = NULL;                                               \
    pthread_spin_init(&_stack->_lock, 0);                               \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_destroy(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    _stack->_head = NULL;                                               \
    pthread_spin_destroy(&_stack->_lock);                               \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_push_ext(ENAME ## _lfstack_t*_stack, struct ENAME ## _lfstack_cell_s*_cell, int single) \
  {                                                                     \
    if(!single)                                                         \
      pthread_spin_lock(&_stack->_lock);                                \
    _cell->_next = _stack->_head;                                       \
    _stack->_head = _cell;                                              \
    if(!single)                                                         \
      pthread_spin_unlock(&_stack->_lock);                              \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfstack_push(ENAME ## _lfstack_t*_stack, struct ENAME ## _lfstack_cell_s*_cell) \
  {                                                                     \
    ENAME ## _lfstack_push_ext(_stack, _cell, 0);                       \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline struct ENAME ## _lfstack_cell_s* ENAME ## _lfstack_pop_ext(ENAME ## _lfstack_t*_stack, int single) \
  {                                                                     \
    if(!single)                                                         \
      pthread_spin_lock(&_stack->_lock);                                \
    struct ENAME ## _lfstack_cell_s*_cell = _stack->_head;              \
    if(_cell != NULL)                                                   \
      {                                                                 \
        _stack->_head = _cell->_next;                                   \
        _cell->_next = NULL;                                            \
      }                                                                 \
    if(!single)                                                         \
      pthread_spin_unlock(&_stack->_lock);                              \
    return _cell;                                                       \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline struct ENAME ## _lfstack_cell_s* ENAME ## _lfstack_pop(ENAME ## _lfstack_t*_stack) \
  {                                                                     \
    return ENAME ## _lfstack_pop_ext(_stack, 0);                        \
  }


#endif /* __x86_64__ || __aarch64__ */

#endif /* PUK_LFSTACK_H */
