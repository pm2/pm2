/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * Stub to use C++ LOO queue from C code.
 */

#ifndef PUK_LOOQUEUE_H
#define PUK_LOOQUEUE_H

#ifdef PUK_ENABLE_LOOQUEUE

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

  void puk_looqueue_init(void**q);
  void puk_looqueue_enqueue(void*q, void*v);
  void*puk_looqueue_dequeue(void*q);
  void puk_looqueue_free(void*q);


#define PUK_LFQUEUE_LOOQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)      \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    void*q;                                                             \
  };                                                                    \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    puk_looqueue_init(&q->q);                                           \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    puk_looqueue_enqueue(q->q, val);                                    \
    return 0; /* always sucess; never full */                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    TYPE val = puk_looqueue_dequeue(q->q);                              \
    return val;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0; /* isempty is only a hint; it's ok to always pretend queue is not empty */ \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    puk_looqueue_free(q->q);                                            \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }

#ifdef __cplusplus
} /* extern "C" */
#endif

#else /* PUK_ENABLE_LOOQUEUE */
#warning "PUK_ENABLE_LOOQUEUE undefined but still including Puk-looqueue.h"
#endif /* PUK_ENABLE_LOOQUEUE */

#endif /* PUK_LOOQUEUE_H */
