/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief wait-free FIFO based on simqueue from libconcurrent.
 * Only API adaptation code is present here.
 * Implementation is in externals/libconcurrent/.
 */

#ifndef PUK_LIBCONCURRENT_H
#define PUK_LIBCONCURRENT_H

#include <stddef.h>
#include <unistd.h>
#include <pthread.h>

#include "sim.h"
#include "simqueue.h"
#include "ccqueue.h"
#include "dsmqueue.h"
#include "hqueue.h"
#include "osciqueue.h"
#include "fcqueue.h"

#define PUK_LFQUEUE_SIMQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)      \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(SimQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)

#define SimQueueInitStub(Q, N) SimQueueStructInit(Q, N, 10)

#define PUK_LFQUEUE_CCQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(CCQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)
#define CCQueueEnqueue CCQueueApplyEnqueue
#define CCQueueDequeue CCQueueApplyDequeue
#define CCQueueInitStub(Q, N) CCQueueStructInit(Q, N)

#define PUK_LFQUEUE_DSMQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)      \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(DSMQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)
#define DSMQueueEnqueue DSMQueueApplyEnqueue
#define DSMQueueDequeue DSMQueueApplyDequeue
#define DSMQueueInitStub(Q, N) DSMQueueStructInit(Q, N)

#define PUK_LFQUEUE_HQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)        \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(HQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)
#define HQueueEnqueue HQueueApplyEnqueue
#define HQueueDequeue HQueueApplyDequeue
#define HQueueInitStub(Q, N) HQueueInit(Q, N, 2)

#define PUK_LFQUEUE_OSCIQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)      \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(OsciQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)
#define OsciQueueEnqueue OsciQueueApplyEnqueue
#define OsciQueueDequeue OsciQueueApplyDequeue
#define OsciQueueInitStub(Q, N) OsciQueueInit(Q, N, 32)

#define PUK_LFQUEUE_FCQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)       \
  PUK_LFQUEUE_LIBCONCURRENT_TYPE(FCQueue, ENAME, TYPE, LFQUEUE_NULL, SIZE)
#define FCQueueEnqueue FCQueueApplyEnqueue
#define FCQueueDequeue FCQueueApplyDequeue
#define FCQueueInitStub(Q, N) FCQueueStructInit(Q, N)

#define PUK_LFQUEUE_LIBCONCURRENT_TYPE(QUEUE, ENAME, TYPE, LFQUEUE_NULL, SIZE) \
  struct ENAME ## _simqueue_handle_s                                    \
  {                                                                     \
    QUEUE ## ThreadState state;                                          \
    int id;                                                             \
  };                                                                    \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    struct QUEUE ## Struct q;                                           \
    pthread_key_t key;                                                  \
    int next_id;                                                        \
  };                                                                    \
  /** @internal resolve local thread state; create it lazily if needed */ \
  static inline struct ENAME ## _simqueue_handle_s* ENAME ## _simqueue_get_handle(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _simqueue_handle_s*th = pthread_getspecific(q->key); \
    if(th == NULL)                                                      \
      {                                                                 \
        th = malloc(sizeof(struct ENAME ## _simqueue_handle_s));        \
        th->id = __sync_fetch_and_add(&q->next_id, 1);                  \
        QUEUE ## ThreadStateInit(&q->q, &th->state, th->id);             \
        pthread_setspecific(q->key, th);                                \
      }                                                                 \
    return th;                                                          \
  }                                                                     \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    q->next_id = 0;                                                     \
    QUEUE ## InitStub(&q->q, 256);                                      \
    pthread_key_create(&q->key, NULL /* TODO- missing destructor */);   \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    struct ENAME ## _simqueue_handle_s*th = ENAME ## _simqueue_get_handle(q); \
    QUEUE ## Enqueue(&q->q, &th->state, (uintptr_t)val, th->id);         \
    return 0; /* always sucess; never full */                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _simqueue_handle_s*th = ENAME ## _simqueue_get_handle(q); \
    uintptr_t v = QUEUE ## Dequeue(&q->q, &th->state, th->id);           \
    if(v == EMPTY_QUEUE)                                                \
      return NULL;                                                      \
    else                                                                \
      return (TYPE)v;                                                   \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1;                                                           \
  }



#endif /* PUK_LIBCONCURRENT_H */
