/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief slab-based allocator for objects with fixed size
 * uses lock-free stacks
 */

#ifndef PUK_ALLOCATOR_H
#define PUK_ALLOCATOR_H


#include <stdlib.h>
#include <unistd.h>


#define puk_container_of(ptr, type, member)                             \
        ((type *)((char *)(__typeof__ (&((type *)0)->member))(ptr)- ((size_t) &((type *)0)->member)))


#define PUK_ALLOCATOR_ALIGN 16

static inline void puk_allocator_cell_clear(void*ptr __attribute__((unused)), size_t size __attribute__((unused)))
{
#ifdef PUK_DEBUG
  memset(ptr, 0, size);
#endif
}
#ifdef PUK_DEBUG
#define PUK_ALLOCATOR_REDZONE_SIZE   8
#define PUK_ALLOCATOR_REDZONE(LABEL) char LABEL[PUK_ALLOCATOR_REDZONE_SIZE];
#else
#define PUK_ALLOCATOR_REDZONE(LABEL)
#define PUK_ALLOCATOR_REDZONE_SIZE   0
#endif

#define PUK_ALLOCATOR_NULL { .blocks = NULL, .slices = NULL, .slab_size = 0 }

/** thread-safe allocator type */
#define PUK_ALLOCATOR_TYPE(ENAME, TYPE)                 \
  PUK_ALLOCATOR_TYPE_EXT(ENAME, TYPE, 0)

/** single thread allocator type, to use in single thread context or with external locking */
#define PUK_ALLOCATOR_TYPE_SINGLE(ENAME, TYPE)  \
  PUK_ALLOCATOR_TYPE_EXT(ENAME, TYPE, 1)

/** allocator with thread safety declared at init time, not at compile-time */
#define PUK_ALLOCATOR_TYPE_DYNTHREAD(ENAME, TYPE)       \
  PUK_ALLOCATOR_TYPE_EXT(ENAME, TYPE, -1)

#define PUK_ALLOCATOR_TYPE_EXT(ENAME, TYPE, SINGLE)                     \
  PUK_LFSTACK_TYPE(ENAME##_block,                                       \
                   PUK_ALLOCATOR_REDZONE(_z1);                          \
                   TYPE block __attribute__((aligned(PUK_ALLOCATOR_ALIGN))); \
                   PUK_ALLOCATOR_REDZONE(_z2); );                       \
  PUK_LFSTACK_TYPE(ENAME##_slice, struct ENAME##_block_lfstack_cell_s block; ); \
  struct ENAME##_allocator_s                                            \
  {                                                                     \
    ENAME##_block_lfstack_t blocks;                                     \
    ENAME##_slice_lfstack_t slices;                                     \
    ssize_t slab_size; /**< size of the last allocated slab */          \
    int new_slab; /**< whether a new slab is currently beeing allocated */ \
    int single; /**< whether single thread or multithread */            \
  };                                                                    \
  typedef struct ENAME##_allocator_s*ENAME##_allocator_t;               \
  __attribute__((unused))                                               \
  /** explicit thread level*/                                           \
  static inline void ENAME##_allocator_init_thread(struct ENAME##_allocator_s*a, ssize_t slab_size, int single) \
  {                                                                     \
    ENAME ## _block_lfstack_init(&a->blocks);                           \
    ENAME ## _slice_lfstack_init(&a->slices);                           \
    a->slab_size = slab_size;                                           \
    a->new_slab = 0;                                                    \
    a->single = single;                                                 \
    PUK_VG_CREATE_MEMPOOL(a, PUK_ALLOCATOR_REDZONE_SIZE, 0);            \
  }                                                                     \
  __attribute__((unused))                                               \
  /** default compile-time thread level */                              \
  static inline void ENAME##_allocator_init(struct ENAME##_allocator_s*a, ssize_t slab_size) \
  {                                                                     \
    assert((SINGLE) != -1); /* cannot use default init when dynamic thread level is expected */ \
    ENAME##_allocator_init_thread(a, slab_size, (SINGLE));              \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline ENAME##_allocator_t ENAME##_allocator_new(ssize_t slab_size) \
  {                                                                     \
    struct ENAME##_allocator_s*a =                                      \
      (struct ENAME##_allocator_s*)padico_malloc(sizeof(struct ENAME##_allocator_s)); \
    ENAME##_allocator_init(a, slab_size);                               \
    return a;                                                           \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME##_allocator_destroy(struct ENAME##_allocator_s*a) \
  {                                                                     \
    struct ENAME##_slice_lfstack_cell_s*slice = NULL;                   \
    do {                                                                \
      slice = ENAME##_slice_lfstack_pop_ext(&a->slices, a->single);     \
      padico_free(slice);                                               \
    } while(slice != NULL);                                             \
    ENAME ## _block_lfstack_destroy(&a->blocks);                        \
    ENAME ## _slice_lfstack_destroy(&a->slices);                        \
    a->slab_size = -1;                                                  \
    PUK_VG_DESTROY_MEMPOOL(a);                                          \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME##_allocator_delete(ENAME##_allocator_t a)    \
  {                                                                     \
    ENAME##_allocator_destroy(a);                                       \
    padico_free(a);                                                     \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline TYPE*ENAME##_malloc(ENAME##_allocator_t a)              \
  {                                                                     \
    int r = 0;                                                          \
    struct ENAME##_block_lfstack_cell_s*cell = NULL;                    \
  retry:                                                                \
    cell = ENAME##_block_lfstack_pop_ext(&a->blocks, a->single);        \
    if(cell == NULL)                                                    \
      {                                                                 \
        __sync_synchronize();                                           \
        if(!__sync_bool_compare_and_swap(&a->new_slab, 0, 1))           \
          { puk_lfbackoff(r++); goto retry; }                           \
        else                                                            \
          {                                                             \
            const ssize_t slab_size = a->slab_size;                     \
            a->slab_size *= 2;                                          \
            const ssize_t slice_size = sizeof(struct ENAME##_slice_lfstack_cell_s) + \
              sizeof(struct ENAME##_block_lfstack_cell_s) * slab_size;  \
            struct ENAME##_slice_lfstack_cell_s*slice = NULL;           \
            padico_memalign((void**)&slice, 16, slice_size);            \
            if(slice == NULL)                                           \
              {                                                         \
                fprintf(stderr, "Puk: out of memory in allocator %s for type '%s' (block size = %zu)." \
                        " Cannot allocate slice of size %zu bytes (%zu entries).\n", \
                        #ENAME, #TYPE, sizeof(TYPE), slice_size, slab_size); \
                abort();                                                \
              }                                                         \
            ENAME##_slice_lfstack_push_ext(&a->slices, slice, a->single); \
            struct ENAME##_block_lfstack_cell_s*block = &slice->block;  \
            cell = &block[0];                                           \
            ssize_t i;                                                  \
            for(i = 1; i < slab_size; i++)                              \
              {                                                         \
                PUK_VG_MAKE_MEM_NOACCESS(&block[i].block, sizeof(TYPE)); \
                ENAME##_block_lfstack_push_ext(&a->blocks, &block[i], a->single); \
              }                                                         \
            a->new_slab = 0;                                            \
            __sync_synchronize();                                       \
          }                                                             \
      }                                                                 \
    PUK_VG_MEMPOOL_ALLOC(a, &cell->block, sizeof(TYPE));                \
    puk_allocator_cell_clear(&cell->block, sizeof(TYPE));               \
    return &cell->block;                                                \
  }                                                                     \
  __attribute__((unused))                                               \
  static inline void ENAME##_free(ENAME##_allocator_t a, TYPE*b)        \
  {                                                                     \
    struct ENAME##_block_lfstack_cell_s*cell = puk_container_of(b, struct ENAME##_block_lfstack_cell_s, block); \
    puk_allocator_cell_clear(&cell->block, sizeof(TYPE));               \
    PUK_VG_MEMPOOL_FREE(a, b);                                          \
    ENAME##_block_lfstack_push_ext(&a->blocks, cell, a->single);        \
  }


#endif /* PUK_ALLOCATOR_H */
