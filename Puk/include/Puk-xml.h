/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk frontend to XML parser
 */

#include <sys/uio.h>

/** @defgroup PukParse API: Puk XML parsing
 * @ingroup Puk
 */

/* *** Puk XML front-end ********************************** */

/** @addtogroup PukParse
 * @{
 */

typedef struct puk_parse_entity_s*puk_parse_entity_t;
PUK_VECT_TYPE(puk_parse_entity,  puk_parse_entity_t)
struct puk_parse_entity_s
{
  const char* tag;     /**< the XML tag */
  char**      xa;      /**< attributes- vector of (label, value) pairs */
  void*       content; /**< cookie at disposal for user's code */
  size_t      content_len; /**< length of binary attachment (pointer is 'content') */
  char*       text;    /**< enclosed text between opening and closing tag */
  padico_rc_t rc;      /**< result of action */
  struct puk_parse_context_s*context; /**< our container */
  struct puk_parse_entity_vect_s contained_entities;
  struct puk_parse_entity_s*parent; /**< parent in parsing stack */
};

/** One of this structure is associated with each Expat XML parser instance
 */
struct puk_parse_context_s
{
  const char*buffer;
  int size;
  int                     trust_level; /**< the trust level of the parser */
  struct puk_parse_entity_vect_s parse_stack; /**< the stack of currently parsed entities */
  enum {
    PUK_PARSE_OK,         /**< ok, continue parsing */
    PUK_PARSE_ERROR,      /**< the user-provided code detected an error (not conformant to schema) */
    PUK_PARSE_UNTRUSTED,
    PUK_PARSE_UNKNOWN_TAG /**< we found a tag described by no Puk XML action */
  } status; /**< error status of the parsing. If status != PUK_PARSE_OK, no need to continue parsing. */
  struct puk_parse_entity_s root_entity; /**< the root entity (filled in after parsing to get the result) */
};

/** Describes how to deal with a given XML tag
 */
struct puk_tag_action_s
{
  const char*xml_tag;
  void  (*start_handler)(puk_parse_entity_t);
  void  (*end_handler)(puk_parse_entity_t);
  int   required_level;
  const char*help;
};

void puk_xml_add_action(struct puk_tag_action_s action);
void puk_xml_delete_action(const char*tag);
struct puk_parse_entity_s puk_xml_parse_buffer(const char*buffer, int len, int trust_level);
struct puk_parse_entity_s puk_xml_parse_file  (const char*filename, int trust_level);
struct puk_parse_entity_s puk_xml_parse_init(const char*init_name);
/** @internal */
const char*puk_xml_xa_getattr  (const char*label, char**xa);

/** Find our parent in the XML tree */
static inline puk_parse_entity_t puk_parse_parent(puk_parse_entity_t e)
{ return e->parent; }
/** set the content cookie in the entity */
static inline void puk_parse_set_content(puk_parse_entity_t e, void*c)
{ e->content = c; }
/** get the content cookie in the entity */
static inline void* puk_parse_get_content(puk_parse_entity_t e)
{ return e->content; }
/** get the entities enclosed in an entity */
static inline puk_parse_entity_vect_t puk_parse_get_contained(puk_parse_entity_t e)
{ return &e->contained_entities; }
/** test tag of entity */
static inline int puk_parse_is(puk_parse_entity_t e, const char*tag)
{ return (strcmp(e->tag, tag) == 0); }
/** get the text enclosed in a tag */
static inline void* puk_parse_get_text(puk_parse_entity_t e)
{ return e->text; }
static inline void puk_parse_set_rc(puk_parse_entity_t e, padico_rc_t rc)
{ e->rc = rc; }
static inline padico_rc_t puk_parse_get_rc(puk_parse_entity_t e)
{ return e->rc; }
static inline const char*puk_parse_getattr(puk_parse_entity_t e, const char*label)
{ const char*value = puk_xml_xa_getattr(label, e->xa); return value; }
/** get a binary part in a multi-part message */
static inline struct iovec puk_parse_getpart(puk_parse_entity_t e)
{
  struct iovec part = { e->content, e->content_len };
  assert(puk_parse_is(e, "puk:part"));
  return part;
}

/** command origin is: local node (console, boot, ...) */
#define PUK_TRUST_LOCAL   900

/** system message from other node */
#define PUK_TRUST_SYSTEM 800

/** command from other node in the same cluster */
#define PUK_TRUST_CLUSTER 600

/** command from a trusted remote control */
#define PUK_TRUST_CONTROL 500

/** command from any remote control */
#define PUK_TRUST_OUTSIDE 200

/** only what _everybody_ can do */
#define PUK_TRUST_OTHER   0

/** @} */ /* PukParse */
