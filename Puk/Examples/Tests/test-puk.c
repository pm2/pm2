/** @file test Puk alone without PukABI and PadicoTM
 */

#include <Padico/Puk.h>

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(test_puk, NULL, NULL, NULL);

/* ********************************************************* */
/* *** Interface 'Dummy' */

struct padico_dummy_driver_s
{
  int (*dummy)(void*_status, int);
};

PUK_IFACE_TYPE(Dummy, struct padico_dummy_driver_s);

/* ********************************************************* */
/* *** drivers */

static void*test_comp_instantiate(puk_instance_t ai, puk_context_t context);
static void test_comp_destroy(void*_status);

static const struct puk_component_driver_s test_comp_component_driver =
  {
    .instantiate = &test_comp_instantiate,
    .destroy     = &test_comp_destroy
  };

static int test_comp_dummy(void*_status, int a);

static const struct padico_dummy_driver_s test_comp_dummy_driver =
  {
    .dummy = &test_comp_dummy
  };


/* ********************************************************* */
/* *** implementation */

/* internal status of component instance */
struct test_comp_dummy_s
{
  int v;
};

static int test_comp_dummy(void*_status, int a) __attribute__((noinline));
static int test_comp_dummy(void*_status, int a)
{
  struct test_comp_dummy_s*status = _status;
  status->v += a;
  return status->v;
}

static void*test_comp_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct test_comp_dummy_s*status = malloc(sizeof(struct test_comp_dummy_s));
  status->v = 0;
  return status;
}

static void test_comp_destroy(void*_status)
{
  free(_status);
}

/* ********************************************************* */

int main(int argc, char**argv)
{
  /* initialize Puk */
  padico_puk_init();
  fprintf(stderr, "Hello world!\n");

  /* declare component */
  puk_component_t component =
    puk_component_declare("TestComponents",
                          puk_component_provides("PadicoComponent", "instance", &test_comp_component_driver),
                          puk_component_provides("Dummy",         "dummy", &test_comp_dummy_driver),
                          puk_component_attr("toto", "default_value"));
  /* alternate method to find the component: resolve component from known name */
  puk_component_resolve("TestComponents");
  /* instanciate component */
  puk_instance_t instance = puk_component_instantiate(component);
  /* resolve receptacle for interface with type "Dummy" with any name */
  struct puk_receptacle_Dummy_s receptacle;
  puk_instance_indirect_Dummy(instance, NULL, &receptacle);

  /* some calls to component methods */
  int d = (*receptacle.driver->dummy)(receptacle._status, 12);
  printf("%d\n", d);
  d = (*receptacle.driver->dummy)(receptacle._status, 42);
  printf("%d\n", d);

  /* destroy the component instance */
  puk_instance_destroy(instance);
  /* destroy the component */
  puk_component_destroy(component);

  padico_puk_shutdown();
  return 0;
}
