/** @file
 * Puk test: attributes with driver 'binary'
 * @author Alexandre Denis
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

static int dynattr_run(int argc, char**arv);

PADICO_MODULE_DECLARE(dynattr, NULL, &dynattr_run, NULL);

PADICO_MODULE_ATTR(attr1, "DYNATTR_ATTRIBUTE1", "attribute for tests with dynamically loaded modules", int, 0);
PADICO_MODULE_ATTR(attr2, "DYNATTR_ATTRIBUTE2", "attribute for tests with dynamically loaded modules with early value", int, 0);

static int dynattr_run(int argc, char**arv)
{
  printf("in module: dynattr1 = %d\n", padico_module_attr_attr1_getvalue());
  printf("in module: dynattr2 = %d\n", padico_module_attr_attr2_getvalue());

}
