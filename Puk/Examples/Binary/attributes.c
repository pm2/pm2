/** @file
 * Puk test: attributes with driver 'builtin'
 * @author Alexandre Denis
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(example, NULL, NULL, NULL);

PADICO_MODULE_ATTR(test_attribute1, "TEST_ATTRIBUTE1", "attribute for tests with boolean", bool, 0);
PADICO_MODULE_ATTR(test_attribute2, "TEST_ATTRIBUTE2", "attribute for tests with integers", int, 0);
PADICO_MODULE_ATTR(test_attribute3, "TEST_ATTRIBUTE3", "attribute for tests with strings", string, "default");
PADICO_MODULE_ATTR(test_attribute4, "TEST_ATTRIBUTE4", "attribute for tests with integers with early value", int, 0);

int main(int argc, char**argv)
{
  puk_opt_setvalue("example", "test_attribute4", "42");
  puk_opt_setvalue("dynattr", "attr2", "12");

  padico_puk_init();

  struct puk_opt_s*attr1 = puk_opt_find("example", "test_attribute1");
  char*v1 = puk_opt_value_to_string(attr1);
  printf("attr1 = %d / %d(%s)\n", puk_opt_getvalue_bool(attr1), padico_module_attr_test_attribute1_getvalue(), v1);
  padico_free(v1);

  struct puk_opt_s*attr2 = puk_opt_find("example", "test_attribute2");
  char*v2 = puk_opt_value_to_string(attr2);
  printf("attr2 = %d / %d (%s)\n", puk_opt_getvalue_int(attr2), padico_module_attr_test_attribute2_getvalue(), v2);
  padico_free(v2);

  struct puk_opt_s*attr3 = puk_opt_find("example", "test_attribute3");
  char*v3 = puk_opt_value_to_string(attr3);
  printf("attr3 = %s / %s (%s)\n", puk_opt_getvalue_string(attr3), padico_module_attr_test_attribute3_getvalue(), v3);
  padico_free(v3);

  struct puk_opt_s*attr4 = puk_opt_find("example", "test_attribute4");
  char*v4 = puk_opt_value_to_string(attr4);
  printf("attr4 = %d / %d (%s)\n", puk_opt_getvalue_int(attr4), padico_module_attr_test_attribute4_getvalue(), v4);
  padico_free(v4);

  puk_mod_t mod;
  padico_puk_mod_resolve(&mod, "./dynattr");
  padico_puk_mod_load(mod);

  struct puk_opt_s*dynattr1 = puk_opt_find("dynattr", "attr1");
  printf("dynattr1 = %d\n", puk_opt_getvalue_int(dynattr1));

  struct puk_opt_s*dynattr2 = puk_opt_find("dynattr", "attr2");
  printf("dynattr2 = %d\n", puk_opt_getvalue_int(dynattr2));

  struct puk_job_s*job = padico_malloc(sizeof(struct puk_job_s));
  *job = (struct puk_job_s){ .argc = 0, .argv = NULL, .notify = NULL, .notify_key = NULL };
  padico_puk_mod_start(mod, job);

  padico_puk_shutdown();
  return 0;
}
