/* Padico tutorial: "Hello world" as plain binary
 * author: Alexandre Denis
 */

#include <Padico/Puk.h>

int main(int argc, char**argv)
{
  padico_puk_init();
  printf("Hello world!\n");
  padico_puk_shutdown();
  return 0;
}
