/** @file
 * test lfqueues with interrupts
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>

#include <Padico/Puk.h>

#define INTERVAL_USEC 20

#define LFQUEUE_SIZE (1024)

#define ARRAY_SIZE (1024*1024*16)

#define GRANULARITY (1024* 1024)

static uint64_t array[ARRAY_SIZE];

static pthread_barrier_t b1, b2;

#ifdef QUEUE_TYPE
#define MKTYPE(Q) PUK_LFQUEUE_## Q ## _TYPE
#define LFQUEUE_BENCH_TYPE_(Q) MKTYPE(Q)
#define LFQUEUE_BENCH_TYPE(QUEUE_TYPE, ENAME, TYPE, SIZE) \
  LFQUEUE_BENCH_TYPE_(QUEUE_TYPE)(ENAME, TYPE, NULL, SIZE)
LFQUEUE_BENCH_TYPE(QUEUE_TYPE, bench, uint64_t*, LFQUEUE_SIZE);
#define MKKIND(Q) #Q
#define QUEUE_KIND_(Q) MKKIND(Q)
#define QUEUE_KIND QUEUE_KIND_(QUEUE_TYPE)
#else
PUK_LFQUEUE_TYPE(bench, uint64_t*, NULL, LFQUEUE_SIZE);
#define QUEUE_KIND PUK_LFQUEUE_KIND
#endif

static struct bench_lfqueue_s q;
static volatile int nthreads = 0;


/* ** producer/consumer ************************************ */

static inline void enable_thread_interrupt(void)
{
  sigset_t sigmask_set;
  sigemptyset(&sigmask_set);
  sigaddset(&sigmask_set, SIGUSR1);
  pthread_sigmask(SIG_UNBLOCK, &sigmask_set, NULL);
}

static inline void disable_thread_interrupt(void)
{
  sigset_t sigmask_set;
  sigemptyset(&sigmask_set);
  sigaddset(&sigmask_set, SIGUSR1);
  pthread_sigmask(SIG_BLOCK, &sigmask_set, NULL);
}

static void sig_handler(int sig)
{
  static long long count = 0;
  if(nthreads == 0)
    return;
  long long c = __sync_fetch_and_add(&count, 1);
  if(c % 2 == 1)
    {
      int rc = -1;
      do
        {
          rc = bench_lfqueue_enqueue(&q, &array[count % ARRAY_SIZE]);
        }
      while((rc != 0) && (nthreads > 0));
    }
  else
    {
      uint64_t*v = NULL;
      do
        {
          v = bench_lfqueue_dequeue(&q);
        }
      while((v == NULL) && (nthreads > 0));
    }
}

static void*producer(void*_d)
{
  pthread_barrier_wait(&b1);

  enable_thread_interrupt();

  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      if(i % GRANULARITY == 0)
        {
          fprintf(stderr, "p");
          fflush(stderr);
        }
      int rc = -1;
      do
        {
          rc = bench_lfqueue_enqueue(&q, &array[i % ARRAY_SIZE]);
        }
      while(rc != 0);
    }
  __sync_fetch_and_add(&nthreads, -1);
  pthread_barrier_wait(&b2);
  return NULL;
}

static void*consumer(void*_d)
{
  pthread_barrier_wait(&b1);
  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      if(i % GRANULARITY == 0)
        {
          fprintf(stderr, "c");
          fflush(stderr);
        }
      uint64_t*v = NULL;
      do
        {
          v = bench_lfqueue_dequeue(&q);
        }
      while(v == NULL);
    }
  __sync_fetch_and_add(&nthreads, -1);
  pthread_barrier_wait(&b2);
  return NULL;
}

static void benchmark_producerconsumer(int np, int nc)
{
  assert((nc == 1) || (np == 1) || (nc == np));
  int dp = ARRAY_SIZE;
  int dc = ARRAY_SIZE;

  bench_lfqueue_init(&q);
  nthreads = np + nc;

  fprintf(stderr, "# starting %d -> %d threads ... dp = %d; dc = %d\n", np, nc, dp, dc);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);

  pthread_t p[np], c[nc];
  pthread_barrier_init(&b1, NULL, np + nc);
  pthread_barrier_init(&b2, NULL, np + nc);
  int k;
  for(k = 0; k < nc; k++)
    {
      pthread_create(&c[k], NULL, &consumer, (void*)(uintptr_t)dc);
    }
  for(k = 0; k < np; k++)
    {
      pthread_create(&p[k], NULL, &producer, (void*)(uintptr_t)dp);
    }

  struct sigaction act;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  act.sa_handler = &sig_handler;
  int rc = sigaction(SIGALRM, &act, NULL);
  if(rc != 0)
    abort();

  disable_thread_interrupt();

  struct itimerval iv;
  iv.it_interval.tv_sec = 0;
  iv.it_interval.tv_usec = INTERVAL_USEC;
  iv.it_value.tv_sec = 0;
  iv.it_value.tv_usec = INTERVAL_USEC;
  rc = setitimer(ITIMER_REAL, &iv, NULL);
  if(rc != 0)
    abort();

  for(k = 0; k < nc; k++)
    {
      pthread_join(c[k], NULL);
    }
  for(k = 0; k < np; k++)
    {
      pthread_join(p[k], NULL);
    }
  pthread_barrier_destroy(&b1);
  pthread_barrier_destroy(&b2);

  iv.it_interval.tv_sec = 0;
  iv.it_interval.tv_usec = 0;
  iv.it_value.tv_sec = 0;
  iv.it_value.tv_usec = 0;
  rc = setitimer(ITIMER_REAL, &iv, NULL);
  assert(rc == 0);
  fprintf(stderr, "\n");

  PUK_GET_TICK(t2);
  double d = padico_timing_diff_usec(&t1, &t2);
  printf("%d \t %d \t %f\n", np, nc, ((np?:1) * dp) / d);
}


int main(int argc, char**argv)
{
  const int max_threads = 16;
  padico_puk_init();

  int i;
  for(i = 0; i < ARRAY_SIZE; i++)
    {
      array[i] = i;
    }

  printf("# lfqueue kind = %s\n", QUEUE_KIND);
  printf("# N.prod\t| N.cons\t| throughput M.elements / s\n");

  printf("# benchmark type: producer/consumer\n");
  int n;
  for(n = 2; n <= max_threads; n++)
    {
      benchmark_producerconsumer(n, n);
    }

  puk_profile_dump();
  padico_puk_shutdown();
  return 0;
}
