#! /bin/bash

localhost=$1
peer=$2

echo "# localhost = ${localhost}; peer = ${peer}"

# if [ "x${localhost}" = "x" ]; then
#     exit 1
# fi
# if [ "x${peer}" = "x" ]; then
#     exit 1
# fi

for q in atomic nblfq nblfq2 looqueue wcq simqueue koganpetrank crturn lcrq msqueue crdoublelink wfqueue bitnext lineararray scq2; do
    echo
    echo "# queue: ${q}"
    echo
    ./run-one.sh ${q} ${localhost} ${peer}
done

