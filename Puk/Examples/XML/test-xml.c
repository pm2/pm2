/** @file test Puk XML
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <time.h>

PADICO_MODULE_BUILTIN(test_xml, NULL, NULL, NULL);

#define NUM_BENCH 100000


static void test_handler(puk_parse_entity_t e)
{
  puk_parse_set_content(e, NULL);
}
static const struct puk_tag_action_s test_action =
  {
    .xml_tag = "test",
    .start_handler = NULL,
    .end_handler = &test_handler,
    .required_level = PUK_TRUST_CONTROL,
    .help = "test XML"
  };

static const char test_message[] = "<test/>";

int main(int argc, char**argv)
{
  padico_puk_init();
  fprintf(stderr, "starting...\n");
  puk_xml_add_action(test_action);

  struct timespec t1, t2;
  int i;
  clock_gettime(CLOCK_MONOTONIC, &t1);
  for(i = 0; i < NUM_BENCH; i++)
    {
      puk_xml_parse_buffer(test_message, strlen(test_message), PUK_TRUST_LOCAL);
    }
  clock_gettime(CLOCK_MONOTONIC, &t2);
  const double lat_usec = (1000000.0*(t2.tv_sec - t1.tv_sec) + (t2.tv_nsec - t1.tv_nsec) / 1000.0) / NUM_BENCH;
  fprintf(stderr, "time = %f usec\n", lat_usec);

  padico_puk_shutdown();
  return 0;
}
