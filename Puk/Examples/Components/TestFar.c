#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Puk.h>

#include "dummy.h"


/* ********************************************************* */
/* *** drivers */

static void*test_far_instantiate(puk_instance_t ai, puk_context_t context);
static void test_far_destroy(void*_instance);

static const struct puk_component_driver_s test_far_component_driver =
  {
    .instantiate = &test_far_instantiate,
    .destroy     = &test_far_destroy
  };

static int test_far_dummy(int a);

static const struct padico_dummy_driver_s test_far_dummy_driver =
  {
    .dummy = &test_far_dummy
  };


/* ********************************************************* */
/* *** implementation */

static void*test_far_instantiate(puk_instance_t instance, puk_context_t context)
{
  return padico_malloc(sizeof(int));
}

static void test_far_destroy(void*_instance)
{
  free(_instance);
}

static int test_far_dummy(int a)
{
  return a;
}

/* ********************************************************* */
/* *** component */

PADICO_MODULE_COMPONENT(TestFar,
  puk_component_declare("TestFar",
                        puk_component_provides("PadicoComponent", "component", &test_far_component_driver),
                        puk_component_provides("Dummy",         "dummy", &test_far_dummy_driver)));

/* *** misc */

extern int test_extern_dummy(int a)
{
  return a;
}
