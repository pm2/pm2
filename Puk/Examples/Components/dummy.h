
/* ********************************************************* */
/* *** Interface 'Dummy' */

struct padico_dummy_driver_s
{
  int (*dummy)(int);
};

PUK_IFACE_TYPE(Dummy, struct padico_dummy_driver_s);
