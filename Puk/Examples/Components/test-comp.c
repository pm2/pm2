
#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "dummy.h"

PADICO_MODULE_DECLARE(TestComponents);

/* inlined version of one-at-a-time */
static inline uint32_t test_hash_oneatatime(const unsigned char*key, int len)
{
  register uint32_t z = 0;
  register int i;
  for (i = 0; i < len; i++)
    {
      z += key[i];
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  return z;
}

static inline uint32_t test_hash_murmurhash64a(const void*_data, size_t len)
{
  const uint64_t seed = 0;
  const uint64_t m = 0xc6a4a7935bd1e995ULL;
  const int r = 47;
  const uint64_t *data = (const uint64_t *)_data;
  const uint64_t *end = data + (len/sizeof(*data));
  uint64_t h;

  h = seed ^ (len * m);
  while(data != end) {
    uint64_t k = *data++;

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;
  }

  const unsigned char * data2 = (const unsigned char*)data;

  switch(len & 7)
    {
    case 7:
      h ^= (uint64_t)(data2[6]) << 48;
    case 6:
      h ^= (uint64_t)(data2[5]) << 40;
    case 5:
      h ^= (uint64_t)(data2[4]) << 32;
    case 4:
      h ^= (uint64_t)(data2[3]) << 24;
    case 3:
      h ^= (uint64_t)(data2[2]) << 16;
    case 2:
      h ^= (uint64_t)(data2[1]) << 8;
    case 1:
      h ^= (uint64_t)(data2[0]);
      h *= m;
    };

  h ^= h >> r;
  h *= m;
  h ^= h >> r;

  /* xor-fold the 64 bit hash into 32 bits */
  return (uint32_t)((h & 0xFFFFFFFF) ^ ((h & 0xFFFFFFFF00000000ULL) >> 32));
}

/* ********************************************************* */
/* *** drivers */

static void*test_comp_instantiate(puk_instance_t ai, puk_context_t context);
static void test_comp_destroy(void*_instance);

static const struct puk_component_driver_s test_comp_component_driver =
  {
    .instantiate = &test_comp_instantiate,
    .destroy     = &test_comp_destroy
  };

static int test_comp_dummy(int a);

static const struct padico_dummy_driver_s test_comp_dummy_driver =
  {
    .dummy = &test_comp_dummy
  };

/* ********************************************************* */
/* *** hashing */

static inline uint32_t test_hash_int_default(const int i)
{
  return puk_hash_int((void*)i);
}
static inline uint32_t test_hash_int_murmurhash(const int i)
{
  return test_hash_murmurhash64a(&i, sizeof(i));
}
static inline int test_hash_int_eq(const int i, const int j)
{
  return (i == j);
}
PUK_HASHTABLE_TYPE(test_hash_default, int, void*,
                   &test_hash_int_default, &test_hash_int_eq, NULL);

PUK_HASHTABLE_TYPE(test_hash_murmur, int, void*,
                   &test_hash_int_murmurhash, &test_hash_int_eq, NULL);

/* ********************************************************* */
/* *** main */

static puk_component_t component;
static puk_instance_t instance;

int main(int argc, char**argv)
{

  putenv("PADICO_VERBOSE=yes");

  padico_puk_init();

  component =
    puk_component_declare("TestComponents",
                          puk_component_provides("PadicoComponent", "component", &test_comp_component_driver),
                          puk_component_provides("Dummy",         "dummy", &test_comp_dummy_driver));
  instance = puk_component_instantiate(component);

  int i;
  const int iters = 100000;
  padico_timing_t t1, t2;
  double t;

  padico_print("TestComponents: *** Components ***\n");

  /* *** iface_lookup */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_iface_lookup("Dummy");
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_iface_lookup: %fusec\n", t/iters);

  /* *** component_resolve */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_component_resolve("TestComponents");
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_component_resolve: %fusec\n", t/iters);

  /* *** get_driver */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_component_get_driver_Dummy(component, NULL);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_component_getdriver: %fusec\n", t/iters);

  /* *** instance_indirect */
  struct puk_receptacle_Dummy_s comp_receptacle;
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_instance_indirect_Dummy(instance, NULL, &comp_receptacle);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_instance_indirect: %fusec\n", t/iters);

  /* *** invoke: indirect, local */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      (*comp_receptacle.driver->dummy)(1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: invoke (indirect, local): %fusec\n", t/iters);

  /* *** invoke: indirect, extern */
  puk_mod_t mod = NULL;
  padico_puk_mod_open(&mod, "./TestFar");
  puk_component_t far_component = puk_component_resolve("TestFar");
  assert(far_component != NULL);
  puk_instance_t far_instance = puk_component_instantiate(far_component);
  struct puk_receptacle_Dummy_s far_dummy;
  puk_instance_indirect_Dummy(far_instance, NULL, &far_dummy);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      (*far_dummy.driver->dummy)(1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: invoke (indirect, extern): %fusec\n", t/iters);

  /* *** invoke: direct, local */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_comp_dummy(1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: invoke (direct, local): %fusec\n", t/iters);

  /* *** invoke: direct, extern */
#if 0
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_extern_dummy(1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: invoke (direct, extern): %fusec\n", t/iters);
#endif

  /* *** iface_lookup, instance_indirect & invoke */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_instance_indirect_Dummy(instance, NULL, &comp_receptacle);
      (*comp_receptacle.driver->dummy)(1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: iface_lookup, instance_indirect & invoke: %fusec\n", t/iters);


  padico_print("TestComponents: *** Data containers ***\n");

  /* *** hash function */
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_hash_int((void*)1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hash_int: %fusec\n", t/iters);

  unsigned char hashtest[128] = { 0 };
  int k;
  uint32_t z = 0;
  for(k = 0; k < 128; k++)
    {
      hashtest[k] = rand() % 256;
    }
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_hash_oneatatime(hashtest, 16);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hash_oneatatime(16): %fusec\n", t/iters);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      puk_hash_oneatatime(hashtest, 128);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hash_oneatatime(128): %fusec\n", t/iters);

  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_oneatatime(hashtest, 16);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_oneatatime(16): %fusec (%d)\n", t/iters, z);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_oneatatime(hashtest, 32);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_oneatatime(32): %fusec (%d)\n", t/iters, z);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_oneatatime(hashtest, 128);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_oneatatime(128): %fusec (%d)\n", t/iters, z);

    padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_murmurhash64a(hashtest, 16);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_murmurhash64a(16): %fusec (%d)\n", t/iters, z);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_murmurhash64a(hashtest, 32);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_murmurhash64a(32): %fusec (%d)\n", t/iters, z);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      z += test_hash_murmurhash64a(hashtest, 128);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: test_hash_murmurhash64a(128): %fusec (%d)\n", t/iters, z);


  /* *** hashtable default hash */
  test_hash_default_hashtable_t h1 = test_hash_default_hashtable_new();
  test_hash_default_hashtable_insert(h1, 1, instance);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_hash_default_hashtable_lookup(h1, 1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hashtable_lookup [default] (no load): %fusec\n", t/iters);
  for(i = 0; i < 1000; i++)
    {
      test_hash_default_hashtable_insert(h1, (rand()), instance);
    }
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_hash_default_hashtable_lookup(h1, 1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hashtable_lookup [default] (loaded): %fusec\n", t/iters);

  /* *** hashtable murmurhash64a */
  test_hash_murmur_hashtable_t h2 = test_hash_murmur_hashtable_new();
  test_hash_murmur_hashtable_insert(h2, 1, instance);
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_hash_murmur_hashtable_lookup(h2, 1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hashtable_lookup [murmurhash64a] (no load): %fusec\n", t/iters);
  for(i = 0; i < 1000; i++)
    {
      test_hash_murmur_hashtable_insert(h2, (rand()), instance);
    }
  padico_timing_get_tick(&t1);
  for(i = 0; i < iters; i++)
    {
      test_hash_murmur_hashtable_lookup(h2, 1);
    }
  padico_timing_get_tick(&t2);
  t = padico_timing_diff_usec(&t1, &t2);
  padico_print("TestComponents: puk_hashtable_lookup [murmurhash64a] (loaded): %fusec\n", t/iters);

  /* cleanup */
  test_hash_default_hashtable_delete(h1);
  test_hash_murmur_hashtable_delete(h2);
  puk_instance_destroy(instance);
  puk_instance_destroy(far_instance);
  puk_component_destroy(component);
  puk_component_destroy(far_component);

  /* *** memcpy */
  padico_print("TestComponents: *** Memcpy ***\n");

  {
    long source, dest;
    padico_timing_get_tick(&t1);
    for(i = 0; i < iters; i++)
      {
        memcpy(&dest, &source, sizeof(source));
      }
    padico_timing_get_tick(&t2);
    t = padico_timing_diff_usec(&t1, &t2);
    padico_print("TestComponents: memcpy sizeof(long): %fusec\n", t/iters);
  }
  {
    const int msize = 128;
    char source[msize], dest[msize];
    padico_timing_get_tick(&t1);
    for(i = 0; i < iters; i++)
      {
        memcpy(dest, source, msize);
      }
    padico_timing_get_tick(&t2);
    t = padico_timing_diff_usec(&t1, &t2);
    padico_print("TestComponents: memcpy 128 bytes: %fusec\n", t/iters);
  }
  {
    const int msize = 4096;
    char source[msize], dest[msize];
    padico_timing_get_tick(&t1);
    for(i = 0; i < iters; i++)
      {
        memcpy(dest, source, msize);
      }
    padico_timing_get_tick(&t2);
    t = padico_timing_diff_usec(&t1, &t2);
    padico_print("TestComponents: memcpy 4 KB: %fusec\n", t/iters);
  }
  return 0;
}

/* ********************************************************* */
/* *** implementation */

extern int test_comp_dummy(int a) __attribute__((noinline));
int test_comp_dummy(int a)
{
  return a;
}

static void*test_comp_instantiate(puk_instance_t instance, puk_context_t context)
{
  return padico_malloc(sizeof(int));
}

static void test_comp_destroy(void*_instance)
{
  free(_instance);
}
