#include <stdio.h>
#include <pthread.h>
#include <Padico/Puk.h>

PUK_ALLOCATOR_TYPE(test_allocator, int);

#define TEST_ALLOCATOR_NUM (1000 * 1000)
#define TEST_ALLOCATOR_ROUND 100
#define MAX_THREADS 16

static test_allocator_allocator_t allocator = NULL;

void*worker(void*_dummy)
{
  int**refs = malloc(TEST_ALLOCATOR_NUM * sizeof(int*));
  int i;
  for(i = 0; i < TEST_ALLOCATOR_NUM; i++)
    {
      refs[i] = test_allocator_malloc(allocator);
    }
  for(i = 0; i < TEST_ALLOCATOR_NUM; i++)
    {
      test_allocator_free(allocator, refs[i]);
    }
  free(refs);
  return NULL;
}

int main(int argc, char**argv)
{
  padico_puk_init();

  allocator = test_allocator_allocator_new(16);
  int r;
  for(r = 0; r < TEST_ALLOCATOR_ROUND; r++)
    {
      fprintf(stderr, "# round = %d; slab_size = %d\n", r, (int)allocator->slab_size);
      pthread_t tids[MAX_THREADS];
      int t;
      for(t = 0; t < MAX_THREADS; t++)
        {
          pthread_create(&tids[t], NULL, &worker, NULL);
        }
      for(t = 0; t < MAX_THREADS; t++)
        {
          pthread_join(tids[t], NULL);
        }
    }

  test_allocator_allocator_destroy(allocator);

  padico_puk_shutdown();
  return 0;
}
