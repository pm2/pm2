#!/bin/bash

#partitions=("haswell" "sklb")
#nb_cores=(32 48)
#nb_rank=(8)
#collectives=("iallgather")
#implems=("openmpi" "mpich" "mvapich" "madmpi" "mpc" "impi")

partitions=("sandy")
nb_cores=(16)
nb_rank=(8 32 64)
collectives=("ibcast" "ireduce" "ialltoall" "iallgather")
implems=("madmpi" "mpich" "openmpi" "impi" "mvapich")

mkdir -p ./build
for imp in "${implems[@]}"
do
    mkdir -p ./build/${imp}
    ( cd ./build/${imp} ; ../../configure MPICC=${MPI_DIR}/${imp}/bin/mpicc && make )
    if [ "x$?" != "x0" ]; then exit 1; fi
done
  #export CUR_MPI=impi;
#export CC_ARG="-cc=/usr/bin/gcc";
#make bench_nbc;
#export CC_ARG="";
for i in `seq 0 "${#partitions[@]}"`
do
  OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" srun -N1 -n1 -c${nb_cores[i]} -p${partitions[i]} --exclusive ./bench_raw_estim ./temp/${partitions[i]};
  for n in "${nb_rank[@]}"
  do
    for coll in "${collectives[@]}"
    do
      #openmpi
      #echo "OPENMPI: ${coll}, ${n} ranks, ${partitions[i]}";
      OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/openmpi/bin/mpiexec --map-by node --mca btl openib --mca  btl_openib_allow_ib 1 ./build/openmpi/bench_nbc -C ${coll} -T -o output/openmpi_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      #mpich
      #echo "MPICH: ${coll}, ${n} ranks, ${partitions[i]}";
      OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/mpich/bin/mpiexec --map-by node ./build/mpich/bench_nbc -C ${coll} -T -o output/mpich_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      MPICH_ASYNC_PROGRESS=1 OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -t240 -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/mpich/bin/mpiexec --map-by node ./build/mpich/bench_nbc -C ${coll} -T -o output/mpichASYNC_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      #mvapich
      #echo "MVAPICH: ${coll}, ${n} ranks, ${partitions[i]}";
      OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/mvapich/bin/mpiexec --map-by node ./build/mvapich/bench_nbc -C ${coll} -T -o output/mvapich_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      MV2_ASYNC_PROGRESS=1 MV2_OPTIMIZED_ASYNC_PROGRESS=1 OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/mvapich/bin/mpiexec --map-by node ./build/mvapich/bench_nbc -C ${coll} -T -o output/mvapichASYNC_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      #madmpi
      #echo "MADMPI: ${coll}, ${n} ranks, ${partitions[i]}";
      OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" NMAD_DRIVER=iblr2 salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/madmpi/bin/mpiexec ./build/madmpi/bench_nbc -C ${coll} -T -o output/madmpi_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&

      PIOM_VERBOSE=1 PIOM_DEDICATED=1 PIOM_DEDICATED_LEVEL=pu PIOM_DEDICATED_DISTRIB=first OMP_NUM_THREADS=$((${nb_cores[i]}-1)) GOMP_CPU_AFFINITY="1-${nb_cores[i]}" NMAD_DRIVER=iblr2 salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive ~/ptmp/mpi/madmpi/bin/mpiexec ./build/madmpi/bench_nbc -C ${coll} -T -o output/madmpi1DED_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
      #intelmpi
      #echo "INTELMPI: ${coll}, ${n} ranks, ${partitions[i]}";
      OMP_NUM_THREADS=${nb_cores[i]} GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive /ccc/products/intelmpi-2019.0.5.281/system/default/intel64/bin/mpiexec --map-by node ./build/impi/bench_nbc -C ${coll} -T -o output/impi_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]};
      I_MPI_ASYNC_PROGRESS=1 I_MPI_ASYNC_PROGRESS_THREADS=1 I_MPI_ASYNC_PROGRESS_PIN="0" OMP_NUM_THREADS=$((${nb_cores[i]}-1)) GOMP_CPU_AFFINITY="0-${nb_cores[i]}" salloc -N$n -n$n -c${nb_cores[i]} -p${partitions[i]} --exclusive /ccc/products/intelmpi-2019.0.5.281/system/default/intel64/bin/mpiexec --map-by node ./build/impi/bench_nbc -C ${coll} -T -o output/impi1DED_${coll}_${n}_${partitions[i]} -b ./temp/${partitions[i]}&
    done
  done
done
