/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "bench_launcher.h"


int main(int argc, char** argv)
{
  int myrank, nbprocs,provided;
  uint64_t t = 0;
#ifdef GLOBAL_BUFFER
  data = malloc(2000000000);
  data2 = malloc(2000000000);
  a=malloc(5000*5000*sizeof(double));
  b=malloc(5000*5000*sizeof(double));
  res=calloc(5000*5000,sizeof(double));
#endif
  srand(1994);
  bench_options_t*options = arg_parser(argc,argv);
  comp_time_info_t**comp_times;
  MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE,&provided);
  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
  MPI_Comm_size(MPI_COMM_WORLD,&nbprocs);
    print_topo(myrank);
  double freq = estimate_frequency(myrank);
  comp_times = init_comp_time(options);
  int i;
  coll_time_info_t**coll_times = estimate_coll_times(options->flags,options->coll_time,options->data_size,myrank,nbprocs,freq,options->collective);
  if(myrank == ROOT){
    get_time_r(&t);
  }

  bench_general_t*general = malloc(sizeof(bench_general_t));
  general->options = options;
  general->measures = malloc(sizeof(bench_measures_t));
  general->measures->tgeneral.all_ranks = malloc(sizeof(double)*NB_REP);
  general->measures->tcall.all_ranks = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc.all_ranks = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc_ref.all_ranks = malloc(sizeof(double)*NB_REP);
  general->measures->tcoll_ref.all_ranks = malloc(sizeof(double)*NB_REP);

  general->measures->tgeneral.median = malloc(sizeof(double)*NB_REP);
  general->measures->tcall.median = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc.median = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc_ref.median = malloc(sizeof(double)*NB_REP);
  general->measures->tcoll_ref.median = malloc(sizeof(double)*NB_REP);

  general->measures->tgeneral.min = malloc(sizeof(double)*NB_REP);
  general->measures->tcall.min = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc.min = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc_ref.min = malloc(sizeof(double)*NB_REP);
  general->measures->tcoll_ref.min = malloc(sizeof(double)*NB_REP);

  general->measures->tgeneral.max = malloc(sizeof(double)*NB_REP);
  general->measures->tcall.max = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc.max = malloc(sizeof(double)*NB_REP);
  general->measures->tcalc_ref.max = malloc(sizeof(double)*NB_REP);
  general->measures->tcoll_ref.max = malloc(sizeof(double)*NB_REP);

  general->output=init_file_descriptors(options,myrank);
  general->freq=freq;
  general->ref_comp_time = comp_times;
  general->ref_coll_time = coll_times;
  general->myrank = myrank;
  general->nbprocs = nbprocs;
#ifdef SYNC_2
  general->clock = malloc(sizeof(mpi_sync_clocks_t*));
#endif
  for(i=0; i < NB_MATRIX_SIZE ;i++){
    fprintf(stderr,"%d %d %.2lf-%.2lf-%.2lf;%.2lf\n",comp_times[i]->theorical_time,comp_times[i]->matrix_size,comp_times[i]->real_time->min[0],comp_times[i]->real_time->max[0],comp_times[i]->real_time->median[0],sqrt(comp_times[i]->real_time->var[0]));
  }
  broadcast_comp_time(general);
  /*Test a specific data size/computation time couple*/
  if(options->comp_time > 0 || options->mat_size > 0)
  {
    bench_specific(general);
  }
  /*General benchmark*/
  else
  {
    bench_all(general);
  }
  if(myrank == ROOT)
    print_statistics(t,general->options->collective,general->freq,general->output);
  MPI_Finalize();

//  freq = estimate_frequency(myrank);
//  coll_time_info_t**coll_times2 = estimate_coll_times(options->flags,options->coll_time,options->data_size,myrank,nbprocs,freq,options->collective);
  //free_all(general);

  return 0;
}

void print_topo(int myrank){
  hwloc_topology_t topo;
  hwloc_topology_init(&topo);
  hwloc_topology_load(topo);
  int num=0, i,nbpu=hwloc_get_nbobjs_by_type(topo,HWLOC_OBJ_PU);
  short count[nbpu];
  for(i = 0; i < nbpu; i++)
    count[i]=0;
#pragma omp parallel
  {
  hwloc_cpuset_t cpuset = hwloc_bitmap_alloc();
  hwloc_get_last_cpu_location(topo,cpuset,HWLOC_CPUBIND_THREAD);
  hwloc_obj_t o = hwloc_get_first_largest_obj_inside_cpuset(topo,cpuset);
  num = omp_get_num_threads();
  #pragma omp atomic
  count[o->logical_index]++;
  hwloc_bitmap_free(cpuset);
  }
  hwloc_topology_destroy(topo);
  if(myrank == ROOT){
    fprintf(stderr,"%d OMP thread(s)\n",num);
  for(i = 0; i < nbpu; i++){
      fprintf(stderr,"[PU%d]:%d thread(s)\n",i,count[i]);
  }
  }
}
void free_all(bench_general_t*general){
  int i;
  free(general->ref_comp_time);
  free(general->ref_coll_time);
  for(i = 0; i < NB_OUTPUT_FILES + (general->options->flags & ALL_RANK_PRINT); i++){
    free(general->output[i]);
  }
  for(i = 0; i < general->nbprocs; i++){
    free(general->measures->tgeneral.all_ranks[i]);
    free(general->measures->tcall.all_ranks[i]);
    free(general->measures->tcalc.all_ranks[i]);
    free(general->measures->tcalc_ref.all_ranks[i]);
    free(general->measures->tcoll_ref.all_ranks[i]);
  }
    free(general->measures->tgeneral.all_ranks);
    free(general->measures->tcall.all_ranks);
    free(general->measures->tcalc.all_ranks);
    free(general->measures->tcalc_ref.all_ranks);
    free(general->measures->tcoll_ref.all_ranks);

    free(general->measures->tgeneral.median);
    free(general->measures->tcall.median);
    free(general->measures->tcalc.median);
    free(general->measures->tcalc_ref.median);
    free(general->measures->tcoll_ref.median);

    free(general->measures->tgeneral.min);
    free(general->measures->tcall.min);
    free(general->measures->tcalc.min);
    free(general->measures->tcalc_ref.min);
    free(general->measures->tcoll_ref.min);

    free(general->measures->tgeneral.max);
    free(general->measures->tcall.max);
    free(general->measures->tcalc.max);
    free(general->measures->tcalc_ref.max);
    free(general->measures->tcoll_ref.max);

    free(general->measures);
  free(general->options);
  free(general);
}

void broadcast_comp_time(bench_general_t*general){
  int i;
  if(general->options->flags & USE_STATIC || general->options->comp_time > 0 || general->options->mat_size > 0){
    return;
  }
  for(i = 0; i < NB_MATRIX_SIZE; i++){
    MPI_Bcast(&general->ref_comp_time[i]->theorical_time,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    MPI_Bcast(&general->ref_comp_time[i]->matrix_size,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    //MPI_Bcast(&general->ref_comp_time[i]->real_time,1,MPI_INT,ROOT,MPI_COMM_WORLD);
  }
}
