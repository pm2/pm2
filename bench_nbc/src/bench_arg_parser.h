/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef BENCH_ARG_PARSER_H
#define BENCH_ARG_PARSER_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bench_constants.h"

typedef struct bench_raw_cpu_s
{
  int t_time;
  double r_time;
  int m_size;
  unsigned long int nb_op;
} bench_raw_cpu_t;

typedef struct bench_options_s
{
  int data_size;
  int coll_time;
  int mat_size;
  int comp_time;
  int flags;
  char* file_prefix;
  int collective;
  bench_raw_cpu_t*raw_nbop;

} bench_options_t;

FILE**init_file_descriptors(bench_options_t*,int);
bench_options_t*arg_parser(int , char**);
int get_coll(char*);
bench_raw_cpu_t*read_nbop(char*);
#endif
