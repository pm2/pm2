/*
 * Bench NBC
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef BENCH_COLL_TIME_ESTIM_H
#define BENCH_COLL_TIME_ESTIM_H
#include "bench_constants.h"
#include "misc.h"
#include "math/Matrix.h"
#include <mpi.h>
typedef struct coll_time_info_s
{
  int theorical_time;
  bench_time_t*real_time;
  int data_size;
} coll_time_info_t;

coll_time_info_t**estimate_coll_times(int,int,int,int,int,double,int);
int data_size_from_time(coll_time_info_t*coll,double target,int myrank,int nbprocs,double freq,int collective, int flags);
bench_time_t*real_time_from_data_size(coll_time_info_t*t, int collective,double freq, int myrank, int nbprocs, int flags);

#ifdef GLOBAL_BUFFER
void start_collective(int,int,MPI_Request*,int);
void block_collective(int,int,int);
#else
void start_collective(void*,void*,int,int,MPI_Request*,int);
void block_collective(void*,void*,int,int,int);
#endif

#endif /* BENCH_COLL_TIME_ESTIM_H */
