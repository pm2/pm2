/* ############################ MALP License ############################## */
/* # Fri Jan 18 14:00:00 CET 2013                                         # */
/* # Copyright or (C) or Copr. Commissariat a l'Energie Atomique          # */
/* #                                                                      # */
/* # This software is governed by the CeCILL-C license under French law   # */
/* # and abiding by the rules of distribution of free software.  You can  # */
/* # use, modify and/ or redistribute the software under the terms of     # */
/* # the CeCILL-C license as circulated by CEA, CNRS and INRIA at the     # */
/* # following URL http://www.cecill.info.                                # */
/* #                                                                      # */
/* # The fact that you are presently reading this means that you have     # */
/* # had knowledge of the CeCILL-C license and that you accept its        # */
/* # terms.                                                               # */
/* #                                                                      # */
/* # Authors:                                                             # */
/* #   - BESNARD Jean-Baptiste jean-baptiste.besnard@cea.fr               # */
/* #                                                                      # */
/* ######################################################################## */
#ifndef TIMER_H
#define TIMER_H

#ifdef __cplusplus
extern "C"
{
#endif



#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>


    void Timer_set_origin();
    void Timer_set_offset(long long int offset );

    void Timer_Init_scale();

    extern long long int Process_Sync_Offset;
    extern uint64_t Process_time_origin;
    extern double Process_timer_second;


    static inline double Timer_second()
    {
        return Process_timer_second;
    }

#if (defined(__GNUC__) || defined(__ICC))

    static inline void sctk_rdtsc_barrier(void)
    {
        asm("mfence");
        asm("lfence");
    }

    static inline void Timer_tsc_ref(uint64_t *tsc_ret){

        uint64_t tsc;
        __asm__ __volatile__("rdtscp; "         // serializing read of tsc
                "shl $32,%%rdx; "// shift higher 32 bits stored in rdx up
                "or %%rdx,%%rax"// and or onto rax
                : "=a"(tsc)// output to tsc variable
                :
                : "%rcx", "%rdx");// rcx and rdx are clobbered
        *tsc_ret = tsc + Process_Sync_Offset - Process_time_origin;
    }
#if defined(__ia64__)
    static inline uint64_t Timer_tsc (void)
    {
        //if( Process_Sync_Offset )
        //printf("\n\nOFF : %d \n",Process_Sync_Offset);
        unsigned long t;
        __asm__ volatile ("mov %0=ar%1":"=r" (t):"i" (44));
        return (uint64_t) t + Process_Sync_Offset - Process_time_origin;
    }
#elif defined(__i686__)
    static inline uint64_t Timer_tsc (void)
    {


        unsigned long long t;
        sctk_rdtsc_barrier();
        __asm__ volatile ("rdtsc":"=A" (t));
        return (uint64_t) t + Process_Sync_Offset - Process_time_origin;
    }



#elif defined(__x86_64__)
    static inline uint64_t Timer_tsc (void)
    {

#define RDTSCP_BASED_SYNC
#if defined(RDTSCP_BASED_SYNC)
        uint64_t tsc;
        __asm__ __volatile__("rdtscp; "         // serializing read of tsc
                "shl $32,%%rdx; "// shift higher 32 bits stored in rdx up
                "or %%rdx,%%rax"// and or onto rax
                : "=a"(tsc)// output to tsc variable
                :
                : "%rcx", "%rdx");// rcx and rdx are clobbered
        return tsc + Process_Sync_Offset - Process_time_origin;

#elif defined(RDTSC_BASED_SYNC)

        unsigned int a;
        unsigned int d;
        unsigned long t;
        sctk_rdtsc_barrier();
        __asm__ volatile ("rdtsc":"=a" (a), "=d" (d));
        t = ((unsigned long) a) | (((unsigned long) d) << 32);
        uint64_t ret = (uint64_t) t + Process_Sync_Offset - Process_time_origin;

        return ret;
#else
        // Use clock getime
        uint64_t usec;
        struct timespec t;
#if defined(CLOCK_MONOTONIC_RAW)
        clock_gettime(CLOCK_MONOTONIC_RAW,&t);
#else
        clock_gettime(CLOCK_MONOTONIC,&t);
#endif
        usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;
        return usec + Process_Sync_Offset - Process_time_origin;
#endif
    }

#else
#warning "Use get time of day for profiling, interprocess sync will not work !"

#include<sys/time.h>

    static inline uint64_t Timer_tsc (void)
    {
        struct timeval tp;
        gettimeofday (&tp, NULL);
        return tp.tv_usec + tp.tv_sec * 1000000;
    }
#endif

#else
#error Compile with GCC or ICC

#endif

#ifdef __cplusplus
}
#endif




#endif
