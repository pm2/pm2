/* ############################ MALP License ############################## */
/* # Fri Jan 18 14:00:00 CET 2013                                         # */
/* # Copyright or (C) or Copr. Commissariat a l'Energie Atomique          # */
/* #                                                                      # */
/* # This software is governed by the CeCILL-C license under French law   # */
/* # and abiding by the rules of distribution of free software.  You can  # */
/* # use, modify and/ or redistribute the software under the terms of     # */
/* # the CeCILL-C license as circulated by CEA, CNRS and INRIA at the     # */
/* # following URL http://www.cecill.info.                                # */
/* #                                                                      # */
/* # The fact that you are presently reading this means that you have     # */
/* # had knowledge of the CeCILL-C license and that you accept its        # */
/* # terms.                                                               # */
/* #                                                                      # */
/* # Authors:                                                             # */
/* #   - BESNARD Jean-Baptiste jean-baptiste.besnard@cea.fr               # */
/* #                                                                      # */
/* ######################################################################## */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <dlfcn.h>
#include <unistd.h>
#include <mpi.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "Timer.h"



#ifndef SYNC_TIME
#define SYNC_TIME

#define MAX_LOOP_TRY 20
#define SYNC_TAG 1234
#define MAX_AVG 10



struct sync_tree_conf {
	int source_node;
	int target_node;
	int current_pow;
	int comm_size;
	long long int offset;
};





void sync_mpi();



//TREE
void compute_sync_tree( struct sync_tree_conf *conf,
                        void (*action)(struct sync_tree_conf *next_conf, void *arg),
                        void (*per_child_act)(struct sync_tree_conf *conf, void *arg ),
                        void (*post_action)(struct sync_tree_conf *next_conf, void *arg),
                        void *arg
                      );




void bootstrap_sync_tree( void (*action)(struct sync_tree_conf *next_conf, void *arg),
                          void (*per_child_act)(struct sync_tree_conf *conf , void *arg),
                          void (*post_action)(struct sync_tree_conf *next_conf, void *arg),
                          void *arg
                        );

//TREE ACTIONS
void perchild_host(struct sync_tree_conf *c, void *arg);
void post_client(struct sync_tree_conf *c, void *arg);
void action_set_off(struct sync_tree_conf *c, void *poffset);
// ~~~ DBG
void action_val(struct sync_tree_conf *c, void *arg);



//SYNC

void sync_server( int dest_rank );
void sync_client(int parent, void *poffset);



extern long long int Process_Sync_Offset;



//UTILITIES FUNCTIONS

static inline uint64_t abs_diff( uint64_t a , uint64_t b )
{
	if( a < b )
		return b - a;
	else
		return a - b;
}

static inline uint32_t nearest_pow (uint32_t num)
{
	uint32_t n = num > 0 ? num - 1 : 0;

	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n++;

	return n;
}




#endif
