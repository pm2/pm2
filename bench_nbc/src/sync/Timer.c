/* ############################ MALP License ############################## */
/* # Fri Jan 18 14:00:00 CET 2013                                         # */
/* # Copyright or (C) or Copr. Commissariat a l'Energie Atomique          # */
/* #                                                                      # */
/* # This software is governed by the CeCILL-C license under French law   # */
/* # and abiding by the rules of distribution of free software.  You can  # */
/* # use, modify and/ or redistribute the software under the terms of     # */
/* # the CeCILL-C license as circulated by CEA, CNRS and INRIA at the     # */
/* # following URL http://www.cecill.info.                                # */
/* #                                                                      # */
/* # The fact that you are presently reading this means that you have     # */
/* # had knowledge of the CeCILL-C license and that you accept its        # */
/* # terms.                                                               # */
/* #                                                                      # */
/* # Authors:                                                             # */
/* #   - BESNARD Jean-Baptiste jean-baptiste.besnard@cea.fr               # */
/* #                                                                      # */
/* ######################################################################## */
#include "Timer.h"
#include <unistd.h>

long long int Process_Sync_Offset = 0;
uint64_t Process_time_origin = 0;
double Process_timer_second = 1;

void Timer_set_origin()
{
	Process_time_origin = Timer_tsc();
}

void Timer_set_offset(long long int offset )
{
	Process_Sync_Offset = offset;
}


void Timer_Init_scale()
{
	double ret = 0;

	uint64_t begin = 0, end = 0;
	double t_begin = 0, t_end = 0;

	struct timeval b_tv;
	struct timeval e_tv;

	//printf( "Calibrating timer...");
	gettimeofday( &b_tv , NULL );
	begin = Timer_tsc ();
	sleep( 1 );
	end = Timer_tsc ();
	gettimeofday( &e_tv , NULL );

	t_begin = b_tv.tv_sec + b_tv.tv_usec/1000000;
	t_end = e_tv.tv_sec + e_tv.tv_usec/1000000;

	ret = (end-begin)/(t_end-t_begin);

	Process_timer_second = ret;
}
