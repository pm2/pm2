/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "Matrix.h"
#include "../bench_constants.h"
extern int sctk_thread_yield(void);
#define COLMAJOR

// matrix addition with double
void Matrix_add(double *a, double *b, double *res, long int size) {
  assert(a != NULL);
  assert(b != NULL);
  assert(res != NULL);

  long int i, j;

  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      res[icm(size, i, j)] = a[icm(size, i, j)] + b[icm(size, i, j)];
    }
  }
}

// matrix multiplication with double
void Matrix_mul(double *a, double *b, double *res, long int size) {
  assert(a != NULL);
  assert(b != NULL);
  assert(res != NULL);

  long int i, j, k;

  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      for (k = 0; k < size; k++) {
        res[icm(size, i, j)] += a[icm(size, i, k)] * b[icm(size, k, j)];
      }
      //sctk_thread_yield();
    }
  }
}


long int init_compute(double time){
  int old_size = 1;
  int size,i,closer_size=0, count=0;
  //size of matrices

  // double *a,*b,*res;
  //times
  //uint64_t t,t1,old_t,t_cpu_exploit = 0;
  double old_t = -1,t_cpu_exploit = 0,closer_delta=10e9;
  struct timespec t,t1;
  size = 100;
  //fprintf(stderr,"approximate matrix size:");
  double *tab;
#ifdef GLOBAL_BUFFER
  matrix_compute(size);
#else
  double *a,*b,*res;
  a=malloc(size*size*sizeof(double));
  b=malloc(size*size*sizeof(double));
  res=calloc(size*size,sizeof(double));
  matrix_compute(a,b,res,size);
  free(a);
  free(b);
  free(res);
#endif
  size = 2;
  do{


    //    else if(t_cpu_exploit > time){
    //      over=1;
    //      size = (old_size + size) / 2;
    //    }


    int nb_rep = get_nb_rep(0,size,NB_REP);
    tab=malloc(sizeof(double)*nb_rep);
    for(i = 0; i < nb_rep; i++){
#ifdef GLOBAL_BUFFER
      clock_gettime(CLOCK_MONOTONIC,&t);
      matrix_compute(size);
      clock_gettime(CLOCK_MONOTONIC,&t1);
      tab[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
#else
      a=malloc(size*size*sizeof(double));
      b=malloc(size*size*sizeof(double));
      res=calloc(size*size,sizeof(double));

      //test if we have a matrix size which have a "time" compute time
      /*get_time_r(&t);
        matrix_compute(a,b,res,size);
      //get_time_r(&t1);
      old_t = t_cpu_exploit;
      t_cpu_exploit = t1 - t;*/
      clock_gettime(CLOCK_MONOTONIC,&t);
      matrix_compute(a,b,res,size);
      clock_gettime(CLOCK_MONOTONIC,&t1);
      tab[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
      //fprintf(stderr,"size=%ld time=%lf t_cpu_exploit=%lf\n",size,time,t_cpu_exploit);

      free(a);
      free(b);
      free(res);
#endif
    }
    old_t = t_cpu_exploit;
    t_cpu_exploit = median(tab,nb_rep);
    free(tab);
    int temp = interpolate(old_size,size,(double)old_t,(double)t_cpu_exploit,(double)time);
    if(fabs((time-t_cpu_exploit)) < closer_delta){
      closer_size = size;
      closer_delta = fabs(time-t_cpu_exploit);
    }
    if(closer_delta < time/100 || count > 100){
      return closer_size;
    }
    old_size = size;
    size = temp;
    count++;

  }
  while(1);
  return size;
}
#ifdef GLOBAL_BUFFER
void matrix_compute(long int size) {
#else
void matrix_compute(double *a, double *b, double *res,long int size) {
#endif
#pragma omp parallel
  {
    long int i, j, k;
#pragma omp for collapse(3)
    for (i = 0; i < size; i++) {
      for (j = 0; j < size; j++) {
        for (k = 0; k < size; k++) {
          res[icm(size, i, j)] += a[icm(size, i, k)] * b[icm(size, k, j)];
        }
        //sctk_thread_yield();
      }
    }

  }
}

long long int interpolate(long long int n1, long long int n2,double t1, double t2, double target){
  /*t = alpha n^3 +c*/
  /* t1-t2 = alpha (n1^3 -n2^3) <=> alpha = (t1-t2)/(n1^3-n2^3)*/
  double alpha = (t1-t2)/(n1*n1*n1-n2*n2*n2);
  /*c = t1-alpha n1^3*/
  long double c = t1 - alpha *n1*n1*n1;
  /*n=sqrt3((t-c)/alpha) */
  long double n = fabs(cbrtf((target-c)/alpha));
  //fprintf(stderr,"n1:%lld n2:%lld t1:%lf t2:%lf target:%f\nalpha:= %g, c= %Lf,n(double) = %Lf, n = %lld\n",n1,n2,t1,t2,target,alpha, c,n, (long long int)n);
  return (long long int)n;
}
