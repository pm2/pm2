/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef BENCH_CORE_H
#define BENCH_CORE_H
#include "bench_constants.h"
#include "misc.h"
#include "math/Matrix.h"
#include "bench_arg_parser.h"
#include "bench_coll_time_estim.h"


typedef struct bench_measures_s
{
  bench_time_t tgeneral;
  bench_time_t tcall;
  bench_time_t tcalc;
  bench_time_t tcalc_ref;
  bench_time_t tcoll_ref;

}bench_measures_t;

typedef struct bench_general_s
{
  bench_options_t*options;
  comp_time_info_t**ref_comp_time;
  coll_time_info_t**ref_coll_time;
  bench_measures_t*measures;
  FILE**output;
  double freq;
  int myrank;
  int nbprocs;
#ifdef SYNC_2
  mpi_sync_clocks_t*clock;
#endif
}bench_general_t;

extern void bench_specific(bench_general_t*);
extern void bench_all(bench_general_t*);
extern void print_raw_times(bench_general_t*);
extern void print_statistics(uint64_t,int,double,FILE**);
extern uint64_t compute(uint64_t);
extern void sync_start(int);
extern double overlap_ratio(double,double,double);
extern void print_first_line(int,FILE**,comp_time_info_t**);
extern void bench_collective(bench_general_t*,int,int,double*,double*,double*);
extern void gather_results(bench_general_t*,double*,double*,double*,int,int,int);
extern void print_results(bench_general_t*,int,int,int);
extern comp_time_info_t**init_comp_time(bench_options_t*);
extern double estimate_frequency(int);
#endif
