/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "coll_sync_test.h"

#define ROOT 0
#define NB_REP 100
#define NB_REP_FREQ 25
#define NB_BYTE 8
int main(int argc, char** argv)
{

    int myrank, nbprocs;
    MPI_Init(&argc,&argv);
    int j;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nbprocs);
    uint64_t t0,t1;
    double d0,d1,*t0_tab=NULL,*t1_tab=NULL,*tres_tab=NULL;
    void* data = malloc(NB_BYTE);
    void* data2 = malloc(NB_BYTE*nbprocs);
    double freq;
    if(myrank == ROOT)
    {
        double* tab = malloc(sizeof(double)*NB_REP_FREQ);
        fprintf(stderr,"estimating processor frequence ...\n");
        for(j = 0; j < NB_REP_FREQ; j++){
            tab[j]  = get_proc_freq();
        }
        freq = median(tab,NB_REP_FREQ);
        fprintf(stderr,"%f Mhz estimated\n",1/freq);
        t0_tab = malloc(sizeof(double)*nbprocs);
        t1_tab = malloc(sizeof(double)*nbprocs);
        tres_tab = malloc(sizeof(double)*NB_REP);
    }
    MPI_Bcast(&freq,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);

    for(j=0; j < NB_REP;j++)
    {
        Process_Sync_Offset = 0;
        sync_mpi();
        MPI_Barrier(MPI_COMM_WORLD);
        t0 = Timer_tsc();
        MPI_Alltoall(data,NB_BYTE,MPI_BYTE,data2,NB_BYTE,MPI_BYTE,MPI_COMM_WORLD);
        t1 = Timer_tsc();

        d0 = (double)t0*freq;
        d1 = (double)t1*freq;
        MPI_Gather(&d0,1,MPI_DOUBLE,t0_tab,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
        MPI_Gather(&d1,1,MPI_DOUBLE,t1_tab,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
        if(myrank == ROOT){
            double delta = max(t1_tab,nbprocs) - min(t1_tab,nbprocs);
            tres_tab[j] = delta;
        }
    }

    if(myrank == ROOT)
        fprintf(stdout,"[%d ranks] mean delta:%lf\n          median delta:%lf\ncollective time:%lf\n\n",nbprocs,mean(tres_tab,NB_REP),median(tres_tab,NB_REP),d1 - d0);
    MPI_Finalize();
}
