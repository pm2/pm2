/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "bench_core.h"

void bench_specific(bench_general_t*general){
  int j;

  int nb_rep = get_nb_rep(general->ref_coll_time[0]->data_size,general->ref_comp_time[0]->matrix_size,NB_REP);
#ifdef SYNC_2
  *general->clock = mpi_sync_clocks_init(MPI_COMM_WORLD);
#endif
  for(j = 0; j < nb_rep; j++){
    double t1,t2,t3;
    bench_collective(general,0,0,&t1,&t2,&t3);
    gather_results(general,&t1,&t2,&t3,j,0,0);
  }
#ifdef SYNC_2
mpi_sync_clocks_shutdown(*general->clock);
#endif
  print_results(general,0,0,nb_rep);
  print_raw_times(general);
  return;
}

void bench_all(bench_general_t*general){
  int j, data_size_index,matrix_size_index;

  print_raw_times(general);
  print_first_line(general->myrank,general->output,general->ref_comp_time);
  for(data_size_index = 0; data_size_index < NB_DATA_SIZE; data_size_index++){

   /* if(general->myrank == ROOT && general->options->flags & USE_COLL_TIME){
      fprintf(general->output[0],"%d ",general->ref_coll_time[data_size_index]->theorical_time);
      fprintf(general->output[1],"%d ",general->ref_coll_time[data_size_index]->theorical_time);
      fprintf(general->output[2],"%d ",general->ref_coll_time[data_size_index]->theorical_time);
    }

    else*/ if(general->myrank == ROOT){
      fprintf(general->output[0],"%d-%d-%2f ",general->ref_coll_time[data_size_index]->data_size,general->ref_coll_time[data_size_index]->theorical_time,general->ref_coll_time[data_size_index]->real_time->median[0]);
      fprintf(general->output[2],"[%d-%d-%2f]\n\n",general->ref_coll_time[data_size_index]->data_size,general->ref_coll_time[data_size_index]->theorical_time,general->ref_coll_time[data_size_index]->real_time->median[0]);
    }

#ifdef SYNC_2
    *general->clock = mpi_sync_clocks_init(MPI_COMM_WORLD);
#endif
    for(matrix_size_index = 0; matrix_size_index < NB_MATRIX_SIZE ; matrix_size_index++){
      int nb_rep = get_nb_rep(general->ref_coll_time[data_size_index]->data_size,general->ref_comp_time[matrix_size_index]->matrix_size,NB_REP);

      for(j = 0; j < nb_rep; j++){

        double t1,t2,t3;
        bench_collective(general,data_size_index,matrix_size_index,&t1,&t2,&t3);
        gather_results(general,&t1,&t2,&t3,j,matrix_size_index,data_size_index);

      }
      print_results(general,matrix_size_index,data_size_index,nb_rep);
    }
#ifdef SYNC_2
mpi_sync_clocks_shutdown(*general->clock);
#endif
    if(general->myrank == ROOT){
      fprintf(general->output[0],"\n");
      fprintf(general->output[1],"\n");
      fprintf(general->output[2],"\n");
    }
    if(general->options->flags & ALL_RANK_PRINT)
      fprintf(general->output[3],"\n");
  }
}
void print_raw_times(bench_general_t*general){
  if(general->myrank != ROOT) return;

  FILE**output = general->output;

  if(general->options->comp_time || general->options->mat_size){

      unsigned long int exp_nb_op = (unsigned long int)general->ref_comp_time[0]->matrix_size*(unsigned long int)general->ref_comp_time[0]->matrix_size*((unsigned long int)general->ref_comp_time[0]->matrix_size+((unsigned long int)general->ref_comp_time[0]->matrix_size - 1));
    fprintf(output[2],"\n#COLLECTIVE:theorical_time size min max median variance(S.D)\n");
    fprintf(output[2],"%d %d %.2lf %.2lf %.2lf %.2lf(%.2lf)\n",general->ref_coll_time[0]->theorical_time,general->ref_coll_time[0]->data_size,general->ref_coll_time[0]->real_time->min[0],general->ref_coll_time[0]->real_time->max[0],general->ref_coll_time[0]->real_time->median[0],general->ref_coll_time[0]->real_time->var[0],sqrt(general->ref_coll_time[0]->real_time->var[0]));
    fprintf(output[2],"\n\n");
    fprintf(output[2],"#COMPUTE:theorical_time size nbop min max median variance(S.D)\n");
    fprintf(output[2],"%d %d %ld %.2lf %.2lf %.2lf %.2lf(%.2lf)\n",general->ref_comp_time[0]->theorical_time,general->ref_comp_time[0]->matrix_size,exp_nb_op,general->ref_comp_time[0]->real_time->min[0],general->ref_comp_time[0]->real_time->max[0],general->ref_comp_time[0]->real_time->median[0],general->ref_comp_time[0]->real_time->var[0],sqrt(general->ref_comp_time[0]->real_time->var[0]));
    fprintf(output[2],"\n\n");
    fprintf(output[2],"#NO MPI COMPUTE:theorical_time size nbop real_comp_time\n");
    fprintf(output[2],"%d %d %ld %.2lf\n",general->options->raw_nbop[0].t_time,general->options->raw_nbop[0].m_size,general->options->raw_nbop[0].nb_op,general->options->raw_nbop[0].r_time);
    fprintf(output[2],"\n\n");
    return;
    ;
  }

  int i;
    fprintf(output[2],"\n#COLLECTIVE:theorical_time size min max median variance(S.D)\n");
  for(i = 0; i < NB_DATA_SIZE; i++){

    fprintf(output[2],"%d %d %.2lf %.2lf %.2lf %.2lf(%.2lf)\n",general->ref_coll_time[i]->theorical_time,general->ref_coll_time[i]->data_size,general->ref_coll_time[i]->real_time->min[0],general->ref_coll_time[i]->real_time->max[0],general->ref_coll_time[i]->real_time->median[0],general->ref_coll_time[i]->real_time->var[0],sqrt(general->ref_coll_time[i]->real_time->var[0]));
  }
  fprintf(output[2],"\n\n");
  fprintf(output[2],"#COMPUTE:theorical_time size min max median variance(S.D)\n");
  for(i = 0; i < NB_MATRIX_SIZE; i++){
      unsigned long int exp_nb_op = (unsigned long int)general->ref_comp_time[i]->matrix_size*(unsigned long int)general->ref_comp_time[i]->matrix_size*((unsigned long int)general->ref_comp_time[i]->matrix_size+((unsigned long int)general->ref_comp_time[i]->matrix_size - 1));
    fprintf(output[2],"%d %d %ld %.2lf %.2lf %.2lf %.2lf(%.2lf)\n",general->ref_comp_time[i]->theorical_time,general->ref_comp_time[i]->matrix_size,exp_nb_op,general->ref_comp_time[i]->real_time->min[0],general->ref_comp_time[i]->real_time->max[0],general->ref_comp_time[i]->real_time->median[0],general->ref_comp_time[i]->real_time->var[0],sqrt(general->ref_comp_time[i]->real_time->var[0]));
  }
    fprintf(output[2],"\n\n");
    fprintf(output[2],"#NO MPI COMPUTE:theorical_time size nbop real_comp_time\n");
  for(i = 0; i < NB_MATRIX_SIZE; i++){
    fprintf(output[2],"%d %d %lu %.2lf\n",general->options->raw_nbop[i].t_time,general->options->raw_nbop[i].m_size,general->options->raw_nbop[i].nb_op,general->options->raw_nbop[i].r_time);
  }
  fprintf(output[2],"\n\n");

}
void print_statistics(uint64_t t, int call_collective, double freq,FILE** output){

  uint64_t tt;
  get_time_r(&tt);
  int totaltime = (double) (tt - t)*freq/1000000;
  int h = totaltime / 3600;
  totaltime %= 3600;
  int m = totaltime / 60;
  totaltime %= 60;
  fprintf(output[2],"\n\n#collective:%d execution time:%dh,%dm,%ds\n",call_collective,h,m,totaltime);
}

uint64_t compute(uint64_t nb_tick)
{
  uint64_t t_origin = get_time();
  uint64_t d = 0;
  while(get_time() - t_origin < nb_tick){
    d += (rand() * rand()) % 1000;
  }
  return d;
}

#ifdef SYNC_1
void sync_start(int myrank)
{
  uint64_t rdv=0,t=0;
  if(myrank == ROOT)
    Timer_tsc_ref(&rdv);
  rdv += 2*10e6;
  MPI_Bcast(&rdv,1,MPI_UNSIGNED_LONG,ROOT,MPI_COMM_WORLD);
  do{Timer_tsc_ref(&t);}while(t < rdv);
  //fprintf(stderr,"GO\n");
}
#endif /* SYNC_1 */

double overlap_ratio(double ttotal,double tcalc,double tcoll)
{

  double toverlap = 0;
  double tmin = 0;
  if(tcalc > tcoll){
    toverlap = tcalc;
    tmin = tcoll;
  } else {
    toverlap = tcoll;
    tmin = tcalc;
  }
  /*fprintf(stderr,"toverlap:%lf\n",toverlap);
    fprintf(stderr,"tmin:%lf\n",tmin);*/

  // fprintf(stderr,"tgeneral:%lf\n",ttotal);
  // fprintf(stderr,"tcoll_p:%lf\n",tcoll);
  // fprintf(stderr,"tcalc:%lf\n",tcalc);
  double ratio = (ttotal - toverlap)/tmin;
  // fprintf(stderr,"\nratio:%lf\n\n",ratio);

  return ratio;
}

void print_first_line(int myrank,FILE** output,comp_time_info_t**comp_time){
  if(myrank != ROOT)
    return;
  int j;
  int i;
  for(i = 0; i < 1; i++){
    fprintf(output[i],"0.00 ");
    for (j=0; j < NB_MATRIX_SIZE; j++){
      fprintf(output[i],"%d-%d-%2f ",comp_time[j]->matrix_size,comp_time[j]->theorical_time,comp_time[j]->real_time->median[0]);
    }
    fprintf(output[i],"\n");
  }
}


void bench_collective(bench_general_t*general,int data_index, int matrix_index,double *t1, double *t2, double *t3){
  int data_size = general->ref_coll_time[data_index]->data_size;
  int matrix_size = general->ref_comp_time[matrix_index]->matrix_size;
#ifndef GLOBAL_BUFFER
  void *data,*data2;
  data = malloc(data_size);
  data2 = malloc(data_size);
  double *a,*b,*res;
  a=malloc(matrix_size*matrix_size*sizeof(double));
  b=malloc(matrix_size*matrix_size*sizeof(double));
  res=calloc(matrix_size*matrix_size,sizeof(double));
#endif
  //static i = 0;
#ifdef SYNC_1
  uint64_t t1_1, t1_2, t2_1,t2_2;
#endif
#ifdef SYNC_2
double t1_1, t1_2, t2_1,t2_2;
mpi_sync_clocks_t clock = *general->clock;
#endif
  MPI_Request req;
  MPI_Status st;

#ifdef SYNC_1
  Process_Sync_Offset = 0;
  //MPI_Barrier(MPI_COMM_WORLD);
  // fprintf(stderr,"sync\n");
  sync_mpi();//make all t0(i) equals
  //fprintf(stderr,"go\n");
  sync_start(myrank);
  Timer_tsc_ref(&t1_1);
#endif
#ifdef SYNC_2
  mpi_sync_clocks_synchronize(clock);
  double barr;
  mpi_sync_clocks_barrier(clock,&barr);
  t1_1 = mpi_sync_clocks_get_time_usec(clock);
#endif
  //fprintf(stderr,"[COLL%d,d_index%d,m_index%d]:data:%p data2:%p,size:%d,coll:%d,req:%p\n",i,data_index,matrix_index,data,data2,data_size,general->options->collective,&req);
#ifdef GLOBAL_BUFFER
  start_collective(data_size,general->options->collective,&req,general->nbprocs);
#else
  start_collective(data,data2,data_size,general->options->collective,&req,general->nbprocs);
#endif
#ifdef SYNC_1
  Timer_tsc_ref(&t2_1);
#endif
#ifdef SYNC_2
  t2_1 = mpi_sync_clocks_get_time_usec(clock);
#endif
  //fprintf(stderr,"compute\n");
#ifdef GLOBAL_BUFFER
  matrix_compute(matrix_size);
#else
  matrix_compute(a,b,res,matrix_size);
#endif
#ifdef SYNC_1
  Timer_tsc_ref(&t2_2);
#endif
#ifdef SYNC_2
  t2_2 = mpi_sync_clocks_get_time_usec(clock);
#endif
  //fprintf(stderr,"wait\n");
  MPI_Wait(&req, &st);
#ifdef SYNC_1
  Timer_tsc_ref(&t1_2);
#endif
#ifdef SYNC_2
  t1_2 = mpi_sync_clocks_get_time_usec(clock);
#endif
  //MPI_Barrier(MPI_COMM_WORLD);
#ifndef GLOBAL_BUFFER
  free(a);
  free(b);
  free(res);
  free(data);
  free(data2);
#endif
#ifdef SYNC_1
  *t1 = (double)(t1_2 - t1_1) * freq;
  *t3 = (double)(t2_1 - t1_1) * freq;
  *t2 = (double)(t2_2 - t2_1) * freq;
#endif
#ifdef SYNC_2
*t1 = mpi_sync_clocks_local_to_global(clock,t1_2) - mpi_sync_clocks_local_to_global(clock,t1_1);
*t3 = mpi_sync_clocks_local_to_global(clock,t2_1) - mpi_sync_clocks_local_to_global(clock,t1_1);
*t2 = mpi_sync_clocks_local_to_global(clock,t2_2) - mpi_sync_clocks_local_to_global(clock,t2_1);
#endif
}

void gather_results(bench_general_t*general, double *t1, double *t2, double *t3, int step_index,int comp_index,int coll_index){
  int nbprocs = general->nbprocs;
  bench_measures_t*measure = general->measures;

  if(general->myrank == ROOT){
    measure->tgeneral.all_ranks[step_index] = malloc(sizeof(double)*nbprocs);
    measure->tcalc.all_ranks[step_index] = malloc(sizeof(double)*nbprocs);
    measure->tcall.all_ranks[step_index] = malloc(sizeof(double)*nbprocs);
    measure->tcalc_ref.all_ranks[step_index] = malloc(sizeof(double)*nbprocs);
    measure->tcoll_ref.all_ranks[step_index] = malloc(sizeof(double)*nbprocs);
  }

  MPI_Gather(t1,1,MPI_DOUBLE,measure->tgeneral.all_ranks[step_index],1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
  MPI_Gather(t2,1,MPI_DOUBLE,measure->tcalc.all_ranks[step_index],1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
  MPI_Gather(t3,1,MPI_DOUBLE,measure->tcall.all_ranks[step_index],1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
  MPI_Gather(general->ref_comp_time[comp_index]->real_time->median,1,MPI_DOUBLE,measure->tcalc_ref.all_ranks[step_index],1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
  MPI_Gather(&general->ref_coll_time[coll_index]->real_time->median[0],1,MPI_DOUBLE,measure->tcoll_ref.all_ranks[step_index],1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);

  if(general->myrank == ROOT){
    measure->tgeneral.median[step_index] = median(measure->tgeneral.all_ranks[step_index],nbprocs);
    measure->tgeneral.min[step_index] = min(measure->tgeneral.all_ranks[step_index],nbprocs);
    measure->tgeneral.max[step_index] = max(measure->tgeneral.all_ranks[step_index],nbprocs);
    measure->tcalc.median[step_index] = median(measure->tcalc.all_ranks[step_index],nbprocs);
    measure->tcalc.min[step_index] = min(measure->tcalc.all_ranks[step_index],nbprocs);
    measure->tcalc.max[step_index] = max(measure->tcalc.all_ranks[step_index],nbprocs);
    measure->tcall.median[step_index] = median(measure->tcall.all_ranks[step_index],nbprocs);
    measure->tcall.min[step_index] = min(measure->tcall.all_ranks[step_index],nbprocs);
    measure->tcall.max[step_index] = max(measure->tcall.all_ranks[step_index],nbprocs);
    measure->tcalc_ref.median[step_index] = median(measure->tcalc_ref.all_ranks[step_index],nbprocs);
    measure->tcalc_ref.min[step_index] = min(measure->tcalc_ref.all_ranks[step_index],nbprocs);
    measure->tcalc_ref.max[step_index] = max(measure->tcalc_ref.all_ranks[step_index],nbprocs);
    measure->tcoll_ref.median[step_index] = median(measure->tcoll_ref.all_ranks[step_index],nbprocs);
    measure->tcoll_ref.min[step_index] = min(measure->tcoll_ref.all_ranks[step_index],nbprocs);
    measure->tcoll_ref.max[step_index] = max(measure->tcoll_ref.all_ranks[step_index],nbprocs);
  }
}

void print_results(bench_general_t*general,int comp_index,int coll_index,int nb_rep){


  bench_measures_t*measure = general->measures;
  FILE**output = general->output;
    comp_time_info_t*ref_comp_time = general->ref_comp_time[comp_index];

  if(general->options->flags & ALL_RANK_PRINT){
    //TODO
  }
  if(general->myrank == ROOT){

    double tcall,tcall_max,tcall_min,tcalc,tcalc_max,tcalc_min,twait,twait_max,twait_min,ttotal,ttotal_max,ttotal_min,or;
    or =overlap_ratio(median(measure->tgeneral.max,nb_rep),median(measure->tcalc_ref.max,nb_rep),median(measure->tcoll_ref.max,nb_rep));
    fprintf(output[0],"%.2lf ",or);
    fflush(output[0]);
    if(general->options->raw_nbop != NULL && coll_index == 0){
     bench_raw_cpu_t raw_nbop = general->options->raw_nbop[comp_index];
      unsigned long int exp_nb_op = (unsigned long int)ref_comp_time->matrix_size*(unsigned long int)ref_comp_time->matrix_size*((unsigned long int)ref_comp_time->matrix_size+((unsigned long int)ref_comp_time->matrix_size - 1));
    fprintf(output[1],"[%d-%d-%lf] %.2lf\n",ref_comp_time->theorical_time,ref_comp_time->matrix_size,ref_comp_time->real_time->median[0],(double)(exp_nb_op/ref_comp_time->real_time->median[0])/(double)(raw_nbop.nb_op/raw_nbop.r_time));
    //fprintf(stderr,"%d %lu %lu\n",ref_comp_time->matrix_size,exp_nb_op,raw_nbop.nb_op);
    fflush(output[1]);
    }
    ttotal = median(measure->tgeneral.median,nb_rep);
    ttotal_min = median(measure->tgeneral.min,nb_rep);
    ttotal_max = median(measure->tgeneral.max,nb_rep);
    tcall = median(measure->tcall.median,nb_rep);
    tcall_min = median(measure->tcall.min,nb_rep);
    tcall_max = median(measure->tcall.max,nb_rep);
    tcalc = median(measure->tcalc.median,nb_rep);
    tcalc_min = median(measure->tcalc.min,nb_rep);
    tcalc_max = median(measure->tcalc.max,nb_rep);
    twait = ttotal - tcalc - tcall;
    twait_min = ttotal_min - tcalc_min - tcall_min;
    twait_max = ttotal_max - tcalc_max - tcall_max;
    fprintf(output[2],"[%d-%d-%lf] %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf %.2lf\n",ref_comp_time->theorical_time,ref_comp_time->matrix_size,ref_comp_time->real_time->median[0],tcall_min,tcalc_min, twait_min,ttotal_min,tcall_max,tcalc_max,twait_max,ttotal_max,tcall,tcalc,twait,ttotal,or);
    fflush(output[2]);

  }
}

comp_time_info_t**init_comp_time(bench_options_t*opt){

  int comp_time = opt->comp_time;
  int mat_size = opt->mat_size;
  if(comp_time > 0 || mat_size > 0){
    int i;
    struct timespec t,t1;
    comp_time_info_t**temp = malloc(sizeof(comp_time_info_t*));
    *temp = malloc(sizeof(comp_time_info_t));
    (*temp)->theorical_time = comp_time;
    if(mat_size ==0){
      (*temp)->matrix_size = init_compute((double)(comp_time));
    }else{
      (*temp)->matrix_size = mat_size;
    }
    int size = (*temp)->matrix_size;
    int nb_rep = get_nb_rep(0,size,NB_REP);
    double* purecalc_time = malloc(sizeof(double)*nb_rep);
#ifndef GLOBAL_BUFFER
    double *a,*b,*res;
    a=malloc(size*size*sizeof(double));
    b=malloc(size*size*sizeof(double));
    res=calloc(size*size,sizeof(double));
#endif
    for(i=0; i < nb_rep; i++){
      clock_gettime(CLOCK_MONOTONIC,&t);
#ifdef GLOBAL_BUFFER
      matrix_compute(size);
#else
      matrix_compute(a,b,res,size);
#endif
      clock_gettime(CLOCK_MONOTONIC,&t1);
      purecalc_time[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
    }
    bench_time_t*times = malloc(sizeof(bench_time_t));//,b,c;
    times->median = malloc(sizeof(double));
    times->var = malloc(sizeof(double));
    times->max = malloc(sizeof(double));
    times->min = malloc(sizeof(double));
    times->median[0] = median(purecalc_time,nb_rep);
    times->var[0] = variance(purecalc_time,nb_rep);
    times->max[0] = max(purecalc_time,nb_rep);
    times->min[0] = min(purecalc_time,nb_rep);
    (*temp)->real_time = times;
#ifndef GLOBAL_BUFFER
    free(a);
    free(b);
    free(res);
#endif
    fprintf(stderr,"comp_time done\n");
    return temp;
  }

  comp_time_info_t** temp = NULL;
  temp =  malloc(sizeof(comp_time_info_t*)*NB_MATRIX_SIZE);
  /*if(flags & USE_STATIC){
    for(i = 0; i < NB_MATRIX_SIZE; i++){

      temp[i] = malloc(sizeof(comp_time_info_t));
      temp[i]->theorical_time = 0;
      temp[i]->matrix_size = MATRIX_SIZES[i];
      temp[i]->real_time = estimate_comp_time(temp[i]->matrix_size);
    }
    return temp;
  }*/

  int j, temp_time = BASE_COMPUTE_TIME;
  for(j = 0; j < NB_MATRIX_SIZE; j++){
    temp[j] = malloc(sizeof(comp_time_info_t));
    temp[j]->theorical_time = temp_time;
    temp[j]->matrix_size = init_compute((double)(temp_time));
    temp_time *= MATRIX_COMP_STEP;
    temp[j]->real_time = estimate_comp_time(temp[j]->matrix_size);
  }
  return temp;
}

double estimate_frequency(int myrank){
  double freq;
  if(myrank == ROOT)
  {
    double* tab = malloc(sizeof(double)*NB_REP_FREQ);
    /*fprintf(stderr,"estimating processor frequence ...\n");*/
    int j;
    for(j = 0; j < NB_REP_FREQ; j++){
      tab[j]  = get_proc_freq();
    }
    freq = median(tab,NB_REP_FREQ);
    fprintf(stderr,"%f Mhz estimated\n",1/freq);
    free(tab);
  }
  MPI_Bcast(&freq,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
  return freq;
}
