/*
 * Bench NBC
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "bench_arg_parser.h"

const char* HELP = "OPTIONS:\n\n"
        "-C: the MPI collective to test(ibcast,ireduce...)\n"
        "-d: specific data size to test\n"
        "-c: specific compute time to test\n"
        "-t: specific collective time to test\n"
        "-o: output file prefix\n"
        "-a: all rank prints\n"
        "-T: Use collective time instead of message size for general bench\n"
        "-S: Use static sizes for general bench\n"
        "-b: Use raw cpu estim file\n"
        "-B: Use blocking call to calibrate base collective time\n\n";

bench_options_t*arg_parser(int argc, char**argv)
{
  bench_options_t*options= malloc(sizeof(bench_options_t));
  options->data_size=0;
  options->coll_time=0;
  options->mat_size=0;
  options->comp_time=0;
  options->file_prefix = NULL;
  options->flags = 0;

  int opt;
  if(argc == 1){
    fprintf(stdout,HELP);
    exit(0);
  }
  while((opt = getopt(argc,argv,"d:c:t:o:C:aThSs:b:B:")) != -1){
    switch(opt){
      case 'd':
        options->data_size = atoi(optarg);
        break;
      case 'c':
        options->comp_time = atoi(optarg);
        break;
      case 't':
        options->coll_time = atoi(optarg);
        break;
      case 'o':
        options->file_prefix = malloc(strlen(optarg)+1);
        strcpy(options->file_prefix,optarg);
        break;
      case 'a':
        options->flags |= ALL_RANK_PRINT;
        break;
      case 'C':
        options->collective = get_coll(optarg);
        break;
      case 'T':
        options->flags |= USE_COLL_TIME;
        break;
      case 'S':
        options->flags |= USE_STATIC;
        break;
      case 's':
        options->mat_size = atoi(optarg);
        break;
      case 'b':
        options->raw_nbop = read_nbop(optarg);
        break;
    case 'B':
      options->flags |= USE_BLOCKING;
      break;
      case 'h':
      default:
        fprintf(stdout,HELP);
        exit(0);
        break;
    }
  }
  if(options->data_size && options->coll_time){
    fprintf(stderr,"ERROR: you can't specify both specific data size and collective time at the same time\n");
    exit(1);
  }

  if(options->mat_size && options->comp_time){
    fprintf(stderr,"ERROR: you can't specify both specific matrix size and compute time at the same time\n");
    exit(1);
  }

  if((((options->data_size || options->coll_time) && !options->comp_time && !options->mat_size) || ((options->mat_size || options->comp_time) && !options->coll_time && !options->data_size))
      ){
    fprintf(stderr,"ERROR: you can't specify only coll_time/data size or comp_time\n");
    exit(2);
  }

  return options;
}

bench_raw_cpu_t*read_nbop(char*file){
  int i;
  bench_raw_cpu_t*rt= malloc(sizeof(bench_raw_cpu_t)*NB_MATRIX_SIZE);
  FILE*f = fopen(file,"r");
  for(i=0; i < NB_MATRIX_SIZE; i++){
    fscanf(f,"%d %lf %d %ld\n",&rt[i].t_time,&rt[i].r_time, &rt[i].m_size,&rt[i].nb_op);
  }
  return rt;
}

FILE**init_file_descriptors(bench_options_t*options,int myrank){
  int i;
  int nb_out = NB_OUTPUT_FILES + (options->flags & ALL_RANK_PRINT);
  FILE**output = malloc(sizeof(FILE*)*nb_out);
  if(options->file_prefix == NULL){
    for(i = 0; i < nb_out; i++){
      output[i] = stdout;
    }
    return output;
  }

  char*n1 = malloc(sizeof(char)*(strlen(options->file_prefix)+11));
  char*n2 = malloc(sizeof(char)*(strlen(options->file_prefix)+12));
  char*n3 = malloc(sizeof(char)*(strlen(options->file_prefix)+13));
  snprintf(n1, strlen(options->file_prefix) + 11, "%s_OL.dat", options->file_prefix);
  snprintf(n2, strlen(options->file_prefix) + 12, "%s_CI.dat", options->file_prefix);
  snprintf(n3, strlen(options->file_prefix) + 12, "%s_raw.dat", options->file_prefix);
  if(options->flags & ALL_RANK_PRINT)
    {
      char*n4 = malloc(sizeof(char)*(strlen(options->file_prefix)+14));
      snprintf(n4, strlen(options->file_prefix)+14, "%s_rank%d.dat", options->file_prefix, myrank);
      if(!(output[3] = fopen(n4,"w")))
	{
	  fprintf(stderr,"ERROR: opening file %s\n", n4);
	  abort();
	}
      free(n4);
    }
  if(!(output[0] = fopen(n1,"w")))
    {
      fprintf(stderr,"ERROR: opening file %s\n", n1);
      abort();
    }
  if(!(output[1] = fopen(n2,"w")))
    {
      fprintf(stderr,"ERROR: opening file %s\n", n2);
      abort();
    }
  if(!(output[2] = fopen(n3,"w")))
    {
      fprintf(stderr,"ERROR: opening file %s\n", n3);
      abort();
    }
  free(n1);
  free(n2);
  free(n3);
  return output;
}

int get_coll(char* s){
  if(strcmp(s,"ibarrier") == 0)
    return CALL_MPI_IBARRIER;
  if(strcmp(s,"ibcast") == 0)
    return CALL_MPI_IBCAST;
  if(strcmp(s,"ireduce") == 0)
    return CALL_MPI_IREDUCE;
  if(strcmp(s,"ialltoall") == 0)
    return CALL_MPI_IALLTOALL;
  if(strcmp(s,"iallreduce") == 0)
    return CALL_MPI_IALLREDUCE;
  if(strcmp(s,"igather") == 0)
    return CALL_MPI_IGATHER;
  if(strcmp(s,"iallgather") == 0)
    return CALL_MPI_IALLGATHER;
  fprintf(stderr,"WARNING:BAD OR NO COLLECTIVE SPECIFIED\n");
  exit(1);
}
