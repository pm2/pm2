# -*- mode: Markdown;-*-
README for Bench NBC
====================

This document describes **Bench NBC** installation and configuration.

For any question, mailto: `Florian.Reynier@inria.fr and` `Alexandre.Denis@inria.fr`

For more information, see: http://pm2.gitlabpages.inria.fr/bench_nbc/


Quick Start
-----------

A quick cheat sheet for the impatient:

    ./configure
    make
    ./bench_raw_estim
    mpiexec -n 64 ./bench_nbc -C iallgather -b bench_raw_estim.out -T -o out

It runs from 30 minutes through 2h, depanding on network speed and
number of nodes. It will create data files based on the "out" token, in
particular out_raw.dat. Then build the **performance report** using:

    ./bench_nbc_gen_graphs out_raw.dat

It outputs pdf graphs. It is possible to transfer data to another host
and extract the performance report with another installation of Bench NBC
so as to not have to install `R` on the computing nodes.

Please send the `out_raw.dat` to <Florian.Reynier@inria.fr> and
<Alexandre.Denis@inria.fr> to have it integrated on the Bench NBC
benchmark web site.


Requirements
------------
  - MPI library
  - autoconf (v 2.50 or later, for git users)
  - OpenMP compiler
  - hwloc
  - R (optional, only to generate graphs)
  - doxygen (optional, for doc generation)


Installation
------------

Bench NBC benchmark follows usual autoconf procedure:

    ./configure [your options here]
    make
    make install

The `make install` step is optional. The benchmark may be run from its
build directory. To get help on supported flags for configure, run:

    ./configure --help

Flags that may be of interest are MPICC= to give the name of the
command to build MPI applications, and --prefix= to give installation
path.


Documentation
-------------

- Bench NBC needs a first non-MPI calibration, performed by `bench_raw_estim`.
  You can give an output filename on the command line to `bench_raw_estim`. If
  no filename is given, the default output is `bench_raw_estim.out`.

- In a second pass, run the MPI benchmark `bench_nbc`. It needs the following args:

  -C <op>
  the collective operation to benchmark. Supported operations are:
    + ibarrier, ibcast, ireduce, ialltoall, iallreduce, igather, iallgather
    
  -b <file>
  the filename containing the output from the first non-MPI benchmark.

  -o <name>
  a basename used to output data

  -T
  use steps by time instead of data size for collective. This parameter is
  mandatory with the current version.

- To generate graphs from data, a set of R scripts are available in the
  `script/` directory. For convenience, command bench_nbc_gen_graphs calls
  them in sequence so as to generate all graphs at once.

