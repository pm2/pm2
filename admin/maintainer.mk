# ## maintainer makefile

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir  := $(dir $(mkfile_path))

PM2_ROOT    := $(mkfile_dir)/..

DATE                    := $(shell date +%Y-%m-%d )
PM2_VERSION             := $(shell cat $(PM2_ROOT)/building-tools/VERSION)
MPIBENCHMARK_VERSION    := $(shell cat $(PM2_ROOT)/mpibenchmark/VERSION)
MPI_SYNC_CLOCKS_VERSION := $(shell cat $(PM2_ROOT)/mpi_sync_clocks/VERSION)

show:
	@echo "DATE                    = $(DATE)"
	@echo "PM2_VERSION             = $(PM2_VERSION)"
	@echo "MPIBENCHMARK_VERSION    = $(MPIBENCHMARK_VERSION)"
	@echo "MPI_SYNC_CLOCKS_VERSION = $(MPI_SYNC_CLOCKS_VERSION)"
	@echo
	@if [ "$(DATE)" != "$(PM2_VERSION)" ]; then echo "inconsistency in version number"; exit 1; fi

# ## bare git checkout, unconfigured

tmp/pm2-git:
	@echo "# ## extracting PM2 from git repo to $@"
	-rm -r ./tmp/pm2-git
	-mkdir -p ./tmp/pm2-git
	( cd `git rev-parse --show-toplevel` && git archive --format=tar master ) | ( cd ./tmp/pm2-git ; tar x )

# ## full PM2

release-pm2: show pm2-$(PM2_VERSION).tar.gz

pm2-$(PM2_VERSION).tar.gz: tmp/pm2-$(PM2_VERSION)
	@echo "# Packaging $@..."
	( cd ./tmp ; tar czf pm2-$(PM2_VERSION).tar.gz ./pm2-$(PM2_VERSION) )
	-rm -r ./tmp/pm2-$(PM2_VERSION)
	@echo
	@echo "# ## file ready: $@"
	@echo

tmp/pm2-$(PM2_VERSION): tmp/pm2-git
	@echo "# ## Generating release $@"
	-rm -r ./tmp/pm2-$(PM2_VERSION)
	cp -a ./tmp/pm2-git ./tmp/pm2-$(PM2_VERSION)
	-rm -r ./tmp/pm2-git
	@echo "# Generating configuration files..."
	( cd ./tmp/pm2-$(PM2_VERSION)/admin && ./autogen.sh )

# ## MPI Sync Clocks

release-mpi_sync_clocks: show mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION).tar.gz

mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION).tar.gz: tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION)
	@echo "# Packaging $@..."
	( cd ./tmp ; tar czf mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION).tar.gz ./mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION) )
	-rm -r ./tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION)
	@echo
	@echo "# ## file ready: $@"
	@echo

tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION): tmp/pm2-git
	@echo "# ## Generating release $@"
	-rm -r ./tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION)
	cp -a ./tmp/pm2-git/mpi_sync_clocks ./tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION)
	cp -a ./tmp/pm2-git/building-tools  ./tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION)/
	( cd ./tmp/mpi_sync_clocks-$(MPI_SYNC_CLOCKS_VERSION) ; ./autogen.sh )
	-rm -r ./tmp/pm2-git


# ## MadMPI Benchmark

release-mpibenchmark: show mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz

mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz: tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	@echo "# Packaging $@..."
	( cd ./tmp ; tar czf mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz ./mpibenchmark-$(MPIBENCHMARK_VERSION) )
	-rm -r ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	@echo
	@echo "# ## file ready: $@"
	@echo

tmp/mpibenchmark-$(MPIBENCHMARK_VERSION): tmp/pm2-git
	@echo "# ## Generating release $@"
	-rm -r ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	cp -a ./tmp/pm2-git/mpibenchmark    ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	cp -a ./tmp/pm2-git/building-tools  ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/
	cp -a ./tmp/pm2-git/mpi_sync_clocks ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/
	( cd ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/mpi_sync_clocks ; ./autogen.sh )
	( cd ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION) ; ./autogen.sh )
	-rm -r ./tmp/pm2-git

# ## Bench NBC

release-bench_nbc: bench_nbc_$(VERSION).tar.gz

bench_nbc_$(VERSION).tar.gz: tmp/bench_nbc_$(VERSION)
	( cd tmp ; tar czf bench_nbc_$(VERSION).tar.gz ./bench_nbc )
	@echo "# ## file ready: $@"

tmp/bench_nbc_$(VERSION):
	-mkdir -p tmp
	-rm -r ./tmp/bench_nbc
	svn export svn+ssh://scm.gforge.inria.fr/svn/pm2/trunk/bench_nbc ./tmp/bench_nbc
	svn export svn+ssh://scm.gforge.inria.fr/svn/pm2/trunk/mpi_sync_clocks ./tmp/bench_nbc/mpi_sync_clocks
	svn export svn+ssh://scm.gforge.inria.fr/svn/padico/PadicoTM/trunk/PadicoTM/building-tools ./tmp/bench_nbc/building-tools
	( cd tmp/bench_nbc/mpi_sync_clocks ; ./autogen.sh )
	( cd tmp/bench_nbc ; ./autogen.sh )

# ## clean

clean:
	-rm -r ./tmp
