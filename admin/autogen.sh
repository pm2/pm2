#! /bin/sh

for d in ../*; do
    if [ -x ${d}/autogen.sh -a ${d} != "../admin" ]; then
	echo "# ## $d ###########"
	( cd $d && ./autogen.sh )
    fi
done
